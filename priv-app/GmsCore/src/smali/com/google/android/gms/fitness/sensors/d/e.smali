.class public final Lcom/google/android/gms/fitness/sensors/d/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/location/n;


# instance fields
.field private final a:Lcom/google/android/gms/fitness/sensors/d/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/fitness/sensors/d/a;)V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/d/e;->a:Lcom/google/android/gms/fitness/sensors/d/a;

    .line 36
    return-void
.end method


# virtual methods
.method public final a(Landroid/location/Location;)V
    .locals 12

    .prologue
    const/4 v6, 0x1

    const/4 v9, 0x0

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/d/e;->a:Lcom/google/android/gms/fitness/sensors/d/a;

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->l:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/sensors/d/a;->a(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/l;

    move-result-object v2

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/d/e;->a:Lcom/google/android/gms/fitness/sensors/d/a;

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->n:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/sensors/d/a;->a(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/l;

    move-result-object v8

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/d/e;->a:Lcom/google/android/gms/fitness/sensors/d/a;

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->p:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/sensors/d/a;->a(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/l;

    move-result-object v3

    .line 49
    if-nez v2, :cond_1

    if-nez v8, :cond_1

    if-nez v3, :cond_1

    .line 50
    const-string v0, "Received location %s with no listeners %s, ignoring"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v9

    iget-object v2, p0, Lcom/google/android/gms/fitness/sensors/d/e;->a:Lcom/google/android/gms/fitness/sensors/d/a;

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 90
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/d/e;->a:Lcom/google/android/gms/fitness/sensors/d/a;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/d/a;->b()Landroid/location/Location;

    move-result-object v4

    .line 56
    const/4 v0, 0x0

    .line 57
    if-eqz v4, :cond_b

    .line 58
    invoke-virtual {p1, v4}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v1

    .line 59
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->aG:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_2

    .line 60
    const-string v0, "Location has only changed by %f meters. Skipping the location point."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v2, v9

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_2
    move v7, v1

    .line 65
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/d/e;->a:Lcom/google/android/gms/fitness/sensors/d/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/fitness/sensors/d/a;->a(Landroid/location/Location;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 66
    const-string v0, "Unable to update previous location."

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 70
    :cond_3
    if-eqz v2, :cond_6

    .line 71
    const-string v0, "Sending Location data"

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/d/e;->a:Lcom/google/android/gms/fitness/sensors/d/a;

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->l:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/sensors/d/a;->b(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v10

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v10, v11, v1}, Lcom/google/android/gms/fitness/data/DataPoint;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    sget-object v1, Lcom/google/android/gms/fitness/data/Field;->f:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/Field;)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v1

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v10

    double-to-float v5, v10

    invoke-virtual {v1, v5}, Lcom/google/android/gms/fitness/data/Value;->a(F)V

    sget-object v1, Lcom/google/android/gms/fitness/data/Field;->g:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/Field;)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v10

    double-to-float v5, v10

    invoke-virtual {v1, v5}, Lcom/google/android/gms/fitness/data/Value;->a(F)V

    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/android/gms/fitness/data/Field;->h:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/Field;)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v1

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/fitness/data/Value;->a(F)V

    :cond_4
    invoke-virtual {p1}, Landroid/location/Location;->hasAltitude()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lcom/google/android/gms/fitness/data/Field;->i:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/Field;)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v1

    invoke-virtual {p1}, Landroid/location/Location;->getAltitude()D

    move-result-wide v10

    double-to-float v5, v10

    invoke-virtual {v1, v5}, Lcom/google/android/gms/fitness/data/Value;->a(F)V

    :cond_5
    :try_start_0
    invoke-interface {v2, v0}, Lcom/google/android/gms/fitness/data/l;->a(Lcom/google/android/gms/fitness/data/DataPoint;)V

    const-string v0, "Finished dispatching location event to listener"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 75
    :cond_6
    :goto_2
    if-eqz v3, :cond_7

    .line 76
    const-string v0, "Sending Speed data"

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 77
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "Location does not have speed: %s Skipping."

    new-array v1, v6, [Ljava/lang/Object;

    aput-object p1, v1, v9

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 80
    :cond_7
    :goto_3
    if-nez v4, :cond_a

    .line 81
    const-string v0, "Not reporting distance data since no previous location was found"

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_0

    .line 72
    :catch_0
    move-exception v0

    const-string v1, "Couldn\'t send location event to listener. Assuming listener is dead."

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    .line 77
    :cond_8
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v1

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->aH:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_9

    const-string v0, "Speed %f is less than threshold. Skipping."

    new-array v2, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v2, v9

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_3

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/d/e;->a:Lcom/google/android/gms/fitness/sensors/d/a;

    sget-object v2, Lcom/google/android/gms/fitness/data/DataType;->p:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/sensors/d/a;->b(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v0

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v10

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v10, v11, v2}, Lcom/google/android/gms/fitness/data/DataPoint;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    sget-object v2, Lcom/google/android/gms/fitness/data/Field;->m:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/Field;)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/fitness/data/Value;->a(F)V

    :try_start_1
    invoke-interface {v3, v0}, Lcom/google/android/gms/fitness/data/l;->a(Lcom/google/android/gms/fitness/data/DataPoint;)V

    const-string v0, "Finished dispatching speed event to listener"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    const-string v1, "Couldn\'t send speed event to listener. Assuming listener is dead."

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_3

    .line 85
    :cond_a
    if-eqz v8, :cond_0

    .line 86
    const-string v0, "Sending Distance data"

    new-array v1, v9, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 87
    invoke-virtual {v4}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/d/e;->a:Lcom/google/android/gms/fitness/sensors/d/a;

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->n:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/sensors/d/a;->b(Lcom/google/android/gms/fitness/data/DataType;)Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/DataSource;)Lcom/google/android/gms/fitness/data/DataPoint;

    move-result-object v1

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sget-object v6, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/fitness/data/DataPoint;->a(JJLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/data/DataPoint;

    sget-object v0, Lcom/google/android/gms/fitness/data/Field;->j:Lcom/google/android/gms/fitness/data/Field;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Lcom/google/android/gms/fitness/data/Field;)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/gms/fitness/data/Value;->a(F)V

    :try_start_2
    invoke-interface {v8, v1}, Lcom/google/android/gms/fitness/data/l;->a(Lcom/google/android/gms/fitness/data/DataPoint;)V

    const-string v0, "Finished dispatching distance event to listener"

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    const-string v1, "Couldn\'t send distance event to listener. Assuming listener is dead."

    new-array v2, v9, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto/16 :goto_0

    :cond_b
    move v7, v0

    goto/16 :goto_1
.end method
