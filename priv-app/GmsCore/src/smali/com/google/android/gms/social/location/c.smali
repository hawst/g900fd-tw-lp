.class public final Lcom/google/android/gms/social/location/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:Z

.field private static c:I

.field private static d:Landroid/graphics/Bitmap;

.field private static e:Landroid/graphics/Paint;


# instance fields
.field a:Landroid/graphics/Bitmap;

.field private f:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    sget-boolean v0, Lcom/google/android/gms/social/location/c;->b:Z

    if-nez v0, :cond_0

    sput-boolean v2, Lcom/google/android/gms/social/location/c;->b:Z

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    sput-object v1, Lcom/google/android/gms/social/location/c;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    sget-object v1, Lcom/google/android/gms/social/location/c;->e:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/ColorMatrix;

    invoke-direct {v2}, Landroid/graphics/ColorMatrix;-><init>()V

    const v3, 0x3f7d70a4    # 0.99f

    invoke-virtual {v2, v3}, Landroid/graphics/ColorMatrix;->setSaturation(F)V

    new-instance v3, Landroid/graphics/ColorMatrixColorFilter;

    invoke-direct {v3, v2}, Landroid/graphics/ColorMatrixColorFilter;-><init>(Landroid/graphics/ColorMatrix;)V

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    sget v1, Lcom/google/android/gms/g;->b:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    sput v1, Lcom/google/android/gms/social/location/c;->c:I

    sget v1, Lcom/google/android/gms/h;->Q:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    sget v1, Lcom/google/android/gms/social/location/c;->c:I

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/k;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/social/location/c;->d:Landroid/graphics/Bitmap;

    .line 36
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/social/location/c;->f:Landroid/content/Context;

    .line 37
    return-void
.end method


# virtual methods
.method public final a()Landroid/graphics/Bitmap;
    .locals 3

    .prologue
    .line 53
    iget-object v1, p0, Lcom/google/android/gms/social/location/c;->f:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/social/location/c;->a:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/social/location/c;->d:Landroid/graphics/Bitmap;

    :goto_0
    sget-object v2, Lcom/google/android/gms/social/location/c;->e:Landroid/graphics/Paint;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/common/util/k;->a(Landroid/content/Context;Landroid/graphics/Bitmap;Landroid/graphics/Paint;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/social/location/c;->a:Landroid/graphics/Bitmap;

    sget v2, Lcom/google/android/gms/social/location/c;->c:I

    invoke-static {v0, v2}, Lcom/google/android/gms/common/util/k;->a(Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method
