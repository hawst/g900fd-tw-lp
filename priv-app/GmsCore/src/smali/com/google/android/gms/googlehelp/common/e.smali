.class public final Lcom/google/android/gms/googlehelp/common/e;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x5
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 31
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "data2"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/googlehelp/common/e;->a:[Ljava/lang/String;

    .line 34
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "vnd.android.cursor.item/name"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/googlehelp/common/e;->b:[Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)Lcom/google/ad/a/a/u;
    .locals 3

    .prologue
    .line 132
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 133
    const/4 v0, 0x0

    .line 161
    :cond_0
    :goto_0
    return-object v0

    .line 136
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/common/util/s;->a(Landroid/content/Context;)V

    .line 138
    const-string v0, "escalation_options"

    new-instance v1, Lcom/google/ad/a/a/u;

    invoke-direct {v1}, Lcom/google/ad/a/a/u;-><init>()V

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/gms/googlehelp/common/f;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/ad/a/a/u;

    .line 141
    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 142
    iget-object v1, v0, Lcom/google/ad/a/a/u;->c:Lcom/google/ad/a/a/q;

    if-eqz v1, :cond_2

    .line 143
    const-string v1, "ongoing_chat_request_pool_id"

    const-string v2, ""

    invoke-virtual {p2, v1, v2}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 146
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 147
    iget-object v2, v0, Lcom/google/ad/a/a/u;->c:Lcom/google/ad/a/a/q;

    iput-object v1, v2, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    .line 151
    :cond_2
    iget-object v1, v0, Lcom/google/ad/a/a/u;->e:Lcom/google/ad/a/a/w;

    if-eqz v1, :cond_0

    .line 152
    const-string v1, "ongoing_video_request_pool_id"

    const-string v2, ""

    invoke-virtual {p2, v1, v2}, Lcom/google/android/gms/googlehelp/common/a;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 155
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 156
    iget-object v2, v0, Lcom/google/ad/a/a/u;->e:Lcom/google/ad/a/a/w;

    iput-object v1, v2, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Ljava/lang/String;
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 65
    const-string v6, ""

    .line 66
    const/4 v7, 0x0

    .line 69
    :try_start_0
    sget-object v0, Landroid/provider/ContactsContract$Profile;->CONTENT_URI:Landroid/net/Uri;

    const-string v1, "data"

    invoke-static {v0, v1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 72
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/googlehelp/common/e;->a:[Ljava/lang/String;

    const-string v3, "mimetype = ?"

    sget-object v4, Lcom/google/android/gms/googlehelp/common/e;->b:[Ljava/lang/String;

    const-string v5, "is_primary DESC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 79
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 83
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 92
    :goto_0
    if-eqz v1, :cond_0

    .line 93
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 96
    :cond_0
    :goto_1
    return-object v0

    .line 85
    :catch_0
    move-exception v0

    move-object v1, v7

    .line 89
    :goto_2
    :try_start_2
    const-string v2, "GOOGLEHELP_EscalationOptionsUtil"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": Querying the DB for profile info threw: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 92
    if-eqz v1, :cond_2

    .line 93
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move-object v0, v6

    goto :goto_1

    .line 92
    :catchall_0
    move-exception v0

    move-object v1, v7

    :goto_3
    if-eqz v1, :cond_1

    .line 93
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_1
    throw v0

    .line 92
    :catchall_1
    move-exception v0

    goto :goto_3

    .line 85
    :catch_1
    move-exception v0

    goto :goto_2

    :cond_2
    move-object v0, v6

    goto :goto_1

    :cond_3
    move-object v0, v6

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 53
    const-string v0, "name"

    const-string v1, ""

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/gms/googlehelp/common/i;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 55
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xe

    if-lt v1, v2, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 57
    invoke-static {p0}, Lcom/google/android/gms/googlehelp/common/e;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 59
    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 121
    new-instance v0, Lcom/google/android/gms/googlehelp/common/g;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/googlehelp/common/g;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/g;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "display_country"

    invoke-virtual {v0, v1, p3}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "phone_number"

    invoke-virtual {v0, v1, p4}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 127
    return-void
.end method

.method public static a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V
    .locals 4

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->q()Ljava/lang/String;

    move-result-object v0

    .line 202
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/a;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    const-string v2, "ongoing_chat_request_pool_id"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "chat_version:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "chat_convo_id:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "chat_queue_pos:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "chat_failed_attempts:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "hangout_was_opened"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 210
    return-void
.end method

.method public static a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;Lcom/google/ad/a/a/b;)V
    .locals 6

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->q()Ljava/lang/String;

    move-result-object v0

    .line 170
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/a;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    const-string v2, "ongoing_chat_request_pool_id"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "chat_version:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-wide v4, p2, Lcom/google/ad/a/a/b;->c:J

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;J)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "chat_convo_id:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p2, Lcom/google/ad/a/a/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "chat_queue_pos:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v2, p2, Lcom/google/ad/a/a/b;->a:I

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;I)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 179
    return-void
.end method

.method public static a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;Lcom/google/ad/a/a/z;)V
    .locals 6

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->s()Ljava/lang/String;

    move-result-object v0

    .line 187
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/a;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    const-string v2, "ongoing_video_request_pool_id"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "vc_version:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-wide v4, p2, Lcom/google/ad/a/a/z;->c:J

    invoke-virtual {v1, v2, v4, v5}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;J)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "vc_url:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p2, Lcom/google/ad/a/a/z;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "vc_queue_pos:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v2, p2, Lcom/google/ad/a/a/z;->a:I

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;I)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 197
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 107
    const-string v0, "phone_number"

    const-string v1, ""

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/gms/googlehelp/common/i;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 109
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 110
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    .line 113
    :cond_0
    invoke-static {v0}, Landroid/telephony/PhoneNumberUtils;->stripSeparators(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->isGlobalPhoneNumber(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    return-object v0

    :cond_1
    const-string v0, ""

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V
    .locals 4

    .prologue
    .line 229
    new-instance v0, Lcom/google/android/gms/googlehelp/common/g;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/googlehelp/common/g;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/g;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "name"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "display_country"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "phone_number"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "locale"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 237
    const-string v0, "escalation_options"

    new-instance v1, Lcom/google/android/gms/googlehelp/common/f;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/googlehelp/common/f;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    new-instance v2, Ljava/io/File;

    iget-object v3, v1, Lcom/google/android/gms/googlehelp/common/f;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v3

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/common/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v3, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 240
    :cond_0
    if-eqz p2, :cond_1

    .line 242
    invoke-static {p1, p2}, Lcom/google/android/gms/googlehelp/common/e;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V

    .line 245
    invoke-static {p1, p2}, Lcom/google/android/gms/googlehelp/common/e;->b(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V

    .line 247
    :cond_1
    return-void
.end method

.method public static b(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V
    .locals 4

    .prologue
    .line 214
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->s()Ljava/lang/String;

    move-result-object v0

    .line 215
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/a;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    const-string v2, "ongoing_video_request_pool_id"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "vc_version:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "vc_url:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "vc_queue_pos:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "vc_failed_attempts:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "hangout_was_opened"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 225
    return-void
.end method
