.class public final Lcom/google/android/gms/people/service/a/b/b;
.super Lcom/google/android/gms/people/service/a/b/c;
.source "SourceFile"


# instance fields
.field private final i:Ljava/lang/String;

.field private final j:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 9

    .prologue
    .line 41
    move-object v1, p0

    move-object v2, p1

    move-object v3, p5

    move-object v4, p2

    move v5, p3

    move-object v6, p4

    move-object v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/people/service/a/b/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    move-object/from16 v0, p8

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/b;->i:Ljava/lang/String;

    .line 44
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/b;->j:Ljava/util/List;

    .line 45
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 55
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/u;->b()V

    .line 57
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/b;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/b;->j:Ljava/util/List;

    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->i(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 61
    :catch_0
    move-exception v0

    .line 63
    iget-object v2, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v2, :cond_4

    iget-object v2, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v2, v2, Lcom/android/volley/m;->a:I

    const/16 v3, 0x193

    if-ne v2, v3, :cond_4

    .line 65
    new-instance v0, Landroid/util/Pair;

    sget-object v2, Lcom/google/android/gms/people/service/b;->j:Lcom/google/android/gms/people/service/b;

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 110
    :goto_1
    return-object v0

    .line 57
    :cond_0
    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {p2, p3, v2, v5, v4}, Lcom/google/android/gms/people/service/e;->c(Lcom/google/android/gms/people/e/a;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;

    move-result-object v2

    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v4, "PeopleService"

    const-string v5, "%s people added to %s"

    const/4 v0, 0x2

    new-array v6, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v0

    const/4 v3, 0x1

    if-nez v2, :cond_3

    move-object v0, v1

    :goto_2
    aput-object v0, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/u;->b()V
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    .line 70
    if-nez v2, :cond_5

    .line 71
    new-instance v0, Landroid/util/Pair;

    sget-object v2, Lcom/google/android/gms/people/service/b;->f:Lcom/google/android/gms/people/service/b;

    invoke-direct {v0, v2, v1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1

    .line 57
    :cond_3
    :try_start_2
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->e()Ljava/lang/String;
    :try_end_2
    .catch Lcom/android/volley/ac; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    goto :goto_2

    .line 67
    :cond_4
    throw v0

    .line 79
    :cond_5
    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    .line 80
    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/b;->g:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/b;->h:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/people/service/a/b/b;->i:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/people/service/a/b/b;->j:Ljava/util/List;

    invoke-virtual {v0, v3, v4, v5, v6}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)I

    move-result v0

    .line 84
    if-ne v0, v8, :cond_7

    .line 85
    new-instance v0, Landroid/content/Intent;

    const-string v3, "com.google.android.gms.people.BROADCAST_CIRCLES_CHANGED"

    invoke-direct {v0, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "com.google.android.gms.permission.INTERNAL_BROADCAST"

    invoke-virtual {p1, v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 94
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/b;->h:Ljava/lang/String;

    if-nez v0, :cond_6

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/b;->g:Ljava/lang/String;

    const-string v4, "AddPeopleToCircle"

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/people/sync/u;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_6
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 103
    const-string v0, "circle_id"

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/b;->i:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    const-string v4, "circle_name"

    if-nez v2, :cond_8

    move-object v0, v1

    :goto_4
    invoke-virtual {v3, v4, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 108
    const-string v1, "added_people"

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/b;->j:Ljava/util/List;

    sget-object v2, Lcom/google/android/gms/people/internal/at;->d:[Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v3, v1, v0}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 110
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    invoke-direct {v0, v1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 89
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/b;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/b;->g:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/b;->h:Ljava/lang/String;

    const-string v5, "AddPeopleToCircle"

    invoke-virtual {v0, v3, v4, v5}, Lcom/google/android/gms/people/sync/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    goto :goto_3

    .line 104
    :cond_8
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/CircleEntity;->d()Ljava/lang/String;

    move-result-object v0

    goto :goto_4
.end method
