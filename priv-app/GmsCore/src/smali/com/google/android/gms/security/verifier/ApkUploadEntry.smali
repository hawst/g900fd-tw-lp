.class public Lcom/google/android/gms/security/verifier/ApkUploadEntry;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:J

.field public final b:Ljava/lang/String;

.field public final c:I

.field public final d:[B

.field public final e:J

.field public final f:I

.field public final g:J

.field public final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 97
    new-instance v0, Lcom/google/android/gms/security/verifier/g;

    invoke-direct {v0}, Lcom/google/android/gms/security/verifier/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;I[BJIJI)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-wide p1, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->a:J

    .line 48
    iput-object p3, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    .line 49
    iput p4, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->c:I

    .line 50
    iput-object p5, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->d:[B

    .line 51
    iput-wide p6, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->e:J

    .line 52
    iput p8, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->f:I

    .line 53
    iput-wide p9, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->g:J

    .line 54
    iput p11, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->h:I

    .line 55
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I[BJ)V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    const/4 v0, 0x0

    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-wide v2, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->a:J

    .line 60
    iput-object p1, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    .line 61
    iput p2, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->c:I

    .line 62
    iput-object p3, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->d:[B

    .line 63
    iput-wide p4, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->e:J

    .line 64
    iput v0, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->f:I

    .line 65
    iput-wide v2, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->g:J

    .line 66
    iput v0, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->h:I

    .line 67
    return-void
.end method


# virtual methods
.method public final a(I)Z
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->h:I

    add-int/lit8 v0, v0, 0x1

    if-lt v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 93
    const-string v0, "id: %d, package_name=%s, version_code=%d, state=%d"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-wide v4, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget v3, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->f:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 81
    iget-wide v0, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->a:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 83
    iget v0, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->d:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 85
    iget-wide v0, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 86
    iget v0, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 87
    iget-wide v0, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    .line 88
    iget v0, p0, Lcom/google/android/gms/security/verifier/ApkUploadEntry;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 89
    return-void
.end method
