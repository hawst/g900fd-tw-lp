.class final Lcom/google/android/gms/fitness/k/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:J

.field b:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 278
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 279
    const-wide v0, 0x7fffffffffffffffL

    iput-wide v0, p0, Lcom/google/android/gms/fitness/k/h;->a:J

    .line 280
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/fitness/k/h;->b:J

    .line 281
    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 1

    .prologue
    .line 283
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 284
    iput-wide p1, p0, Lcom/google/android/gms/fitness/k/h;->a:J

    .line 285
    iput-wide p3, p0, Lcom/google/android/gms/fitness/k/h;->b:J

    .line 286
    return-void
.end method


# virtual methods
.method final a(Lcom/google/android/gms/fitness/k/h;)Lcom/google/android/gms/fitness/k/h;
    .locals 4

    .prologue
    .line 289
    iget-wide v0, p1, Lcom/google/android/gms/fitness/k/h;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/fitness/k/h;->a:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/fitness/k/h;->a:J

    .line 290
    iget-wide v0, p1, Lcom/google/android/gms/fitness/k/h;->b:J

    iget-wide v2, p0, Lcom/google/android/gms/fitness/k/h;->b:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/fitness/k/h;->b:J

    .line 291
    return-object p0
.end method
