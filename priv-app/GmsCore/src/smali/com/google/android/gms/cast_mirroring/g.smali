.class final Lcom/google/android/gms/cast_mirroring/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/google/android/gms/cast_mirroring/f;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast_mirroring/f;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/gms/cast_mirroring/g;->b:Lcom/google/android/gms/cast_mirroring/f;

    iput-object p2, p0, Lcom/google/android/gms/cast_mirroring/g;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 110
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/g;->b:Lcom/google/android/gms/cast_mirroring/f;

    iget-object v0, v0, Lcom/google/android/gms/cast_mirroring/f;->b:Lcom/google/android/gms/cast/media/CastMirroringProvider;

    iget-object v1, p0, Lcom/google/android/gms/cast_mirroring/g;->b:Lcom/google/android/gms/cast_mirroring/f;

    iget-object v1, v1, Lcom/google/android/gms/cast_mirroring/f;->c:Lcom/google/android/gms/cast_mirroring/b/a;

    iget-object v2, p0, Lcom/google/android/gms/cast_mirroring/g;->b:Lcom/google/android/gms/cast_mirroring/f;

    invoke-static {v2}, Lcom/google/android/gms/cast_mirroring/f;->a(Lcom/google/android/gms/cast_mirroring/f;)Lcom/google/android/gms/cast_mirroring/b/f;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/cast_mirroring/g;->b:Lcom/google/android/gms/cast_mirroring/f;

    invoke-static {v3}, Lcom/google/android/gms/cast_mirroring/f;->b(Lcom/google/android/gms/cast_mirroring/f;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/cast_mirroring/g;->b:Lcom/google/android/gms/cast_mirroring/f;

    invoke-static {v4}, Lcom/google/android/gms/cast_mirroring/f;->c(Lcom/google/android/gms/cast_mirroring/f;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/gms/cast/media/CastMirroringProvider;->a(Lcom/google/android/gms/cast_mirroring/b/a;Lcom/google/android/gms/cast_mirroring/b/f;Ljava/lang/String;Landroid/app/PendingIntent;)V

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/g;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/cast_mirroring/CastRemoteMirroringService;->a(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/cast_mirroring/g;->b:Lcom/google/android/gms/cast_mirroring/f;

    iget-object v0, v0, Lcom/google/android/gms/cast_mirroring/f;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 116
    return-void

    .line 115
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/cast_mirroring/g;->b:Lcom/google/android/gms/cast_mirroring/f;

    iget-object v1, v1, Lcom/google/android/gms/cast_mirroring/f;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    throw v0
.end method
