.class public final Lcom/google/android/gms/cast/c/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/cast/e/h;

.field private static b:J

.field private static final c:Ljava/lang/String;

.field private static final d:Ljava/lang/String;


# instance fields
.field private final e:Landroid/os/Handler;

.field private final f:Ljava/util/Map;

.field private final g:Ljava/util/Set;

.field private final h:Ljava/util/Set;

.field private final i:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastNearbyDeviceFilter"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/c/a;->a:Lcom/google/android/gms/cast/e/h;

    .line 31
    sget-object v0, Lcom/google/android/gms/cast/a/b;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/cast/c/a;->b:J

    .line 34
    sget-object v0, Lcom/google/android/gms/cast/a/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/cast/c/a;->c:Ljava/lang/String;

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/cast/d/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/search"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/c/a;->d:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/a;->e:Landroid/os/Handler;

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/a;->f:Ljava/util/Map;

    .line 42
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/a;->g:Ljava/util/Set;

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/a;->h:Ljava/util/Set;

    .line 64
    iput-object p1, p0, Lcom/google/android/gms/cast/c/a;->i:Landroid/content/Context;

    .line 65
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/cast/c/a;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->i:Landroid/content/Context;

    return-object v0
.end method

.method private a(Ljava/lang/String;J)Lcom/google/android/gms/cast/c/c;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 121
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 134
    :goto_0
    return-object v0

    .line 124
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/cast/c/a;->f:Ljava/util/Map;

    monitor-enter v2

    .line 125
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/c;

    .line 126
    if-nez v0, :cond_1

    .line 127
    monitor-exit v2

    move-object v0, v1

    goto :goto_0

    .line 129
    :cond_1
    iget-wide v4, v0, Lcom/google/android/gms/cast/c/c;->b:J

    sub-long v4, p2, v4

    sget-wide v6, Lcom/google/android/gms/cast/c/a;->b:J

    cmp-long v3, v4, v6

    if-lez v3, :cond_2

    .line 130
    sget-object v0, Lcom/google/android/gms/cast/c/a;->a:Lcom/google/android/gms/cast/e/h;

    const-string v3, "Result has expired."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 131
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    goto :goto_0

    .line 134
    :cond_2
    monitor-exit v2

    goto :goto_0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/c/a;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/gms/cast/c/a;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 199
    iget-object v1, p0, Lcom/google/android/gms/cast/c/a;->g:Ljava/util/Set;

    monitor-enter v1

    .line 200
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/d;

    .line 201
    invoke-interface {v0, p1}, Lcom/google/android/gms/cast/c/d;->a(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 203
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/cast/c/a;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->e:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/gms/cast/c/a;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/cast/c/a;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->f:Ljava/util/Map;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 191
    iget-object v1, p0, Lcom/google/android/gms/cast/c/a;->g:Ljava/util/Set;

    monitor-enter v1

    .line 192
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/d;

    .line 193
    invoke-interface {v0}, Lcom/google/android/gms/cast/c/d;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/cast/c/a;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->h:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/cast/c/a;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/a;->c()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 111
    sget-object v0, Lcom/google/android/gms/cast/c/a;->a:Lcom/google/android/gms/cast/e/h;

    const-string v1, "stopFiltering"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 112
    iget-object v1, p0, Lcom/google/android/gms/cast/c/a;->h:Ljava/util/Set;

    monitor-enter v1

    .line 113
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/b;

    .line 114
    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Lcom/google/android/gms/cast/c/b;->cancel(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 116
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 117
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Lcom/google/android/gms/cast/c/d;)V
    .locals 2

    .prologue
    .line 68
    iget-object v1, p0, Lcom/google/android/gms/cast/c/a;->g:Ljava/util/Set;

    monitor-enter v1

    .line 69
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 70
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 80
    sget-object v0, Lcom/google/android/gms/cast/c/a;->a:Lcom/google/android/gms/cast/e/h;

    const-string v1, "startFiltering"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    iget-object v1, p0, Lcom/google/android/gms/cast/c/a;->h:Ljava/util/Set;

    monitor-enter v1

    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 83
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/aa;

    .line 84
    iget-object v0, v0, Lcom/google/android/gms/cast/c/aa;->b:Ljava/lang/String;

    .line 87
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    sget-object v3, Lcom/google/android/gms/cast/c/a;->c:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 88
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    .line 91
    invoke-direct {p0, v0, v4, v5}, Lcom/google/android/gms/cast/c/a;->a(Ljava/lang/String;J)Lcom/google/android/gms/cast/c/c;

    move-result-object v3

    .line 92
    if-nez v3, :cond_1

    .line 93
    new-instance v3, Lcom/google/android/gms/cast/c/b;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gms/cast/c/b;-><init>(Lcom/google/android/gms/cast/c/a;Ljava/lang/String;)V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->h:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 95
    :cond_1
    :try_start_1
    iget-boolean v3, v3, Lcom/google/android/gms/cast/c/c;->a:Z

    if-eqz v3, :cond_0

    .line 96
    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/c/a;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :cond_2
    sget-object v0, Lcom/google/android/gms/cast/c/a;->a:Lcom/google/android/gms/cast/e/h;

    const-string v2, "# of requests: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/cast/c/a;->h:Ljava/util/Set;

    invoke-interface {v5}, Ljava/util/Set;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 101
    invoke-direct {p0}, Lcom/google/android/gms/cast/c/a;->c()V

    .line 107
    :cond_3
    monitor-exit v1

    return-void

    .line 103
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->h:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/b;

    .line 104
    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/cast/c/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final b(Lcom/google/android/gms/cast/c/d;)V
    .locals 2

    .prologue
    .line 74
    iget-object v1, p0, Lcom/google/android/gms/cast/c/a;->g:Ljava/util/Set;

    monitor-enter v1

    .line 75
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/a;->g:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 76
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
