.class final Lcom/google/android/gms/identity/service/c;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/identity/service/AddressService;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/identity/service/AddressService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/gms/identity/service/c;->a:Lcom/google/android/gms/identity/service/AddressService;

    .line 47
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 48
    return-void
.end method


# virtual methods
.method public final d(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 55
    if-gtz p2, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "clientVersion too old"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/identity/service/c;->a:Lcom/google/android/gms/identity/service/AddressService;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 62
    new-instance v0, Lcom/google/android/gms/identity/service/a;

    iget-object v1, p0, Lcom/google/android/gms/identity/service/c;->a:Lcom/google/android/gms/identity/service/AddressService;

    invoke-direct {v0, v1, p3}, Lcom/google/android/gms/identity/service/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/identity/service/a;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 65
    const/4 v1, 0x0

    const/4 v2, 0x0

    :try_start_0
    invoke-interface {p1, v1, v0, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v0

    const-string v0, "AddressService"

    const-string v1, "Client died while brokering Address service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
