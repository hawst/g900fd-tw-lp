.class final Lcom/google/android/gms/plus/plusone/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/internal/au;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/plusone/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/plus/plusone/e;)V
    .locals 0

    .prologue
    .line 179
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/i;->a:Lcom/google/android/gms/plus/plusone/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/data/a/c;)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    .line 182
    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 183
    const-string v0, "PlusOneButtonView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSignUpStateLoaded: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz p2, :cond_2

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/i;->a:Lcom/google/android/gms/plus/plusone/e;

    invoke-static {v0, p2}, Lcom/google/android/gms/plus/plusone/e;->a(Lcom/google/android/gms/plus/plusone/e;Lcom/google/android/gms/plus/data/a/c;)Lcom/google/android/gms/plus/data/a/c;

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/i;->a:Lcom/google/android/gms/plus/plusone/e;

    invoke-static {v0}, Lcom/google/android/gms/plus/plusone/e;->b(Lcom/google/android/gms/plus/plusone/e;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/i;->a:Lcom/google/android/gms/plus/plusone/e;

    invoke-static {v1}, Lcom/google/android/gms/plus/plusone/e;->f(Lcom/google/android/gms/plus/plusone/e;)Lcom/google/android/gms/plus/internal/aq;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/plusone/i;->a:Lcom/google/android/gms/plus/plusone/e;

    iget-object v2, v2, Lcom/google/android/gms/plus/plusone/e;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/aq;Ljava/lang/String;)V

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/i;->a:Lcom/google/android/gms/plus/plusone/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/plusone/e;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 193
    const-string v0, "PlusOneButtonView"

    invoke-static {v0, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 194
    const-string v0, "PlusOneButtonView"

    const-string v1, "onSignUpStateLoaded: performing pending click"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 199
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/i;->a:Lcom/google/android/gms/plus/plusone/e;

    iput-boolean v3, v0, Lcom/google/android/gms/plus/plusone/e;->a:Z

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/i;->a:Lcom/google/android/gms/plus/plusone/e;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/plusone/e;->performClick()Z

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/i;->a:Lcom/google/android/gms/plus/plusone/e;

    iput-boolean v3, v0, Lcom/google/android/gms/plus/plusone/e;->n:Z

    .line 206
    :cond_2
    return-void
.end method
