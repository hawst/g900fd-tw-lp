.class public final Lcom/google/android/gms/wallet/service/ia/at;
.super Lcom/google/android/gms/wallet/service/ia/m;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/wallet/service/ia/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/service/ia/m;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ia/m;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/at;->a:Lcom/google/android/gms/wallet/service/ia/m;

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 109
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 110
    const/4 v0, 0x0

    .line 112
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/at;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 128
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 129
    const/4 v0, 0x0

    .line 131
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/at;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 119
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 120
    const/4 v0, 0x0

    .line 122
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/at;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 46
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    const/4 v0, 0x0

    .line 49
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/at;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 28
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 29
    const/4 v0, 0x0

    .line 31
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/at;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 82
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 83
    const/4 v0, 0x0

    .line 85
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/at;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 100
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 101
    const/4 v0, 0x0

    .line 103
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/at;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 91
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 92
    const/4 v0, 0x0

    .line 94
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/at;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 137
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 138
    const/4 v0, 0x0

    .line 140
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/at;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 64
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    const/4 v0, 0x0

    .line 67
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/at;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 73
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 74
    const/4 v0, 0x0

    .line 76
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/at;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 55
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    const/4 v0, 0x0

    .line 58
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/at;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 37
    invoke-static {}, Lcom/google/android/gms/wallet/shared/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 38
    const/4 v0, 0x0

    .line 40
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/at;->a:Lcom/google/android/gms/wallet/service/ia/m;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0
.end method
