.class public Lcom/google/android/gms/car/CarCall;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public final b:I

.field public c:Lcom/google/android/gms/car/CarCall;

.field public d:Ljava/util/List;

.field public e:Ljava/lang/String;

.field public f:I

.field public g:Lcom/google/android/gms/car/CarCall$Details;

.field public h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/car/cr;

    invoke-direct {v0}, Lcom/google/android/gms/car/cr;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IILcom/google/android/gms/car/CarCall;Ljava/util/List;Ljava/lang/String;ILcom/google/android/gms/car/CarCall$Details;Z)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput p1, p0, Lcom/google/android/gms/car/CarCall;->a:I

    .line 69
    iput p2, p0, Lcom/google/android/gms/car/CarCall;->b:I

    .line 70
    iput-object p3, p0, Lcom/google/android/gms/car/CarCall;->c:Lcom/google/android/gms/car/CarCall;

    .line 71
    iput-object p4, p0, Lcom/google/android/gms/car/CarCall;->d:Ljava/util/List;

    .line 72
    iput-object p5, p0, Lcom/google/android/gms/car/CarCall;->e:Ljava/lang/String;

    .line 73
    iput p6, p0, Lcom/google/android/gms/car/CarCall;->f:I

    .line 74
    iput-object p7, p0, Lcom/google/android/gms/car/CarCall;->g:Lcom/google/android/gms/car/CarCall$Details;

    .line 75
    iput-boolean p8, p0, Lcom/google/android/gms/car/CarCall;->h:Z

    .line 76
    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/car/CarCall;Ljava/util/List;Ljava/lang/String;ILcom/google/android/gms/car/CarCall$Details;Z)V
    .locals 9

    .prologue
    .line 48
    const/4 v1, 0x1

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v6, p5

    move-object v7, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/car/CarCall;-><init>(IILcom/google/android/gms/car/CarCall;Ljava/util/List;Ljava/lang/String;ILcom/google/android/gms/car/CarCall$Details;Z)V

    .line 56
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/gms/car/CarCall;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 94
    instance-of v0, p1, Lcom/google/android/gms/car/CarCall;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/CarCall;->b:I

    check-cast p1, Lcom/google/android/gms/car/CarCall;

    iget v1, p1, Lcom/google/android/gms/car/CarCall;->b:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 99
    iget v0, p0, Lcom/google/android/gms/car/CarCall;->b:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 104
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CarCall{id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/car/CarCall;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", parent="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall;->c:Lcom/google/android/gms/car/CarCall;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", cannedTextResponses="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall;->d:Ljava/util/List;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", remainingPostDialSequence=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x27

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", state="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/CarCall;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", details="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall;->g:Lcom/google/android/gms/car/CarCall$Details;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hasChildren="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/car/CarCall;->h:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 85
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/cr;->a(Lcom/google/android/gms/car/CarCall;Landroid/os/Parcel;I)V

    .line 86
    return-void
.end method
