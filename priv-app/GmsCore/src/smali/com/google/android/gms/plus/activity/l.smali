.class final Lcom/google/android/gms/plus/activity/l;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"


# instance fields
.field a:Landroid/os/Handler;

.field private final g:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;[Ljava/lang/String;)V

    .line 42
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/activity/l;->a:Landroid/os/Handler;

    .line 54
    iput-object p4, p0, Lcom/google/android/gms/plus/activity/l;->g:[Ljava/lang/String;

    .line 55
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 31
    invoke-static {p1}, Lcom/google/android/gms/plus/internal/j;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/i;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 4

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 142
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 143
    const-string v2, "skip_oob"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 144
    const-string v2, "request_visible_actions"

    iget-object v3, p0, Lcom/google/android/gms/plus/activity/l;->g:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 145
    const-string v2, "auth_package"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    new-instance v2, Lcom/google/android/gms/common/internal/GetServiceRequest;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Lcom/google/android/gms/common/internal/GetServiceRequest;-><init>(I)V

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/internal/GetServiceRequest;->a(Ljava/lang/String;)Lcom/google/android/gms/common/internal/GetServiceRequest;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->d:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/common/util/as;->a([Ljava/lang/String;)[Lcom/google/android/gms/common/api/Scope;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->a([Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/internal/GetServiceRequest;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/GetServiceRequest;->a(Landroid/os/Bundle;)Lcom/google/android/gms/common/internal/GetServiceRequest;

    move-result-object v0

    .line 150
    invoke-interface {p1, p2, v0}, Lcom/google/android/gms/common/internal/bj;->b(Lcom/google/android/gms/common/internal/bg;Lcom/google/android/gms/common/internal/GetServiceRequest;)V

    .line 151
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/util/w;)V
    .locals 3

    .prologue
    .line 64
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    new-instance v1, Lcom/google/android/gms/plus/activity/m;

    invoke-direct {v1, p0, p2}, Lcom/google/android/gms/plus/activity/m;-><init>(Lcom/google/android/gms/plus/activity/l;Lcom/google/android/gms/common/util/w;)V

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/plus/internal/i;->d(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 78
    const-string v1, "AccountSignUpClient"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 117
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/plus/internal/i;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 121
    :goto_0
    return-void

    .line 118
    :catch_0
    move-exception v0

    .line 119
    const-string v1, "AccountSignUpClient"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/plus/activity/q;)V
    .locals 6

    .prologue
    .line 89
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/activity/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    new-instance v1, Lcom/google/android/gms/plus/activity/o;

    invoke-direct {v1, p0, p5}, Lcom/google/android/gms/plus/activity/o;-><init>(Lcom/google/android/gms/plus/activity/l;Lcom/google/android/gms/plus/activity/q;)V

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 113
    :goto_0
    return-void

    .line 110
    :catch_0
    move-exception v0

    .line 111
    const-string v1, "AccountSignUpClient"

    invoke-virtual {v0}, Landroid/os/RemoteException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    const-string v0, "com.google.android.gms.plus.service.internal.START"

    return-object v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 130
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    return-object v0
.end method
