.class public final Lcom/google/android/gms/gcm/nts/v;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/google/android/gms/common/a/d;

.field static final b:Lcom/google/android/gms/common/a/d;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Landroid/app/AlarmManager;

.field private final e:Z

.field private final f:Landroid/app/PendingIntent;

.field private g:J

.field private h:J

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    const-string v0, "nts.min_wakeup_delay"

    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/gcm/nts/v;->a:Lcom/google/android/gms/common/a/d;

    .line 30
    const-string v0, "nts.use_jobscheduler"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/gcm/nts/v;->b:Lcom/google/android/gms/common/a/d;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 86
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.gcm.ACTION_CHECK_QUEUE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v1, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/gcm/nts/v;->f:Landroid/app/PendingIntent;

    .line 91
    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/google/android/gms/gcm/nts/v;->d:Landroid/app/AlarmManager;

    .line 92
    iput-object p1, p0, Lcom/google/android/gms/gcm/nts/v;->c:Landroid/content/Context;

    .line 93
    sget-object v0, Lcom/google/android/gms/gcm/nts/v;->b:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->a(Lcom/google/android/gms/common/a/d;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/gcm/nts/v;->e:Z

    .line 95
    return-void

    :cond_0
    move v0, v1

    .line 93
    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 190
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a(J)V
    .locals 11

    .prologue
    const-wide/16 v8, 0x3e8

    const/4 v7, 0x2

    .line 140
    iget-wide v0, p0, Lcom/google/android/gms/gcm/nts/v;->h:J

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 141
    iget-wide v0, p0, Lcom/google/android/gms/gcm/nts/v;->h:J

    sget-object v2, Lcom/google/android/gms/gcm/nts/v;->a:Lcom/google/android/gms/common/a/d;

    invoke-static {v2}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v2

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    add-long/2addr v2, p1

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/gcm/nts/v;->g:J

    invoke-static {v2, v3, p1, p2}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    iget-boolean v4, p0, Lcom/google/android/gms/gcm/nts/v;->e:Z

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/gcm/nts/v;->d:Landroid/app/AlarmManager;

    iget-object v5, p0, Lcom/google/android/gms/gcm/nts/v;->f:Landroid/app/PendingIntent;

    invoke-virtual {v4, v7, v0, v1, v5}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    :goto_0
    const-string v4, "NetworkScheduler.Wakeup"

    invoke-static {v4, v7}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "NetworkScheduler.Wakeup"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Wake-up scheduled for window ["

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long/2addr v2, p1

    div-long/2addr v2, v8

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sub-long/2addr v0, p1

    div-long/2addr v0, v8

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "] secs"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 145
    :cond_0
    :goto_1
    return-void

    .line 141
    :cond_1
    const-string v4, "com.google.android.gms.gcm.ACTION_SCHEDULE_WAKEUP"

    invoke-static {v4}, Lcom/google/android/gms/gcm/nts/v;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "_constraints"

    iget v6, p0, Lcom/google/android/gms/gcm/nts/v;->i:I

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "_delay"

    invoke-virtual {v4, v5, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "_expiry"

    invoke-virtual {v4, v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/gcm/nts/v;->c:Landroid/content/Context;

    invoke-virtual {v5, v4}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_0

    .line 143
    :cond_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/gcm/nts/v;->d:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/v;->f:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_1

    :cond_3
    const-string v0, "com.google.android.gms.gcm.ACTION_CANCEL_WAKEUP"

    invoke-static {v0}, Lcom/google/android/gms/gcm/nts/v;->a(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/gcm/nts/v;->c:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto :goto_1
.end method

.method final a(JLcom/google/android/gms/gcm/nts/k;)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x0

    .line 118
    iget-wide v0, p3, Lcom/google/android/gms/gcm/nts/k;->e:J

    cmp-long v0, v0, p1

    if-lez v0, :cond_1

    iget-wide v0, p3, Lcom/google/android/gms/gcm/nts/k;->e:J

    iget-wide v2, p0, Lcom/google/android/gms/gcm/nts/v;->h:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    .line 120
    invoke-virtual {p3}, Lcom/google/android/gms/gcm/nts/k;->c()I

    move-result v0

    .line 121
    iget v1, p0, Lcom/google/android/gms/gcm/nts/v;->i:I

    invoke-static {v0, v1}, Lcom/google/android/gms/gcm/nts/l;->a(II)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 122
    iput v0, p0, Lcom/google/android/gms/gcm/nts/v;->i:I

    .line 124
    :cond_0
    iget-wide v0, p3, Lcom/google/android/gms/gcm/nts/k;->e:J

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/gcm/nts/v;->h:J

    .line 126
    :cond_1
    iget-wide v0, p3, Lcom/google/android/gms/gcm/nts/k;->f:J

    iget-wide v2, p0, Lcom/google/android/gms/gcm/nts/v;->g:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_3

    .line 127
    invoke-virtual {p3}, Lcom/google/android/gms/gcm/nts/k;->c()I

    move-result v0

    .line 128
    iget v1, p0, Lcom/google/android/gms/gcm/nts/v;->i:I

    invoke-static {v0, v1}, Lcom/google/android/gms/gcm/nts/l;->a(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 129
    iput v0, p0, Lcom/google/android/gms/gcm/nts/v;->i:I

    .line 131
    :cond_2
    iget-wide v0, p3, Lcom/google/android/gms/gcm/nts/k;->f:J

    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/gcm/nts/v;->g:J

    .line 133
    :cond_3
    return-void
.end method

.method final a()Z
    .locals 1

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/google/android/gms/gcm/nts/v;->e:Z

    return v0
.end method

.method final b()V
    .locals 2

    .prologue
    const-wide v0, 0x7fffffffffffffffL

    .line 102
    iput-wide v0, p0, Lcom/google/android/gms/gcm/nts/v;->h:J

    .line 103
    iput-wide v0, p0, Lcom/google/android/gms/gcm/nts/v;->g:J

    .line 104
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/gcm/nts/v;->i:I

    .line 105
    return-void
.end method

.method final c()J
    .locals 2

    .prologue
    .line 195
    iget-wide v0, p0, Lcom/google/android/gms/gcm/nts/v;->g:J

    return-wide v0
.end method

.method final d()J
    .locals 2

    .prologue
    .line 199
    iget-wide v0, p0, Lcom/google/android/gms/gcm/nts/v;->h:J

    return-wide v0
.end method
