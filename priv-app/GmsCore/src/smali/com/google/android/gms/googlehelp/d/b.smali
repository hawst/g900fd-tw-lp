.class public final Lcom/google/android/gms/googlehelp/d/b;
.super Lcom/google/android/gms/googlehelp/d/a;
.source "SourceFile"


# static fields
.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final e:Lcom/google/android/gms/googlehelp/d/c;

.field private final f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 53
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "child_ids"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/gms/googlehelp/d/b;->b:[Ljava/lang/String;

    .line 57
    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "id"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "snippet"

    aput-object v1, v0, v5

    const-string v1, "url"

    aput-object v1, v0, v6

    const/4 v1, 0x4

    const-string v2, "api_url"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "etag"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "visited_time"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/googlehelp/d/b;->c:[Ljava/lang/String;

    .line 67
    new-array v0, v6, [Ljava/lang/String;

    const-string v1, "snippet"

    aput-object v1, v0, v3

    const-string v1, "title"

    aput-object v1, v0, v4

    const-string v1, "url"

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/gms/googlehelp/d/b;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/d/a;-><init>()V

    .line 86
    new-instance v0, Lcom/google/android/gms/googlehelp/d/c;

    invoke-direct {v0, p1}, Lcom/google/android/gms/googlehelp/d/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/d/b;->e:Lcom/google/android/gms/googlehelp/d/c;

    .line 87
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/d/b;->f:Ljava/lang/String;

    .line 88
    return-void
.end method

.method private a([Ljava/lang/String;)Ljava/util/Map;
    .locals 19

    .prologue
    .line 161
    const/4 v10, 0x0

    .line 162
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/d/b;->b()V

    .line 164
    :try_start_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "app_package_name=\""

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/googlehelp/d/b;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\" AND id IN (\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\",\""

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 166
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "help_responses"

    sget-object v4, Lcom/google/android/gms/googlehelp/d/b;->c:[Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v10

    .line 174
    :try_start_1
    const-string v2, "id"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v11

    .line 175
    const-string v2, "title"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v12

    .line 176
    const-string v2, "snippet"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    .line 177
    const-string v2, "url"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    .line 178
    const-string v2, "api_url"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v15

    .line 179
    const-string v2, "etag"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v16

    .line 180
    const-string v2, "visited_time"

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v17

    .line 182
    new-instance v18, Ljava/util/HashMap;

    invoke-direct/range {v18 .. v18}, Ljava/util/HashMap;-><init>()V

    .line 183
    :cond_0
    :goto_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 184
    invoke-interface {v10, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 185
    invoke-interface {v10, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 186
    invoke-interface {v10, v14}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 187
    invoke-interface {v10, v15}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 188
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 190
    invoke-interface {v10, v13}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    move/from16 v0, v16

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    move/from16 v0, v17

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    int-to-long v8, v8

    invoke-static/range {v2 .. v9}, Lcom/google/android/gms/googlehelp/common/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v2

    .line 200
    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-interface {v0, v3, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 204
    :catchall_0
    move-exception v2

    move-object v3, v10

    :goto_1
    if-eqz v3, :cond_1

    .line 205
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 207
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    throw v2

    .line 204
    :cond_2
    if-eqz v10, :cond_3

    .line 205
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 207
    :cond_3
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    return-object v18

    .line 204
    :catchall_1
    move-exception v2

    move-object v3, v10

    goto :goto_1
.end method

.method private d(Lcom/google/android/gms/googlehelp/common/k;)Landroid/content/ContentValues;
    .locals 4

    .prologue
    .line 349
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 350
    const-string v1, "id"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    const-string v1, "app_package_name"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/d/b;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 353
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->e()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    .line 375
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The type of HelpResponse is not supported: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 355
    :pswitch_0
    const-string v1, "child_ids"

    const-string v2, ","

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->t()Ljava/util/List;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    :goto_0
    return-object v0

    .line 360
    :pswitch_1
    const-string v1, "title"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 361
    const-string v1, "snippet"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 362
    const-string v1, "url"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    const-string v1, "api_url"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->o()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    const-string v1, "etag"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    const-string v1, "visited_time"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->q()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    .line 369
    :pswitch_2
    const-string v1, "title"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->k()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 370
    const-string v1, "snippet"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 371
    const-string v1, "url"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 353
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected final a()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/b;->e:Lcom/google/android/gms/googlehelp/d/c;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/d/c;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 313
    .line 314
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->b()V

    .line 316
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "id=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" AND app_package_name=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/d/b;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 318
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "help_responses"

    sget-object v2, Lcom/google/android/gms/googlehelp/d/b;->d:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 321
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-nez v0, :cond_1

    .line 322
    if-eqz v1, :cond_0

    .line 342
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 344
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    move-object v0, v8

    :goto_0
    return-object v0

    .line 325
    :cond_1
    :try_start_2
    const-string v0, "snippet"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 326
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 327
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result v2

    if-eqz v2, :cond_3

    .line 328
    if-eqz v1, :cond_2

    .line 342
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 344
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    move-object v0, v8

    goto :goto_0

    .line 331
    :cond_3
    :try_start_3
    const-string v2, "title"

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 332
    const-string v3, "url"

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    .line 334
    const-string v4, "GOOGLEHELP_HelpResponseDatabase"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Read leaf answer from database for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-static {p1, v2, v3, v0, v4}, Lcom/google/android/gms/googlehelp/common/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 341
    if-eqz v1, :cond_4

    .line 342
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 344
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    goto :goto_0

    .line 341
    :catchall_0
    move-exception v0

    :goto_1
    if-eqz v8, :cond_5

    .line 342
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 344
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    throw v0

    .line 341
    :catchall_1
    move-exception v0

    move-object v8, v1

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/t;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 108
    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->b()V

    .line 111
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "id=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" AND app_package_name=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/d/b;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v1, "help_responses"

    sget-object v2, Lcom/google/android/gms/googlehelp/d/b;->b:[Ljava/lang/String;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 121
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_1

    .line 122
    invoke-static {p3, p1}, Lcom/google/android/gms/googlehelp/common/t;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/gms/googlehelp/common/t;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 153
    if-eqz v1, :cond_0

    .line 154
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 156
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    :goto_0
    return-object v0

    .line 125
    :cond_1
    :try_start_2
    const-string v0, "child_ids"

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 126
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 128
    invoke-static {p3, p1}, Lcom/google/android/gms/googlehelp/common/t;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/gms/googlehelp/common/t;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    .line 153
    if-eqz v1, :cond_2

    .line 154
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 156
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    goto :goto_0

    .line 131
    :cond_3
    :try_start_3
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 132
    invoke-static {p2}, Lcom/google/android/gms/googlehelp/common/k;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v4

    .line 133
    invoke-interface {v3, p2, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    .line 136
    invoke-direct {p0, v5}, Lcom/google/android/gms/googlehelp/d/b;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v6

    .line 137
    array-length v7, v5

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v7, :cond_5

    aget-object v0, v5, v2

    .line 138
    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;

    .line 139
    if-eqz v0, :cond_4

    .line 140
    invoke-virtual {v4, v0}, Lcom/google/android/gms/googlehelp/common/k;->c(Lcom/google/android/gms/googlehelp/common/k;)V

    .line 141
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v8

    invoke-interface {v3, v8, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 137
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 146
    :cond_5
    invoke-interface {v3}, Ljava/util/Map;->size()I

    move-result v0

    const/4 v2, 0x1

    if-gt v0, v2, :cond_7

    .line 147
    invoke-static {p3, p1}, Lcom/google/android/gms/googlehelp/common/t;->a(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/gms/googlehelp/common/t;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v0

    .line 153
    if-eqz v1, :cond_6

    .line 154
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 156
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    goto :goto_0

    .line 150
    :cond_7
    :try_start_4
    new-instance v0, Lcom/google/android/gms/googlehelp/common/t;

    const-string v2, ""

    invoke-direct {v0, p2, v3, p3, v2}, Lcom/google/android/gms/googlehelp/common/t;-><init>(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 153
    if-eqz v1, :cond_8

    .line 154
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 156
    :cond_8
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    goto :goto_0

    .line 153
    :catchall_0
    move-exception v0

    move-object v1, v8

    :goto_2
    if-eqz v1, :cond_9

    .line 154
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 156
    :cond_9
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    throw v0

    .line 153
    :catchall_1
    move-exception v0

    goto :goto_2
.end method

.method public final a(Ljava/util/List;Ljava/util/Map;)Ljava/util/Map;
    .locals 4

    .prologue
    .line 222
    if-eqz p2, :cond_3

    .line 224
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 225
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 226
    invoke-interface {p2, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 227
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 230
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 243
    :goto_1
    return-object p2

    :cond_2
    move-object p1, v1

    .line 237
    :cond_3
    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/d/b;->a([Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    .line 239
    if-eqz p2, :cond_4

    .line 240
    invoke-interface {v0, p2}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    :cond_4
    move-object p2, v0

    .line 243
    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/googlehelp/common/k;)V
    .locals 4

    .prologue
    .line 293
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->b()V

    .line 295
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 302
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    .line 303
    :goto_0
    return-void

    .line 298
    :cond_0
    :try_start_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/googlehelp/d/b;->d(Lcom/google/android/gms/googlehelp/common/k;)Landroid/content/ContentValues;

    move-result-object v0

    .line 299
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "help_responses"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 300
    const-string v0, "GOOGLEHELP_HelpResponseDatabase"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Wrote help response "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to database."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 302
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/googlehelp/common/t;)V
    .locals 6

    .prologue
    .line 253
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->b()V

    .line 255
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    .line 284
    :goto_0
    return-void

    .line 258
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 261
    :try_start_2
    iget-object v0, p1, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 262
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/k;

    .line 264
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/t;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->v()Z

    move-result v2

    if-nez v2, :cond_1

    .line 265
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->s()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 269
    const-string v2, "GOOGLEHELP_HelpResponseDatabase"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Skip writing answer link "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " to database."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 280
    :catchall_0
    move-exception v0

    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 283
    :catchall_1
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    throw v0

    .line 274
    :cond_3
    :try_start_4
    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/d/b;->d(Lcom/google/android/gms/googlehelp/common/k;)Landroid/content/ContentValues;

    move-result-object v2

    .line 275
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v4, "help_responses"

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5, v2}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 276
    const-string v2, "GOOGLEHELP_HelpResponseDatabase"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Wrote help response "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " to database."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 278
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 280
    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 283
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    goto/16 :goto_0
.end method

.method public final b(Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 433
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->b()V

    .line 435
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 436
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    const/4 v0, 0x0

    .line 442
    :goto_0
    return v0

    .line 438
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "id=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\" AND app_package_name=\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/d/b;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 440
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "help_responses"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 442
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/googlehelp/common/k;)V
    .locals 5

    .prologue
    .line 389
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->b()V

    .line 391
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 401
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    .line 402
    :goto_0
    return-void

    .line 394
    :cond_0
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 395
    const-string v1, "visited_time"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->q()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 396
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "id=\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" AND app_package_name=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/d/b;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 398
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "help_responses"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 399
    const-string v0, "GOOGLEHELP_HelpResponseDatabase"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updated visited_time for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 401
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/googlehelp/common/k;)V
    .locals 5

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->b()V

    .line 413
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    .line 424
    :goto_0
    return-void

    .line 416
    :cond_0
    :try_start_1
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 417
    const-string v1, "etag"

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "id=\""

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\" AND app_package_name=\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/d/b;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 420
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v3, "help_responses"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 421
    const-string v0, "GOOGLEHELP_HelpResponseDatabase"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Updated etag "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->p()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 423
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    throw v0
.end method

.method public final d()I
    .locals 4

    .prologue
    .line 452
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->b()V

    .line 454
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isReadOnly()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 455
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    const/4 v0, 0x0

    .line 460
    :goto_0
    return v0

    .line 457
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "app_package_name=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/d/b;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 458
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/d/b;->a:Landroid/database/sqlite/SQLiteDatabase;

    const-string v2, "help_responses"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v0, v3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 460
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/d/b;->c()V

    throw v0
.end method
