.class final Lcom/google/android/gms/people/service/a/a/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/os/ParcelFileDescriptor;

.field final synthetic b:Lcom/google/android/gms/people/f/m;

.field final synthetic c:I


# direct methods
.method constructor <init>(Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/people/f/m;I)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/android/gms/people/service/a/a/h;->a:Landroid/os/ParcelFileDescriptor;

    iput-object p2, p0, Lcom/google/android/gms/people/service/a/a/h;->b:Lcom/google/android/gms/people/f/m;

    iput p3, p0, Lcom/google/android/gms/people/service/a/a/h;->c:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 116
    .line 119
    :try_start_0
    new-instance v3, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a/h;->a:Landroid/os/ParcelFileDescriptor;

    invoke-direct {v3, v0}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 120
    :try_start_1
    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a/h;->b:Lcom/google/android/gms/people/f/m;

    invoke-virtual {v0}, Lcom/google/android/gms/people/f/m;->a()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 123
    :try_start_2
    new-instance v0, Ljava/io/BufferedInputStream;

    iget v2, p0, Lcom/google/android/gms/people/service/a/a/h;->c:I

    invoke-direct {v0, v3, v2}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;I)V

    const/4 v2, 0x1

    iget v4, p0, Lcom/google/android/gms/people/service/a/a/h;->c:I

    invoke-static {v0, v1, v2, v4}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;Ljava/io/OutputStream;ZI)J
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 128
    invoke-static {v3}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 129
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a/h;->b:Lcom/google/android/gms/people/f/m;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 131
    :goto_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    move-object v1, v2

    .line 126
    :goto_1
    :try_start_3
    const-string v3, "PeopleService"

    const-string v4, "Failed to write to pipe"

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 128
    invoke-static {v2}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 129
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a/h;->b:Lcom/google/android/gms/people/f/m;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    goto :goto_0

    .line 128
    :catchall_0
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    :goto_2
    invoke-static {v3}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 129
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 130
    iget-object v1, p0, Lcom/google/android/gms/people/service/a/a/h;->b:Lcom/google/android/gms/people/f/m;

    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0

    .line 128
    :catchall_1
    move-exception v0

    move-object v1, v2

    goto :goto_2

    :catchall_2
    move-exception v0

    goto :goto_2

    :catchall_3
    move-exception v0

    move-object v3, v2

    goto :goto_2

    .line 125
    :catch_1
    move-exception v0

    move-object v1, v2

    move-object v2, v3

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v2, v3

    goto :goto_1
.end method
