.class public final Lcom/google/android/gms/drive/metadata/sync/b/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/drive/metadata/sync/b/c;


# instance fields
.field private final b:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/b/c;

    const-string v1, ""

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/b/c;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/metadata/sync/b/c;->a:Lcom/google/android/gms/drive/metadata/sync/b/c;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 38
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/b/c;->b:Ljava/lang/String;

    .line 39
    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/metadata/sync/b/c;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 6

    .prologue
    .line 251
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/b/c;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "(not %s)"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/drive/metadata/sync/b/c;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/b/c;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/drive/metadata/sync/b/c;Lcom/google/android/gms/drive/metadata/sync/b/c;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 75
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 76
    :cond_0
    const/4 v0, 0x0

    .line 78
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/drive/metadata/sync/b/c;

    const/4 v1, 0x0

    aput-object p0, v0, v1

    const/4 v1, 0x1

    aput-object p1, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    const-string v1, "and"

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 3

    .prologue
    .line 110
    const-string v0, "parents"

    invoke-static {p0}, Lcom/google/android/gms/drive/g/ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "in"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 5

    .prologue
    .line 261
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/b/c;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s has { %s }"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/b/c;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 5

    .prologue
    .line 348
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/b/c;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s %s %s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p0, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    const/4 v4, 0x2

    aput-object p1, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/b/c;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 213
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 216
    const-string v0, "or"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 217
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/b/c;->a:Lcom/google/android/gms/drive/metadata/sync/b/c;

    .line 242
    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    .line 216
    goto :goto_0

    .line 222
    :cond_1
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/b/c;->a:Lcom/google/android/gms/drive/metadata/sync/b/c;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/b/c;->b:Ljava/lang/String;

    .line 223
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v3, v0

    move v4, v2

    move v5, v1

    :cond_2
    :goto_2
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/b/c;

    .line 224
    sget-object v7, Lcom/google/android/gms/drive/metadata/sync/b/c;->a:Lcom/google/android/gms/drive/metadata/sync/b/c;

    invoke-virtual {v7, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_4

    .line 225
    if-eqz v5, :cond_3

    .line 226
    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/b/c;->b:Ljava/lang/String;

    move-object v3, v0

    move v5, v2

    .line 227
    goto :goto_2

    .line 229
    :cond_3
    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v7, "%s %s %s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    aput-object v3, v8, v2

    aput-object p0, v8, v1

    const/4 v3, 0x2

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/b/c;->b:Ljava/lang/String;

    aput-object v0, v8, v3

    invoke-static {v4, v7, v8}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v0

    move v4, v1

    .line 231
    goto :goto_2

    .line 234
    :cond_4
    const-string v0, "or"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 235
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/b/c;->a:Lcom/google/android/gms/drive/metadata/sync/b/c;

    goto :goto_1

    .line 239
    :cond_5
    if-eqz v4, :cond_6

    .line 240
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 242
    :cond_6
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/b/c;

    invoke-direct {v0, v3}, Lcom/google/android/gms/drive/metadata/sync/b/c;-><init>(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static a(Ljava/util/Date;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 5

    .prologue
    .line 142
    if-nez p0, :cond_0

    .line 143
    const/4 v0, 0x0

    .line 154
    :goto_0
    return-object v0

    .line 144
    :cond_0
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    const-wide v2, 0x7fffffffffffffffL

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    .line 145
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/b/c;->a:Lcom/google/android/gms/drive/metadata/sync/b/c;

    goto :goto_0

    .line 147
    :cond_1
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    const-wide v2, 0x7fffffffff9222ffL

    cmp-long v0, v0, v2

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 149
    invoke-virtual {p0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    const-wide/32 v2, 0x6ddd00

    add-long/2addr v0, v2

    .line 150
    new-instance v2, Lcom/google/android/gms/drive/database/y;

    const-string v3, "yyyy-MM-dd\'T\'HH:mm:ss"

    sget-object v4, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/drive/database/y;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 152
    const-string v3, "UTC"

    invoke-static {v3}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/database/y;->a(Ljava/util/TimeZone;)V

    .line 153
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "\'"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Lcom/google/android/gms/drive/database/y;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 154
    const-string v1, "modifiedDate"

    const-string v2, "<"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    goto :goto_0

    .line 147
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/util/List;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 1

    .prologue
    .line 198
    const-string v0, "and"

    invoke-static {v0, p0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    return-object v0
.end method

.method public static b()Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 3

    .prologue
    .line 95
    const-string v0, "mimeType"

    const-string v1, "application/vnd.google-apps.folder"

    invoke-static {v1}, Lcom/google/android/gms/drive/g/ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "!="

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 1

    .prologue
    .line 271
    const-string v0, "in"

    invoke-static {p1, p0, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/List;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 1

    .prologue
    .line 207
    const-string v0, "or"

    invoke-static {v0, p0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    return-object v0
.end method

.method public static c()Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 3

    .prologue
    .line 103
    const-string v0, "application/vnd.google-apps.folder"

    const-string v1, "mimeType"

    invoke-static {v0}, Lcom/google/android/gms/drive/g/ax;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "="

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 1

    .prologue
    .line 281
    const-string v0, "contains"

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    return-object v0
.end method

.method public static d()Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 3

    .prologue
    .line 133
    const-string v0, "sharedWithMe"

    const-string v1, "true"

    const-string v2, "="

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    return-object v0
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 1

    .prologue
    .line 292
    const-string v0, "="

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    return-object v0
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 1

    .prologue
    .line 313
    const-string v0, "<"

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    return-object v0
.end method

.method public static f(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 1

    .prologue
    .line 323
    const-string v0, "<="

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    return-object v0
.end method

.method public static g(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 1

    .prologue
    .line 333
    const-string v0, ">"

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    return-object v0
.end method

.method public static h(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;
    .locals 1

    .prologue
    .line 343
    const-string v0, ">="

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/drive/metadata/sync/b/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/b/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 51
    if-ne p1, p0, :cond_0

    .line 52
    const/4 v0, 0x1

    .line 57
    :goto_0
    return v0

    .line 53
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/drive/metadata/sync/b/c;

    if-nez v0, :cond_1

    .line 54
    const/4 v0, 0x0

    goto :goto_0

    .line 56
    :cond_1
    check-cast p1, Lcom/google/android/gms/drive/metadata/sync/b/c;

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/c;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/drive/metadata/sync/b/c;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/c;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
