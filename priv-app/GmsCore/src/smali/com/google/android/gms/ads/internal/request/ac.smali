.class public final Lcom/google/android/gms/ads/internal/request/ac;
.super Lcom/google/android/gms/ads/internal/util/a;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/ads/internal/request/j;

.field private final b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

.field private final c:Lcom/google/android/gms/ads/internal/o/b;

.field private final d:Lcom/google/android/gms/ads/internal/request/ae;

.field private final f:Ljava/lang/Object;

.field private g:Ljava/util/concurrent/Future;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/b;Lcom/google/android/gms/ads/internal/a/r;Lcom/google/android/gms/ads/internal/o/b;Lcom/google/android/gms/ads/internal/request/j;)V
    .locals 6

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/gms/ads/internal/request/ae;

    new-instance v4, Lcom/google/android/gms/ads/internal/util/p;

    invoke-direct {v4}, Lcom/google/android/gms/ads/internal/util/p;-><init>()V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/request/ae;-><init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/b;Lcom/google/android/gms/ads/internal/a/r;Lcom/google/android/gms/ads/internal/util/p;Lcom/google/android/gms/ads/internal/o/b;)V

    invoke-direct {p0, p4, p5, v0}, Lcom/google/android/gms/ads/internal/request/ac;-><init>(Lcom/google/android/gms/ads/internal/o/b;Lcom/google/android/gms/ads/internal/request/j;Lcom/google/android/gms/ads/internal/request/ae;)V

    .line 76
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/ads/internal/o/b;Lcom/google/android/gms/ads/internal/request/j;Lcom/google/android/gms/ads/internal/request/ae;)V
    .locals 1

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/util/a;-><init>()V

    .line 60
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/ac;->f:Ljava/lang/Object;

    .line 82
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/ac;->c:Lcom/google/android/gms/ads/internal/o/b;

    .line 83
    iget-object v0, p1, Lcom/google/android/gms/ads/internal/o/b;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/ac;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    .line 84
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/ac;->a:Lcom/google/android/gms/ads/internal/request/j;

    .line 85
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/request/ac;->d:Lcom/google/android/gms/ads/internal/request/ae;

    .line 86
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 31

    .prologue
    .line 90
    const/4 v6, -0x2

    .line 91
    const/4 v3, 0x0

    .line 96
    :try_start_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/ads/internal/request/ac;->f:Ljava/lang/Object;

    monitor-enter v4
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_3

    .line 97
    :try_start_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/request/ac;->d:Lcom/google/android/gms/ads/internal/request/ae;

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/util/c;->a(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/ads/internal/request/ac;->g:Ljava/util/concurrent/Future;

    .line 98
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 100
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/request/ac;->g:Ljava/util/concurrent/Future;

    const-wide/32 v4, 0xea60

    sget-object v7, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v4, v5, v7}, Ljava/util/concurrent/Future;->get(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/ads/internal/o/a;
    :try_end_2
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_2 .. :try_end_2} :catch_3

    .line 113
    :goto_0
    if-eqz v2, :cond_0

    .line 114
    :goto_1
    sget-object v3, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/gms/ads/internal/request/ad;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2}, Lcom/google/android/gms/ads/internal/request/ad;-><init>(Lcom/google/android/gms/ads/internal/request/ac;Lcom/google/android/gms/ads/internal/o/a;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 120
    return-void

    .line 98
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v4

    throw v2
    :try_end_3
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/util/concurrent/CancellationException; {:try_start_3 .. :try_end_3} :catch_3

    .line 102
    :catch_0
    move-exception v2

    const-string v2, "Timed out waiting for native ad."

    invoke-static {v2}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 103
    const/4 v6, 0x2

    move-object v2, v3

    .line 110
    goto :goto_0

    .line 105
    :catch_1
    move-exception v2

    const/4 v6, 0x0

    move-object v2, v3

    .line 110
    goto :goto_0

    .line 107
    :catch_2
    move-exception v2

    const/4 v6, -0x1

    move-object v2, v3

    .line 110
    goto :goto_0

    .line 109
    :catch_3
    move-exception v2

    const/4 v6, -0x1

    move-object v2, v3

    goto :goto_0

    .line 113
    :cond_0
    new-instance v2, Lcom/google/android/gms/ads/internal/o/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/ads/internal/request/ac;->c:Lcom/google/android/gms/ads/internal/o/b;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/o/b;->a:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->c:Lcom/google/android/gms/ads/internal/client/AdRequestParcel;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/ads/internal/request/ac;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget v9, v9, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->l:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/ads/internal/request/ac;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    iget-wide v10, v10, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->k:J

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/gms/ads/internal/request/ac;->c:Lcom/google/android/gms/ads/internal/o/b;

    iget-object v12, v12, Lcom/google/android/gms/ads/internal/o/b;->a:Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;

    iget-object v12, v12, Lcom/google/android/gms/ads/internal/request/AdRequestInfoParcel;->i:Ljava/lang/String;

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/ac;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->i:J

    move-wide/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/ac;->c:Lcom/google/android/gms/ads/internal/o/b;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/b;->d:Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/ac;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->g:J

    move-wide/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/ac;->c:Lcom/google/android/gms/ads/internal/o/b;

    move-object/from16 v24, v0

    move-object/from16 v0, v24

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/o/b;->f:J

    move-wide/from16 v24, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/ac;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v26, v0

    move-object/from16 v0, v26

    iget-wide v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->n:J

    move-wide/from16 v26, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/ac;->b:Lcom/google/android/gms/ads/internal/request/AdResponseParcel;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/AdResponseParcel;->o:Ljava/lang/String;

    move-object/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/request/ac;->c:Lcom/google/android/gms/ads/internal/o/b;

    move-object/from16 v29, v0

    move-object/from16 v0, v29

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/o/b;->h:Lorg/json/JSONObject;

    move-object/from16 v29, v0

    const/16 v30, 0x0

    invoke-direct/range {v2 .. v30}, Lcom/google/android/gms/ads/internal/o/a;-><init>(Lcom/google/android/gms/ads/internal/client/AdRequestParcel;Lcom/google/android/gms/ads/internal/p/a;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLcom/google/android/gms/ads/internal/k/c;Lcom/google/android/gms/ads/internal/k/a/d;Ljava/lang/String;Lcom/google/android/gms/ads/internal/k/d;Lcom/google/android/gms/ads/internal/k/f;JLcom/google/android/gms/ads/internal/client/AdSizeParcel;JJJLjava/lang/String;Lorg/json/JSONObject;Lcom/google/android/gms/ads/internal/f/d;)V

    goto/16 :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 125
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/ac;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 127
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/ac;->g:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/ac;->g:Ljava/util/concurrent/Future;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 130
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
