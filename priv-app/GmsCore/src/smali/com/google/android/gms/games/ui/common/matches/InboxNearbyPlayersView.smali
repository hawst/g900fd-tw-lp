.class public final Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;
.super Landroid/support/v7/widget/CardView;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;)V

    .line 26
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 30
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/widget/CardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 34
    return-void
.end method


# virtual methods
.method public final a(ZI)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 46
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 49
    if-eqz p1, :cond_2

    .line 50
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/h;->Y:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/AnimationDrawable;

    .line 52
    const/16 v2, 0x10

    invoke-static {v2}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 53
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 57
    :goto_0
    invoke-virtual {v0}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 58
    if-ltz p2, :cond_1

    .line 59
    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/n;->g:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, p2, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 72
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 73
    return-void

    .line 55
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 63
    :cond_1
    sget v0, Lcom/google/android/gms/p;->iT:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 67
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/gms/h;->aa:I

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    .line 68
    sget v0, Lcom/google/android/gms/p;->iS:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method protected final onFinishInflate()V
    .locals 1

    .prologue
    .line 38
    invoke-super {p0}, Landroid/support/v7/widget/CardView;->onFinishInflate()V

    .line 40
    sget v0, Lcom/google/android/gms/j;->jV:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->a:Landroid/widget/ImageView;

    .line 41
    sget v0, Lcom/google/android/gms/j;->si:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/InboxNearbyPlayersView;->b:Landroid/widget/TextView;

    .line 42
    return-void
.end method
