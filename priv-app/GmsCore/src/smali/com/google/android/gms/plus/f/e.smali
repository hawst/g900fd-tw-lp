.class public Lcom/google/android/gms/plus/f/e;
.super Landroid/support/v4/app/m;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 24
    return-void
.end method

.method public static a(Ljava/lang/CharSequence;)Lcom/google/android/gms/plus/f/e;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/gms/plus/f/e;->a(Ljava/lang/CharSequence;Z)Lcom/google/android/gms/plus/f/e;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/CharSequence;Z)Lcom/google/android/gms/plus/f/e;
    .locals 3

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/gms/plus/f/e;

    invoke-direct {v0}, Lcom/google/android/gms/plus/f/e;-><init>()V

    .line 41
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 42
    const-string v2, "message"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    .line 43
    const-string v2, "cancelable"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 44
    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/f/e;->setArguments(Landroid/os/Bundle;)V

    .line 45
    return-object v0
.end method


# virtual methods
.method public a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 65
    new-instance v0, Landroid/app/ProgressDialog;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/f/e;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    sget v3, Lcom/google/android/gms/q;->C:I

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 67
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/gms/plus/f/e;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "message"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/plus/f/e;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "cancelable"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    .line 70
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setCanceledOnTouchOutside(Z)V

    .line 71
    return-object v0
.end method

.method protected final b(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 1

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->a_(Landroid/os/Bundle;)Landroid/app/Dialog;

    move-result-object v0

    return-object v0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCancel(Landroid/content/DialogInterface;)V

    .line 81
    invoke-virtual {p0}, Lcom/google/android/gms/plus/f/e;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 82
    instance-of v1, v0, Lcom/google/android/gms/plus/f/f;

    if-eqz v1, :cond_0

    .line 83
    check-cast v0, Lcom/google/android/gms/plus/f/f;

    invoke-interface {v0}, Lcom/google/android/gms/plus/f/f;->Y_()V

    .line 85
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCreate(Landroid/os/Bundle;)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/plus/f/e;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "cancelable"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/f/e;->a(Z)V

    .line 61
    return-void
.end method
