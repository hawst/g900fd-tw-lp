.class public Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private a:Landroid/webkit/WebView;

.field private b:Ljava/util/Stack;

.field private c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 178
    return-void
.end method

.method private b(Z)V
    .locals 6

    .prologue
    const/4 v3, 0x1

    const/4 v5, 0x0

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {v1}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setLoadsImagesAutomatically(Z)V

    invoke-virtual {v2, v5}, Landroid/webkit/WebSettings;->setBlockNetworkLoads(Z)V

    invoke-virtual {v2, v5}, Landroid/webkit/WebSettings;->setBlockNetworkImage(Z)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {v2, v3}, Landroid/webkit/WebSettings;->setLoadWithOverviewMode(Z)V

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xb

    if-ge v3, v4, :cond_2

    const/4 v3, -0x1

    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    invoke-virtual {v1, v5}, Landroid/webkit/WebView;->setScrollBarStyle(I)V

    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    new-instance v3, Lcom/google/android/gms/googlehelp/fragments/s;

    invoke-direct {v3, v0}, Lcom/google/android/gms/googlehelp/fragments/s;-><init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V

    invoke-virtual {v1, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    :cond_1
    new-instance v0, Lcom/google/android/gms/googlehelp/fragments/t;

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/googlehelp/fragments/t;-><init>(Landroid/webkit/WebSettings;Landroid/webkit/WebView;)V

    invoke-virtual {v1, v0}, Landroid/webkit/WebView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 200
    return-void

    .line 199
    :cond_2
    invoke-virtual {v2, v5}, Landroid/webkit/WebSettings;->setDisplayZoomControls(Z)V

    const-string v3, "goldfish"

    sget-object v4, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v1, v5}, Landroid/webkit/WebView;->setBackgroundColor(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/common/k;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 7

    .prologue
    .line 109
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->b()Ljava/lang/String;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->a(Lcom/google/android/gms/googlehelp/common/k;Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V

    .line 110
    return-void
.end method

.method public final a(Lcom/google/android/gms/googlehelp/common/k;Ljava/lang/String;ILjava/lang/String;ZLjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 121
    const-string v0, "ARTICLE_HELP_LINK_CLICKED"

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-nez p5, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->empty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->clear()V

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->a:Landroid/webkit/WebView;

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->l()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, p6, v1, v2, v3}, Lcom/google/android/gms/googlehelp/fragments/r;->a(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 132
    iput-boolean v3, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->d:Z

    .line 134
    if-nez p5, :cond_1

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->b:Ljava/util/Stack;

    new-instance v1, Lcom/google/android/gms/googlehelp/fragments/g;

    invoke-direct {v1}, Lcom/google/android/gms/googlehelp/fragments/g;-><init>()V

    iput-object p1, v1, Lcom/google/android/gms/googlehelp/fragments/g;->a:Lcom/google/android/gms/googlehelp/common/k;

    iput-object p2, v1, Lcom/google/android/gms/googlehelp/fragments/g;->b:Ljava/lang/String;

    iput p3, v1, Lcom/google/android/gms/googlehelp/fragments/g;->c:I

    invoke-virtual {v0, v1}, Ljava/util/Stack;->push(Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->n()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, p2, v1, p3, p4}, Lcom/google/android/gms/googlehelp/f/l;->a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 139
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V
    .locals 1

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->b(Z)V

    .line 79
    return-void
.end method

.method public final a(Z)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 144
    if-eqz p1, :cond_0

    .line 145
    invoke-virtual {v0, p0}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 149
    :goto_0
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 150
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    .line 154
    :goto_1
    return-void

    .line 147
    :cond_0
    invoke-virtual {v0, p0}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    goto :goto_0

    .line 152
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto :goto_1
.end method

.method public final a()Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-le v0, v5, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/g;

    .line 91
    :goto_0
    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/g;->b:Ljava/lang/String;

    const-string v1, "ARTICLE_HELP_LINK_CLICKED"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/g;

    .line 93
    iget-object v1, v0, Lcom/google/android/gms/googlehelp/fragments/g;->a:Lcom/google/android/gms/googlehelp/common/k;

    iget-object v2, v0, Lcom/google/android/gms/googlehelp/fragments/g;->b:Ljava/lang/String;

    iget v3, v0, Lcom/google/android/gms/googlehelp/fragments/g;->c:I

    const-string v4, ""

    move-object v0, p0

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->a(Lcom/google/android/gms/googlehelp/common/k;Ljava/lang/String;ILjava/lang/String;Z)V

    .line 98
    :goto_1
    return v5

    .line 88
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/g;

    goto :goto_0

    .line 97
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->b()V

    .line 98
    const/4 v5, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 157
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->d:Z

    if-nez v0, :cond_0

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->a:Landroid/webkit/WebView;

    const-string v1, "about:blank"

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 162
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->d:Z

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->b:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/fragments/g;

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/fragments/g;->a:Lcom/google/android/gms/googlehelp/common/k;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->a:Landroid/webkit/WebView;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/googlehelp/fragments/o;->a(Landroid/content/Context;Ljava/lang/String;Landroid/webkit/WebView;)V

    .line 175
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 59
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    if-eqz v0, :cond_0

    .line 65
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->b(Z)V

    .line 68
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->a(Z)V

    .line 69
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 50
    sget v0, Lcom/google/android/gms/l;->cg:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 51
    sget v0, Lcom/google/android/gms/j;->hy:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->a:Landroid/webkit/WebView;

    .line 52
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->b:Ljava/util/Stack;

    .line 53
    iput-boolean v2, p0, Lcom/google/android/gms/googlehelp/fragments/HelpAnswerFragment;->d:Z

    .line 54
    return-object v1
.end method
