.class public final Lcom/google/android/gms/feedback/ac;
.super Ljava/lang/Thread;
.source "SourceFile"


# static fields
.field private static final c:Ljava/util/Comparator;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/feedback/ErrorReport;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 153
    new-instance v0, Lcom/google/android/gms/feedback/ae;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/ae;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/ac;->c:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/feedback/ErrorReport;)V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 69
    iput-object p1, p0, Lcom/google/android/gms/feedback/ac;->a:Landroid/content/Context;

    .line 70
    iput-object p2, p0, Lcom/google/android/gms/feedback/ac;->b:Lcom/google/android/gms/feedback/ErrorReport;

    .line 71
    return-void
.end method

.method public static a(Lcom/google/android/gms/feedback/ErrorReport;Landroid/content/Context;)Li/i;
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v8, 0x0

    .line 184
    new-instance v2, Li/i;

    invoke-direct {v2}, Li/i;-><init>()V

    .line 185
    invoke-static {p0, p1}, Lcom/google/android/gms/feedback/ac;->b(Lcom/google/android/gms/feedback/ErrorReport;Landroid/content/Context;)Li/e;

    move-result-object v0

    iput-object v0, v2, Li/i;->a:Li/e;

    .line 186
    new-instance v3, Li/j;

    invoke-direct {v3}, Li/j;-><init>()V

    new-instance v0, Li/d;

    invoke-direct {v0}, Li/d;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->f:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/d;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->g:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/d;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->h:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/d;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->i:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/d;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->j:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/d;->e:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->l:I

    iput v1, v0, Li/d;->f:I

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->m:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/d;->g:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->n:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/d;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->o:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/d;->i:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->p:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/d;->j:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->q:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/d;->k:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->k:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/d;->l:Ljava/lang/String;

    iput-object v0, v3, Li/j;->c:Li/d;

    new-instance v0, Li/k;

    invoke-direct {v0}, Li/k;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    iput-object v1, v0, Li/k;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->processName:Ljava/lang/String;

    const-string v4, "unknown"

    invoke-static {v1, v4}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/k;->c:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->d:I

    iput v1, v0, Li/k;->d:I

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->e:Ljava/lang/String;

    const-string v4, "unknown"

    invoke-static {v1, v4}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/k;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-boolean v1, v1, Landroid/app/ApplicationErrorReport;->systemApp:Z

    iput-boolean v1, v0, Li/k;->f:Z

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->installerPackageName:Ljava/lang/String;

    const-string v4, "unknown"

    invoke-static {v1, v4}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/k;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->S:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/k;->g:Ljava/lang/String;

    iput-object v0, v3, Li/j;->b:Li/k;

    new-instance v4, Li/o;

    invoke-direct {v4}, Li/o;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->t:[Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string v0, "\n"

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->t:[Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Li/o;->c:Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->s:[Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v0, "\n"

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->s:[Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Li/o;->b:Ljava/lang/String;

    :cond_1
    iget-object v5, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-wide v0, v0, Landroid/app/ApplicationErrorReport;->time:J

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-nez v0, :cond_a

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    :goto_0
    iput-wide v0, v5, Landroid/app/ApplicationErrorReport;->time:J

    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-wide v0, v0, Landroid/app/ApplicationErrorReport;->time:J

    iput-wide v0, v4, Li/o;->a:J

    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    iput-object v0, v4, Li/o;->e:[Ljava/lang/String;

    :cond_2
    const v0, 0x6768a8

    iput v0, v4, Li/o;->g:I

    const-string v0, "6.7.77 (1747363-000)"

    iput-object v0, v4, Li/o;->h:Ljava/lang/String;

    new-instance v0, Li/p;

    invoke-direct {v0}, Li/p;-><init>()V

    iget v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->z:I

    iput v1, v0, Li/p;->a:I

    iget v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->A:I

    iput v1, v0, Li/p;->c:I

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->B:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/p;->b:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->G:I

    iput v1, v0, Li/p;->d:I

    iget v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->H:I

    iput v1, v0, Li/p;->e:I

    iput-object v0, v4, Li/o;->f:Li/p;

    iput-object v4, v3, Li/j;->a:Li/o;

    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    if-eqz v0, :cond_b

    new-instance v0, Li/f;

    invoke-direct {v0}, Li/f;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionClassName:Ljava/lang/String;

    iput-object v1, v0, Li/f;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwClassName:Ljava/lang/String;

    iput-object v1, v0, Li/f;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwMethodName:Ljava/lang/String;

    iput-object v1, v0, Li/f;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    iput-object v1, v0, Li/f;->g:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwLineNumber:I

    iput v1, v0, Li/f;->d:I

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwFileName:Ljava/lang/String;

    const-string v4, "unknown"

    invoke-static {v1, v4}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/f;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionMessage:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/f;->b:Ljava/lang/String;

    iput-object v0, v3, Li/j;->d:Li/f;

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget v0, v0, Landroid/app/ApplicationErrorReport;->type:I

    const/16 v1, 0xb

    if-eq v0, v1, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->Q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    :cond_4
    new-instance v0, Li/q;

    invoke-direct {v0}, Li/q;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->v:Ljava/lang/String;

    if-eqz v1, :cond_5

    new-instance v1, Li/h;

    invoke-direct {v1}, Li/h;-><init>()V

    const-string v4, "image/jpeg"

    iput-object v4, v1, Li/h;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/feedback/ErrorReport;->v:Ljava/lang/String;

    iput-object v4, v1, Li/h;->b:Ljava/lang/String;

    new-instance v4, Li/g;

    invoke-direct {v4}, Li/g;-><init>()V

    iget v5, p0, Lcom/google/android/gms/feedback/ErrorReport;->x:I

    int-to-float v5, v5

    iput v5, v4, Li/g;->b:F

    iget v5, p0, Lcom/google/android/gms/feedback/ErrorReport;->y:I

    int-to-float v5, v5

    iput v5, v4, Li/g;->a:F

    iput-object v4, v1, Li/h;->c:Li/g;

    iput-object v1, v0, Li/q;->d:Li/h;

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->Q:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->Q:Ljava/lang/String;

    iput-object v1, v0, Li/q;->f:Ljava/lang/String;

    :cond_6
    iget-object v1, v0, Li/q;->d:Li/h;

    if-nez v1, :cond_7

    iget-object v1, v0, Li/q;->f:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_8

    :cond_7
    iput-object v0, v3, Li/j;->h:Li/q;

    :cond_8
    iget-boolean v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->F:Z

    iput-boolean v0, v3, Li/j;->k:Z

    iget-boolean v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->I:Z

    iput-boolean v0, v3, Li/j;->l:Z

    iput-object v3, v2, Li/i;->b:Li/j;

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget v0, v0, Landroid/app/ApplicationErrorReport;->type:I

    iput v0, v2, Li/i;->c:I

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->U:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 190
    const-string v0, "GoogleHelp"

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 191
    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->U:Ljava/lang/String;

    const-string v3, ".bmp"

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/feedback/w;->a(Ljava/io/File;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :cond_9
    return-object v2

    .line 186
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-wide v0, v0, Landroid/app/ApplicationErrorReport;->time:J

    goto/16 :goto_0

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    if-eqz v0, :cond_e

    new-instance v0, Li/b;

    invoke-direct {v0}, Li/b;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$AnrInfo;->cause:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/b;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$AnrInfo;->info:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/b;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$AnrInfo;->activity:Ljava/lang/String;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$AnrInfo;->activity:Ljava/lang/String;

    iput-object v1, v0, Li/b;->a:Ljava/lang/String;

    :cond_c
    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->u:Ljava/lang/String;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->u:Ljava/lang/String;

    iput-object v1, v0, Li/b;->d:Ljava/lang/String;

    :cond_d
    iput-object v0, v3, Li/j;->e:Li/b;

    goto/16 :goto_1

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    if-eqz v0, :cond_f

    new-instance v0, Li/c;

    invoke-direct {v0}, Li/c;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget v1, v1, Landroid/app/ApplicationErrorReport$BatteryInfo;->usagePercent:I

    iput v1, v0, Li/c;->a:I

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-wide v4, v1, Landroid/app/ApplicationErrorReport$BatteryInfo;->durationMicros:J

    iput-wide v4, v0, Li/c;->b:J

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$BatteryInfo;->usageDetails:Ljava/lang/String;

    const-string v4, "unknown"

    invoke-static {v1, v4}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/c;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$BatteryInfo;->checkinDetails:Ljava/lang/String;

    const-string v4, "unknown"

    invoke-static {v1, v4}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/c;->d:Ljava/lang/String;

    iput-object v0, v3, Li/j;->f:Li/c;

    goto/16 :goto_1

    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    if-eqz v0, :cond_3

    new-instance v0, Li/n;

    invoke-direct {v0}, Li/n;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    iget-wide v4, v1, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->durationMillis:J

    iput-wide v4, v0, Li/n;->a:J

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->serviceDetails:Ljava/lang/String;

    const-string v4, "unknown"

    invoke-static {v1, v4}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Li/n;->b:Ljava/lang/String;

    iput-object v0, v3, Li/j;->g:Li/n;

    goto/16 :goto_1
.end method

.method public static a(Landroid/content/Context;Li/i;)Ljava/io/File;
    .locals 5

    .prologue
    .line 121
    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v1

    const-string v2, "reports"

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 122
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v1

    if-nez v1, :cond_0

    .line 123
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    move-result v1

    if-nez v1, :cond_0

    .line 124
    new-instance v0, Ljava/io/IOException;

    const-string v1, "failed to create reports directory"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/feedback/ac;->a(Ljava/io/File;)V

    .line 132
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Li/i;->hashCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 133
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".tmp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 134
    new-instance v3, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ".proto.gz"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 137
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 138
    new-instance v1, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v1, v0}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 139
    invoke-static {p1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/zip/GZIPOutputStream;->write([B)V

    .line 140
    invoke-virtual {v1}, Ljava/util/zip/GZIPOutputStream;->close()V

    .line 141
    invoke-virtual {v2, v3}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 142
    new-instance v0, Ljava/io/IOException;

    const-string v1, "failed to rename temporary file"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    throw v0

    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    return-object v3
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 438
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 439
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 444
    :goto_0
    return-object p1

    .line 442
    :cond_0
    const-string p1, ""

    goto :goto_0

    :cond_1
    move-object p1, p0

    .line 444
    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/feedback/ErrorReport;ILandroid/content/Context;)V
    .locals 3

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->F:Z

    if-eqz v0, :cond_0

    .line 107
    :goto_0
    return-void

    .line 101
    :cond_0
    new-instance v0, Lcom/google/android/gms/feedback/ad;

    invoke-direct {v0, p2, p1}, Lcom/google/android/gms/feedback/ad;-><init>(Landroid/content/Context;I)V

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method private static declared-synchronized a(Ljava/io/File;)V
    .locals 5

    .prologue
    .line 171
    const-class v1, Lcom/google/android/gms/feedback/ac;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v2

    .line 172
    array-length v0, v2

    add-int/lit8 v3, v0, -0x3

    .line 173
    if-lez v3, :cond_0

    .line 174
    sget-object v0, Lcom/google/android/gms/feedback/ac;->c:Ljava/util/Comparator;

    invoke-static {v2, v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 175
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    .line 176
    aget-object v4, v2, v0

    invoke-virtual {v4}, Ljava/io/File;->delete()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 179
    :cond_0
    monitor-exit v1

    return-void

    .line 171
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Lcom/google/android/gms/feedback/ErrorReport;Landroid/content/Context;)Li/e;
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 200
    new-instance v4, Li/e;

    invoke-direct {v4}, Li/e;-><init>()V

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->D:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Li/e;->b:Ljava/lang/String;

    .line 202
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    invoke-static {v0, v7}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Li/e;->a:Ljava/lang/String;

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    iput-object v0, v4, Li/e;->c:Ljava/lang/String;

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 207
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 210
    new-instance v5, Li/m;

    invoke-direct {v5}, Li/m;-><init>()V

    .line 212
    iput-object v0, v5, Li/m;->a:Ljava/lang/String;

    .line 213
    iget-object v6, p0, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    invoke-virtual {v6, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v7}, Lcom/google/android/gms/feedback/ac;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v5, Li/m;->b:Ljava/lang/String;

    .line 214
    invoke-interface {v1, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 216
    :cond_1
    new-array v0, v2, [Li/m;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Li/m;

    iput-object v0, v4, Li/e;->e:[Li/m;

    .line 219
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 220
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 222
    new-instance v6, Li/l;

    invoke-direct {v6}, Li/l;-><init>()V

    .line 224
    const-string v0, "text/plain"

    iput-object v0, v6, Li/l;->b:Ljava/lang/String;

    .line 225
    const-string v0, "GoogleHelp"

    iget-object v1, p0, Lcom/google/android/gms/feedback/ErrorReport;->Y:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x1

    move v1, v0

    .line 227
    :goto_1
    iget-object v7, p0, Lcom/google/android/gms/feedback/ErrorReport;->W:[Ljava/lang/String;

    array-length v8, v7

    move v3, v2

    :goto_2
    if-ge v3, v8, :cond_4

    aget-object v0, v7, v3

    .line 228
    iput-object v0, v6, Li/l;->a:Ljava/lang/String;

    .line 230
    :try_start_0
    invoke-static {p1, v0, v1}, Lcom/google/android/gms/feedback/w;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/os/AsyncTask;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/AsyncTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 232
    const-string v9, "\n"

    invoke-static {v9, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, v6, Li/l;->c:[B

    .line 233
    invoke-interface {v5, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 227
    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_3
    move v1, v2

    .line 225
    goto :goto_1

    .line 234
    :catch_0
    move-exception v0

    :goto_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_3

    .line 238
    :cond_4
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 239
    new-array v0, v2, [Li/l;

    invoke-interface {v5, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Li/l;

    iput-object v0, v4, Li/e;->d:[Li/l;

    .line 243
    :cond_5
    return-object v4

    .line 234
    :catch_1
    move-exception v0

    goto :goto_4
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 76
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/ac;->b:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/feedback/ac;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/gms/feedback/ac;->a(Lcom/google/android/gms/feedback/ErrorReport;Landroid/content/Context;)Li/i;

    move-result-object v0

    .line 78
    iget-object v1, p0, Lcom/google/android/gms/feedback/ac;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/feedback/ac;->a(Landroid/content/Context;Li/i;)Ljava/io/File;

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/feedback/ac;->b:Lcom/google/android/gms/feedback/ErrorReport;

    sget v1, Lcom/google/android/gms/p;->oc:I

    iget-object v2, p0, Lcom/google/android/gms/feedback/ac;->a:Landroid/content/Context;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/feedback/ac;->a(Lcom/google/android/gms/feedback/ErrorReport;ILandroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/ac;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/feedback/ac;->a:Landroid/content/Context;

    const-class v3, Lcom/google/android/gms/feedback/SendService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 87
    :goto_0
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    iget-object v1, p0, Lcom/google/android/gms/feedback/ac;->b:Lcom/google/android/gms/feedback/ErrorReport;

    sget v2, Lcom/google/android/gms/p;->mP:I

    iget-object v3, p0, Lcom/google/android/gms/feedback/ac;->a:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/feedback/ac;->a(Lcom/google/android/gms/feedback/ErrorReport;ILandroid/content/Context;)V

    .line 82
    const-string v1, "GFEEDBACK_SaveReportThread"

    const-string v2, "failed to write bug report"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 83
    :catch_1
    move-exception v0

    .line 85
    const-string v1, "GFEEDBACK_SaveReportThread"

    const-string v2, "invalid report"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
