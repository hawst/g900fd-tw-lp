.class final Lcom/google/android/gms/car/qk;
.super Lcom/google/android/gms/car/qj;
.source "SourceFile"


# instance fields
.field public final b:Landroid/os/Handler;

.field public final c:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 277
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/qj;-><init>(B)V

    .line 278
    iput-object p1, p0, Lcom/google/android/gms/car/qk;->b:Landroid/os/Handler;

    .line 279
    iput-object p2, p0, Lcom/google/android/gms/car/qk;->c:Ljava/lang/Runnable;

    .line 280
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 283
    const-string v0, "CAR.TIME"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    const-string v0, "CAR.TIME"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "timeout for handler:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/qk;->b:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " runnable:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/qk;->c:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/qk;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/qk;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 287
    return-void
.end method
