.class public final Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/plus/service/pos/l;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/pos/q;

.field private static final a:Ljava/util/HashMap;


# instance fields
.field private final b:Ljava/util/Set;

.field private final c:I

.field private d:J

.field private e:J

.field private f:Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 275
    new-instance v0, Lcom/google/android/gms/plus/service/pos/q;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/pos/q;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->CREATOR:Lcom/google/android/gms/plus/service/pos/q;

    .line 305
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 308
    sput-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->a:Ljava/util/HashMap;

    const-string v1, "adgroupId"

    const-string v2, "adgroupId"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->a:Ljava/util/HashMap;

    const-string v1, "creativeId"

    const-string v2, "creativeId"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->a:Ljava/util/HashMap;

    const-string v1, "globalCounts"

    const-string v2, "globalCounts"

    const/4 v3, 0x4

    const-class v4, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 313
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->a:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "title"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 314
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->a:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 315
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 364
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 365
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->c:I

    .line 366
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->b:Ljava/util/Set;

    .line 367
    return-void
.end method

.method constructor <init>(Ljava/util/Set;IJJLcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 378
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 379
    iput-object p1, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->b:Ljava/util/Set;

    .line 380
    iput p2, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->c:I

    .line 381
    iput-wide p3, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->d:J

    .line 382
    iput-wide p5, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->e:J

    .line 383
    iput-object p7, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->f:Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;

    .line 384
    iput-object p8, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->g:Ljava/lang/String;

    .line 385
    iput-object p9, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->h:Ljava/lang/String;

    .line 386
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 319
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;J)V
    .locals 5

    .prologue
    .line 1230
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 1231
    packed-switch v0, :pswitch_data_0

    .line 1239
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a long."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1233
    :pswitch_0
    iput-wide p3, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->d:J

    .line 1242
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1243
    return-void

    .line 1236
    :pswitch_1
    iput-wide p3, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->e:J

    goto :goto_0

    .line 1231
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 1266
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 1267
    packed-switch v0, :pswitch_data_0

    .line 1272
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1269
    :pswitch_0
    check-cast p3, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->f:Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;

    .line 1276
    iget-object v1, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1277
    return-void

    .line 1267
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1248
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 1249
    packed-switch v0, :pswitch_data_0

    .line 1257
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1251
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->g:Ljava/lang/String;

    .line 1260
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1261
    return-void

    .line 1254
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->h:Ljava/lang/String;

    goto :goto_0

    .line 1249
    nop

    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 1182
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->b:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1187
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1199
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1189
    :pswitch_0
    iget-wide v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->d:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    .line 1197
    :goto_0
    return-object v0

    .line 1191
    :pswitch_1
    iget-wide v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->e:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 1193
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->f:Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;

    goto :goto_0

    .line 1195
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->g:Ljava/lang/String;

    goto :goto_0

    .line 1197
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->h:Ljava/lang/String;

    goto :goto_0

    .line 1187
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1172
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 271
    return-object p0
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1177
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Lcom/google/android/gms/plus/service/pos/m;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->f:Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1161
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->CREATOR:Lcom/google/android/gms/plus/service/pos/q;

    const/4 v0, 0x0

    return v0
.end method

.method final e()Ljava/util/Set;
    .locals 1

    .prologue
    .line 410
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->b:Ljava/util/Set;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1305
    instance-of v0, p1, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;

    if-nez v0, :cond_0

    move v0, v1

    .line 1336
    :goto_0
    return v0

    .line 1310
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 1311
    goto :goto_0

    .line 1314
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;

    .line 1315
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 1316
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1317
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1319
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 1321
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1326
    goto :goto_0

    .line 1329
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 1331
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1336
    goto :goto_0
.end method

.method final f()I
    .locals 1

    .prologue
    .line 417
    iget v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->c:I

    return v0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 426
    iget-wide v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->d:J

    return-wide v0
.end method

.method public final h()J
    .locals 2

    .prologue
    .line 443
    iget-wide v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->e:J

    return-wide v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 1292
    const/4 v0, 0x0

    .line 1293
    sget-object v1, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 1294
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 1295
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 1296
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 1298
    goto :goto_0

    .line 1299
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method final k()Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->f:Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 482
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1166
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;->CREATOR:Lcom/google/android/gms/plus/service/pos/q;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/service/pos/q;->a(Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity;Landroid/os/Parcel;I)V

    .line 1167
    return-void
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 1287
    const/4 v0, 0x1

    return v0
.end method
