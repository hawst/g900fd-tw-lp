.class public final Lcom/google/android/gms/wallet/common/ui/ab;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Landroid/widget/Filterable;


# instance fields
.field final a:[C

.field b:Ljava/util/ArrayList;

.field c:Ljava/lang/CharSequence;

.field private final d:Landroid/content/Context;

.field private final e:I

.field private final f:I

.field private final g:Ljava/lang/String;

.field private final h:C

.field private final i:[C

.field private final j:Ljava/util/List;

.field private final k:Landroid/view/LayoutInflater;

.field private l:Lcom/google/android/gms/wallet/common/ui/ac;


# direct methods
.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;C[CLjava/lang/String;Ljava/util/List;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 87
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 88
    if-eqz p6, :cond_0

    array-length v0, p6

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "remainingFields are required"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 90
    if-eqz p8, :cond_1

    invoke-interface {p8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "addressSources are required"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 93
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/ab;->d:Landroid/content/Context;

    .line 94
    iput p2, p0, Lcom/google/android/gms/wallet/common/ui/ab;->e:I

    .line 95
    iput p3, p0, Lcom/google/android/gms/wallet/common/ui/ab;->f:I

    .line 96
    iput-object p4, p0, Lcom/google/android/gms/wallet/common/ui/ab;->g:Ljava/lang/String;

    .line 97
    iput-char p5, p0, Lcom/google/android/gms/wallet/common/ui/ab;->h:C

    .line 98
    invoke-static {p6}, Lcom/google/android/gms/wallet/common/ui/ab;->a([C)[C

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->a:[C

    .line 99
    if-eqz p7, :cond_2

    invoke-virtual {p7}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    :goto_2
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->i:[C

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p8}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->j:Ljava/util/List;

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->k:Landroid/view/LayoutInflater;

    .line 103
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->b:Ljava/util/ArrayList;

    .line 104
    return-void

    :cond_0
    move v0, v2

    .line 88
    goto :goto_0

    :cond_1
    move v1, v2

    .line 90
    goto :goto_1

    .line 99
    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method private a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 152
    if-nez p2, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->k:Landroid/view/LayoutInflater;

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/ab;->e:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 156
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ab;->a(I)Lcom/google/android/gms/wallet/common/a/c;

    move-result-object v1

    .line 157
    sget v0, Lcom/google/android/gms/j;->eg:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 158
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/a/c;->c()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    return-object p2
.end method

.method private a(I)Lcom/google/android/gms/wallet/common/a/c;
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/a/c;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/ab;)Ljava/util/List;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->j:Ljava/util/List;

    return-object v0
.end method

.method private static final a([C)[C
    .locals 2

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 61
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_0

    aget-char v1, p0, v0

    invoke-static {v1}, Lcom/google/android/gms/wallet/dynamite/common/a/a;->a(C)Z

    move-result v1

    if-nez v1, :cond_0

    .line 62
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 64
    :cond_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 65
    array-length v1, p0

    invoke-static {p0, v0, v1}, Ljava/util/Arrays;->copyOfRange([CII)[C

    move-result-object v0

    return-object v0

    .line 67
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "fields must contain at least one valid field"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/ab;)C
    .locals 1

    .prologue
    .line 36
    iget-char v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->h:C

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/common/ui/ab;)I
    .locals 1

    .prologue
    .line 36
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->f:I

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/common/ui/ab;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/common/ui/ab;)[C
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->i:[C

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->c:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 148
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/ab;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getFilter()Landroid/widget/Filter;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->l:Lcom/google/android/gms/wallet/common/ui/ac;

    if-nez v0, :cond_0

    .line 136
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ac;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ac;-><init>(Lcom/google/android/gms/wallet/common/ui/ab;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->l:Lcom/google/android/gms/wallet/common/ui/ac;

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ab;->l:Lcom/google/android/gms/wallet/common/ui/ac;

    return-object v0
.end method

.method public final synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ab;->a(I)Lcom/google/android/gms/wallet/common/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 130
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 143
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/ab;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
