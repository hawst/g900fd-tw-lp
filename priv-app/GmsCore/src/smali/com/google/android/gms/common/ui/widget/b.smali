.class public final Lcom/google/android/gms/common/ui/widget/b;
.super Lcom/google/android/gms/common/ui/widget/c;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/ui/widget/a;

.field private final c:[Lcom/google/android/gms/common/ui/widget/c;

.field private final d:I


# direct methods
.method public varargs constructor <init>(Lcom/google/android/gms/common/ui/widget/a;[Lcom/google/android/gms/common/ui/widget/c;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 576
    iput-object p1, p0, Lcom/google/android/gms/common/ui/widget/b;->a:Lcom/google/android/gms/common/ui/widget/a;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/widget/c;-><init>(Lcom/google/android/gms/common/ui/widget/a;)V

    .line 577
    iput-object p2, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    move v1, v0

    .line 580
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 581
    iget-object v2, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v2, v2, v0

    if-eqz v2, :cond_0

    .line 582
    iget-object v2, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/ui/widget/c;->a()I

    move-result v2

    add-int/2addr v1, v2

    .line 580
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 585
    :cond_1
    iput v1, p0, Lcom/google/android/gms/common/ui/widget/b;->d:I

    .line 586
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 590
    iget v0, p0, Lcom/google/android/gms/common/ui/widget/b;->d:I

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 2

    .prologue
    .line 626
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 627
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 628
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/ui/widget/c;->a()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 629
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v0, v1, v0

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/common/ui/widget/c;->a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0

    .line 632
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/ui/widget/c;->a()I

    move-result v1

    sub-int/2addr p1, v1

    .line 626
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 637
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bad item index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 595
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 596
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 597
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/ui/widget/c;->a()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 598
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v0, v1, v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/ui/widget/c;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0

    .line 600
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/ui/widget/c;->a()I

    move-result v1

    sub-int/2addr p1, v1

    .line 595
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 605
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bad item index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final a(Ljava/util/ArrayList;I)V
    .locals 2

    .prologue
    .line 642
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 643
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 644
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v1, v1, v0

    invoke-virtual {v1, p1, p2}, Lcom/google/android/gms/common/ui/widget/c;->a(Ljava/util/ArrayList;I)V

    .line 645
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/ui/widget/c;->a()I

    move-result v1

    add-int/2addr p2, v1

    .line 642
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 648
    :cond_1
    return-void
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 610
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 611
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 612
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/ui/widget/c;->a()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 613
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v0, v1, v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/ui/widget/c;->b(I)I

    move-result v0

    return v0

    .line 615
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/ui/widget/b;->c:[Lcom/google/android/gms/common/ui/widget/c;

    aget-object v1, v1, v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/ui/widget/c;->a()I

    move-result v1

    sub-int/2addr p1, v1

    .line 610
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 620
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bad item index"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
