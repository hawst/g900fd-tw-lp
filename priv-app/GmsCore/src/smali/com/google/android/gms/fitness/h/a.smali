.class public abstract Lcom/google/android/gms/fitness/h/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/fitness/h/a;
    .locals 1

    .prologue
    .line 49
    instance-of v0, p0, Lcom/google/android/gms/fitness/h/b;

    if-eqz v0, :cond_0

    .line 50
    check-cast p0, Lcom/google/android/gms/fitness/h/b;

    invoke-interface {p0}, Lcom/google/android/gms/fitness/h/b;->a()Lcom/google/android/gms/fitness/h/a;

    move-result-object v0

    .line 53
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/fitness/h/c;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/fitness/h/a;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(Lcom/google/android/gms/fitness/k/a;)Lcom/google/android/gms/fitness/a/o;
.end method

.method public abstract a(Lcom/google/android/gms/fitness/sensors/a;)Lcom/google/android/gms/fitness/k/a;
.end method

.method public abstract a()Lcom/google/android/gms/fitness/l/z;
.end method

.method public abstract a(Landroid/os/Handler;)Lcom/google/android/gms/fitness/sensors/a/a;
.end method

.method public abstract a(Ljava/util/List;Lcom/google/android/gms/fitness/sensors/a/a;)Lcom/google/android/gms/fitness/sensors/a;
.end method

.method public abstract a(Lcom/google/android/gms/fitness/sensors/a;Lcom/google/android/gms/fitness/l/a/a;)Lcom/google/android/gms/fitness/service/av;
.end method

.method public abstract b()Lcom/google/android/gms/fitness/sync/d;
.end method
