.class abstract Lcom/google/android/gms/fitness/l/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field protected static final a:Ljava/lang/String;

.field protected static final b:Ljava/lang/String;

.field protected static final c:[Ljava/lang/String;

.field protected static final d:Ljava/lang/String;

.field protected static final e:Ljava/lang/String;

.field protected static final f:Ljava/lang/String;


# instance fields
.field protected final g:Landroid/content/Context;

.field h:Landroid/database/sqlite/SQLiteDatabase;

.field i:I

.field private final j:Lcom/google/android/gms/fitness/l/y;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    sput-object v0, Lcom/google/android/gms/fitness/l/a;->a:Ljava/lang/String;

    .line 44
    sput-object v0, Lcom/google/android/gms/fitness/l/a;->b:Ljava/lang/String;

    .line 45
    sput-object v0, Lcom/google/android/gms/fitness/l/a;->c:[Ljava/lang/String;

    .line 46
    sput-object v0, Lcom/google/android/gms/fitness/l/a;->d:Ljava/lang/String;

    .line 47
    sput-object v0, Lcom/google/android/gms/fitness/l/a;->e:Ljava/lang/String;

    .line 48
    sput-object v0, Lcom/google/android/gms/fitness/l/a;->f:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/gms/fitness/l/y;)V
    .locals 1

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/fitness/l/a;->i:I

    .line 58
    iput-object p1, p0, Lcom/google/android/gms/fitness/l/a;->g:Landroid/content/Context;

    .line 59
    iput-object p2, p0, Lcom/google/android/gms/fitness/l/a;->j:Lcom/google/android/gms/fitness/l/y;

    .line 60
    return-void
.end method

.method private a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    .line 232
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object v7, p5

    move-object v8, p6

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 241
    :catch_0
    move-exception v0

    .line 242
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/a;->g()V

    .line 243
    const-string v1, "Cannot query %s(%s). Where clause: %s, args: %s, sort order: %s, limit clause: %s, group by: %s, having: %s"

    const/16 v2, 0x8

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object p3, v2, v3

    const/4 v3, 0x3

    invoke-static {p4}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    aput-object p5, v2, v3

    const/4 v3, 0x5

    aput-object p6, v2, v3

    const/4 v3, 0x6

    aput-object p7, v2, v3

    const/4 v3, 0x7

    aput-object p8, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 254
    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/l/a;->a(Ljava/lang/RuntimeException;Ljava/lang/String;)V

    .line 255
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method private static a(Landroid/database/sqlite/SQLiteException;)V
    .locals 1

    .prologue
    .line 408
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->aa:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 410
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 411
    new-instance v0, Lcom/google/android/gms/fitness/l/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/fitness/l/g;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 413
    :cond_0
    new-instance v0, Lcom/google/android/gms/fitness/l/ae;

    invoke-direct {v0, p0}, Lcom/google/android/gms/fitness/l/ae;-><init>(Ljava/lang/Throwable;)V

    throw v0
.end method

.method private static a(Ljava/lang/RuntimeException;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 420
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->ab:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 422
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 423
    new-instance v0, Lcom/google/android/gms/fitness/l/g;

    invoke-direct {v0, p1, p0}, Lcom/google/android/gms/fitness/l/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0

    .line 425
    :cond_0
    return-void
.end method

.method protected static a(Landroid/database/Cursor;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 281
    invoke-static {p0, p1}, Lcom/google/android/gms/fitness/l/a;->b(Landroid/database/Cursor;Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected static b(Landroid/database/Cursor;Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 288
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method protected static c(Landroid/database/Cursor;Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 295
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method protected static d(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 302
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 303
    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/a;->f()V

    .line 140
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 141
    :catch_0
    move-exception v0

    .line 142
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/a;->g()V

    .line 143
    const-string v1, "Cannot update %s with %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 144
    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/l/a;->a(Ljava/lang/RuntimeException;Ljava/lang/String;)V

    .line 145
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 5

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/a;->f()V

    .line 155
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 156
    :catch_0
    move-exception v0

    .line 157
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/a;->g()V

    .line 158
    const-string v1, "Cannot delete from %s. Where clause: %s, args: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p2, v2, v3

    const/4 v3, 0x2

    invoke-static {p3}, Ljava/util/Arrays;->toString([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 161
    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/l/a;->a(Ljava/lang/RuntimeException;Ljava/lang/String;)V

    .line 162
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected final a(Ljava/lang/String;Landroid/content/ContentValues;)J
    .locals 4

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/a;->f()V

    .line 124
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    return-wide v0

    .line 125
    :catch_0
    move-exception v0

    .line 126
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/a;->g()V

    .line 127
    const-string v1, "Cannot insert %s into %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 128
    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/l/a;->a(Ljava/lang/RuntimeException;Ljava/lang/String;)V

    .line 129
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method protected final a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 170
    sget-object v5, Lcom/google/android/gms/fitness/l/a;->a:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/fitness/l/a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 179
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/a;->f()V

    .line 180
    sget-object v5, Lcom/google/android/gms/fitness/l/a;->a:Ljava/lang/String;

    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/fitness/l/a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 7

    .prologue
    .line 188
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/a;->f()V

    .line 189
    const/4 v6, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/fitness/l/a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 9

    .prologue
    .line 207
    const/4 v0, -0x1

    if-ne p6, v0, :cond_0

    const/4 v6, 0x0

    .line 208
    :goto_0
    sget-object v7, Lcom/google/android/gms/fitness/l/a;->d:Ljava/lang/String;

    sget-object v8, Lcom/google/android/gms/fitness/l/a;->e:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/fitness/l/a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0

    .line 207
    :cond_0
    invoke-static {p6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    goto :goto_0
.end method

.method protected final varargs a(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/sqlite/SQLiteStatement;
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/l/a;->f()V

    .line 198
    sget-object v5, Lcom/google/android/gms/fitness/l/a;->a:Ljava/lang/String;

    sget-object v6, Lcom/google/android/gms/fitness/l/a;->f:Ljava/lang/String;

    sget-object v8, Lcom/google/android/gms/fitness/l/a;->e:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v7, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/fitness/l/a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;I)Landroid/database/Cursor;
    .locals 9

    .prologue
    const/4 v4, 0x1

    .line 217
    const-string v0, "%s,%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 218
    sget-object v7, Lcom/google/android/gms/fitness/l/a;->d:Ljava/lang/String;

    sget-object v8, Lcom/google/android/gms/fitness/l/a;->e:Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/fitness/l/a;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final varargs b(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;
    .locals 7

    .prologue
    const/16 v6, 0x2c

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 347
    array-length v0, p2

    if-lez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "There must be at least one column"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 348
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 349
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v2

    .line 350
    :goto_1
    array-length v5, p2

    if-ge v0, v5, :cond_2

    .line 351
    if-eqz v0, :cond_0

    .line 352
    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 353
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 355
    :cond_0
    aget-object v5, p2, v0

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 356
    const-string v5, "?"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 350
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    .line 347
    goto :goto_0

    .line 358
    :cond_2
    const-string v0, "INSERT INTO %s(%s) VALUES(%s)"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p1, v5, v2

    aput-object v3, v5, v1

    const/4 v1, 0x2

    aput-object v4, v5, v1

    invoke-static {v0, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 360
    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/fitness/l/a;->a(Ljava/lang/String;[Ljava/lang/Object;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v0

    return-object v0
.end method

.method protected final b()V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 83
    const/16 v0, 0xb

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionNonExclusive()V

    .line 92
    :goto_0
    return-void

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 88
    :catch_0
    move-exception v0

    .line 89
    const-string v1, "SQL bug detected in beginTransaction"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 90
    invoke-static {v0}, Lcom/google/android/gms/fitness/l/a;->a(Landroid/database/sqlite/SQLiteException;)V

    goto :goto_0
.end method

.method protected final declared-synchronized d()V
    .locals 1

    .prologue
    .line 107
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 111
    :cond_0
    iget v0, p0, Lcom/google/android/gms/fitness/l/a;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/fitness/l/a;->i:I

    .line 112
    iget v0, p0, Lcom/google/android/gms/fitness/l/a;->i:I

    if-nez v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->j:Lcom/google/android/gms/fitness/l/y;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/l/y;->close()V

    .line 114
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 116
    :cond_1
    monitor-exit p0

    return-void

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected e()Z
    .locals 2

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->g:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a;->j:Lcom/google/android/gms/fitness/l/y;

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/l/y;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->deleteDatabase(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method protected final f()V
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    const-string v1, "Not in an active transaction!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 311
    return-void
.end method

.method protected finalize()V
    .locals 2

    .prologue
    .line 323
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->isOpen()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "Database is open!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 325
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 326
    return-void

    .line 323
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 325
    :catchall_0
    move-exception v0

    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    throw v0
.end method

.method protected g()V
    .locals 0

    .prologue
    .line 317
    return-void
.end method

.method protected final declared-synchronized q_()V
    .locals 3

    .prologue
    .line 67
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->j:Lcom/google/android/gms/fitness/l/y;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/l/y;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 68
    const-string v1, "Could not open Fitness database"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    iput-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    .line 70
    iget v0, p0, Lcom/google/android/gms/fitness/l/a;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/fitness/l/a;->i:I
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    :goto_0
    monitor-exit p0

    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 72
    :try_start_1
    const-string v1, "SQL bug detected in openDatabase"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 73
    invoke-static {v0}, Lcom/google/android/gms/fitness/l/a;->a(Landroid/database/sqlite/SQLiteException;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 67
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final r_()V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a;->h:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 100
    return-void
.end method
