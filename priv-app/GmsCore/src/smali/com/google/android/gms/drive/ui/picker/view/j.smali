.class public Lcom/google/android/gms/drive/ui/picker/view/j;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewGroup$OnHierarchyChangeListener;
.implements Landroid/widget/AbsListView$OnScrollListener;


# instance fields
.field a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

.field b:Lcom/google/android/gms/drive/ui/picker/view/CustomListView;

.field private c:Lcom/google/android/gms/drive/ui/picker/a/a/m;

.field private final d:Lcom/google/android/gms/drive/ui/picker/a/a/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 38
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/j;->b:Lcom/google/android/gms/drive/ui/picker/a/a/j;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->d:Lcom/google/android/gms/drive/ui/picker/a/a/j;

    .line 43
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/j;->a()V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/j;->b:Lcom/google/android/gms/drive/ui/picker/a/a/j;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->d:Lcom/google/android/gms/drive/ui/picker/a/a/j;

    .line 48
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/j;->a()V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 38
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/j;->b:Lcom/google/android/gms/drive/ui/picker/a/a/j;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->d:Lcom/google/android/gms/drive/ui/picker/a/a/j;

    .line 53
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/j;->a()V

    .line 54
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/j;->setWillNotDraw(Z)V

    .line 60
    invoke-virtual {p0, p0}, Lcom/google/android/gms/drive/ui/picker/view/j;->setOnHierarchyChangeListener(Landroid/view/ViewGroup$OnHierarchyChangeListener;)V

    .line 61
    return-void
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->draw(Landroid/graphics/Canvas;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/ui/picker/a/a/l;->a(Landroid/graphics/Canvas;)V

    .line 108
    return-void
.end method

.method public e()V
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/a/a/l;->b()V

    .line 73
    return-void
.end method

.method public final i()V
    .locals 4

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/a/a/l;->e()V

    .line 80
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/j;->getDrawingRect(Landroid/graphics/Rect;)V

    iget v1, v0, Landroid/graphics/Rect;->right:I

    iget v2, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v1, v2

    iget v2, v0, Landroid/graphics/Rect;->bottom:I

    iget v0, v0, Landroid/graphics/Rect;->top:I

    sub-int v0, v2, v0

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/j;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v2, v1, v0, v3}, Lcom/google/android/gms/drive/ui/picker/a/a/l;->a(IILandroid/content/res/Resources;)V

    .line 81
    return-void
.end method

.method public onChildViewAdded(Landroid/view/View;Landroid/view/View;)V
    .locals 4

    .prologue
    .line 144
    instance-of v0, p2, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;

    if-eqz v0, :cond_0

    .line 147
    check-cast p2, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;

    iput-object p2, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->b:Lcom/google/android/gms/drive/ui/picker/view/CustomListView;

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->d:Lcom/google/android/gms/drive/ui/picker/a/a/j;

    .line 149
    new-instance v1, Lcom/google/android/gms/drive/ui/picker/a/a/l;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->b:Lcom/google/android/gms/drive/ui/picker/view/CustomListView;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->b:Lcom/google/android/gms/drive/ui/picker/view/CustomListView;

    invoke-direct {v1, v2, v3, p0, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/l;-><init>(Landroid/content/Context;Landroid/widget/AbsListView;Landroid/view/View;Lcom/google/android/gms/drive/ui/picker/a/a/j;)V

    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->b:Lcom/google/android/gms/drive/ui/picker/view/CustomListView;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;->a(Lcom/google/android/gms/drive/ui/picker/a/a/l;)V

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->c:Lcom/google/android/gms/drive/ui/picker/a/a/m;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/a/a/l;->a(Lcom/google/android/gms/drive/ui/picker/a/a/m;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->b:Lcom/google/android/gms/drive/ui/picker/view/CustomListView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    .line 154
    :cond_0
    return-void
.end method

.method public onChildViewRemoved(Landroid/view/View;Landroid/view/View;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->b:Lcom/google/android/gms/drive/ui/picker/view/CustomListView;

    if-ne p2, v0, :cond_0

    .line 161
    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->b:Lcom/google/android/gms/drive/ui/picker/view/CustomListView;

    .line 162
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/j;->e()V

    .line 163
    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    .line 165
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/ui/picker/a/a/l;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/drive/ui/picker/a/a/l;->a(Landroid/widget/AbsListView;III)V

    .line 140
    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    .prologue
    .line 131
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 2

    .prologue
    .line 123
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/j;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/drive/ui/picker/a/a/l;->a(IILandroid/content/res/Resources;)V

    .line 127
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/ui/picker/a/a/l;->b(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 179
    if-eqz v0, :cond_0

    .line 180
    const/4 v0, 0x1

    .line 183
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method
