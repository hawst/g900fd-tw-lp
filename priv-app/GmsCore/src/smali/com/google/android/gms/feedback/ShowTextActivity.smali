.class public Lcom/google/android/gms/feedback/ShowTextActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 94
    if-nez p1, :cond_0

    .line 95
    sget v0, Lcom/google/android/gms/j;->sH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 96
    sget v0, Lcom/google/android/gms/j;->fw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 97
    sget v0, Lcom/google/android/gms/j;->sz:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 105
    :goto_0
    return-void

    .line 99
    :cond_0
    sget v0, Lcom/google/android/gms/j;->sy:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 100
    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    sget v0, Lcom/google/android/gms/j;->fw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 102
    sget v0, Lcom/google/android/gms/j;->sH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 103
    sget v0, Lcom/google/android/gms/j;->sz:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method protected final a([Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v1, 0x8

    .line 144
    array-length v0, p1

    if-nez v0, :cond_0

    .line 145
    sget v0, Lcom/google/android/gms/j;->sH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 146
    sget v0, Lcom/google/android/gms/j;->fw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 147
    sget v0, Lcom/google/android/gms/j;->sz:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 158
    :goto_0
    return-void

    .line 149
    :cond_0
    sget v0, Lcom/google/android/gms/j;->sH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 150
    sget v0, Lcom/google/android/gms/j;->fw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 151
    sget v0, Lcom/google/android/gms/j;->sz:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    .line 152
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 153
    new-instance v1, Landroid/widget/ArrayAdapter;

    const v2, 0x1090003

    invoke-direct {v1, p0, v2, p1}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 155
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 156
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setDividerHeight(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 54
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 55
    sget v0, Lcom/google/android/gms/l;->fo:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->setContentView(I)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/ShowTextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "feedback.FIELD_NAME"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 59
    invoke-static {}, Lcom/google/android/gms/feedback/FeedbackActivity;->e()Lcom/google/android/gms/feedback/ErrorReport;

    move-result-object v1

    .line 60
    if-eqz v1, :cond_0

    if-nez v0, :cond_2

    .line 61
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/ShowTextActivity;->finish()V

    .line 88
    :cond_1
    :goto_0
    return-void

    .line 65
    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->setTitle(I)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/ShowTextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v2, "feedback.FIELD_VALUE"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 67
    const-string v2, "running applications"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 68
    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->r:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a([Ljava/lang/String;)V

    goto :goto_0

    .line 69
    :cond_3
    const-string v2, "system logs"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 70
    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->s:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a([Ljava/lang/String;)V

    goto :goto_0

    .line 71
    :cond_4
    const-string v2, "event logs"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 72
    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->t:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a([Ljava/lang/String;)V

    goto :goto_0

    .line 73
    :cond_5
    const-string v2, "stack trace"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 74
    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 75
    :cond_6
    const-string v2, "anr info"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 76
    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->anrInfo:Landroid/app/ApplicationErrorReport$AnrInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$AnrInfo;->info:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 77
    :cond_7
    const-string v2, "anr stack trace"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 78
    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->u:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 79
    :cond_8
    const-string v2, "battery usage details"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 80
    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$BatteryInfo;->usageDetails:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 81
    :cond_9
    const-string v2, "battery checkin details"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 82
    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->batteryInfo:Landroid/app/ApplicationErrorReport$BatteryInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$BatteryInfo;->checkinDetails:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 83
    :cond_a
    const-string v2, "running service details"

    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 84
    iget-object v0, v1, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->runningServiceInfo:Landroid/app/ApplicationErrorReport$RunningServiceInfo;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport$RunningServiceInfo;->serviceDetails:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/feedback/ShowTextActivity;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 85
    :cond_b
    const-string v1, "product specific binary data details"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/ShowTextActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "feedback.OBJECT_VALUE"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/feedback/am;

    invoke-direct {v1, p0, p0, v0}, Lcom/google/android/gms/feedback/am;-><init>(Lcom/google/android/gms/feedback/ShowTextActivity;Landroid/app/Activity;Ljava/lang/String;)V

    new-array v0, v3, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto/16 :goto_0
.end method
