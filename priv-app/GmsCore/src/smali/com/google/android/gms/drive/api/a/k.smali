.class public final Lcom/google/android/gms/drive/api/a/k;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/api/k;

.field private final f:Lcom/google/android/gms/drive/b/d;

.field private final g:Lcom/google/android/gms/drive/e/b;

.field private final h:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/api/k;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/e/b;Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x2

    invoke-direct {p0, p1, p6, v0}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 47
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/k;->e:Lcom/google/android/gms/drive/api/k;

    .line 48
    iput-object p3, p0, Lcom/google/android/gms/drive/api/a/k;->f:Lcom/google/android/gms/drive/b/d;

    .line 49
    iput-object p4, p0, Lcom/google/android/gms/drive/api/a/k;->g:Lcom/google/android/gms/drive/e/b;

    .line 50
    iput-object p5, p0, Lcom/google/android/gms/drive/api/a/k;->h:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    .line 51
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/k;->h:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    const-string v2, "Invalid close request: no request"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/k;->h:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/k;->h:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->b()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v3

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/k;->h:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->c()Lcom/google/android/gms/drive/Contents;

    move-result-object v6

    .line 61
    const-string v0, "Invalid close request: no DriveId"

    invoke-static {v2, v0}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 62
    const-string v0, "Invalid close request: no metadata"

    invoke-static {v3, v0}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 64
    const-string v0, "Invalid close request: no contents"

    invoke-static {v6, v0}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 66
    invoke-virtual {v6}, Lcom/google/android/gms/drive/Contents;->e()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v4

    :goto_0
    const-string v5, "Invalid close request: invalid request"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/service/i;->a(ZLjava/lang/String;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/k;->h:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->e()Ljava/lang/String;

    move-result-object v0

    .line 70
    iget-object v5, p0, Lcom/google/android/gms/drive/api/a/k;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v5, v2}, Lcom/google/android/gms/drive/api/d;->b(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v7

    .line 71
    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ah;->E()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/drive/api/a/k;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v5}, Lcom/google/android/gms/drive/api/d;->c()Lcom/google/android/gms/drive/DriveId;

    move-result-object v5

    if-ne v2, v5, :cond_2

    .line 72
    :cond_0
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v2, 0xa

    const-string v3, "The user cannot edit the resource."

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    :cond_1
    move v0, v1

    .line 66
    goto :goto_0

    .line 75
    :cond_2
    invoke-static {p2, v7, v3}, Lcom/google/android/gms/drive/metadata/e;->a(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Z

    .line 77
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/k;->h:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->f()I

    move-result v1

    .line 78
    invoke-static {v1}, Lcom/google/android/gms/drive/ad;->b(I)Z

    move-result v2

    const-string v5, "Invalid commitStrategy."

    invoke-static {v2, v5}, Lcom/google/android/gms/common/service/i;->a(ZLjava/lang/String;)V

    .line 82
    invoke-static {v1}, Lcom/google/android/gms/drive/ad;->a(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 83
    invoke-virtual {v6}, Lcom/google/android/gms/drive/Contents;->f()Z

    move-result v2

    const-string v5, "Invalid close request: contents not valid for conflict detection"

    invoke-static {v2, v5}, Lcom/google/android/gms/common/service/i;->a(ZLjava/lang/String;)V

    .line 85
    if-eqz v0, :cond_3

    .line 86
    invoke-static {v0}, Lcom/google/android/gms/drive/ad;->a(Ljava/lang/String;)Z

    move-result v2

    const-string v5, "Invalid tracking tag"

    invoke-static {v2, v5}, Lcom/google/android/gms/common/service/i;->a(ZLjava/lang/String;)V

    .line 91
    :cond_3
    new-instance v8, Lcom/google/android/gms/drive/e/a;

    invoke-direct {v8, v7, v3}, Lcom/google/android/gms/drive/e/a;-><init>(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    .line 92
    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/k;->h:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->d()Z

    move-result v2

    iget-object v5, p0, Lcom/google/android/gms/drive/api/a/k;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v5}, Lcom/google/android/gms/drive/api/d;->e()Z

    move-result v5

    iget-object v9, p0, Lcom/google/android/gms/drive/api/a/k;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v9}, Lcom/google/android/gms/drive/api/d;->i()Ljava/lang/String;

    move-result-object v9

    invoke-static {v1, v2, v5, v0, v9}, Lcom/google/android/gms/drive/a/a/l;->a(IZZLjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/drive/a/a/l;

    move-result-object v5

    .line 98
    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/k;->c:Lcom/google/android/gms/drive/c/a;

    invoke-interface {v2, v7}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/database/model/ah;)Lcom/google/android/gms/drive/c/a;

    move-result-object v2

    new-instance v7, Lcom/google/android/gms/drive/ad;

    iget-object v9, p0, Lcom/google/android/gms/drive/api/a/k;->h:Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;

    invoke-virtual {v9}, Lcom/google/android/gms/drive/internal/CloseContentsAndUpdateMetadataRequest;->d()Z

    move-result v9

    invoke-direct {v7, v0, v9, v1}, Lcom/google/android/gms/drive/ad;-><init>(Ljava/lang/String;ZI)V

    invoke-interface {v2, v7}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/ad;)Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)Lcom/google/android/gms/drive/c/a;

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/k;->e:Lcom/google/android/gms/drive/api/k;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/Contents;->e()I

    move-result v2

    move-object v1, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/drive/api/k;->a(Lcom/google/android/gms/drive/auth/g;ILcom/google/android/gms/drive/metadata/internal/MetadataBundle;ZLcom/google/android/gms/drive/a/a/l;)V

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/k;->g:Lcom/google/android/gms/drive/e/b;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/k;->f:Lcom/google/android/gms/drive/b/d;

    invoke-virtual {v8, v0, v1}, Lcom/google/android/gms/drive/e/a;->a(Lcom/google/android/gms/drive/e/b;Lcom/google/android/gms/drive/b/d;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/k;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v0}, Lcom/google/android/gms/drive/internal/ca;->a()V

    .line 107
    return-void
.end method
