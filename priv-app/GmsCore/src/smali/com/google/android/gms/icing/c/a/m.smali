.class public final Lcom/google/android/gms/icing/c/a/m;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/android/gms/icing/c/a/m;


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2202
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2203
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/m;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/c/a/m;->b:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/m;->c:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/m;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/m;->cachedSize:I

    .line 2204
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/c/a/m;
    .locals 2

    .prologue
    .line 2182
    sget-object v0, Lcom/google/android/gms/icing/c/a/m;->d:[Lcom/google/android/gms/icing/c/a/m;

    if-nez v0, :cond_1

    .line 2183
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2185
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/c/a/m;->d:[Lcom/google/android/gms/icing/c/a/m;

    if-nez v0, :cond_0

    .line 2186
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/c/a/m;

    sput-object v0, Lcom/google/android/gms/icing/c/a/m;->d:[Lcom/google/android/gms/icing/c/a/m;

    .line 2188
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2190
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/c/a/m;->d:[Lcom/google/android/gms/icing/c/a/m;

    return-object v0

    .line 2188
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2273
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2274
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/m;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2275
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/m;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2278
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/c/a/m;->b:I

    if-eqz v1, :cond_1

    .line 2279
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/c/a/m;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2282
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/m;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2283
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/m;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2286
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2217
    if-ne p1, p0, :cond_1

    .line 2218
    const/4 v0, 0x1

    .line 2241
    :cond_0
    :goto_0
    return v0

    .line 2220
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/m;

    if-eqz v1, :cond_0

    .line 2223
    check-cast p1, Lcom/google/android/gms/icing/c/a/m;

    .line 2224
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/m;->a:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 2225
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/m;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2231
    :cond_2
    iget v1, p0, Lcom/google/android/gms/icing/c/a/m;->b:I

    iget v2, p1, Lcom/google/android/gms/icing/c/a/m;->b:I

    if-ne v1, v2, :cond_0

    .line 2234
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/m;->c:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 2235
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/m;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2241
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/m;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2228
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/m;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/m;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2238
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/m;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/m;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2246
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/m;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2249
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/c/a/m;->b:I

    add-int/2addr v0, v2

    .line 2250
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/m;->c:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 2252
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/m;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2253
    return v0

    .line 2246
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/m;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2250
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/m;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2176
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/m;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/m;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/c/a/m;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/m;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2259
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/m;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2260
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/m;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2262
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/c/a/m;->b:I

    if-eqz v0, :cond_1

    .line 2263
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/c/a/m;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2265
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/m;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2266
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/m;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2268
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2269
    return-void
.end method
