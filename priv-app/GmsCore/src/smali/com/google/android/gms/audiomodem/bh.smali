.class public final Lcom/google/android/gms/audiomodem/bh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/audiomodem/y;

.field final b:Lcom/google/android/gms/audiomodem/ar;

.field c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    new-instance v0, Lcom/google/android/gms/audiomodem/y;

    invoke-direct {v0, p1}, Lcom/google/android/gms/audiomodem/y;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/bh;->a:Lcom/google/android/gms/audiomodem/y;

    .line 28
    new-instance v0, Lcom/google/android/gms/audiomodem/ar;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/ar;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/bh;->b:Lcom/google/android/gms/audiomodem/ar;

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/audiomodem/Encoding;)Lcom/google/android/gms/audiomodem/TokenDecoder;
    .locals 4

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bh;->b:Lcom/google/android/gms/audiomodem/ar;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/audiomodem/ar;->a(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bh;->b:Lcom/google/android/gms/audiomodem/ar;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/audiomodem/ar;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/audiomodem/TokenDecoder;

    .line 82
    :goto_0
    return-object v0

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bh;->b:Lcom/google/android/gms/audiomodem/ar;

    iget v1, v0, Lcom/google/android/gms/audiomodem/ar;->c:I

    .line 78
    new-instance v0, Lcom/google/android/gms/audiomodem/TokenDecoder;

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/bh;->a:Lcom/google/android/gms/audiomodem/y;

    iget-object v2, v2, Lcom/google/android/gms/audiomodem/y;->b:Lcom/google/android/gms/audiomodem/bb;

    iget v2, v2, Lcom/google/android/gms/audiomodem/bb;->d:I

    iget-object v3, p0, Lcom/google/android/gms/audiomodem/bh;->a:Lcom/google/android/gms/audiomodem/y;

    invoke-virtual {v3}, Lcom/google/android/gms/audiomodem/y;->b()I

    move-result v3

    invoke-direct {v0, p1, v1, v2, v3}, Lcom/google/android/gms/audiomodem/TokenDecoder;-><init>(Lcom/google/android/gms/audiomodem/Encoding;III)V

    .line 80
    iget-object v1, p0, Lcom/google/android/gms/audiomodem/bh;->b:Lcom/google/android/gms/audiomodem/ar;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/audiomodem/ar;->a(Ljava/lang/Object;)Z

    move-result v2

    iget-object v3, v1, Lcom/google/android/gms/audiomodem/ar;->b:Ljava/util/HashMap;

    invoke-virtual {v3, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-nez v2, :cond_1

    iget-object v2, v1, Lcom/google/android/gms/audiomodem/ar;->a:Ljava/util/HashMap;

    iget v3, v1, Lcom/google/android/gms/audiomodem/ar;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-virtual {v1}, Lcom/google/android/gms/audiomodem/ar;->a()I

    move-result v2

    iput v2, v1, Lcom/google/android/gms/audiomodem/ar;->c:I

    goto :goto_0
.end method

.method final a()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bh;->b:Lcom/google/android/gms/audiomodem/ar;

    iget-object v0, v0, Lcom/google/android/gms/audiomodem/ar;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bh;->a:Lcom/google/android/gms/audiomodem/y;

    iget-boolean v0, v0, Lcom/google/android/gms/audiomodem/y;->f:Z

    if-eqz v0, :cond_1

    .line 104
    :cond_0
    :goto_0
    return-void

    .line 98
    :cond_1
    const-string v0, "audioModem"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 99
    const-string v0, "TokenDecoderManager: Refreshing Gservices settings."

    const-string v1, "audioModem"

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/bh;->a:Lcom/google/android/gms/audiomodem/y;

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/y;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/audiomodem/bh;->c:Z

    goto :goto_0
.end method
