.class public final Lcom/google/android/gms/icing/c/a/ac;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/android/gms/icing/c/a/ac;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:D

.field public d:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2908
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2909
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/ac;->b:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/c/a/ac;->c:D

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/ac;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/ac;->cachedSize:I

    .line 2910
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/c/a/ac;
    .locals 2

    .prologue
    .line 2885
    sget-object v0, Lcom/google/android/gms/icing/c/a/ac;->e:[Lcom/google/android/gms/icing/c/a/ac;

    if-nez v0, :cond_1

    .line 2886
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2888
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/c/a/ac;->e:[Lcom/google/android/gms/icing/c/a/ac;

    if-nez v0, :cond_0

    .line 2889
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/c/a/ac;

    sput-object v0, Lcom/google/android/gms/icing/c/a/ac;->e:[Lcom/google/android/gms/icing/c/a/ac;

    .line 2891
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2893
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/c/a/ac;->e:[Lcom/google/android/gms/icing/c/a/ac;

    return-object v0

    .line 2891
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 3001
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3002
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 3003
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3006
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/ac;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 3007
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/ac;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3010
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/ac;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    .line 3012
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/android/gms/icing/c/a/ac;->c:D

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 3015
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v1

    move v3, v1

    .line 3018
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_4

    .line 3019
    iget-object v4, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 3020
    if-eqz v4, :cond_3

    .line 3021
    add-int/lit8 v3, v3, 0x1

    .line 3022
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 3018
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3026
    :cond_4
    add-int/2addr v0, v2

    .line 3027
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 3029
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 2924
    if-ne p1, p0, :cond_1

    .line 2925
    const/4 v0, 0x1

    .line 2955
    :cond_0
    :goto_0
    return v0

    .line 2927
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/ac;

    if-eqz v1, :cond_0

    .line 2930
    check-cast p1, Lcom/google/android/gms/icing/c/a/ac;

    .line 2931
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 2932
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2938
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/ac;->b:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 2939
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/ac;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2946
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/ac;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 2947
    iget-wide v4, p1, Lcom/google/android/gms/icing/c/a/ac;->c:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 2951
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2955
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/ac;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2935
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2942
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/ac;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/ac;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2960
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2963
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/ac;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 2966
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/ac;->c:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 2967
    mul-int/lit8 v0, v0, 0x1f

    const/16 v1, 0x20

    ushr-long v4, v2, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 2969
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2971
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/ac;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2972
    return v0

    .line 2960
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2963
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/ac;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2879
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/ac;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/ac;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/c/a/ac;->c:D

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x19 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 2978
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2979
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2981
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/ac;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2982
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/ac;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2984
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/icing/c/a/ac;->c:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 2986
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/ac;->c:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 2988
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 2989
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_4

    .line 2990
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/ac;->d:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 2991
    if-eqz v1, :cond_3

    .line 2992
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2989
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2996
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2997
    return-void
.end method
