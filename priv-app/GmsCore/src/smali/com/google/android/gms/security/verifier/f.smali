.class public final Lcom/google/android/gms/security/verifier/f;
.super Lcom/android/volley/p;
.source "SourceFile"


# instance fields
.field private final f:Lcom/android/volley/x;

.field private final g:I

.field private final h:[B

.field private final i:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/android/volley/w;Lcom/android/volley/x;I[BZ)V
    .locals 4

    .prologue
    .line 38
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/android/volley/p;-><init>(ILjava/lang/String;Lcom/android/volley/w;)V

    .line 39
    iput-object p3, p0, Lcom/google/android/gms/security/verifier/f;->f:Lcom/android/volley/x;

    .line 40
    iput p4, p0, Lcom/google/android/gms/security/verifier/f;->g:I

    .line 41
    iput-object p5, p0, Lcom/google/android/gms/security/verifier/f;->h:[B

    .line 42
    iput-boolean p6, p0, Lcom/google/android/gms/security/verifier/f;->i:Z

    .line 44
    new-instance v1, Lcom/android/volley/f;

    sget-object v0, Lcom/google/android/gms/security/verifier/x;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v0, v2, v3}, Lcom/android/volley/f;-><init>(IIF)V

    iput-object v1, p0, Lcom/android/volley/p;->d:Lcom/android/volley/z;

    .line 45
    return-void
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 72
    new-array v0, v2, [Ljava/lang/Object;

    iget v1, p1, Lcom/android/volley/m;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v4

    .line 74
    iget v0, p1, Lcom/android/volley/m;->a:I

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 75
    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "Received status %d from server"

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p1, Lcom/android/volley/m;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    .line 104
    :goto_0
    return-object v0

    .line 79
    :cond_0
    iget-object v0, p1, Lcom/android/volley/m;->c:Ljava/util/Map;

    const-string v1, "X-Goog-Upload-Status"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 80
    if-nez v0, :cond_1

    .line 81
    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "Response did not include the upload status header"

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0

    .line 86
    :cond_1
    const-string v1, "final"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 87
    iget-boolean v0, p0, Lcom/google/android/gms/security/verifier/f;->i:Z

    if-eqz v0, :cond_2

    .line 89
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0

    .line 93
    :cond_2
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0

    .line 98
    :cond_3
    const-string v1, "active"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 99
    new-array v1, v2, [Ljava/lang/Object;

    aput-object v0, v1, v4

    .line 100
    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "Server returned unknown status value"

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0

    .line 104
    :cond_4
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0, v3}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 18
    check-cast p1, Ljava/lang/Integer;

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/f;->f:Lcom/android/volley/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/f;->f:Lcom/android/volley/x;

    invoke-interface {v0, p1}, Lcom/android/volley/x;->a(Ljava/lang/Object;)V

    :cond_0
    return-void
.end method

.method public final i()Ljava/util/Map;
    .locals 3

    .prologue
    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 55
    iget-boolean v1, p0, Lcom/google/android/gms/security/verifier/f;->i:Z

    if-eqz v1, :cond_0

    .line 56
    const-string v1, "X-Goog-Upload-Command"

    const-string v2, "upload, finalize"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    :goto_0
    const-string v1, "X-Goog-Upload-Http-Method"

    const-string v2, "POST"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 61
    const-string v1, "X-Goog-Upload-Offset"

    iget v2, p0, Lcom/google/android/gms/security/verifier/f;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 62
    return-object v0

    .line 58
    :cond_0
    const-string v1, "X-Goog-Upload-Command"

    const-string v2, "upload"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    const-string v0, "application/octet-stream"

    return-object v0
.end method

.method public final m()[B
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/f;->h:[B

    return-object v0
.end method
