.class public final Lcom/google/android/gms/common/ui/j;
.super Landroid/support/v4/app/m;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private j:I

.field private k:I

.field private l:I

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    return-void
.end method

.method public static a(I)Lcom/google/android/gms/common/ui/j;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 48
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 49
    const-string v1, "title"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 50
    const-string v1, "message"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 51
    const-string v1, "finish_button"

    const v2, 0x104000a

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 52
    const-string v1, "retry_button"

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 53
    new-instance v1, Lcom/google/android/gms/common/ui/j;

    invoke-direct {v1}, Lcom/google/android/gms/common/ui/j;-><init>()V

    .line 54
    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/ui/j;->setArguments(Landroid/os/Bundle;)V

    .line 55
    return-object v1
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 97
    if-eqz p1, :cond_0

    .line 98
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/j;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_0

    .line 100
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 101
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 104
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/j;->dismiss()V

    .line 105
    return-void
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 70
    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/j;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    sget v3, Lcom/google/android/gms/q;->C:I

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 72
    iget v1, p0, Lcom/google/android/gms/common/ui/j;->j:I

    if-eqz v1, :cond_0

    .line 73
    iget v1, p0, Lcom/google/android/gms/common/ui/j;->j:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 75
    :cond_0
    iget v1, p0, Lcom/google/android/gms/common/ui/j;->k:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/common/ui/j;->l:I

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    .line 78
    iget v1, p0, Lcom/google/android/gms/common/ui/j;->m:I

    if-eqz v1, :cond_1

    .line 79
    iget v1, p0, Lcom/google/android/gms/common/ui/j;->m:I

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 81
    :cond_1
    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 82
    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 83
    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/ui/j;->b(Z)V

    .line 89
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 1

    .prologue
    .line 93
    const/4 v0, -0x2

    if-ne p2, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/common/ui/j;->b(Z)V

    .line 94
    return-void

    .line 93
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 60
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCreate(Landroid/os/Bundle;)V

    .line 61
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/j;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 62
    const-string v1, "title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/common/ui/j;->j:I

    .line 63
    const-string v1, "message"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/common/ui/j;->k:I

    .line 64
    const-string v1, "finish_button"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/common/ui/j;->l:I

    .line 65
    const-string v1, "retry_button"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/ui/j;->m:I

    .line 66
    return-void
.end method
