.class public Lcom/google/android/gms/appdatasearch/Feature;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/n;


# instance fields
.field final a:I

.field public final b:I

.field final c:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/appdatasearch/n;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/n;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/Feature;->CREATOR:Lcom/google/android/gms/appdatasearch/n;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 57
    const/4 v0, 0x1

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/gms/appdatasearch/Feature;-><init>(IILandroid/os/Bundle;)V

    .line 58
    return-void
.end method

.method constructor <init>(IILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput p1, p0, Lcom/google/android/gms/appdatasearch/Feature;->a:I

    .line 45
    iput p2, p0, Lcom/google/android/gms/appdatasearch/Feature;->b:I

    .line 46
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/Feature;->c:Landroid/os/Bundle;

    .line 47
    return-void
.end method

.method public static a(I[Lcom/google/android/gms/appdatasearch/Feature;)Lcom/google/android/gms/appdatasearch/Feature;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 70
    if-nez p1, :cond_0

    move-object v0, v1

    .line 76
    :goto_0
    return-object v0

    .line 71
    :cond_0
    const/4 v0, 0x0

    :goto_1
    array-length v2, p1

    if-ge v0, v2, :cond_2

    .line 72
    aget-object v2, p1, v0

    iget v2, v2, Lcom/google/android/gms/appdatasearch/Feature;->b:I

    if-ne v2, p0, :cond_1

    .line 73
    aget-object v0, p1, v0

    goto :goto_0

    .line 71
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 76
    goto :goto_0
.end method

.method public static a(Ljava/util/List;Lcom/google/android/gms/appdatasearch/Feature;)V
    .locals 3

    .prologue
    .line 84
    if-eqz p1, :cond_2

    .line 85
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 86
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/Feature;

    iget v0, v0, Lcom/google/android/gms/appdatasearch/Feature;->b:I

    iget v2, p1, Lcom/google/android/gms/appdatasearch/Feature;->b:I

    if-ne v0, v2, :cond_0

    .line 87
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Feature "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/google/android/gms/appdatasearch/Feature;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " already exists"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 85
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 90
    :cond_1
    invoke-interface {p0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 92
    :cond_2
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/Feature;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/Feature;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/Feature;->c:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/google/android/gms/appdatasearch/Feature;->CREATOR:Lcom/google/android/gms/appdatasearch/n;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 103
    sget-object v0, Lcom/google/android/gms/appdatasearch/Feature;->CREATOR:Lcom/google/android/gms/appdatasearch/n;

    invoke-static {p0, p1}, Lcom/google/android/gms/appdatasearch/n;->a(Lcom/google/android/gms/appdatasearch/Feature;Landroid/os/Parcel;)V

    .line 104
    return-void
.end method
