.class final Lcom/google/android/gms/blescanner/compat/k;
.super Lcom/google/android/gms/blescanner/compat/a;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/blescanner/b/b;

.field final b:Lcom/google/android/gms/blescanner/b/d;

.field final c:Ljava/util/HashMap;

.field final d:Ljava/util/HashMap;

.field final e:Ljava/util/HashMap;

.field final f:Ljava/util/List;

.field final g:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

.field private final h:Landroid/app/AlarmManager;

.field private final i:Landroid/app/PendingIntent;

.field private j:J

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:J

.field private final p:Landroid/bluetooth/BluetoothAdapter;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothManager;Landroid/app/AlarmManager;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 166
    new-instance v4, Lcom/google/android/gms/blescanner/b/f;

    invoke-direct {v4}, Lcom/google/android/gms/blescanner/b/f;-><init>()V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/blescanner/compat/ScanWakefulBroadcastReceiver;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p1, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/blescanner/compat/k;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothManager;Landroid/app/AlarmManager;Lcom/google/android/gms/blescanner/b/d;Landroid/app/PendingIntent;)V

    .line 169
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothManager;Landroid/app/AlarmManager;Lcom/google/android/gms/blescanner/b/d;Landroid/app/PendingIntent;)V
    .locals 2

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/a;-><init>()V

    .line 116
    const/16 v0, 0x34bc

    iput v0, p0, Lcom/google/android/gms/blescanner/compat/k;->k:I

    .line 117
    const/16 v0, 0x5dc

    iput v0, p0, Lcom/google/android/gms/blescanner/compat/k;->l:I

    .line 120
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/blescanner/compat/k;->m:I

    .line 125
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/blescanner/compat/k;->o:J

    .line 141
    new-instance v0, Lcom/google/android/gms/blescanner/compat/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/blescanner/compat/l;-><init>(Lcom/google/android/gms/blescanner/compat/k;)V

    iput-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->g:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    .line 178
    invoke-virtual {p2}, Landroid/bluetooth/BluetoothManager;->getAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->p:Landroid/bluetooth/BluetoothAdapter;

    .line 179
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->f:Ljava/util/List;

    .line 180
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->d:Ljava/util/HashMap;

    .line 181
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->c:Ljava/util/HashMap;

    .line 182
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->e:Ljava/util/HashMap;

    .line 183
    iput-object p3, p0, Lcom/google/android/gms/blescanner/compat/k;->h:Landroid/app/AlarmManager;

    .line 184
    iput-object p5, p0, Lcom/google/android/gms/blescanner/compat/k;->i:Landroid/app/PendingIntent;

    .line 185
    iput-object p4, p0, Lcom/google/android/gms/blescanner/compat/k;->b:Lcom/google/android/gms/blescanner/b/d;

    .line 186
    new-instance v0, Lcom/google/android/gms/blescanner/compat/m;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/blescanner/compat/m;-><init>(Lcom/google/android/gms/blescanner/compat/k;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->a:Lcom/google/android/gms/blescanner/b/b;

    .line 192
    return-void
.end method

.method private static a(I)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 548
    packed-switch p0, :pswitch_data_0

    .line 556
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown scan mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-array v2, v0, [Ljava/lang/Exception;

    invoke-static {v1, v2}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V

    .line 557
    :goto_0
    :pswitch_0
    return v0

    .line 550
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 552
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 548
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private declared-synchronized a(Ljava/lang/String;Lcom/google/android/gms/blescanner/compat/ScanResult;)V
    .locals 7

    .prologue
    .line 293
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/blescanner/compat/o;

    .line 294
    iget-object v1, v0, Lcom/google/android/gms/blescanner/compat/o;->a:Ljava/util/List;

    invoke-static {v1, p2}, Lcom/google/android/gms/blescanner/compat/k;->a(Ljava/util/List;Lcom/google/android/gms/blescanner/compat/ScanResult;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 295
    iget-object v1, v0, Lcom/google/android/gms/blescanner/compat/o;->b:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    .line 296
    iget-object v1, v0, Lcom/google/android/gms/blescanner/compat/o;->d:Lcom/google/android/gms/blescanner/compat/ScanSettings;

    invoke-virtual {v1}, Lcom/google/android/gms/blescanner/compat/ScanSettings;->b()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 297
    and-int/lit8 v4, v1, 0x2

    .line 298
    and-int/lit8 v1, v1, 0x1

    .line 301
    or-int/2addr v4, v1

    if-eqz v4, :cond_1

    .line 303
    if-nez v3, :cond_2

    .line 304
    :try_start_1
    iget-object v1, v0, Lcom/google/android/gms/blescanner/compat/o;->c:Lcom/google/android/gms/blescanner/compat/v;

    const/4 v4, 0x2

    invoke-virtual {v1, v4, p2}, Lcom/google/android/gms/blescanner/compat/v;->a(ILcom/google/android/gms/blescanner/compat/ScanResult;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 312
    :cond_1
    :goto_1
    if-nez v3, :cond_0

    .line 313
    :try_start_2
    iget-object v0, v0, Lcom/google/android/gms/blescanner/compat/o;->b:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 305
    :cond_2
    if-eqz v1, :cond_1

    .line 306
    :try_start_3
    iget-object v1, v0, Lcom/google/android/gms/blescanner/compat/o;->c:Lcom/google/android/gms/blescanner/compat/v;

    const/4 v4, 0x1

    invoke-virtual {v1, v4, p2}, Lcom/google/android/gms/blescanner/compat/v;->a(ILcom/google/android/gms/blescanner/compat/ScanResult;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 308
    :catch_0
    move-exception v1

    .line 309
    :try_start_4
    const-string v4, "Failure while handling scan result"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Exception;

    const/4 v6, 0x0

    aput-object v1, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V

    goto :goto_1

    .line 318
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 319
    monitor-exit p0

    return-void
.end method

.method private a(Z)V
    .locals 8

    .prologue
    .line 518
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/k;->f()J

    move-result-wide v2

    .line 519
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/blescanner/compat/n;

    .line 520
    if-nez p1, :cond_1

    iget-wide v6, v0, Lcom/google/android/gms/blescanner/compat/n;->c:J

    cmp-long v1, v6, v2

    if-gtz v1, :cond_0

    .line 521
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    iget-object v5, v0, Lcom/google/android/gms/blescanner/compat/n;->b:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v5

    invoke-direct {v1, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v5, v0, Lcom/google/android/gms/blescanner/compat/n;->b:Ljava/util/HashMap;

    invoke-virtual {v5}, Ljava/util/HashMap;->clear()V

    :try_start_0
    iget-object v5, v0, Lcom/google/android/gms/blescanner/compat/n;->a:Lcom/google/android/gms/blescanner/compat/o;

    iget-object v5, v5, Lcom/google/android/gms/blescanner/compat/o;->c:Lcom/google/android/gms/blescanner/compat/v;

    invoke-virtual {v5, v1}, Lcom/google/android/gms/blescanner/compat/v;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-wide v6, v0, Lcom/google/android/gms/blescanner/compat/n;->c:J

    cmp-long v1, v6, v2

    if-gtz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/blescanner/compat/n;->a:Lcom/google/android/gms/blescanner/compat/o;

    iget-object v1, v1, Lcom/google/android/gms/blescanner/compat/o;->d:Lcom/google/android/gms/blescanner/compat/ScanSettings;

    invoke-virtual {v1}, Lcom/google/android/gms/blescanner/compat/ScanSettings;->d()J

    move-result-wide v6

    add-long/2addr v6, v2

    iput-wide v6, v0, Lcom/google/android/gms/blescanner/compat/n;->c:J

    goto :goto_0

    :catch_0
    move-exception v1

    const-string v5, "Failure during forced deliver of batch results"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Exception;

    const/4 v7, 0x0

    aput-object v1, v6, v7

    invoke-static {v5, v6}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V

    goto :goto_1

    .line 524
    :cond_2
    return-void
.end method

.method private a(Lcom/google/android/gms/blescanner/compat/ScanSettings;Ljava/util/List;Lcom/google/android/gms/blescanner/compat/v;)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 333
    new-instance v2, Lcom/google/android/gms/blescanner/compat/o;

    invoke-direct {v2, p1, p2, p3}, Lcom/google/android/gms/blescanner/compat/o;-><init>(Lcom/google/android/gms/blescanner/compat/ScanSettings;Ljava/util/List;Lcom/google/android/gms/blescanner/compat/v;)V

    .line 334
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p3, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 336
    iget-object v0, v2, Lcom/google/android/gms/blescanner/compat/o;->d:Lcom/google/android/gms/blescanner/compat/ScanSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/ScanSettings;->b()I

    move-result v0

    .line 337
    and-int/lit8 v1, v0, 0x2

    .line 338
    and-int/lit8 v0, v0, 0x1

    .line 342
    or-int/2addr v0, v1

    if-eqz v0, :cond_1

    .line 343
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 344
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 345
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/blescanner/compat/ScanResult;

    .line 346
    invoke-static {p2, v0}, Lcom/google/android/gms/blescanner/compat/k;->a(Ljava/util/List;Lcom/google/android/gms/blescanner/compat/ScanResult;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 350
    :try_start_0
    iget-object v4, v2, Lcom/google/android/gms/blescanner/compat/o;->c:Lcom/google/android/gms/blescanner/compat/v;

    const/4 v5, 0x2

    invoke-virtual {v4, v5, v0}, Lcom/google/android/gms/blescanner/compat/v;->a(ILcom/google/android/gms/blescanner/compat/ScanResult;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 354
    :goto_1
    iget-object v0, v2, Lcom/google/android/gms/blescanner/compat/o;->b:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 351
    :catch_0
    move-exception v0

    .line 352
    const-string v4, "Failure while handling scan result for new listener"

    new-array v5, v7, [Ljava/lang/Exception;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V

    goto :goto_1

    .line 359
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/blescanner/compat/k;->c()V

    .line 360
    return v7
.end method

.method private static a(Ljava/util/List;Lcom/google/android/gms/blescanner/compat/ScanResult;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 635
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    .line 643
    :goto_0
    return v0

    .line 638
    :cond_1
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/blescanner/compat/ScanFilter;

    .line 639
    invoke-virtual {v0, p1}, Lcom/google/android/gms/blescanner/compat/ScanFilter;->a(Lcom/google/android/gms/blescanner/compat/ScanResult;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 640
    goto :goto_0

    .line 643
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;Lcom/google/android/gms/blescanner/compat/ScanResult;)V
    .locals 4

    .prologue
    .line 486
    const/4 v0, 0x0

    .line 488
    iget-object v1, p0, Lcom/google/android/gms/blescanner/compat/k;->e:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/blescanner/compat/n;

    .line 489
    iget-object v3, v0, Lcom/google/android/gms/blescanner/compat/n;->a:Lcom/google/android/gms/blescanner/compat/o;

    iget-object v3, v3, Lcom/google/android/gms/blescanner/compat/o;->a:Ljava/util/List;

    invoke-static {v3, p2}, Lcom/google/android/gms/blescanner/compat/k;->a(Ljava/util/List;Lcom/google/android/gms/blescanner/compat/ScanResult;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 490
    iget-object v3, v0, Lcom/google/android/gms/blescanner/compat/n;->b:Ljava/util/HashMap;

    invoke-virtual {v3, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/blescanner/compat/n;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 493
    goto :goto_0

    .line 496
    :cond_1
    const/16 v0, 0x3e8

    if-le v1, v0, :cond_2

    .line 497
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/blescanner/compat/k;->a(Z)V

    .line 499
    :cond_2
    return-void
.end method

.method private b(Lcom/google/android/gms/blescanner/compat/ScanSettings;Ljava/util/List;Lcom/google/android/gms/blescanner/compat/v;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 458
    new-instance v0, Lcom/google/android/gms/blescanner/compat/n;

    new-instance v1, Lcom/google/android/gms/blescanner/compat/o;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/gms/blescanner/compat/o;-><init>(Lcom/google/android/gms/blescanner/compat/ScanSettings;Ljava/util/List;Lcom/google/android/gms/blescanner/compat/v;)V

    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/k;->f()J

    move-result-wide v2

    invoke-virtual {p1}, Lcom/google/android/gms/blescanner/compat/ScanSettings;->d()J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/blescanner/compat/n;-><init>(Lcom/google/android/gms/blescanner/compat/o;J)V

    .line 461
    iget-object v1, p0, Lcom/google/android/gms/blescanner/compat/k;->e:Ljava/util/HashMap;

    invoke-virtual {v1, p3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 464
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 465
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/blescanner/compat/ScanResult;

    .line 466
    invoke-static {p2, v0}, Lcom/google/android/gms/blescanner/compat/k;->a(Ljava/util/List;Lcom/google/android/gms/blescanner/compat/ScanResult;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 467
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 473
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 475
    :try_start_0
    invoke-virtual {p3, v1}, Lcom/google/android/gms/blescanner/compat/v;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 481
    :cond_2
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/blescanner/compat/k;->c()V

    .line 482
    return v6

    .line 476
    :catch_0
    move-exception v0

    .line 477
    const-string v1, "Failure while sending first scan results for new batch listener"

    new-array v2, v6, [Ljava/lang/Exception;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method private d()V
    .locals 11

    .prologue
    const/4 v10, 0x0

    .line 406
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->c:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 407
    iget-wide v0, p0, Lcom/google/android/gms/blescanner/compat/k;->o:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->b:Lcom/google/android/gms/blescanner/b/d;

    invoke-interface {v0}, Lcom/google/android/gms/blescanner/b/d;->a()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/google/android/gms/blescanner/compat/k;->o:J

    sub-long/2addr v0, v4

    move-wide v4, v0

    .line 410
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 411
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 412
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 413
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/blescanner/compat/ScanResult;

    .line 414
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/ScanResult;->d()J

    move-result-wide v6

    invoke-virtual {v2, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v6

    cmp-long v2, v6, v4

    if-gez v2, :cond_0

    .line 415
    iget-object v2, p0, Lcom/google/android/gms/blescanner/compat/k;->d:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/blescanner/compat/o;

    iget-object v7, v2, Lcom/google/android/gms/blescanner/compat/o;->d:Lcom/google/android/gms/blescanner/compat/ScanSettings;

    invoke-virtual {v7}, Lcom/google/android/gms/blescanner/compat/ScanSettings;->b()I

    move-result v7

    and-int/lit8 v7, v7, 0x1

    iget-object v8, v2, Lcom/google/android/gms/blescanner/compat/o;->d:Lcom/google/android/gms/blescanner/compat/ScanSettings;

    invoke-virtual {v8}, Lcom/google/android/gms/blescanner/compat/ScanSettings;->b()I

    move-result v8

    and-int/lit8 v8, v8, 0x4

    iget-object v9, v2, Lcom/google/android/gms/blescanner/compat/o;->b:Ljava/util/Set;

    invoke-interface {v9, v1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_1

    or-int/2addr v7, v8

    if-eqz v7, :cond_1

    :try_start_0
    iget-object v2, v2, Lcom/google/android/gms/blescanner/compat/o;->c:Lcom/google/android/gms/blescanner/compat/v;

    const/4 v7, 0x4

    invoke-virtual {v2, v7, v0}, Lcom/google/android/gms/blescanner/compat/v;->a(ILcom/google/android/gms/blescanner/compat/ScanResult;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    const-string v7, "Failure while sending \'lost\' scan result to listener"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Exception;

    aput-object v2, v8, v10

    invoke-static {v7, v8}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V

    goto :goto_1

    .line 407
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->b:Lcom/google/android/gms/blescanner/b/d;

    invoke-interface {v0}, Lcom/google/android/gms/blescanner/b/d;->a()J

    move-result-wide v0

    const-wide/16 v4, 0x4

    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/k;->g()I

    move-result v2

    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/k;->h()I

    move-result v6

    add-int/2addr v2, v6

    int-to-long v6, v2

    mul-long/2addr v4, v6

    sub-long/2addr v0, v4

    move-wide v4, v0

    goto :goto_0

    .line 416
    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_0

    .line 421
    :cond_4
    invoke-direct {p0, v10}, Lcom/google/android/gms/blescanner/compat/k;->a(Z)V

    .line 422
    return-void
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 612
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()J
    .locals 4

    .prologue
    .line 651
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    iget-object v1, p0, Lcom/google/android/gms/blescanner/compat/k;->b:Lcom/google/android/gms/blescanner/b/d;

    invoke-interface {v1}, Lcom/google/android/gms/blescanner/b/d;->b()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private g()I
    .locals 2

    .prologue
    .line 683
    iget v0, p0, Lcom/google/android/gms/blescanner/compat/k;->m:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/blescanner/compat/k;->m:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/gms/blescanner/compat/k;->l:I

    goto :goto_0
.end method

.method private h()I
    .locals 2

    .prologue
    .line 692
    iget v0, p0, Lcom/google/android/gms/blescanner/compat/k;->m:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/blescanner/compat/k;->n:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/gms/blescanner/compat/k;->k:I

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(IIJ)V
    .locals 1

    .prologue
    .line 373
    monitor-enter p0

    :try_start_0
    iput p1, p0, Lcom/google/android/gms/blescanner/compat/k;->m:I

    .line 374
    iput p2, p0, Lcom/google/android/gms/blescanner/compat/k;->n:I

    .line 376
    invoke-virtual {p0}, Lcom/google/android/gms/blescanner/compat/k;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 377
    monitor-exit p0

    return-void

    .line 373
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/blescanner/compat/v;)V
    .locals 1

    .prologue
    .line 394
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 395
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 396
    invoke-virtual {p0}, Lcom/google/android/gms/blescanner/compat/k;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 397
    monitor-exit p0

    return-void

    .line 394
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/util/List;Lcom/google/android/gms/blescanner/compat/ScanSettings;Lcom/google/android/gms/blescanner/compat/v;)Z
    .locals 4

    .prologue
    .line 324
    monitor-enter p0

    :try_start_0
    invoke-virtual {p2}, Lcom/google/android/gms/blescanner/compat/ScanSettings;->d()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    .line 325
    invoke-direct {p0, p2, p1, p3}, Lcom/google/android/gms/blescanner/compat/k;->b(Lcom/google/android/gms/blescanner/compat/ScanSettings;Ljava/util/List;Lcom/google/android/gms/blescanner/compat/v;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 327
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    :try_start_1
    invoke-direct {p0, p2, p1, p3}, Lcom/google/android/gms/blescanner/compat/k;->a(Lcom/google/android/gms/blescanner/compat/ScanSettings;Ljava/util/List;Lcom/google/android/gms/blescanner/compat/v;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 324
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final declared-synchronized b()V
    .locals 12

    .prologue
    .line 207
    monitor-enter p0

    :try_start_0
    const-string v2, "Starting BLE Active Scan Cycle."

    invoke-static {v2}, Lcom/google/android/gms/blescanner/b/e;->b(Ljava/lang/String;)V

    .line 208
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/k;->g()I

    move-result v2

    .line 209
    iget-object v3, p0, Lcom/google/android/gms/blescanner/compat/k;->b:Lcom/google/android/gms/blescanner/b/d;

    invoke-interface {v3}, Lcom/google/android/gms/blescanner/b/d;->a()J

    move-result-wide v4

    .line 210
    int-to-long v2, v2

    add-long v10, v4, v2

    .line 212
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/k;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 213
    iget-object v2, p0, Lcom/google/android/gms/blescanner/compat/k;->p:Landroid/bluetooth/BluetoothAdapter;

    iget-object v3, p0, Lcom/google/android/gms/blescanner/compat/k;->g:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothAdapter;->startLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 217
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/blescanner/compat/k;->b:Lcom/google/android/gms/blescanner/b/d;

    invoke-interface {v2}, Lcom/google/android/gms/blescanner/b/d;->a()J

    move-result-wide v2

    cmp-long v2, v2, v10

    if-gez v2, :cond_3

    .line 218
    const-wide/16 v2, 0x32

    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V

    .line 219
    iget-object v3, p0, Lcom/google/android/gms/blescanner/compat/k;->f:Ljava/util/List;

    monitor-enter v3
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    new-instance v2, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/google/android/gms/blescanner/compat/k;->f:Ljava/util/List;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v4, p0, Lcom/google/android/gms/blescanner/compat/k;->f:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Lcom/google/android/gms/blescanner/compat/p;

    move-object v6, v0

    iget-object v2, v6, Lcom/google/android/gms/blescanner/compat/p;->a:Landroid/bluetooth/BluetoothDevice;

    if-nez v2, :cond_2

    const/4 v2, 0x0

    move-object v8, v2

    :goto_1
    new-instance v2, Lcom/google/android/gms/blescanner/compat/ScanResult;

    iget-object v3, v6, Lcom/google/android/gms/blescanner/compat/p;->a:Landroid/bluetooth/BluetoothDevice;

    iget-object v4, v6, Lcom/google/android/gms/blescanner/compat/p;->c:[B

    invoke-static {v4}, Lcom/google/android/gms/blescanner/compat/y;->a([B)Lcom/google/android/gms/blescanner/compat/y;

    move-result-object v4

    iget v5, v6, Lcom/google/android/gms/blescanner/compat/p;->b:I

    iget-wide v6, v6, Lcom/google/android/gms/blescanner/compat/p;->d:J

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/blescanner/compat/ScanResult;-><init>(Landroid/bluetooth/BluetoothDevice;Lcom/google/android/gms/blescanner/compat/y;IJ)V

    invoke-direct {p0, v8, v2}, Lcom/google/android/gms/blescanner/compat/k;->a(Ljava/lang/String;Lcom/google/android/gms/blescanner/compat/ScanResult;)V

    invoke-direct {p0, v8, v2}, Lcom/google/android/gms/blescanner/compat/k;->b(Ljava/lang/String;Lcom/google/android/gms/blescanner/compat/ScanResult;)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 221
    :catch_0
    move-exception v2

    .line 222
    :try_start_4
    const-string v3, "Exception in ScanCycle Sleep"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Exception;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 226
    :try_start_5
    iget-object v2, p0, Lcom/google/android/gms/blescanner/compat/k;->p:Landroid/bluetooth/BluetoothAdapter;

    iget-object v3, p0, Lcom/google/android/gms/blescanner/compat/k;->g:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V
    :try_end_5
    .catch Ljava/lang/NullPointerException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_2

    .line 233
    :goto_2
    :try_start_6
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/k;->d()V

    .line 236
    :cond_1
    :goto_3
    const-string v2, "Stopping BLE Active Scan Cycle."

    invoke-static {v2}, Lcom/google/android/gms/blescanner/b/e;->b(Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 237
    monitor-exit p0

    return-void

    .line 219
    :catchall_0
    move-exception v2

    :try_start_7
    monitor-exit v3

    throw v2
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 225
    :catchall_1
    move-exception v2

    .line 226
    :try_start_8
    iget-object v3, p0, Lcom/google/android/gms/blescanner/compat/k;->p:Landroid/bluetooth/BluetoothAdapter;

    iget-object v4, p0, Lcom/google/android/gms/blescanner/compat/k;->g:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v3, v4}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V
    :try_end_8
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 233
    :goto_4
    :try_start_9
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/k;->d()V

    throw v2
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    .line 207
    :catchall_2
    move-exception v2

    monitor-exit p0

    throw v2

    .line 219
    :cond_2
    :try_start_a
    iget-object v2, v6, Lcom/google/android/gms/blescanner/compat/p;->a:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/InterruptedException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    move-result-object v2

    move-object v8, v2

    goto :goto_1

    .line 226
    :cond_3
    :try_start_b
    iget-object v2, p0, Lcom/google/android/gms/blescanner/compat/k;->p:Landroid/bluetooth/BluetoothAdapter;

    iget-object v3, p0, Lcom/google/android/gms/blescanner/compat/k;->g:Landroid/bluetooth/BluetoothAdapter$LeScanCallback;

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothAdapter;->stopLeScan(Landroid/bluetooth/BluetoothAdapter$LeScanCallback;)V
    :try_end_b
    .catch Ljava/lang/NullPointerException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    .line 233
    :goto_5
    :try_start_c
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/k;->d()V

    goto :goto_3

    .line 229
    :catch_1
    move-exception v2

    const-string v2, "NPE thrown in BlockingScanCycle"

    invoke-static {v2}, Lcom/google/android/gms/blescanner/b/e;->b(Ljava/lang/String;)V

    goto :goto_5

    :catch_2
    move-exception v2

    const-string v2, "NPE thrown in BlockingScanCycle"

    invoke-static {v2}, Lcom/google/android/gms/blescanner/b/e;->b(Ljava/lang/String;)V

    goto :goto_2

    :catch_3
    move-exception v3

    const-string v3, "NPE thrown in BlockingScanCycle"

    invoke-static {v3}, Lcom/google/android/gms/blescanner/b/e;->b(Ljava/lang/String;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_2

    goto :goto_4
.end method

.method final c()V
    .locals 7

    .prologue
    const/16 v6, 0x5dc

    const/4 v2, -0x1

    .line 588
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/blescanner/compat/o;

    iget-object v0, v0, Lcom/google/android/gms/blescanner/compat/o;->d:Lcom/google/android/gms/blescanner/compat/ScanSettings;

    if-eq v1, v2, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/ScanSettings;->a()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/gms/blescanner/compat/k;->a(I)I

    move-result v4

    invoke-static {v1}, Lcom/google/android/gms/blescanner/compat/k;->a(I)I

    move-result v5

    if-le v4, v5, :cond_7

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/ScanSettings;->a()I

    move-result v0

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->e:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/blescanner/compat/n;

    iget-object v0, v0, Lcom/google/android/gms/blescanner/compat/n;->a:Lcom/google/android/gms/blescanner/compat/o;

    iget-object v0, v0, Lcom/google/android/gms/blescanner/compat/o;->d:Lcom/google/android/gms/blescanner/compat/ScanSettings;

    if-eq v1, v2, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/ScanSettings;->a()I

    move-result v4

    invoke-static {v4}, Lcom/google/android/gms/blescanner/compat/k;->a(I)I

    move-result v4

    invoke-static {v1}, Lcom/google/android/gms/blescanner/compat/k;->a(I)I

    move-result v5

    if-le v4, v5, :cond_2

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/compat/ScanSettings;->a()I

    move-result v1

    goto :goto_2

    :cond_4
    packed-switch v1, :pswitch_data_0

    :pswitch_0
    const/16 v0, 0x34bc

    iput v0, p0, Lcom/google/android/gms/blescanner/compat/k;->k:I

    iput v6, p0, Lcom/google/android/gms/blescanner/compat/k;->l:I

    .line 590
    :goto_3
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/k;->e()Z

    move-result v0

    if-nez v0, :cond_5

    .line 592
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->h:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/gms/blescanner/compat/k;->i:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 593
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/blescanner/compat/k;->j:J

    .line 594
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->a:Lcom/google/android/gms/blescanner/b/b;

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/b/b;->d()V

    .line 595
    const-string v0, "Scan : No clients left, canceling alarm."

    invoke-static {v0}, Lcom/google/android/gms/blescanner/b/e;->c(Ljava/lang/String;)V

    .line 609
    :goto_4
    return-void

    .line 588
    :pswitch_1
    const/16 v0, 0xa7

    iput v0, p0, Lcom/google/android/gms/blescanner/compat/k;->k:I

    iput v6, p0, Lcom/google/android/gms/blescanner/compat/k;->l:I

    goto :goto_3

    :pswitch_2
    const v0, 0x24414

    iput v0, p0, Lcom/google/android/gms/blescanner/compat/k;->k:I

    iput v6, p0, Lcom/google/android/gms/blescanner/compat/k;->l:I

    goto :goto_3

    .line 597
    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/k;->h()I

    move-result v0

    .line 598
    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/k;->g()I

    move-result v1

    add-int/2addr v1, v0

    .line 599
    if-eqz v0, :cond_6

    iget-wide v2, p0, Lcom/google/android/gms/blescanner/compat/k;->j:J

    int-to-long v4, v1

    cmp-long v0, v2, v4

    if-eqz v0, :cond_6

    .line 600
    int-to-long v0, v1

    iput-wide v0, p0, Lcom/google/android/gms/blescanner/compat/k;->j:J

    .line 602
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->h:Landroid/app/AlarmManager;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/blescanner/compat/k;->b:Lcom/google/android/gms/blescanner/b/d;

    invoke-interface {v2}, Lcom/google/android/gms/blescanner/b/d;->a()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/blescanner/compat/k;->j:J

    iget-object v6, p0, Lcom/google/android/gms/blescanner/compat/k;->i:Landroid/app/PendingIntent;

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    .line 605
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Scan alarm setup complete @ "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/blescanner/b/e;->c(Ljava/lang/String;)V

    .line 607
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/k;->a:Lcom/google/android/gms/blescanner/b/b;

    invoke-virtual {v0}, Lcom/google/android/gms/blescanner/b/b;->c()V

    goto :goto_4

    :cond_7
    move v0, v1

    goto/16 :goto_1

    .line 588
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
