.class public Lcom/google/android/gms/cast/service/CastOperationService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static a:Z

.field private static final b:Lcom/google/android/gms/cast/e/h;

.field private static final c:Ljava/util/concurrent/LinkedBlockingQueue;


# instance fields
.field private d:Lcom/google/android/gms/cast/service/c;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/cast/service/CastOperationService;->a:Z

    .line 57
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastOperationService"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/service/CastOperationService;->b:Lcom/google/android/gms/cast/e/h;

    .line 59
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/cast/service/CastOperationService;->c:Ljava/util/concurrent/LinkedBlockingQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 77
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 78
    return-void
.end method

.method static synthetic a()Lcom/google/android/gms/cast/e/h;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gms/cast/service/CastOperationService;->b:Lcom/google/android/gms/cast/e/h;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;)V
    .locals 1

    .prologue
    .line 144
    new-instance v0, Lcom/google/android/gms/cast/service/a/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/cast/service/a/a;-><init>(Lcom/google/android/gms/cast/b/e;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 145
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;DDZ)V
    .locals 8

    .prologue
    .line 196
    new-instance v0, Lcom/google/android/gms/cast/service/a/m;

    move-object v1, p1

    move-wide v2, p2

    move-wide v4, p4

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/cast/service/a/m;-><init>(Lcom/google/android/gms/cast/b/e;DDZ)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 198
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;I)V
    .locals 1

    .prologue
    .line 222
    new-instance v0, Lcom/google/android/gms/cast/service/a/o;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/cast/service/a/o;-><init>(Lcom/google/android/gms/cast/b/e;I)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 223
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Lcom/google/g/a/g;)V
    .locals 1

    .prologue
    .line 232
    new-instance v0, Lcom/google/android/gms/cast/service/a/f;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/cast/service/a/f;-><init>(Lcom/google/android/gms/cast/b/e;Lcom/google/g/a/g;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 233
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 202
    new-instance v0, Lcom/google/android/gms/cast/service/a/r;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/cast/service/a/r;-><init>(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 203
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V
    .locals 1

    .prologue
    .line 161
    new-instance v0, Lcom/google/android/gms/cast/service/a/d;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/cast/service/a/d;-><init>(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;Lcom/google/android/gms/cast/LaunchOptions;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 163
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 155
    new-instance v0, Lcom/google/android/gms/cast/service/a/c;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/cast/service/a/c;-><init>(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 157
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V
    .locals 8

    .prologue
    .line 184
    new-instance v0, Lcom/google/android/gms/cast/service/a/j;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/cast/service/a/j;-><init>(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 186
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;[BJLjava/lang/String;)V
    .locals 8

    .prologue
    .line 178
    new-instance v0, Lcom/google/android/gms/cast/service/a/i;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/cast/service/a/i;-><init>(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;[BJLjava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 180
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Z)V
    .locals 1

    .prologue
    .line 149
    new-instance v0, Lcom/google/android/gms/cast/service/a/b;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/cast/service/a/b;-><init>(Lcom/google/android/gms/cast/b/e;Z)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 151
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;ZDZ)V
    .locals 7

    .prologue
    .line 190
    new-instance v1, Lcom/google/android/gms/cast/service/a/k;

    move-object v2, p1

    move v3, p2

    move-wide v4, p3

    move v6, p5

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/cast/service/a/k;-><init>(Lcom/google/android/gms/cast/b/e;ZDZ)V

    invoke-static {p0, v1}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 192
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V
    .locals 1

    .prologue
    .line 63
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    .line 64
    sget-object v0, Lcom/google/android/gms/cast/service/CastOperationService;->c:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/LinkedBlockingQueue;->offer(Ljava/lang/Object;)Z

    .line 65
    sget-boolean v0, Lcom/google/android/gms/cast/service/CastOperationService;->a:Z

    if-nez v0, :cond_0

    .line 70
    const-string v0, "com.google.android.gms.cast.service.OPERATION"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 72
    :cond_0
    return-void
.end method

.method static synthetic b()Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gms/cast/service/CastOperationService;->c:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object v0
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;)V
    .locals 1

    .prologue
    .line 167
    new-instance v0, Lcom/google/android/gms/cast/service/a/e;

    invoke-direct {v0, p1}, Lcom/google/android/gms/cast/service/a/e;-><init>(Lcom/google/android/gms/cast/b/e;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 169
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;I)V
    .locals 1

    .prologue
    .line 227
    new-instance v0, Lcom/google/android/gms/cast/service/a/p;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/cast/service/a/p;-><init>(Lcom/google/android/gms/cast/b/e;I)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 228
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 207
    new-instance v0, Lcom/google/android/gms/cast/service/a/g;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/cast/service/a/g;-><init>(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 208
    return-void
.end method

.method public static c(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;)V
    .locals 1

    .prologue
    .line 172
    new-instance v0, Lcom/google/android/gms/cast/service/a/h;

    invoke-direct {v0, p1}, Lcom/google/android/gms/cast/service/a/h;-><init>(Lcom/google/android/gms/cast/b/e;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 173
    return-void
.end method

.method public static c(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 212
    new-instance v0, Lcom/google/android/gms/cast/service/a/t;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/cast/service/a/t;-><init>(Lcom/google/android/gms/cast/b/e;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 213
    return-void
.end method

.method public static d(Landroid/content/Context;Lcom/google/android/gms/cast/b/e;)V
    .locals 1

    .prologue
    .line 217
    new-instance v0, Lcom/google/android/gms/cast/service/a/n;

    invoke-direct {v0, p1}, Lcom/google/android/gms/cast/service/a/n;-><init>(Lcom/google/android/gms/cast/b/e;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/cast/service/CastOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/cast/service/b;)V

    .line 218
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 82
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 83
    new-instance v0, Lcom/google/android/gms/cast/service/c;

    const-string v1, "CastOperationService"

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/cast/service/c;-><init>(Lcom/google/android/gms/cast/service/CastOperationService;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/service/CastOperationService;->d:Lcom/google/android/gms/cast/service/c;

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/cast/service/CastOperationService;->d:Lcom/google/android/gms/cast/service/c;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/service/c;->start()V

    .line 85
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/cast/service/CastOperationService;->a:Z

    .line 86
    sget-object v0, Lcom/google/android/gms/cast/service/CastOperationService;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastOperationService created"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 92
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/service/CastOperationService;->d:Lcom/google/android/gms/cast/service/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/service/CastOperationService;->d:Lcom/google/android/gms/cast/service/c;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/service/c;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/cast/service/CastOperationService;->d:Lcom/google/android/gms/cast/service/c;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/service/c;->interrupt()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 95
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/service/CastOperationService;->d:Lcom/google/android/gms/cast/service/c;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/service/c;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 100
    :cond_0
    :goto_0
    sput-boolean v3, Lcom/google/android/gms/cast/service/CastOperationService;->a:Z

    .line 101
    sget-object v0, Lcom/google/android/gms/cast/service/CastOperationService;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastOperationService destroyed"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 102
    return-void

    .line 100
    :catchall_0
    move-exception v0

    sput-boolean v3, Lcom/google/android/gms/cast/service/CastOperationService;->a:Z

    .line 101
    sget-object v1, Lcom/google/android/gms/cast/service/CastOperationService;->b:Lcom/google/android/gms/cast/e/h;

    const-string v2, "CastOperationService destroyed"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    .prologue
    .line 112
    sget-object v0, Lcom/google/android/gms/cast/service/CastOperationService;->b:Lcom/google/android/gms/cast/e/h;

    const-string v1, "CastOperationService started"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 113
    const/4 v0, 0x2

    return v0
.end method
