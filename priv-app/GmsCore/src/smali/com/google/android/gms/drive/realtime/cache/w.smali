.class public final Lcom/google/android/gms/drive/realtime/cache/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/d/c;


# instance fields
.field private final a:Lcom/google/android/gms/drive/g/n;

.field private final b:Ljava/util/concurrent/ExecutorService;

.field private final c:Lcom/google/android/gms/drive/g/aw;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/g/aw;)V
    .locals 1

    .prologue
    .line 27
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/drive/realtime/cache/w;-><init>(Lcom/google/android/gms/drive/g/aw;Ljava/util/concurrent/ExecutorService;)V

    .line 28
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/g/aw;Ljava/util/concurrent/ExecutorService;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    invoke-virtual {p1}, Lcom/google/android/gms/drive/g/aw;->e()Lcom/google/android/gms/drive/g/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/w;->a:Lcom/google/android/gms/drive/g/n;

    .line 33
    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/cache/w;->b:Ljava/util/concurrent/ExecutorService;

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/cache/w;->c:Lcom/google/android/gms/drive/g/aw;

    .line 35
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/w;->a:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/n;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-virtual {p0}, Lcom/google/android/gms/drive/realtime/cache/w;->b()V

    .line 42
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/w;->c:Lcom/google/android/gms/drive/g/aw;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->f()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/w;->c:Lcom/google/android/gms/drive/g/aw;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/realtime/b/a;->a(Landroid/content/Context;Lcom/google/android/gms/drive/g/aw;)Lcom/google/android/gms/drive/realtime/b/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/b/a;->c()Lcom/google/android/gms/drive/realtime/cache/q;

    move-result-object v0

    .line 52
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/w;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v2, Lcom/google/android/gms/drive/realtime/cache/x;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/drive/realtime/cache/x;-><init>(Lcom/google/android/gms/drive/realtime/cache/w;Lcom/google/android/gms/drive/realtime/cache/q;)V

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 58
    return-void
.end method
