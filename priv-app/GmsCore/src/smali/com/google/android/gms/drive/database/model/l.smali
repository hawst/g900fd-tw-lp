.class public final Lcom/google/android/gms/drive/database/model/l;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:Z

.field private final d:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;JIIZ)V
    .locals 2

    .prologue
    .line 23
    invoke-static {}, Lcom/google/android/gms/drive/database/model/m;->a()Lcom/google/android/gms/drive/database/model/m;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 24
    iput-wide p2, p0, Lcom/google/android/gms/drive/database/model/l;->d:J

    .line 25
    iput p4, p0, Lcom/google/android/gms/drive/database/model/l;->a:I

    .line 26
    iput p5, p0, Lcom/google/android/gms/drive/database/model/l;->b:I

    .line 27
    iput-boolean p6, p0, Lcom/google/android/gms/drive/database/model/l;->c:Z

    .line 28
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/gms/drive/database/model/n;->a:Lcom/google/android/gms/drive/database/model/n;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/n;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/l;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 33
    sget-object v0, Lcom/google/android/gms/drive/database/model/n;->b:Lcom/google/android/gms/drive/database/model/n;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/n;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/drive/database/model/l;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 34
    sget-object v0, Lcom/google/android/gms/drive/database/model/n;->c:Lcom/google/android/gms/drive/database/model/n;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/n;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/drive/database/model/l;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 35
    sget-object v0, Lcom/google/android/gms/drive/database/model/n;->d:Lcom/google/android/gms/drive/database/model/n;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/n;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/drive/database/model/l;->c:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 36
    return-void
.end method
