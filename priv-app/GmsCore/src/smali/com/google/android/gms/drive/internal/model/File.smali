.class public final Lcom/google/android/gms/drive/internal/model/File;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/drive/internal/model/j;

.field private static final Q:Ljava/util/HashMap;


# instance fields
.field A:Ljava/lang/String;

.field B:Ljava/util/List;

.field C:Ljava/util/List;

.field D:Ljava/util/List;

.field E:Ljava/util/List;

.field F:J

.field G:Z

.field H:Ljava/lang/String;

.field I:Lcom/google/android/gms/drive/internal/model/User;

.field J:Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

.field K:Ljava/lang/String;

.field L:Ljava/lang/String;

.field M:Lcom/google/android/gms/drive/internal/model/Permission;

.field N:Ljava/lang/String;

.field O:Ljava/lang/String;

.field P:Z

.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Z

.field e:Ljava/util/List;

.field f:Z

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field j:Ljava/lang/String;

.field k:Z

.field l:Ljava/lang/String;

.field m:Ljava/lang/String;

.field n:Z

.field o:Ljava/lang/String;

.field p:J

.field q:Ljava/lang/String;

.field r:Ljava/lang/String;

.field s:Lcom/google/android/gms/drive/internal/model/File$IndexableText;

.field t:Lcom/google/android/gms/drive/internal/model/File$Labels;

.field u:Lcom/google/android/gms/drive/internal/model/User;

.field v:Ljava/lang/String;

.field w:Lcom/google/android/gms/drive/internal/model/FileLocalId;

.field x:Ljava/lang/String;

.field y:Ljava/lang/String;

.field z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/gms/drive/internal/model/j;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/model/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/model/File;->CREATOR:Lcom/google/android/gms/drive/internal/model/j;

    .line 402
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 405
    sput-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "alternateLink"

    const-string v2, "alternateLink"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 406
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "appDataContents"

    const-string v2, "appDataContents"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 407
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "authorizedAppIds"

    const-string v2, "authorizedAppIds"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 408
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "copyable"

    const-string v2, "copyable"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 409
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "createdDate"

    const-string v2, "createdDate"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "creatorAppId"

    const-string v2, "creatorAppId"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 411
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "description"

    const-string v2, "description"

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 412
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "downloadUrl"

    const-string v2, "downloadUrl"

    const/16 v3, 0xb

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 413
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "editable"

    const-string v2, "editable"

    const/16 v3, 0xd

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 414
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "embedLink"

    const-string v2, "embedLink"

    const/16 v3, 0xe

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 415
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "etag"

    const-string v2, "etag"

    const/16 v3, 0xf

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 416
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "explicitlyTrashed"

    const-string v2, "explicitlyTrashed"

    const/16 v3, 0x10

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 417
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "fileExtension"

    const-string v2, "fileExtension"

    const/16 v3, 0x12

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 418
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "fileSize"

    const-string v2, "fileSize"

    const/16 v3, 0x13

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 419
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "headRevisionId"

    const-string v2, "headRevisionId"

    const/16 v3, 0x19

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 420
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "id"

    const-string v2, "id"

    const/16 v3, 0x1b

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 421
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "indexableText"

    const-string v2, "indexableText"

    const/16 v3, 0x1d

    const-class v4, Lcom/google/android/gms/drive/internal/model/File$IndexableText;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "labels"

    const-string v2, "labels"

    const/16 v3, 0x1f

    const-class v4, Lcom/google/android/gms/drive/internal/model/File$Labels;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 427
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "lastModifyingUser"

    const-string v2, "lastModifyingUser"

    const/16 v3, 0x20

    const-class v4, Lcom/google/android/gms/drive/internal/model/User;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 430
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "lastViewedByMeDate"

    const-string v2, "lastViewedByMeDate"

    const/16 v3, 0x22

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "localId"

    const-string v2, "localId"

    const/16 v3, 0x23

    const-class v4, Lcom/google/android/gms/drive/internal/model/FileLocalId;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "md5Checksum"

    const-string v2, "md5Checksum"

    const/16 v3, 0x25

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "mimeType"

    const-string v2, "mimeType"

    const/16 v3, 0x26

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 436
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "modifiedByMeDate"

    const-string v2, "modifiedByMeDate"

    const/16 v3, 0x27

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 437
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "modifiedDate"

    const-string v2, "modifiedDate"

    const/16 v3, 0x28

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 438
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "ownerNames"

    const-string v2, "ownerNames"

    const/16 v3, 0x2b

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 439
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "owners"

    const-string v2, "owners"

    const/16 v3, 0x2c

    const-class v4, Lcom/google/android/gms/drive/internal/model/User;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 441
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "parents"

    const-string v2, "parents"

    const/16 v3, 0x2d

    const-class v4, Lcom/google/android/gms/drive/internal/model/ParentReference;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 443
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "properties"

    const-string v2, "properties"

    const/16 v3, 0x2f

    const-class v4, Lcom/google/android/gms/drive/internal/model/Property;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 445
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "quotaBytesUsed"

    const-string v2, "quotaBytesUsed"

    const/16 v3, 0x30

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 446
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "shared"

    const-string v2, "shared"

    const/16 v3, 0x32

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 447
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "sharedWithMeDate"

    const-string v2, "sharedWithMeDate"

    const/16 v3, 0x33

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 448
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "sharingUser"

    const-string v2, "sharingUser"

    const/16 v3, 0x34

    const-class v4, Lcom/google/android/gms/drive/internal/model/User;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 451
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "thumbnail"

    const-string v2, "thumbnail"

    const/16 v3, 0x37

    const-class v4, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 454
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "thumbnailLink"

    const-string v2, "thumbnailLink"

    const/16 v3, 0x38

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 455
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "title"

    const/16 v3, 0x39

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 456
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "userPermission"

    const-string v2, "userPermission"

    const/16 v3, 0x3a

    const-class v4, Lcom/google/android/gms/drive/internal/model/Permission;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 459
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "webContentLink"

    const-string v2, "webContentLink"

    const/16 v3, 0x3c

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 460
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "webViewLink"

    const-string v2, "webViewLink"

    const/16 v3, 0x3d

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 461
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    const-string v1, "writersCanShare"

    const-string v2, "writersCanShare"

    const/16 v3, 0x3e

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 462
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 738
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 739
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/model/File;->b:I

    .line 740
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    .line 741
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;ZLjava/util/List;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZLjava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/internal/model/File$IndexableText;Lcom/google/android/gms/drive/internal/model/File$Labels;Lcom/google/android/gms/drive/internal/model/User;Ljava/lang/String;Lcom/google/android/gms/drive/internal/model/FileLocalId;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;JZLjava/lang/String;Lcom/google/android/gms/drive/internal/model/User;Lcom/google/android/gms/drive/internal/model/File$Thumbnail;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/internal/model/Permission;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 787
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 788
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    .line 789
    iput p2, p0, Lcom/google/android/gms/drive/internal/model/File;->b:I

    .line 790
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->c:Ljava/lang/String;

    .line 791
    iput-boolean p4, p0, Lcom/google/android/gms/drive/internal/model/File;->d:Z

    .line 792
    iput-object p5, p0, Lcom/google/android/gms/drive/internal/model/File;->e:Ljava/util/List;

    .line 793
    iput-boolean p6, p0, Lcom/google/android/gms/drive/internal/model/File;->f:Z

    .line 794
    iput-object p7, p0, Lcom/google/android/gms/drive/internal/model/File;->g:Ljava/lang/String;

    .line 795
    iput-object p8, p0, Lcom/google/android/gms/drive/internal/model/File;->h:Ljava/lang/String;

    .line 796
    iput-object p9, p0, Lcom/google/android/gms/drive/internal/model/File;->i:Ljava/lang/String;

    .line 797
    iput-object p10, p0, Lcom/google/android/gms/drive/internal/model/File;->j:Ljava/lang/String;

    .line 798
    iput-boolean p11, p0, Lcom/google/android/gms/drive/internal/model/File;->k:Z

    .line 799
    iput-object p12, p0, Lcom/google/android/gms/drive/internal/model/File;->l:Ljava/lang/String;

    .line 800
    iput-object p13, p0, Lcom/google/android/gms/drive/internal/model/File;->m:Ljava/lang/String;

    .line 801
    move/from16 v0, p14

    iput-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->n:Z

    .line 802
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->o:Ljava/lang/String;

    .line 803
    move-wide/from16 v0, p16

    iput-wide v0, p0, Lcom/google/android/gms/drive/internal/model/File;->p:J

    .line 804
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->q:Ljava/lang/String;

    .line 805
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->r:Ljava/lang/String;

    .line 806
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->s:Lcom/google/android/gms/drive/internal/model/File$IndexableText;

    .line 807
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->t:Lcom/google/android/gms/drive/internal/model/File$Labels;

    .line 808
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->u:Lcom/google/android/gms/drive/internal/model/User;

    .line 809
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->v:Ljava/lang/String;

    .line 810
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->w:Lcom/google/android/gms/drive/internal/model/FileLocalId;

    .line 811
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->x:Ljava/lang/String;

    .line 812
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->y:Ljava/lang/String;

    .line 813
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->z:Ljava/lang/String;

    .line 814
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->A:Ljava/lang/String;

    .line 815
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->B:Ljava/util/List;

    .line 816
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->C:Ljava/util/List;

    .line 817
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->D:Ljava/util/List;

    .line 818
    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->E:Ljava/util/List;

    .line 819
    move-wide/from16 v0, p33

    iput-wide v0, p0, Lcom/google/android/gms/drive/internal/model/File;->F:J

    .line 820
    move/from16 v0, p35

    iput-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->G:Z

    .line 821
    move-object/from16 v0, p36

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->H:Ljava/lang/String;

    .line 822
    move-object/from16 v0, p37

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->I:Lcom/google/android/gms/drive/internal/model/User;

    .line 823
    move-object/from16 v0, p38

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->J:Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    .line 824
    move-object/from16 v0, p39

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->K:Ljava/lang/String;

    .line 825
    move-object/from16 v0, p40

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->L:Ljava/lang/String;

    .line 826
    move-object/from16 v0, p41

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->M:Lcom/google/android/gms/drive/internal/model/Permission;

    .line 827
    move-object/from16 v0, p42

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->N:Ljava/lang/String;

    .line 828
    move-object/from16 v0, p43

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->O:Ljava/lang/String;

    .line 829
    move/from16 v0, p44

    iput-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->P:Z

    .line 830
    return-void
.end method


# virtual methods
.method public final A()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1282
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->z:Ljava/lang/String;

    return-object v0
.end method

.method public final B()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1298
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->A:Ljava/lang/String;

    return-object v0
.end method

.method public final C()Ljava/util/List;
    .locals 1

    .prologue
    .line 1313
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->B:Ljava/util/List;

    return-object v0
.end method

.method public final D()Ljava/util/List;
    .locals 1

    .prologue
    .line 1345
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->D:Ljava/util/List;

    return-object v0
.end method

.method public final E()Ljava/util/List;
    .locals 1

    .prologue
    .line 1360
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->E:Ljava/util/List;

    return-object v0
.end method

.method public final F()J
    .locals 2

    .prologue
    .line 1375
    iget-wide v0, p0, Lcom/google/android/gms/drive/internal/model/File;->F:J

    return-wide v0
.end method

.method public final G()Z
    .locals 1

    .prologue
    .line 1390
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->G:Z

    return v0
.end method

.method public final H()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1405
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->H:Ljava/lang/String;

    return-object v0
.end method

.method public final I()Lcom/google/android/gms/drive/internal/model/User;
    .locals 1

    .prologue
    .line 1420
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->I:Lcom/google/android/gms/drive/internal/model/User;

    return-object v0
.end method

.method public final J()Lcom/google/android/gms/drive/internal/model/File$Thumbnail;
    .locals 1

    .prologue
    .line 1436
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->J:Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    return-object v0
.end method

.method public final K()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1451
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->K:Ljava/lang/String;

    return-object v0
.end method

.method public final L()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1466
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->L:Ljava/lang/String;

    return-object v0
.end method

.method public final M()Lcom/google/android/gms/drive/internal/model/Permission;
    .locals 1

    .prologue
    .line 1481
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->M:Lcom/google/android/gms/drive/internal/model/Permission;

    return-object v0
.end method

.method public final N()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1498
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->N:Ljava/lang/String;

    return-object v0
.end method

.method public final O()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1514
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->O:Ljava/lang/String;

    return-object v0
.end method

.method public final P()Z
    .locals 1

    .prologue
    .line 1529
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->P:Z

    return v0
.end method

.method public final a(Lcom/google/android/gms/drive/internal/model/File$IndexableText;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    .prologue
    .line 1706
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File;->s:Lcom/google/android/gms/drive/internal/model/File$IndexableText;

    .line 1707
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    const/16 v1, 0x1d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1708
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/drive/internal/model/File$Labels;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    .prologue
    .line 1715
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File;->t:Lcom/google/android/gms/drive/internal/model/File$Labels;

    .line 1716
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    const/16 v1, 0x1f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1717
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/drive/internal/model/File$Thumbnail;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    .prologue
    .line 1917
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File;->J:Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    .line 1918
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    const/16 v1, 0x37

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1919
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/drive/internal/model/FileLocalId;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    .prologue
    .line 1742
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File;->w:Lcom/google/android/gms/drive/internal/model/FileLocalId;

    .line 1743
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    const/16 v1, 0x23

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1744
    return-object p0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    .prologue
    .line 1836
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File;->D:Ljava/util/List;

    .line 1837
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    const/16 v1, 0x2d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1838
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    .prologue
    .line 1630
    iput-boolean p1, p0, Lcom/google/android/gms/drive/internal/model/File;->k:Z

    .line 1631
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1632
    return-object p0
.end method

.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 466
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;J)V
    .locals 5

    .prologue
    .line 2980
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 2981
    sparse-switch v0, :sswitch_data_0

    .line 2989
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a long."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2983
    :sswitch_0
    iput-wide p3, p0, Lcom/google/android/gms/drive/internal/model/File;->p:J

    .line 2992
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2993
    return-void

    .line 2986
    :sswitch_1
    iput-wide p3, p0, Lcom/google/android/gms/drive/internal/model/File;->F:J

    goto :goto_0

    .line 2981
    nop

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x30 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 3119
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 3120
    sparse-switch v0, :sswitch_data_0

    .line 3143
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3122
    :sswitch_0
    check-cast p3, Lcom/google/android/gms/drive/internal/model/File$IndexableText;

    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->s:Lcom/google/android/gms/drive/internal/model/File$IndexableText;

    .line 3147
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3148
    return-void

    .line 3125
    :sswitch_1
    check-cast p3, Lcom/google/android/gms/drive/internal/model/File$Labels;

    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->t:Lcom/google/android/gms/drive/internal/model/File$Labels;

    goto :goto_0

    .line 3128
    :sswitch_2
    check-cast p3, Lcom/google/android/gms/drive/internal/model/User;

    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->u:Lcom/google/android/gms/drive/internal/model/User;

    goto :goto_0

    .line 3131
    :sswitch_3
    check-cast p3, Lcom/google/android/gms/drive/internal/model/FileLocalId;

    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->w:Lcom/google/android/gms/drive/internal/model/FileLocalId;

    goto :goto_0

    .line 3134
    :sswitch_4
    check-cast p3, Lcom/google/android/gms/drive/internal/model/User;

    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->I:Lcom/google/android/gms/drive/internal/model/User;

    goto :goto_0

    .line 3137
    :sswitch_5
    check-cast p3, Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->J:Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    goto :goto_0

    .line 3140
    :sswitch_6
    check-cast p3, Lcom/google/android/gms/drive/internal/model/Permission;

    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->M:Lcom/google/android/gms/drive/internal/model/Permission;

    goto :goto_0

    .line 3120
    :sswitch_data_0
    .sparse-switch
        0x1d -> :sswitch_0
        0x1f -> :sswitch_1
        0x20 -> :sswitch_2
        0x23 -> :sswitch_3
        0x34 -> :sswitch_4
        0x37 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 3028
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 3029
    sparse-switch v0, :sswitch_data_0

    .line 3091
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3031
    :sswitch_0
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->c:Ljava/lang/String;

    .line 3094
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3095
    return-void

    .line 3034
    :sswitch_1
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->g:Ljava/lang/String;

    goto :goto_0

    .line 3037
    :sswitch_2
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->h:Ljava/lang/String;

    goto :goto_0

    .line 3040
    :sswitch_3
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->i:Ljava/lang/String;

    goto :goto_0

    .line 3043
    :sswitch_4
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->j:Ljava/lang/String;

    goto :goto_0

    .line 3046
    :sswitch_5
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->l:Ljava/lang/String;

    goto :goto_0

    .line 3049
    :sswitch_6
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->m:Ljava/lang/String;

    goto :goto_0

    .line 3052
    :sswitch_7
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->o:Ljava/lang/String;

    goto :goto_0

    .line 3055
    :sswitch_8
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->q:Ljava/lang/String;

    goto :goto_0

    .line 3058
    :sswitch_9
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->r:Ljava/lang/String;

    goto :goto_0

    .line 3061
    :sswitch_a
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->v:Ljava/lang/String;

    goto :goto_0

    .line 3064
    :sswitch_b
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->x:Ljava/lang/String;

    goto :goto_0

    .line 3067
    :sswitch_c
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->y:Ljava/lang/String;

    goto :goto_0

    .line 3070
    :sswitch_d
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->z:Ljava/lang/String;

    goto :goto_0

    .line 3073
    :sswitch_e
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->A:Ljava/lang/String;

    goto :goto_0

    .line 3076
    :sswitch_f
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->H:Ljava/lang/String;

    goto :goto_0

    .line 3079
    :sswitch_10
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->K:Ljava/lang/String;

    goto :goto_0

    .line 3082
    :sswitch_11
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->L:Ljava/lang/String;

    goto :goto_0

    .line 3085
    :sswitch_12
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->N:Ljava/lang/String;

    goto :goto_0

    .line 3088
    :sswitch_13
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->O:Ljava/lang/String;

    goto :goto_0

    .line 3029
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x6 -> :sswitch_1
        0x7 -> :sswitch_2
        0xa -> :sswitch_3
        0xb -> :sswitch_4
        0xe -> :sswitch_5
        0xf -> :sswitch_6
        0x12 -> :sswitch_7
        0x19 -> :sswitch_8
        0x1b -> :sswitch_9
        0x22 -> :sswitch_a
        0x25 -> :sswitch_b
        0x26 -> :sswitch_c
        0x27 -> :sswitch_d
        0x28 -> :sswitch_e
        0x33 -> :sswitch_f
        0x38 -> :sswitch_10
        0x39 -> :sswitch_11
        0x3c -> :sswitch_12
        0x3d -> :sswitch_13
    .end sparse-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 3153
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 3154
    packed-switch v0, :pswitch_data_0

    .line 3165
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known array of custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3156
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->C:Ljava/util/List;

    .line 3169
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3170
    return-void

    .line 3159
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->D:Ljava/util/List;

    goto :goto_0

    .line 3162
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->E:Ljava/util/List;

    goto :goto_0

    .line 3154
    :pswitch_data_0
    .packed-switch 0x2c
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 2998
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 2999
    sparse-switch v0, :sswitch_data_0

    .line 3019
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a boolean."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3001
    :sswitch_0
    iput-boolean p3, p0, Lcom/google/android/gms/drive/internal/model/File;->d:Z

    .line 3022
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3023
    return-void

    .line 3004
    :sswitch_1
    iput-boolean p3, p0, Lcom/google/android/gms/drive/internal/model/File;->f:Z

    goto :goto_0

    .line 3007
    :sswitch_2
    iput-boolean p3, p0, Lcom/google/android/gms/drive/internal/model/File;->k:Z

    goto :goto_0

    .line 3010
    :sswitch_3
    iput-boolean p3, p0, Lcom/google/android/gms/drive/internal/model/File;->n:Z

    goto :goto_0

    .line 3013
    :sswitch_4
    iput-boolean p3, p0, Lcom/google/android/gms/drive/internal/model/File;->G:Z

    goto :goto_0

    .line 3016
    :sswitch_5
    iput-boolean p3, p0, Lcom/google/android/gms/drive/internal/model/File;->P:Z

    goto :goto_0

    .line 2999
    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x5 -> :sswitch_1
        0xd -> :sswitch_2
        0x10 -> :sswitch_3
        0x32 -> :sswitch_4
        0x3e -> :sswitch_5
    .end sparse-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 2862
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/util/List;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    .prologue
    .line 1859
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File;->E:Ljava/util/List;

    .line 1860
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    const/16 v1, 0x2f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1861
    return-object p0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 2867
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 2949
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2869
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->c:Ljava/lang/String;

    .line 2947
    :goto_0
    return-object v0

    .line 2871
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2873
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->e:Ljava/util/List;

    goto :goto_0

    .line 2875
    :pswitch_4
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->f:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2877
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->g:Ljava/lang/String;

    goto :goto_0

    .line 2879
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->h:Ljava/lang/String;

    goto :goto_0

    .line 2881
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->i:Ljava/lang/String;

    goto :goto_0

    .line 2883
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->j:Ljava/lang/String;

    goto :goto_0

    .line 2885
    :pswitch_9
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->k:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2887
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->l:Ljava/lang/String;

    goto :goto_0

    .line 2889
    :pswitch_b
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->m:Ljava/lang/String;

    goto :goto_0

    .line 2891
    :pswitch_c
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->n:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2893
    :pswitch_d
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->o:Ljava/lang/String;

    goto :goto_0

    .line 2895
    :pswitch_e
    iget-wide v0, p0, Lcom/google/android/gms/drive/internal/model/File;->p:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 2897
    :pswitch_f
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->q:Ljava/lang/String;

    goto :goto_0

    .line 2899
    :pswitch_10
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->r:Ljava/lang/String;

    goto :goto_0

    .line 2901
    :pswitch_11
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->s:Lcom/google/android/gms/drive/internal/model/File$IndexableText;

    goto :goto_0

    .line 2903
    :pswitch_12
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->t:Lcom/google/android/gms/drive/internal/model/File$Labels;

    goto :goto_0

    .line 2905
    :pswitch_13
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->u:Lcom/google/android/gms/drive/internal/model/User;

    goto :goto_0

    .line 2907
    :pswitch_14
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->v:Ljava/lang/String;

    goto :goto_0

    .line 2909
    :pswitch_15
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->w:Lcom/google/android/gms/drive/internal/model/FileLocalId;

    goto :goto_0

    .line 2911
    :pswitch_16
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->x:Ljava/lang/String;

    goto :goto_0

    .line 2913
    :pswitch_17
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->y:Ljava/lang/String;

    goto :goto_0

    .line 2915
    :pswitch_18
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->z:Ljava/lang/String;

    goto :goto_0

    .line 2917
    :pswitch_19
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->A:Ljava/lang/String;

    goto :goto_0

    .line 2919
    :pswitch_1a
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->B:Ljava/util/List;

    goto :goto_0

    .line 2921
    :pswitch_1b
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->C:Ljava/util/List;

    goto :goto_0

    .line 2923
    :pswitch_1c
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->D:Ljava/util/List;

    goto :goto_0

    .line 2925
    :pswitch_1d
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->E:Ljava/util/List;

    goto :goto_0

    .line 2927
    :pswitch_1e
    iget-wide v0, p0, Lcom/google/android/gms/drive/internal/model/File;->F:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    .line 2929
    :pswitch_1f
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->G:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 2931
    :pswitch_20
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->H:Ljava/lang/String;

    goto :goto_0

    .line 2933
    :pswitch_21
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->I:Lcom/google/android/gms/drive/internal/model/User;

    goto :goto_0

    .line 2935
    :pswitch_22
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->J:Lcom/google/android/gms/drive/internal/model/File$Thumbnail;

    goto :goto_0

    .line 2937
    :pswitch_23
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->K:Ljava/lang/String;

    goto/16 :goto_0

    .line 2939
    :pswitch_24
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->L:Ljava/lang/String;

    goto/16 :goto_0

    .line 2941
    :pswitch_25
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->M:Lcom/google/android/gms/drive/internal/model/Permission;

    goto/16 :goto_0

    .line 2943
    :pswitch_26
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->N:Ljava/lang/String;

    goto/16 :goto_0

    .line 2945
    :pswitch_27
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->O:Ljava/lang/String;

    goto/16 :goto_0

    .line 2947
    :pswitch_28
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->P:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_0

    .line 2867
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_8
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_0
        :pswitch_11
        :pswitch_0
        :pswitch_12
        :pswitch_13
        :pswitch_0
        :pswitch_14
        :pswitch_15
        :pswitch_0
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_0
        :pswitch_0
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_0
        :pswitch_1d
        :pswitch_1e
        :pswitch_0
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_0
        :pswitch_0
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_0
        :pswitch_26
        :pswitch_27
        :pswitch_28
    .end packed-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 2852
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 927
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->c:Ljava/lang/String;

    return-object v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 3100
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 3101
    sparse-switch v0, :sswitch_data_0

    .line 3109
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be an array of String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 3103
    :sswitch_0
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->e:Ljava/util/List;

    .line 3113
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 3114
    return-void

    .line 3106
    :sswitch_1
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/File;->B:Ljava/util/List;

    goto :goto_0

    .line 3101
    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x2b -> :sswitch_1
    .end sparse-switch
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 942
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->d:Z

    return v0
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 2857
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Ljava/util/List;
    .locals 1

    .prologue
    .line 958
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->e:Ljava/util/List;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 2841
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->CREATOR:Lcom/google/android/gms/drive/internal/model/j;

    const/4 v0, 0x0

    return v0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    .prologue
    .line 1593
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File;->g:Ljava/lang/String;

    .line 1594
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1595
    return-object p0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 973
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->f:Z

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 3187
    instance-of v0, p1, Lcom/google/android/gms/drive/internal/model/File;

    if-nez v0, :cond_0

    move v0, v1

    .line 3218
    :goto_0
    return v0

    .line 3192
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 3193
    goto :goto_0

    .line 3196
    :cond_1
    check-cast p1, Lcom/google/android/gms/drive/internal/model/File;

    .line 3197
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 3198
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/File;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 3199
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/File;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 3201
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/File;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/File;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 3203
    goto :goto_0

    :cond_3
    move v0, v1

    .line 3208
    goto :goto_0

    .line 3211
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/File;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 3213
    goto :goto_0

    :cond_5
    move v0, v2

    .line 3218
    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    .prologue
    .line 1611
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File;->i:Ljava/lang/String;

    .line 1612
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1613
    return-object p0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 988
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final g(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    .prologue
    .line 1733
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File;->v:Ljava/lang/String;

    .line 1734
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    const/16 v1, 0x22

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1735
    return-object p0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1003
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final h(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    .prologue
    .line 1763
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File;->y:Ljava/lang/String;

    .line 1764
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    const/16 v1, 0x26

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1765
    return-object p0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1018
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 3174
    const/4 v0, 0x0

    .line 3175
    sget-object v1, Lcom/google/android/gms/drive/internal/model/File;->Q:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 3176
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/File;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 3177
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 3178
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/File;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 3180
    goto :goto_0

    .line 3181
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final i(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    .prologue
    .line 1773
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File;->z:Ljava/lang/String;

    .line 1774
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    const/16 v1, 0x27

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1775
    return-object p0
.end method

.method public final j(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    .prologue
    .line 1783
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File;->A:Ljava/lang/String;

    .line 1784
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    const/16 v1, 0x28

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1785
    return-object p0
.end method

.method public final k(Ljava/lang/String;)Lcom/google/android/gms/drive/internal/model/File;
    .locals 2

    .prologue
    .line 1935
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/File;->L:Ljava/lang/String;

    .line 1936
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->a:Ljava/util/Set;

    const/16 v1, 0x39

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1937
    return-object p0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1034
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 1049
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->k:Z

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1079
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 1095
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/File;->n:Z

    return v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1112
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->o:Ljava/lang/String;

    return-object v0
.end method

.method public final q()J
    .locals 2

    .prologue
    .line 1127
    iget-wide v0, p0, Lcom/google/android/gms/drive/internal/model/File;->p:J

    return-wide v0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1143
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final s()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1158
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final t()Lcom/google/android/gms/drive/internal/model/File$IndexableText;
    .locals 1

    .prologue
    .line 1173
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->s:Lcom/google/android/gms/drive/internal/model/File$IndexableText;

    return-object v0
.end method

.method public final u()Lcom/google/android/gms/drive/internal/model/File$Labels;
    .locals 1

    .prologue
    .line 1188
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->t:Lcom/google/android/gms/drive/internal/model/File$Labels;

    return-object v0
.end method

.method public final v()Lcom/google/android/gms/drive/internal/model/User;
    .locals 1

    .prologue
    .line 1203
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->u:Lcom/google/android/gms/drive/internal/model/User;

    return-object v0
.end method

.method public final w()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1218
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->v:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 2846
    sget-object v0, Lcom/google/android/gms/drive/internal/model/File;->CREATOR:Lcom/google/android/gms/drive/internal/model/j;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/drive/internal/model/j;->a(Lcom/google/android/gms/drive/internal/model/File;Landroid/os/Parcel;I)V

    .line 2847
    return-void
.end method

.method public final x()Lcom/google/android/gms/drive/internal/model/FileLocalId;
    .locals 1

    .prologue
    .line 1233
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->w:Lcom/google/android/gms/drive/internal/model/FileLocalId;

    return-object v0
.end method

.method public final y()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1249
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->x:Ljava/lang/String;

    return-object v0
.end method

.method public final z()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1266
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/File;->y:Ljava/lang/String;

    return-object v0
.end method
