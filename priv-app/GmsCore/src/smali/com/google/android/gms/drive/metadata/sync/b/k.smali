.class public final Lcom/google/android/gms/drive/metadata/sync/b/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/metadata/sync/b/g;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/metadata/sync/b/j;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Date;

.field private final d:Lcom/google/android/gms/drive/api/a/al;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/metadata/sync/b/j;Ljava/lang/String;Ljava/util/Date;Lcom/google/android/gms/drive/api/a/al;)V
    .locals 0

    .prologue
    .line 244
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/b/k;->a:Lcom/google/android/gms/drive/metadata/sync/b/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 245
    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/sync/b/k;->b:Ljava/lang/String;

    .line 246
    iput-object p3, p0, Lcom/google/android/gms/drive/metadata/sync/b/k;->c:Ljava/util/Date;

    .line 247
    iput-object p4, p0, Lcom/google/android/gms/drive/metadata/sync/b/k;->d:Lcom/google/android/gms/drive/api/a/al;

    .line 248
    return-void
.end method


# virtual methods
.method public final a(Z)V
    .locals 6

    .prologue
    .line 252
    if-eqz p1, :cond_1

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/k;->a:Lcom/google/android/gms/drive/metadata/sync/b/j;

    iget-object v0, v0, Lcom/google/android/gms/drive/metadata/sync/b/j;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/b/k;->a:Lcom/google/android/gms/drive/metadata/sync/b/j;

    iget-object v1, v1, Lcom/google/android/gms/drive/metadata/sync/b/j;->c:Lcom/google/android/gms/drive/database/model/a;

    new-instance v2, Lcom/google/android/gms/drive/metadata/sync/a/k;

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/b/k;->b:Ljava/lang/String;

    invoke-direct {v2, v3}, Lcom/google/android/gms/drive/metadata/sync/a/k;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/b/k;->c:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-interface {v0, v1, v2, v4, v5}, Lcom/google/android/gms/drive/database/r;->a(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/metadata/sync/a/c;J)Lcom/google/android/gms/drive/database/model/bb;

    move-result-object v0

    .line 255
    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/b/k;->d:Lcom/google/android/gms/drive/api/a/al;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/bb;->a:Lcom/google/android/gms/drive/metadata/sync/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/sync/a/d;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/api/a/al;->a(Z)V

    .line 260
    :goto_1
    return-void

    .line 255
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 258
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/k;->d:Lcom/google/android/gms/drive/api/a/al;

    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/drive/api/a/al;->a:Lcom/google/android/gms/drive/internal/ca;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x8

    const-string v3, "Sync more failed."

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v1, "SyncMoreOperation"

    const-string v2, "Unable to report sync more result."

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    goto :goto_1
.end method
