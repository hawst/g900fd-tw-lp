.class public Lcom/google/android/gms/fitness/sensors/sample/CollectSensorService;
.super Lcom/google/android/gms/common/app/d;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/gms/fitness/sensors/sample/CollectSensorService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/app/d;-><init>(Ljava/lang/String;)V

    .line 38
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v6, 0x0

    .line 42
    const-string v0, "adapter_id"

    const-wide/16 v2, -0x1

    invoke-virtual {p1, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    .line 43
    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/sensors/sample/c;->a(J)Lcom/google/android/gms/fitness/sensors/sample/c;

    move-result-object v4

    .line 45
    if-nez v4, :cond_0

    .line 46
    const-string v2, "CollectSensorService did not find adapter %d"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 75
    :goto_0
    return-void

    .line 50
    :cond_0
    const-string v0, "request_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 51
    invoke-virtual {v4, v5}, Lcom/google/android/gms/fitness/sensors/sample/c;->a(I)Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

    move-result-object v7

    .line 52
    if-nez v7, :cond_1

    .line 53
    const-string v0, "CollectSensorService did not find request %d"

    new-array v1, v8, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 57
    :cond_1
    const-string v0, "max_sample_secs"

    const/16 v1, 0x14

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    .line 58
    const-string v0, "max_sample_points"

    const/16 v1, 0xa

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 59
    new-instance v0, Lcom/google/android/gms/fitness/sensors/sample/b;

    invoke-virtual {v7}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->c()Lcom/google/android/gms/fitness/data/l;

    move-result-object v1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/fitness/sensors/sample/b;-><init>(Lcom/google/android/gms/fitness/data/l;IILcom/google/android/gms/fitness/sensors/sample/c;IB)V

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/sensors/sample/c;->b()Lcom/google/android/gms/fitness/sensors/a;

    move-result-object v1

    invoke-virtual {v7, v0}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->b(Lcom/google/android/gms/fitness/data/l;)Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/sensors/a;->a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/k/k/a/af;

    move-result-object v6

    new-instance v0, Lcom/google/android/gms/fitness/sensors/sample/a;

    move-object v1, p0

    move-object v2, p1

    move-object v3, v7

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/fitness/sensors/sample/a;-><init>(Lcom/google/android/gms/fitness/sensors/sample/CollectSensorService;Landroid/content/Intent;Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;Lcom/google/android/gms/fitness/sensors/sample/c;I)V

    invoke-static {v6, v0}, Lcom/google/k/k/a/n;->a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/m;)V

    goto :goto_0
.end method
