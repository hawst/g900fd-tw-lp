.class final Lcom/google/android/gms/people/profile/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/profile/AvatarActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/people/profile/AvatarActivity;)V
    .locals 0

    .prologue
    .line 526
    iput-object p1, p0, Lcom/google/android/gms/people/profile/c;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 5

    .prologue
    .line 526
    check-cast p1, Lcom/google/android/gms/people/q;

    invoke-interface {p1}, Lcom/google/android/gms/people/q;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/people/q;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "People.Avatar"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onAvatarSet "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/people/profile/c;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    invoke-static {v2}, Lcom/google/android/gms/people/profile/AvatarActivity;->e(Lcom/google/android/gms/people/profile/AvatarActivity;)Z

    iget-object v2, p0, Lcom/google/android/gms/people/profile/c;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    invoke-static {v2}, Lcom/google/android/gms/people/profile/AvatarActivity;->f(Lcom/google/android/gms/people/profile/AvatarActivity;)Landroid/support/v4/app/m;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/people/profile/c;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    invoke-static {v2}, Lcom/google/android/gms/people/profile/AvatarActivity;->f(Lcom/google/android/gms/people/profile/AvatarActivity;)Landroid/support/v4/app/m;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/m;->dismiss()V

    :cond_1
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string v0, "com.google.android.gms.people.profile.EXTRA_AVATAR_URL"

    invoke-virtual {v2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/people/profile/c;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    sget-object v1, Lcom/google/android/gms/common/analytics/q;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    const-string v3, "2"

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Lcom/google/android/gms/people/profile/AvatarActivity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/c;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    sget-object v1, Lcom/google/android/gms/common/analytics/o;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {v0, v1}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Lcom/google/android/gms/people/profile/AvatarActivity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/c;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(ILandroid/content/Intent;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/people/profile/c;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->finish()V

    return-void

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/profile/c;->a:Lcom/google/android/gms/people/profile/AvatarActivity;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0
.end method
