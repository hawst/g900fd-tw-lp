.class public abstract Lcom/google/android/gms/people/service/a/a/c;
.super Lcom/google/android/gms/people/service/a/a/e;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/people/service/a/a/e;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;)V

    .line 25
    return-void
.end method


# virtual methods
.method protected abstract g()[B
.end method

.method protected final h()Lcom/google/android/gms/people/service/a/a/f;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, -0x1

    const/4 v3, 0x0

    .line 30
    invoke-virtual {p0}, Lcom/google/android/gms/people/service/a/a/c;->g()[B

    move-result-object v2

    .line 31
    sget-object v0, Lcom/google/android/gms/people/service/a/a/c;->i:[B

    if-eq v2, v0, :cond_0

    if-nez v2, :cond_1

    .line 32
    :cond_0
    sget-object v0, Lcom/google/android/gms/people/service/a/a/f;->a:Lcom/google/android/gms/people/service/a/a/f;

    .line 52
    :goto_0
    return-object v0

    .line 34
    :cond_1
    sget-object v0, Lcom/google/android/gms/people/service/a/a/c;->j:[B

    if-ne v2, v0, :cond_2

    .line 35
    sget-object v0, Lcom/google/android/gms/people/service/a/a/f;->b:Lcom/google/android/gms/people/service/a/a/f;

    goto :goto_0

    .line 38
    :cond_2
    sget-object v0, Lcom/google/android/gms/people/a/a;->aF:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    array-length v1, v2

    invoke-static {v2, v3, v1, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v1, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v0, v0, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    .line 42
    :goto_1
    sget-object v0, Lcom/google/android/gms/people/f/l;->a:Lcom/google/android/gms/people/f/l;

    invoke-static {}, Lcom/google/android/gms/people/f/l;->a()[Lcom/google/android/gms/people/f/m;

    move-result-object v0

    .line 43
    aget-object v3, v0, v3

    .line 44
    aget-object v4, v0, v4

    .line 50
    :try_start_0
    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/people/service/a/a/d;

    invoke-direct {v0, v4, v2}, Lcom/google/android/gms/people/service/a/a/d;-><init>(Lcom/google/android/gms/people/f/m;[B)V

    invoke-static {v0}, Lcom/google/android/gms/people/f/j;->a(Ljava/lang/Runnable;)V

    .line 52
    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v3, v2, v0, v1}, Lcom/google/android/gms/people/service/a/a/f;->a(Lcom/google/android/gms/people/f/m;IIZ)Lcom/google/android/gms/people/service/a/a/f;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 38
    :cond_3
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    move-object v1, v0

    goto :goto_1

    .line 55
    :catch_0
    move-exception v0

    .line 57
    invoke-static {v3}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 58
    invoke-static {v4}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 59
    throw v0
.end method
