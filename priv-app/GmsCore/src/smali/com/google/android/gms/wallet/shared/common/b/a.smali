.class public final Lcom/google/android/gms/wallet/shared/common/b/a;
.super Lcom/google/android/gms/wallet/dynamite/image/d;
.source "SourceFile"


# instance fields
.field private final g:Lcom/google/android/gms/wallet/shared/common/b/e;

.field private final h:Lcom/google/android/gms/wallet/shared/common/d/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 72
    new-instance v0, Lcom/google/android/gms/wallet/shared/common/b/e;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/shared/common/b/e;-><init>()V

    new-instance v1, Lcom/google/android/gms/wallet/shared/common/d/a;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/wallet/shared/common/d/a;-><init>(B)V

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/wallet/shared/common/b/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/common/b/e;Lcom/google/android/gms/wallet/shared/common/d/a;)V

    .line 73
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/common/b/e;Lcom/google/android/gms/wallet/shared/common/d/a;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/dynamite/image/d;-><init>(Landroid/content/Context;)V

    .line 62
    iput-object p2, p0, Lcom/google/android/gms/wallet/shared/common/b/a;->g:Lcom/google/android/gms/wallet/shared/common/b/e;

    .line 63
    iput-object p3, p0, Lcom/google/android/gms/wallet/shared/common/b/a;->h:Lcom/google/android/gms/wallet/shared/common/d/a;

    .line 64
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 172
    invoke-virtual {p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "mounted"

    invoke-static {}, Landroid/os/Environment;->getExternalStorageState()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Landroid/content/Context;->getExternalCacheDir()Ljava/io/File;

    move-result-object v0

    :cond_0
    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0, p3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 174
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_1

    .line 175
    invoke-virtual {v2}, Ljava/io/File;->mkdir()Z

    .line 178
    :cond_1
    :try_start_0
    const-string v0, "bitmap"

    const/4 v3, 0x0

    invoke-static {v0, v3, v2}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 179
    :try_start_1
    new-instance v2, Lcom/google/android/gms/wallet/shared/common/d/c;

    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/google/android/gms/wallet/shared/common/d/c;-><init>(Ljava/net/URL;)V

    .line 180
    iget-object v3, p0, Lcom/google/android/gms/wallet/shared/common/b/a;->h:Lcom/google/android/gms/wallet/shared/common/d/a;

    new-instance v4, Ljava/io/FileOutputStream;

    invoke-direct {v4, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v5, 0x0

    invoke-virtual {v3, v2, v4, v5}, Lcom/google/android/gms/wallet/shared/common/d/a;->a(Lcom/google/android/gms/wallet/shared/common/d/c;Ljava/io/OutputStream;I)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result v2

    .line 182
    if-nez v2, :cond_2

    move-object v0, v1

    .line 188
    :cond_2
    :goto_0
    return-object v0

    .line 185
    :catch_0
    move-exception v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 186
    :goto_1
    const-string v2, "ImageNetworkWorker"

    const-string v3, "Exception downloading image to disk"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 185
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method private a(Ljava/lang/String;I)[B
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 147
    new-instance v1, Ljava/io/ByteArrayOutputStream;

    const/16 v2, 0xc00

    invoke-direct {v1, v2}, Ljava/io/ByteArrayOutputStream;-><init>(I)V

    .line 149
    :try_start_0
    new-instance v2, Lcom/google/android/gms/wallet/shared/common/d/c;

    new-instance v3, Ljava/net/URL;

    invoke-direct {v3, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/google/android/gms/wallet/shared/common/d/c;-><init>(Ljava/net/URL;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    iget-object v3, p0, Lcom/google/android/gms/wallet/shared/common/b/a;->h:Lcom/google/android/gms/wallet/shared/common/d/a;

    invoke-virtual {v3, v2, v1, p2}, Lcom/google/android/gms/wallet/shared/common/d/a;->a(Lcom/google/android/gms/wallet/shared/common/d/c;Ljava/io/OutputStream;I)Z

    move-result v2

    .line 157
    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    .line 153
    :catch_0
    move-exception v1

    const-string v1, "ImageNetworkWorker"

    const-string v2, "Bad URL provided."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
    .locals 7

    .prologue
    .line 42
    check-cast p1, Lcom/google/android/gms/wallet/shared/common/b/b;

    const/4 v0, 0x0

    iget-boolean v1, p1, Lcom/google/android/gms/wallet/shared/common/b/b;->b:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/wallet/shared/common/b/a;->b:Landroid/content/Context;

    iget-object v2, p1, Lcom/google/android/gms/wallet/shared/common/b/b;->a:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/wallet/shared/common/b/a;->g:Lcom/google/android/gms/wallet/shared/common/b/e;

    iget-object v3, v3, Lcom/google/android/gms/wallet/shared/common/b/e;->d:Ljava/lang/String;

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/gms/wallet/shared/common/b/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/shared/common/b/a;->g:Lcom/google/android/gms/wallet/shared/common/b/e;

    iget v2, v2, Lcom/google/android/gms/wallet/shared/common/b/e;->a:I

    iget-object v3, p0, Lcom/google/android/gms/wallet/shared/common/b/a;->g:Lcom/google/android/gms/wallet/shared/common/b/e;

    iget v3, v3, Lcom/google/android/gms/wallet/shared/common/b/e;->b:I

    new-instance v4, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v4}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    const/4 v5, 0x1

    iput-boolean v5, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    iget v5, v4, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v6, v4, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-static {v5, v6, v2, v3}, Lcom/google/android/gms/wallet/dynamite/image/c;->a(IIII)I

    move-result v2

    iput v2, v4, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    const/4 v2, 0x0

    iput-boolean v2, v4, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    invoke-static {v0, v4}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;Landroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p1, Lcom/google/android/gms/wallet/shared/common/b/b;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/wallet/shared/common/b/a;->g:Lcom/google/android/gms/wallet/shared/common/b/e;

    iget v2, v2, Lcom/google/android/gms/wallet/shared/common/b/e;->c:I

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/wallet/shared/common/b/a;->a(Ljava/lang/String;I)[B

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/io/ByteArrayInputStream;

    invoke-direct {v0, v1}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/common/b/c;)V
    .locals 3

    .prologue
    .line 83
    iget-object v0, p1, Lcom/google/android/gms/wallet/shared/common/b/c;->e:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget v0, p1, Lcom/google/android/gms/wallet/shared/common/b/c;->d:I

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/common/b/a;->e:Landroid/graphics/Bitmap;

    iput-object v0, p1, Lcom/google/android/gms/wallet/shared/common/b/c;->e:Landroid/graphics/Bitmap;

    .line 86
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/wallet/shared/common/b/c;->e:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    .line 87
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/common/b/c;->a()Lcom/google/android/gms/wallet/shared/common/b/b;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/wallet/shared/common/b/c;->c:Landroid/widget/ImageView;

    iget-object v2, p1, Lcom/google/android/gms/wallet/shared/common/b/c;->e:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/wallet/shared/common/b/a;->a(Ljava/lang/Object;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    .line 92
    :goto_0
    return-void

    .line 90
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/common/b/c;->a()Lcom/google/android/gms/wallet/shared/common/b/b;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/wallet/shared/common/b/c;->c:Landroid/widget/ImageView;

    iget v2, p1, Lcom/google/android/gms/wallet/shared/common/b/c;->d:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/wallet/shared/common/b/a;->a(Ljava/lang/Object;Landroid/widget/ImageView;I)V

    goto :goto_0
.end method
