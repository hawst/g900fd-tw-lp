.class public final Lcom/google/android/gms/appstate/service/d;
.super Lcom/google/android/gms/common/f/a;
.source "SourceFile"


# static fields
.field private static final a:Landroid/os/Bundle;

.field private static final b:Lcom/google/android/gms/appstate/service/e;


# instance fields
.field private final c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/google/android/gms/appstate/service/d;->a:Landroid/os/Bundle;

    .line 37
    new-instance v0, Lcom/google/android/gms/appstate/service/e;

    invoke-direct {v0}, Lcom/google/android/gms/appstate/service/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/appstate/service/d;->b:Lcom/google/android/gms/appstate/service/e;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 45
    const-string v0, "com.google.android.gms.appstate"

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/common/f/a;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 46
    invoke-virtual {p0}, Lcom/google/android/gms/appstate/service/d;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/appstate/service/d;->c:Landroid/content/Context;

    .line 47
    return-void
.end method

.method static synthetic a()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/gms/appstate/service/d;->a:Landroid/os/Bundle;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 2

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_0

    .line 119
    sget-object v1, Lcom/google/android/gms/appstate/service/d;->b:Lcom/google/android/gms/appstate/service/e;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/appstate/service/e;->a(Ljava/lang/String;)V

    .line 121
    :cond_0
    return-void
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 20

    .prologue
    .line 56
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/appstate/service/d;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 57
    const-string v2, "AppStateSyncAdapter"

    const-string v3, "In restricted profile; skipping sync."

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :goto_0
    return-void

    .line 61
    :cond_0
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/appstate/service/d;->a(Z)V

    .line 62
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/appstate/service/d;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/appstate/a/e;->a(Landroid/content/Context;)Lcom/google/android/gms/appstate/a/e;

    move-result-object v11

    .line 64
    if-eqz p2, :cond_2

    :try_start_0
    const-string v2, "initialize"

    const/4 v3, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v2

    if-gez v2, :cond_1

    const/4 v2, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    :cond_1
    const/4 v2, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    sget-object v2, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1, v2}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    .line 66
    :goto_1
    invoke-virtual {v11}, Lcom/google/android/gms/appstate/a/e;->a()V

    .line 67
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/appstate/service/d;->a(Z)V

    goto :goto_0

    .line 64
    :cond_2
    :try_start_1
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/appstate/service/d;->c:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/appstate/a/d;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v2

    :try_start_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/appstate/service/d;->c:Landroid/content/Context;

    iget-object v4, v11, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v4}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V
    :try_end_2
    .catch Lcom/google/android/gms/auth/q; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/gms/appstate/d/b/a; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    :try_start_3
    iget-object v12, v11, Lcom/google/android/gms/appstate/a/e;->b:Lcom/google/android/gms/appstate/a/a;

    const/4 v10, 0x0

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v13

    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    invoke-virtual {v3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v13}, Lcom/google/android/gms/appstate/provider/d;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/appstate/a/c;->a:[Ljava/lang/String;

    const-string v5, "upsync_required=1"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result-object v16

    :goto_2
    :try_start_4
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    const/4 v3, 0x2

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v9

    const/4 v3, 0x3

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/4 v3, 0x4

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    const/4 v3, 0x5

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const/4 v3, 0x6

    move-object/from16 v0, v16

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x7

    move-object/from16 v0, v16

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4, v3, v13}, Lcom/google/android/gms/appstate/a/d;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v4

    invoke-virtual {v14}, Landroid/content/ContentValues;->clear()V

    move-object v3, v12

    move-object v5, v14

    invoke-virtual/range {v3 .. v9}, Lcom/google/android/gms/appstate/a/a;->a(Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;Ljava/lang/String;ILjava/lang/String;[B)I

    move-result v3

    sparse-switch v3, :sswitch_data_0

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown status code "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :catchall_0
    move-exception v2

    :try_start_5
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    throw v2
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    :catchall_1
    move-exception v2

    :try_start_6
    iget-object v3, v11, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v3}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v2
    :try_end_6
    .catch Lcom/google/android/gms/auth/q; {:try_start_6 .. :try_end_6} :catch_0
    .catch Lcom/google/android/gms/appstate/d/b/a; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :catch_0
    move-exception v2

    :try_start_7
    const-string v3, "AppStateSyncAdapter"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Auth error executing an operation: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p5

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    goto/16 :goto_1

    .line 66
    :catchall_2
    move-exception v2

    invoke-virtual {v11}, Lcom/google/android/gms/appstate/a/e;->a()V

    .line 67
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/appstate/service/d;->a(Z)V

    throw v2

    .line 64
    :sswitch_0
    const/4 v3, 0x1

    :goto_3
    :try_start_8
    invoke-virtual {v14}, Landroid/content/ContentValues;->size()I

    move-result v4

    if-lez v4, :cond_6

    invoke-static/range {v18 .. v19}, Lcom/google/android/gms/appstate/provider/d;->a(J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v14}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    move v10, v3

    goto/16 :goto_2

    :cond_3
    :try_start_9
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_4

    const-string v3, "AppStateAgent"

    invoke-static {v2, v15, v3}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    :cond_4
    if-eqz v10, :cond_5

    move-object/from16 v0, p5

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numIoExceptions:J
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :cond_5
    :try_start_a
    iget-object v2, v11, Lcom/google/android/gms/appstate/a/e;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V
    :try_end_a
    .catch Lcom/google/android/gms/auth/q; {:try_start_a .. :try_end_a} :catch_0
    .catch Lcom/google/android/gms/appstate/d/b/a; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    goto/16 :goto_1

    :catch_1
    move-exception v2

    :try_start_b
    const-string v3, "AppStateSyncAdapter"

    invoke-virtual {v2}, Lcom/google/android/gms/appstate/d/b/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object/from16 v0, p5

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v6, 0x1

    add-long/2addr v4, v6

    iput-wide v4, v2, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_2

    goto/16 :goto_1

    :cond_6
    move v10, v3

    goto/16 :goto_2

    :sswitch_1
    move v3, v10

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_0
        0x3 -> :sswitch_0
        0x7d0 -> :sswitch_1
        0x7d2 -> :sswitch_0
        0x7d3 -> :sswitch_0
        0x7d4 -> :sswitch_0
    .end sparse-switch
.end method
