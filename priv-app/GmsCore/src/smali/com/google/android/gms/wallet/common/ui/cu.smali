.class final Lcom/google/android/gms/wallet/common/ui/cu;
.super Landroid/widget/Filter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/ct;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/ct;)V
    .locals 0

    .prologue
    .line 125
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/cu;->a:Lcom/google/android/gms/wallet/common/ui/ct;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)Ljava/util/ArrayList;
    .locals 9

    .prologue
    const/4 v3, 0x0

    .line 129
    if-nez p1, :cond_0

    .line 130
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 167
    :goto_0
    return-object v0

    .line 132
    :cond_0
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 134
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cu;->a:Lcom/google/android/gms/wallet/common/ui/ct;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ct;->a(Lcom/google/android/gms/wallet/common/ui/ct;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/a/v;

    .line 138
    :try_start_0
    invoke-interface {v0, p1}, Lcom/google/android/gms/wallet/common/a/v;->a(Ljava/lang/CharSequence;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 144
    :goto_1
    if-eqz v0, :cond_1

    .line 145
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_2
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/a/w;

    .line 146
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/a/w;->a()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/a/w;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    .line 148
    :goto_3
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 149
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/a/w;->b()Ljava/lang/CharSequence;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/a/w;->b()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    move-object v4, v1

    .line 153
    :goto_4
    invoke-virtual {v6, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 154
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 155
    invoke-virtual {v6, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashSet;

    invoke-virtual {v1, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 162
    :goto_5
    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 163
    invoke-virtual {v6, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    invoke-virtual {v0, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 139
    :catch_0
    move-exception v1

    .line 140
    const-string v2, "PhoneNumberSourceResult"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Caught exception while fetching phone numbers from "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/a/v;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v3

    .line 142
    goto :goto_1

    :cond_3
    move-object v2, v3

    .line 146
    goto :goto_3

    :cond_4
    move-object v4, v3

    .line 149
    goto :goto_4

    .line 160
    :cond_5
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v6, v2, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_5

    :cond_6
    move-object v0, v5

    .line 167
    goto/16 :goto_0
.end method


# virtual methods
.method public final convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 205
    instance-of v0, p1, Lcom/google/android/gms/wallet/common/a/w;

    if-eqz v0, :cond_0

    .line 206
    check-cast p1, Lcom/google/android/gms/wallet/common/a/w;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/common/a/w;->a()Ljava/lang/CharSequence;

    move-result-object v0

    .line 209
    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 211
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/Filter;->convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method protected final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 2

    .prologue
    .line 175
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/cu;->a(Ljava/lang/CharSequence;)Ljava/util/ArrayList;

    move-result-object v0

    .line 177
    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 178
    iput-object v0, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 179
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 180
    return-object v1
.end method

.method protected final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 186
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cu;->a:Lcom/google/android/gms/wallet/common/ui/ct;

    iput-object p1, v1, Lcom/google/android/gms/wallet/common/ui/ct;->b:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cu;->a:Lcom/google/android/gms/wallet/common/ui/ct;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/ui/ct;->a:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cu;->a:Lcom/google/android/gms/wallet/common/ui/ct;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/ct;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cu;->a:Lcom/google/android/gms/wallet/common/ui/ct;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/ct;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cu;->a:Lcom/google/android/gms/wallet/common/ui/ct;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ct;->notifyDataSetChanged()V

    .line 187
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cu;->a:Lcom/google/android/gms/wallet/common/ui/ct;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ct;->notifyDataSetInvalidated()V

    goto :goto_0
.end method
