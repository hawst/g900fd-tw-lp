.class public final Lcom/google/android/gms/ads/internal/request/a/k;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field final a:Ljava/lang/Object;

.field b:Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:Lcom/google/android/gms/ads/internal/util/j;

.field e:Lcom/google/android/gms/ads/internal/p/a;

.field f:Lcom/google/android/gms/ads/internal/util/a/a;

.field public final g:Lcom/google/android/gms/ads/internal/g/m;

.field public final h:Lcom/google/android/gms/ads/internal/g/m;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/k;->a:Ljava/lang/Object;

    .line 33
    new-instance v0, Lcom/google/android/gms/ads/internal/util/j;

    invoke-direct {v0}, Lcom/google/android/gms/ads/internal/util/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/k;->d:Lcom/google/android/gms/ads/internal/util/j;

    .line 41
    new-instance v0, Lcom/google/android/gms/ads/internal/request/a/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/request/a/l;-><init>(Lcom/google/android/gms/ads/internal/request/a/k;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/k;->g:Lcom/google/android/gms/ads/internal/g/m;

    .line 63
    new-instance v0, Lcom/google/android/gms/ads/internal/request/a/m;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/request/a/m;-><init>(Lcom/google/android/gms/ads/internal/request/a/k;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/k;->h:Lcom/google/android/gms/ads/internal/g/m;

    .line 97
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/request/a/k;->c:Ljava/lang/String;

    .line 98
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/a/k;->b:Ljava/lang/String;

    .line 99
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/ads/internal/util/a/a;
    .locals 1

    .prologue
    .line 111
    const-string v0, "setAdWebView must be called on the main thread."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/request/a/k;->f:Lcom/google/android/gms/ads/internal/util/a/a;

    return-object v0
.end method
