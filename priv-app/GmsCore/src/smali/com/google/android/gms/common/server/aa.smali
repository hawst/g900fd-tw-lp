.class public final Lcom/google/android/gms/common/server/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/common/server/aa;


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()Lcom/google/android/gms/common/server/aa;
    .locals 2

    .prologue
    .line 34
    const-class v1, Lcom/google/android/gms/common/server/aa;

    monitor-enter v1

    .line 35
    :try_start_0
    sget-object v0, Lcom/google/android/gms/common/server/aa;->a:Lcom/google/android/gms/common/server/aa;

    if-nez v0, :cond_0

    .line 36
    new-instance v0, Lcom/google/android/gms/common/server/aa;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/aa;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/server/aa;->a:Lcom/google/android/gms/common/server/aa;

    .line 38
    :cond_0
    sget-object v0, Lcom/google/android/gms/common/server/aa;->a:Lcom/google/android/gms/common/server/aa;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 39
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    monitor-enter p0

    .line 57
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/server/aa;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 58
    invoke-static {p1}, Lcom/google/android/gms/common/util/e;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/server/aa;->b:Ljava/lang/String;

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/server/aa;->b:Ljava/lang/String;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/util/HashMap;Ljava/lang/String;Ljava/lang/Long;)V
    .locals 2

    .prologue
    .line 50
    const-string v0, "X-Container-Url"

    invoke-virtual {p2, v0, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    const-string v0, "X-Network-ID"

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/server/aa;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 52
    const-string v1, "X-Auth-Time"

    if-nez p4, :cond_0

    const-string v0, "none"

    :goto_0
    invoke-virtual {p2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    return-void

    .line 52
    :cond_0
    invoke-virtual {p4}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
