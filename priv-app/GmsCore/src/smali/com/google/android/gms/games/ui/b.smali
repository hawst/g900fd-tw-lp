.class public abstract Lcom/google/android/gms/games/ui/b;
.super Lcom/google/android/gms/games/ui/o;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final a:Landroid/content/Context;

.field protected b:I

.field protected c:Lcom/google/android/gms/common/data/d;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:Z

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Landroid/view/View;

.field private k:Z

.field private l:Landroid/view/View;

.field private m:Landroid/view/View;

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:I

.field private t:Lcom/google/android/gms/games/ui/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/o;-><init>()V

    .line 35
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->g:Z

    .line 39
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->k:Z

    .line 43
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/b;->n:Z

    .line 45
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->o:Z

    .line 46
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->p:Z

    .line 47
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->q:Z

    .line 51
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/b;->r:Z

    .line 59
    iput v0, p0, Lcom/google/android/gms/games/ui/b;->s:I

    .line 65
    iput v0, p0, Lcom/google/android/gms/games/ui/b;->b:I

    .line 83
    iput-object p1, p0, Lcom/google/android/gms/games/ui/b;->a:Landroid/content/Context;

    .line 84
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 87
    invoke-static {v0}, Lcom/google/android/gms/games/ui/b;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/b;->h:Landroid/view/View;

    .line 88
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b;->h:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->pR:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/b;->i:Landroid/view/View;

    .line 89
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b;->h:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->qs:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/b;->j:Landroid/view/View;

    .line 90
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b;->j:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 91
    invoke-static {v0}, Lcom/google/android/gms/games/ui/b;->a(Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/b;->d:Landroid/view/View;

    .line 92
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b;->h:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->pR:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/b;->e:Landroid/view/View;

    .line 93
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b;->h:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->qs:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/b;->f:Landroid/view/View;

    .line 94
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b;->f:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 95
    sget v1, Lcom/google/android/gms/l;->ba:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/b;->l:Landroid/view/View;

    .line 96
    return-void
.end method

.method private static a(Landroid/view/LayoutInflater;)Landroid/view/View;
    .locals 3

    .prologue
    .line 105
    sget v0, Lcom/google/android/gms/l;->bn:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 451
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->p:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)Z
    .locals 1

    .prologue
    .line 455
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->o:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/b;->s:I

    add-int/lit8 v0, v0, -0x1

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(I)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 585
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->t:Lcom/google/android/gms/games/ui/c;

    if-eqz v0, :cond_0

    .line 587
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->t:Lcom/google/android/gms/games/ui/c;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/c;->G_()V

    .line 588
    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "pageDirection needs to be NEXT or PREV"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/b;->g:Z

    .line 598
    :goto_0
    return-void

    .line 588
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->i:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/b;->k:Z

    goto :goto_0

    .line 595
    :cond_0
    const-string v0, "DataBufferAdapter"

    const-string v1, "Reached the end of a paginated DataBuffer, but no OnEndOfWindowReachedListener registered!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 588
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private g()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->c:Lcom/google/android/gms/common/data/d;

    if-nez v0, :cond_1

    .line 180
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/b;->p:Z

    .line 181
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/b;->o:Z

    .line 182
    iput v2, p0, Lcom/google/android/gms/games/ui/b;->s:I

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->r:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->c:Lcom/google/android/gms/common/data/d;

    invoke-static {v0}, Lcom/google/android/gms/common/data/k;->b(Lcom/google/android/gms/common/data/d;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->p:Z

    .line 185
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->r:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->c:Lcom/google/android/gms/common/data/d;

    invoke-static {v0}, Lcom/google/android/gms/common/data/k;->a(Lcom/google/android/gms/common/data/d;)Z

    move-result v0

    if-eqz v0, :cond_4

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/b;->o:Z

    .line 187
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b;->b()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/b;->s:I

    .line 188
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->p:Z

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/games/ui/b;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/b;->s:I

    .line 189
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->o:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/b;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/b;->s:I

    goto :goto_0

    :cond_3
    move v0, v2

    .line 184
    goto :goto_1

    :cond_4
    move v1, v2

    .line 185
    goto :goto_2
.end method

.method private h()Z
    .locals 1

    .prologue
    .line 377
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->q:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->m:Landroid/view/View;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/games/ui/b;->s:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->c:Lcom/google/android/gms/common/data/d;

    iget v1, p0, Lcom/google/android/gms/games/ui/b;->b:I

    add-int/2addr v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 542
    if-nez p2, :cond_0

    .line 543
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b;->a:Landroid/content/Context;

    invoke-virtual {p0, v1, p3}, Lcom/google/android/gms/games/ui/b;->a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 548
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b;->a:Landroid/content/Context;

    invoke-virtual {p0, p2, v0}, Lcom/google/android/gms/games/ui/b;->a(Landroid/view/View;Ljava/lang/Object;)V

    .line 549
    return-object p2
.end method

.method public abstract a(Landroid/content/Context;Landroid/view/ViewGroup;)Landroid/view/View;
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 149
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/b;->a(Lcom/google/android/gms/common/data/d;)V

    .line 150
    return-void
.end method

.method public final a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 370
    iput-object p1, p0, Lcom/google/android/gms/games/ui/b;->m:Landroid/view/View;

    .line 371
    iget v0, p0, Lcom/google/android/gms/games/ui/b;->s:I

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->q:Z

    if-nez v0, :cond_0

    .line 372
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b;->notifyDataSetChanged()V

    .line 374
    :cond_0
    return-void
.end method

.method public abstract a(Landroid/view/View;Ljava/lang/Object;)V
.end method

.method public a(Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->c:Lcom/google/android/gms/common/data/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->c:Lcom/google/android/gms/common/data/d;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    :goto_0
    return-void

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->c:Lcom/google/android/gms/common/data/d;

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->c:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->w_()V

    .line 165
    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/games/ui/b;->c:Lcom/google/android/gms/common/data/d;

    .line 166
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b;->g()V

    .line 167
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->q:Z

    .line 168
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/ui/c;)V
    .locals 0

    .prologue
    .line 399
    iput-object p1, p0, Lcom/google/android/gms/games/ui/b;->t:Lcom/google/android/gms/games/ui/c;

    .line 400
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 386
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->n:Z

    if-eq v0, p1, :cond_0

    .line 387
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/b;->n:Z

    .line 388
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b;->notifyDataSetChanged()V

    .line 390
    :cond_0
    return-void
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 554
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->q:Z

    if-eqz v0, :cond_0

    .line 555
    const/4 v0, 0x0

    .line 566
    :goto_0
    return v0

    .line 557
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 558
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    goto :goto_0

    .line 566
    :cond_1
    invoke-super {p0}, Lcom/google/android/gms/games/ui/o;->areAllItemsEnabled()Z

    move-result v0

    goto :goto_0
.end method

.method public b()I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 258
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b;->c:Lcom/google/android/gms/common/data/d;

    if-nez v1, :cond_1

    .line 263
    :cond_0
    :goto_0
    return v0

    .line 261
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b;->c:Lcom/google/android/gms/common/data/d;

    invoke-interface {v1}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v1

    iget v2, p0, Lcom/google/android/gms/games/ui/b;->b:I

    sub-int/2addr v1, v2

    .line 262
    if-ltz v1, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/common/data/d;
    .locals 1

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->c:Lcom/google/android/gms/common/data/d;

    return-object v0
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 308
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->q:Z

    if-eq v1, v0, :cond_0

    .line 309
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/b;->q:Z

    .line 310
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b;->notifyDataSetChanged()V

    .line 312
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->i:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->j:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 352
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->k:Z

    .line 353
    return-void
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 404
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->n:Z

    if-nez v0, :cond_0

    .line 406
    const/4 v0, 0x0

    .line 413
    :goto_0
    return v0

    .line 408
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->q:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 411
    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    .line 413
    :cond_2
    iget v0, p0, Lcom/google/android/gms/games/ui/b;->s:I

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 418
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/b;->q:Z

    if-eqz v1, :cond_1

    .line 447
    :cond_0
    :goto_0
    return-object v0

    .line 422
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b;->h()Z

    move-result v1

    if-nez v1, :cond_0

    .line 426
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b;->c:Lcom/google/android/gms/common/data/d;

    if-eqz v1, :cond_0

    .line 429
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/b;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 433
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/b;->p:Z

    if-eqz v1, :cond_2

    .line 434
    if-eqz p1, :cond_0

    .line 439
    add-int/lit8 p1, p1, -0x1

    .line 447
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->c:Lcom/google/android/gms/common/data/d;

    iget v1, p0, Lcom/google/android/gms/games/ui/b;->b:I

    add-int/2addr v1, p1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    .prologue
    .line 460
    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 475
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->q:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    .line 479
    :goto_0
    return v0

    .line 476
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x4

    goto :goto_0

    .line 477
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/b;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    .line 478
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/b;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x2

    goto :goto_0

    .line 479
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 484
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->q:Z

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->l:Landroid/view/View;

    .line 519
    :goto_0
    return-object v0

    .line 488
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 489
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->m:Landroid/view/View;

    goto :goto_0

    .line 491
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/b;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 493
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->k:Z

    if-nez v0, :cond_2

    .line 496
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/b;->c(I)V

    .line 498
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->h:Landroid/view/View;

    goto :goto_0

    .line 500
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->p:Z

    if-eqz v0, :cond_6

    .line 501
    if-nez p1, :cond_5

    .line 502
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/b;->g:Z

    if-nez v0, :cond_4

    .line 506
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/b;->c(I)V

    .line 508
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->d:Landroid/view/View;

    goto :goto_0

    .line 511
    :cond_5
    add-int/lit8 p1, p1, -0x1

    .line 515
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->h:Landroid/view/View;

    if-eq p2, v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->d:Landroid/view/View;

    if-ne p2, v0, :cond_8

    .line 516
    :cond_7
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "trying to convert header/footer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 519
    :cond_8
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/b;->a(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 470
    const/4 v0, 0x5

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 465
    const/4 v0, 0x0

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 571
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/b;->q:Z

    if-eqz v1, :cond_1

    .line 581
    :cond_0
    :goto_0
    return v0

    .line 574
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b;->h()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 575
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->m:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isEnabled()Z

    move-result v0

    goto :goto_0

    .line 577
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/b;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/b;->b(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 581
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/o;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 286
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b;->g()V

    .line 287
    invoke-super {p0}, Lcom/google/android/gms/games/ui/o;->notifyDataSetChanged()V

    .line 288
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->j:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 603
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/b;->c(I)V

    .line 607
    :cond_0
    :goto_0
    return-void

    .line 604
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/b;->j:Landroid/view/View;

    if-ne p1, v0, :cond_0

    .line 605
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/b;->c(I)V

    goto :goto_0
.end method
