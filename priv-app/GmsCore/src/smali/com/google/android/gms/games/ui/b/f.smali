.class public final Lcom/google/android/gms/games/ui/b/f;
.super Lcom/google/android/gms/games/ui/b/b;
.source "SourceFile"


# instance fields
.field private j:I

.field private k:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 12
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b/b;-><init>()V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/b/f;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 26
    new-instance v0, Lcom/google/android/gms/games/ui/b/f;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/b/f;-><init>()V

    iput v3, v0, Lcom/google/android/gms/games/ui/b/f;->j:I

    iput v3, v0, Lcom/google/android/gms/games/ui/b/f;->k:I

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "message"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "isCancelable"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "isIndeterminate"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v2, "max"

    const/16 v3, 0x64

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/b/f;->setArguments(Landroid/os/Bundle;)V

    return-object v0
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 58
    if-eqz p1, :cond_0

    .line 59
    const-string v0, "progress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/b/f;->j:I

    .line 60
    const-string v0, "secondaryProgress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/b/f;->k:I

    .line 63
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b/f;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 64
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->ky:I

    invoke-static {v1, v2}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    .line 66
    new-instance v2, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b/f;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    .line 67
    const-string v3, "message"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    .line 68
    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setCustomTitle(Landroid/view/View;)V

    .line 69
    const-string v1, "isIndeterminate"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    .line 70
    const-string v1, "max"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setMax(I)V

    .line 71
    iget v1, p0, Lcom/google/android/gms/games/ui/b/f;->j:I

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setProgress(I)V

    .line 72
    iget v1, p0, Lcom/google/android/gms/games/ui/b/f;->k:I

    invoke-virtual {v2, v1}, Landroid/app/ProgressDialog;->setSecondaryProgress(I)V

    .line 73
    const-string v1, "isCancelable"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/b/f;->a(Z)V

    .line 74
    return-object v2
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 79
    const-string v0, "progress"

    iget v1, p0, Lcom/google/android/gms/games/ui/b/f;->j:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 80
    const-string v0, "secondaryProgress"

    iget v1, p0, Lcom/google/android/gms/games/ui/b/f;->k:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 81
    return-void
.end method
