.class public final Lcom/google/android/gms/plus/service/a/as;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:[Ljava/lang/String;

.field private final f:[Ljava/lang/String;

.field private final g:I

.field private final h:[Ljava/lang/String;

.field private final i:Lcom/google/android/gms/common/internal/bg;

.field private final j:Ljava/lang/String;

.field private final k:Lcom/google/android/gms/plus/service/f;

.field private final l:Lcom/google/android/gms/plus/service/a/b;

.field private final m:Lcom/google/android/gms/plus/internal/PlusCommonExtras;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;I[Ljava/lang/String;Lcom/google/android/gms/common/internal/bg;Ljava/lang/String;Lcom/google/android/gms/plus/service/f;Lcom/google/android/gms/plus/service/a/b;Lcom/google/android/gms/plus/internal/PlusCommonExtras;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/as;->a:Ljava/lang/String;

    .line 97
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    .line 98
    iput-object p3, p0, Lcom/google/android/gms/plus/service/a/as;->c:Ljava/lang/String;

    .line 99
    iput-object p4, p0, Lcom/google/android/gms/plus/service/a/as;->d:Ljava/lang/String;

    .line 100
    iput-object p5, p0, Lcom/google/android/gms/plus/service/a/as;->e:[Ljava/lang/String;

    .line 101
    iput-object p6, p0, Lcom/google/android/gms/plus/service/a/as;->f:[Ljava/lang/String;

    .line 102
    iput p7, p0, Lcom/google/android/gms/plus/service/a/as;->g:I

    .line 103
    iput-object p9, p0, Lcom/google/android/gms/plus/service/a/as;->i:Lcom/google/android/gms/common/internal/bg;

    .line 104
    iput-object p8, p0, Lcom/google/android/gms/plus/service/a/as;->h:[Ljava/lang/String;

    .line 105
    iput-object p10, p0, Lcom/google/android/gms/plus/service/a/as;->j:Ljava/lang/String;

    .line 106
    iput-object p11, p0, Lcom/google/android/gms/plus/service/a/as;->k:Lcom/google/android/gms/plus/service/f;

    .line 107
    iput-object p12, p0, Lcom/google/android/gms/plus/service/a/as;->l:Lcom/google/android/gms/plus/service/a/b;

    .line 108
    iput-object p13, p0, Lcom/google/android/gms/plus/service/a/as;->m:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    .line 109
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/c;
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    .line 120
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->k:Lcom/google/android/gms/plus/service/f;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/as;->c:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/plus/service/a/as;->g:I

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->j()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/plus/service/a/as;->f:[Ljava/lang/String;

    move-object v1, p1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/plus/service/f;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/c;
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 154
    :goto_0
    return-object v0

    .line 124
    :catch_0
    move-exception v0

    .line 127
    new-instance v1, Lcom/google/android/gms/plus/n;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/plus/n;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/plus/n;->c:Landroid/content/Intent;

    iput v8, v1, Lcom/google/android/gms/plus/n;->b:I

    .line 130
    const-string v0, "<<default account>>"

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 133
    iput-boolean v7, v1, Lcom/google/android/gms/plus/n;->a:Z

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-virtual {v1}, Lcom/google/android/gms/plus/n;->a()Landroid/content/Intent;

    move-result-object v1

    invoke-static {p1, v0, v2, v1}, Lcom/google/android/gms/plus/service/g;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lcom/google/android/gms/common/c;

    move-result-object v0

    goto :goto_0

    .line 139
    :catch_1
    move-exception v0

    new-instance v0, Lcom/google/android/gms/plus/n;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/plus/n;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iput v8, v0, Lcom/google/android/gms/plus/n;->b:I

    invoke-virtual {v0}, Lcom/google/android/gms/plus/n;->a()Landroid/content/Intent;

    move-result-object v0

    .line 142
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    const/16 v2, 0x8

    invoke-static {p1, v1, v2, v0}, Lcom/google/android/gms/plus/service/g;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lcom/google/android/gms/common/c;

    move-result-object v0

    goto :goto_0

    .line 148
    :catch_2
    move-exception v0

    new-instance v0, Lcom/google/android/gms/plus/n;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/plus/n;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 149
    const-string v1, "<<default account>>"

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 152
    iput-boolean v7, v0, Lcom/google/android/gms/plus/n;->a:Z

    .line 154
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0}, Lcom/google/android/gms/plus/n;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v1, v2, v0}, Lcom/google/android/gms/plus/service/g;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lcom/google/android/gms/common/c;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 6

    .prologue
    .line 322
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/as;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    move-object v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 325
    sget-object v1, Lcom/google/android/gms/plus/service/f;->b:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    .line 326
    const-string v1, "application_name"

    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/as;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->m:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->l()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/os/Bundle;)V

    .line 329
    return-object v0
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 6

    .prologue
    .line 334
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    iget v1, p0, Lcom/google/android/gms/plus/service/a/as;->g:I

    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/as;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/service/a/as;->c:Ljava/lang/String;

    move-object v3, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 336
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->e:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    .line 337
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->f:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->b([Ljava/lang/String;)V

    .line 338
    const-string v1, "application_name"

    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/as;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 339
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->m:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->l()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->a(Landroid/os/Bundle;)V

    .line 341
    return-object v0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 346
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Lcom/google/android/gms/common/c;Landroid/os/IBinder;[B)V

    .line 347
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/c;Landroid/os/IBinder;[B)V
    .locals 4

    .prologue
    .line 352
    invoke-virtual {p2}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    .line 353
    if-eqz v0, :cond_0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/common/c;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 356
    const-string v1, "ValidateAccountOperatio"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "no resolution provided for status "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 357
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "no resolution provided!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 360
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "<<default account>>"

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 362
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->l:Lcom/google/android/gms/plus/service/a/b;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/plus/service/a/b;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 365
    :cond_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 366
    invoke-virtual {p2}, Lcom/google/android/gms/common/c;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 367
    const-string v1, "pendingIntent"

    invoke-virtual {p2}, Lcom/google/android/gms/common/c;->d()Landroid/app/PendingIntent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 370
    :cond_2
    if-eqz p4, :cond_3

    .line 371
    const-string v1, "loaded_person"

    invoke-virtual {v0, v1, p4}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 374
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->i:Lcom/google/android/gms/common/internal/bg;

    invoke-virtual {p2}, Lcom/google/android/gms/common/c;->c()I

    move-result v2

    invoke-interface {v1, v2, p3, v0}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 375
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/b/b;)[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 489
    const/4 v0, 0x0

    .line 491
    :try_start_0
    invoke-virtual {p3, p1, p2}, Lcom/google/android/gms/plus/b/b;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)[B
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 525
    :goto_0
    return-object v0

    .line 492
    :catch_0
    move-exception v0

    .line 502
    new-instance v1, Lcom/google/android/gms/plus/n;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/plus/n;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/plus/n;->c:Landroid/content/Intent;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/n;->a()Landroid/content/Intent;

    move-result-object v0

    .line 505
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    const/4 v2, 0x6

    invoke-static {p1, v1, v2, v0}, Lcom/google/android/gms/plus/service/g;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lcom/google/android/gms/common/c;

    move-result-object v0

    .line 507
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Lcom/google/android/gms/common/c;)V

    .line 508
    new-array v0, v3, [B

    goto :goto_0

    .line 510
    :catch_1
    move-exception v0

    new-instance v0, Lcom/google/android/gms/plus/n;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/plus/n;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 512
    const-string v1, "<<default account>>"

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 515
    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/plus/n;->a:Z

    .line 517
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-virtual {v0}, Lcom/google/android/gms/plus/n;->a()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v1, v2, v0}, Lcom/google/android/gms/plus/service/g;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lcom/google/android/gms/common/c;

    move-result-object v0

    .line 519
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Lcom/google/android/gms/common/c;)V

    .line 520
    new-array v0, v3, [B

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 11

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->l:Lcom/google/android/gms/plus/service/a/b;

    invoke-interface {v0, p1}, Lcom/google/android/gms/plus/service/a/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/plus/service/g;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lcom/google/android/gms/common/c;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Lcom/google/android/gms/common/c;)V

    .line 236
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->l:Lcom/google/android/gms/plus/service/a/b;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/plus/service/a/b;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->e:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->e:[Ljava/lang/String;

    array-length v0, v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_5

    const-string v0, "plus_one_placeholder_scope"

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->e:[Ljava/lang/String;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->e:[Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->e:[Ljava/lang/String;

    array-length v1, v1

    if-eqz v1, :cond_3

    :cond_2
    if-eqz v0, :cond_6

    :cond_3
    sget-object v8, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->k:Lcom/google/android/gms/plus/service/f;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->j:Ljava/lang/String;

    invoke-direct {p0, p1, v7}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    invoke-direct {p0, v7}, Lcom/google/android/gms/plus/service/a/as;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/plus/service/i;

    invoke-direct {v5}, Lcom/google/android/gms/plus/service/i;-><init>()V

    iget-object v6, p0, Lcom/google/android/gms/plus/service/a/as;->f:[Ljava/lang/String;

    move-object v2, p1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/plus/service/f;->a(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/service/i;[Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v8, v0, v1}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Lcom/google/android/gms/common/c;Landroid/os/IBinder;[B)V

    const/4 v0, 0x1

    :goto_2
    if-nez v0, :cond_0

    .line 174
    new-instance v5, Lcom/google/android/gms/plus/service/a/at;

    invoke-direct {v5, v7}, Lcom/google/android/gms/plus/service/a/at;-><init>(Ljava/lang/String;)V

    if-nez v7, :cond_4

    const-string v0, "<<default account>>"

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    const/4 v1, 0x5

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/plus/service/g;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lcom/google/android/gms/common/c;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Lcom/google/android/gms/common/c;Landroid/os/IBinder;[B)V

    const/4 v0, 0x1

    iput-boolean v0, v5, Lcom/google/android/gms/plus/service/a/at;->c:Z

    .line 176
    :cond_4
    :goto_3
    iget-boolean v0, v5, Lcom/google/android/gms/plus/service/a/at;->c:Z

    if-nez v0, :cond_0

    .line 179
    iget-object v2, v5, Lcom/google/android/gms/plus/service/a/at;->a:Ljava/lang/String;

    .line 185
    invoke-direct {p0, p1, v2}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    .line 188
    invoke-direct {p0, v2}, Lcom/google/android/gms/plus/service/a/as;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v4

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->k:Lcom/google/android/gms/plus/service/f;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->h:[Ljava/lang/String;

    invoke-interface {v0, p1, v3, v4, v1}, Lcom/google/android/gms/plus/service/f;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Lcom/google/android/gms/common/c;

    move-result-object v0

    .line 193
    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->c()I

    move-result v1

    if-eqz v1, :cond_e

    .line 194
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Lcom/google/android/gms/common/c;)V

    goto/16 :goto_0

    .line 170
    :cond_5
    const/4 v0, 0x0

    goto :goto_1

    :cond_6
    const/4 v0, 0x0

    goto :goto_2

    .line 174
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->k:Lcom/google/android/gms/plus/service/f;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/service/f;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->l:Lcom/google/android/gms/plus/service/a/b;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/plus/service/a/b;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_9

    const/4 v0, 0x1

    :goto_4
    if-nez v0, :cond_a

    const/4 v2, 0x0

    :cond_8
    :goto_5
    iput-object v2, v5, Lcom/google/android/gms/plus/service/a/at;->a:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, v5, Lcom/google/android/gms/plus/service/a/at;->b:Z

    iget-object v0, v5, Lcom/google/android/gms/plus/service/a/at;->a:Ljava/lang/String;

    if-nez v0, :cond_4

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/service/a/as;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/plus/n;

    invoke-direct {v1, p1, v0}, Lcom/google/android/gms/plus/n;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    const/4 v0, 0x1

    iput-boolean v0, v1, Lcom/google/android/gms/plus/n;->a:Z

    const/4 v0, 0x2

    iput v0, v1, Lcom/google/android/gms/plus/n;->b:I

    invoke-virtual {v1}, Lcom/google/android/gms/plus/n;->a()Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    const/4 v2, 0x4

    invoke-static {p1, v1, v2, v0}, Lcom/google/android/gms/plus/service/g;->a(Landroid/content/Context;Ljava/lang/String;ILandroid/content/Intent;)Lcom/google/android/gms/common/c;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Lcom/google/android/gms/common/c;Landroid/os/IBinder;[B)V

    const/4 v0, 0x1

    iput-boolean v0, v5, Lcom/google/android/gms/plus/service/a/at;->c:Z

    goto :goto_3

    :cond_9
    const/4 v0, 0x0

    goto :goto_4

    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->l:Lcom/google/android/gms/plus/service/a/b;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/plus/service/a/b;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    const/4 v2, 0x0

    const/4 v1, 0x0

    const/4 v0, 0x0

    move v3, v0

    :goto_6
    if-ge v3, v6, :cond_c

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/service/a/as;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v7

    invoke-direct {p0, p1, v7}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/c;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/common/c;->c()I

    move-result v7

    sparse-switch v7, :sswitch_data_0

    move v0, v1

    move-object v1, v2

    :goto_7
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v1

    move v1, v0

    goto :goto_6

    :sswitch_0
    if-nez v2, :cond_b

    move v10, v1

    move-object v1, v0

    move v0, v10

    goto :goto_7

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->l:Lcom/google/android/gms/plus/service/a/b;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/plus/service/a/b;->c(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v2, 0x0

    goto :goto_5

    :sswitch_1
    const/4 v0, 0x1

    move-object v1, v2

    goto :goto_7

    :cond_c
    if-eqz v1, :cond_d

    if-eqz v2, :cond_8

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->l:Lcom/google/android/gms/plus/service/a/b;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/plus/service/a/b;->c(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_5

    .line 199
    :cond_e
    invoke-direct {p0, p1, v4}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/c;

    move-result-object v0

    .line 200
    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->b()Z

    move-result v1

    if-nez v1, :cond_f

    .line 201
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Lcom/google/android/gms/common/c;)V

    goto/16 :goto_0

    .line 216
    :cond_f
    invoke-virtual {v4}, Lcom/google/android/gms/common/server/ClientContext;->a()I

    move-result v0

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->a()I

    move-result v1

    if-ne v0, v1, :cond_15

    const/4 v0, 0x1

    move v1, v0

    .line 217
    :goto_8
    const/4 v0, 0x0

    .line 219
    if-nez v1, :cond_18

    iget-object v6, p0, Lcom/google/android/gms/plus/service/a/as;->e:[Ljava/lang/String;

    array-length v7, v6

    const/4 v1, 0x0

    :goto_9
    if-ge v1, v7, :cond_17

    aget-object v8, v6, v1

    const-string v9, "profile"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_10

    const-string v9, "https://www.googleapis.com/auth/userinfo.profile"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_10

    const-string v9, "https://www.googleapis.com/auth/plus.me"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_10

    const-string v9, "https://www.googleapis.com/auth/plus.login"

    invoke-virtual {v9, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_16

    :cond_10
    const/4 v1, 0x1

    :goto_a
    if-eqz v1, :cond_18

    .line 220
    invoke-direct {p0, p1, v4, p2}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/b/b;)[B

    move-result-object v0

    .line 221
    if-eqz v0, :cond_11

    array-length v1, v0

    if-eqz v1, :cond_0

    :cond_11
    move-object v7, v0

    .line 226
    :goto_b
    iget-boolean v0, v5, Lcom/google/android/gms/plus/service/a/at;->b:Z

    if-eqz v0, :cond_14

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->l:Lcom/google/android/gms/plus/service/a/b;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/plus/service/a/b;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 228
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v5, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v5

    if-eqz v5, :cond_12

    invoke-virtual {v1, v5}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_12
    :goto_c
    if-nez v0, :cond_13

    sget v0, Lcom/google/android/gms/p;->vS:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_13
    sget v1, Lcom/google/android/gms/p;->tl:I

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object v2, v5, v0

    invoke-virtual {p1, v1, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v5

    invoke-direct {v1, v5}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v5, Lcom/google/android/gms/plus/service/a/au;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    invoke-direct {v5, p1, v0, v2, v6}, Lcom/google/android/gms/plus/service/a/au;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 231
    :cond_14
    sget-object v8, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->k:Lcom/google/android/gms/plus/service/f;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->j:Ljava/lang/String;

    new-instance v5, Lcom/google/android/gms/plus/service/i;

    invoke-direct {v5}, Lcom/google/android/gms/plus/service/i;-><init>()V

    iget-object v6, p0, Lcom/google/android/gms/plus/service/a/as;->f:[Ljava/lang/String;

    move-object v2, p1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/plus/service/f;->a(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/service/i;[Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v0

    invoke-direct {p0, p1, v8, v0, v7}, Lcom/google/android/gms/plus/service/a/as;->a(Landroid/content/Context;Lcom/google/android/gms/common/c;Landroid/os/IBinder;[B)V

    goto/16 :goto_0

    .line 216
    :cond_15
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/as;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    move v1, v0

    goto/16 :goto_8

    .line 219
    :cond_16
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_9

    :cond_17
    const/4 v1, 0x0

    goto :goto_a

    :catch_0
    move-exception v1

    goto :goto_c

    :cond_18
    move-object v7, v0

    goto :goto_b

    .line 174
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x7 -> :sswitch_1
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 379
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->i:Lcom/google/android/gms/common/internal/bg;

    if-eqz v0, :cond_0

    .line 380
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/as;->i:Lcom/google/android/gms/common/internal/bg;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 382
    :cond_0
    return-void
.end method
