.class public final Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/deviceconnection/a/a;

.field private b:Lcom/google/android/gms/deviceconnection/service/c;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 188
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;)Lcom/google/android/gms/deviceconnection/service/c;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;->b:Lcom/google/android/gms/deviceconnection/service/c;

    return-object v0
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 63
    const-string v0, "com.google.android.gms.deviceconnection.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    new-instance v0, Lcom/google/android/gms/deviceconnection/service/d;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/deviceconnection/service/d;-><init>(Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/deviceconnection/service/d;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 66
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onCreate()V
    .locals 2

    .prologue
    .line 48
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 50
    invoke-static {p0}, Lcom/google/android/gms/deviceconnection/a/a;->a(Landroid/content/Context;)Lcom/google/android/gms/deviceconnection/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;->a:Lcom/google/android/gms/deviceconnection/a/a;

    .line 51
    new-instance v0, Lcom/google/android/gms/deviceconnection/service/c;

    iget-object v1, p0, Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;->a:Lcom/google/android/gms/deviceconnection/a/a;

    invoke-direct {v0, v1}, Lcom/google/android/gms/deviceconnection/service/c;-><init>(Lcom/google/android/gms/deviceconnection/a/a;)V

    iput-object v0, p0, Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;->b:Lcom/google/android/gms/deviceconnection/service/c;

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;->b:Lcom/google/android/gms/deviceconnection/service/c;

    invoke-virtual {v0}, Lcom/google/android/gms/deviceconnection/service/c;->start()V

    .line 53
    return-void
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/service/DeviceConnectionServiceBroker;->b:Lcom/google/android/gms/deviceconnection/service/c;

    invoke-virtual {v0}, Lcom/google/android/gms/deviceconnection/service/c;->a()V

    .line 58
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 59
    return-void
.end method
