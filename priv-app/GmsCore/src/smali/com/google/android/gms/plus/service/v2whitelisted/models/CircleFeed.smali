.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/f;

.field private static final j:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Ljava/util/List;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/f;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/f;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/f;

    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 78
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->j:Ljava/util/HashMap;

    const-string v1, "etag"

    const-string v2, "etag"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->j:Ljava/util/HashMap;

    const-string v1, "items"

    const-string v2, "items"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleEntity;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->j:Ljava/util/HashMap;

    const-string v1, "nextLink"

    const-string v2, "nextLink"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->j:Ljava/util/HashMap;

    const-string v1, "nextPageToken"

    const-string v2, "nextPageToken"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->j:Ljava/util/HashMap;

    const-string v1, "selfLink"

    const-string v2, "selfLink"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->j:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "title"

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->j:Ljava/util/HashMap;

    const-string v1, "totalItems"

    const-string v2, "totalItems"

    const/16 v3, 0x9

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 149
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 150
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->b:I

    .line 151
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->a:Ljava/util/Set;

    .line 152
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 166
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->a:Ljava/util/Set;

    .line 167
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->b:I

    .line 168
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->c:Ljava/lang/String;

    .line 169
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->d:Ljava/util/List;

    .line 170
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->e:Ljava/lang/String;

    .line 171
    iput-object p6, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->f:Ljava/lang/String;

    .line 172
    iput-object p7, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->g:Ljava/lang/String;

    .line 173
    iput-object p8, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->h:Ljava/lang/String;

    .line 174
    iput p9, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->i:I

    .line 175
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->j:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 367
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 368
    packed-switch v0, :pswitch_data_0

    .line 373
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be an int."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 370
    :pswitch_0
    iput p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->i:I

    .line 376
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 377
    return-void

    .line 368
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 382
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 383
    packed-switch v0, :pswitch_data_0

    .line 400
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 385
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->c:Ljava/lang/String;

    .line 403
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 404
    return-void

    .line 388
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->e:Ljava/lang/String;

    goto :goto_0

    .line 391
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->f:Ljava/lang/String;

    goto :goto_0

    .line 394
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->g:Ljava/lang/String;

    goto :goto_0

    .line 397
    :pswitch_5
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->h:Ljava/lang/String;

    goto :goto_0

    .line 383
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 409
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 410
    packed-switch v0, :pswitch_data_0

    .line 415
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known array of custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 412
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->d:Ljava/util/List;

    .line 419
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 420
    return-void

    .line 410
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 331
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 347
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 333
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->c:Ljava/lang/String;

    .line 345
    :goto_0
    return-object v0

    .line 335
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->d:Ljava/util/List;

    goto :goto_0

    .line 337
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->e:Ljava/lang/String;

    goto :goto_0

    .line 339
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->f:Ljava/lang/String;

    goto :goto_0

    .line 341
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->g:Ljava/lang/String;

    goto :goto_0

    .line 343
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->h:Ljava/lang/String;

    goto :goto_0

    .line 345
    :pswitch_7
    iget v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->i:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 331
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->d:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 316
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/f;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 437
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;

    if-nez v0, :cond_0

    move v0, v1

    .line 468
    :goto_0
    return v0

    .line 442
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 443
    goto :goto_0

    .line 446
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;

    .line 447
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 448
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 449
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 451
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 453
    goto :goto_0

    :cond_3
    move v0, v1

    .line 458
    goto :goto_0

    .line 461
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 463
    goto :goto_0

    :cond_5
    move v0, v2

    .line 468
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 424
    const/4 v0, 0x0

    .line 425
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->j:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 426
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 427
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 428
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 430
    goto :goto_0

    .line 431
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 321
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/f;

    invoke-static {p0, p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/f;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/CircleFeed;Landroid/os/Parcel;)V

    .line 322
    return-void
.end method
