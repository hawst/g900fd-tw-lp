.class final Lcom/google/android/gms/cast/media/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/google/android/gms/cast/media/d;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/media/d;I)V
    .locals 0

    .prologue
    .line 751
    iput-object p1, p0, Lcom/google/android/gms/cast/media/i;->b:Lcom/google/android/gms/cast/media/d;

    iput p2, p0, Lcom/google/android/gms/cast/media/i;->a:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 754
    iget-object v0, p0, Lcom/google/android/gms/cast/media/i;->b:Lcom/google/android/gms/cast/media/d;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/d;->a:Lcom/google/android/gms/cast/media/y;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/y;->a()Z

    move-result v0

    if-nez v0, :cond_4

    .line 755
    iget-object v0, p0, Lcom/google/android/gms/cast/media/i;->b:Lcom/google/android/gms/cast/media/d;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/d;->a:Lcom/google/android/gms/cast/media/y;

    iput-boolean v3, v0, Lcom/google/android/gms/cast/media/y;->c:Z

    .line 756
    iget-object v0, p0, Lcom/google/android/gms/cast/media/i;->b:Lcom/google/android/gms/cast/media/d;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/d;->a:Lcom/google/android/gms/cast/media/y;

    iput-boolean v2, v0, Lcom/google/android/gms/cast/media/y;->d:Z

    .line 758
    iget v0, p0, Lcom/google/android/gms/cast/media/i;->a:I

    if-nez v0, :cond_0

    .line 760
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/i;->b:Lcom/google/android/gms/cast/media/d;

    iget-object v1, v1, Lcom/google/android/gms/cast/media/d;->a:Lcom/google/android/gms/cast/media/y;

    iget-object v1, v1, Lcom/google/android/gms/cast/media/y;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 763
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/o;

    .line 764
    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/o;->i()V

    goto :goto_0

    .line 769
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/cast/media/i;->b:Lcom/google/android/gms/cast/media/d;

    iget-object v1, v1, Lcom/google/android/gms/cast/media/d;->a:Lcom/google/android/gms/cast/media/y;

    iget-object v1, v1, Lcom/google/android/gms/cast/media/y;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 772
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/o;

    .line 773
    iget-boolean v1, v0, Lcom/google/android/gms/cast/media/o;->l:Z

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    if-nez v1, :cond_1

    .line 774
    invoke-virtual {v0}, Lcom/google/android/gms/cast/media/o;->i()V

    goto :goto_1

    :cond_2
    move v1, v3

    .line 773
    goto :goto_2

    .line 778
    :cond_3
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "MRP is trying to reconnect"

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 780
    iget-object v0, p0, Lcom/google/android/gms/cast/media/i;->b:Lcom/google/android/gms/cast/media/d;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/d;->a:Lcom/google/android/gms/cast/media/y;

    iput-boolean v2, v0, Lcom/google/android/gms/cast/media/y;->c:Z

    .line 781
    iget-object v0, p0, Lcom/google/android/gms/cast/media/i;->b:Lcom/google/android/gms/cast/media/d;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/d;->a:Lcom/google/android/gms/cast/media/y;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/y;->a:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/b/e;->c()V

    .line 784
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/cast/media/i;->b:Lcom/google/android/gms/cast/media/d;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/d;->c:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->f(Lcom/google/android/gms/cast/media/a;)V

    .line 785
    return-void
.end method
