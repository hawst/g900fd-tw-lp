.class public final Lcom/google/android/gms/smart_profile/card/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:[Lcom/google/ac/c/a/a/a/f;

.field final b:Ljava/util/List;

.field final c:Landroid/util/SparseArray;

.field final d:Ljava/lang/String;

.field final e:Ljava/util/List;

.field private final f:Lcom/google/android/gms/people/identity/models/Person;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/identity/models/Person;Landroid/os/Bundle;Ljava/lang/String;Ljava/util/List;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/smart_profile/card/d;->b:Ljava/util/List;

    .line 57
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/smart_profile/card/d;->c:Landroid/util/SparseArray;

    .line 74
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/d;->f:Lcom/google/android/gms/people/identity/models/Person;

    .line 75
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/card/d;->d:Ljava/lang/String;

    .line 76
    iput-object p4, p0, Lcom/google/android/gms/smart_profile/card/d;->e:Ljava/util/List;

    .line 78
    if-eqz p2, :cond_1

    const-string v2, "com.google.android.gms.people.smart_profile.CARDS"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 79
    const-string v2, "com.google.android.gms.people.smart_profile.CARDS"

    invoke-virtual {p2, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v2

    .line 80
    if-eqz v2, :cond_2

    .line 82
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 83
    :goto_0
    array-length v4, v2

    if-ge v0, v4, :cond_2

    .line 84
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 85
    iget-object v4, p0, Lcom/google/android/gms/smart_profile/card/d;->b:Ljava/util/List;

    aget v5, v2, v0

    invoke-direct {p0, v5}, Lcom/google/android/gms/smart_profile/card/d;->a(I)Lcom/google/ac/c/a/a/a/g;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 86
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 83
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 91
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/d;->b:Ljava/util/List;

    invoke-direct {p0, v0}, Lcom/google/android/gms/smart_profile/card/d;->a(I)Lcom/google/ac/c/a/a/a/g;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/d;->b:Ljava/util/List;

    const/4 v2, 0x3

    invoke-direct {p0, v2}, Lcom/google/android/gms/smart_profile/card/d;->a(I)Lcom/google/ac/c/a/a/a/g;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/d;->b:Ljava/util/List;

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/gms/smart_profile/card/d;->a(I)Lcom/google/ac/c/a/a/a/g;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/d;->b:Ljava/util/List;

    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/google/android/gms/smart_profile/card/d;->a(I)Lcom/google/ac/c/a/a/a/g;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/d;->b:Ljava/util/List;

    const/4 v2, 0x4

    invoke-direct {p0, v2}, Lcom/google/android/gms/smart_profile/card/d;->a(I)Lcom/google/ac/c/a/a/a/g;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/d;->b:Ljava/util/List;

    const/4 v2, 0x5

    invoke-direct {p0, v2}, Lcom/google/android/gms/smart_profile/card/d;->a(I)Lcom/google/ac/c/a/a/a/g;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_2
    if-eqz p2, :cond_4

    const-string v0, "com.google.android.gms.people.smart_profile.CARD_BYTES"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 96
    const-string v0, "com.google.android.gms.people.smart_profile.CARD_BYTES"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/ar;->b([B)Lcom/google/ac/c/a/a/a/h;

    move-result-object v0

    .line 97
    if-nez v0, :cond_3

    move-object v0, v1

    :goto_1
    iput-object v0, p0, Lcom/google/android/gms/smart_profile/card/d;->a:[Lcom/google/ac/c/a/a/a/f;

    .line 102
    :goto_2
    const-string v0, "CardMixer"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "using card params "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/d;->b:Ljava/util/List;

    invoke-static {v2}, Lcom/google/android/gms/smart_profile/ar;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " with passed in cards "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/d;->a:[Lcom/google/ac/c/a/a/a/f;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for extras "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/smart_profile/bc;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    return-void

    .line 97
    :cond_3
    iget-object v0, v0, Lcom/google/ac/c/a/a/a/h;->a:[Lcom/google/ac/c/a/a/a/f;

    goto :goto_1

    .line 99
    :cond_4
    iput-object v1, p0, Lcom/google/android/gms/smart_profile/card/d;->a:[Lcom/google/ac/c/a/a/a/f;

    goto :goto_2
.end method

.method static a(Lcom/google/ac/c/a/a/a/f;Lcom/google/ac/c/a/a/a/f;)Lcom/google/ac/c/a/a/a/f;
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 250
    if-nez p0, :cond_1

    if-nez p1, :cond_1

    move-object p1, v3

    .line 281
    :cond_0
    :goto_0
    return-object p1

    .line 254
    :cond_1
    if-eqz p0, :cond_0

    .line 258
    if-nez p1, :cond_2

    move-object p1, p0

    .line 259
    goto :goto_0

    .line 262
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 263
    iget-object v0, p1, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    iget-object v5, v0, Lcom/google/ac/c/a/a/a/ai;->b:[Lcom/google/ac/c/a/a/a/ag;

    move v0, v1

    move v2, v1

    .line 266
    :goto_1
    array-length v6, v5

    if-ge v0, v6, :cond_4

    const/16 v6, 0xa

    if-ge v2, v6, :cond_4

    .line 268
    aget-object v6, v5, v0

    .line 269
    iget-object v7, v6, Lcom/google/ac/c/a/a/a/ag;->a:Ljava/lang/Integer;

    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 270
    iput-object v3, v6, Lcom/google/ac/c/a/a/a/ag;->g:Ljava/lang/String;

    .line 271
    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 272
    add-int/lit8 v2, v2, 0x1

    .line 267
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 275
    :cond_4
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/ai;->b:[Lcom/google/ac/c/a/a/a/ag;

    .line 277
    :goto_2
    array-length v2, v0

    if-ge v1, v2, :cond_5

    .line 278
    aget-object v2, v0, v1

    invoke-interface {v4, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 280
    :cond_5
    new-instance v0, Lcom/google/android/gms/smart_profile/card/e;

    invoke-direct {v0}, Lcom/google/android/gms/smart_profile/card/e;-><init>()V

    invoke-static {v4, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 281
    invoke-static {v4}, Lcom/google/android/gms/smart_profile/ar;->b(Ljava/util/List;)Lcom/google/ac/c/a/a/a/f;

    move-result-object p1

    goto :goto_0
.end method

.method static a(Ljava/util/List;)Lcom/google/ac/c/a/a/a/f;
    .locals 3

    .prologue
    .line 197
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 198
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    .line 199
    iget-object v2, v0, Lcom/google/ac/c/a/a/a/f;->c:Lcom/google/ac/c/a/a/a/al;

    if-eqz v2, :cond_0

    .line 203
    :goto_1
    return-object v0

    .line 197
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 203
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(I)Lcom/google/ac/c/a/a/a/g;
    .locals 4

    .prologue
    const/16 v2, 0xa

    .line 462
    new-instance v0, Lcom/google/ac/c/a/a/a/g;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/g;-><init>()V

    .line 463
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/d;->f:Lcom/google/android/gms/people/identity/models/Person;

    if-nez v1, :cond_0

    .line 495
    :goto_0
    return-object v0

    .line 466
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 497
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized SmartProfile.Card "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 468
    :pswitch_0
    new-instance v1, Lcom/google/ac/c/a/a/a/k;

    invoke-direct {v1}, Lcom/google/ac/c/a/a/a/k;-><init>()V

    iput-object v1, v0, Lcom/google/ac/c/a/a/a/g;->e:Lcom/google/ac/c/a/a/a/k;

    goto :goto_0

    .line 471
    :pswitch_1
    new-instance v1, Lcom/google/ac/c/a/a/a/am;

    invoke-direct {v1}, Lcom/google/ac/c/a/a/a/am;-><init>()V

    iput-object v1, v0, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    .line 472
    iget-object v1, v0, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/c/a/a/a/am;->b:Ljava/lang/Integer;

    .line 473
    iget-object v1, v0, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    new-instance v2, Lcom/google/ac/c/a/a/a/aa;

    invoke-direct {v2}, Lcom/google/ac/c/a/a/a/aa;-><init>()V

    iput-object v2, v1, Lcom/google/ac/c/a/a/a/am;->a:Lcom/google/ac/c/a/a/a/aa;

    .line 474
    iget-object v1, v0, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    iget-object v1, v1, Lcom/google/ac/c/a/a/a/am;->a:Lcom/google/ac/c/a/a/a/aa;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/d;->f:Lcom/google/android/gms/people/identity/models/Person;

    invoke-interface {v2}, Lcom/google/android/gms/people/identity/models/Person;->k()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/c/a/a/a/aa;->a:Ljava/lang/String;

    goto :goto_0

    .line 477
    :pswitch_2
    new-instance v1, Lcom/google/ac/c/a/a/a/ar;

    invoke-direct {v1}, Lcom/google/ac/c/a/a/a/ar;-><init>()V

    iput-object v1, v0, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    .line 478
    iget-object v1, v0, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/c/a/a/a/ar;->b:Ljava/lang/Integer;

    .line 479
    iget-object v1, v0, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    new-instance v2, Lcom/google/ac/c/a/a/a/aa;

    invoke-direct {v2}, Lcom/google/ac/c/a/a/a/aa;-><init>()V

    iput-object v2, v1, Lcom/google/ac/c/a/a/a/ar;->a:Lcom/google/ac/c/a/a/a/aa;

    .line 480
    iget-object v1, v0, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    iget-object v1, v1, Lcom/google/ac/c/a/a/a/ar;->a:Lcom/google/ac/c/a/a/a/aa;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/d;->f:Lcom/google/android/gms/people/identity/models/Person;

    invoke-interface {v2}, Lcom/google/android/gms/people/identity/models/Person;->k()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/c/a/a/a/aa;->a:Ljava/lang/String;

    goto :goto_0

    .line 483
    :pswitch_3
    new-instance v1, Lcom/google/ac/c/a/a/a/aj;

    invoke-direct {v1}, Lcom/google/ac/c/a/a/a/aj;-><init>()V

    .line 485
    const/16 v2, 0x14

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iput-object v2, v1, Lcom/google/ac/c/a/a/a/aj;->b:Ljava/lang/Integer;

    .line 486
    new-instance v2, Lcom/google/ac/c/a/a/a/aa;

    invoke-direct {v2}, Lcom/google/ac/c/a/a/a/aa;-><init>()V

    iput-object v2, v1, Lcom/google/ac/c/a/a/a/aj;->a:Lcom/google/ac/c/a/a/a/aa;

    .line 487
    iget-object v2, v1, Lcom/google/ac/c/a/a/a/aj;->a:Lcom/google/ac/c/a/a/a/aa;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/card/d;->f:Lcom/google/android/gms/people/identity/models/Person;

    invoke-interface {v3}, Lcom/google/android/gms/people/identity/models/Person;->k()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/ac/c/a/a/a/aa;->a:Ljava/lang/String;

    .line 488
    iput-object v1, v0, Lcom/google/ac/c/a/a/a/g;->b:Lcom/google/ac/c/a/a/a/aj;

    goto/16 :goto_0

    .line 491
    :pswitch_4
    new-instance v1, Lcom/google/ac/c/a/a/a/c;

    invoke-direct {v1}, Lcom/google/ac/c/a/a/a/c;-><init>()V

    iput-object v1, v0, Lcom/google/ac/c/a/a/a/g;->d:Lcom/google/ac/c/a/a/a/c;

    goto/16 :goto_0

    .line 494
    :pswitch_5
    new-instance v1, Lcom/google/ac/c/a/a/a/u;

    invoke-direct {v1}, Lcom/google/ac/c/a/a/a/u;-><init>()V

    iput-object v1, v0, Lcom/google/ac/c/a/a/a/g;->f:Lcom/google/ac/c/a/a/a/u;

    goto/16 :goto_0

    .line 466
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static a(Lcom/google/ac/c/a/a/a/g;)Z
    .locals 1

    .prologue
    .line 455
    if-eqz p0, :cond_1

    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->b:Lcom/google/ac/c/a/a/a/aj;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Ljava/util/List;)Lcom/google/ac/c/a/a/a/f;
    .locals 3

    .prologue
    .line 207
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 208
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    .line 209
    iget-object v2, v0, Lcom/google/ac/c/a/a/a/f;->a:Lcom/google/ac/c/a/a/a/aq;

    if-eqz v2, :cond_0

    .line 213
    :goto_1
    return-object v0

    .line 207
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 213
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static c(Ljava/util/List;)Lcom/google/ac/c/a/a/a/f;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 224
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v2

    .line 234
    :goto_0
    return-object v0

    .line 227
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 228
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    .line 229
    iget-object v3, v0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    if-eqz v3, :cond_2

    .line 230
    invoke-interface {p0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 227
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    move-object v0, v2

    .line 234
    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/util/List;
    .locals 2

    .prologue
    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 121
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/card/d;->d:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 132
    :cond_0
    :goto_0
    return-object v0

    .line 124
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/d;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 125
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 127
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/d;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 130
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/card/d;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final c()Lcom/google/ac/c/a/a/a/f;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 363
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 364
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_4

    .line 365
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    .line 366
    if-eqz v5, :cond_0

    if-eq v5, v3, :cond_0

    const/4 v0, 0x2

    if-ne v5, v0, :cond_2

    :cond_0
    move v0, v3

    :goto_1
    if-eqz v0, :cond_3

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 368
    if-eqz v0, :cond_3

    .line 369
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ac/c/a/a/a/f;

    .line 370
    if-eqz v0, :cond_1

    iget-object v6, v0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    if-eqz v6, :cond_1

    iget-object v6, v0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    iget-object v6, v6, Lcom/google/ac/c/a/a/a/ai;->b:[Lcom/google/ac/c/a/a/a/ag;

    if-eqz v6, :cond_1

    .line 373
    iget-object v0, v0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/ai;->b:[Lcom/google/ac/c/a/a/a/ag;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    :cond_2
    move v0, v2

    .line 366
    goto :goto_1

    .line 364
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 380
    :cond_4
    invoke-static {v4}, Lcom/google/android/gms/smart_profile/ar;->b(Ljava/util/List;)Lcom/google/ac/c/a/a/a/f;

    move-result-object v0

    return-object v0
.end method

.method final d()Ljava/util/List;
    .locals 4

    .prologue
    .line 389
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 390
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 391
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 392
    const/4 v3, 0x3

    if-ne v3, v0, :cond_1

    .line 393
    iget-object v3, p0, Lcom/google/android/gms/smart_profile/card/d;->c:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 394
    if-eqz v0, :cond_1

    .line 395
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 400
    :cond_0
    return-object v2

    .line 390
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method final e()Z
    .locals 2

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ac/c/a/a/a/g;

    .line 420
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/ac/c/a/a/a/g;->b:Lcom/google/ac/c/a/a/a/aj;

    if-eqz v0, :cond_0

    .line 421
    const/4 v0, 0x1

    .line 424
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final f()Z
    .locals 2

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/d;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ac/c/a/a/a/g;

    .line 429
    invoke-static {v0}, Lcom/google/android/gms/smart_profile/card/d;->a(Lcom/google/ac/c/a/a/a/g;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 430
    const/4 v0, 0x1

    .line 433
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
