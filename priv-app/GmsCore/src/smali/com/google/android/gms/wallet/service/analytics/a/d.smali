.class public Lcom/google/android/gms/wallet/service/analytics/a/d;
.super Lcom/google/android/gms/wallet/service/analytics/a/h;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/k/f/a/a/u;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/analytics/a/h;-><init>()V

    .line 29
    new-instance v0, Lcom/google/k/f/a/a/u;

    invoke-direct {v0}, Lcom/google/k/f/a/a/u;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/d;->a:Lcom/google/k/f/a/a/u;

    .line 30
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/wallet/common/ag;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/h;-><init>(Lcom/google/android/gms/wallet/common/ag;)V

    .line 34
    const-class v0, Lcom/google/k/f/a/a/u;

    new-instance v1, Lcom/google/k/f/a/a/u;

    invoke-direct {v1}, Lcom/google/k/f/a/a/u;-><init>()V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/wallet/common/ag;->a(Ljava/lang/Class;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/k/f/a/a/u;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/d;->a:Lcom/google/k/f/a/a/u;

    .line 37
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V
    .locals 3

    .prologue
    .line 58
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;

    if-eqz v0, :cond_0

    .line 59
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;

    .line 61
    iget-object v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/d;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/content/Context;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/d;->a:Lcom/google/k/f/a/a/u;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/OwWalletFragmentButtonClickedEvent;->d:I

    iput v1, v0, Lcom/google/k/f/a/a/u;->a:I

    .line 66
    :goto_0
    return-void

    .line 64
    :cond_0
    const-string v0, "OwButtonClickedSessionState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to fill data for event "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/wallet/common/ah;)V
    .locals 1

    .prologue
    .line 41
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/h;->a(Lcom/google/android/gms/wallet/common/ah;)V

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/d;->a:Lcom/google/k/f/a/a/u;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/common/ah;->a(Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/common/ah;

    .line 43
    return-void
.end method

.method protected final a(Lcom/google/k/f/a/a/j;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/d;->a:Lcom/google/k/f/a/a/u;

    iput-object v0, p1, Lcom/google/k/f/a/a/j;->k:Lcom/google/k/f/a/a/u;

    .line 48
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/d;->a:Lcom/google/k/f/a/a/u;

    iget v0, v0, Lcom/google/k/f/a/a/u;->a:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
