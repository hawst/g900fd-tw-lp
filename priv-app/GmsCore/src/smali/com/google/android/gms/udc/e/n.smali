.class public final Lcom/google/android/gms/udc/e/n;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/udc/e/s;

.field public b:Lcom/google/android/gms/udc/e/s;

.field public c:I

.field public d:Lcom/google/android/gms/udc/e/s;

.field public e:Lcom/google/android/gms/udc/e/s;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 42
    iput-object v1, p0, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/udc/e/n;->c:I

    iput-object v1, p0, Lcom/google/android/gms/udc/e/n;->d:Lcom/google/android/gms/udc/e/s;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/n;->e:Lcom/google/android/gms/udc/e/s;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/n;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/udc/e/n;->cachedSize:I

    .line 43
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 146
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 147
    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    if-eqz v1, :cond_0

    .line 148
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 151
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    if-eqz v1, :cond_1

    .line 152
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->d:Lcom/google/android/gms/udc/e/s;

    if-eqz v1, :cond_2

    .line 156
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/udc/e/n;->d:Lcom/google/android/gms/udc/e/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 159
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->e:Lcom/google/android/gms/udc/e/s;

    if-eqz v1, :cond_3

    .line 160
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/udc/e/n;->e:Lcom/google/android/gms/udc/e/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    :cond_3
    iget v1, p0, Lcom/google/android/gms/udc/e/n;->c:I

    if-eqz v1, :cond_4

    .line 164
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/udc/e/n;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 58
    if-ne p1, p0, :cond_1

    .line 59
    const/4 v0, 0x1

    .line 104
    :cond_0
    :goto_0
    return v0

    .line 61
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/udc/e/n;

    if-eqz v1, :cond_0

    .line 64
    check-cast p1, Lcom/google/android/gms/udc/e/n;

    .line 65
    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_6

    .line 66
    iget-object v1, p1, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_0

    .line 74
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_7

    .line 75
    iget-object v1, p1, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_0

    .line 83
    :cond_3
    iget v1, p0, Lcom/google/android/gms/udc/e/n;->c:I

    iget v2, p1, Lcom/google/android/gms/udc/e/n;->c:I

    if-ne v1, v2, :cond_0

    .line 86
    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->d:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_8

    .line 87
    iget-object v1, p1, Lcom/google/android/gms/udc/e/n;->d:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_0

    .line 95
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->e:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_9

    .line 96
    iget-object v1, p1, Lcom/google/android/gms/udc/e/n;->e:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_0

    .line 104
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/android/gms/udc/e/n;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 70
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 79
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 91
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->d:Lcom/google/android/gms/udc/e/s;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/n;->d:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 100
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->e:Lcom/google/android/gms/udc/e/s;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/n;->e:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 112
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 114
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/udc/e/n;->c:I

    add-int/2addr v0, v2

    .line 115
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->d:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 117
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/n;->e:Lcom/google/android/gms/udc/e/s;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 119
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/udc/e/n;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    return v0

    .line 109
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/s;->hashCode()I

    move-result v0

    goto :goto_0

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/s;->hashCode()I

    move-result v0

    goto :goto_1

    .line 115
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->d:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/s;->hashCode()I

    move-result v0

    goto :goto_2

    .line 117
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->e:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1}, Lcom/google/android/gms/udc/e/s;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 9
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/udc/e/n;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->d:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/n;->d:Lcom/google/android/gms/udc/e/s;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->d:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->e:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/n;->e:Lcom/google/android/gms/udc/e/s;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->e:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/udc/e/n;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_0

    .line 127
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_1

    .line 130
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->d:Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_2

    .line 133
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->d:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 135
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/n;->e:Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_3

    .line 136
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/udc/e/n;->e:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 138
    :cond_3
    iget v0, p0, Lcom/google/android/gms/udc/e/n;->c:I

    if-eqz v0, :cond_4

    .line 139
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/gms/udc/e/n;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 141
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 142
    return-void
.end method
