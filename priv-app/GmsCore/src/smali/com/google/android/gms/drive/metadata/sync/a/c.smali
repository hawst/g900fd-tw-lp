.class public abstract Lcom/google/android/gms/drive/metadata/sync/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final c:Lcom/google/android/gms/drive/metadata/sync/a/e;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/drive/metadata/sync/a/e;)V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/a/e;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/c;->c:Lcom/google/android/gms/drive/metadata/sync/a/e;

    .line 18
    return-void
.end method


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 29
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "Feed[%s:%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/a/c;->c:Lcom/google/android/gms/drive/metadata/sync/a/e;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/sync/a/c;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
