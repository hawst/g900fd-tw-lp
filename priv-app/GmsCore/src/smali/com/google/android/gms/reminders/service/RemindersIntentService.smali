.class public Lcom/google/android/gms/reminders/service/RemindersIntentService;
.super Lcom/google/android/gms/common/service/c;
.source "SourceFile"


# static fields
.field private static final b:Lcom/google/android/gms/common/service/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/common/service/d;

    invoke-direct {v0}, Lcom/google/android/gms/common/service/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/reminders/service/RemindersIntentService;->b:Lcom/google/android/gms/common/service/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 31
    const-string v0, "RemindersIntentService"

    sget-object v1, Lcom/google/android/gms/reminders/service/RemindersIntentService;->b:Lcom/google/android/gms/common/service/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/service/c;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/service/d;)V

    .line 32
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V
    .locals 1

    .prologue
    .line 35
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    .line 36
    sget-object v0, Lcom/google/android/gms/reminders/service/RemindersIntentService;->b:Lcom/google/android/gms/common/service/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/service/d;->offer(Ljava/lang/Object;)Z

    .line 37
    const-string v0, "com.google.android.gms.reminders.service.INTENT"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 38
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/reminders/internal/b;)V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/gms/reminders/service/a/b;

    invoke-direct {v0, p1}, Lcom/google/android/gms/reminders/service/a/b;-><init>(Lcom/google/android/gms/reminders/internal/b;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/reminders/service/RemindersIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 47
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/LoadRemindersOptions;)V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/gms/reminders/service/a/e;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/reminders/service/a/e;-><init>(Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/LoadRemindersOptions;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/reminders/service/RemindersIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 43
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskEntity;)V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/gms/reminders/service/a/c;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/reminders/service/a/c;-><init>(Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskEntity;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/reminders/service/RemindersIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 52
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskIdEntity;)V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/google/android/gms/reminders/service/a/d;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/reminders/service/a/d;-><init>(Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskIdEntity;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/reminders/service/RemindersIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 62
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskEntity;)V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/gms/reminders/service/a/f;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/reminders/service/a/f;-><init>(Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskEntity;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/reminders/service/RemindersIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 57
    return-void
.end method
