.class public final Lcom/google/android/gms/icing/c/a/h;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/android/gms/icing/c/a/h;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4144
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 4145
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->d:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/h;->cachedSize:I

    .line 4146
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/c/a/h;
    .locals 2

    .prologue
    .line 4121
    sget-object v0, Lcom/google/android/gms/icing/c/a/h;->e:[Lcom/google/android/gms/icing/c/a/h;

    if-nez v0, :cond_1

    .line 4122
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 4124
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/c/a/h;->e:[Lcom/google/android/gms/icing/c/a/h;

    if-nez v0, :cond_0

    .line 4125
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/c/a/h;

    sput-object v0, Lcom/google/android/gms/icing/c/a/h;->e:[Lcom/google/android/gms/icing/c/a/h;

    .line 4127
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4129
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/c/a/h;->e:[Lcom/google/android/gms/icing/c/a/h;

    return-object v0

    .line 4127
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 4233
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 4234
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4235
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/h;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4238
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 4239
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/h;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4242
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 4243
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/h;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4246
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 4247
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/h;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4250
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4160
    if-ne p1, p0, :cond_1

    .line 4161
    const/4 v0, 0x1

    .line 4195
    :cond_0
    :goto_0
    return v0

    .line 4163
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/h;

    if-eqz v1, :cond_0

    .line 4166
    check-cast p1, Lcom/google/android/gms/icing/c/a/h;

    .line 4167
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->a:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 4168
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/h;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4174
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->b:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 4175
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/h;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4181
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->c:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 4182
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/h;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4188
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->d:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 4189
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/h;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4195
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/h;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 4171
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/h;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 4178
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/h;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 4185
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/h;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 4192
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/h;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4200
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4203
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 4205
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 4207
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/h;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 4209
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/h;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4210
    return v0

    .line 4200
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 4203
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 4205
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 4207
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4115
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/h;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4216
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4217
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4219
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4220
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4222
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 4223
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4225
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/h;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 4226
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/h;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4228
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4229
    return-void
.end method
