.class public abstract Lcom/google/android/gms/cast/b/af;
.super Lcom/google/android/gms/cast/internal/b;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field b:Ljava/lang/String;

.field final c:Lcom/google/android/gms/cast/internal/p;

.field final d:Lcom/google/android/gms/cast/internal/p;

.field final e:Lcom/google/android/gms/cast/internal/p;

.field final f:Lcom/google/android/gms/cast/internal/p;

.field final g:Lcom/google/android/gms/cast/internal/p;

.field h:D

.field i:Z

.field j:Z

.field k:I

.field l:I

.field private final o:Lcom/google/android/gms/cast/internal/o;

.field private final p:Lcom/google/android/gms/cast/internal/o;

.field private final q:Lcom/google/android/gms/cast/internal/o;

.field private final r:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v3, -0x1

    const/4 v6, 0x0

    .line 46
    sget-object v0, Lcom/google/android/gms/cast/internal/j;->a:Ljava/lang/String;

    const-string v1, "ReceiverControlChannel"

    invoke-direct {p0, v0, v1, p2}, Lcom/google/android/gms/cast/internal/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    sget-object v0, Lcom/google/android/gms/cast/a/e;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 48
    sget-object v0, Lcom/google/android/gms/cast/a/e;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 50
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 51
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "receiverDestinationId can\'t be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 54
    :cond_0
    iput v3, p0, Lcom/google/android/gms/cast/b/af;->k:I

    .line 55
    iput v3, p0, Lcom/google/android/gms/cast/b/af;->l:I

    .line 56
    iput-object p1, p0, Lcom/google/android/gms/cast/b/af;->a:Ljava/lang/String;

    .line 57
    iput-boolean p3, p0, Lcom/google/android/gms/cast/b/af;->r:Z

    .line 59
    new-instance v2, Lcom/google/android/gms/cast/internal/p;

    int-to-long v4, v1

    invoke-direct {v2, v4, v5}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v2, p0, Lcom/google/android/gms/cast/b/af;->c:Lcom/google/android/gms/cast/internal/p;

    .line 60
    new-instance v1, Lcom/google/android/gms/cast/b/ag;

    invoke-direct {v1, p0, v6}, Lcom/google/android/gms/cast/b/ag;-><init>(Lcom/google/android/gms/cast/b/af;B)V

    iput-object v1, p0, Lcom/google/android/gms/cast/b/af;->o:Lcom/google/android/gms/cast/internal/o;

    .line 61
    new-instance v1, Lcom/google/android/gms/cast/internal/p;

    int-to-long v2, v0

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v1, p0, Lcom/google/android/gms/cast/b/af;->d:Lcom/google/android/gms/cast/internal/p;

    .line 62
    new-instance v1, Lcom/google/android/gms/cast/b/ai;

    invoke-direct {v1, p0, v6}, Lcom/google/android/gms/cast/b/ai;-><init>(Lcom/google/android/gms/cast/b/af;B)V

    iput-object v1, p0, Lcom/google/android/gms/cast/b/af;->p:Lcom/google/android/gms/cast/internal/o;

    .line 63
    new-instance v1, Lcom/google/android/gms/cast/internal/p;

    int-to-long v2, v0

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v1, p0, Lcom/google/android/gms/cast/b/af;->e:Lcom/google/android/gms/cast/internal/p;

    .line 64
    new-instance v1, Lcom/google/android/gms/cast/b/ah;

    invoke-direct {v1, p0, v6}, Lcom/google/android/gms/cast/b/ah;-><init>(Lcom/google/android/gms/cast/b/af;B)V

    iput-object v1, p0, Lcom/google/android/gms/cast/b/af;->q:Lcom/google/android/gms/cast/internal/o;

    .line 65
    new-instance v1, Lcom/google/android/gms/cast/internal/p;

    int-to-long v2, v0

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v1, p0, Lcom/google/android/gms/cast/b/af;->f:Lcom/google/android/gms/cast/internal/p;

    .line 66
    new-instance v1, Lcom/google/android/gms/cast/internal/p;

    int-to-long v2, v0

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v1, p0, Lcom/google/android/gms/cast/b/af;->g:Lcom/google/android/gms/cast/internal/p;

    .line 67
    return-void
.end method


# virtual methods
.method protected abstract a()V
.end method

.method public final a(DDZ)V
    .locals 5

    .prologue
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    const-wide/16 v0, 0x0

    .line 156
    cmpg-double v4, p1, v0

    if-gez v4, :cond_1

    move-wide p1, v0

    .line 162
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/af;->c()J

    move-result-wide v0

    .line 163
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 165
    :try_start_0
    const-string v3, "requestId"

    invoke-virtual {v2, v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 166
    const-string v3, "type"

    const-string v4, "SET_VOLUME"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 168
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 169
    const-string v4, "level"

    invoke-virtual {v3, v4, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 170
    const-string v4, "volume"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 172
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 173
    const-string v4, "level"

    invoke-virtual {v3, v4, p3, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 174
    const-string v4, "muted"

    invoke-virtual {v3, v4, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 175
    const-string v4, "expectedVolume"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 178
    :goto_1
    iput-wide p1, p0, Lcom/google/android/gms/cast/b/af;->h:D

    .line 179
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/cast/b/af;->a:Ljava/lang/String;

    invoke-virtual {p0, v2, v0, v1, v3}, Lcom/google/android/gms/cast/b/af;->a(Ljava/lang/String;JLjava/lang/String;)V

    .line 180
    iget-object v2, p0, Lcom/google/android/gms/cast/b/af;->f:Lcom/google/android/gms/cast/internal/p;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/gms/cast/internal/p;->a(JLcom/google/android/gms/cast/internal/o;)V

    .line 181
    return-void

    .line 158
    :cond_1
    cmpl-double v0, p1, v2

    if-lez v0, :cond_0

    move-wide p1, v2

    .line 159
    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method protected abstract a(I)V
.end method

.method protected abstract a(Lcom/google/android/gms/cast/b/b;)V
.end method

.method protected abstract a(Lcom/google/android/gms/cast/b/b;ZLcom/google/android/gms/cast/internal/DeviceStatus;)V
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 77
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/af;->c()J

    move-result-wide v2

    .line 80
    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 81
    const-string v1, "type"

    const-string v4, "LAUNCH"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 82
    const-string v1, "appId"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 83
    if-eqz p2, :cond_0

    .line 84
    const-string v1, "language"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/b/af;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/google/android/gms/cast/b/af;->a(Ljava/lang/String;JLjava/lang/String;)V

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->c:Lcom/google/android/gms/cast/internal/p;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/af;->o:Lcom/google/android/gms/cast/internal/o;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/cast/internal/p;->a(JLcom/google/android/gms/cast/internal/o;)V

    .line 90
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final a(ZDZ)V
    .locals 6

    .prologue
    .line 201
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/af;->c()J

    move-result-wide v0

    .line 202
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 204
    :try_start_0
    const-string v3, "requestId"

    invoke-virtual {v2, v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 205
    const-string v3, "type"

    const-string v4, "SET_VOLUME"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 207
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 208
    const-string v4, "muted"

    invoke-virtual {v3, v4, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 209
    const-string v4, "volume"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 211
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 212
    const-string v4, "level"

    invoke-virtual {v3, v4, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 213
    const-string v4, "muted"

    invoke-virtual {v3, v4, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 214
    const-string v4, "expectedVolume"

    invoke-virtual {v2, v4, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    :goto_0
    iput-boolean p1, p0, Lcom/google/android/gms/cast/b/af;->i:Z

    .line 218
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/cast/b/af;->a:Ljava/lang/String;

    invoke-virtual {p0, v2, v0, v1, v3}, Lcom/google/android/gms/cast/b/af;->a(Ljava/lang/String;JLjava/lang/String;)V

    .line 219
    iget-object v2, p0, Lcom/google/android/gms/cast/b/af;->g:Lcom/google/android/gms/cast/internal/p;

    const/4 v3, 0x0

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/gms/cast/internal/p;->a(JLcom/google/android/gms/cast/internal/o;)V

    .line 220
    return-void

    :catch_0
    move-exception v3

    goto :goto_0
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 14

    .prologue
    const/16 v0, 0xd

    const/4 v13, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 224
    iget-object v3, p0, Lcom/google/android/gms/cast/b/af;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v4, "Received: %s"

    new-array v5, v2, [Ljava/lang/Object;

    aput-object p1, v5, v1

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 227
    :try_start_0
    new-instance v4, Lorg/json/JSONObject;

    invoke-direct {v4, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 229
    const-string v3, "type"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 230
    const-string v5, "requestId"

    const-wide/16 v6, -0x1

    invoke-virtual {v4, v5, v6, v7}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v6

    .line 233
    const-string v5, "RECEIVER_STATUS"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_11

    .line 234
    new-instance v5, Lcom/google/android/gms/cast/internal/DeviceStatus;

    invoke-direct {v5}, Lcom/google/android/gms/cast/internal/DeviceStatus;-><init>()V

    .line 235
    const/4 v3, 0x0

    .line 238
    const-string v0, "status"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v4

    .line 240
    const-string v0, "applications"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    const-string v0, "applications"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 242
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v8

    if-lez v8, :cond_0

    .line 244
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v8

    .line 246
    :try_start_1
    new-instance v0, Lcom/google/android/gms/cast/b/b;

    invoke-direct {v0, v8}, Lcom/google/android/gms/cast/b/b;-><init>(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    move-object v3, v0

    .line 255
    :cond_0
    :goto_0
    const/4 v0, -0x1

    :try_start_2
    iput v0, p0, Lcom/google/android/gms/cast/b/af;->k:I

    .line 256
    const-string v0, "isActiveInput"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 257
    const-string v0, "isActiveInput"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    :goto_1
    iput v0, p0, Lcom/google/android/gms/cast/b/af;->k:I

    .line 260
    :cond_1
    iget v0, p0, Lcom/google/android/gms/cast/b/af;->k:I

    invoke-virtual {v5, v0}, Lcom/google/android/gms/cast/internal/DeviceStatus;->a(I)V

    .line 262
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/cast/b/af;->l:I

    .line 263
    const-string v0, "isStandBy"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 264
    const-string v0, "isStandBy"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v0, v2

    :goto_2
    iput v0, p0, Lcom/google/android/gms/cast/b/af;->l:I

    .line 267
    :cond_2
    iget v0, p0, Lcom/google/android/gms/cast/b/af;->l:I

    invoke-virtual {v5, v0}, Lcom/google/android/gms/cast/internal/DeviceStatus;->b(I)V

    .line 269
    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->c:Lcom/google/android/gms/cast/internal/p;

    const/4 v8, 0x0

    invoke-virtual {v0, v6, v7, v8}, Lcom/google/android/gms/cast/internal/p;->a(JI)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 270
    if-eqz v3, :cond_3

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v4, "application launch has completed"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 272
    invoke-virtual {p0, v3}, Lcom/google/android/gms/cast/b/af;->a(Lcom/google/android/gms/cast/b/b;)V

    .line 360
    :cond_3
    :goto_3
    return-void

    .line 248
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v8, "Error extracting the application info."

    const/4 v9, 0x0

    new-array v9, v9, [Ljava/lang/Object;

    invoke-virtual {v0, v8, v9}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 357
    :catch_1
    move-exception v0

    .line 358
    iget-object v3, p0, Lcom/google/android/gms/cast/b/af;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v4, "Message is malformed (%s); ignoring: %s"

    new-array v5, v13, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v1

    aput-object p1, v5, v2

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/cast/internal/k;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3

    :cond_4
    move v0, v1

    .line 257
    goto :goto_1

    :cond_5
    move v0, v1

    .line 264
    goto :goto_2

    .line 277
    :cond_6
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->e:Lcom/google/android/gms/cast/internal/p;

    const/4 v8, 0x0

    invoke-virtual {v0, v6, v7, v8}, Lcom/google/android/gms/cast/internal/p;->a(JI)Z

    move-result v8

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->b:Ljava/lang/String;

    if-eqz v0, :cond_f

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Lcom/google/android/gms/cast/b/b;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v9, p0, Lcom/google/android/gms/cast/b/af;->b:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    :cond_7
    move v0, v2

    .line 285
    :goto_4
    iget-object v9, p0, Lcom/google/android/gms/cast/b/af;->d:Lcom/google/android/gms/cast/internal/p;

    const/4 v10, 0x0

    invoke-virtual {v9, v6, v7, v10}, Lcom/google/android/gms/cast/internal/p;->a(JI)Z

    move-result v9

    .line 288
    if-eqz v0, :cond_8

    .line 289
    iget-object v10, p0, Lcom/google/android/gms/cast/b/af;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v11, "application has stopped"

    const/4 v12, 0x0

    new-array v12, v12, [Ljava/lang/Object;

    invoke-virtual {v10, v11, v12}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 292
    :cond_8
    if-nez v0, :cond_9

    if-eqz v9, :cond_b

    .line 293
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/af;->r:Z

    if-eqz v0, :cond_a

    .line 294
    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->d:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/p;->c()Z

    .line 296
    :cond_a
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/af;->a()V

    .line 301
    :cond_b
    sget-object v9, Lcom/google/android/gms/cast/internal/p;->a:Ljava/lang/Object;

    monitor-enter v9
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    .line 303
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->f:Lcom/google/android/gms/cast/internal/p;

    const/4 v10, 0x0

    invoke-virtual {v0, v6, v7, v10}, Lcom/google/android/gms/cast/internal/p;->a(JI)Z

    .line 304
    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->g:Lcom/google/android/gms/cast/internal/p;

    const/4 v10, 0x0

    invoke-virtual {v0, v6, v7, v10}, Lcom/google/android/gms/cast/internal/p;->a(JI)Z

    .line 306
    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->f:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/p;->b()Z

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->g:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/p;->b()Z

    move-result v0

    if-eqz v0, :cond_10

    :cond_c
    move v0, v2

    .line 307
    :goto_5
    monitor-exit v9
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 308
    :try_start_5
    iget-object v9, p0, Lcom/google/android/gms/cast/b/af;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v10, "requestId = %d, ignoreVolume = %b"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v11, v12

    const/4 v6, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v11, v6

    invoke-virtual {v9, v10, v11}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 310
    iget-boolean v6, p0, Lcom/google/android/gms/cast/b/af;->j:Z

    if-nez v6, :cond_d

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v6, "first status received, so not ignoring volume change"

    const/4 v7, 0x0

    new-array v7, v7, [Ljava/lang/Object;

    invoke-virtual {v0, v6, v7}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 313
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/af;->j:Z

    move v0, v1

    .line 316
    :cond_d
    const-string v6, "volume"

    invoke-virtual {v4, v6}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_e

    if-nez v0, :cond_e

    .line 317
    const-string v0, "volume"

    invoke-virtual {v4, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 318
    const-string v4, "level"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v6

    iput-wide v6, p0, Lcom/google/android/gms/cast/b/af;->h:D

    .line 319
    const-string v4, "muted"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/af;->i:Z

    .line 321
    :cond_e
    iget-wide v6, p0, Lcom/google/android/gms/cast/b/af;->h:D

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/cast/internal/DeviceStatus;->a(D)V

    .line 322
    iget-boolean v0, p0, Lcom/google/android/gms/cast/b/af;->i:Z

    invoke-virtual {v5, v0}, Lcom/google/android/gms/cast/internal/DeviceStatus;->a(Z)V

    .line 324
    invoke-virtual {p0, v3, v8, v5}, Lcom/google/android/gms/cast/b/af;->a(Lcom/google/android/gms/cast/b/b;ZLcom/google/android/gms/cast/internal/DeviceStatus;)V

    goto/16 :goto_3

    :cond_f
    move v0, v1

    .line 282
    goto/16 :goto_4

    :cond_10
    move v0, v1

    .line 306
    goto :goto_5

    .line 307
    :catchall_0
    move-exception v0

    monitor-exit v9

    throw v0

    .line 325
    :cond_11
    const-string v5, "LAUNCH_ERROR"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_17

    .line 327
    const-string v3, "reason"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 328
    const-string v4, "BAD_PARAMETER"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_13

    .line 329
    const/16 v0, 0x7d1

    .line 341
    :cond_12
    :goto_6
    iget-object v3, p0, Lcom/google/android/gms/cast/b/af;->c:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v3, v6, v7, v0}, Lcom/google/android/gms/cast/internal/p;->a(JI)Z

    goto/16 :goto_3

    .line 330
    :cond_13
    const-string v4, "CANCELLED"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 331
    const/16 v0, 0x7d2

    goto :goto_6

    .line 332
    :cond_14
    const-string v4, "NOT_ALLOWED"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_15

    .line 333
    const/16 v0, 0x7d3

    goto :goto_6

    .line 334
    :cond_15
    const-string v4, "NOT_FOUND"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_16

    .line 335
    const/16 v0, 0x7d4

    goto :goto_6

    .line 336
    :cond_16
    const-string v4, "CAST_INIT_TIMEOUT"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    .line 337
    const/16 v0, 0xf

    goto :goto_6

    .line 342
    :cond_17
    const-string v5, "INVALID_REQUEST"

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 343
    const-string v3, "reason"

    invoke-virtual {v4, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 347
    const-string v4, "INVALID_COMMAND"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_18

    const-string v4, "DUPLICATE_REQUEST_ID"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_18

    const-string v4, "INVALID_SESSION_ID"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_19

    .line 350
    :cond_18
    const/16 v0, 0x7d1

    .line 353
    :cond_19
    iget-object v3, p0, Lcom/google/android/gms/cast/b/af;->c:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v3, v6, v7, v0}, Lcom/google/android/gms/cast/internal/p;->a(JI)Z

    .line 354
    iget-object v3, p0, Lcom/google/android/gms/cast/b/af;->d:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v3, v6, v7, v0}, Lcom/google/android/gms/cast/internal/p;->a(JI)Z

    .line 355
    iget-object v3, p0, Lcom/google/android/gms/cast/b/af;->e:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v3, v6, v7, v0}, Lcom/google/android/gms/cast/internal/p;->a(JI)Z
    :try_end_5
    .catch Lorg/json/JSONException; {:try_start_5 .. :try_end_5} :catch_1

    goto/16 :goto_3
.end method

.method public final b()V
    .locals 5

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->e:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/p;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    :goto_0
    return-void

    .line 133
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/af;->c()J

    move-result-wide v0

    .line 134
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 136
    :try_start_0
    const-string v3, "requestId"

    invoke-virtual {v2, v3, v0, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 137
    const-string v3, "type"

    const-string v4, "GET_STATUS"

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    :goto_1
    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/cast/b/af;->a:Ljava/lang/String;

    invoke-virtual {p0, v2, v0, v1, v3}, Lcom/google/android/gms/cast/b/af;->a(Ljava/lang/String;JLjava/lang/String;)V

    .line 142
    iget-object v2, p0, Lcom/google/android/gms/cast/b/af;->e:Lcom/google/android/gms/cast/internal/p;

    iget-object v3, p0, Lcom/google/android/gms/cast/b/af;->q:Lcom/google/android/gms/cast/internal/o;

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/gms/cast/internal/p;->a(JLcom/google/android/gms/cast/internal/o;)V

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method protected abstract b(I)V
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 108
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/af;->c()J

    move-result-wide v2

    .line 111
    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 112
    const-string v1, "type"

    const-string v4, "STOP"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 113
    if-eqz p1, :cond_0

    const-string v1, ""

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 114
    const-string v1, "sessionId"

    invoke-virtual {v0, v1, p1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 118
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/cast/b/af;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/google/android/gms/cast/b/af;->a(Ljava/lang/String;JLjava/lang/String;)V

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->d:Lcom/google/android/gms/cast/internal/p;

    iget-object v1, p0, Lcom/google/android/gms/cast/b/af;->p:Lcom/google/android/gms/cast/internal/o;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/cast/internal/p;->a(JLcom/google/android/gms/cast/internal/o;)V

    .line 120
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method protected abstract c(I)V
.end method

.method public final c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 377
    iget-object v0, p0, Lcom/google/android/gms/cast/b/af;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "current transport id (in control channel) is now: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 378
    iput-object p1, p0, Lcom/google/android/gms/cast/b/af;->b:Ljava/lang/String;

    .line 379
    return-void
.end method
