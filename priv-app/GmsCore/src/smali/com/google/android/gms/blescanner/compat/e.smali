.class final Lcom/google/android/gms/blescanner/compat/e;
.super Lcom/google/android/gms/blescanner/compat/v;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/blescanner/compat/c;


# direct methods
.method constructor <init>(Lcom/google/android/gms/blescanner/compat/c;)V
    .locals 0

    .prologue
    .line 227
    iput-object p1, p0, Lcom/google/android/gms/blescanner/compat/e;->a:Lcom/google/android/gms/blescanner/compat/c;

    invoke-direct {p0}, Lcom/google/android/gms/blescanner/compat/v;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 240
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Batch scan: onScanFailed("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Exception;

    invoke-static {v0, v1}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V

    .line 241
    return-void
.end method

.method public final a(ILcom/google/android/gms/blescanner/compat/ScanResult;)V
    .locals 2

    .prologue
    .line 235
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Batch scan: unexpected call to onScanResult("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Exception;

    invoke-static {v0, v1}, Lcom/google/android/gms/blescanner/b/e;->a(Ljava/lang/String;[Ljava/lang/Exception;)V

    .line 236
    return-void
.end method

.method public final a(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/gms/blescanner/compat/e;->a:Lcom/google/android/gms/blescanner/compat/c;

    invoke-static {v0, p1}, Lcom/google/android/gms/blescanner/compat/c;->a(Lcom/google/android/gms/blescanner/compat/c;Ljava/util/List;)V

    .line 231
    return-void
.end method
