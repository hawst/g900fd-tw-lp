.class public Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;
.super Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:Z

.field public final c:Z

.field public final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/i;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/i;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(IZZZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>()V

    .line 28
    iput-object p5, p0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->j:Ljava/lang/String;

    .line 29
    iput p1, p0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->a:I

    .line 30
    iput-boolean p2, p0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->b:Z

    .line 31
    iput-boolean p3, p0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->c:Z

    .line 32
    iput-boolean p4, p0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->d:Z

    .line 33
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>(Landroid/os/Parcel;)V

    .line 37
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->a:I

    .line 38
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->b:Z

    .line 39
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->c:Z

    .line 40
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_2

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->d:Z

    .line 41
    return-void

    :cond_0
    move v0, v2

    .line 38
    goto :goto_0

    :cond_1
    move v0, v2

    .line 39
    goto :goto_1

    :cond_2
    move v1, v2

    .line 40
    goto :goto_2
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static a(Landroid/content/Context;IZZZLjava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;-><init>(IZZZLjava/lang/String;)V

    .line 47
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 48
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 58
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 59
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 60
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 61
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->c:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 62
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->d:Z

    if-eqz v0, :cond_2

    :goto_2
    int-to-byte v0, v1

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 63
    return-void

    :cond_0
    move v0, v2

    .line 60
    goto :goto_0

    :cond_1
    move v0, v2

    .line 61
    goto :goto_1

    :cond_2
    move v1, v2

    .line 62
    goto :goto_2
.end method
