.class public Lcom/google/android/gms/wallet/analytics/events/OcrActivityLaunchedEvent;
.super Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/g;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/OcrActivityLaunchedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>(Landroid/os/Parcel;)V

    .line 26
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/OcrActivityLaunchedEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/gms/wallet/analytics/events/OcrActivityLaunchedEvent;->j:Ljava/lang/String;

    .line 22
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/OcrActivityLaunchedEvent;

    invoke-direct {v0, p1}, Lcom/google/android/gms/wallet/analytics/events/OcrActivityLaunchedEvent;-><init>(Ljava/lang/String;)V

    .line 31
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 32
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/OcrActivityLaunchedEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 43
    return-void
.end method
