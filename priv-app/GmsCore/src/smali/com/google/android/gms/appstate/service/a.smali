.class public final Lcom/google/android/gms/appstate/service/a;
.super Lcom/google/android/gms/appstate/c/l;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/common/server/ClientContext;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 165
    invoke-direct {p0}, Lcom/google/android/gms/appstate/c/l;-><init>()V

    .line 167
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/a/d;->a(Landroid/content/Context;)V

    .line 169
    iput-object p1, p0, Lcom/google/android/gms/appstate/service/a;->a:Landroid/content/Context;

    .line 170
    iput-object p2, p0, Lcom/google/android/gms/appstate/service/a;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 171
    iput-object p3, p0, Lcom/google/android/gms/appstate/service/a;->c:Ljava/lang/String;

    .line 172
    return-void
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 203
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/service/a;->b()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 204
    const/4 v0, 0x1

    .line 208
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 194
    sget-object v0, Lcom/google/android/gms/appstate/b/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/gms/appstate/c/h;)V
    .locals 3

    .prologue
    .line 238
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/appstate/service/a;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lcom/google/android/gms/appstate/service/a;->c:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;)V

    .line 240
    return-void
.end method

.method public final a(Lcom/google/android/gms/appstate/c/h;I)V
    .locals 5

    .prologue
    .line 228
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    invoke-direct {p0, p2}, Lcom/google/android/gms/appstate/service/a;->a(I)Z

    move-result v0

    const-string v1, "State key is out of bounds: %d is not between 0 and %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/service/a;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/appstate/service/a;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lcom/google/android/gms/appstate/service/a;->c:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2, p2}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;I)V

    .line 234
    return-void
.end method

.method public final a(Lcom/google/android/gms/appstate/c/h;ILjava/lang/String;[B)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 245
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 246
    invoke-direct {p0, p2}, Lcom/google/android/gms/appstate/service/a;->a(I)Z

    move-result v0

    const-string v3, "State key is out of bounds: %d is not between 0 and %d"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/service/a;->b()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 249
    const-string v0, "Must provide a non-null resolved version"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    if-eqz p4, :cond_0

    .line 251
    array-length v0, p4

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/service/a;->a()I

    move-result v3

    if-gt v0, v3, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "App state data is too large (%d bytes). The maximum is %d"

    new-array v4, v6, [Ljava/lang/Object;

    array-length v5, p4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/service/a;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/appstate/service/a;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lcom/google/android/gms/appstate/service/a;->c:Ljava/lang/String;

    move-object v2, p1

    move v4, p2

    move-object v5, p3

    move-object v6, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;ILjava/lang/String;[B)V

    .line 257
    return-void

    :cond_1
    move v0, v2

    .line 251
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/appstate/c/h;I[B)V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 214
    invoke-direct {p0, p2}, Lcom/google/android/gms/appstate/service/a;->a(I)Z

    move-result v0

    const-string v3, "State key is out of bounds: %d is not between 0 and %d"

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/service/a;->b()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 217
    if-eqz p3, :cond_0

    .line 218
    array-length v0, p3

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/service/a;->a()I

    move-result v3

    if-gt v0, v3, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "App state data is too large (%d bytes). The maximum is %d"

    new-array v4, v6, [Ljava/lang/Object;

    array-length v5, p3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/service/a;->a()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v1

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/appstate/service/a;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lcom/google/android/gms/appstate/service/a;->c:Ljava/lang/String;

    move-object v2, p1

    move v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;I[B)V

    .line 224
    return-void

    :cond_1
    move v0, v2

    .line 218
    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 199
    sget-object v0, Lcom/google/android/gms/appstate/b/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/appstate/c/h;)V
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/appstate/service/a;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/a;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 280
    invoke-static {}, Lcom/google/android/gms/appstate/service/AppStateAndroidService;->a()V

    .line 282
    if-eqz p1, :cond_0

    .line 284
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/appstate/c/h;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 289
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/appstate/c/h;I)V
    .locals 5

    .prologue
    .line 261
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 262
    invoke-direct {p0, p2}, Lcom/google/android/gms/appstate/service/a;->a(I)Z

    move-result v0

    const-string v1, "State key is out of bounds: %d is not between 0 and %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/appstate/service/a;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 265
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/appstate/service/a;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lcom/google/android/gms/appstate/service/a;->c:Ljava/lang/String;

    invoke-static {v0, v1, p1, v2, p2}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;I)V

    .line 267
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/appstate/c/h;)V
    .locals 2

    .prologue
    .line 271
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    .line 273
    :cond_0
    const-string v0, "Must provide a valid callback object"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 274
    iget-object v0, p0, Lcom/google/android/gms/appstate/service/a;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/appstate/service/a;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;)V

    .line 275
    return-void
.end method
