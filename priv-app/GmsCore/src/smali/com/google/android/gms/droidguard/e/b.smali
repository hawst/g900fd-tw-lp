.class final Lcom/google/android/gms/droidguard/e/b;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/droidguard/e/a;

.field private final b:Landroid/media/MediaDrm;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/droidguard/e/a;Landroid/media/MediaDrm;)V
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/gms/droidguard/e/b;->a:Lcom/google/android/gms/droidguard/e/a;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/droidguard/e/b;->b:Landroid/media/MediaDrm;

    .line 38
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/e/b;->a:Lcom/google/android/gms/droidguard/e/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/droidguard/e/a;->a(Lcom/google/android/gms/droidguard/e/a;Z)Z

    iget-object v0, p0, Lcom/google/android/gms/droidguard/e/b;->a:Lcom/google/android/gms/droidguard/e/a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/droidguard/e/a;->b(Lcom/google/android/gms/droidguard/e/a;Z)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    new-instance v0, Lcom/google/android/gms/droidguard/e/d;

    iget-object v1, p0, Lcom/google/android/gms/droidguard/e/b;->a:Lcom/google/android/gms/droidguard/e/a;

    invoke-static {v1}, Lcom/google/android/gms/droidguard/e/a;->a(Lcom/google/android/gms/droidguard/e/a;)Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/droidguard/e/d;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/gms/droidguard/e/b;->b:Landroid/media/MediaDrm;

    invoke-virtual {v1}, Landroid/media/MediaDrm;->getProvisionRequest()Landroid/media/MediaDrm$ProvisionRequest;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/media/MediaDrm$ProvisionRequest;->getDefaultUrl()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "&signedRequest="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/lang/String;

    invoke-virtual {v1}, Landroid/media/MediaDrm$ProvisionRequest;->getData()[B

    move-result-object v1

    const-string v4, "UTF-8"

    invoke-direct {v3, v1, v4}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [B

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/droidguard/e/d;->a(Ljava/lang/String;[B)[B

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/droidguard/e/b;->a:Lcom/google/android/gms/droidguard/e/a;

    iget-object v2, p0, Lcom/google/android/gms/droidguard/e/b;->b:Landroid/media/MediaDrm;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/droidguard/e/a;->a(Lcom/google/android/gms/droidguard/e/a;[BLandroid/media/MediaDrm;)V

    iget-object v0, p0, Lcom/google/android/gms/droidguard/e/b;->a:Lcom/google/android/gms/droidguard/e/a;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/droidguard/e/a;->a(Lcom/google/android/gms/droidguard/e/a;Z)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/droidguard/e/b;->a:Lcom/google/android/gms/droidguard/e/a;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/droidguard/e/a;->b(Lcom/google/android/gms/droidguard/e/a;Z)Z

    .line 47
    :goto_1
    return-void

    .line 43
    :catch_0
    move-exception v0

    const-string v1, "DG.WV"

    const-string v2, "Could not communicate with the provisioning server."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 44
    :catch_1
    move-exception v0

    .line 45
    const-string v1, "DG.WV"

    const-string v2, "Could not process the provisioning request."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method
