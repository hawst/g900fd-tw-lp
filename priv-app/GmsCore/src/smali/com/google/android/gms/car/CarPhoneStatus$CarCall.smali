.class public Lcom/google/android/gms/car/CarPhoneStatus$CarCall;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:I

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/gms/car/fy;

    invoke-direct {v0}, Lcom/google/android/gms/car/fy;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->a:I

    .line 87
    return-void
.end method

.method public constructor <init>(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 76
    iput p1, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->a:I

    .line 77
    iput p2, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->b:I

    .line 78
    iput-object p4, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->d:Ljava/lang/String;

    .line 79
    iput p3, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->c:I

    .line 80
    iput-object p5, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->e:Ljava/lang/String;

    .line 81
    iput-object p6, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->f:Ljava/lang/String;

    .line 82
    iput-object p7, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->g:[B

    .line 83
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 103
    iget v0, p0, Lcom/google/android/gms/car/CarPhoneStatus$CarCall;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 96
    invoke-static {p0, p1}, Lcom/google/android/gms/car/fy;->a(Lcom/google/android/gms/car/CarPhoneStatus$CarCall;Landroid/os/Parcel;)V

    .line 97
    return-void
.end method
