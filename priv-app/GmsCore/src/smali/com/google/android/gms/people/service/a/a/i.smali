.class public final Lcom/google/android/gms/people/service/a/a/i;
.super Lcom/google/android/gms/people/service/a/a/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Lcom/google/android/gms/people/model/AvatarReference;Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct/range {p0 .. p6}, Lcom/google/android/gms/people/service/a/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Lcom/google/android/gms/people/model/AvatarReference;Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;)V

    .line 26
    return-void
.end method


# virtual methods
.method protected final g()[B
    .locals 8

    .prologue
    const/4 v6, 0x2

    const/16 v7, 0x9

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 30
    iget-object v3, p0, Lcom/google/android/gms/people/service/a/a/i;->g:Lcom/google/android/gms/people/model/AvatarReference;

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, v3, Lcom/google/android/gms/people/model/AvatarReference;->a:I

    if-ne v0, v6, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v0, v3, Lcom/google/android/gms/people/model/AvatarReference;->b:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    iget-object v0, v3, Lcom/google/android/gms/people/model/AvatarReference;->b:Ljava/lang/String;

    add-int/lit8 v5, v4, 0x1

    invoke-virtual {v0, v7, v5}, Ljava/lang/String;->indexOf(II)I

    move-result v5

    if-lez v4, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    if-lez v5, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v0, v3, Lcom/google/android/gms/people/model/AvatarReference;->b:Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 32
    iget-object v4, p0, Lcom/google/android/gms/people/service/a/a/i;->g:Lcom/google/android/gms/people/model/AvatarReference;

    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget v0, v4, Lcom/google/android/gms/people/model/AvatarReference;->a:I

    if-ne v0, v6, :cond_4

    move v0, v1

    :goto_3
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v0, v4, Lcom/google/android/gms/people/model/AvatarReference;->b:Ljava/lang/String;

    invoke-virtual {v0, v7}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    iget-object v0, v4, Lcom/google/android/gms/people/model/AvatarReference;->b:Ljava/lang/String;

    add-int/lit8 v6, v5, 0x1

    invoke-virtual {v0, v7, v6}, Ljava/lang/String;->indexOf(II)I

    move-result v6

    if-lez v5, :cond_5

    move v0, v1

    :goto_4
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    if-lez v6, :cond_6

    move v0, v1

    :goto_5
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    iget-object v0, v4, Lcom/google/android/gms/people/model/AvatarReference;->b:Ljava/lang/String;

    add-int/lit8 v4, v5, 0x1

    invoke-virtual {v0, v4, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 35
    const/4 v4, 0x3

    invoke-static {v4}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 36
    const-string v4, "PeopleService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "load contact image: account="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " fid="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 40
    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/people/service/a/a/i;->a:Landroid/content/Context;

    invoke-static {v4, v3, v0}, Lcom/google/android/gms/people/b/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v4

    .line 42
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gez v0, :cond_7

    .line 43
    const/4 v0, 0x0

    .line 46
    :goto_6
    return-object v0

    :cond_1
    move v0, v2

    .line 30
    goto/16 :goto_0

    :cond_2
    move v0, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    move v0, v2

    .line 32
    goto :goto_3

    :cond_5
    move v0, v2

    goto :goto_4

    :cond_6
    move v0, v2

    goto :goto_5

    .line 45
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a/i;->h:Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;->d()Z

    move-result v0

    if-nez v0, :cond_8

    .line 46
    :goto_7
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/a/i;->a:Landroid/content/Context;

    invoke-static {v0, v4, v5, v1}, Lcom/google/android/gms/people/b/a;->a(Landroid/content/Context;JZ)[B

    move-result-object v0

    goto :goto_6

    :cond_8
    move v1, v2

    .line 45
    goto :goto_7
.end method
