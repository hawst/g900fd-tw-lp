.class public final Lcom/google/android/gms/wearable/node/bluetooth/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/Object;

.field b:Lcom/google/android/gms/wearable/node/bluetooth/d;

.field c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

.field final d:Lcom/google/android/gms/wearable/node/bluetooth/h;

.field private final e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->a:Ljava/lang/Object;

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->e:Landroid/content/Context;

    .line 35
    new-instance v0, Lcom/google/android/gms/wearable/node/bluetooth/h;

    invoke-direct {v0, p1}, Lcom/google/android/gms/wearable/node/bluetooth/h;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->d:Lcom/google/android/gms/wearable/node/bluetooth/h;

    .line 36
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 42
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 43
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->b:Lcom/google/android/gms/wearable/node/bluetooth/d;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final b()V
    .locals 6

    .prologue
    const/16 v4, 0xc

    const/4 v0, 0x0

    .line 108
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 109
    :try_start_0
    const-string v2, "WearableBluetooth"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 110
    const-string v2, "WearableBluetooth"

    const-string v3, "updateConnectionState"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_0
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothAdapter;->getState()I

    move-result v3

    if-eq v3, v4, :cond_2

    const-string v2, "WearableBluetooth"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "WearableBluetooth"

    const-string v3, "  bluetooth adapter not on"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    if-nez v0, :cond_5

    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/node/bluetooth/a;->c()V

    .line 115
    monitor-exit v1

    .line 131
    :goto_1
    return-void

    .line 113
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v3}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v2

    invoke-virtual {v2}, Landroid/bluetooth/BluetoothDevice;->getBondState()I

    move-result v2

    if-eq v2, v4, :cond_3

    const-string v2, "WearableBluetooth"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "WearableBluetooth"

    const-string v3, "  bluetooth device not bonded"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 113
    :cond_3
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->h()Z

    move-result v2

    if-nez v2, :cond_4

    const-string v2, "WearableBluetooth"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "WearableBluetooth"

    const-string v3, "  bluetooth device connection is not enabled"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_4
    const/4 v0, 0x1

    goto :goto_0

    .line 118
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->b:Lcom/google/android/gms/wearable/node/bluetooth/d;

    if-eqz v0, :cond_7

    .line 119
    const-string v0, "WearableBluetooth"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 120
    const-string v0, "WearableBluetooth"

    const-string v2, "  already started"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :cond_6
    monitor-exit v1

    goto :goto_1

    .line 125
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->d:Lcom/google/android/gms/wearable/node/bluetooth/h;

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wearable/node/bluetooth/h;->b(Ljava/lang/String;)V

    .line 126
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    invoke-virtual {v2}, Lcom/google/android/gms/wearable/ConnectionConfiguration;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/bluetooth/BluetoothAdapter;->getRemoteDevice(Ljava/lang/String;)Landroid/bluetooth/BluetoothDevice;

    move-result-object v0

    .line 128
    new-instance v2, Lcom/google/android/gms/wearable/node/bluetooth/d;

    iget-object v3, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->e:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->c:Lcom/google/android/gms/wearable/ConnectionConfiguration;

    iget-object v5, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->d:Lcom/google/android/gms/wearable/node/bluetooth/h;

    invoke-direct {v2, v3, v0, v4, v5}, Lcom/google/android/gms/wearable/node/bluetooth/d;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothDevice;Lcom/google/android/gms/wearable/ConnectionConfiguration;Lcom/google/android/gms/wearable/node/bluetooth/h;)V

    iput-object v2, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->b:Lcom/google/android/gms/wearable/node/bluetooth/d;

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->b:Lcom/google/android/gms/wearable/node/bluetooth/d;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->start()V

    .line 131
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_1
.end method

.method final c()V
    .locals 3

    .prologue
    .line 162
    iget-object v1, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 163
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->b:Lcom/google/android/gms/wearable/node/bluetooth/d;

    if-eqz v0, :cond_1

    .line 164
    const-string v0, "WearableBluetooth"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 165
    const-string v0, "WearableBluetooth"

    const-string v2, "Interrupting bluetooth thread"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->b:Lcom/google/android/gms/wearable/node/bluetooth/d;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/bluetooth/d;->a()V

    .line 168
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->b:Lcom/google/android/gms/wearable/node/bluetooth/d;

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/bluetooth/a;->d:Lcom/google/android/gms/wearable/node/bluetooth/h;

    invoke-virtual {v0}, Lcom/google/android/gms/wearable/node/bluetooth/h;->b()V

    .line 171
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
