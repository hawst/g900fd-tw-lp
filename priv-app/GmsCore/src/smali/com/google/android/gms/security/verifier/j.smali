.class final Lcom/google/android/gms/security/verifier/j;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 67
    const-string v0, "upload_queue.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 68
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1

    .prologue
    .line 72
    const-string v0, "CREATE TABLE upload_queue (id INTEGER PRIMARY KEY AUTOINCREMENT, package_name STRING, version_code INTEGER, sha256_digest BLOB, timestamp INTEGER, state INTEGER, last_update_time INTEGER, upload_attempts INTEGER);"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 73
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 86
    :try_start_0
    const-string v0, "DROP TABLE upload_queue"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/security/verifier/j;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 87
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
