.class final Lcom/google/android/gms/ads/internal/a/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/g/m;


# instance fields
.field final synthetic a:Lcom/google/android/gms/ads/internal/a/d;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/a/d;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/a/l;->a:Lcom/google/android/gms/ads/internal/a/d;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/l;->a:Lcom/google/android/gms/ads/internal/a/d;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/ads/internal/a/d;->a(Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 370
    :goto_0
    return-void

    .line 368
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Received request to untrack: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/l;->a:Lcom/google/android/gms/ads/internal/a/d;

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/a/d;->b(Lcom/google/android/gms/ads/internal/a/d;)Lcom/google/android/gms/ads/internal/a/b;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/a/b;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 369
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/l;->a:Lcom/google/android/gms/ads/internal/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/a/d;->b()V

    goto :goto_0
.end method
