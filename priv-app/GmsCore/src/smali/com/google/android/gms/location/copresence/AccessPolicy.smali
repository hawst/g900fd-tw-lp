.class public Lcom/google/android/gms/location/copresence/AccessPolicy;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final a:Lcom/google/android/gms/common/people/data/Audience;


# instance fields
.field private final b:I

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:J

.field private final f:Lcom/google/android/gms/location/copresence/AccessLock;

.field private final g:Lcom/google/android/gms/common/people/data/Audience;

.field private final h:I

.field private final i:I

.field private final j:Lcom/google/android/gms/location/copresence/AclResourceId;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/gms/location/copresence/d;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/location/copresence/AccessPolicy;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 61
    new-instance v0, Lcom/google/android/gms/common/people/data/a;

    invoke-direct {v0}, Lcom/google/android/gms/common/people/data/a;-><init>()V

    const-string v1, "public"

    const-string v2, "Public"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/people/data/a;->a(Ljava/util/Collection;)Lcom/google/android/gms/common/people/data/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/a;->a()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/copresence/AccessPolicy;->a:Lcom/google/android/gms/common/people/data/Audience;

    return-void
.end method

.method constructor <init>(IILjava/lang/String;JLcom/google/android/gms/location/copresence/AccessLock;Lcom/google/android/gms/common/people/data/Audience;IILcom/google/android/gms/location/copresence/AclResourceId;)V
    .locals 0

    .prologue
    .line 131
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    iput p1, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->b:I

    .line 133
    iput p2, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->c:I

    .line 134
    iput-object p3, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->d:Ljava/lang/String;

    .line 135
    iput-wide p4, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->e:J

    .line 136
    iput-object p6, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->f:Lcom/google/android/gms/location/copresence/AccessLock;

    .line 137
    iput-object p7, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->g:Lcom/google/android/gms/common/people/data/Audience;

    .line 138
    iput p8, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->h:I

    .line 139
    iput p9, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->i:I

    .line 140
    iput-object p10, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->j:Lcom/google/android/gms/location/copresence/AclResourceId;

    .line 141
    return-void
.end method

.method private constructor <init>(ILjava/lang/String;JLcom/google/android/gms/location/copresence/AccessLock;Lcom/google/android/gms/common/people/data/Audience;IILcom/google/android/gms/location/copresence/AclResourceId;)V
    .locals 11

    .prologue
    .line 146
    const/4 v1, 0x1

    move-object v0, p0

    move v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/location/copresence/AccessPolicy;-><init>(IILjava/lang/String;JLcom/google/android/gms/location/copresence/AccessLock;Lcom/google/android/gms/common/people/data/Audience;IILcom/google/android/gms/location/copresence/AclResourceId;)V

    .line 148
    return-void
.end method

.method public synthetic constructor <init>(ILjava/lang/String;JLcom/google/android/gms/location/copresence/AccessLock;Lcom/google/android/gms/common/people/data/Audience;IILcom/google/android/gms/location/copresence/AclResourceId;B)V
    .locals 1

    .prologue
    .line 28
    invoke-direct/range {p0 .. p9}, Lcom/google/android/gms/location/copresence/AccessPolicy;-><init>(ILjava/lang/String;JLcom/google/android/gms/location/copresence/AccessLock;Lcom/google/android/gms/common/people/data/Audience;IILcom/google/android/gms/location/copresence/AclResourceId;)V

    return-void
.end method

.method public static synthetic o()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/gms/location/copresence/AccessPolicy;->a:Lcom/google/android/gms/common/people/data/Audience;

    return-object v0
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 151
    iget v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->b:I

    return v0
.end method

.method final b()I
    .locals 1

    .prologue
    .line 155
    iget v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->c:I

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 159
    iget v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->c:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->d:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 370
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 167
    iget v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->c:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 171
    iget-wide v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->e:J

    return-wide v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 175
    iget v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->c:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Lcom/google/android/gms/location/copresence/AccessLock;
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->f:Lcom/google/android/gms/location/copresence/AccessLock;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 183
    iget v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->c:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->g:Lcom/google/android/gms/common/people/data/Audience;

    return-object v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 191
    iget v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->h:I

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 195
    iget v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->i:I

    return v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 199
    iget v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->c:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Lcom/google/android/gms/location/copresence/AclResourceId;
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/AccessPolicy;->j:Lcom/google/android/gms/location/copresence/AclResourceId;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 375
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/location/copresence/d;->a(Lcom/google/android/gms/location/copresence/AccessPolicy;Landroid/os/Parcel;I)V

    .line 376
    return-void
.end method
