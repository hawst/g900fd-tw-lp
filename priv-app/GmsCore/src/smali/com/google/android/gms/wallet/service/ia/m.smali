.class public abstract Lcom/google/android/gms/wallet/service/ia/m;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/service/ia/l;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 15
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/wallet/service/ia/m;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 16
    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/wallet/service/ia/l;
    .locals 2

    .prologue
    .line 23
    if-nez p0, :cond_0

    .line 24
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    .line 26
    :cond_0
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 27
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/wallet/service/ia/l;

    if-eqz v1, :cond_1

    .line 28
    check-cast v0, Lcom/google/android/gms/wallet/service/ia/l;

    goto :goto_0

    .line 30
    :cond_1
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/service/ia/n;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 34
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 38
    sparse-switch p1, :sswitch_data_0

    .line 410
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 42
    :sswitch_0
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v3

    .line 43
    goto :goto_0

    .line 47
    :sswitch_1
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 49
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v1, v0

    .line 56
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    sget-object v0, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;

    .line 62
    :goto_2
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 63
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 64
    if-eqz v0, :cond_2

    .line 65
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 66
    invoke-virtual {v0, p3, v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_3
    move v0, v3

    .line 71
    goto :goto_0

    :cond_0
    move-object v1, v2

    .line 53
    goto :goto_1

    :cond_1
    move-object v0, v2

    .line 60
    goto :goto_2

    .line 69
    :cond_2
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_3

    .line 75
    :sswitch_2
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    .line 78
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v1, v0

    .line 84
    :goto_4
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 85
    sget-object v0, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;

    .line 90
    :goto_5
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 91
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 92
    if-eqz v0, :cond_5

    .line 93
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 94
    invoke-virtual {v0, p3, v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_6
    move v0, v3

    .line 99
    goto :goto_0

    :cond_3
    move-object v1, v2

    .line 81
    goto :goto_4

    :cond_4
    move-object v0, v2

    .line 88
    goto :goto_5

    .line 97
    :cond_5
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_6

    .line 103
    :sswitch_3
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 105
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    .line 106
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v1, v0

    .line 112
    :goto_7
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    .line 113
    sget-object v0, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;

    .line 118
    :goto_8
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 119
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 120
    if-eqz v0, :cond_8

    .line 121
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 122
    invoke-virtual {v0, p3, v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_9
    move v0, v3

    .line 127
    goto/16 :goto_0

    :cond_6
    move-object v1, v2

    .line 109
    goto :goto_7

    :cond_7
    move-object v0, v2

    .line 116
    goto :goto_8

    .line 125
    :cond_8
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_9

    .line 131
    :sswitch_4
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 133
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    .line 134
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v1, v0

    .line 140
    :goto_a
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_a

    .line 141
    sget-object v0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;

    .line 146
    :goto_b
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 147
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 148
    if-eqz v0, :cond_b

    .line 149
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 150
    invoke-virtual {v0, p3, v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_c
    move v0, v3

    .line 155
    goto/16 :goto_0

    :cond_9
    move-object v1, v2

    .line 137
    goto :goto_a

    :cond_a
    move-object v0, v2

    .line 144
    goto :goto_b

    .line 153
    :cond_b
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_c

    .line 159
    :sswitch_5
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 161
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_c

    .line 162
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v1, v0

    .line 168
    :goto_d
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_d

    .line 169
    sget-object v0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;

    .line 174
    :goto_e
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 175
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 176
    if-eqz v0, :cond_e

    .line 177
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 178
    invoke-virtual {v0, p3, v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_f
    move v0, v3

    .line 183
    goto/16 :goto_0

    :cond_c
    move-object v1, v2

    .line 165
    goto :goto_d

    :cond_d
    move-object v0, v2

    .line 172
    goto :goto_e

    .line 181
    :cond_e
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_f

    .line 187
    :sswitch_6
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 189
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_f

    .line 190
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v1, v0

    .line 196
    :goto_10
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_10

    .line 197
    sget-object v0, Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;

    .line 202
    :goto_11
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 203
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 204
    if-eqz v0, :cond_11

    .line 205
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 206
    invoke-virtual {v0, p3, v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_12
    move v0, v3

    .line 211
    goto/16 :goto_0

    :cond_f
    move-object v1, v2

    .line 193
    goto :goto_10

    :cond_10
    move-object v0, v2

    .line 200
    goto :goto_11

    .line 209
    :cond_11
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_12

    .line 215
    :sswitch_7
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 217
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_12

    .line 218
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v1, v0

    .line 224
    :goto_13
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_13

    .line 225
    sget-object v0, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;

    .line 230
    :goto_14
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 231
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 232
    if-eqz v0, :cond_14

    .line 233
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 234
    invoke-virtual {v0, p3, v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_15
    move v0, v3

    .line 239
    goto/16 :goto_0

    :cond_12
    move-object v1, v2

    .line 221
    goto :goto_13

    :cond_13
    move-object v0, v2

    .line 228
    goto :goto_14

    .line 237
    :cond_14
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_15

    .line 243
    :sswitch_8
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 245
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_15

    .line 246
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v1, v0

    .line 252
    :goto_16
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_16

    .line 253
    sget-object v0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;

    .line 258
    :goto_17
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 259
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 260
    if-eqz v0, :cond_17

    .line 261
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 262
    invoke-virtual {v0, p3, v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_18
    move v0, v3

    .line 267
    goto/16 :goto_0

    :cond_15
    move-object v1, v2

    .line 249
    goto :goto_16

    :cond_16
    move-object v0, v2

    .line 256
    goto :goto_17

    .line 265
    :cond_17
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_18

    .line 271
    :sswitch_9
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 273
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_18

    .line 274
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v1, v0

    .line 280
    :goto_19
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_19

    .line 281
    sget-object v0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;

    .line 286
    :goto_1a
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 287
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 288
    if-eqz v0, :cond_1a

    .line 289
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 290
    invoke-virtual {v0, p3, v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_1b
    move v0, v3

    .line 295
    goto/16 :goto_0

    :cond_18
    move-object v1, v2

    .line 277
    goto :goto_19

    :cond_19
    move-object v0, v2

    .line 284
    goto :goto_1a

    .line 293
    :cond_1a
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1b

    .line 299
    :sswitch_a
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 301
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1b

    .line 302
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v1, v0

    .line 308
    :goto_1c
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1c

    .line 309
    sget-object v0, Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;

    .line 314
    :goto_1d
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 315
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 316
    if-eqz v0, :cond_1d

    .line 317
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 318
    invoke-virtual {v0, p3, v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_1e
    move v0, v3

    .line 323
    goto/16 :goto_0

    :cond_1b
    move-object v1, v2

    .line 305
    goto :goto_1c

    :cond_1c
    move-object v0, v2

    .line 312
    goto :goto_1d

    .line 321
    :cond_1d
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_1e

    .line 327
    :sswitch_b
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 329
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1e

    .line 330
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v1, v0

    .line 336
    :goto_1f
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1f

    .line 337
    sget-object v0, Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;

    .line 342
    :goto_20
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 343
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 344
    if-eqz v0, :cond_20

    .line 345
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 346
    invoke-virtual {v0, p3, v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_21
    move v0, v3

    .line 351
    goto/16 :goto_0

    :cond_1e
    move-object v1, v2

    .line 333
    goto :goto_1f

    :cond_1f
    move-object v0, v2

    .line 340
    goto :goto_20

    .line 349
    :cond_20
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_21

    .line 355
    :sswitch_c
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 357
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_21

    .line 358
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v1, v0

    .line 364
    :goto_22
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_22

    .line 365
    sget-object v0, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;

    .line 370
    :goto_23
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 371
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 372
    if-eqz v0, :cond_23

    .line 373
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 374
    invoke-virtual {v0, p3, v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_24
    move v0, v3

    .line 379
    goto/16 :goto_0

    :cond_21
    move-object v1, v2

    .line 361
    goto :goto_22

    :cond_22
    move-object v0, v2

    .line 368
    goto :goto_23

    .line 377
    :cond_23
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_24

    .line 383
    :sswitch_d
    const-string v0, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 385
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_24

    .line 386
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-object v1, v0

    .line 392
    :goto_25
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_25

    .line 393
    sget-object v0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;

    .line 398
    :goto_26
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/service/ia/m;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 399
    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    .line 400
    if-eqz v0, :cond_26

    .line 401
    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 402
    invoke-virtual {v0, p3, v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->writeToParcel(Landroid/os/Parcel;I)V

    :goto_27
    move v0, v3

    .line 407
    goto/16 :goto_0

    :cond_24
    move-object v1, v2

    .line 389
    goto :goto_25

    :cond_25
    move-object v0, v2

    .line 396
    goto :goto_26

    .line 405
    :cond_26
    invoke-virtual {p3, v4}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_27

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
