.class public final Lcom/google/android/gms/people/d/h;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/people/d/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 797
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 798
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/d/h;->cachedSize:I

    .line 799
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 847
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 848
    iget-object v1, p0, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    if-eqz v1, :cond_0

    .line 849
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 852
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 809
    if-ne p1, p0, :cond_1

    .line 825
    :cond_0
    :goto_0
    return v0

    .line 812
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/people/d/h;

    if-nez v2, :cond_2

    move v0, v1

    .line 813
    goto :goto_0

    .line 815
    :cond_2
    check-cast p1, Lcom/google/android/gms/people/d/h;

    .line 816
    iget-object v2, p0, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    if-nez v2, :cond_3

    .line 817
    iget-object v2, p1, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    if-eqz v2, :cond_0

    move v0, v1

    .line 818
    goto :goto_0

    .line 821
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    iget-object v3, p1, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/people/d/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 822
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 830
    iget-object v0, p0, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 833
    return v0

    .line 830
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    invoke-virtual {v0}, Lcom/google/android/gms/people/d/c;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 777
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/people/d/c;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/c;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 839
    iget-object v0, p0, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    if-eqz v0, :cond_0

    .line 840
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 842
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 843
    return-void
.end method
