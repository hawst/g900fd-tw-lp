.class public final Lcom/google/android/gms/auth/be/proximity/authorization/a/c/i;
.super Lcom/google/android/gms/auth/be/proximity/authorization/d;
.source "SourceFile"


# instance fields
.field final a:[B


# direct methods
.method private constructor <init>([B)V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/be/proximity/authorization/d;-><init>(B)V

    .line 37
    iput-object p1, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/i;->a:[B

    .line 38
    return-void
.end method

.method public static a(Lorg/json/JSONObject;)Lcom/google/android/gms/auth/be/proximity/authorization/a/c/i;
    .locals 4

    .prologue
    .line 25
    :try_start_0
    new-instance v0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/i;

    const-string v1, "encrypted_data"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x8

    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/i;-><init>([B)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 27
    :catch_0
    move-exception v0

    .line 28
    new-instance v1, Lorg/json/JSONException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to decode the data."

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 46
    instance-of v0, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/i;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/i;->a:[B

    check-cast p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/i;

    iget-object v1, p1, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/i;->a:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    .line 50
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 55
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/i;->a:[B

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
