.class public final Lcom/google/android/gms/ads/internal/k/f;
.super Lcom/google/android/gms/ads/internal/k/a/h;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:Lcom/google/android/gms/ads/internal/k/j;

.field private c:Lcom/google/android/gms/ads/internal/k/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/k/a/h;-><init>()V

    .line 15
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->a:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 33
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/f;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 34
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->c:Lcom/google/android/gms/ads/internal/k/e;

    if-eqz v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->c:Lcom/google/android/gms/ads/internal/k/e;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/k/e;->i()V

    .line 37
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 53
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/f;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 54
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->b:Lcom/google/android/gms/ads/internal/k/j;

    if-eqz v0, :cond_0

    .line 56
    const/4 v0, 0x3

    if-ne p1, v0, :cond_1

    const/4 v0, 0x1

    .line 60
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/k/f;->b:Lcom/google/android/gms/ads/internal/k/j;

    invoke-interface {v2, v0}, Lcom/google/android/gms/ads/internal/k/j;->a(I)V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->b:Lcom/google/android/gms/ads/internal/k/j;

    .line 63
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    .line 56
    :cond_1
    const/4 v0, 0x2

    goto :goto_0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/k/e;)V
    .locals 2

    .prologue
    .line 108
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/f;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 109
    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/k/f;->c:Lcom/google/android/gms/ads/internal/k/e;

    .line 110
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/k/j;)V
    .locals 2

    .prologue
    .line 25
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/f;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 26
    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/k/f;->b:Lcom/google/android/gms/ads/internal/k/j;

    .line 27
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 43
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/f;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 44
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->c:Lcom/google/android/gms/ads/internal/k/e;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->c:Lcom/google/android/gms/ads/internal/k/e;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/k/e;->j()V

    .line 47
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 69
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/f;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 70
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->c:Lcom/google/android/gms/ads/internal/k/e;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->c:Lcom/google/android/gms/ads/internal/k/e;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/k/e;->k()V

    .line 73
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 79
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/f;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->c:Lcom/google/android/gms/ads/internal/k/e;

    if-eqz v0, :cond_0

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->c:Lcom/google/android/gms/ads/internal/k/e;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/k/e;->l()V

    .line 83
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 89
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/k/f;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->b:Lcom/google/android/gms/ads/internal/k/j;

    if-eqz v0, :cond_0

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->b:Lcom/google/android/gms/ads/internal/k/j;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/google/android/gms/ads/internal/k/j;->a(I)V

    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->b:Lcom/google/android/gms/ads/internal/k/j;

    .line 95
    monitor-exit v1

    .line 101
    :goto_0
    return-void

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->c:Lcom/google/android/gms/ads/internal/k/e;

    if-eqz v0, :cond_1

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/k/f;->c:Lcom/google/android/gms/ads/internal/k/e;

    invoke-interface {v0}, Lcom/google/android/gms/ads/internal/k/e;->m()V

    .line 101
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
