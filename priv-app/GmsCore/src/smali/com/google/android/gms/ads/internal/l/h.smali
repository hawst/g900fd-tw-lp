.class public final Lcom/google/android/gms/ads/internal/l/h;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Z

.field private final b:Z

.field private final c:Z

.field private final d:Z

.field private final e:Z


# direct methods
.method private constructor <init>(Lcom/google/android/gms/ads/internal/l/i;)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iget-boolean v0, p1, Lcom/google/android/gms/ads/internal/l/i;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/l/h;->a:Z

    .line 30
    iget-boolean v0, p1, Lcom/google/android/gms/ads/internal/l/i;->b:Z

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/l/h;->b:Z

    .line 31
    iget-boolean v0, p1, Lcom/google/android/gms/ads/internal/l/i;->c:Z

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/l/h;->c:Z

    .line 32
    iget-boolean v0, p1, Lcom/google/android/gms/ads/internal/l/i;->d:Z

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/l/h;->d:Z

    .line 33
    iget-boolean v0, p1, Lcom/google/android/gms/ads/internal/l/i;->e:Z

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/l/h;->e:Z

    .line 34
    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/ads/internal/l/i;B)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/google/android/gms/ads/internal/l/h;-><init>(Lcom/google/android/gms/ads/internal/l/i;)V

    return-void
.end method


# virtual methods
.method public final a()Lorg/json/JSONObject;
    .locals 3

    .prologue
    .line 98
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "sms"

    iget-boolean v2, p0, Lcom/google/android/gms/ads/internal/l/h;->a:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "tel"

    iget-boolean v2, p0, Lcom/google/android/gms/ads/internal/l/h;->b:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "calendar"

    iget-boolean v2, p0, Lcom/google/android/gms/ads/internal/l/h;->c:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "storePicture"

    iget-boolean v2, p0, Lcom/google/android/gms/ads/internal/l/h;->d:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "inlineVideo"

    iget-boolean v2, p0, Lcom/google/android/gms/ads/internal/l/h;->e:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 107
    :goto_0
    return-object v0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    const-string v1, "Error occured while obtaining the MRAID capabilities."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 107
    const/4 v0, 0x0

    goto :goto_0
.end method
