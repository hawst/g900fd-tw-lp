.class public final Lcom/google/android/gms/drive/api/a/v;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/internal/GetMetadataRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/GetMetadataRequest;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 1

    .prologue
    .line 29
    const/16 v0, 0xa

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 30
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/v;->e:Lcom/google/android/gms/drive/internal/GetMetadataRequest;

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 3

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/v;->e:Lcom/google/android/gms/drive/internal/GetMetadataRequest;

    const-string v1, "Invalid get metadata request: no request"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/v;->e:Lcom/google/android/gms/drive/internal/GetMetadataRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/GetMetadataRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    const-string v1, "Invalid get metadata request: no id"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/v;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/v;->e:Lcom/google/android/gms/drive/internal/GetMetadataRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/GetMetadataRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/api/d;->a(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    .line 41
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/v;->c:Lcom/google/android/gms/drive/c/a;

    new-instance v2, Lcom/google/android/gms/drive/internal/l;

    invoke-direct {v2, v0}, Lcom/google/android/gms/drive/internal/l;-><init>(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/aj;)Lcom/google/android/gms/drive/c/a;

    .line 43
    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/v;->b:Lcom/google/android/gms/drive/internal/ca;

    new-instance v2, Lcom/google/android/gms/drive/internal/OnMetadataResponse;

    invoke-direct {v2, v0}, Lcom/google/android/gms/drive/internal/OnMetadataResponse;-><init>(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/drive/internal/ca;->a(Lcom/google/android/gms/drive/internal/OnMetadataResponse;)V

    .line 44
    return-void
.end method
