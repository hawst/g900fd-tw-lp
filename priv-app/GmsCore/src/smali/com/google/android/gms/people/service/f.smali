.class public final Lcom/google/android/gms/people/service/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Landroid/os/Bundle;

.field static b:Landroid/os/Bundle;

.field private static c:Ljava/util/Locale;


# direct methods
.method static a(Landroid/content/Context;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v4, 0x1

    const/4 v6, 0x2

    const/4 v5, 0x0

    .line 56
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    .line 57
    sget-object v1, Lcom/google/android/gms/people/service/f;->c:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 69
    :goto_0
    return-void

    .line 60
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Email;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v4, v3}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    invoke-static {v2, v6, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Email;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v1, v6, v2}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    sput-object v1, Lcom/google/android/gms/people/service/f;->a:Landroid/os/Bundle;

    .line 61
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v4, v3}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    invoke-static {v2, v7, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v6, v3}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    invoke-static {v2, v6, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v7, v3}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/4 v3, 0x5

    invoke-static {v2, v3, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-static {v1, v8, v3}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/4 v3, 0x5

    invoke-static {v2, v8, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/4 v3, 0x6

    const/16 v4, 0xd

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/4 v3, 0x7

    const/4 v4, 0x6

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0x8

    const/16 v4, 0xa

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0x9

    const/16 v4, 0x13

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0xa

    const/16 v4, 0x9

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0xb

    const/16 v4, 0xe

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0xc

    const/16 v4, 0xb

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0xd

    const/16 v4, 0x8

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0xe

    const/16 v4, 0xf

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0xf

    const/16 v4, 0x10

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0x10

    const/16 v4, 0x11

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0x11

    const/16 v4, 0x12

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0x12

    const/16 v4, 0xc

    invoke-static {v2, v4, v5}, Landroid/provider/ContactsContract$CommonDataKinds$Phone;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    const/16 v3, 0x13

    sget v4, Lcom/google/android/gms/p;->rQ:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v3, v2}, Lcom/google/android/gms/people/service/f;->a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V

    sput-object v1, Lcom/google/android/gms/people/service/f;->b:Landroid/os/Bundle;

    .line 63
    invoke-static {v6}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 64
    const-string v1, "DataTypeLabelMap"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "email types="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gms/people/service/f;->a:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    const-string v1, "DataTypeLabelMap"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "phone types="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, Lcom/google/android/gms/people/service/f;->b:Landroid/os/Bundle;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_1
    sput-object v0, Lcom/google/android/gms/people/service/f;->c:Ljava/util/Locale;

    goto/16 :goto_0
.end method

.method private static a(Landroid/os/Bundle;ILjava/lang/CharSequence;)V
    .locals 2

    .prologue
    .line 72
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 73
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 75
    :cond_0
    return-void
.end method
