.class final Lcom/google/android/gms/games/ui/common/matches/f;
.super Lcom/google/android/gms/games/ui/card/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/c;-><init>(Landroid/view/View;)V

    .line 138
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/f;->b(Z)V

    .line 139
    return-void
.end method

.method private A()V
    .locals 4

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/f;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/d;

    .line 272
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->d(Lcom/google/android/gms/games/ui/common/matches/d;)Lcom/google/android/gms/games/ui/common/matches/e;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/f;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->c(Lcom/google/android/gms/games/ui/common/matches/d;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->a(Lcom/google/android/gms/games/ui/common/matches/d;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v3, v0}, Lcom/google/android/gms/games/ui/common/matches/e;->a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V
    .locals 15

    .prologue
    .line 133
    check-cast p3, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-super/range {p0 .. p3}, Lcom/google/android/gms/games/ui/card/c;->a(Lcom/google/android/gms/games/ui/ac;ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/f;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v6

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/multiplayer/Invitation;->l()Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/games/ui/d/al;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v7

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/multiplayer/Invitation;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v8

    invoke-interface {v8}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->a(Lcom/google/android/gms/games/ui/common/matches/d;)Ljava/lang/String;

    move-result-object v3

    const/4 v1, 0x0

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_2

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v5

    if-eqz v5, :cond_1

    invoke-interface {v5}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    :goto_1
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->b(Lcom/google/android/gms/games/ui/common/matches/d;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v8}, Lcom/google/android/gms/games/multiplayer/Participant;->j()Landroid/net/Uri;

    move-result-object v3

    sget v1, Lcom/google/android/gms/h;->ag:I

    move-object v4, v3

    move v3, v1

    :goto_2
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v11

    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/multiplayer/Invitation;->k()I

    move-result v12

    const/4 v1, 0x0

    move v5, v1

    :goto_3
    if-ge v5, v11, :cond_4

    invoke-virtual {v7, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Participant;->l()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v13, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-nez v14, :cond_0

    invoke-virtual {v13, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_0

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Participant;->h()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto :goto_3

    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_2
    const/4 v1, 0x0

    move-object v2, v1

    goto :goto_1

    :cond_3
    invoke-interface/range {p3 .. p3}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->k()Landroid/net/Uri;

    move-result-object v3

    sget v1, Lcom/google/android/gms/h;->af:I

    invoke-interface {v8}, Lcom/google/android/gms/games/multiplayer/Participant;->h()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v10, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v4, v3

    move v3, v1

    goto :goto_2

    :cond_4
    const/4 v1, 0x0

    :goto_4
    if-ge v1, v12, :cond_5

    const/4 v2, 0x0

    invoke-virtual {v10, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_5
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/f;->d(Z)V

    invoke-virtual {p0, v4, v3}, Lcom/google/android/gms/games/ui/common/matches/f;->a(Landroid/net/Uri;I)V

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->b(Lcom/google/android/gms/games/ui/common/matches/d;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_5
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/f;->c(Z)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/f;->l:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->mJ:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/f;->b(Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/matches/f;->e(Z)V

    sget v2, Lcom/google/android/gms/h;->ag:I

    invoke-virtual {p0, v10, v2}, Lcom/google/android/gms/games/ui/common/matches/f;->a(Ljava/util/ArrayList;I)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/f;->i(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/card/c;->k:Lcom/google/android/gms/games/ui/card/a;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/card/a;->f()Landroid/database/CharArrayBuffer;

    move-result-object v1

    invoke-interface {v8, v1}, Lcom/google/android/gms/games/multiplayer/Participant;->a(Landroid/database/CharArrayBuffer;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/f;->a(Landroid/database/CharArrayBuffer;)V

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->b(Lcom/google/android/gms/games/ui/common/matches/d;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/f;->l:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->jj:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/f;->f(Ljava/lang/String;)V

    invoke-interface {v6}, Lcom/google/android/gms/games/Game;->r()Z

    move-result v1

    if-eqz v1, :cond_8

    sget v2, Lcom/google/android/gms/p;->jb:I

    sget v1, Lcom/google/android/gms/p;->jc:I

    :goto_7
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/matches/f;->h(I)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/f;->i(I)V

    sget v1, Lcom/google/android/gms/p;->jf:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/f;->j(I)V

    sget v1, Lcom/google/android/gms/p;->jg:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/f;->k(I)V

    sget v1, Lcom/google/android/gms/m;->i:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/common/matches/f;->g(I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/f;->l:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->je:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-interface {v8}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/f;->a(Ljava/lang/String;)V

    return-void

    :cond_6
    const/4 v1, 0x0

    goto :goto_5

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/f;->l:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->ji:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-interface {v6}, Lcom/google/android/gms/games/Game;->s_()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :cond_8
    sget v2, Lcom/google/android/gms/p;->jk:I

    sget v1, Lcom/google/android/gms/p;->jl:I

    goto :goto_7
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/f;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/d;

    .line 321
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/f;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/Invitation;

    .line 322
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v2

    .line 323
    sget v4, Lcom/google/android/gms/j;->lR:I

    if-ne v2, v4, :cond_0

    .line 324
    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v1

    .line 325
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->d(Lcom/google/android/gms/games/ui/common/matches/d;)Lcom/google/android/gms/games/ui/common/matches/e;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/e;->b(Lcom/google/android/gms/games/Game;)V

    move v0, v3

    .line 336
    :goto_0
    return v0

    .line 327
    :cond_0
    sget v4, Lcom/google/android/gms/j;->lO:I

    if-ne v2, v4, :cond_1

    .line 328
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/f;->p()Lcom/google/android/gms/common/data/d;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/data/x;

    invoke-interface {v2, v1}, Lcom/google/android/gms/common/data/x;->b(Ljava/lang/Object;)V

    .line 329
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->d(Lcom/google/android/gms/games/ui/common/matches/d;)Lcom/google/android/gms/games/ui/common/matches/e;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/e;->d(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    move v0, v3

    .line 330
    goto :goto_0

    .line 331
    :cond_1
    sget v4, Lcom/google/android/gms/j;->lT:I

    if-ne v2, v4, :cond_2

    .line 332
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->d(Lcom/google/android/gms/games/ui/common/matches/d;)Lcom/google/android/gms/games/ui/common/matches/e;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->c(Lcom/google/android/gms/games/ui/common/matches/d;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->a(Lcom/google/android/gms/games/ui/common/matches/d;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v4, v0}, Lcom/google/android/gms/games/ui/common/matches/e;->a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    .line 334
    goto :goto_0

    .line 336
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final v()V
    .locals 4

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/f;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/d;

    .line 279
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->d(Lcom/google/android/gms/games/ui/common/matches/d;)Lcom/google/android/gms/games/ui/common/matches/e;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/f;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->c(Lcom/google/android/gms/games/ui/common/matches/d;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->a(Lcom/google/android/gms/games/ui/common/matches/d;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v1, v3, v0}, Lcom/google/android/gms/games/ui/common/matches/e;->a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    return-void
.end method

.method public final w()V
    .locals 0

    .prologue
    .line 285
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/f;->A()V

    .line 286
    return-void
.end method

.method public final x()V
    .locals 0

    .prologue
    .line 290
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/f;->A()V

    .line 291
    return-void
.end method

.method public final y()V
    .locals 4

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/f;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/d;

    .line 297
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/f;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/Invitation;

    .line 298
    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v2

    .line 300
    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->r()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 301
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->d(Lcom/google/android/gms/games/ui/common/matches/d;)Lcom/google/android/gms/games/ui/common/matches/e;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/e;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 305
    :goto_0
    return-void

    .line 303
    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->d(Lcom/google/android/gms/games/ui/common/matches/d;)Lcom/google/android/gms/games/ui/common/matches/e;

    move-result-object v0

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/ui/common/matches/e;->a(Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final z()V
    .locals 3

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/f;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/d;

    .line 312
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/f;->o()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/multiplayer/Invitation;

    .line 313
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/f;->p()Lcom/google/android/gms/common/data/d;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/data/x;

    invoke-interface {v2, v1}, Lcom/google/android/gms/common/data/x;->b(Ljava/lang/Object;)V

    .line 314
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->d(Lcom/google/android/gms/games/ui/common/matches/d;)Lcom/google/android/gms/games/ui/common/matches/e;

    move-result-object v0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/ui/common/matches/e;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 315
    return-void
.end method
