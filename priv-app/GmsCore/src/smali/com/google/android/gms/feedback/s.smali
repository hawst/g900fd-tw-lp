.class final Lcom/google/android/gms/feedback/s;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/feedback/FeedbackService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/feedback/FeedbackService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 138
    iput-object p1, p0, Lcom/google/android/gms/feedback/s;->a:Lcom/google/android/gms/feedback/FeedbackService;

    .line 139
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 140
    return-void
.end method


# virtual methods
.method public final o(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/gms/feedback/s;->a:Lcom/google/android/gms/feedback/FeedbackService;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/feedback/s;->a:Lcom/google/android/gms/feedback/FeedbackService;

    invoke-virtual {v0}, Lcom/google/android/gms/feedback/FeedbackService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 151
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lcom/google/android/gms/feedback/r;

    iget-object v2, p0, Lcom/google/android/gms/feedback/s;->a:Lcom/google/android/gms/feedback/FeedbackService;

    invoke-direct {v1, v2}, Lcom/google/android/gms/feedback/r;-><init>(Landroid/app/Service;)V

    invoke-virtual {v1}, Lcom/google/android/gms/feedback/r;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 156
    :goto_0
    return-void

    .line 154
    :catch_0
    move-exception v0

    const-string v0, "FeedbackServiceImpl"

    const-string v1, "client died while brokering service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
