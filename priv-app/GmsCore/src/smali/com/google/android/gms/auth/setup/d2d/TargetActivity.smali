.class public Lcom/google/android/gms/auth/setup/d2d/TargetActivity;
.super Lcom/google/android/gms/auth/setup/d2d/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/setup/d2d/ag;
.implements Lcom/google/android/gms/auth/setup/d2d/al;
.implements Lcom/google/android/gms/auth/setup/d2d/ap;
.implements Lcom/google/android/gms/auth/setup/d2d/aw;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final b:Lcom/google/android/gms/auth/d/a;

.field private static c:Lcom/google/android/gms/auth/setup/d2d/a;

.field private static final d:Landroid/util/SparseIntArray;


# instance fields
.field private e:Lcom/google/android/gms/auth/setup/d2d/aq;

.field private f:Landroid/app/Fragment;

.field private g:Landroid/app/Fragment;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 42
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "D2D"

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "TargetActivity"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b:Lcom/google/android/gms/auth/d/a;

    .line 62
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 63
    sput-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->d:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/gms/p;->aW:I

    invoke-virtual {v0, v5, v1}, Landroid/util/SparseIntArray;->append(II)V

    .line 64
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->d:Landroid/util/SparseIntArray;

    const/4 v1, 0x2

    sget v2, Lcom/google/android/gms/p;->aO:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 66
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->d:Landroid/util/SparseIntArray;

    const/4 v1, 0x3

    sget v2, Lcom/google/android/gms/p;->aW:I

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->append(II)V

    .line 67
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/e;-><init>()V

    .line 292
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;)Landroid/app/Fragment;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->g:Landroid/app/Fragment;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;Landroid/app/Fragment;)Landroid/app/Fragment;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->g:Landroid/app/Fragment;

    return-object p1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 70
    invoke-static {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 71
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 72
    const-string v1, "theme"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 73
    const-string v1, "useImmersiveMode"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 76
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/app/Fragment;)V
    .locals 2

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 274
    :goto_0
    return-void

    .line 246
    :cond_0
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->h()Lcom/google/android/gms/auth/setup/d2d/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/x;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/auth/setup/d2d/x;-><init>(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;Landroid/app/Fragment;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/setup/d2d/a;->a(Lcom/google/android/gms/auth/setup/d2d/c;)V

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 81
    const/16 v1, 0x15

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 82
    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b:Lcom/google/android/gms/auth/d/a;

    const-string v2, "D2D account setup only available on L."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    .line 99
    :goto_0
    return v0

    .line 85
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->c(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 86
    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Not primary user."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 89
    :cond_1
    invoke-static {p0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v1

    .line 90
    if-nez v1, :cond_2

    .line 91
    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b:Lcom/google/android/gms/auth/d/a;

    const-string v2, "NFC not available."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_2
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v1

    .line 95
    if-nez v1, :cond_3

    .line 96
    sget-object v1, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Bluetooth not available."

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 99
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;)Landroid/app/Fragment;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->f:Landroid/app/Fragment;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;Landroid/app/Fragment;)Landroid/app/Fragment;
    .locals 0

    .prologue
    .line 36
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->f:Landroid/app/Fragment;

    return-object p1
.end method

.method private b(I)V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/e;->a:Lcom/google/android/gms/auth/f/e;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/f/e;->e(I)Lcom/google/android/gms/auth/f/e;

    .line 278
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->d:Landroid/util/SparseIntArray;

    sget v1, Lcom/google/android/gms/p;->aW:I

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseIntArray;->get(II)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/ae;->a(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->a(Landroid/app/Fragment;)V

    .line 280
    return-void
.end method

.method public static declared-synchronized h()Lcom/google/android/gms/auth/setup/d2d/a;
    .locals 2

    .prologue
    .line 51
    const-class v1, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->c:Lcom/google/android/gms/auth/setup/d2d/a;

    if-nez v0, :cond_0

    .line 52
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/a;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->c:Lcom/google/android/gms/auth/setup/d2d/a;

    .line 54
    :cond_0
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->c:Lcom/google/android/gms/auth/setup/d2d/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic o()Lcom/google/android/gms/auth/d/a;
    .locals 1

    .prologue
    .line 36
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b:Lcom/google/android/gms/auth/d/a;

    return-object v0
.end method

.method private p()V
    .locals 1

    .prologue
    .line 238
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/am;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/am;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->a(Landroid/app/Fragment;)V

    .line 239
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 312
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->onBackPressed()V

    .line 313
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 222
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b(I)V

    .line 223
    return-void
.end method

.method public final a(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2

    .prologue
    .line 306
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/setup/d2d/e;->a(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V

    .line 307
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->b()Landroid/widget/Button;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->co:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 308
    return-void
.end method

.method public final a(Lcom/google/android/gms/auth/setup/d2d/a/b;Lcom/google/ab/b/a/e/h;)V
    .locals 1

    .prologue
    .line 176
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/ah;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/ah;-><init>()V

    .line 177
    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/setup/d2d/ah;->a(Lcom/google/android/gms/auth/setup/d2d/a/b;)V

    .line 178
    invoke-virtual {v0, p2}, Lcom/google/android/gms/auth/setup/d2d/ah;->a(Lcom/google/ab/b/a/e/h;)V

    .line 179
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->a(Landroid/app/Fragment;)V

    .line 180
    return-void
.end method

.method public final a(Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 193
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->h()Lcom/google/android/gms/auth/setup/d2d/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/w;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/gms/auth/setup/d2d/w;-><init>(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;Ljava/util/ArrayList;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/setup/d2d/a;->a(Lcom/google/android/gms/auth/setup/d2d/c;)V

    .line 213
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 317
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->d()V

    .line 318
    return-void
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 283
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->h()Lcom/google/android/gms/auth/setup/d2d/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/y;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/setup/d2d/y;-><init>(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/setup/d2d/a;->a(Lcom/google/android/gms/auth/setup/d2d/c;)V

    .line 289
    invoke-super {p0}, Lcom/google/android/gms/auth/setup/d2d/e;->d()V

    .line 290
    return-void
.end method

.method public final i()V
    .locals 0

    .prologue
    .line 164
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->p()V

    .line 165
    return-void
.end method

.method public final j()V
    .locals 0

    .prologue
    .line 169
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->d()V

    .line 170
    return-void
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 184
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b(I)V

    .line 185
    return-void
.end method

.method public final l()V
    .locals 0

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->p()V

    .line 218
    return-void
.end method

.method public final m()Lcom/google/android/gms/droidguard/b;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->e:Lcom/google/android/gms/auth/setup/d2d/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/setup/d2d/aq;->a()Lcom/google/android/gms/droidguard/b;

    move-result-object v0

    return-object v0
.end method

.method public final n()V
    .locals 0

    .prologue
    .line 232
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->d()V

    .line 233
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 108
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/setup/d2d/e;->onCreate(Landroid/os/Bundle;)V

    .line 110
    invoke-static {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 111
    sget-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->b:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Started D2D account setup but preconditions not met!"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    .line 112
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->d()V

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 116
    :cond_1
    if-nez p1, :cond_2

    .line 117
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->h()Lcom/google/android/gms/auth/setup/d2d/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/setup/d2d/a;->a()V

    .line 119
    :cond_2
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->h()Lcom/google/android/gms/auth/setup/d2d/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/auth/setup/d2d/a;->a(Landroid/app/Activity;)V

    .line 121
    new-instance v0, Lcom/google/android/setupwizard/util/b;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/b;-><init>(Landroid/content/Context;)V

    .line 122
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->setContentView(Landroid/view/View;)V

    .line 123
    sget v1, Lcom/google/android/gms/p;->aY:I

    sget v2, Lcom/google/android/gms/l;->p:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/setupwizard/util/b;->a(II)Landroid/view/View;

    .line 125
    sget v0, Lcom/google/android/gms/j;->mh:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/v;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/setup/d2d/v;-><init>(Lcom/google/android/gms/auth/setup/d2d/TargetActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 136
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "main"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->f:Landroid/app/Fragment;

    .line 139
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "dialog"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->g:Landroid/app/Fragment;

    .line 142
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "resources"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/setup/d2d/aq;

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->e:Lcom/google/android/gms/auth/setup/d2d/aq;

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->e:Lcom/google/android/gms/auth/setup/d2d/aq;

    if-nez v0, :cond_0

    .line 145
    new-instance v0, Lcom/google/android/gms/auth/setup/d2d/aq;

    invoke-direct {v0}, Lcom/google/android/gms/auth/setup/d2d/aq;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->e:Lcom/google/android/gms/auth/setup/d2d/aq;

    .line 146
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->e:Lcom/google/android/gms/auth/setup/d2d/aq;

    const-string v2, "resources"

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 153
    invoke-virtual {p0}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->h()Lcom/google/android/gms/auth/setup/d2d/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/setup/d2d/a;->a()V

    .line 155
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->c:Lcom/google/android/gms/auth/setup/d2d/a;

    .line 157
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/auth/setup/d2d/e;->onDestroy()V

    .line 158
    return-void
.end method
