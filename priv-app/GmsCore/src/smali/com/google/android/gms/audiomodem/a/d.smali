.class public final Lcom/google/android/gms/audiomodem/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/audiomodem/a/t;

.field public b:Lcom/google/android/gms/audiomodem/a/t;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/location/internal/z;

.field private e:Lcom/google/android/gms/audiomodem/a/h;

.field private f:Landroid/os/IBinder;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/location/internal/z;)V
    .locals 2

    .prologue
    .line 42
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/a/d;->c:Landroid/content/Context;

    .line 44
    iput-object p2, p0, Lcom/google/android/gms/audiomodem/a/d;->d:Lcom/google/android/gms/location/internal/z;

    .line 45
    new-instance v0, Lcom/google/android/gms/audiomodem/a/t;

    sget-object v1, Lcom/google/android/gms/audiomodem/a/y;->a:Lcom/google/android/gms/audiomodem/a/u;

    invoke-direct {v0, v1}, Lcom/google/android/gms/audiomodem/a/t;-><init>(Lcom/google/android/gms/audiomodem/a/u;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/a/d;->a:Lcom/google/android/gms/audiomodem/a/t;

    .line 46
    new-instance v0, Lcom/google/android/gms/audiomodem/a/t;

    sget-object v1, Lcom/google/android/gms/audiomodem/a/w;->a:Lcom/google/android/gms/audiomodem/a/u;

    invoke-direct {v0, v1}, Lcom/google/android/gms/audiomodem/a/t;-><init>(Lcom/google/android/gms/audiomodem/a/u;)V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/a/d;->b:Lcom/google/android/gms/audiomodem/a/t;

    .line 47
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/location/internal/z;)Lcom/google/android/gms/audiomodem/a/d;
    .locals 1

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/audiomodem/a/d;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/audiomodem/a/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/location/internal/z;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/api/m;)V
    .locals 2

    .prologue
    .line 104
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 105
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/audiomodem/a/h;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/a/d;->d:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v0}, Lcom/google/android/gms/location/internal/z;->a()V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/a/d;->e:Lcom/google/android/gms/audiomodem/a/h;

    if-nez v0, :cond_0

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/a/d;->d:Lcom/google/android/gms/location/internal/z;

    invoke-interface {v0}, Lcom/google/android/gms/location/internal/z;->b()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/internal/q;

    invoke-interface {v0}, Lcom/google/android/gms/location/internal/q;->c()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/a/i;->a(Landroid/os/IBinder;)Lcom/google/android/gms/audiomodem/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/a/d;->e:Lcom/google/android/gms/audiomodem/a/h;

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/a/d;->e:Lcom/google/android/gms/audiomodem/a/h;

    return-object v0
.end method

.method public final b()Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/a/d;->f:Landroid/os/IBinder;

    if-nez v0, :cond_0

    .line 109
    new-instance v0, Landroid/os/Binder;

    invoke-direct {v0}, Landroid/os/Binder;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/audiomodem/a/d;->f:Landroid/os/IBinder;

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/a/d;->f:Landroid/os/IBinder;

    return-object v0
.end method
