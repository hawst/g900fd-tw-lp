.class public final Lcom/google/android/gms/people/service/a/b/d;
.super Lcom/google/android/gms/people/service/a/b/c;
.source "SourceFile"


# instance fields
.field private final i:Ljava/lang/String;

.field private final j:Z

.field private final k:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 9

    .prologue
    .line 37
    move-object v1, p0

    move-object v2, p1

    move-object v3, p6

    move-object v4, p2

    move v5, p4

    move-object v6, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/people/service/a/b/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    move-object/from16 v0, p9

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/b/d;->i:Ljava/lang/String;

    .line 40
    move/from16 v0, p10

    iput-boolean v0, p0, Lcom/google/android/gms/people/service/a/b/d;->j:Z

    .line 41
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    iput-object p2, p0, Lcom/google/android/gms/people/service/a/b/d;->k:Ljava/lang/String;

    .line 42
    return-void

    :cond_0
    move-object p2, p3

    .line 41
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;
    .locals 7

    .prologue
    .line 48
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/u;->b()V

    .line 50
    iget-boolean v0, p0, Lcom/google/android/gms/people/service/a/b/d;->j:Z

    if-eqz v0, :cond_1

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/d;->i:Ljava/lang/String;

    invoke-virtual {p2, p3, v0}, Lcom/google/android/gms/people/service/e;->a(Lcom/google/android/gms/people/e/a;Ljava/lang/String;)V

    .line 60
    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/d;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/d;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/b/d;->i:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/people/internal/at;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/people/c/f;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    .line 66
    :goto_0
    invoke-static {p1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/u;->b()V

    .line 68
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/b/d;->k:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/d;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/d;->i:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/gms/people/service/a/b/d;->j:Z

    iget-object v4, p0, Lcom/google/android/gms/people/service/a/b/d;->h:Ljava/lang/String;

    new-instance v5, Ljava/util/ArrayList;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;-><init>()V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bw;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    new-instance v5, Lcom/google/android/gms/common/server/y;

    invoke-direct {v5, p1}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v5, v1}, Lcom/google/android/gms/common/server/y;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v5

    if-eqz v3, :cond_2

    sget-object v1, Lcom/google/android/gms/plus/a/m;->g:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_1
    invoke-virtual {v5, v1}, Lcom/google/android/gms/common/server/y;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/plus/a/b;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/google/android/gms/common/server/y;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/server/y;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/y;)V

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/d;->h:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/d;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/ah;->h()Lcom/google/android/gms/people/sync/u;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/b/d;->g:Ljava/lang/String;

    const-string v2, "BlockPerson"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/sync/u;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_0
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    .line 63
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/d;->i:Ljava/lang/String;

    invoke-virtual {p2, p3, v0}, Lcom/google/android/gms/people/service/e;->b(Lcom/google/android/gms/people/e/a;Ljava/lang/String;)V

    goto :goto_0

    .line 68
    :cond_2
    sget-object v1, Lcom/google/android/gms/plus/a/m;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_1
.end method
