.class public Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;
.super Lcom/google/android/gms/wallet/common/ui/ds;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/bp;
.implements Lcom/google/android/gms/wallet/common/ui/cd;


# static fields
.field public static final a:[I

.field private static final i:Ljava/lang/String;


# instance fields
.field b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field c:Landroid/widget/ProgressBar;

.field d:Landroid/view/View;

.field e:Landroid/widget/TextView;

.field f:Lcom/google/android/gms/wallet/common/ui/cc;

.field g:Lcom/google/android/gms/wallet/common/ui/bb;

.field h:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Lcom/google/checkout/inapp/proto/ai;

.field private m:Lcom/google/checkout/inapp/proto/j;

.field private n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private o:Landroid/accounts/Account;

.field private p:Z

.field private q:Z

.field private r:[I

.field private s:[Ljava/lang/String;

.field private t:Lcom/google/android/apps/common/a/a/i;

.field private u:Lcom/google/android/apps/common/a/a/h;

.field private v:Lcom/google/android/gms/wallet/common/ui/dh;

.field private w:Z

.field private x:Z

.field private y:Ljava/util/ArrayList;

.field private final z:Lcom/google/android/gms/wallet/service/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    new-array v0, v0, [I

    sput-object v0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->a:[I

    .line 82
    const-string v0, "PickInstrumentActivity"

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->i:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ds;-><init>()V

    .line 423
    new-instance v0, Lcom/google/android/gms/wallet/ia/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ia/o;-><init>(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->z:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;Lcom/google/checkout/inapp/proto/ai;)Lcom/google/checkout/inapp/proto/ai;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->l:Lcom/google/checkout/inapp/proto/ai;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    .prologue
    .line 61
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->y:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic a(Lcom/google/checkout/inapp/proto/ai;)Ljava/util/ArrayList;
    .locals 6

    .prologue
    .line 61
    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v2, v2

    add-int/2addr v0, v2

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    iget-object v5, v4, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v5, :cond_0

    iget-object v4, v4, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    return-object v1
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)V
    .locals 2

    .prologue
    .line 61
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;[Lcom/google/checkout/inapp/proto/j;)V
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/j;

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->c(Lcom/google/checkout/inapp/proto/j;)I

    move-result v1

    const/16 v2, 0x7f

    if-eq v1, v2, :cond_0

    const/4 v0, 0x0

    :cond_0
    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/j;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, p1}, Lcom/google/android/gms/wallet/common/ui/cc;->a([Lcom/google/checkout/inapp/proto/j;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->b(Z)V

    return-void
.end method

.method private b(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 488
    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->c:Landroid/widget/ProgressBar;

    if-eqz p1, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 489
    if-nez p1, :cond_0

    const/4 v1, 0x1

    .line 490
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->setEnabled(Z)V

    .line 491
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->setEnabled(Z)V

    .line 492
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->g()V

    .line 493
    return-void

    .line 488
    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private c(Lcom/google/checkout/inapp/proto/j;)I
    .locals 5

    .prologue
    const/16 v0, 0x79

    .line 539
    if-nez p1, :cond_1

    .line 540
    const/16 v0, 0x77

    .line 575
    :cond_0
    :goto_0
    return v0

    .line 542
    :cond_1
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v1, v1

    if-lez v1, :cond_3

    .line 544
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v4, v1

    .line 545
    const/4 v1, 0x0

    move v2, v1

    move v1, v0

    :goto_1
    if-ge v2, v4, :cond_2

    .line 546
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/j;->g:[I

    aget v1, v1, v2

    const/4 v3, 0x2

    if-ne v1, v3, :cond_0

    .line 547
    const/16 v3, 0x7d

    .line 545
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v3

    goto :goto_1

    :cond_2
    move v0, v1

    .line 552
    goto :goto_0

    .line 554
    :cond_3
    iget v1, p1, Lcom/google/checkout/inapp/proto/j;->h:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 556
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->s:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->s:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_4

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->s:[Ljava/lang/String;

    iget-object v1, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v1, v1, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 561
    const/16 v0, 0x7a

    goto :goto_0

    .line 562
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->p:Z

    if-eqz v0, :cond_5

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 564
    const/16 v0, 0x7c

    goto :goto_0

    .line 565
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->q:Z

    if-eqz v0, :cond_6

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 567
    const/16 v0, 0x7e

    goto :goto_0

    .line 569
    :cond_6
    const/16 v0, 0x7f

    goto :goto_0

    .line 572
    :pswitch_1
    const/16 v0, 0x78

    goto :goto_0

    .line 554
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->t:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->u:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)Lcom/google/android/apps/common/a/a/i;
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->t:Lcom/google/android/apps/common/a/a/i;

    return-object v0
.end method

.method private e()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 236
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->x:Z

    .line 237
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->b(Z)V

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->v:Lcom/google/android/gms/wallet/common/ui/dh;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->z:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/android/gms/wallet/service/l;)V

    .line 240
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "inapp.PickInstrumentActivity.GetProfile"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 247
    :goto_0
    return-void

    .line 245
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->f()V

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)Lcom/google/android/apps/common/a/a/h;
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->u:Lcom/google/android/apps/common/a/a/h;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->l:Lcom/google/checkout/inapp/proto/ai;

    if-eqz v0, :cond_0

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->z:Lcom/google/android/gms/wallet/service/l;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->l:Lcom/google/checkout/inapp/proto/ai;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/service/l;->a(Lcom/google/checkout/inapp/proto/ai;)V

    .line 258
    :goto_0
    return-void

    .line 253
    :cond_0
    new-instance v0, Lcom/google/checkout/inapp/proto/ah;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/ah;-><init>()V

    .line 254
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->r:[I

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/ah;->a:[I

    .line 255
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->v:Lcom/google/android/gms/wallet/common/ui/dh;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/service/k;->a(Lcom/google/checkout/inapp/proto/ah;)V

    .line 256
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->b(Z)V

    goto :goto_0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 496
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->c:Landroid/widget/ProgressBar;

    invoke-virtual {v0}, Landroid/widget/ProgressBar;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    .line 498
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->h:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 499
    return-void

    .line 496
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic g(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)V
    .locals 3

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "inapp.PickInstrumentActivity.GetProfile"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic h(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)V
    .locals 2

    .prologue
    .line 61
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic i(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)V
    .locals 2

    .prologue
    .line 61
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic j(Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;)V
    .locals 2

    .prologue
    .line 61
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->a(ILandroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const/4 v8, 0x0

    const/4 v2, 0x0

    .line 377
    .line 378
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->s:[Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->s:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_0

    .line 379
    new-instance v3, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->s:[Ljava/lang/String;

    array-length v0, v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 380
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->s:[Ljava/lang/String;

    array-length v4, v1

    move v0, v8

    :goto_0
    if-ge v0, v4, :cond_1

    aget-object v5, v1, v0

    .line 381
    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 380
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    move-object v3, v2

    .line 384
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->o:Landroid/accounts/Account;

    iget-boolean v4, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->p:Z

    iget-boolean v5, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->q:Z

    iget-object v11, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->y:Ljava/util/ArrayList;

    move-object v6, v2

    move-object v7, v2

    move-object v9, v2

    move-object v10, v2

    invoke-static/range {v0 .. v11}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Ljava/lang/String;Ljava/util/ArrayList;ZZ[I[IILjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;)Landroid/content/Intent;

    move-result-object v0

    .line 397
    const/16 v1, 0x1f5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 398
    return-void
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 503
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    .line 504
    packed-switch p1, :pswitch_data_0

    .line 512
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected button "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 506
    :pswitch_0
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->a(ILandroid/content/Intent;)V

    .line 510
    :goto_0
    return-void

    .line 509
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->f()V

    goto :goto_0

    .line 515
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected errorCode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 504
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 346
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->c(Lcom/google/checkout/inapp/proto/j;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    .line 347
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->o:Landroid/accounts/Account;

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->p:Z

    iget-boolean v4, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->q:Z

    iget-object v7, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->y:Ljava/util/ArrayList;

    move-object v2, p1

    move-object v6, v5

    move-object v8, v5

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/wallet/shared/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/j;ZZLjava/lang/String;Lcom/google/checkout/inapp/proto/q;Ljava/util/Collection;Lcom/google/checkout/inapp/proto/a/b;)Landroid/content/Intent;

    move-result-object v0

    .line 357
    const/16 v1, 0x1f6

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 362
    :goto_1
    return-void

    .line 346
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 359
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/j;

    .line 360
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->g()V

    goto :goto_1

    .line 346
    :pswitch_data_0
    .packed-switch 0x7c
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 408
    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->j:Landroid/view/View;

    if-eqz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/view/View;->setVisibility(I)V

    .line 409
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->k:Landroid/view/View;

    if-eqz p1, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 410
    return-void

    :cond_0
    move v0, v2

    .line 408
    goto :goto_0

    :cond_1
    move v1, v2

    .line 409
    goto :goto_1
.end method

.method public final b(Lcom/google/checkout/inapp/proto/j;)V
    .locals 2

    .prologue
    .line 402
    const-string v0, "PickInstrumentActivity"

    const-string v1, "Unexpected instrument info requested"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 403
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 284
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/ds;->onActivityResult(IILandroid/content/Intent;)V

    .line 285
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->w:Z

    .line 286
    packed-switch p1, :pswitch_data_0

    .line 326
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "pick_instrument"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->x:Z

    if-eqz v0, :cond_0

    .line 330
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->e()V

    .line 333
    :cond_0
    return-void

    .line 288
    :pswitch_0
    packed-switch p2, :pswitch_data_1

    .line 301
    const-string v0, "PickInstrumentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed adding an instrument, resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 290
    :pswitch_1
    const-string v0, "PickInstrumentActivity"

    const-string v1, "Successfully added an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 291
    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 294
    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->l:Lcom/google/checkout/inapp/proto/ai;

    .line 295
    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/j;

    goto :goto_0

    .line 298
    :pswitch_2
    const-string v0, "PickInstrumentActivity"

    const-string v1, "User canceled adding an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 308
    :pswitch_3
    packed-switch p2, :pswitch_data_2

    .line 321
    const-string v0, "PickInstrumentActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed updating an instrument, resultCode="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 310
    :pswitch_4
    const-string v0, "PickInstrumentActivity"

    const-string v1, "Successfully updated an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    const-string v0, "com.google.android.gms.wallet.instrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p3, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/content/Intent;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 314
    iput-object v2, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->l:Lcom/google/checkout/inapp/proto/ai;

    .line 315
    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/j;

    goto/16 :goto_0

    .line 318
    :pswitch_5
    const-string v0, "PickInstrumentActivity"

    const-string v1, "User canceled updating an instrument"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 286
    nop

    :pswitch_data_0
    .packed-switch 0x1f5
        :pswitch_0
        :pswitch_3
    .end packed-switch

    .line 288
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 308
    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 338
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 339
    const-string v1, "com.google.android.gms.wallet.instrumentId"

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/j;

    iget-object v2, v2, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 340
    const-string v1, "com.google.android.gms.wallet.instrument"

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 341
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->a(ILandroid/content/Intent;)V

    .line 342
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 119
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ds;->onCreate(Landroid/os/Bundle;)V

    .line 120
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dg;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/dg;

    .line 121
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ck;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/wallet/common/ui/ck;

    .line 123
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 124
    const-string v0, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    if-eqz v0, :cond_6

    move v0, v2

    :goto_0
    const-string v1, "Activity requires buyFlowConfig!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 126
    const-string v0, "com.google.android.gms.wallet.account"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->o:Landroid/accounts/Account;

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->o:Landroid/accounts/Account;

    if-eqz v0, :cond_7

    move v0, v2

    :goto_1
    const-string v1, "Activity requires buyerAccount!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 128
    const-string v0, "com.google.android.gms.wallet.requiresCreditCardFullAddress"

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->p:Z

    .line 131
    const-string v0, "com.google.android.gms.wallet.phoneNumberRequired"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->q:Z

    .line 134
    const-string v0, "com.google.android.gms.wallet.allowedInstrumentFamilies"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->r:[I

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->r:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->r:[I

    array-length v0, v0

    if-nez v0, :cond_1

    .line 137
    :cond_0
    sget-object v0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->r:[I

    .line 139
    :cond_1
    const-string v0, "com.google.android.gms.wallet.allowedCountryCodes"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->s:[Ljava/lang/String;

    .line 141
    const-string v0, "com.google.android.gms.wallet.infoText"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 143
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v5, Lcom/google/android/gms/wallet/common/ui/dt;->c:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v5}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 146
    sget v0, Lcom/google/android/gms/l;->gj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->setContentView(I)V

    .line 147
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/Window;)V

    .line 148
    sget v0, Lcom/google/android/gms/j;->cc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->j:Landroid/view/View;

    .line 149
    sget v0, Lcom/google/android/gms/j;->te:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->k:Landroid/view/View;

    .line 156
    :goto_2
    sget v0, Lcom/google/android/gms/j;->tc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iget-object v5, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->o:Landroid/accounts/Account;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->b:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a()V

    .line 160
    sget v0, Lcom/google/android/gms/j;->pN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->c:Landroid/widget/ProgressBar;

    .line 161
    sget v0, Lcom/google/android/gms/j;->nX:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->d:Landroid/view/View;

    .line 163
    sget v0, Lcom/google/android/gms/j;->kq:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->e:Landroid/widget/TextView;

    .line 164
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 171
    :goto_3
    sget v0, Lcom/google/android/gms/j;->kz:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cc;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cc;

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->p:Z

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Z)V

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->q:Z

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->b_(Z)V

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, p0}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/android/gms/wallet/common/ui/cd;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cc;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->s:[Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a([Ljava/lang/String;)V

    .line 177
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 178
    sget v0, Lcom/google/android/gms/j;->fQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;

    .line 179
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cc;

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/bj;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bj;)V

    .line 180
    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ExpanderContainer;->a(Lcom/google/android/gms/wallet/common/ui/bp;)V

    .line 183
    :cond_2
    sget v0, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->h:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->h:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    .line 186
    if-eqz p1, :cond_a

    .line 187
    const-string v0, "profileResponse"

    const-class v1, Lcom/google/checkout/inapp/proto/ai;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/ai;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->l:Lcom/google/checkout/inapp/proto/ai;

    .line 189
    const-string v0, "selectedInstrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/j;

    .line 191
    const-string v0, "waitingForActivityResult"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->w:Z

    .line 206
    :cond_3
    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->v:Lcom/google/android/gms/wallet/common/ui/dh;

    if-nez v0, :cond_4

    .line 207
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dh;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->v:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 210
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->v:Lcom/google/android/gms/wallet/common/ui/dh;

    if-nez v0, :cond_5

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->o:Landroid/accounts/Account;

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dh;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->v:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 213
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->v:Lcom/google/android/gms/wallet/common/ui/dh;

    sget-object v2, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 217
    :cond_5
    return-void

    :cond_6
    move v0, v3

    .line 125
    goto/16 :goto_0

    :cond_7
    move v0, v3

    .line 127
    goto/16 :goto_1

    .line 151
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v5, Lcom/google/android/gms/wallet/common/ui/dt;->b:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v5}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 153
    sget v0, Lcom/google/android/gms/l;->gi:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->setContentView(I)V

    goto/16 :goto_2

    .line 168
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->e:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 194
    :cond_a
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v1, "pick_instrument"

    invoke-direct {v0, v1}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->t:Lcom/google/android/apps/common/a/a/i;

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->t:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->u:Lcom/google/android/apps/common/a/a/h;

    .line 196
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->n:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v3, "pick_instrument"

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 198
    const-string v0, "com.google.android.gms.wallet.instrumentId"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 200
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 201
    new-instance v1, Lcom/google/checkout/inapp/proto/j;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/j;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/j;

    .line 202
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/j;

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    goto/16 :goto_4
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 262
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/ds;->onPause()V

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->v:Lcom/google/android/gms/wallet/common/ui/dh;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->z:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;)V

    .line 265
    return-void
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 227
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/ds;->onResume()V

    .line 228
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->w:Z

    if-eqz v0, :cond_0

    .line 229
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->x:Z

    .line 233
    :goto_0
    return-void

    .line 231
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->e()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->v:Lcom/google/android/gms/wallet/common/ui/dh;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a()Lcom/google/android/gms/wallet/service/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->z:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;)V

    .line 271
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ds;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 273
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->l:Lcom/google/checkout/inapp/proto/ai;

    if-eqz v0, :cond_0

    .line 274
    const-string v0, "profileResponse"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->l:Lcom/google/checkout/inapp/proto/ai;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 276
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_1

    .line 277
    const-string v0, "selectedInstrument"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->m:Lcom/google/checkout/inapp/proto/j;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 279
    :cond_1
    const-string v0, "waitingForActivityResult"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->w:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 280
    return-void
.end method

.method public startActivityForResult(Landroid/content/Intent;I)V
    .locals 2

    .prologue
    .line 469
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->w:Z

    .line 470
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/common/ui/ds;->startActivityForResult(Landroid/content/Intent;I)V

    .line 471
    sget v0, Lcom/google/android/gms/b;->E:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/wallet/ia/PickInstrumentActivity;->overridePendingTransition(II)V

    .line 472
    return-void
.end method
