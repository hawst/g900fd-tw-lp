.class public final Lcom/google/android/gms/drive/database/model/at;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Lcom/google/android/gms/drive/database/model/aw;

.field public final c:Lcom/google/android/gms/drive/database/model/ae;

.field public final d:Lcom/google/android/gms/drive/database/model/ab;

.field public final e:Z

.field public final f:Ljava/util/Set;

.field public final g:Ljava/lang/Object;

.field public final h:Z

.field public final i:Lcom/google/android/gms/drive/database/model/av;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;ZLjava/util/Set;ZLjava/lang/Object;Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;Lcom/google/android/gms/drive/database/model/av;)V
    .locals 1

    .prologue
    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 238
    invoke-static {p9}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 240
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/at;->a:Ljava/lang/String;

    .line 241
    iput-object p2, p0, Lcom/google/android/gms/drive/database/model/at;->b:Lcom/google/android/gms/drive/database/model/aw;

    .line 242
    invoke-static {p4}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/at;->f:Ljava/util/Set;

    .line 243
    iput-boolean p5, p0, Lcom/google/android/gms/drive/database/model/at;->h:Z

    .line 244
    iput-object p6, p0, Lcom/google/android/gms/drive/database/model/at;->g:Ljava/lang/Object;

    .line 245
    iput-object p7, p0, Lcom/google/android/gms/drive/database/model/at;->c:Lcom/google/android/gms/drive/database/model/ae;

    .line 246
    iput-object p8, p0, Lcom/google/android/gms/drive/database/model/at;->d:Lcom/google/android/gms/drive/database/model/ab;

    .line 247
    iput-object p9, p0, Lcom/google/android/gms/drive/database/model/at;->i:Lcom/google/android/gms/drive/database/model/av;

    .line 248
    iput-boolean p3, p0, Lcom/google/android/gms/drive/database/model/at;->e:Z

    .line 249
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;ZLjava/util/Set;ZLjava/lang/Object;Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;Lcom/google/android/gms/drive/database/model/av;B)V
    .locals 0

    .prologue
    .line 23
    invoke-direct/range {p0 .. p9}, Lcom/google/android/gms/drive/database/model/at;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;ZLjava/util/Set;ZLjava/lang/Object;Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;Lcom/google/android/gms/drive/database/model/av;)V

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 253
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "FieldDefinition[%s, %s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/at;->a:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/at;->b:Lcom/google/android/gms/drive/database/model/aw;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
