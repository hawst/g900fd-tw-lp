.class public abstract Lcom/google/android/gms/drive/api/a/d;
.super Lcom/google/android/gms/drive/api/a/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/api/a/c;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;B)V
    .locals 1

    .prologue
    .line 28
    const/16 v0, 0x9

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/drive/api/a/c;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 29
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;)V
    .locals 4

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/d;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/api/d;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 35
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0xa

    const-string v2, "The calling package is not authorized to make this request"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 38
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/api/a/d;->a()V

    .line 39
    return-void
.end method
