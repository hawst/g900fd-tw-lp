.class public final Lcom/google/android/gms/games/g/o;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 830
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 831
    iput v1, p0, Lcom/google/android/gms/games/g/o;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/o;->b:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/games/g/o;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/o;->cachedSize:I

    .line 832
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 894
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 895
    iget v1, p0, Lcom/google/android/gms/games/g/o;->a:I

    if-eqz v1, :cond_0

    .line 896
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/g/o;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 899
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/o;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 900
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/g/o;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 903
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/g/o;->c:I

    if-eqz v1, :cond_2

    .line 904
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/g/o;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 907
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 844
    if-ne p1, p0, :cond_1

    .line 864
    :cond_0
    :goto_0
    return v0

    .line 847
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/o;

    if-nez v2, :cond_2

    move v0, v1

    .line 848
    goto :goto_0

    .line 850
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/o;

    .line 851
    iget v2, p0, Lcom/google/android/gms/games/g/o;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/o;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 852
    goto :goto_0

    .line 854
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/o;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 855
    iget-object v2, p1, Lcom/google/android/gms/games/g/o;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 856
    goto :goto_0

    .line 858
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/o;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/o;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 859
    goto :goto_0

    .line 861
    :cond_5
    iget v2, p0, Lcom/google/android/gms/games/g/o;->c:I

    iget v3, p1, Lcom/google/android/gms/games/g/o;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 862
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 869
    iget v0, p0, Lcom/google/android/gms/games/g/o;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 871
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/o;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 873
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/o;->c:I

    add-int/2addr v0, v1

    .line 874
    return v0

    .line 871
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/o;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 797
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/o;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/o;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/o;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 880
    iget v0, p0, Lcom/google/android/gms/games/g/o;->a:I

    if-eqz v0, :cond_0

    .line 881
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/g/o;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 883
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/o;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 884
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/g/o;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 886
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/g/o;->c:I

    if-eqz v0, :cond_2

    .line 887
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/g/o;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 889
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 890
    return-void
.end method
