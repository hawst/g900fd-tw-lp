.class public final Lcom/google/android/gms/games/g/r;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/games/g/af;

.field public b:Lcom/google/android/gms/games/g/ao;

.field public c:Lcom/google/android/gms/games/g/v;

.field public d:Lcom/google/android/gms/games/g/j;

.field public e:Lcom/google/android/gms/games/g/ae;

.field public f:Lcom/google/android/gms/games/g/t;

.field public g:Lcom/google/android/gms/games/g/w;

.field public h:Lcom/google/android/gms/games/g/ac;

.field public i:Lcom/google/android/gms/games/g/e;

.field public j:Lcom/google/android/gms/games/g/ag;

.field public k:Lcom/google/android/gms/games/g/ap;

.field public l:Lcom/google/android/gms/games/g/as;

.field public m:Lcom/google/android/gms/games/g/ad;

.field public n:Lcom/google/android/gms/games/g/am;

.field public o:Lcom/google/android/gms/games/g/ab;

.field public p:Lcom/google/android/gms/games/g/aq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9084
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 9085
    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->d:Lcom/google/android/gms/games/g/j;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->e:Lcom/google/android/gms/games/g/ae;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->g:Lcom/google/android/gms/games/g/w;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->o:Lcom/google/android/gms/games/g/ab;

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/r;->cachedSize:I

    .line 9086
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 9359
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 9360
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    if-eqz v1, :cond_0

    .line 9361
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9364
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    if-eqz v1, :cond_1

    .line 9365
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9368
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    if-eqz v1, :cond_2

    .line 9369
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9372
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->d:Lcom/google/android/gms/games/g/j;

    if-eqz v1, :cond_3

    .line 9373
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->d:Lcom/google/android/gms/games/g/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9376
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->e:Lcom/google/android/gms/games/g/ae;

    if-eqz v1, :cond_4

    .line 9377
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->e:Lcom/google/android/gms/games/g/ae;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9380
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    if-eqz v1, :cond_5

    .line 9381
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9384
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->g:Lcom/google/android/gms/games/g/w;

    if-eqz v1, :cond_6

    .line 9385
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->g:Lcom/google/android/gms/games/g/w;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9388
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    if-eqz v1, :cond_7

    .line 9389
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9392
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    if-eqz v1, :cond_8

    .line 9393
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9396
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    if-eqz v1, :cond_9

    .line 9397
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9400
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    if-eqz v1, :cond_a

    .line 9401
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9404
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    if-eqz v1, :cond_b

    .line 9405
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9408
    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    if-eqz v1, :cond_c

    .line 9409
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9412
    :cond_c
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    if-eqz v1, :cond_d

    .line 9413
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9416
    :cond_d
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->o:Lcom/google/android/gms/games/g/ab;

    if-eqz v1, :cond_e

    .line 9417
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->o:Lcom/google/android/gms/games/g/ab;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9420
    :cond_e
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    if-eqz v1, :cond_f

    .line 9421
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9424
    :cond_f
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 9111
    if-ne p1, p0, :cond_1

    .line 9262
    :cond_0
    :goto_0
    return v0

    .line 9114
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/r;

    if-nez v2, :cond_2

    move v0, v1

    .line 9115
    goto :goto_0

    .line 9117
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/r;

    .line 9118
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    if-nez v2, :cond_3

    .line 9119
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    if-eqz v2, :cond_4

    move v0, v1

    .line 9120
    goto :goto_0

    .line 9123
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/af;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 9124
    goto :goto_0

    .line 9127
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    if-nez v2, :cond_5

    .line 9128
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    if-eqz v2, :cond_6

    move v0, v1

    .line 9129
    goto :goto_0

    .line 9132
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/ao;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 9133
    goto :goto_0

    .line 9136
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    if-nez v2, :cond_7

    .line 9137
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    if-eqz v2, :cond_8

    move v0, v1

    .line 9138
    goto :goto_0

    .line 9141
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/v;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 9142
    goto :goto_0

    .line 9145
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->d:Lcom/google/android/gms/games/g/j;

    if-nez v2, :cond_9

    .line 9146
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->d:Lcom/google/android/gms/games/g/j;

    if-eqz v2, :cond_a

    move v0, v1

    .line 9147
    goto :goto_0

    .line 9150
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->d:Lcom/google/android/gms/games/g/j;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->d:Lcom/google/android/gms/games/g/j;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/j;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 9151
    goto :goto_0

    .line 9154
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->e:Lcom/google/android/gms/games/g/ae;

    if-nez v2, :cond_b

    .line 9155
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->e:Lcom/google/android/gms/games/g/ae;

    if-eqz v2, :cond_c

    move v0, v1

    .line 9156
    goto :goto_0

    .line 9159
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->e:Lcom/google/android/gms/games/g/ae;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->e:Lcom/google/android/gms/games/g/ae;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/ae;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 9160
    goto :goto_0

    .line 9163
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    if-nez v2, :cond_d

    .line 9164
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    if-eqz v2, :cond_e

    move v0, v1

    .line 9165
    goto :goto_0

    .line 9168
    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/t;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 9169
    goto/16 :goto_0

    .line 9172
    :cond_e
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->g:Lcom/google/android/gms/games/g/w;

    if-nez v2, :cond_f

    .line 9173
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->g:Lcom/google/android/gms/games/g/w;

    if-eqz v2, :cond_10

    move v0, v1

    .line 9174
    goto/16 :goto_0

    .line 9177
    :cond_f
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->g:Lcom/google/android/gms/games/g/w;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->g:Lcom/google/android/gms/games/g/w;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/w;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 9178
    goto/16 :goto_0

    .line 9181
    :cond_10
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    if-nez v2, :cond_11

    .line 9182
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    if-eqz v2, :cond_12

    move v0, v1

    .line 9183
    goto/16 :goto_0

    .line 9186
    :cond_11
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/ac;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    .line 9187
    goto/16 :goto_0

    .line 9190
    :cond_12
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    if-nez v2, :cond_13

    .line 9191
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    if-eqz v2, :cond_14

    move v0, v1

    .line 9192
    goto/16 :goto_0

    .line 9195
    :cond_13
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/e;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    move v0, v1

    .line 9196
    goto/16 :goto_0

    .line 9199
    :cond_14
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    if-nez v2, :cond_15

    .line 9200
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    if-eqz v2, :cond_16

    move v0, v1

    .line 9201
    goto/16 :goto_0

    .line 9204
    :cond_15
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/ag;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move v0, v1

    .line 9205
    goto/16 :goto_0

    .line 9208
    :cond_16
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    if-nez v2, :cond_17

    .line 9209
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    if-eqz v2, :cond_18

    move v0, v1

    .line 9210
    goto/16 :goto_0

    .line 9213
    :cond_17
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/ap;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_18

    move v0, v1

    .line 9214
    goto/16 :goto_0

    .line 9217
    :cond_18
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    if-nez v2, :cond_19

    .line 9218
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    if-eqz v2, :cond_1a

    move v0, v1

    .line 9219
    goto/16 :goto_0

    .line 9222
    :cond_19
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/as;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    move v0, v1

    .line 9223
    goto/16 :goto_0

    .line 9226
    :cond_1a
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    if-nez v2, :cond_1b

    .line 9227
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    if-eqz v2, :cond_1c

    move v0, v1

    .line 9228
    goto/16 :goto_0

    .line 9231
    :cond_1b
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/ad;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    move v0, v1

    .line 9232
    goto/16 :goto_0

    .line 9235
    :cond_1c
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    if-nez v2, :cond_1d

    .line 9236
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    if-eqz v2, :cond_1e

    move v0, v1

    .line 9237
    goto/16 :goto_0

    .line 9240
    :cond_1d
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/am;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1e

    move v0, v1

    .line 9241
    goto/16 :goto_0

    .line 9244
    :cond_1e
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->o:Lcom/google/android/gms/games/g/ab;

    if-nez v2, :cond_1f

    .line 9245
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->o:Lcom/google/android/gms/games/g/ab;

    if-eqz v2, :cond_20

    move v0, v1

    .line 9246
    goto/16 :goto_0

    .line 9249
    :cond_1f
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->o:Lcom/google/android/gms/games/g/ab;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->o:Lcom/google/android/gms/games/g/ab;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/ab;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    move v0, v1

    .line 9250
    goto/16 :goto_0

    .line 9253
    :cond_20
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    if-nez v2, :cond_21

    .line 9254
    iget-object v2, p1, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    if-eqz v2, :cond_0

    move v0, v1

    .line 9255
    goto/16 :goto_0

    .line 9258
    :cond_21
    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    iget-object v3, p1, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/aq;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 9259
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 9267
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 9270
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 9272
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 9274
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->d:Lcom/google/android/gms/games/g/j;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 9276
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->e:Lcom/google/android/gms/games/g/ae;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 9278
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 9280
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->g:Lcom/google/android/gms/games/g/w;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 9282
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 9284
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 9286
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 9288
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 9290
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    .line 9292
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    .line 9294
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    .line 9296
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->o:Lcom/google/android/gms/games/g/ab;

    if-nez v0, :cond_e

    move v0, v1

    :goto_e
    add-int/2addr v0, v2

    .line 9298
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    if-nez v2, :cond_f

    :goto_f
    add-int/2addr v0, v1

    .line 9300
    return v0

    .line 9267
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/af;->hashCode()I

    move-result v0

    goto :goto_0

    .line 9270
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/ao;->hashCode()I

    move-result v0

    goto :goto_1

    .line 9272
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/v;->hashCode()I

    move-result v0

    goto :goto_2

    .line 9274
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->d:Lcom/google/android/gms/games/g/j;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/j;->hashCode()I

    move-result v0

    goto :goto_3

    .line 9276
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->e:Lcom/google/android/gms/games/g/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/ae;->hashCode()I

    move-result v0

    goto :goto_4

    .line 9278
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/t;->hashCode()I

    move-result v0

    goto :goto_5

    .line 9280
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->g:Lcom/google/android/gms/games/g/w;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/w;->hashCode()I

    move-result v0

    goto :goto_6

    .line 9282
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/ac;->hashCode()I

    move-result v0

    goto :goto_7

    .line 9284
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/e;->hashCode()I

    move-result v0

    goto :goto_8

    .line 9286
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/ag;->hashCode()I

    move-result v0

    goto :goto_9

    .line 9288
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/ap;->hashCode()I

    move-result v0

    goto :goto_a

    .line 9290
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/as;->hashCode()I

    move-result v0

    goto :goto_b

    .line 9292
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/ad;->hashCode()I

    move-result v0

    goto :goto_c

    .line 9294
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/am;->hashCode()I

    move-result v0

    goto :goto_d

    .line 9296
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->o:Lcom/google/android/gms/games/g/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/ab;->hashCode()I

    move-result v0

    goto :goto_e

    .line 9298
    :cond_f
    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    invoke-virtual {v1}, Lcom/google/android/gms/games/g/aq;->hashCode()I

    move-result v1

    goto :goto_f
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 9019
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/g/af;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/af;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/games/g/ao;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/ao;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/games/g/v;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/v;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->d:Lcom/google/android/gms/games/g/j;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/games/g/j;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/j;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->d:Lcom/google/android/gms/games/g/j;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->d:Lcom/google/android/gms/games/g/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->e:Lcom/google/android/gms/games/g/ae;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/gms/games/g/ae;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/ae;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->e:Lcom/google/android/gms/games/g/ae;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->e:Lcom/google/android/gms/games/g/ae;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/android/gms/games/g/t;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->g:Lcom/google/android/gms/games/g/w;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/android/gms/games/g/w;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/w;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->g:Lcom/google/android/gms/games/g/w;

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->g:Lcom/google/android/gms/games/g/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/android/gms/games/g/ac;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/ac;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/android/gms/games/g/e;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/android/gms/games/g/ag;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/ag;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/android/gms/games/g/ap;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/ap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    if-nez v0, :cond_c

    new-instance v0, Lcom/google/android/gms/games/g/as;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/as;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    if-nez v0, :cond_d

    new-instance v0, Lcom/google/android/gms/games/g/ad;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/ad;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    if-nez v0, :cond_e

    new-instance v0, Lcom/google/android/gms/games/g/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/am;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->o:Lcom/google/android/gms/games/g/ab;

    if-nez v0, :cond_f

    new-instance v0, Lcom/google/android/gms/games/g/ab;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/ab;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->o:Lcom/google/android/gms/games/g/ab;

    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->o:Lcom/google/android/gms/games/g/ab;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    if-nez v0, :cond_10

    new-instance v0, Lcom/google/android/gms/games/g/aq;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/aq;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    :cond_10
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x82 -> :sswitch_10
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 9306
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    if-eqz v0, :cond_0

    .line 9307
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->a:Lcom/google/android/gms/games/g/af;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9309
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    if-eqz v0, :cond_1

    .line 9310
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->b:Lcom/google/android/gms/games/g/ao;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9312
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    if-eqz v0, :cond_2

    .line 9313
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->c:Lcom/google/android/gms/games/g/v;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9315
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->d:Lcom/google/android/gms/games/g/j;

    if-eqz v0, :cond_3

    .line 9316
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->d:Lcom/google/android/gms/games/g/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9318
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->e:Lcom/google/android/gms/games/g/ae;

    if-eqz v0, :cond_4

    .line 9319
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->e:Lcom/google/android/gms/games/g/ae;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9321
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    if-eqz v0, :cond_5

    .line 9322
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->f:Lcom/google/android/gms/games/g/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9324
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->g:Lcom/google/android/gms/games/g/w;

    if-eqz v0, :cond_6

    .line 9325
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->g:Lcom/google/android/gms/games/g/w;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9327
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    if-eqz v0, :cond_7

    .line 9328
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->h:Lcom/google/android/gms/games/g/ac;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9330
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    if-eqz v0, :cond_8

    .line 9331
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->i:Lcom/google/android/gms/games/g/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9333
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    if-eqz v0, :cond_9

    .line 9334
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->j:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9336
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    if-eqz v0, :cond_a

    .line 9337
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->k:Lcom/google/android/gms/games/g/ap;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9339
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    if-eqz v0, :cond_b

    .line 9340
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->l:Lcom/google/android/gms/games/g/as;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9342
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    if-eqz v0, :cond_c

    .line 9343
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->m:Lcom/google/android/gms/games/g/ad;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9345
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    if-eqz v0, :cond_d

    .line 9346
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->n:Lcom/google/android/gms/games/g/am;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9348
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->o:Lcom/google/android/gms/games/g/ab;

    if-eqz v0, :cond_e

    .line 9349
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->o:Lcom/google/android/gms/games/g/ab;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9351
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    if-eqz v0, :cond_f

    .line 9352
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/android/gms/games/g/r;->p:Lcom/google/android/gms/games/g/aq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9354
    :cond_f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 9355
    return-void
.end method
