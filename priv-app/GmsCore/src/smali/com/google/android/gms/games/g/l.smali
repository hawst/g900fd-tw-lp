.class public final Lcom/google/android/gms/games/g/l;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Z

.field public c:Ljava/lang/String;

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 212
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 213
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/l;->a:J

    iput-boolean v2, p0, Lcom/google/android/gms/games/g/l;->b:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/games/g/l;->c:Ljava/lang/String;

    iput v2, p0, Lcom/google/android/gms/games/g/l;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/l;->cachedSize:I

    .line 214
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 285
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 286
    iget-wide v2, p0, Lcom/google/android/gms/games/g/l;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 287
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/games/g/l;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 290
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/games/g/l;->b:Z

    if-eqz v1, :cond_1

    .line 291
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/games/g/l;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 294
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/l;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 295
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/g/l;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 298
    :cond_2
    iget v1, p0, Lcom/google/android/gms/games/g/l;->d:I

    if-eqz v1, :cond_3

    .line 299
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/games/g/l;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 302
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 227
    if-ne p1, p0, :cond_1

    .line 250
    :cond_0
    :goto_0
    return v0

    .line 230
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/l;

    if-nez v2, :cond_2

    move v0, v1

    .line 231
    goto :goto_0

    .line 233
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/l;

    .line 234
    iget-wide v2, p0, Lcom/google/android/gms/games/g/l;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/l;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 235
    goto :goto_0

    .line 237
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/games/g/l;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/games/g/l;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 238
    goto :goto_0

    .line 240
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/l;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 241
    iget-object v2, p1, Lcom/google/android/gms/games/g/l;->c:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 242
    goto :goto_0

    .line 244
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/g/l;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/g/l;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 245
    goto :goto_0

    .line 247
    :cond_6
    iget v2, p0, Lcom/google/android/gms/games/g/l;->d:I

    iget v3, p1, Lcom/google/android/gms/games/g/l;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 248
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 255
    iget-wide v0, p0, Lcom/google/android/gms/games/g/l;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/games/g/l;->a:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 258
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/games/g/l;->b:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 259
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/l;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v1

    .line 261
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/l;->d:I

    add-int/2addr v0, v1

    .line 262
    return v0

    .line 258
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/l;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 183
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/l;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/g/l;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/g/l;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/l;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 268
    iget-wide v0, p0, Lcom/google/android/gms/games/g/l;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 269
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/games/g/l;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 271
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/games/g/l;->b:Z

    if-eqz v0, :cond_1

    .line 272
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/gms/games/g/l;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 274
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/l;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 275
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/g/l;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 277
    :cond_2
    iget v0, p0, Lcom/google/android/gms/games/g/l;->d:I

    if-eqz v0, :cond_3

    .line 278
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/games/g/l;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 280
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 281
    return-void
.end method
