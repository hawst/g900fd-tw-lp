.class public final Lcom/google/android/gms/identity/accounts/service/AccountDataService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/identity/accounts/service/f;


# instance fields
.field private final b:Lcom/google/android/gms/identity/accounts/service/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Lcom/google/android/gms/identity/accounts/service/e;

    invoke-direct {v0}, Lcom/google/android/gms/identity/accounts/service/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/identity/accounts/service/AccountDataService;->a:Lcom/google/android/gms/identity/accounts/service/f;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/gms/identity/accounts/service/AccountDataService;->a:Lcom/google/android/gms/identity/accounts/service/f;

    invoke-direct {p0, v0}, Lcom/google/android/gms/identity/accounts/service/AccountDataService;-><init>(Lcom/google/android/gms/identity/accounts/service/f;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/identity/accounts/service/f;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/android/gms/identity/accounts/service/AccountDataService;->b:Lcom/google/android/gms/identity/accounts/service/f;

    .line 46
    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 54
    const-string v0, "com.google.android.gms.accounts.ACCOUNT_SERVICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    new-instance v0, Lcom/google/android/gms/identity/accounts/service/g;

    iget-object v1, p0, Lcom/google/android/gms/identity/accounts/service/AccountDataService;->b:Lcom/google/android/gms/identity/accounts/service/f;

    invoke-interface {v1, p0}, Lcom/google/android/gms/identity/accounts/service/f;->a(Landroid/content/Context;)Lcom/google/android/gms/identity/accounts/service/h;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/identity/accounts/service/g;-><init>(Landroid/content/Context;Lcom/google/android/gms/identity/accounts/service/h;)V

    invoke-virtual {v0}, Lcom/google/android/gms/identity/accounts/service/g;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 58
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
