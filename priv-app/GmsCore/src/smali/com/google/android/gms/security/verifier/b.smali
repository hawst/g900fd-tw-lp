.class final Lcom/google/android/gms/security/verifier/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/w;


# instance fields
.field final synthetic a:Lcom/google/android/gms/security/verifier/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/security/verifier/a;)V
    .locals 0

    .prologue
    .line 93
    iput-object p1, p0, Lcom/google/android/gms/security/verifier/b;->a:Lcom/google/android/gms/security/verifier/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/ac;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 96
    iget-object v0, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-nez v0, :cond_0

    .line 97
    const-string v0, "Unknown error while starting apk upload: %s"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/security/verifier/ah;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/b;->a:Lcom/google/android/gms/security/verifier/a;

    iget-object v0, v0, Lcom/google/android/gms/security/verifier/a;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    .line 116
    :goto_0
    return-void

    .line 103
    :cond_0
    iget-object v0, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget-object v0, v0, Lcom/android/volley/m;->c:Ljava/util/Map;

    const-string v1, "X-Goog-Upload-Status"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 106
    if-eqz v0, :cond_1

    const-string v1, "final"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/b;->a:Lcom/google/android/gms/security/verifier/a;

    iget-object v0, v0, Lcom/google/android/gms/security/verifier/a;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 113
    :cond_1
    const-string v0, "Unknown error while starting apk upload: %s"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/security/verifier/ah;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/b;->a:Lcom/google/android/gms/security/verifier/a;

    iget-object v0, v0, Lcom/google/android/gms/security/verifier/a;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
