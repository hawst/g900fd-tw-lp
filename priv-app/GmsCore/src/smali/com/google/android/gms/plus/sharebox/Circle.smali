.class public final Lcom/google/android/gms/plus/sharebox/Circle;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/sharebox/m;

.field public static a:Lcom/google/android/gms/plus/sharebox/Circle;

.field private static b:Lcom/google/android/gms/plus/sharebox/Circle;

.field private static c:Ljava/lang/String;

.field private static d:I


# instance fields
.field private final e:I

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/plus/sharebox/m;

    invoke-direct {v0}, Lcom/google/android/gms/plus/sharebox/m;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/sharebox/Circle;->CREATOR:Lcom/google/android/gms/plus/sharebox/m;

    .line 22
    const-string v0, "create_circle"

    sput-object v0, Lcom/google/android/gms/plus/sharebox/Circle;->c:Ljava/lang/String;

    .line 24
    const/4 v0, -0x1

    sput v0, Lcom/google/android/gms/plus/sharebox/Circle;->d:I

    .line 29
    new-instance v0, Lcom/google/android/gms/plus/sharebox/Circle;

    const-string v1, ""

    const-string v2, ""

    sget v3, Lcom/google/android/gms/plus/sharebox/Circle;->d:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/plus/sharebox/Circle;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/plus/sharebox/Circle;->a:Lcom/google/android/gms/plus/sharebox/Circle;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    iput p1, p0, Lcom/google/android/gms/plus/sharebox/Circle;->e:I

    .line 83
    iput-object p2, p0, Lcom/google/android/gms/plus/sharebox/Circle;->f:Ljava/lang/String;

    .line 84
    iput-object p3, p0, Lcom/google/android/gms/plus/sharebox/Circle;->g:Ljava/lang/String;

    .line 85
    iput p4, p0, Lcom/google/android/gms/plus/sharebox/Circle;->h:I

    .line 86
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/people/model/d;)V
    .locals 4

    .prologue
    .line 69
    const/4 v0, 0x1

    invoke-interface {p1}, Lcom/google/android/gms/people/model/d;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/people/model/d;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/people/model/d;->g()I

    move-result v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/plus/sharebox/Circle;-><init>(ILjava/lang/String;Ljava/lang/String;I)V

    .line 70
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 65
    const/4 v0, 0x1

    sget v1, Lcom/google/android/gms/plus/sharebox/Circle;->d:I

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/google/android/gms/plus/sharebox/Circle;-><init>(ILjava/lang/String;Ljava/lang/String;I)V

    .line 66
    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/plus/sharebox/Circle;-><init>(ILjava/lang/String;Ljava/lang/String;I)V

    .line 74
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/plus/sharebox/Circle;
    .locals 4

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gms/plus/sharebox/Circle;->b:Lcom/google/android/gms/plus/sharebox/Circle;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Lcom/google/android/gms/plus/sharebox/Circle;

    sget-object v1, Lcom/google/android/gms/plus/sharebox/Circle;->c:Ljava/lang/String;

    sget v2, Lcom/google/android/gms/p;->vA:I

    invoke-virtual {p0, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    sget v3, Lcom/google/android/gms/plus/sharebox/Circle;->d:I

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/plus/sharebox/Circle;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    sput-object v0, Lcom/google/android/gms/plus/sharebox/Circle;->b:Lcom/google/android/gms/plus/sharebox/Circle;

    .line 53
    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/sharebox/Circle;->b:Lcom/google/android/gms/plus/sharebox/Circle;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/plus/sharebox/Circle;)Z
    .locals 2

    .prologue
    .line 61
    if-eqz p0, :cond_0

    sget-object v0, Lcom/google/android/gms/plus/sharebox/Circle;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/Circle;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/gms/plus/sharebox/Circle;->e:I

    return v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/Circle;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/Circle;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 109
    iget v0, p0, Lcom/google/android/gms/plus/sharebox/Circle;->h:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/gms/plus/sharebox/Circle;

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/Circle;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    .line 131
    iget v0, p0, Lcom/google/android/gms/plus/sharebox/Circle;->h:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_0

    .line 132
    const-string v0, " ("

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/android/gms/plus/sharebox/Circle;->h:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ") "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    :cond_0
    const-string v0, ">"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 131
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 124
    invoke-static {p0, p1}, Lcom/google/android/gms/plus/sharebox/m;->a(Lcom/google/android/gms/plus/sharebox/Circle;Landroid/os/Parcel;)V

    .line 125
    return-void
.end method
