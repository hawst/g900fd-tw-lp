.class public final Lcom/google/android/gms/droidguard/d/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/List;

.field private b:Ljava/lang/Object;

.field private c:Z

.field private d:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/gms/droidguard/d/m;)Lcom/google/android/gms/droidguard/d/k;
    .locals 1

    .prologue
    .line 91
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/droidguard/d/k;->c:Z

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/k;->b:Ljava/lang/Object;

    invoke-interface {p1, v0}, Lcom/google/android/gms/droidguard/d/m;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 101
    :goto_0
    monitor-exit p0

    return-object p0

    .line 93
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/k;->d:Ljava/lang/Throwable;

    if-eqz v0, :cond_1

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/k;->d:Ljava/lang/Throwable;

    invoke-interface {p1, v0}, Lcom/google/android/gms/droidguard/d/m;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 91
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 96
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/k;->a:Ljava/util/List;

    if-nez v0, :cond_2

    .line 97
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/droidguard/d/k;->a:Ljava/util/List;

    .line 99
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/k;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/droidguard/d/n;)Lcom/google/android/gms/droidguard/d/k;
    .locals 2

    .prologue
    .line 67
    new-instance v0, Lcom/google/android/gms/droidguard/d/k;

    invoke-direct {v0}, Lcom/google/android/gms/droidguard/d/k;-><init>()V

    .line 68
    new-instance v1, Lcom/google/android/gms/droidguard/d/l;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/gms/droidguard/d/l;-><init>(Lcom/google/android/gms/droidguard/d/k;Lcom/google/android/gms/droidguard/d/n;Lcom/google/android/gms/droidguard/d/k;)V

    invoke-virtual {p0, v1}, Lcom/google/android/gms/droidguard/d/k;->a(Lcom/google/android/gms/droidguard/d/m;)Lcom/google/android/gms/droidguard/d/k;

    .line 79
    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/Object;)Lcom/google/android/gms/droidguard/d/k;
    .locals 2

    .prologue
    .line 27
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/droidguard/d/k;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/k;->d:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, p0

    .line 38
    :goto_0
    monitor-exit p0

    return-object v0

    .line 30
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/droidguard/d/k;->c:Z

    .line 31
    iput-object p1, p0, Lcom/google/android/gms/droidguard/d/k;->b:Ljava/lang/Object;

    .line 32
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/k;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/k;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/droidguard/d/m;

    .line 34
    invoke-interface {v0, p1}, Lcom/google/android/gms/droidguard/d/m;->a(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 27
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 37
    :cond_2
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/gms/droidguard/d/k;->a:Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, p0

    .line 38
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/droidguard/d/k;
    .locals 1

    .prologue
    .line 56
    new-instance v0, Ljava/lang/Exception;

    invoke-direct {v0, p1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/Throwable;)Lcom/google/android/gms/droidguard/d/k;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/Throwable;)Lcom/google/android/gms/droidguard/d/k;
    .locals 2

    .prologue
    .line 42
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/droidguard/d/k;->c:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/k;->d:Ljava/lang/Throwable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, p0

    .line 52
    :goto_0
    monitor-exit p0

    return-object v0

    .line 45
    :cond_1
    :try_start_1
    iput-object p1, p0, Lcom/google/android/gms/droidguard/d/k;->d:Ljava/lang/Throwable;

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/k;->a:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/k;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/droidguard/d/m;

    .line 48
    invoke-interface {v0, p1}, Lcom/google/android/gms/droidguard/d/m;->a(Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 42
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 51
    :cond_2
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/gms/droidguard/d/k;->a:Ljava/util/List;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, p0

    .line 52
    goto :goto_0
.end method
