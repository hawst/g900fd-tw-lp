.class public Lcom/google/android/gms/common/server/h;
.super Lcom/google/android/gms/common/server/m;
.source "SourceFile"


# instance fields
.field private final f:Ljava/lang/Class;

.field private final g:Ljava/lang/Object;


# direct methods
.method public constructor <init>(ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V
    .locals 12

    .prologue
    .line 74
    if-nez p3, :cond_0

    const/4 v3, 0x0

    :goto_0
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/common/server/h;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V

    .line 77
    return-void

    .line 74
    :cond_0
    invoke-virtual {p3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method protected constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V
    .locals 10

    .prologue
    .line 100
    move-object v0, p0

    move v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object/from16 v4, p6

    move-object/from16 v5, p7

    move-object/from16 v6, p8

    move-object/from16 v7, p9

    move/from16 v8, p10

    move-object/from16 v9, p11

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/common/server/m;-><init>(ILjava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V

    .line 103
    iput-object p4, p0, Lcom/google/android/gms/common/server/h;->f:Ljava/lang/Class;

    .line 104
    iput-object p5, p0, Lcom/google/android/gms/common/server/h;->g:Ljava/lang/Object;

    .line 105
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Class;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V
    .locals 12

    .prologue
    .line 91
    const/4 v1, -0x1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move/from16 v10, p8

    move-object/from16 v11, p9

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/common/server/h;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V

    .line 94
    return-void
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 5

    .prologue
    .line 120
    :try_start_0
    iget-boolean v0, p0, Lcom/android/volley/p;->c:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/android/volley/toolbox/i;->a(Lcom/android/volley/m;)Lcom/android/volley/c;

    move-result-object v0

    move-object v1, v0

    .line 123
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/server/h;->g:Ljava/lang/Object;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/server/h;->f:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;

    .line 124
    :goto_1
    iget v2, p1, Lcom/android/volley/m;->a:I

    iget-object v3, p1, Lcom/android/volley/m;->b:[B

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse;->a(I[B)V

    .line 125
    invoke-static {v0, v1}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    .line 127
    :goto_2
    return-object v0

    .line 120
    :cond_0
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_0

    .line 123
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/server/h;->f:Ljava/lang/Class;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/common/server/h;->g:Ljava/lang/Object;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/common/server/h;->g:Ljava/lang/Object;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lcom/google/android/gms/common/server/response/m; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_5

    goto :goto_1

    .line 126
    :catch_0
    move-exception v0

    .line 127
    new-instance v1, Lcom/android/volley/o;

    invoke-direct {v1, v0}, Lcom/android/volley/o;-><init>(Ljava/lang/Throwable;)V

    invoke-static {v1}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_2

    .line 128
    :catch_1
    move-exception v0

    .line 130
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 131
    :catch_2
    move-exception v0

    .line 133
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 134
    :catch_3
    move-exception v0

    .line 135
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 136
    :catch_4
    move-exception v0

    .line 137
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 138
    :catch_5
    move-exception v0

    .line 139
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
