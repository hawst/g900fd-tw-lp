.class final Lcom/google/android/gms/wallet/ow/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/w;


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ow/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ow/g;)V
    .locals 0

    .prologue
    .line 1464
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1502
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->h(Lcom/google/android/gms/wallet/ow/g;)V

    .line 1503
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/a/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1469
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    if-eq p1, v0, :cond_0

    .line 1470
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/ow/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ow/g;->d(Lcom/google/android/gms/wallet/ow/g;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;->a(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    .line 1473
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-nez v0, :cond_2

    .line 1475
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    if-eqz v0, :cond_1

    .line 1498
    :goto_0
    return-void

    .line 1478
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->j:Lcom/google/checkout/inapp/proto/a/b;

    .line 1480
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Lcom/google/checkout/inapp/proto/a/b;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 1494
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object p1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->c:Lcom/google/checkout/inapp/proto/a/b;

    .line 1495
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ow/g;->e(Lcom/google/android/gms/wallet/ow/g;)V

    goto :goto_0

    .line 1482
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/ow/g;->A:Z

    if-eqz v0, :cond_3

    .line 1483
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ow/g;->b(Lcom/google/android/gms/wallet/ow/g;Lcom/google/checkout/inapp/proto/a/b;)V

    goto :goto_0

    .line 1487
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ow/g;->z:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x3

    iput v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->g:I

    .line 1488
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/k;->a:Lcom/google/android/gms/wallet/ow/g;

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/ow/g;->a(Lcom/google/android/gms/wallet/ow/g;Z)V

    goto :goto_0

    .line 1480
    :pswitch_data_0
    .packed-switch 0x7b
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
