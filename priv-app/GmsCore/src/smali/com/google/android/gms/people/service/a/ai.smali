.class public final Lcom/google/android/gms/people/service/a/ai;
.super Lcom/google/android/gms/people/service/a/a;
.source "SourceFile"


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:I

.field private final k:I

.field private final l:J

.field private final m:Lcom/google/android/gms/people/model/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Lcom/google/android/gms/common/data/DataHolder;IIJ)V
    .locals 15

    .prologue
    .line 51
    new-instance v9, Lcom/google/android/gms/people/model/b;

    move-object/from16 v0, p6

    invoke-direct {v9, v0}, Lcom/google/android/gms/people/model/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    move-object v3, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    move/from16 v7, p4

    move-object/from16 v8, p5

    move/from16 v10, p7

    move/from16 v11, p8

    move-wide/from16 v12, p9

    invoke-direct/range {v3 .. v13}, Lcom/google/android/gms/people/service/a/ai;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Lcom/google/android/gms/people/model/b;IIJ)V

    .line 53
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Lcom/google/android/gms/people/model/b;IIJ)V
    .locals 9

    .prologue
    .line 59
    invoke-virtual {p6}, Lcom/google/android/gms/people/model/b;->g()Ljava/lang/String;

    move-result-object v7

    move-object v2, p0

    move-object v3, p1

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/people/service/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;)V

    .line 61
    iput-object p2, p0, Lcom/google/android/gms/people/service/a/ai;->g:Ljava/lang/String;

    .line 62
    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/gms/people/service/a/ai;->j:I

    .line 63
    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/gms/people/service/a/ai;->k:I

    .line 64
    move-wide/from16 v0, p9

    iput-wide v0, p0, Lcom/google/android/gms/people/service/a/ai;->l:J

    .line 65
    iput-object p6, p0, Lcom/google/android/gms/people/service/a/ai;->m:Lcom/google/android/gms/people/model/b;

    .line 66
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/ai;->m:Lcom/google/android/gms/people/model/b;

    invoke-virtual {v2}, Lcom/google/android/gms/people/model/b;->h()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/people/service/a/ai;->h:Ljava/lang/String;

    .line 67
    iget-object v2, p0, Lcom/google/android/gms/people/service/a/ai;->m:Lcom/google/android/gms/people/model/b;

    invoke-virtual {v2}, Lcom/google/android/gms/people/model/b;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/people/service/a/ai;->i:Ljava/lang/String;

    .line 70
    invoke-virtual {p6}, Lcom/google/android/gms/people/model/b;->i()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/people/service/a/ai;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    .line 71
    return-void
.end method

.method private a(I)Z
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/ai;->m:Lcom/google/android/gms/people/model/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/model/b;->b(I)Lcom/google/android/gms/people/model/c;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/people/model/c;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 188
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 185
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 181
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private a(Ljava/util/ArrayList;I)Z
    .locals 3

    .prologue
    .line 153
    invoke-direct {p0, p2}, Lcom/google/android/gms/people/service/a/ai;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 154
    const/4 v0, 0x0

    .line 174
    :goto_0
    return v0

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/ai;->m:Lcom/google/android/gms/people/model/b;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/people/model/b;->b(I)Lcom/google/android/gms/people/model/c;

    move-result-object v0

    .line 159
    new-instance v1, Lcom/google/android/gms/people/d/e;

    invoke-direct {v1}, Lcom/google/android/gms/people/d/e;-><init>()V

    .line 160
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 162
    invoke-interface {v0}, Lcom/google/android/gms/people/model/c;->d()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 163
    invoke-interface {v0}, Lcom/google/android/gms/people/model/c;->d()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/people/d/e;->a:Ljava/lang/String;

    .line 166
    :cond_1
    new-instance v0, Lcom/google/android/gms/people/d/f;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/f;-><init>()V

    .line 167
    iput-object v0, v1, Lcom/google/android/gms/people/d/e;->c:Lcom/google/android/gms/people/d/f;

    .line 169
    new-instance v1, Lcom/google/android/gms/people/d/g;

    invoke-direct {v1}, Lcom/google/android/gms/people/d/g;-><init>()V

    .line 170
    iput-object v1, v0, Lcom/google/android/gms/people/d/f;->a:Lcom/google/android/gms/people/d/g;

    .line 172
    iput p2, v1, Lcom/google/android/gms/people/d/g;->a:I

    .line 174
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final f()Landroid/util/Pair;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x2

    .line 75
    sget-object v0, Lcom/google/android/gms/people/a/a;->aC:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    new-instance v4, Lcom/google/android/gms/people/d/b;

    invoke-direct {v4}, Lcom/google/android/gms/people/d/b;-><init>()V

    iget v0, p0, Lcom/google/android/gms/people/service/a/ai;->j:I

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    iput v0, v4, Lcom/google/android/gms/people/d/b;->a:I

    iget v0, v4, Lcom/google/android/gms/people/d/b;->a:I

    if-ne v0, v1, :cond_3

    iget v0, p0, Lcom/google/android/gms/people/service/a/ai;->j:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/service/a/ai;->a(I)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v3

    .line 77
    :goto_1
    if-eqz v0, :cond_0

    .line 78
    iget-object v1, p0, Lcom/google/android/gms/people/service/a/ai;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/ah;->k()Lcom/google/android/gms/people/f/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/people/service/a;->d:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/android/gms/people/f/b;->a:Lcom/google/android/gms/people/f/c;

    const/16 v1, 0x29

    invoke-static {v3, v2, v1, v0}, Lcom/google/android/gms/people/f/c;->a(Ljava/lang/String;Ljava/lang/String;ILcom/google/protobuf/nano/j;)V

    .line 82
    :cond_0
    new-instance v0, Landroid/util/Pair;

    sget-object v1, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    invoke-direct {v0, v1, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v0

    :cond_1
    move v0, v2

    .line 76
    goto :goto_0

    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    iget v2, p0, Lcom/google/android/gms/people/service/a/ai;->j:I

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/people/service/a/ai;->a(Ljava/util/ArrayList;I)Z

    :goto_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/gms/people/d/e;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/people/d/e;

    iput-object v0, v4, Lcom/google/android/gms/people/d/b;->b:[Lcom/google/android/gms/people/d/e;

    new-instance v0, Lcom/google/android/gms/people/d/i;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/i;-><init>()V

    iput-object v0, v4, Lcom/google/android/gms/people/d/b;->c:Lcom/google/android/gms/people/d/i;

    iget-wide v6, p0, Lcom/google/android/gms/people/service/a/ai;->l:J

    iput-wide v6, v0, Lcom/google/android/gms/people/d/i;->b:J

    new-instance v0, Lcom/google/android/gms/people/d/h;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/h;-><init>()V

    iput-object v0, v4, Lcom/google/android/gms/people/d/b;->d:Lcom/google/android/gms/people/d/h;

    new-instance v2, Lcom/google/android/gms/people/d/c;

    invoke-direct {v2}, Lcom/google/android/gms/people/d/c;-><init>()V

    iput-object v2, v0, Lcom/google/android/gms/people/d/h;->a:Lcom/google/android/gms/people/d/c;

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/ai;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    iput v0, v2, Lcom/google/android/gms/people/d/c;->a:I

    new-instance v0, Lcom/google/android/gms/people/d/d;

    invoke-direct {v0}, Lcom/google/android/gms/people/d/d;-><init>()V

    iput-object v0, v4, Lcom/google/android/gms/people/d/b;->e:Lcom/google/android/gms/people/d/d;

    new-instance v2, Lcom/google/android/gms/people/d/j;

    invoke-direct {v2}, Lcom/google/android/gms/people/d/j;-><init>()V

    iput-object v2, v0, Lcom/google/android/gms/people/d/d;->a:Lcom/google/android/gms/people/d/j;

    iget-object v5, p0, Lcom/google/android/gms/people/service/a/ai;->b:Ljava/lang/String;

    iput-object v5, v0, Lcom/google/android/gms/people/d/d;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/ai;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/ew;->a(Landroid/content/res/Resources;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x3

    :goto_3
    iput v0, v2, Lcom/google/android/gms/people/d/j;->a:I

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/ai;->g:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/people/f/b;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, v2, Lcom/google/android/gms/people/d/j;->b:I

    iput v1, v2, Lcom/google/android/gms/people/d/j;->c:I

    const/16 v0, 0x64

    iput v0, v2, Lcom/google/android/gms/people/d/j;->d:I

    move-object v0, v4

    goto/16 :goto_1

    :cond_3
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/people/service/a/ai;->m:Lcom/google/android/gms/people/model/b;

    invoke-virtual {v0}, Lcom/google/android/gms/people/model/b;->c()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v0, 0x0

    :goto_4
    iget-object v5, p0, Lcom/google/android/gms/people/service/a/ai;->m:Lcom/google/android/gms/people/model/b;

    invoke-virtual {v5}, Lcom/google/android/gms/people/model/b;->c()I

    move-result v5

    if-ge v0, v5, :cond_6

    invoke-direct {p0, v2, v0}, Lcom/google/android/gms/people/service/a/ai;->a(Ljava/util/ArrayList;I)Z

    move-result v5

    if-nez v5, :cond_4

    if-nez v0, :cond_6

    move-object v0, v3

    goto/16 :goto_1

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    move v0, v1

    goto :goto_3

    :cond_6
    move-object v0, v2

    goto/16 :goto_2
.end method
