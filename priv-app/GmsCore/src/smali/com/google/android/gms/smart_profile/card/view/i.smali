.class final Lcom/google/android/gms/smart_profile/card/view/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)V
    .locals 0

    .prologue
    .line 278
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/card/view/i;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;B)V
    .locals 0

    .prologue
    .line 278
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/card/view/i;-><init>(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/i;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->a(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Landroid/view/View;

    move-result-object v0

    if-ne p1, v0, :cond_2

    .line 283
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/i;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->b(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/i;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->c(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)V

    .line 314
    :cond_0
    :goto_0
    return-void

    .line 286
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/i;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->d(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)V

    goto :goto_0

    .line 288
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/i;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Lcom/google/android/gms/smart_profile/card/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 290
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->az:I

    if-ne v0, v1, :cond_4

    .line 291
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 292
    sget v1, Lcom/google/android/gms/k;->A:I

    invoke-virtual {v0, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Intent;

    .line 294
    sget v2, Lcom/google/android/gms/k;->z:I

    invoke-virtual {v0, v2}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 296
    if-eqz v1, :cond_3

    .line 297
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/card/view/i;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Lcom/google/android/gms/smart_profile/card/h;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/card/view/i;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v3}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->f(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/smart_profile/card/h;->a(Landroid/content/Intent;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    :cond_3
    move-object p1, v0

    .line 306
    :cond_4
    sget v0, Lcom/google/android/gms/k;->D:I

    invoke-virtual {p1, v0}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 307
    sget v1, Lcom/google/android/gms/k;->B:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    .line 309
    if-eqz v0, :cond_0

    .line 310
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/card/view/i;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v2}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->e(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Lcom/google/android/gms/smart_profile/card/h;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/card/view/i;->a:Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;

    invoke-static {v3}, Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;->f(Lcom/google/android/gms/smart_profile/card/view/ExpandingEntryCardView;)Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v3

    invoke-interface {v2, v0, v1, v3}, Lcom/google/android/gms/smart_profile/card/h;->a(Landroid/content/Intent;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0
.end method
