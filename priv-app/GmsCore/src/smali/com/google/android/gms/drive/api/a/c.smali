.class public abstract Lcom/google/android/gms/drive/api/a/c;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    .line 22
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 27
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 4

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/c;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/api/d;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 40
    new-instance v0, Lcom/google/android/gms/common/service/h;

    const/16 v1, 0xa

    const-string v2, "App is not authorized to make this request."

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/service/h;-><init>(ILjava/lang/String;B)V

    throw v0

    .line 43
    :cond_0
    invoke-virtual {p0, p2}, Lcom/google/android/gms/drive/api/a/c;->a(Lcom/google/android/gms/drive/auth/g;)V

    .line 44
    return-void
.end method

.method public abstract a(Lcom/google/android/gms/drive/auth/g;)V
.end method
