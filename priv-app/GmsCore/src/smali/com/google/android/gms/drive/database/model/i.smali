.class public final Lcom/google/android/gms/drive/database/model/i;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Z

.field public c:Z

.field public final d:J

.field private final g:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;JJLjava/lang/Long;ZZ)V
    .locals 2

    .prologue
    .line 27
    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->a()Lcom/google/android/gms/drive/database/model/j;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 28
    iput-wide p2, p0, Lcom/google/android/gms/drive/database/model/i;->g:J

    .line 29
    iput-wide p4, p0, Lcom/google/android/gms/drive/database/model/i;->d:J

    .line 30
    iput-object p6, p0, Lcom/google/android/gms/drive/database/model/i;->a:Ljava/lang/Long;

    .line 31
    iput-boolean p7, p0, Lcom/google/android/gms/drive/database/model/i;->b:Z

    .line 32
    iput-boolean p8, p0, Lcom/google/android/gms/drive/database/model/i;->c:Z

    .line 33
    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/i;
    .locals 9

    .prologue
    .line 98
    sget-object v0, Lcom/google/android/gms/drive/database/model/k;->b:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 99
    sget-object v0, Lcom/google/android/gms/drive/database/model/k;->a:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 100
    sget-object v0, Lcom/google/android/gms/drive/database/model/k;->d:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v6

    .line 101
    sget-object v0, Lcom/google/android/gms/drive/database/model/k;->e:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->e(Landroid/database/Cursor;)Z

    move-result v7

    .line 103
    sget-object v0, Lcom/google/android/gms/drive/database/model/k;->c:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/database/model/ab;->e(Landroid/database/Cursor;)Z

    move-result v8

    .line 104
    new-instance v0, Lcom/google/android/gms/drive/database/model/i;

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/drive/database/model/i;-><init>(Lcom/google/android/gms/drive/database/i;JJLjava/lang/Long;ZZ)V

    .line 107
    invoke-static {}, Lcom/google/android/gms/drive/database/model/j;->a()Lcom/google/android/gms/drive/database/model/j;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/j;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/drive/database/model/i;->d(J)V

    .line 109
    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 37
    sget-object v0, Lcom/google/android/gms/drive/database/model/k;->b:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/i;->g:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 38
    sget-object v0, Lcom/google/android/gms/drive/database/model/k;->a:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/i;->d:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 39
    sget-object v0, Lcom/google/android/gms/drive/database/model/k;->d:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/i;->a:Ljava/lang/Long;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 40
    sget-object v0, Lcom/google/android/gms/drive/database/model/k;->e:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/drive/database/model/i;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 42
    sget-object v0, Lcom/google/android/gms/drive/database/model/k;->c:Lcom/google/android/gms/drive/database/model/k;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/k;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/drive/database/model/i;->c:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 43
    return-void
.end method
