.class public Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;
.super Lcom/google/android/gms/wallet/common/ui/validator/r;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/validator/c;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/validator/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/validator/r;-><init>(Ljava/lang/CharSequence;)V

    .line 20
    const-string v0, "Blacklisted input must not be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 21
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;->a:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/TextView;)Z
    .locals 2

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;->a:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 36
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;->c()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 37
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    .line 38
    return-void
.end method
