.class final Lcom/google/android/gms/drive/b/a/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/b/a/m;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:[B

.field private d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:B

.field private i:Z


# direct methods
.method constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-byte v0, p0, Lcom/google/android/gms/drive/b/a/o;->h:B

    .line 23
    iput-boolean v0, p0, Lcom/google/android/gms/drive/b/a/o;->i:Z

    .line 27
    if-lez p1, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 28
    iput v1, p0, Lcom/google/android/gms/drive/b/a/o;->a:I

    .line 29
    const v0, 0xfff1

    iput v0, p0, Lcom/google/android/gms/drive/b/a/o;->b:I

    .line 31
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    .line 32
    invoke-virtual {p0}, Lcom/google/android/gms/drive/b/a/o;->c()V

    .line 33
    return-void
.end method

.method private a(II)Ljava/lang/String;
    .locals 4

    .prologue
    .line 134
    new-instance v0, Lcom/google/android/gms/drive/b/a/a/a;

    invoke-direct {v0}, Lcom/google/android/gms/drive/b/a/a/a;-><init>()V

    .line 135
    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v1, v1

    sub-int/2addr v1, p1

    invoke-static {p2, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 136
    iget-object v2, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    invoke-virtual {v0, v2, p1, v1}, Lcom/google/android/gms/drive/b/a/a/a;->a([BII)V

    .line 137
    if-ge v1, p2, :cond_0

    .line 138
    iget-object v2, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    const/4 v3, 0x0

    sub-int v1, p2, v1

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/drive/b/a/a/a;->a([BII)V

    .line 140
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/b/a/a/a;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d(I)I
    .locals 2

    .prologue
    .line 171
    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->b:I

    rem-int v0, p1, v0

    iget v1, p0, Lcom/google/android/gms/drive/b/a/o;->b:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/drive/b/a/o;->b:I

    rem-int/2addr v0, v1

    return v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v0, v0

    return v0
.end method

.method public final a(I)I
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 98
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v0, v0

    if-gt p1, v0, :cond_1

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 102
    iget v2, p0, Lcom/google/android/gms/drive/b/a/o;->d:I

    .line 103
    iget v1, p0, Lcom/google/android/gms/drive/b/a/o;->e:I

    .line 104
    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->g:I

    :goto_2
    if-le v0, p1, :cond_2

    .line 105
    iget-object v3, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    iget-object v4, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v4, v4

    iget v5, p0, Lcom/google/android/gms/drive/b/a/o;->f:I

    add-int/2addr v4, v5

    sub-int/2addr v4, v0

    iget-object v5, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v5, v5

    rem-int/2addr v4, v5

    aget-byte v3, v3, v4

    and-int/lit16 v3, v3, 0xff

    .line 106
    sub-int/2addr v2, v3

    invoke-direct {p0, v2}, Lcom/google/android/gms/drive/b/a/o;->d(I)I

    move-result v2

    .line 107
    mul-int/2addr v3, v0

    sub-int/2addr v1, v3

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/google/android/gms/drive/b/a/o;->d(I)I

    move-result v1

    .line 104
    add-int/lit8 v0, v0, -0x1

    goto :goto_2

    :cond_0
    move v0, v2

    .line 98
    goto :goto_0

    :cond_1
    move v1, v2

    .line 99
    goto :goto_1

    .line 109
    :cond_2
    const v0, 0xffff

    and-int/2addr v0, v2

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    return v0
.end method

.method public final a(B)V
    .locals 3

    .prologue
    .line 65
    and-int/lit16 v0, p1, 0xff

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/drive/b/a/o;->b()Z

    move-result v1

    if-nez v1, :cond_0

    .line 67
    iget v1, p0, Lcom/google/android/gms/drive/b/a/o;->d:I

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/b/a/o;->d(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/b/a/o;->d:I

    .line 68
    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->e:I

    iget v1, p0, Lcom/google/android/gms/drive/b/a/o;->d:I

    add-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/b/a/o;->d(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/b/a/o;->e:I

    .line 69
    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/b/a/o;->g:I

    .line 77
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    iget v1, p0, Lcom/google/android/gms/drive/b/a/o;->f:I

    aput-byte p1, v0, v1

    .line 78
    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->f:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v1, v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/drive/b/a/o;->f:I

    .line 79
    return-void

    .line 71
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    iget v2, p0, Lcom/google/android/gms/drive/b/a/o;->f:I

    aget-byte v1, v1, v2

    and-int/lit16 v1, v1, 0xff

    .line 72
    iget v2, p0, Lcom/google/android/gms/drive/b/a/o;->d:I

    add-int/2addr v0, v2

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/b/a/o;->d(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/b/a/o;->d:I

    .line 73
    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->e:I

    iget v2, p0, Lcom/google/android/gms/drive/b/a/o;->d:I

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v2, v2

    mul-int/2addr v1, v2

    sub-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/b/a/o;->d(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/b/a/o;->e:I

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    iget v1, p0, Lcom/google/android/gms/drive/b/a/o;->f:I

    aget-byte v0, v0, v1

    iput-byte v0, p0, Lcom/google/android/gms/drive/b/a/o;->h:B

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/b/a/o;->i:Z

    goto :goto_0
.end method

.method public final b(I)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 127
    if-ltz p1, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v0, v0

    if-gt p1, v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 129
    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->g:I

    if-lt v0, p1, :cond_2

    :goto_2
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v0, v0

    iget v1, p0, Lcom/google/android/gms/drive/b/a/o;->f:I

    add-int/2addr v0, v1

    sub-int/2addr v0, p1

    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v1, v1

    rem-int/2addr v0, v1

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/drive/b/a/o;->a(II)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 127
    goto :goto_0

    :cond_1
    move v0, v2

    .line 128
    goto :goto_1

    :cond_2
    move v1, v2

    .line 129
    goto :goto_2
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->g:I

    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v1, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)B
    .locals 2

    .prologue
    .line 160
    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->g:I

    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 163
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    aget-byte v0, v0, p1

    return v0

    .line 160
    :cond_0
    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->f:I

    add-int/2addr v0, p1

    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v1, v1

    rem-int p1, v0, v1

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    iput v1, p0, Lcom/google/android/gms/drive/b/a/o;->f:I

    .line 54
    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->a:I

    iput v0, p0, Lcom/google/android/gms/drive/b/a/o;->d:I

    .line 55
    iput v1, p0, Lcom/google/android/gms/drive/b/a/o;->e:I

    .line 56
    iput v1, p0, Lcom/google/android/gms/drive/b/a/o;->g:I

    .line 57
    iput-boolean v1, p0, Lcom/google/android/gms/drive/b/a/o;->i:Z

    .line 58
    return-void
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/drive/b/a/o;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 87
    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->d:I

    const v1, 0xffff

    and-int/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/drive/b/a/o;->e:I

    shl-int/lit8 v1, v1, 0x10

    or-int/2addr v0, v1

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 117
    invoke-virtual {p0}, Lcom/google/android/gms/drive/b/a/o;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 118
    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->f:I

    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v1, v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/b/a/o;->a(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()B
    .locals 1

    .prologue
    .line 145
    iget-byte v0, p0, Lcom/google/android/gms/drive/b/a/o;->h:B

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 150
    iget-boolean v0, p0, Lcom/google/android/gms/drive/b/a/o;->i:Z

    return v0
.end method

.method public final h()I
    .locals 2

    .prologue
    .line 155
    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->g:I

    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/drive/b/a/o;->g:I

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/o;->c:[B

    array-length v0, v0

    goto :goto_0
.end method
