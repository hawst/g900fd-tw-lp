.class public final Lcom/google/android/gms/common/images/ImageBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 21
    const-string v0, "com.google.android.gms.extras.resultReceiver"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/os/ResultReceiver;

    .line 23
    const-string v1, "com.google.android.gms.extras.uri"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    .line 24
    const-string v2, "com.google.android.gms.extras.priority"

    const/4 v3, 0x1

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 25
    invoke-static {p1, v1, v2, v0}, Lcom/google/android/gms/common/images/ImageMultiThreadedIntentService;->a(Landroid/content/Context;Landroid/net/Uri;ILandroid/os/ResultReceiver;)V

    .line 26
    return-void
.end method
