.class public final Lcom/google/android/gms/plus/service/v1/PeopleFeed;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v1/d;

.field private static final a:Ljava/util/HashMap;


# instance fields
.field private final b:Ljava/util/Set;

.field private final c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/util/List;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/plus/service/v1/d;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->CREATOR:Lcom/google/android/gms/plus/service/v1/d;

    .line 73
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 76
    sput-object v0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->a:Ljava/util/HashMap;

    const-string v1, "etag"

    const-string v2, "etag"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->a:Ljava/util/HashMap;

    const-string v1, "items"

    const-string v2, "items"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->a:Ljava/util/HashMap;

    const-string v1, "nextPageToken"

    const-string v2, "nextPageToken"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->a:Ljava/util/HashMap;

    const-string v1, "selfLink"

    const-string v2, "selfLink"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->a:Ljava/util/HashMap;

    const-string v1, "title"

    const-string v2, "title"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->a:Ljava/util/HashMap;

    const-string v1, "totalItems"

    const-string v2, "totalItems"

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 142
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->c:I

    .line 143
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->b:Ljava/util/Set;

    .line 144
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 157
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->b:Ljava/util/Set;

    .line 158
    iput p2, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->c:I

    .line 159
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->d:Ljava/lang/String;

    .line 160
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->e:Ljava/util/List;

    .line 161
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->f:Ljava/lang/String;

    .line 162
    iput-object p6, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->g:Ljava/lang/String;

    .line 163
    iput-object p7, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->h:Ljava/lang/String;

    .line 164
    iput p8, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->i:I

    .line 165
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 372
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 373
    packed-switch v0, :pswitch_data_0

    .line 378
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be an int."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 375
    :pswitch_0
    iput p3, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->i:I

    .line 381
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 382
    return-void

    .line 373
    :pswitch_data_0
    .packed-switch 0x8
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 387
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 388
    packed-switch v0, :pswitch_data_0

    .line 402
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 390
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->d:Ljava/lang/String;

    .line 405
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 406
    return-void

    .line 393
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->f:Ljava/lang/String;

    goto :goto_0

    .line 396
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->g:Ljava/lang/String;

    goto :goto_0

    .line 399
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->h:Ljava/lang/String;

    goto :goto_0

    .line 388
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    .prologue
    .line 411
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 412
    packed-switch v0, :pswitch_data_0

    .line 417
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known array of custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 414
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->e:Ljava/util/List;

    .line 421
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 422
    return-void

    .line 412
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->b:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 327
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 341
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 329
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->d:Ljava/lang/String;

    .line 339
    :goto_0
    return-object v0

    .line 331
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->e:Ljava/util/List;

    goto :goto_0

    .line 333
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->f:Ljava/lang/String;

    goto :goto_0

    .line 335
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->g:Ljava/lang/String;

    goto :goto_0

    .line 337
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->h:Ljava/lang/String;

    goto :goto_0

    .line 339
    :pswitch_6
    iget v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->i:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 327
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 312
    const/4 v0, 0x0

    return-object v0
.end method

.method final b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->b:Ljava/util/Set;

    return-object v0
.end method

.method final c()I
    .locals 1

    .prologue
    .line 198
    iget v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->c:I

    return v0
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 317
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 301
    sget-object v0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->CREATOR:Lcom/google/android/gms/plus/service/v1/d;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/util/List;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->e:Ljava/util/List;

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 439
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v1/PeopleFeed;

    if-nez v0, :cond_0

    move v0, v1

    .line 470
    :goto_0
    return v0

    .line 444
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 445
    goto :goto_0

    .line 448
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v1/PeopleFeed;

    .line 449
    sget-object v0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 450
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 451
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 453
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 455
    goto :goto_0

    :cond_3
    move v0, v1

    .line 460
    goto :goto_0

    .line 463
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 465
    goto :goto_0

    :cond_5
    move v0, v2

    .line 470
    goto :goto_0
.end method

.method final f()Ljava/util/List;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->e:Ljava/util/List;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 426
    const/4 v0, 0x0

    .line 427
    sget-object v1, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 428
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 429
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 430
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 432
    goto :goto_0

    .line 433
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 289
    iget v0, p0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->i:I

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 306
    sget-object v0, Lcom/google/android/gms/plus/service/v1/PeopleFeed;->CREATOR:Lcom/google/android/gms/plus/service/v1/d;

    invoke-static {p0, p1}, Lcom/google/android/gms/plus/service/v1/d;->a(Lcom/google/android/gms/plus/service/v1/PeopleFeed;Landroid/os/Parcel;)V

    .line 307
    return-void
.end method
