.class public final Lcom/google/android/gms/ads/settings/c/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/w;
.implements Lcom/android/volley/x;


# static fields
.field private static final a:Ljava/lang/Object;

.field private static final b:Landroid/content/SharedPreferences;


# instance fields
.field private final c:Landroid/content/BroadcastReceiver;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/android/volley/s;

.field private final f:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/ads/settings/c/f;->a:Ljava/lang/Object;

    .line 36
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    const-string v1, "ads_fdz"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/ContextWrapper;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/settings/c/f;->b:Landroid/content/SharedPreferences;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 43
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/c/f;->f:Ljava/util/Set;

    .line 63
    iput-object p1, p0, Lcom/google/android/gms/ads/settings/c/f;->d:Landroid/content/Context;

    .line 64
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/c/f;->e:Lcom/android/volley/s;

    .line 65
    new-instance v0, Lcom/google/android/gms/ads/settings/c/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/settings/c/g;-><init>(Lcom/google/android/gms/ads/settings/c/f;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/settings/c/f;->c:Landroid/content/BroadcastReceiver;

    .line 71
    return-void
.end method

.method public static a(I)V
    .locals 3

    .prologue
    .line 146
    sget-object v1, Lcom/google/android/gms/ads/settings/c/f;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 147
    :try_start_0
    sget-object v0, Lcom/google/android/gms/ads/settings/c/f;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "fdz"

    invoke-interface {v0, v2, p0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 149
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c/f;->f:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/settings/c/h;

    .line 160
    iget-object v2, p0, Lcom/google/android/gms/ads/settings/c/f;->d:Landroid/content/Context;

    invoke-interface {v0, v2, p1}, Lcom/google/android/gms/ads/settings/c/h;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 162
    :cond_0
    return-void
.end method

.method public static c()I
    .locals 4

    .prologue
    .line 153
    sget-object v1, Lcom/google/android/gms/ads/settings/c/f;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 154
    :try_start_0
    sget-object v0, Lcom/google/android/gms/ads/settings/c/f;->b:Landroid/content/SharedPreferences;

    const-string v2, "fdz"

    const/4 v3, -0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 155
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c/f;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/f;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 74
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 75
    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 76
    const-string v1, "android.intent.action.AIRPLANE_MODE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 77
    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/f;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/settings/c/f;->c:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 78
    return-void
.end method

.method public final a(Lcom/android/volley/ac;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 116
    const-string v0, "AdsFdzState"

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    const-string v0, "AdsFdzState"

    const-string v1, "Failed to retrieve server value of ads fix."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :cond_0
    invoke-direct {p0, v2}, Lcom/google/android/gms/ads/settings/c/f;->b(I)V

    .line 120
    invoke-direct {p0}, Lcom/google/android/gms/ads/settings/c/f;->d()V

    .line 121
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/settings/c/h;)V
    .locals 1

    .prologue
    .line 92
    if-eqz p1, :cond_0

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c/f;->f:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 95
    :cond_0
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 25
    check-cast p1, Ljava/lang/Integer;

    const-string v0, "AdsFdzState"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "AdsFdzState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Successful received response of server value. (response="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/ads/settings/c/f;->d()V

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/settings/c/f;->b(I)V

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/settings/c/f;->b(I)V

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/settings/c/f;->b(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/ads/settings/c/f;->d:Landroid/content/Context;

    const-string v3, "connectivity"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 105
    invoke-direct {p0, v2}, Lcom/google/android/gms/ads/settings/c/f;->b(I)V

    .line 106
    new-instance v0, Lcom/google/android/gms/ads/settings/c/e;

    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/f;->d:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p0}, Lcom/google/android/gms/ads/settings/c/e;-><init>(Landroid/content/Context;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 107
    invoke-virtual {v0, v2}, Lcom/google/android/gms/ads/settings/c/e;->a(Z)Lcom/android/volley/p;

    .line 108
    iget-object v1, p0, Lcom/google/android/gms/ads/settings/c/f;->e:Lcom/android/volley/s;

    invoke-virtual {v1, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 112
    :goto_1
    return-void

    :cond_0
    move v0, v2

    .line 104
    goto :goto_0

    .line 110
    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/gms/ads/settings/c/f;->b(I)V

    goto :goto_1
.end method
