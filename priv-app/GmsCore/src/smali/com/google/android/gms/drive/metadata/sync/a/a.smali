.class public final Lcom/google/android/gms/drive/metadata/sync/a/a;
.super Lcom/google/android/gms/drive/metadata/sync/a/c;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/a/e;->d:Lcom/google/android/gms/drive/metadata/sync/a/e;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/metadata/sync/a/c;-><init>(Lcom/google/android/gms/drive/metadata/sync/a/e;)V

    .line 29
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/a;->a:Ljava/util/Set;

    .line 30
    const-string v0, ","

    invoke-static {p1, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    .line 31
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 32
    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/a/a;->a:Ljava/util/Set;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v4, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 31
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 34
    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/util/Set;)V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/a/e;->d:Lcom/google/android/gms/drive/metadata/sync/a/e;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/metadata/sync/a/c;-><init>(Lcom/google/android/gms/drive/metadata/sync/a/e;)V

    .line 23
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/a/a;->a:Ljava/util/Set;

    .line 25
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 60
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/a/a;->a:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 61
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 62
    const-string v1, ","

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 42
    if-ne p0, p1, :cond_0

    .line 43
    const/4 v0, 0x1

    .line 50
    :goto_0
    return v0

    .line 45
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/drive/metadata/sync/a/a;

    if-nez v0, :cond_1

    .line 46
    const/4 v0, 0x0

    goto :goto_0

    .line 49
    :cond_1
    check-cast p1, Lcom/google/android/gms/drive/metadata/sync/a/a;

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/a;->a:Ljava/util/Set;

    iget-object v1, p1, Lcom/google/android/gms/drive/metadata/sync/a/a;->a:Ljava/util/Set;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/a;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    return v0
.end method
