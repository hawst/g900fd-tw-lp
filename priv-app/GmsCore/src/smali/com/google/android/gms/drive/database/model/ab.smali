.class public final Lcom/google/android/gms/drive/database/model/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/Map;

.field private final b:Lcom/google/android/gms/drive/database/model/at;


# direct methods
.method private constructor <init>(Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 390
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 391
    iput-object p1, p0, Lcom/google/android/gms/drive/database/model/ab;->a:Ljava/util/Map;

    .line 392
    const/16 v0, 0x2b

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/at;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/ab;->b:Lcom/google/android/gms/drive/database/model/at;

    .line 393
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/Map;B)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/database/model/ab;-><init>(Ljava/util/Map;)V

    return-void
.end method

.method public static a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 82
    invoke-interface {p0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    .line 83
    invoke-interface {p0, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    const/4 v0, 0x0

    .line 87
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 44
    if-nez p0, :cond_0

    .line 45
    const-string v0, "NULL"

    .line 49
    :goto_0
    return-object v0

    .line 46
    :cond_0
    instance-of v0, p0, Ljava/lang/Number;

    if-eqz v0, :cond_1

    .line 47
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 49
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x22

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d(I)Lcom/google/android/gms/drive/database/model/at;
    .locals 2

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ab;->a:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/at;

    return-object v0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ab;->b:Lcom/google/android/gms/drive/database/model/at;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 3

    .prologue
    .line 530
    sget-object v0, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/database/model/ab;->a(Lcom/google/android/gms/drive/database/model/aw;)V

    .line 531
    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, p2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Z)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 4

    .prologue
    .line 499
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ab;->a()V

    .line 500
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ab;->d()V

    .line 501
    sget-object v0, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/database/model/ab;->a(Lcom/google/android/gms/drive/database/model/aw;)V

    .line 502
    if-eqz p1, :cond_0

    const-string v0, "!=0"

    .line 503
    :goto_0
    new-instance v1, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v1

    .line 502
    :cond_0
    const-string v0, "=0"

    goto :goto_0
.end method

.method public final a(I)Lcom/google/android/gms/drive/database/model/at;
    .locals 3

    .prologue
    .line 400
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/database/model/ab;->d(I)Lcom/google/android/gms/drive/database/model/at;

    move-result-object v0

    .line 401
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Field not present in version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 402
    return-object v0
.end method

.method public final a(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/model/ab;->f()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 99
    sget-object v0, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/database/model/ab;->a(Lcom/google/android/gms/drive/database/model/aw;)V

    .line 100
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(ILjava/lang/String;Z)Ljava/lang/StringBuilder;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 259
    if-nez p3, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 260
    add-int/lit8 v0, p1, -0x1

    .line 261
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(I)Lcom/google/android/gms/drive/database/model/at;

    move-result-object v3

    .line 262
    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/model/ab;->d(I)Lcom/google/android/gms/drive/database/model/at;

    move-result-object v0

    .line 264
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 265
    if-nez v0, :cond_3

    .line 267
    iget-boolean v0, v3, Lcom/google/android/gms/drive/database/model/at;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, v3, Lcom/google/android/gms/drive/database/model/at;->g:Ljava/lang/Object;

    if-nez v0, :cond_2

    if-nez p3, :cond_2

    .line 269
    new-instance v0, Landroid/database/SQLException;

    const-string v1, "Cannot add not null field without default to existing table"

    invoke-direct {v0, v1}, Landroid/database/SQLException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    move v0, v1

    .line 259
    goto :goto_0

    .line 272
    :cond_2
    iget-object v0, v3, Lcom/google/android/gms/drive/database/model/at;->g:Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/gms/drive/database/model/ab;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 280
    :goto_1
    const-string v0, " AS "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v3, Lcom/google/android/gms/drive/database/model/at;->a:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/drive/database/model/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 281
    return-object v4

    .line 275
    :cond_3
    if-nez p3, :cond_4

    :goto_2
    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 276
    invoke-static {p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 277
    const/16 v1, 0x2e

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 278
    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/at;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/drive/database/model/ab;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_4
    move v2, v1

    .line 275
    goto :goto_2
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 432
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ab;->b:Lcom/google/android/gms/drive/database/model/at;

    const-string v1, "Field not present in current version 43"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 434
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/database/model/aw;)V
    .locals 3

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ab;->b:Lcom/google/android/gms/drive/database/model/at;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/at;->b:Lcom/google/android/gms/drive/database/model/aw;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/database/model/aw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 566
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " field"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 568
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 3

    .prologue
    .line 535
    sget-object v0, Lcom/google/android/gms/drive/database/model/aw;->c:Lcom/google/android/gms/drive/database/model/aw;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/database/model/ab;->a(Lcom/google/android/gms/drive/database/model/aw;)V

    .line 536
    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final b(Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 3

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/google/android/gms/drive/database/model/ab;->f()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ab;->a()V

    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ab;->b:Lcom/google/android/gms/drive/database/model/at;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/at;->b:Lcom/google/android/gms/drive/database/model/aw;

    sget-object v1, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/aw;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 132
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "expected type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 440
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ab;->a()V

    .line 441
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ab;->b:Lcom/google/android/gms/drive/database/model/at;

    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/at;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 425
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/database/model/ab;->d(I)Lcom/google/android/gms/drive/database/model/at;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Landroid/database/Cursor;)J
    .locals 2

    .prologue
    .line 147
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 148
    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 3

    .prologue
    .line 483
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ab;->a()V

    .line 484
    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " IS NULL"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 448
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/database/model/ab;->a(I)Lcom/google/android/gms/drive/database/model/at;

    move-result-object v0

    .line 449
    iget-object v0, v0, Lcom/google/android/gms/drive/database/model/at;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final d(Landroid/database/Cursor;)Ljava/lang/Boolean;
    .locals 4

    .prologue
    .line 162
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    .line 163
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 507
    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ab;->a()V

    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ab;->b:Lcom/google/android/gms/drive/database/model/at;

    iget-boolean v0, v0, Lcom/google/android/gms/drive/database/model/at;->h:Z

    if-nez v0, :cond_0

    .line 508
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is nullable"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 510
    :cond_0
    return-void
.end method

.method public final e()Lcom/google/android/gms/drive/database/SqlWhereClause;
    .locals 4

    .prologue
    .line 525
    sget-object v0, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/database/model/ab;->a(Lcom/google/android/gms/drive/database/model/aw;)V

    .line 526
    new-instance v0, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "!=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x1

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final e(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 173
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/database/model/ab;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final f(Landroid/database/Cursor;)Z
    .locals 1

    .prologue
    .line 187
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/database/model/ab;->d(Landroid/database/Cursor;)Ljava/lang/Boolean;

    move-result-object v0

    .line 188
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 660
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ab;->b:Lcom/google/android/gms/drive/database/model/at;

    if-nez v0, :cond_0

    const-string v0, "[not present]"

    .line 661
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "DatabaseField:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 660
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/ab;->b:Lcom/google/android/gms/drive/database/model/at;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/at;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
