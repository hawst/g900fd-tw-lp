.class public final Lcom/google/android/gms/plus/b/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/plus/service/v1/c;

.field final b:Lcom/google/android/gms/common/server/n;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/plus/service/v1/c;)V
    .locals 0

    .prologue
    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lcom/google/android/gms/plus/b/k;->b:Lcom/google/android/gms/common/server/n;

    .line 58
    iput-object p2, p0, Lcom/google/android/gms/plus/b/k;->a:Lcom/google/android/gms/plus/service/v1/c;

    .line 59
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)[B
    .locals 8

    .prologue
    .line 156
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 157
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/plus/provider/d;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v1

    .line 160
    if-eqz v1, :cond_1

    .line 162
    :try_start_0
    invoke-virtual {v1}, Landroid/database/AbstractWindowedCursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    const-string v0, "updated"

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 165
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getLong(I)J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 169
    const-string v0, "profileJson"

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 171
    const-wide/32 v6, 0x36ee80

    cmp-long v3, v4, v6

    if-gtz v3, :cond_0

    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 176
    new-instance v3, Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    const-class v4, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    invoke-static {v4}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->a(Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FieldMappingDictionary;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;-><init>(Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)V

    .line 178
    new-instance v4, Lcom/google/android/gms/common/server/response/d;

    invoke-direct {v4}, Lcom/google/android/gms/common/server/response/d;-><init>()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    :try_start_1
    invoke-virtual {v4, v0, v3}, Lcom/google/android/gms/common/server/response/d;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 182
    invoke-virtual {v3}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->c()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Parcel;->marshall()[B
    :try_end_1
    .catch Lcom/google/android/gms/common/server/response/m; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 192
    invoke-virtual {v1}, Landroid/database/AbstractWindowedCursor;->close()V

    .line 219
    :goto_0
    return-object v0

    .line 183
    :catch_0
    move-exception v0

    .line 185
    :try_start_2
    const-string v3, "PlusV1Agent"

    const/4 v4, 0x6

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 186
    const-string v3, "PlusV1Agent"

    const-string v4, "Unable to parse the cached profile data"

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 192
    :cond_0
    invoke-virtual {v1}, Landroid/database/AbstractWindowedCursor;->close()V

    .line 197
    :cond_1
    :try_start_3
    const-class v0, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    invoke-static {v0}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->a(Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FieldMappingDictionary;

    move-result-object v0

    .line 199
    iget-object v1, p0, Lcom/google/android/gms/plus/b/k;->a:Lcom/google/android/gms/plus/service/v1/c;

    const-string v3, "me"

    const-class v4, Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    invoke-virtual {v1, p2, v3, v4, v0}, Lcom/google/android/gms/plus/service/v1/c;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/SafeParcelResponse;

    .line 205
    sget-object v1, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;->CREATOR:Lcom/google/android/gms/plus/internal/model/people/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->a(Landroid/os/Parcelable$Creator;)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    .line 208
    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;->toString()Ljava/lang/String;

    move-result-object v1

    .line 210
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    .line 211
    new-instance v4, Landroid/content/ContentValues;

    const/4 v5, 0x3

    invoke-direct {v4, v5}, Landroid/content/ContentValues;-><init>(I)V

    .line 212
    const-string v5, "accountName"

    invoke-virtual {v4, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 213
    const-string v5, "packageName"

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    const-string v5, "profileJson"

    invoke-virtual {v4, v5, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v5

    const-string v1, "com.google.android.gms.plus"

    invoke-virtual {v2, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;
    :try_end_3
    .catch Lcom/android/volley/ac; {:try_start_3 .. :try_end_3} :catch_1

    move-result-object v6

    :try_start_4
    invoke-virtual {v6}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/provider/PlusProvider;

    invoke-virtual {v1, v3, v5, v4}, Lcom/google/android/gms/plus/provider/PlusProvider;->a(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    invoke-virtual {v6}, Landroid/content/ContentProviderClient;->release()Z

    .line 219
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/SafeParcelResponse;->c()Landroid/os/Parcel;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Parcel;->marshall()[B
    :try_end_5
    .catch Lcom/android/volley/ac; {:try_start_5 .. :try_end_5} :catch_1

    move-result-object v0

    goto :goto_0

    .line 192
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/AbstractWindowedCursor;->close()V

    throw v0

    .line 216
    :catchall_1
    move-exception v0

    :try_start_6
    invoke-virtual {v6}, Landroid/content/ContentProviderClient;->release()Z

    throw v0
    :try_end_6
    .catch Lcom/android/volley/ac; {:try_start_6 .. :try_end_6} :catch_1

    .line 220
    :catch_1
    move-exception v0

    .line 222
    iget-object v1, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v1, v1, Lcom/android/volley/m;->a:I

    const/16 v3, 0x190

    if-lt v1, v3, :cond_2

    .line 225
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/plus/provider/d;->b(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    :cond_2
    throw v0
.end method
