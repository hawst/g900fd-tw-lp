.class public Lcom/google/android/gms/security/verifier/ApkUploadService;
.super Landroid/app/Service;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4

    .prologue
    .line 23
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms.security.verifyapps.UPLOAD_APK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 25
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/google/android/gms/security/verifier/InternalApkUploadService;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 26
    invoke-virtual {p0, p1}, Lcom/google/android/gms/security/verifier/ApkUploadService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 30
    :goto_0
    invoke-virtual {p0, p3}, Lcom/google/android/gms/security/verifier/ApkUploadService;->stopSelf(I)V

    .line 31
    const/4 v0, 0x3

    return v0

    .line 28
    :cond_0
    const-string v0, "Ignoring unrecognized intent: %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/security/verifier/ah;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
