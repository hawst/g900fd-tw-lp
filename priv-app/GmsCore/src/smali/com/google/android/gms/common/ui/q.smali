.class final Lcom/google/android/gms/common/ui/q;
.super Landroid/app/AlertDialog;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnKeyListener;


# instance fields
.field private a:Lcom/google/android/gms/common/util/z;

.field private b:Landroid/webkit/WebView;

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v1, -0x1

    .line 50
    invoke-direct {p0, p1}, Landroid/app/AlertDialog;-><init>(Landroid/content/Context;)V

    .line 46
    iput-boolean v6, p0, Lcom/google/android/gms/common/ui/q;->c:Z

    .line 51
    new-instance v2, Lcom/google/android/gms/common/util/z;

    invoke-direct {v2, p1}, Lcom/google/android/gms/common/util/z;-><init>(Landroid/content/Context;)V

    iget-object v0, v2, Lcom/google/android/gms/common/util/z;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/gms/common/util/z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/common/util/z;->b:Ljava/lang/String;

    iget-object v0, v2, Lcom/google/android/gms/common/util/z;->a:Landroid/content/Context;

    invoke-static {p3}, Lcom/google/android/gms/common/util/z;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/common/util/z;->c:Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/common/ui/p;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    sget v0, Lcom/google/android/gms/p;->fl:I

    :goto_0
    invoke-virtual {p1, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v3, v2, Lcom/google/android/gms/common/util/z;->a:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    iget-object v3, v3, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v2, Lcom/google/android/gms/common/util/z;->c:Ljava/lang/String;

    aput-object v5, v4, v6

    invoke-static {v3, v0, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/common/util/z;->d:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/common/util/z;->b()V

    iget-object v0, v2, Lcom/google/android/gms/common/util/z;->e:Landroid/webkit/WebView;

    iget-object v3, v2, Lcom/google/android/gms/common/util/z;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/gms/common/ui/q;->a:Lcom/google/android/gms/common/util/z;

    .line 57
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 58
    invoke-virtual {p0, p4}, Lcom/google/android/gms/common/ui/q;->setTitle(Ljava/lang/CharSequence;)V

    .line 60
    :cond_0
    sget v0, Lcom/google/android/gms/p;->tk:I

    invoke-virtual {p1, v0}, Landroid/app/Activity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/common/ui/r;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/ui/r;-><init>(Lcom/google/android/gms/common/ui/q;)V

    invoke-virtual {p0, v1, v0, v2}, Lcom/google/android/gms/common/ui/q;->setButton(ILjava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)V

    .line 69
    new-instance v2, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/q;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v2, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 70
    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    .line 71
    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v3

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 73
    iget v3, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-lez v3, :cond_2

    .line 75
    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-double v4, v0

    const-wide v6, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v4, v6

    const-wide/high16 v6, 0x3fe0000000000000L    # 0.5

    add-double/2addr v4, v6

    double-to-int v0, v4

    .line 77
    :goto_1
    new-instance v3, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v3, v1, v0}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/common/ui/q;->a:Lcom/google/android/gms/common/util/z;

    invoke-virtual {v0}, Lcom/google/android/gms/common/util/z;->a()Landroid/view/ViewGroup;

    move-result-object v0

    invoke-virtual {v2, v0, v3}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 81
    invoke-direct {p0}, Lcom/google/android/gms/common/ui/q;->a()Landroid/webkit/WebView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/ui/q;->b:Landroid/webkit/WebView;

    .line 83
    invoke-virtual {p0, p0}, Lcom/google/android/gms/common/ui/q;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 84
    invoke-virtual {p0, v2}, Lcom/google/android/gms/common/ui/q;->setView(Landroid/view/View;)V

    .line 85
    return-void

    .line 51
    :cond_1
    sget v0, Lcom/google/android/gms/p;->fk:I

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private a()Landroid/webkit/WebView;
    .locals 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/gms/common/ui/q;->a:Lcom/google/android/gms/common/util/z;

    invoke-virtual {v0}, Lcom/google/android/gms/common/util/z;->a()Landroid/view/ViewGroup;

    move-result-object v1

    .line 135
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 136
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    instance-of v2, v2, Landroid/webkit/WebView;

    if-eqz v2, :cond_0

    .line 137
    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    .line 140
    :goto_1
    return-object v0

    .line 135
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 140
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/common/ui/q;->b:Landroid/webkit/WebView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/ui/q;->b:Landroid/webkit/WebView;

    invoke-static {v0, p1}, Landroid/support/v4/view/ay;->b(Landroid/view/View;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/common/ui/q;->b:Landroid/webkit/WebView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p1}, Landroid/webkit/WebView;->scrollBy(II)V

    .line 131
    :cond_0
    return-void
.end method


# virtual methods
.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 6

    .prologue
    const/16 v5, 0x14

    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 89
    iget-object v2, p0, Lcom/google/android/gms/common/ui/q;->a:Lcom/google/android/gms/common/util/z;

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    const/4 v4, 0x4

    if-ne v3, v4, :cond_0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, v2, Lcom/google/android/gms/common/util/z;->e:Landroid/webkit/WebView;

    invoke-virtual {v3}, Landroid/webkit/WebView;->canGoBack()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v2, v2, Lcom/google/android/gms/common/util/z;->e:Landroid/webkit/WebView;

    invoke-virtual {v2}, Landroid/webkit/WebView;->goBack()V

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    .line 102
    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 89
    goto :goto_0

    .line 94
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    const/16 v3, 0x13

    if-ne v2, v3, :cond_2

    .line 95
    const/16 v1, -0x14

    invoke-direct {p0, v1}, Lcom/google/android/gms/common/ui/q;->a(I)V

    goto :goto_1

    .line 97
    :cond_2
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    if-ne v2, v5, :cond_3

    .line 98
    invoke-direct {p0, v5}, Lcom/google/android/gms/common/ui/q;->a(I)V

    goto :goto_1

    :cond_3
    move v0, v1

    .line 102
    goto :goto_1
.end method

.method public final onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 108
    invoke-super {p0, p1}, Landroid/app/AlertDialog;->onWindowAttributesChanged(Landroid/view/WindowManager$LayoutParams;)V

    .line 109
    iget-boolean v0, p0, Lcom/google/android/gms/common/ui/q;->c:Z

    if-eqz v0, :cond_0

    .line 119
    :goto_0
    return-void

    .line 112
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/ui/q;->c:Z

    .line 113
    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->width:I

    if-ne v0, v1, :cond_1

    iget v0, p1, Landroid/view/WindowManager$LayoutParams;->height:I

    if-eq v0, v1, :cond_2

    .line 115
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/q;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setLayout(II)V

    .line 118
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/ui/q;->c:Z

    goto :goto_0
.end method
