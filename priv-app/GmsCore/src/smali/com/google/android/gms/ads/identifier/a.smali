.class public final Lcom/google/android/gms/ads/identifier/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/common/b;

.field b:Lcom/google/android/gms/ads/identifier/a/a;

.field c:Z

.field d:Ljava/lang/Object;

.field e:Lcom/google/android/gms/ads/identifier/b;

.field final f:J

.field private final g:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 155
    const-wide/16 v0, 0x7530

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/ads/identifier/a;-><init>(Landroid/content/Context;J)V

    .line 156
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;J)V
    .locals 2

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->d:Ljava/lang/Object;

    .line 161
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    iput-object p1, p0, Lcom/google/android/gms/ads/identifier/a;->g:Landroid/content/Context;

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/ads/identifier/a;->c:Z

    .line 164
    iput-wide p2, p0, Lcom/google/android/gms/ads/identifier/a;->f:J

    .line 165
    return-void
.end method

.method private static a(Lcom/google/android/gms/common/b;)Lcom/google/android/gms/ads/identifier/a/a;
    .locals 2

    .prologue
    .line 378
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/identifier/a/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/ads/identifier/a/a;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 380
    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/IOException;

    const-string v1, "Interrupted exception"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/c;
    .locals 4

    .prologue
    .line 358
    new-instance v1, Lcom/google/android/gms/ads/identifier/a;

    const-wide/16 v2, -0x1

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/gms/ads/identifier/a;-><init>(Landroid/content/Context;J)V

    .line 361
    const/4 v0, 0x0

    :try_start_0
    invoke-direct {v1, v0}, Lcom/google/android/gms/ads/identifier/a;->a(Z)V

    .line 362
    invoke-virtual {v1}, Lcom/google/android/gms/ads/identifier/a;->b()Lcom/google/android/gms/ads/identifier/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 364
    invoke-virtual {v1}, Lcom/google/android/gms/ads/identifier/a;->c()V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/ads/identifier/a;->c()V

    throw v0
.end method

.method private a(Z)V
    .locals 1

    .prologue
    .line 182
    const-string v0, "Calling this from your main thread can lead to deadlock"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    .line 184
    monitor-enter p0

    .line 185
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/identifier/a;->c:Z

    if-eqz v0, :cond_0

    .line 186
    invoke-virtual {p0}, Lcom/google/android/gms/ads/identifier/a;->c()V

    .line 188
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/ads/identifier/a;->b(Landroid/content/Context;)Lcom/google/android/gms/common/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->a:Lcom/google/android/gms/common/b;

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->g:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->a:Lcom/google/android/gms/common/b;

    invoke-static {v0}, Lcom/google/android/gms/ads/identifier/a;->a(Lcom/google/android/gms/common/b;)Lcom/google/android/gms/ads/identifier/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->b:Lcom/google/android/gms/ads/identifier/a/a;

    .line 190
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/identifier/a;->c:Z

    .line 191
    if-eqz p1, :cond_1

    .line 192
    invoke-direct {p0}, Lcom/google/android/gms/ads/identifier/a;->d()V

    .line 194
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static b(Landroid/content/Context;)Lcom/google/android/gms/common/b;
    .locals 4

    .prologue
    .line 313
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 314
    const-string v1, "com.android.vending"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 321
    :try_start_1
    invoke-static {p0}, Lcom/google/android/gms/common/ew;->b(Landroid/content/Context;)V
    :try_end_1
    .catch Lcom/google/android/gms/common/eu; {:try_start_1 .. :try_end_1} :catch_1

    .line 328
    new-instance v0, Lcom/google/android/gms/common/b;

    invoke-direct {v0}, Lcom/google/android/gms/common/b;-><init>()V

    .line 329
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.ads.identifier.service.START"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 330
    const-string v2, "com.google.android.gms"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 331
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, p0, v1, v0, v3}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 333
    return-object v0

    .line 317
    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/common/eu;

    const/16 v1, 0x9

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/eu;-><init>(I)V

    throw v0

    .line 322
    :catch_1
    move-exception v0

    .line 325
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 336
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Connection failure"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private d()V
    .locals 6

    .prologue
    .line 209
    iget-object v1, p0, Lcom/google/android/gms/ads/identifier/a;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 210
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->e:Lcom/google/android/gms/ads/identifier/b;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->e:Lcom/google/android/gms/ads/identifier/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/b;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 213
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->e:Lcom/google/android/gms/ads/identifier/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/b;->join()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 218
    :cond_0
    :goto_0
    :try_start_2
    iget-wide v2, p0, Lcom/google/android/gms/ads/identifier/a;->f:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 219
    new-instance v0, Lcom/google/android/gms/ads/identifier/b;

    iget-wide v2, p0, Lcom/google/android/gms/ads/identifier/a;->f:J

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/gms/ads/identifier/b;-><init>(Lcom/google/android/gms/ads/identifier/a;J)V

    iput-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->e:Lcom/google/android/gms/ads/identifier/b;

    .line 221
    :cond_1
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 174
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/identifier/a;->a(Z)V

    .line 175
    return-void
.end method

.method public final b()Lcom/google/android/gms/ads/identifier/c;
    .locals 4

    .prologue
    .line 229
    const-string v0, "Calling this from your main thread can lead to deadlock"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    .line 231
    monitor-enter p0

    .line 233
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/identifier/a;->c:Z

    if-nez v0, :cond_2

    .line 234
    iget-object v1, p0, Lcom/google/android/gms/ads/identifier/a;->d:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 235
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->e:Lcom/google/android/gms/ads/identifier/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->e:Lcom/google/android/gms/ads/identifier/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/b;->b()Z

    move-result v0

    if-nez v0, :cond_1

    .line 236
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v2, "AdvertisingIdClient is not connected."

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 238
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 258
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 238
    :cond_1
    :try_start_3
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 240
    const/4 v0, 0x0

    :try_start_4
    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/identifier/a;->a(Z)V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 244
    :try_start_5
    iget-boolean v0, p0, Lcom/google/android/gms/ads/identifier/a;->c:Z

    if-nez v0, :cond_2

    .line 245
    new-instance v0, Ljava/io/IOException;

    const-string v1, "AdvertisingIdClient cannot reconnect."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 241
    :catch_0
    move-exception v0

    .line 242
    new-instance v1, Ljava/io/IOException;

    const-string v2, "AdvertisingIdClient cannot reconnect."

    invoke-direct {v1, v2, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 248
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->a:Lcom/google/android/gms/common/b;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->b:Lcom/google/android/gms/ads/identifier/a/a;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 251
    :try_start_6
    new-instance v0, Lcom/google/android/gms/ads/identifier/c;

    iget-object v1, p0, Lcom/google/android/gms/ads/identifier/a;->b:Lcom/google/android/gms/ads/identifier/a/a;

    invoke-interface {v1}, Lcom/google/android/gms/ads/identifier/a/a;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/ads/identifier/a;->b:Lcom/google/android/gms/ads/identifier/a/a;

    const/4 v3, 0x1

    invoke-interface {v2, v3}, Lcom/google/android/gms/ads/identifier/a/a;->a(Z)Z

    move-result v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/ads/identifier/c;-><init>(Ljava/lang/String;Z)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 258
    :try_start_7
    monitor-exit p0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 259
    invoke-direct {p0}, Lcom/google/android/gms/ads/identifier/a;->d()V

    .line 260
    return-object v0

    .line 254
    :catch_1
    move-exception v0

    .line 255
    :try_start_8
    const-string v1, "AdvertisingIdClient"

    const-string v2, "GMS remote exception "

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 256
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Remote exception"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 268
    const-string v0, "Calling this from your main thread can lead to deadlock"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    .line 270
    monitor-enter p0

    .line 271
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->g:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->a:Lcom/google/android/gms/common/b;

    if-nez v0, :cond_1

    .line 272
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 287
    :goto_0
    return-void

    .line 275
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/gms/ads/identifier/a;->c:Z

    if-eqz v0, :cond_2

    .line 276
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/ads/identifier/a;->g:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/ads/identifier/a;->a:Lcom/google/android/gms/common/b;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 284
    :cond_2
    :goto_1
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/gms/ads/identifier/a;->c:Z

    .line 285
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->b:Lcom/google/android/gms/ads/identifier/a/a;

    .line 286
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/identifier/a;->a:Lcom/google/android/gms/common/b;

    .line 287
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 278
    :catch_0
    move-exception v0

    .line 282
    :try_start_3
    const-string v1, "AdvertisingIdClient"

    const-string v2, "AdvertisingIdClient unbindService failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method protected final finalize()V
    .locals 0

    .prologue
    .line 296
    invoke-virtual {p0}, Lcom/google/android/gms/ads/identifier/a;->c()V

    .line 297
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 298
    return-void
.end method
