.class public final Lcom/google/android/gms/people/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/concurrent/ConcurrentHashMap;

.field final b:Ljava/util/concurrent/ConcurrentHashMap;

.field private final c:Lcom/google/android/gms/people/c/f;

.field private final d:Lcom/google/android/gms/common/util/an;

.field private final e:Ljava/util/concurrent/ConcurrentHashMap;

.field private final f:Ljava/util/concurrent/ConcurrentHashMap;


# direct methods
.method constructor <init>(Lcom/google/android/gms/people/c/f;)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Lcom/google/android/gms/people/c/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/c/c;-><init>(Lcom/google/android/gms/people/c/b;)V

    iput-object v0, p0, Lcom/google/android/gms/people/c/b;->d:Lcom/google/android/gms/common/util/an;

    .line 68
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/c/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 71
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/c/b;->e:Ljava/util/concurrent/ConcurrentHashMap;

    .line 77
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/c/b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    .line 80
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/c/b;->f:Ljava/util/concurrent/ConcurrentHashMap;

    .line 84
    iput-object p1, p0, Lcom/google/android/gms/people/c/b;->c:Lcom/google/android/gms/people/c/f;

    .line 85
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/people/c/d;
    .locals 2

    .prologue
    .line 113
    if-eqz p3, :cond_0

    .line 114
    new-instance v0, Lcom/google/android/gms/people/c/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/c/d;-><init>(B)V

    .line 118
    :goto_0
    iput-object p1, v0, Lcom/google/android/gms/people/c/d;->a:Ljava/lang/String;

    .line 119
    iput-object p2, v0, Lcom/google/android/gms/people/c/d;->b:Ljava/lang/String;

    .line 120
    return-object v0

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/b;->d:Lcom/google/android/gms/common/util/an;

    invoke-virtual {v0}, Lcom/google/android/gms/common/util/an;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/c/d;

    goto :goto_0
.end method

.method private c(Ljava/lang/String;Ljava/lang/String;)J
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/people/c/b;->c:Lcom/google/android/gms/people/c/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    .line 155
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 156
    const-string v1, "SELECT _id FROM owners WHERE (account_name = ?1) AND (page_gaia_id IS NULL)"

    invoke-static {p1}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v2, v0

    .line 169
    :goto_0
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_1

    .line 170
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    const-wide/16 v0, -0x1

    .line 174
    :goto_1
    return-wide v0

    .line 162
    :cond_0
    const-string v1, "SELECT _id FROM owners WHERE (account_name = ?1) AND (page_gaia_id = ?2)"

    invoke-static {p1, p2}, Lcom/google/android/gms/people/internal/at;->c(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 172
    :cond_1
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 174
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/gms/people/c/b;->c:Lcom/google/android/gms/people/c/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v0

    .line 216
    const-string v1, "SELECT gaia_id FROM owners WHERE (account_name = ?1) AND (page_gaia_id IS NULL)"

    invoke-static {p1}, Lcom/google/android/gms/people/internal/at;->n(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 222
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 223
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    const/4 v0, 0x0

    .line 227
    :goto_0
    return-object v0

    .line 225
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 227
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)J
    .locals 6

    .prologue
    const-wide/16 v2, -0x1

    .line 128
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-wide v0, v2

    .line 148
    :goto_0
    return-wide v0

    .line 133
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/people/c/d;

    move-result-object v4

    .line 135
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v4}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 136
    if-eqz v0, :cond_2

    .line 137
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 146
    :cond_1
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/people/c/b;->d:Lcom/google/android/gms/common/util/an;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/common/util/an;->a(Ljava/lang/Object;)Z

    goto :goto_0

    .line 139
    :cond_2
    :try_start_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/people/c/b;->c(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    .line 140
    cmp-long v2, v0, v2

    if-eqz v2, :cond_1

    .line 142
    iget-object v2, p0, Lcom/google/android/gms/people/c/b;->a:Ljava/util/concurrent/ConcurrentHashMap;

    const/4 v3, 0x1

    invoke-direct {p0, p1, p2, v3}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/people/c/d;

    move-result-object v3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 146
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/c/b;->d:Lcom/google/android/gms/common/util/an;

    invoke-virtual {v1, v4}, Lcom/google/android/gms/common/util/an;->a(Ljava/lang/Object;)Z

    throw v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/gms/people/c/b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 200
    if-eqz v0, :cond_1

    .line 209
    :cond_0
    :goto_0
    return-object v0

    .line 203
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/c/b;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 204
    if-eqz v0, :cond_0

    .line 205
    iget-object v1, p0, Lcom/google/android/gms/people/c/b;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)Landroid/accounts/Account;
    .locals 2

    .prologue
    .line 237
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/people/c/b;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 239
    if-nez v0, :cond_0

    .line 240
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p1, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iget-object v1, p0, Lcom/google/android/gms/people/c/b;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 243
    :cond_0
    return-object v0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 182
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    .line 183
    const-wide/16 v0, -0x1

    cmp-long v0, v2, v0

    if-nez v0, :cond_1

    .line 184
    const-string v0, ""

    .line 192
    :cond_0
    :goto_0
    return-object v0

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/c/b;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 187
    if-nez v0, :cond_0

    .line 190
    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    .line 191
    iget-object v1, p0, Lcom/google/android/gms/people/c/b;->e:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
