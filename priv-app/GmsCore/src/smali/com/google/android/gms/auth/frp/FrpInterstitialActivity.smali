.class public Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;
.super Lcom/google/android/gms/auth/login/g;
.source "SourceFile"

# interfaces
.implements Lcom/android/setupwizard/navigationbar/a;


# instance fields
.field private q:Ljava/lang/String;

.field private r:Landroid/os/AsyncTask;

.field private s:Lcom/google/android/setupwizard/util/b;

.field private t:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/auth/login/g;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 33
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "account_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->q:Ljava/lang/String;

    return-object v0
.end method

.method private a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 51
    const-string v0, "account_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->q:Ljava/lang/String;

    .line 52
    return-void
.end method

.method private l()V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->s:Lcom/google/android/setupwizard/util/b;

    sget v1, Lcom/google/android/gms/p;->bv:I

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/b;->a(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->t:Landroid/view/View;

    .line 113
    return-void
.end method

.method private declared-synchronized m()V
    .locals 2

    .prologue
    .line 134
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->r:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->r:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->r:Landroid/os/AsyncTask;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    :cond_0
    monitor-exit p0

    return-void

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->m()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "errorCode"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->finish()V

    .line 149
    return-void
.end method

.method public final a(Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;)V
    .locals 2

    .prologue
    .line 142
    iget-boolean v0, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->o:Z

    iget-boolean v1, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->o:Z

    invoke-virtual {p1, v0, v1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->a(ZZ)V

    .line 143
    invoke-virtual {p1}, Lcom/android/setupwizard/navigationbar/SetupWizardNavBar;->b()Landroid/widget/Button;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 144
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 154
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2

    .prologue
    .line 103
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->s:Lcom/google/android/setupwizard/util/b;

    if-eqz v0, :cond_0

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->s:Lcom/google/android/setupwizard/util/b;

    iget-object v1, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->t:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/google/android/setupwizard/util/b;->removeView(Landroid/view/View;)V

    .line 106
    invoke-direct {p0}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->l()V

    .line 108
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onCreate(Landroid/os/Bundle;)V

    .line 64
    if-nez p1, :cond_1

    .line 65
    invoke-virtual {p0}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 66
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->a(Landroid/os/Bundle;)V

    .line 70
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->f()Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lcom/google/android/setupwizard/util/b;

    invoke-direct {v0, p0}, Lcom/google/android/setupwizard/util/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->s:Lcom/google/android/setupwizard/util/b;

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->s:Lcom/google/android/setupwizard/util/b;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->setContentView(Landroid/view/View;)V

    .line 73
    new-instance v0, Lcom/google/android/gms/auth/frp/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/frp/e;-><init>(Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/frp/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->r:Landroid/os/AsyncTask;

    .line 96
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->l()V

    .line 97
    return-void

    .line 68
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->a(Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->m()V

    .line 118
    invoke-super {p0}, Lcom/google/android/gms/auth/login/g;->onDestroy()V

    .line 119
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 56
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/login/g;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 57
    const-string v0, "account_id"

    iget-object v1, p0, Lcom/google/android/gms/auth/frp/FrpInterstitialActivity;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    return-void
.end method
