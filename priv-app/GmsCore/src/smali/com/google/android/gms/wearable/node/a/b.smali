.class public final Lcom/google/android/gms/wearable/node/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wearable/d/c;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x12
.end annotation


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Landroid/app/NotificationManager;

.field private c:Landroid/support/v4/app/bk;

.field private d:I

.field private e:Ljava/lang/CharSequence;

.field private f:J

.field private final g:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput-object p1, p0, Lcom/google/android/gms/wearable/node/a/b;->a:Landroid/content/Context;

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->a:Landroid/content/Context;

    const-string v1, "notification"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->b:Landroid/app/NotificationManager;

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->a:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.wearable.STATUS"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v4, v1, v4}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/support/v4/app/bk;

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/a/b;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/a/b;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->Di:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v1

    iput-object v0, v1, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    sget v0, Lcom/google/android/gms/h;->cc:I

    invoke-virtual {v1, v0}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1, v5}, Landroid/support/v4/app/bk;->a(IZ)V

    const/4 v1, -0x2

    iput v1, v0, Landroid/support/v4/app/bk;->j:I

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->c:Landroid/support/v4/app/bk;

    .line 54
    iput v5, p0, Lcom/google/android/gms/wearable/node/a/b;->d:I

    .line 55
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 56
    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 85
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "retry_notif_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(ILjava/lang/CharSequence;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_1

    .line 127
    :cond_0
    :goto_0
    return-void

    .line 97
    :cond_1
    if-eqz p3, :cond_3

    .line 98
    const-string v0, "WearableConn"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p3}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :goto_1
    sget-object v0, Lcom/google/android/gms/wearable/a/b;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    .line 105
    const/4 v0, 0x1

    if-ne p1, v0, :cond_4

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->Dl:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    .line 117
    :cond_2
    :goto_2
    iput p1, p0, Lcom/google/android/gms/wearable/node/a/b;->d:I

    .line 118
    iput-object p2, p0, Lcom/google/android/gms/wearable/node/a/b;->e:Ljava/lang/CharSequence;

    .line 119
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/wearable/node/a/b;->f:J

    .line 121
    sget-object v0, Lcom/google/android/gms/wearable/a/b;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->c:Landroid/support/v4/app/bk;

    invoke-virtual {v0, p2}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->b:Landroid/app/NotificationManager;

    const/16 v1, 0x580f

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/a/b;->c:Landroid/support/v4/app/bk;

    invoke-virtual {v2}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    goto :goto_0

    .line 100
    :cond_3
    const-string v0, "WearableConn"

    invoke-interface {p2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 107
    :cond_4
    const/4 v0, 0x2

    if-ne p1, v0, :cond_5

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->Dk:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    goto :goto_2

    .line 109
    :cond_5
    const/4 v0, 0x3

    if-ne p1, v0, :cond_6

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->Dj:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    goto :goto_2

    .line 111
    :cond_6
    const/4 v0, 0x4

    if-ne p1, v0, :cond_7

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->a:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->Dn:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object p2

    goto :goto_2

    .line 114
    :cond_7
    const-string p2, ""

    goto :goto_2
.end method

.method public final a(Landroid/app/Service;)V
    .locals 4

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 136
    sget-object v0, Lcom/google/android/gms/wearable/a/b;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 137
    const/16 v0, 0x580f

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/a/b;->c:Landroid/support/v4/app/bk;

    iget-object v2, p0, Lcom/google/android/gms/wearable/node/a/b;->a:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->Dm:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/app/Service;->startForeground(ILandroid/app/Notification;)V

    .line 141
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/util/ad;ZZ)V
    .locals 4

    .prologue
    .line 165
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Status: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/wearable/node/a/b;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wearable/node/a/b;->e:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/wearable/node/a/b;->f:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 167
    invoke-virtual {p1}, Lcom/google/android/gms/common/util/ad;->b()V

    .line 168
    return-void
.end method

.method public final b(Landroid/app/Service;)V
    .locals 2

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 145
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/app/Service;->stopForeground(Z)V

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/wearable/node/a/b;->b:Landroid/app/NotificationManager;

    const/16 v1, 0x580f

    invoke-virtual {v0, v1}, Landroid/app/NotificationManager;->cancel(I)V

    .line 147
    return-void
.end method
