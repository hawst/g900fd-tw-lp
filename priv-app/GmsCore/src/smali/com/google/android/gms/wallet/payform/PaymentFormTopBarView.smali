.class public Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

.field b:Landroid/widget/TextView;

.field private c:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->c:I

    .line 42
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/content/Context;)V

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 46
    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->c:I

    .line 48
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/content/Context;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->c:I

    .line 54
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a(Landroid/content/Context;)V

    .line 55
    return-void
.end method

.method private a(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 143
    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->setOrientation(I)V

    .line 145
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 146
    sget v1, Lcom/google/android/gms/l;->hA:I

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 148
    sget v0, Lcom/google/android/gms/j;->nI:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    .line 151
    sget v0, Lcom/google/android/gms/j;->nJ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b:Landroid/widget/TextView;

    .line 153
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 130
    iget v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->c:I

    return v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    .line 86
    return-void
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 75
    if-eqz p1, :cond_0

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/accounts/Account;)V

    .line 78
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/cq;)V
    .locals 1

    .prologue
    .line 63
    if-eqz p1, :cond_0

    .line 64
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->b(I)V

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Lcom/google/android/gms/wallet/common/ui/cq;)V

    .line 67
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->setEnabled(Z)V

    .line 96
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->setVisibility(I)V

    .line 135
    return-void
.end method

.method public final b(I)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 99
    iput p1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->c:I

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a()Landroid/accounts/Account;

    move-result-object v0

    .line 101
    iget v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->c:I

    packed-switch v1, :pswitch_data_0

    .line 110
    invoke-static {}, Lcom/google/android/gms/wallet/a/a;->a()Lcom/google/android/gms/wallet/a/a;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/a/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/w;->a([Landroid/accounts/Account;)[Lcom/google/android/gms/wallet/common/ui/a;

    move-result-object v1

    .line 114
    array-length v2, v1

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/wallet/common/ui/a;

    .line 116
    array-length v3, v1

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 118
    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    new-instance v3, Lcom/google/android/gms/wallet/common/ui/a;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/wallet/payform/PaymentFormActivity;->a(Landroid/content/Context;)Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->getContext()Landroid/content/Context;

    move-result-object v5

    sget v6, Lcom/google/android/gms/p;->BD:I

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/wallet/common/ui/a;-><init>(Landroid/accounts/Account;Ljava/lang/String;)V

    aput-object v3, v2, v1

    .line 122
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a([Lcom/google/android/gms/wallet/common/ui/a;)V

    .line 123
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a(Landroid/accounts/Account;)V

    .line 124
    :goto_0
    return-void

    .line 103
    :pswitch_0
    const/4 v1, 0x1

    new-array v1, v1, [Landroid/accounts/Account;

    aput-object v0, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/w;->a([Landroid/accounts/Account;)[Lcom/google/android/gms/wallet/common/ui/a;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lcom/google/android/gms/wallet/payform/PaymentFormTopBarView;->a:Lcom/google/android/gms/wallet/common/ui/AccountSelector;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/AccountSelector;->a([Lcom/google/android/gms/wallet/common/ui/a;)V

    goto :goto_0

    .line 101
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
