.class public final Lcom/google/android/gms/photos/autobackup/service/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final b:Landroid/os/Handler;

.field private static final c:Landroid/content/ComponentName;


# instance fields
.field final a:Lcom/google/android/gms/photos/autobackup/service/a/h;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/photos/autobackup/service/a/f;

.field private final f:Ljava/util/Queue;

.field private g:Lcom/google/android/apps/a/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 73
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    sput-object v0, Lcom/google/android/gms/photos/autobackup/service/a/b;->b:Landroid/os/Handler;

    .line 80
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.apps.plus"

    const-string v2, "com.google.android.apps.photos.service.PhotosService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/photos/autobackup/service/a/b;->c:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/photos/autobackup/service/a/f;)V
    .locals 2

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v0, Lcom/google/android/gms/photos/autobackup/service/a/c;

    sget-object v1, Lcom/google/android/gms/photos/autobackup/service/a/b;->c:Landroid/content/ComponentName;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/photos/autobackup/service/a/c;-><init>(Lcom/google/android/gms/photos/autobackup/service/a/b;Landroid/content/ComponentName;)V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->a:Lcom/google/android/gms/photos/autobackup/service/a/h;

    .line 141
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->d:Landroid/content/Context;

    .line 142
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->e:Lcom/google/android/gms/photos/autobackup/service/a/f;

    .line 143
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->f:Ljava/util/Queue;

    .line 144
    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->f()Z

    .line 145
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/photos/autobackup/service/a/b;Lcom/google/android/apps/a/a/a;)Lcom/google/android/apps/a/a/a;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->g:Lcom/google/android/apps/a/a/a;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/photos/autobackup/service/a/b;)Lcom/google/android/gms/photos/autobackup/service/a/f;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->e:Lcom/google/android/gms/photos/autobackup/service/a/f;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 132
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    sget-object v2, Lcom/google/android/gms/photos/autobackup/service/a/b;->c:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v1

    .line 133
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 134
    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/photos/autobackup/service/a/b;)V
    .locals 1

    .prologue
    .line 27
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/service/a/g;

    invoke-interface {v0}, Lcom/google/android/gms/photos/autobackup/service/a/g;->a()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/photos/autobackup/service/a/b;)V
    .locals 1

    .prologue
    .line 27
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/photos/autobackup/service/a/g;

    invoke-interface {v0}, Lcom/google/android/gms/photos/autobackup/service/a/g;->b()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/photos/autobackup/service/a/b;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->d:Landroid/content/Context;

    return-object v0
.end method

.method private f()Z
    .locals 3

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->a:Lcom/google/android/gms/photos/autobackup/service/a/h;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/h;->d()Z

    move-result v0

    .line 283
    if-nez v0, :cond_0

    .line 284
    sget-object v1, Lcom/google/android/gms/photos/autobackup/service/a/b;->b:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/photos/autobackup/service/a/d;

    invoke-direct {v2, p0}, Lcom/google/android/gms/photos/autobackup/service/a/d;-><init>(Lcom/google/android/gms/photos/autobackup/service/a/b;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 293
    :cond_0
    return v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/photos/autobackup/service/a/g;)V
    .locals 1

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->a:Lcom/google/android/gms/photos/autobackup/service/a/h;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/h;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    invoke-interface {p1}, Lcom/google/android/gms/photos/autobackup/service/a/g;->a()V

    .line 158
    :goto_0
    return-void

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->f:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 156
    invoke-direct {p0}, Lcom/google/android/gms/photos/autobackup/service/a/b;->f()Z

    goto :goto_0
.end method

.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 164
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->a:Lcom/google/android/gms/photos/autobackup/service/a/h;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/service/a/h;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 165
    const-string v1, "PhotosClient"

    const-string v2, "PhotosService is not connected, returning default, disabled."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 175
    :goto_0
    return v0

    .line 170
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->g:Lcom/google/android/apps/a/a/a;

    invoke-interface {v1}, Lcom/google/android/apps/a/a/a;->a()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    goto :goto_0

    .line 175
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 182
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->a:Lcom/google/android/gms/photos/autobackup/service/a/h;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/service/a/h;->d()Z

    move-result v1

    if-nez v1, :cond_0

    .line 183
    const-string v1, "PhotosClient"

    const-string v2, "PhotosService is not connected, returning default, null."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    :goto_0
    return-object v0

    .line 188
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->g:Lcom/google/android/apps/a/a/a;

    invoke-interface {v1}, Lcom/google/android/apps/a/a/a;->b()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 193
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final c()Landroid/os/Bundle;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->a:Lcom/google/android/gms/photos/autobackup/service/a/h;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/h;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 201
    const-string v0, "PhotosClient"

    const-string v2, "PhotosService is not connected, returning default, null."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 212
    :goto_0
    return-object v0

    .line 206
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->g:Lcom/google/android/apps/a/a/a;

    invoke-interface {v0}, Lcom/google/android/apps/a/a/a;->d()Ljava/util/List;

    move-result-object v0

    .line 207
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0

    .line 212
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method public final d()Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 256
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->a:Lcom/google/android/gms/photos/autobackup/service/a/h;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/h;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 257
    const-string v0, "PhotosClient"

    const-string v2, "PhotosService is not connected, returning."

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    :goto_0
    return v1

    .line 261
    :cond_0
    const/4 v0, 0x1

    .line 263
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->g:Lcom/google/android/apps/a/a/a;

    invoke-interface {v2}, Lcom/google/android/apps/a/a/a;->e()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v1, v0

    .line 268
    goto :goto_0

    .line 265
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_1
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/service/a/b;->a:Lcom/google/android/gms/photos/autobackup/service/a/h;

    invoke-virtual {v0}, Lcom/google/android/gms/photos/autobackup/service/a/h;->d()Z

    move-result v0

    .line 301
    if-eqz v0, :cond_0

    .line 302
    sget-object v0, Lcom/google/android/gms/photos/autobackup/service/a/b;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/service/a/e;

    invoke-direct {v1, p0}, Lcom/google/android/gms/photos/autobackup/service/a/e;-><init>(Lcom/google/android/gms/photos/autobackup/service/a/b;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 309
    :cond_0
    return-void
.end method
