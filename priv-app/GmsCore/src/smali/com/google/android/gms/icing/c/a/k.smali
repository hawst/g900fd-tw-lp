.class public final Lcom/google/android/gms/icing/c/a/k;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/icing/c/a/o;

.field public b:Lcom/google/android/gms/icing/c/a/p;

.field public c:Lcom/google/android/gms/icing/c/a/n;

.field public d:Lcom/google/android/gms/icing/c/a/l;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2532
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2533
    iput-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    iput-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->b:Lcom/google/android/gms/icing/c/a/p;

    iput-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    iput-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->e:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/k;->cachedSize:I

    .line 2534
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2642
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2643
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    if-eqz v1, :cond_0

    .line 2644
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2647
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->b:Lcom/google/android/gms/icing/c/a/p;

    if-eqz v1, :cond_1

    .line 2648
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/k;->b:Lcom/google/android/gms/icing/c/a/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2651
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    if-eqz v1, :cond_2

    .line 2652
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2655
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    if-eqz v1, :cond_3

    .line 2656
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2659
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2660
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/k;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2663
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2549
    if-ne p1, p0, :cond_1

    .line 2550
    const/4 v0, 0x1

    .line 2599
    :cond_0
    :goto_0
    return v0

    .line 2552
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/k;

    if-eqz v1, :cond_0

    .line 2555
    check-cast p1, Lcom/google/android/gms/icing/c/a/k;

    .line 2556
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    if-nez v1, :cond_7

    .line 2557
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    if-nez v1, :cond_0

    .line 2565
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->b:Lcom/google/android/gms/icing/c/a/p;

    if-nez v1, :cond_8

    .line 2566
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/k;->b:Lcom/google/android/gms/icing/c/a/p;

    if-nez v1, :cond_0

    .line 2574
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    if-nez v1, :cond_9

    .line 2575
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    if-nez v1, :cond_0

    .line 2583
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    if-nez v1, :cond_a

    .line 2584
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    if-nez v1, :cond_0

    .line 2592
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->e:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 2593
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/k;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2599
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/k;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2561
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/c/a/o;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2570
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->b:Lcom/google/android/gms/icing/c/a/p;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/k;->b:Lcom/google/android/gms/icing/c/a/p;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/c/a/p;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 2579
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/c/a/n;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 2588
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/c/a/l;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 2596
    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/k;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2604
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2607
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->b:Lcom/google/android/gms/icing/c/a/p;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2609
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2611
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 2613
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/k;->e:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 2615
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/k;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2616
    return v0

    .line 2604
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/c/a/o;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2607
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->b:Lcom/google/android/gms/icing/c/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/c/a/p;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2609
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/c/a/n;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2611
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/c/a/l;->hashCode()I

    move-result v0

    goto :goto_3

    .line 2613
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1760
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/k;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/c/a/o;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/o;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->b:Lcom/google/android/gms/icing/c/a/p;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/icing/c/a/p;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/p;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->b:Lcom/google/android/gms/icing/c/a/p;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->b:Lcom/google/android/gms/icing/c/a/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/icing/c/a/n;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/n;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/icing/c/a/l;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/l;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2622
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    if-eqz v0, :cond_0

    .line 2623
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->a:Lcom/google/android/gms/icing/c/a/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2625
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->b:Lcom/google/android/gms/icing/c/a/p;

    if-eqz v0, :cond_1

    .line 2626
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->b:Lcom/google/android/gms/icing/c/a/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2628
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    if-eqz v0, :cond_2

    .line 2629
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->c:Lcom/google/android/gms/icing/c/a/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2631
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    if-eqz v0, :cond_3

    .line 2632
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->d:Lcom/google/android/gms/icing/c/a/l;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2634
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/k;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2635
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/k;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2637
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2638
    return-void
.end method
