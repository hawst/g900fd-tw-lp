.class public final Lcom/google/android/gms/googlehelp/common/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/internal/bt;


# instance fields
.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:J

.field private final h:I

.field private final i:Ljava/lang/String;

.field private final j:I

.field private final k:Ljava/lang/String;

.field private final l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 13
    const-string v0, "|"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bt;->a(Ljava/lang/String;)Lcom/google/android/gms/common/internal/bt;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/common/o;->a:Lcom/google/android/gms/common/internal/bt;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLjava/lang/String;ILjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput p1, p0, Lcom/google/android/gms/googlehelp/common/o;->b:I

    .line 65
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/common/o;->c:Ljava/lang/String;

    .line 66
    iput-object p3, p0, Lcom/google/android/gms/googlehelp/common/o;->d:Ljava/lang/String;

    .line 67
    iput-object p4, p0, Lcom/google/android/gms/googlehelp/common/o;->e:Ljava/lang/String;

    .line 68
    iput-object p5, p0, Lcom/google/android/gms/googlehelp/common/o;->f:Ljava/lang/String;

    .line 69
    iput p6, p0, Lcom/google/android/gms/googlehelp/common/o;->h:I

    .line 70
    iput-wide p7, p0, Lcom/google/android/gms/googlehelp/common/o;->g:J

    .line 71
    iput-object p9, p0, Lcom/google/android/gms/googlehelp/common/o;->i:Ljava/lang/String;

    .line 72
    iput p10, p0, Lcom/google/android/gms/googlehelp/common/o;->j:I

    .line 73
    iput-object p11, p0, Lcom/google/android/gms/googlehelp/common/o;->k:Ljava/lang/String;

    .line 74
    iput-boolean p12, p0, Lcom/google/android/gms/googlehelp/common/o;->l:Z

    .line 75
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/o;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/o;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/o;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/o;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/google/android/gms/googlehelp/common/o;->h:I

    return v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 102
    iget-wide v0, p0, Lcom/google/android/gms/googlehelp/common/o;->g:J

    return-wide v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/o;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 110
    iget v0, p0, Lcom/google/android/gms/googlehelp/common/o;->j:I

    return v0
.end method

.method public final i()Ljava/lang/String;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/o;->k:Ljava/lang/String;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/common/o;->l:Z

    return v0
.end method
