.class final Lcom/google/android/gms/plus/internal/bi;
.super Lcom/google/android/gms/common/internal/al;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/android/gms/plus/internal/ay;

.field private final c:Lcom/google/android/gms/common/c;

.field private final d:Lcom/google/android/gms/common/data/DataHolder;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ah;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 518
    iput-object p1, p0, Lcom/google/android/gms/plus/internal/bi;->b:Lcom/google/android/gms/plus/internal/ay;

    .line 519
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/al;-><init>(Lcom/google/android/gms/common/internal/aj;Ljava/lang/Object;)V

    .line 520
    iput-object p3, p0, Lcom/google/android/gms/plus/internal/bi;->c:Lcom/google/android/gms/common/c;

    .line 521
    iput-object p4, p0, Lcom/google/android/gms/plus/internal/bi;->d:Lcom/google/android/gms/common/data/DataHolder;

    .line 522
    iput-object p5, p0, Lcom/google/android/gms/plus/internal/bi;->e:Ljava/lang/String;

    .line 523
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/bi;->d:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_0

    .line 535
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/bi;->d:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 537
    :cond_0
    return-void
.end method

.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 512
    check-cast p1, Lcom/google/android/gms/plus/internal/ah;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/bi;->c:Lcom/google/android/gms/common/c;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/bi;->d:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/plus/model/a/b;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/bi;->d:Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v0, v2}, Lcom/google/android/gms/plus/model/a/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/plus/internal/bi;->e:Ljava/lang/String;

    invoke-interface {p1, v1, v0, v2}, Lcom/google/android/gms/plus/internal/ah;->a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/a/b;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
