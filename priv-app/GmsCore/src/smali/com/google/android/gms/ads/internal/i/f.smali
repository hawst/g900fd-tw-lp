.class final Lcom/google/android/gms/ads/internal/i/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/g/m;


# instance fields
.field final synthetic a:Lcom/google/android/gms/ads/internal/util/u;

.field final synthetic b:Lcom/google/android/gms/ads/internal/i/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/i/a;Lcom/google/android/gms/ads/internal/util/u;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/i/f;->b:Lcom/google/android/gms/ads/internal/i/a;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/i/f;->a:Lcom/google/android/gms/ads/internal/util/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V
    .locals 4

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/f;->b:Lcom/google/android/gms/ads/internal/i/a;

    iget-object v1, v0, Lcom/google/android/gms/ads/internal/i/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 121
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/f;->b:Lcom/google/android/gms/ads/internal/i/a;

    const/4 v2, 0x1

    iput v2, v0, Lcom/google/android/gms/ads/internal/i/a;->e:I

    .line 122
    const-string v0, "Javascript is requesting an update"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/f;->b:Lcom/google/android/gms/ads/internal/i/a;

    iget-object v2, v0, Lcom/google/android/gms/ads/internal/i/a;->d:Lcom/google/android/gms/ads/internal/a/p;

    const-string v3, "/requestReload"

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/i/f;->a:Lcom/google/android/gms/ads/internal/util/u;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/util/u;->a:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/ads/internal/g/m;

    invoke-interface {v2, v3, v0}, Lcom/google/android/gms/ads/internal/a/p;->b(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 125
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
