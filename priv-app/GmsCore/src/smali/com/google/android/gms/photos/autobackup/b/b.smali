.class public final Lcom/google/android/gms/photos/autobackup/b/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/libraries/social/account/b;

.field final c:Lcom/google/android/libraries/social/networkcapability/a;

.field final d:Lcom/google/android/libraries/social/autobackup/o;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/b/b;->a:Landroid/content/Context;

    .line 34
    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/b/b;->b:Lcom/google/android/libraries/social/account/b;

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/b/b;->a:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/networkcapability/a;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/networkcapability/a;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/b/b;->c:Lcom/google/android/libraries/social/networkcapability/a;

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/b/b;->a:Landroid/content/Context;

    const-class v1, Lcom/google/android/libraries/social/autobackup/o;

    invoke-static {v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/autobackup/o;

    iput-object v0, p0, Lcom/google/android/gms/photos/autobackup/b/b;->d:Lcom/google/android/libraries/social/autobackup/o;

    .line 37
    return-void
.end method


# virtual methods
.method final a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 86
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 87
    const/4 v0, 0x0

    .line 89
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/b/b;->b:Lcom/google/android/libraries/social/account/b;

    invoke-interface {v0, p1}, Lcom/google/android/libraries/social/account/b;->a(I)Lcom/google/android/libraries/social/account/c;

    move-result-object v0

    const-string v1, "account_name"

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/c;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
