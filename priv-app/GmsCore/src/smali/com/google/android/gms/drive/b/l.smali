.class public final Lcom/google/android/gms/drive/b/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/i;

.field private final b:Lcom/google/android/gms/drive/database/r;

.field private final c:Lcom/google/android/gms/drive/g/i;

.field private final d:Lcom/google/android/gms/drive/b/f;

.field private final e:Ljava/lang/String;

.field private final f:Landroid/os/ParcelFileDescriptor;

.field private volatile g:Ljava/lang/String;

.field private volatile h:Z

.field private volatile i:Z

.field private j:Z

.field private k:Lcom/google/android/gms/drive/b/k;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/b/f;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/b/l;->j:Z

    .line 59
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/l;->a:Lcom/google/android/gms/drive/database/i;

    .line 60
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/l;->b:Lcom/google/android/gms/drive/database/r;

    .line 61
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/l;->c:Lcom/google/android/gms/drive/g/i;

    .line 62
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/f;

    iput-object v0, p0, Lcom/google/android/gms/drive/b/l;->d:Lcom/google/android/gms/drive/b/f;

    .line 63
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/b/l;->e:Ljava/lang/String;

    .line 64
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/l;->d()Ljava/io/File;

    move-result-object v0

    invoke-static {v0, p6}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/b/l;->f:Landroid/os/ParcelFileDescriptor;

    .line 65
    return-void
.end method

.method private d()Ljava/io/File;
    .locals 3

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->d:Lcom/google/android/gms/drive/b/f;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/l;->e:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 215
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->g:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 216
    iget-boolean v0, p0, Lcom/google/android/gms/drive/b/l;->j:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->k:Lcom/google/android/gms/drive/b/k;

    if-nez v0, :cond_2

    .line 217
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/l;->d()Ljava/io/File;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/g/y;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/b/l;->g:Ljava/lang/String;

    .line 222
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->g:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 219
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->k:Lcom/google/android/gms/drive/b/k;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/b/k;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/b/l;->g:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 215
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()Landroid/os/ParcelFileDescriptor;
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 73
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/gms/drive/b/l;->h:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/drive/b/l;->i:Z

    if-nez v1, :cond_0

    :goto_0
    const-string v1, "Cannot get the file descriptor for committed or abandoned content."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 75
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/b/l;->j:Z

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->f:Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 73
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/drive/b/g;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 120
    monitor-enter p0

    .line 121
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/b/l;->h:Z

    if-eqz v0, :cond_0

    .line 122
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Content has already been committed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 124
    :cond_0
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/gms/drive/b/l;->i:Z

    if-eqz v0, :cond_1

    .line 125
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot commit content that has already been abandoned."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 128
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/b/l;->h:Z

    .line 129
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->f:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V

    .line 131
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/l;->e()Ljava/lang/String;

    move-result-object v1

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0}, Lcom/google/android/gms/drive/database/r;->b()V

    .line 135
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/database/r;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v2

    .line 136
    if-eqz v2, :cond_5

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->c:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/google/android/gms/drive/database/model/ax;->g:J

    .line 138
    iget-object v0, v2, Lcom/google/android/gms/drive/database/model/ax;->b:Ljava/lang/String;

    .line 139
    if-nez v0, :cond_3

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->e:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/database/model/ax;->a(Ljava/lang/String;)V

    .line 163
    :goto_0
    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ax;->i()V

    .line 170
    :goto_1
    invoke-interface {p1, v1}, Lcom/google/android/gms/drive/b/g;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    .line 173
    if-eqz v0, :cond_2

    .line 174
    iget-object v1, p0, Lcom/google/android/gms/drive/b/l;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->e()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 178
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/drive/b/l;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->c()V

    .line 181
    iget-object v1, p0, Lcom/google/android/gms/drive/b/l;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v2, p0, Lcom/google/android/gms/drive/b/l;->e:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/gms/drive/database/r;->e(Ljava/lang/String;)V

    return-object v0

    .line 142
    :cond_3
    :try_start_3
    iget-object v3, p0, Lcom/google/android/gms/drive/b/l;->d:Lcom/google/android/gms/drive/b/f;

    const/4 v4, 0x0

    invoke-virtual {v3, v0, v4}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v3

    if-eqz v3, :cond_4

    .line 147
    :try_start_4
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/l;->d()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    .line 149
    :try_start_5
    const-string v3, "PendingContent"

    const-string v4, "Unable to delete redundant content; will be garbage collected later: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/google/android/gms/drive/b/l;->e:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-static {v3, v0, v4, v5}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_0

    .line 178
    :catchall_1
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/b/l;->b:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->c()V

    .line 181
    iget-object v1, p0, Lcom/google/android/gms/drive/b/l;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v2, p0, Lcom/google/android/gms/drive/b/l;->e:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/google/android/gms/drive/database/r;->e(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_4
    :try_start_6
    const-string v3, "PendingContent"

    const-string v4, "Content file %s was deleted outside of the content manager; using identical new file %s instead."

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    iget-object v6, p0, Lcom/google/android/gms/drive/b/l;->e:Ljava/lang/String;

    aput-object v6, v5, v0

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->e:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/database/model/ax;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 165
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->a:Lcom/google/android/gms/drive/database/i;

    iget-object v2, p0, Lcom/google/android/gms/drive/b/l;->c:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v2}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v2

    invoke-direct {p0}, Lcom/google/android/gms/drive/b/l;->d()Ljava/io/File;

    move-result-object v4

    invoke-virtual {v4}, Ljava/io/File;->length()J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/drive/database/model/ax;->a(Lcom/google/android/gms/drive/database/i;Ljava/lang/String;JJ)Lcom/google/android/gms/drive/database/model/ay;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/drive/b/l;->e:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/gms/drive/database/model/ay;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ay;->a()Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ax;->i()V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_1
.end method

.method public final declared-synchronized b()Lcom/google/android/gms/drive/b/k;
    .locals 3

    .prologue
    .line 96
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/b/l;->h:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/drive/b/l;->i:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Cannot get an OutputStream for committed or abandoned content."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->k:Lcom/google/android/gms/drive/b/k;

    if-nez v0, :cond_0

    .line 99
    new-instance v0, Lcom/google/android/gms/drive/b/k;

    new-instance v1, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    iget-object v2, p0, Lcom/google/android/gms/drive/b/l;->f:Landroid/os/ParcelFileDescriptor;

    invoke-direct {v1, v2}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/b/k;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/b/l;->k:Lcom/google/android/gms/drive/b/k;

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->k:Lcom/google/android/gms/drive/b/k;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 96
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 190
    monitor-enter p0

    .line 191
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/b/l;->i:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/drive/b/l;->h:Z

    if-eqz v0, :cond_1

    .line 192
    :cond_0
    monitor-exit p0

    .line 208
    :goto_0
    return-void

    .line 194
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/b/l;->i:Z

    .line 195
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 197
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->f:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 201
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/drive/b/l;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/l;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/database/r;->e(Ljava/lang/String;)V

    .line 203
    :try_start_2
    invoke-direct {p0}, Lcom/google/android/gms/drive/b/l;->d()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 204
    :catch_0
    move-exception v0

    .line 205
    const-string v1, "PendingContent"

    const-string v2, "Unable to delete abandoned content; will be garbage collected later: %s"

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/drive/b/l;->e:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 198
    :catch_1
    move-exception v0

    .line 199
    const-string v1, "PendingContent"

    const-string v2, "Ignored IOException while closing abandoned content: %s"

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/gms/drive/b/l;->e:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method
