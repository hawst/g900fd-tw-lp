.class public abstract Lcom/google/android/gms/people/service/a/b/c;
.super Lcom/google/android/gms/people/service/a/e;
.source "SourceFile"


# instance fields
.field protected final g:Ljava/lang/String;

.field protected final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct/range {p0 .. p7}, Lcom/google/android/gms/people/service/a/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 46
    iput-object p6, p0, Lcom/google/android/gms/people/service/a/b/c;->g:Ljava/lang/String;

    .line 47
    iput-object p7, p0, Lcom/google/android/gms/people/service/a/b/c;->h:Ljava/lang/String;

    .line 48
    return-void
.end method

.method static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 88
    new-instance v1, Ljava/util/ArrayList;

    const/4 v0, 0x1

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 91
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "p"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 93
    new-instance v0, Lcom/google/android/gms/common/server/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/server/y;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v2

    if-eqz p3, :cond_0

    sget-object v0, Lcom/google/android/gms/plus/a/m;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :goto_0
    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/server/y;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/plus/a/b;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/google/android/gms/common/server/y;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-static {v1}, Lcom/google/android/gms/common/analytics/e;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/server/y;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/y;)V

    .line 101
    return-void

    .line 93
    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/a/m;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    goto :goto_0
.end method


# virtual methods
.method public final f()Landroid/util/Pair;
    .locals 3

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/c;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/people/service/a;->d:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/o;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/b/c;->a:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/gms/people/service/a/b/c;->c()Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/plus/n;

    invoke-direct {v2, v0, v1}, Lcom/google/android/gms/plus/n;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    const/4 v1, 0x1

    iput v1, v2, Lcom/google/android/gms/plus/n;->b:I

    invoke-virtual {v2}, Lcom/google/android/gms/plus/n;->a()Landroid/content/Intent;

    move-result-object v1

    const/high16 v2, 0x8000000

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const/4 v0, 0x6

    invoke-static {v0, v1}, Lcom/google/android/gms/people/service/b;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/people/service/b;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/people/service/a/e;->f()Landroid/util/Pair;

    move-result-object v0

    goto :goto_0
.end method
