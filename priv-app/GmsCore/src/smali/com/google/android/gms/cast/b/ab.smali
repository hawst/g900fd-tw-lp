.class public final Lcom/google/android/gms/cast/b/ab;
.super Lcom/google/android/gms/cast/internal/b;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/String;


# instance fields
.field private b:J

.field private c:J

.field private d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-string v0, "com.google.cast.tp.heartbeat"

    invoke-static {v0}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/b/ab;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/gms/cast/b/ab;->a:Ljava/lang/String;

    const-string v1, "HeartbeatChannel"

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/cast/internal/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lcom/google/android/gms/cast/b/ab;->c:J

    .line 32
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/ab;->a()V

    .line 33
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 99
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/cast/b/ab;->b:J

    .line 100
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/cast/b/ab;->d:Z

    .line 101
    return-void
.end method

.method public final a(J)Z
    .locals 9

    .prologue
    const-wide/16 v4, 0x0

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 79
    iget-wide v2, p0, Lcom/google/android/gms/cast/b/ab;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v0

    .line 83
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/cast/b/ab;->b:J

    sub-long v2, p1, v2

    .line 84
    iget-wide v4, p0, Lcom/google/android/gms/cast/b/ab;->c:J

    cmp-long v4, v2, v4

    if-ltz v4, :cond_2

    move v0, v1

    .line 85
    goto :goto_0

    .line 86
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/cast/b/ab;->d:Z

    if-nez v1, :cond_0

    iget-wide v4, p0, Lcom/google/android/gms/cast/b/ab;->c:J

    const-wide/16 v6, 0x2

    div-long/2addr v4, v6

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 88
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/cast/b/ab;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "sending PING"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    const-string v2, "type"

    const-string v3, "PING"

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    :try_start_2
    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    const-wide/16 v2, 0x0

    const-string v4, "transport-0"

    invoke-virtual {p0, v1, v2, v3, v4}, Lcom/google/android/gms/cast/b/ab;->a(Ljava/lang/String;JLjava/lang/String;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/cast/b/ab;->d:Z

    .line 89
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/cast/b/ab;->d:Z
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v2

    goto :goto_1
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 38
    invoke-virtual {p0}, Lcom/google/android/gms/cast/b/ab;->a()V

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/cast/b/ab;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "Received: %s"

    new-array v2, v5, [Ljava/lang/Object;

    aput-object p1, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 42
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 44
    const-string v1, "type"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 45
    const-string v1, "PING"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/cast/b/ab;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "sending PONG"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    const-string v1, "type"

    const-string v2, "PONG"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :goto_0
    :try_start_3
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    const-wide/16 v2, 0x0

    const-string v1, "transport-0"

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/google/android/gms/cast/b/ab;->a(Ljava/lang/String;JLjava/lang/String;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 55
    :cond_0
    :goto_1
    return-void

    .line 52
    :catch_0
    move-exception v0

    .line 53
    iget-object v1, p0, Lcom/google/android/gms/cast/b/ab;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "Message is malformed (%s); ignoring: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    aput-object p1, v3, v5

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/internal/k;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v1

    goto :goto_0
.end method
