.class final Lcom/google/android/gms/people/sync/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/x;


# instance fields
.field final synthetic a:[Ljava/lang/Long;

.field final synthetic b:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

.field final synthetic c:Ljava/util/Set;

.field final synthetic d:Lcom/google/android/gms/people/sync/z;


# direct methods
.method constructor <init>(Lcom/google/android/gms/people/sync/z;[Ljava/lang/Long;Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 949
    iput-object p1, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    iput-object p2, p0, Lcom/google/android/gms/people/sync/ad;->a:[Ljava/lang/Long;

    iput-object p3, p0, Lcom/google/android/gms/people/sync/ad;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    iput-object p4, p0, Lcom/google/android/gms/people/sync/ad;->c:Ljava/util/Set;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 949
    check-cast p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ad;->a:[Ljava/lang/Long;

    aget-object v1, v1, v0

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ad;->a:[Ljava/lang/Long;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->w()V

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ad;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ad;->c:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ad;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/sync/ap;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->b(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/y;

    move-result-object v1

    iget v3, v1, Lcom/google/android/gms/people/sync/y;->L:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/google/android/gms/people/sync/y;->L:I

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ad;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/sync/ap;->d(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->f()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/people/sync/e;->b(Ljava/util/List;)I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;->f()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    const-string v4, "allCircles"

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/people/sync/ap;->k(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->b(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/y;

    move-result-object v0

    iget v1, v0, Lcom/google/android/gms/people/sync/y;->O:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/gms/people/sync/y;->O:I

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->z()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/sync/ap;->y()V

    return-void

    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/people/sync/ad;->c:Ljava/util/Set;

    invoke-interface {v1, v2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/people/sync/ad;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/sync/ap;->c(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;)V

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->b(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/y;

    move-result-object v1

    iget v3, v1, Lcom/google/android/gms/people/sync/y;->M:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/google/android/gms/people/sync/y;->M:I
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v1, "PeopleSync"

    const-string v2, "Failed"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v1}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/people/sync/ap;->y()V

    throw v0

    :cond_3
    :try_start_3
    const-string v4, "circle"

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v4}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/people/sync/ad;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lcom/google/android/gms/people/sync/ap;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->b(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/y;

    move-result-object v0

    iget v4, v0, Lcom/google/android/gms/people/sync/y;->P:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/android/gms/people/sync/y;->P:I

    :cond_4
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_5
    const-string v4, "person"

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v4}, Lcom/google/android/gms/people/sync/z;->a(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/ap;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/people/sync/ad;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Lcom/google/android/gms/people/sync/ap;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Application;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/people/sync/ad;->d:Lcom/google/android/gms/people/sync/z;

    invoke-static {v0}, Lcom/google/android/gms/people/sync/z;->b(Lcom/google/android/gms/people/sync/z;)Lcom/google/android/gms/people/sync/y;

    move-result-object v0

    iget v4, v0, Lcom/google/android/gms/people/sync/y;->Q:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v0, Lcom/google/android/gms/people/sync/y;->Q:I
    :try_end_3
    .catch Ljava/lang/RuntimeException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method
