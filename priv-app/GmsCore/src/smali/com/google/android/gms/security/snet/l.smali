.class public final Lcom/google/android/gms/security/snet/l;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/security/snet/SnetService;

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/security/snet/SnetService;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 48
    iput-object p1, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 44
    iput-boolean v0, p0, Lcom/google/android/gms/security/snet/l;->c:Z

    .line 46
    iput-boolean v0, p0, Lcom/google/android/gms/security/snet/l;->d:Z

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/security/snet/l;->b:Ljava/lang/String;

    .line 50
    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 2

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/gms/security/a/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/security/snet/l;->c:Z

    .line 55
    iget-boolean v0, p0, Lcom/google/android/gms/security/snet/l;->c:Z

    if-eqz v0, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v0}, Lcom/google/android/gms/security/snet/SnetService;->a(Lcom/google/android/gms/security/snet/SnetService;)Lcom/google/android/gms/security/snet/f;

    invoke-static {}, Lcom/google/android/gms/security/snet/f;->a()V

    .line 59
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v0}, Lcom/google/android/gms/security/snet/SnetService;->b(Lcom/google/android/gms/security/snet/SnetService;)Lcom/google/android/gms/security/snet/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/security/snet/a;->c()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/security/snet/l;->d:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 63
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/security/snet/l;->d:Z

    if-eqz v0, :cond_1

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/security/snet/l;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v0}, Lcom/google/android/gms/security/snet/SnetService;->b(Lcom/google/android/gms/security/snet/SnetService;)Lcom/google/android/gms/security/snet/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/security/snet/l;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/security/snet/a;->c(Ljava/lang/String;)V

    .line 69
    :cond_1
    const/4 v0, 0x0

    return-object v0

    .line 60
    :catch_0
    move-exception v0

    .line 61
    iget-object v1, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v1}, Lcom/google/android/gms/security/snet/SnetService;->a(Lcom/google/android/gms/security/snet/SnetService;)Lcom/google/android/gms/security/snet/f;

    invoke-static {v0}, Lcom/google/android/gms/security/snet/f;->a(Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/gms/security/snet/l;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v0}, Lcom/google/android/gms/security/snet/SnetService;->b(Lcom/google/android/gms/security/snet/SnetService;)Lcom/google/android/gms/security/snet/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/security/snet/a;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    iget-object v0, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v0}, Lcom/google/android/gms/security/snet/SnetService;->a(Lcom/google/android/gms/security/snet/SnetService;)Lcom/google/android/gms/security/snet/f;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/google/android/gms/security/snet/f;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    iget-object v1, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v1}, Lcom/google/android/gms/security/snet/SnetService;->b(Lcom/google/android/gms/security/snet/SnetService;)Lcom/google/android/gms/security/snet/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/security/snet/a;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v2}, Lcom/google/android/gms/security/snet/SnetService;->b(Lcom/google/android/gms/security/snet/SnetService;)Lcom/google/android/gms/security/snet/a;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/security/snet/a;->b()Ljava/lang/String;

    move-result-object v2

    iget-boolean v3, p0, Lcom/google/android/gms/security/snet/l;->c:Z

    iget-object v4, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v4}, Lcom/google/android/gms/security/snet/SnetService;->b(Lcom/google/android/gms/security/snet/SnetService;)Lcom/google/android/gms/security/snet/a;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/security/snet/a;->e()J

    move-result-wide v4

    iget-object v6, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v6}, Lcom/google/android/gms/security/snet/SnetService;->c(Lcom/google/android/gms/security/snet/SnetService;)I

    move-result v6

    iget-boolean v7, p0, Lcom/google/android/gms/security/snet/l;->d:Z

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/security/snet/SnetLaunchService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZJIZLjava/lang/String;)Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    iget-object v1, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v1}, Lcom/google/android/gms/security/snet/SnetService;->c(Lcom/google/android/gms/security/snet/SnetService;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/security/snet/WatchdogService;->a(Landroid/content/Context;I)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/security/snet/l;->a:Lcom/google/android/gms/security/snet/SnetService;

    invoke-static {v0}, Lcom/google/android/gms/security/snet/SnetService;->d(Lcom/google/android/gms/security/snet/SnetService;)V

    return-void
.end method
