.class public final Lcom/google/android/gms/games/g/aq;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 8900
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 8901
    iput v0, p0, Lcom/google/android/gms/games/g/aq;->a:I

    iput v0, p0, Lcom/google/android/gms/games/g/aq;->b:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/aq;->c:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/aq;->cachedSize:I

    .line 8902
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 8960
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 8961
    iget v1, p0, Lcom/google/android/gms/games/g/aq;->a:I

    if-eqz v1, :cond_0

    .line 8962
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/games/g/aq;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8965
    :cond_0
    iget v1, p0, Lcom/google/android/gms/games/g/aq;->b:I

    if-eqz v1, :cond_1

    .line 8966
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/games/g/aq;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8969
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/games/g/aq;->c:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 8970
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/games/g/aq;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8973
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8914
    if-ne p1, p0, :cond_1

    .line 8930
    :cond_0
    :goto_0
    return v0

    .line 8917
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/aq;

    if-nez v2, :cond_2

    move v0, v1

    .line 8918
    goto :goto_0

    .line 8920
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/aq;

    .line 8921
    iget v2, p0, Lcom/google/android/gms/games/g/aq;->a:I

    iget v3, p1, Lcom/google/android/gms/games/g/aq;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 8922
    goto :goto_0

    .line 8924
    :cond_3
    iget v2, p0, Lcom/google/android/gms/games/g/aq;->b:I

    iget v3, p1, Lcom/google/android/gms/games/g/aq;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 8925
    goto :goto_0

    .line 8927
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/games/g/aq;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/aq;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 8928
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 8935
    iget v0, p0, Lcom/google/android/gms/games/g/aq;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 8937
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/games/g/aq;->b:I

    add-int/2addr v0, v1

    .line 8938
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/games/g/aq;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/games/g/aq;->c:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 8940
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 8874
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/aq;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/g/aq;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/aq;->c:J

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 8946
    iget v0, p0, Lcom/google/android/gms/games/g/aq;->a:I

    if-eqz v0, :cond_0

    .line 8947
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/games/g/aq;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8949
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/g/aq;->b:I

    if-eqz v0, :cond_1

    .line 8950
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/games/g/aq;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8952
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/games/g/aq;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 8953
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/games/g/aq;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 8955
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8956
    return-void
.end method
