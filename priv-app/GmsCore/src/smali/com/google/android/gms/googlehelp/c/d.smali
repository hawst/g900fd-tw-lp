.class public final Lcom/google/android/gms/googlehelp/c/d;
.super Landroid/widget/LinearLayout;
.source "SourceFile"


# instance fields
.field private a:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/c/af;)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/d;->a:Ljava/util/List;

    if-nez v0, :cond_0

    .line 37
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/c/d;->a:Ljava/util/List;

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/d;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 40
    return-void
.end method

.method public final setVisibility(I)V
    .locals 2

    .prologue
    .line 44
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/c/d;->getVisibility()I

    move-result v0

    if-ne v0, p1, :cond_1

    .line 60
    :cond_0
    return-void

    .line 48
    :cond_1
    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/d;->a:Ljava/util/List;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/d;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/c/af;

    .line 58
    invoke-interface {v0}, Lcom/google/android/gms/googlehelp/c/af;->f()V

    goto :goto_0
.end method
