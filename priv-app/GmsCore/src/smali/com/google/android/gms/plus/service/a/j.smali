.class public final Lcom/google/android/gms/plus/service/a/j;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private final c:I

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/android/gms/plus/internal/f;

.field private final f:[Ljava/lang/String;

.field private final g:Lcom/google/android/gms/plus/service/f;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/plus/internal/f;[Ljava/lang/String;Lcom/google/android/gms/plus/service/f;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/j;->a:Ljava/lang/String;

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/j;->b:Ljava/lang/String;

    .line 36
    iput p3, p0, Lcom/google/android/gms/plus/service/a/j;->c:I

    .line 37
    iput-object p4, p0, Lcom/google/android/gms/plus/service/a/j;->d:Ljava/lang/String;

    .line 38
    iput-object p5, p0, Lcom/google/android/gms/plus/service/a/j;->e:Lcom/google/android/gms/plus/internal/f;

    .line 39
    iput-object p6, p0, Lcom/google/android/gms/plus/service/a/j;->f:[Ljava/lang/String;

    .line 40
    iput-object p7, p0, Lcom/google/android/gms/plus/service/a/j;->g:Lcom/google/android/gms/plus/service/f;

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 10

    .prologue
    const/4 v9, 0x4

    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 46
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/j;->g:Lcom/google/android/gms/plus/service/f;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/a/j;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/plus/service/a/j;->c:I

    iget-object v4, p0, Lcom/google/android/gms/plus/service/a/j;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/service/a/j;->a:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/a/j;->f:[Ljava/lang/String;

    move-object v1, p1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/plus/service/f;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/c;

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/j;->e:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_2

    .line 65
    :goto_0
    return-void

    .line 50
    :catch_0
    move-exception v0

    .line 51
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 52
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v8, v0, v8}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 53
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/j;->e:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v0, v9, v1, v7}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    .line 56
    :catch_1
    move-exception v0

    .line 57
    const-string v1, "NetworkError"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/j;->e:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v7, v7}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/j;->e:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v7, v7}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0

    .line 63
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/j;->e:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v0, v9, v7, v7}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/j;->e:Lcom/google/android/gms/plus/internal/f;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/j;->e:Lcom/google/android/gms/plus/internal/f;

    const/16 v1, 0x8

    invoke-interface {v0, v1, v2, v2}, Lcom/google/android/gms/plus/internal/f;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    .line 72
    :cond_0
    return-void
.end method
