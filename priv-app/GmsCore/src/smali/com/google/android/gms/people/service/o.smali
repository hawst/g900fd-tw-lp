.class public final Lcom/google/android/gms/people/service/o;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[Ljava/lang/String;

.field static b:Ljava/lang/String;

.field private static final c:[Landroid/accounts/Account;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25
    new-array v0, v2, [Landroid/accounts/Account;

    sput-object v0, Lcom/google/android/gms/people/service/o;->c:[Landroid/accounts/Account;

    .line 39
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "service_googleme"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/people/service/o;->a:[Ljava/lang/String;

    .line 105
    const-string v0, ""

    sput-object v0, Lcom/google/android/gms/people/service/o;->b:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Context;)Ljava/util/Set;
    .locals 4

    .prologue
    .line 88
    invoke-static {p0}, Lcom/google/android/gms/people/service/o;->b(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v1

    .line 89
    new-instance v2, Ljava/util/LinkedHashSet;

    array-length v0, v1

    invoke-direct {v2, v0}, Ljava/util/LinkedHashSet;-><init>(I)V

    .line 90
    const/4 v0, 0x0

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    .line 91
    aget-object v3, v1, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    .line 90
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 93
    :cond_0
    return-object v2
.end method

.method public static a(Landroid/content/Context;Landroid/accounts/Account;)Z
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v2, 0x0

    .line 51
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v3

    .line 55
    const-string v0, "com.google"

    invoke-virtual {v3, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v4, v1

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_4

    aget-object v5, v1, v0

    .line 56
    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v6, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 57
    const/4 v0, 0x1

    .line 61
    :goto_1
    if-nez v0, :cond_1

    move v0, v2

    .line 81
    :goto_2
    return v0

    .line 55
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 66
    :cond_1
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v1, "hasFeatures"

    new-instance v0, Lcom/google/android/gms/people/internal/bd;

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/internal/bd;-><init>(Ljava/lang/String;)V

    move-object v1, v0

    .line 69
    :goto_3
    sget-object v0, Lcom/google/android/gms/people/service/o;->a:[Ljava/lang/String;

    invoke-virtual {v3, p1, v0, v7, v7}, Landroid/accounts/AccountManager;->hasFeatures(Landroid/accounts/Account;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    .line 73
    :try_start_0
    invoke-interface {v0}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 74
    const/4 v2, 0x2

    :try_start_1
    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 75
    const-string v2, "PeopleService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Account "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  G+="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    :cond_2
    const-string v2, "PeopleService"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/people/internal/bd;->b(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 78
    :catch_0
    move-exception v1

    .line 79
    :goto_4
    const-string v2, "PeopleService"

    const-string v3, "Unable to get account features."

    invoke-static {v2, v3, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 66
    :cond_3
    sget-object v0, Lcom/google/android/gms/people/internal/be;->a:Lcom/google/android/gms/people/internal/be;

    move-object v1, v0

    goto :goto_3

    .line 78
    :catch_1
    move-exception v0

    move-object v1, v0

    move v0, v2

    goto :goto_4

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 43
    invoke-static {p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/people/service/o;->a(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;)[Landroid/accounts/Account;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 111
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    sget-object v0, Lcom/google/android/gms/people/service/o;->c:[Landroid/accounts/Account;

    .line 125
    :cond_0
    :goto_0
    return-object v0

    .line 114
    :cond_1
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 119
    array-length v3, v0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 120
    sget-object v5, Lcom/google/android/gms/people/service/o;->b:Ljava/lang/String;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 121
    new-instance v2, Ljava/util/ArrayList;

    array-length v3, v0

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    array-length v3, v0

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v0, v1

    sget-object v5, Lcom/google/android/gms/people/service/o;->b:Ljava/lang/String;

    iget-object v6, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_3
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Landroid/accounts/Account;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/accounts/Account;

    goto :goto_0

    .line 119
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method
