.class final Lcom/google/android/gms/droidguard/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/droidguard/d/m;


# instance fields
.field final synthetic a:Lcom/google/android/gms/droidguard/c/m;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Ljava/util/Map;

.field final synthetic d:Lcom/google/android/gms/droidguard/l;


# direct methods
.method constructor <init>(Lcom/google/android/gms/droidguard/l;Lcom/google/android/gms/droidguard/c/m;Ljava/lang/String;Ljava/util/Map;)V
    .locals 0

    .prologue
    .line 223
    iput-object p1, p0, Lcom/google/android/gms/droidguard/m;->d:Lcom/google/android/gms/droidguard/l;

    iput-object p2, p0, Lcom/google/android/gms/droidguard/m;->a:Lcom/google/android/gms/droidguard/c/m;

    iput-object p3, p0, Lcom/google/android/gms/droidguard/m;->b:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/droidguard/m;->c:Ljava/util/Map;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 223
    check-cast p1, [B

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/droidguard/m;->a:Lcom/google/android/gms/droidguard/c/m;

    invoke-interface {v0, p1}, Lcom/google/android/gms/droidguard/c/m;->a([B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "DroidGuardService"

    const-string v2, "Remote callback failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    .line 236
    :try_start_0
    new-instance v0, Lcom/google/android/gms/droidguard/b/f;

    iget-object v1, p0, Lcom/google/android/gms/droidguard/m;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/droidguard/m;->d:Lcom/google/android/gms/droidguard/l;

    iget-object v2, v2, Lcom/google/android/gms/droidguard/l;->a:Lcom/google/android/gms/droidguard/DroidGuardService;

    invoke-direct {v0, v1, v2, p1}, Lcom/google/android/gms/droidguard/b/f;-><init>(Ljava/lang/String;Landroid/content/Context;Ljava/lang/Throwable;)V

    .line 238
    iget-object v1, p0, Lcom/google/android/gms/droidguard/m;->a:Lcom/google/android/gms/droidguard/c/m;

    iget-object v2, p0, Lcom/google/android/gms/droidguard/m;->c:Ljava/util/Map;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/droidguard/b/f;->a(Ljava/util/Map;[B)[B

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/droidguard/c/m;->a([B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 242
    :goto_0
    return-void

    .line 239
    :catch_0
    move-exception v0

    .line 240
    const-string v1, "DroidGuardService"

    const-string v2, "Remote callback failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
