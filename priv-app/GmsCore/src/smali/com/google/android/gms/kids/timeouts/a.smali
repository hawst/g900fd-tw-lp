.class public final Lcom/google/android/gms/kids/timeouts/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:J

.field private static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 24
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/kids/timeouts/a;->a:J

    .line 26
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "start_time_millis"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "end_time_millis"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "repeat_on_day_of_week"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/kids/timeouts/a;->b:[Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Context;)J
    .locals 15

    .prologue
    const/4 v14, 0x4

    const/4 v13, 0x3

    const/4 v12, 0x2

    const/4 v11, 0x1

    const/4 v10, 0x0

    .line 36
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/kids/timeouts/d;->e()Lcom/google/android/gms/kids/timeouts/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/kids/timeouts/d;->d()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/kids/timeouts/a;->b:[Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/kids/common/d;->c()Lcom/google/android/gms/kids/common/c;

    move-result-object v3

    const-string v4, "(%1$s <= %2$d AND %3$s > %2$d AND %4$s = \'\')"

    new-array v5, v14, [Ljava/lang/Object;

    const-string v6, "start_time_millis"

    aput-object v6, v5, v10

    invoke-static {}, Lcom/google/android/gms/kids/common/c;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v11

    const-string v6, "end_time_millis"

    aput-object v6, v5, v12

    const-string v6, "repeat_on_day_of_week"

    aput-object v6, v5, v13

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "(%1$s <= %2$d AND %3$s > %2$d AND %4$s LIKE \'%%[%5$d]%%\')"

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/Object;

    const-string v7, "start_time_millis"

    aput-object v7, v6, v10

    invoke-virtual {v3}, Lcom/google/android/gms/kids/common/c;->b()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v6, v11

    const-string v7, "end_time_millis"

    aput-object v7, v6, v12

    const-string v7, "repeat_on_day_of_week"

    aput-object v7, v6, v13

    invoke-virtual {v3}, Lcom/google/android/gms/kids/common/c;->e()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v6, v14

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v5, "(%1$s = 0)"

    new-array v6, v11, [Ljava/lang/Object;

    const-string v7, "is_deleted"

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "%1$s AND (%2$s OR %3$s)"

    new-array v7, v13, [Ljava/lang/Object;

    aput-object v5, v7, v10

    aput-object v3, v7, v11

    aput-object v4, v7, v12

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 42
    const-wide/16 v0, -0x1

    .line 44
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    :goto_0
    invoke-interface {v4}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v2

    if-nez v2, :cond_2

    .line 45
    const-string v2, "end_time_millis"

    invoke-interface {v4, v2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v4, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 48
    const-string v5, "repeat_on_day_of_week"

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v5

    invoke-interface {v4, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 52
    const-string v6, ""

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 59
    :goto_1
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 44
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v0

    .line 57
    :cond_0
    const-wide v6, 0x7fffffffffffffffL

    cmp-long v5, v2, v6

    if-nez v5, :cond_1

    const-wide v2, 0x7fffffffffffffffL

    goto :goto_1

    :cond_1
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/kids/common/d;->c()Lcom/google/android/gms/kids/common/c;

    move-result-object v5

    invoke-virtual {v5, v2, v3}, Lcom/google/android/gms/kids/common/c;->b(J)J

    move-result-wide v2

    goto :goto_1

    .line 62
    :cond_2
    invoke-static {}, Lcom/google/android/gms/kids/common/d;->c()Lcom/google/android/gms/kids/common/c;

    invoke-static {v0, v1}, Lcom/google/android/gms/kids/common/c;->c(J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 64
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    return-wide v0
.end method

.method static a(Lcom/google/s/a/z;)Landroid/util/Pair;
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    const/4 v6, 0x0

    .line 75
    invoke-static {}, Lcom/google/android/gms/kids/common/d;->c()Lcom/google/android/gms/kids/common/c;

    move-result-object v0

    .line 77
    iget-object v1, p0, Lcom/google/s/a/z;->g:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/s/a/z;->g:[I

    array-length v1, v1

    if-lez v1, :cond_0

    .line 80
    iget-object v1, p0, Lcom/google/s/a/z;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/kids/common/c;->b(J)J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/kids/common/c;->a(J)J

    move-result-wide v0

    .line 86
    :goto_0
    iget-object v2, p0, Lcom/google/s/a/z;->f:Ljava/lang/Long;

    if-nez v2, :cond_1

    .line 87
    invoke-static {v10, v11, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 98
    :goto_1
    return-object v0

    .line 84
    :cond_0
    iget-object v1, p0, Lcom/google/s/a/z;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/kids/common/c;->a(J)J

    move-result-wide v0

    goto :goto_0

    .line 89
    :cond_1
    iget-object v2, p0, Lcom/google/s/a/z;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    add-long/2addr v2, v0

    .line 90
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    cmp-long v4, v2, v4

    if-lez v4, :cond_2

    .line 91
    const-string v4, "TimeoutUtils"

    const-string v5, "startTime %d, elapsedTime %d, endTime %d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 95
    invoke-static {v10, v11, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1

    .line 98
    :cond_2
    invoke-static {v6, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 137
    const-wide v0, 0x7fffffffffffffffL

    cmp-long v0, p0, v0

    if-nez v0, :cond_0

    .line 138
    const-string v0, ""

    .line 141
    :goto_0
    return-object v0

    .line 140
    :cond_0
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "hh:mm a"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 141
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1, p0, p1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a([I)Ljava/lang/String;
    .locals 6

    .prologue
    .line 126
    if-nez p0, :cond_0

    .line 127
    const-string v0, ""

    .line 133
    :goto_0
    return-object v0

    .line 129
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, ""

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 130
    array-length v2, p0

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget v3, p0, v0

    .line 131
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "["

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "]"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 130
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 133
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Lcom/google/s/a/z;)Landroid/content/ContentValues;
    .locals 6

    .prologue
    .line 103
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 104
    const-string v0, "key"

    iget-object v1, p0, Lcom/google/s/a/z;->a:Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    const-string v0, "value"

    invoke-static {p0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 110
    iget-object v0, p0, Lcom/google/s/a/z;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 111
    iget-object v0, p0, Lcom/google/s/a/z;->f:Ljava/lang/Long;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/s/a/z;->f:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    add-long/2addr v0, v4

    .line 114
    :goto_0
    const-string v3, "start_time_millis"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 117
    const-string v3, "end_time_millis"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 120
    const-string v0, "repeat_on_day_of_week"

    iget-object v1, p0, Lcom/google/s/a/z;->g:[I

    invoke-static {v1}, Lcom/google/android/gms/kids/timeouts/a;->a([I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    return-object v2

    .line 111
    :cond_0
    const-wide v0, 0x7fffffffffffffffL

    goto :goto_0
.end method
