.class public abstract Lcom/google/android/gms/googlehelp/f/a;
.super Lcom/google/android/gms/googlehelp/f/c;
.source "SourceFile"


# instance fields
.field protected a:Lcom/google/android/gms/googlehelp/f/n;

.field private b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/f/n;)V
    .locals 4

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/f/c;-><init>()V

    .line 50
    const-string v0, "GOOGLEHELP_CancellableTask"

    const-string v1, "Create task %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 51
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/f/a;->a:Lcom/google/android/gms/googlehelp/f/n;

    .line 52
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 53
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/a;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/a;->a:Lcom/google/android/gms/googlehelp/f/n;

    invoke-interface {v0, p0}, Lcom/google/android/gms/googlehelp/f/n;->c(Lcom/google/android/gms/googlehelp/f/a;)V

    .line 56
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 96
    return-void
.end method

.method protected a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 103
    return-void
.end method

.method public final a(Z)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 64
    const-string v0, "GOOGLEHELP_CancellableTask"

    const-string v1, "Cancel task %s"

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/a;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 69
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/f/a;->cancel(Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 71
    if-eqz p1, :cond_1

    .line 72
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/a;->a:Lcom/google/android/gms/googlehelp/f/n;

    invoke-interface {v1, p0}, Lcom/google/android/gms/googlehelp/f/n;->d(Lcom/google/android/gms/googlehelp/f/a;)V

    :cond_1
    return v0

    .line 71
    :catchall_0
    move-exception v0

    if-eqz p1, :cond_2

    .line 72
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/a;->a:Lcom/google/android/gms/googlehelp/f/n;

    invoke-interface {v1, p0}, Lcom/google/android/gms/googlehelp/f/n;->d(Lcom/google/android/gms/googlehelp/f/a;)V

    :cond_2
    throw v0
.end method

.method public b()Z
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 111
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/f/a;->c:Z

    return v0
.end method

.method protected onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 79
    const-string v0, "GOOGLEHELP_CancellableTask"

    const-string v1, "Execute onPostExecute of %s"

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xe

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/a;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    :goto_0
    return-void

    .line 85
    :cond_0
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/f/a;->c:Z

    .line 86
    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/f/a;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/a;->a:Lcom/google/android/gms/googlehelp/f/n;

    invoke-interface {v0, p0}, Lcom/google/android/gms/googlehelp/f/n;->d(Lcom/google/android/gms/googlehelp/f/a;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/a;->a:Lcom/google/android/gms/googlehelp/f/n;

    invoke-interface {v1, p0}, Lcom/google/android/gms/googlehelp/f/n;->d(Lcom/google/android/gms/googlehelp/f/a;)V

    throw v0
.end method

.method protected onPreExecute()V
    .locals 0

    .prologue
    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/f/a;->a()V

    .line 61
    return-void
.end method
