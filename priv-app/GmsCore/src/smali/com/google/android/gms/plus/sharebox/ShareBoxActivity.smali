.class public Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/audience/ae;
.implements Lcom/google/android/gms/plus/audience/bh;
.implements Lcom/google/android/gms/plus/sharebox/as;
.implements Lcom/google/android/gms/plus/sharebox/b;
.implements Lcom/google/android/gms/plus/sharebox/bf;
.implements Lcom/google/android/gms/plus/sharebox/bi;
.implements Lcom/google/android/gms/plus/sharebox/o;
.implements Lcom/google/android/gms/plus/sharebox/r;
.implements Lcom/google/android/gms/plus/sharebox/t;


# instance fields
.field protected a:Lcom/google/android/gms/plus/sharebox/aq;

.field protected b:Lcom/google/android/gms/plus/sharebox/al;

.field c:Lcom/google/android/gms/plus/sharebox/bg;

.field protected final d:Landroid/os/Handler;

.field private e:Lcom/google/android/gms/plus/sharebox/au;

.field private f:Lcom/google/android/gms/plus/sharebox/bh;

.field private g:Lcom/google/android/gms/common/people/data/Audience;

.field private h:Lcom/google/android/gms/plus/audience/bg;

.field private i:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

.field private j:Ljava/lang/String;

.field private k:Lcom/google/android/gms/plus/internal/PlusCommonExtras;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 153
    new-instance v0, Lcom/google/android/gms/plus/sharebox/am;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/sharebox/am;-><init>(Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->d:Landroid/os/Handler;

    return-void
.end method

.method private static a(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1158
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1159
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bz;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1164
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1165
    if-nez p1, :cond_1

    move v0, v1

    .line 1166
    :goto_0
    if-ge v1, v0, :cond_2

    .line 1167
    aget-object v4, p1, v1

    invoke-static {v4}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cc;

    move-result-object v4

    .line 1169
    if-eqz v4, :cond_0

    .line 1170
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1166
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1165
    :cond_1
    array-length v0, p1

    goto :goto_0

    .line 1174
    :cond_2
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;-><init>()V

    iput-object v2, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;->a:Ljava/util/List;

    iget-object v1, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;->b:Ljava/util/Set;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1177
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_3

    .line 1178
    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;

    .line 1181
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1183
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/ci;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1185
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;-><init>()V

    new-instance v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;

    invoke-direct {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/cf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/cf;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bw;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    return-object v0
.end method

.method private static a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;
    .locals 3

    .prologue
    .line 1208
    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1209
    const/4 v0, 0x0

    .line 1219
    :goto_0
    return-object v0

    .line 1211
    :cond_0
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;-><init>()V

    .line 1213
    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;

    .line 1215
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1217
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/ci;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1219
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;-><init>()V

    new-instance v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;

    invoke-direct {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;-><init>()V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cg;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/cf;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/cf;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bw;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    goto :goto_0
.end method

.method private a(II)Ljava/util/List;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 1293
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1295
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->g:Lcom/google/android/gms/common/people/data/Audience;

    if-eqz v0, :cond_1

    .line 1296
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->g:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    .line 1297
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 1298
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->g:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/Audience;->a()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 1299
    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->l()Landroid/os/Bundle;

    move-result-object v4

    .line 1300
    const-string v5, "selectionSource"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 1301
    packed-switch p1, :pswitch_data_0

    .line 1323
    const-string v5, "contactType"

    invoke-virtual {v4, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 1324
    if-ne p2, v6, :cond_0

    if-ne v4, v6, :cond_0

    .line 1326
    invoke-static {v2, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    .line 1297
    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1303
    :pswitch_0
    if-ne v5, v6, :cond_0

    .line 1304
    invoke-static {v2, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    goto :goto_1

    .line 1308
    :pswitch_1
    const/4 v4, 0x2

    if-ne v5, v4, :cond_0

    .line 1309
    invoke-static {v2, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    goto :goto_1

    .line 1313
    :pswitch_2
    if-nez v5, :cond_0

    .line 1314
    invoke-static {v2, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    goto :goto_1

    .line 1318
    :pswitch_3
    const/4 v4, 0x3

    if-ne v5, v4, :cond_0

    .line 1319
    invoke-static {v2, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;Lcom/google/android/gms/common/people/data/AudienceMember;)V

    goto :goto_1

    .line 1332
    :cond_1
    return-object v2

    .line 1301
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method private a(I)V
    .locals 0

    .prologue
    .line 1065
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(I)V

    .line 1066
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u()V

    .line 1067
    return-void
.end method

.method private static a(Ljava/util/List;Lcom/google/android/gms/common/people/data/AudienceMember;)V
    .locals 1

    .prologue
    .line 1338
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cc;

    move-result-object v0

    .line 1340
    if-eqz v0, :cond_0

    .line 1341
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1343
    :cond_0
    return-void
.end method

.method private static b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;
    .locals 2

    .prologue
    .line 1267
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;-><init>()V

    .line 1268
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->a(I)Lcom/google/android/gms/plus/service/v1whitelisted/models/w;

    .line 1270
    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/v;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/v;)Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/z;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    return-object v0
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 1070
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 1071
    return-void
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 989
    const-string v0, "pref_com.google.android.gms.plus.sharebox"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 991
    const-string v1, "pref_dont_ask_again"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 992
    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 993
    return-void
.end method

.method private c(I)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 1276
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    if-nez v1, :cond_0

    .line 1287
    :goto_0
    return v0

    .line 1279
    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 1281
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/al;->e()I

    move-result v0

    goto :goto_0

    .line 1283
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/al;->c()I

    move-result v0

    goto :goto_0

    .line 1285
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/al;->d()I

    move-result v0

    goto :goto_0

    .line 1279
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static c(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cc;
    .locals 4

    .prologue
    .line 1193
    invoke-static {p0}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1194
    invoke-static {p0}, Lcom/google/android/gms/people/internal/at;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1195
    new-instance v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;

    invoke-direct {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;-><init>()V

    .line 1196
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1197
    invoke-virtual {v2, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;->b(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;

    .line 1203
    :goto_0
    invoke-virtual {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/cc;

    move-result-object v0

    :goto_1
    return-object v0

    .line 1198
    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1199
    invoke-virtual {v2, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cd;

    goto :goto_0

    .line 1201
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private t()V
    .locals 3

    .prologue
    .line 1048
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "post_error_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/ui/j;

    .line 1050
    if-nez v0, :cond_0

    .line 1051
    sget v0, Lcom/google/android/gms/p;->vJ:I

    invoke-static {v0}, Lcom/google/android/gms/common/ui/j;->a(I)Lcom/google/android/gms/common/ui/j;

    move-result-object v0

    .line 1056
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "post_error_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/ui/j;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 1058
    :cond_0
    return-void
.end method

.method private u()V
    .locals 1

    .prologue
    .line 1074
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->setResult(I)V

    .line 1075
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->finish()V

    .line 1076
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 927
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->v:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/common/analytics/v;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 929
    return-void
.end method

.method public final a(Landroid/content/Intent;Z)V
    .locals 3

    .prologue
    .line 937
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->p:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/common/analytics/v;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 940
    if-eqz p2, :cond_0

    .line 941
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->r:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/common/analytics/v;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 943
    invoke-direct {p0, p2}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(Z)V

    .line 948
    :cond_0
    const/4 v0, 0x3

    :try_start_0
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 949
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/v;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/common/analytics/v;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 955
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 570
    if-nez p1, :cond_0

    .line 574
    :goto_0
    return-void

    .line 573
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/sharebox/aq;->a(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 1

    .prologue
    .line 552
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 553
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->e()V

    .line 557
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->p()V

    .line 558
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/plus/sharebox/AddToCircleData;)V
    .locals 2

    .prologue
    .line 579
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 580
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/google/android/gms/plus/sharebox/aq;->a(Lcom/google/android/gms/plus/sharebox/AddToCircleData;Z)V

    .line 585
    :goto_0
    return-void

    .line 582
    :cond_0
    const-string v0, "ShareBox"

    const-string v1, "Failed to load add-to-circle data"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 583
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->e()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/plus/sharebox/Circle;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 589
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/plus/sharebox/aq;->a(Lcom/google/android/gms/plus/sharebox/Circle;)V

    .line 592
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v2, Lcom/google/android/gms/common/analytics/aa;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p2}, Lcom/google/android/gms/plus/sharebox/Circle;->b()Ljava/lang/String;

    move-result-object v0

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;

    invoke-direct {v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;-><init>()V

    invoke-virtual {v4, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bz;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;-><init>()V

    new-instance v4, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;

    invoke-direct {v4}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;-><init>()V

    invoke-virtual {v4, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bu;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bt;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/bt;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bw;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    invoke-virtual {v1, v2, v5, v0, v5}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    .line 601
    :goto_0
    return-void

    .line 598
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->f()V

    .line 599
    sget v0, Lcom/google/android/gms/p;->vz:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v1, 0x0

    .line 606
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 608
    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v4, Lcom/google/android/gms/common/analytics/aa;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;-><init>()V

    invoke-virtual {v0, p2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ca;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bz;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    if-nez p3, :cond_1

    move v0, v1

    :goto_0
    move v2, v1

    :goto_1
    if-ge v2, v0, :cond_2

    aget-object v7, p3, v2

    invoke-static {v7}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c(Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cc;

    move-result-object v7

    if-eqz v7, :cond_0

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    array-length v0, p3

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;-><init>()V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;

    move-result-object v0

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    iput-object v6, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a:Ljava/util/List;

    iget-object v2, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->b:Ljava/util/Set;

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/bx;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/bw;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    new-instance v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    invoke-direct {v2}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;-><init>()V

    invoke-direct {p0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c(I)I

    move-result v5

    invoke-virtual {v2, v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a(I)Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    move-result-object v2

    new-instance v5, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;

    invoke-direct {v5}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;-><init>()V

    if-nez p3, :cond_4

    :goto_2
    invoke-virtual {v5, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->a(I)Lcom/google/android/gms/plus/service/v1whitelisted/models/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/v;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/v;)Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/z;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {v3, v4, v8, v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    .line 615
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/s;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {p2, p3}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v2

    invoke-virtual {v0, v1, v8, v2, v8}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    .line 623
    :goto_3
    return-void

    .line 608
    :cond_4
    array-length v1, p3

    goto :goto_2

    .line 621
    :cond_5
    sget v0, Lcom/google/android/gms/p;->vx:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(I)V

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 488
    if-nez p1, :cond_1

    .line 489
    sget v0, Lcom/google/android/gms/p;->vG:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(I)V

    .line 508
    :cond_0
    :goto_0
    return-void

    .line 492
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 494
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p1, p0, v0}, Lcom/google/android/gms/common/c;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 496
    :catch_0
    move-exception v0

    const-string v0, "ShareBox"

    const-string v1, "Failed to start connection resolution"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 497
    sget v0, Lcom/google/android/gms/p;->vG:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(I)V

    goto :goto_0

    .line 501
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/common/ew;->b(ILandroid/app/Activity;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 503
    sget v0, Lcom/google/android/gms/p;->vG:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(I)V

    .line 504
    const-string v0, "ShareBox"

    const-string v1, "Failed to get GooglePlayServices dialog"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/data/a/a;)V
    .locals 3

    .prologue
    .line 562
    const-string v0, "ShareBox"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 563
    const-string v0, "ShareBox"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loaded preview: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    :goto_0
    invoke-virtual {v0, p2}, Lcom/google/android/gms/plus/sharebox/aq;->a(Lcom/google/android/gms/plus/data/a/a;)V

    .line 566
    return-void

    .line 565
    :cond_1
    const/4 p2, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 14

    .prologue
    .line 627
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "progress_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/f/e;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/f/e;->a()V

    .line 629
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_d

    .line 630
    sget v0, Lcom/google/android/gms/p;->vL:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(I)V

    .line 632
    const/4 v0, 0x1

    const/4 v1, -0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(II)Ljava/util/List;

    move-result-object v1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c(I)I

    move-result v2

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v4, Lcom/google/android/gms/common/analytics/u;->H:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v5, Lcom/google/android/gms/common/analytics/v;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v6

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;-><init>()V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    invoke-virtual {v0, v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->a(I)Lcom/google/android/gms/plus/service/v1whitelisted/models/w;

    if-lez v2, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v7

    int-to-float v7, v7

    int-to-float v8, v2

    div-float/2addr v7, v8

    iput v7, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->d:F

    iget-object v7, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->i:Ljava/util/Set;

    const/16 v8, 0x9

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-interface {v7, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v7, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    invoke-direct {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/v;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/v;)Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/z;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {v3, v4, v5, v6, v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    :cond_2
    const/4 v0, 0x2

    const/4 v3, -0x1

    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(II)Ljava/util/List;

    move-result-object v3

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c(I)I

    move-result v4

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v6, Lcom/google/android/gms/common/analytics/u;->I:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v7, Lcom/google/android/gms/common/analytics/v;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {v3}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v8

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;-><init>()V

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    invoke-virtual {v0, v9}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->a(I)Lcom/google/android/gms/plus/service/v1whitelisted/models/w;

    if-lez v4, :cond_3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v9

    int-to-float v9, v9

    int-to-float v10, v4

    div-float/2addr v9, v10

    iput v9, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->f:F

    iget-object v9, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->i:Ljava/util/Set;

    const/16 v10, 0xb

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-interface {v9, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    new-instance v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    invoke-direct {v9}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/v;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/v;)Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/z;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {v5, v6, v7, v8, v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    :cond_4
    const/4 v0, 0x0

    const/4 v5, -0x1

    invoke-direct {p0, v0, v5}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(II)Ljava/util/List;

    move-result-object v5

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c(I)I

    move-result v6

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v7, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v8, Lcom/google/android/gms/common/analytics/u;->J:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v9, Lcom/google/android/gms/common/analytics/v;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {v5}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v10

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;-><init>()V

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v11

    invoke-virtual {v0, v11}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->a(I)Lcom/google/android/gms/plus/service/v1whitelisted/models/w;

    if-lez v6, :cond_5

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v11

    int-to-float v11, v11

    int-to-float v12, v6

    div-float/2addr v11, v12

    iput v11, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->b:F

    iget-object v11, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->i:Ljava/util/Set;

    const/4 v12, 0x7

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v11, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_5
    new-instance v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    invoke-direct {v11}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/v;

    move-result-object v0

    invoke-virtual {v11, v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/v;)Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/z;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {v7, v8, v9, v10, v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    :cond_6
    const/4 v0, 0x3

    const/4 v7, -0x1

    invoke-direct {p0, v0, v7}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_7

    iget-object v7, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v8, Lcom/google/android/gms/common/analytics/u;->K:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v9, Lcom/google/android/gms/common/analytics/v;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v10

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    move-result-object v0

    invoke-virtual {v7, v8, v9, v10, v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    :cond_7
    const/4 v0, -0x1

    const/4 v7, 0x1

    invoke-direct {p0, v0, v7}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_8

    iget-object v7, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v8, Lcom/google/android/gms/common/analytics/u;->L:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v9, Lcom/google/android/gms/common/analytics/v;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v10

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    move-result-object v0

    invoke-virtual {v7, v8, v9, v10, v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    :cond_8
    iget-object v7, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v8, Lcom/google/android/gms/common/analytics/u;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->g:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v0}, Lcom/google/android/gms/common/analytics/e;->a(Lcom/google/android/gms/common/people/data/Audience;)Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-result-object v10

    if-nez p2, :cond_9

    const/4 v0, 0x0

    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    add-int v11, v1, v3

    add-int/2addr v11, v5

    new-instance v12, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    invoke-direct {v12}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;-><init>()V

    iput-object v0, v12, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a:Ljava/lang/String;

    iget-object v0, v12, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->d:Ljava/util/Set;

    const/4 v13, 0x5

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v0, v13}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v12, v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a(I)Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    move-result-object v0

    new-instance v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;

    invoke-direct {v6}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;-><init>()V

    iput v5, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->a:I

    iget-object v5, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->i:Ljava/util/Set;

    const/4 v12, 0x6

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-interface {v5, v12}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput v1, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->c:I

    iget-object v1, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->i:Ljava/util/Set;

    const/16 v5, 0x8

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput v3, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->e:I

    iget-object v1, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->i:Ljava/util/Set;

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput v2, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->g:I

    iget-object v1, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->i:Ljava/util/Set;

    const/16 v2, 0xd

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput v4, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->h:I

    iget-object v1, v6, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->i:Ljava/util/Set;

    const/16 v2, 0xe

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6, v11}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->a(I)Lcom/google/android/gms/plus/service/v1whitelisted/models/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/w;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/v;)Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/z;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {v7, v8, v9, v10, v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    .line 634
    const-string v0, "pref_com.google.android.gms.plus.sharebox"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v1, "pref_dont_ask_again"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v0, "com.google.android.apps.plus"

    invoke-static {p0, v0}, Lcom/google/android/gms/common/util/e;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_a

    const/4 v0, 0x1

    :goto_1
    if-nez v0, :cond_b

    .line 636
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->setResult(I)V

    .line 637
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->finish()V

    .line 648
    :goto_2
    return-void

    .line 632
    :cond_9
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/plus/model/posts/Post;->j()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 634
    :cond_a
    const/4 v0, 0x0

    goto :goto_1

    .line 642
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "install_app_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_c

    sget v0, Lcom/google/android/gms/p;->tH:I

    sget v1, Lcom/google/android/gms/p;->tE:I

    const-string v2, "com.google.android.apps.plus"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/s;->a(Landroid/content/Context;IILjava/lang/String;)Lcom/google/android/gms/plus/sharebox/s;

    move-result-object v0

    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "install_app_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/s;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto :goto_2

    :cond_c
    check-cast v0, Lcom/google/android/gms/plus/sharebox/s;

    goto :goto_3

    .line 644
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 646
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t()V

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/posts/Settings;)V
    .locals 3

    .prologue
    .line 512
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_3

    .line 513
    :cond_0
    const-string v0, "ShareBox"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 514
    const-string v0, "ShareBox"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to load settings: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 516
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 517
    sget v0, Lcom/google/android/gms/p;->vG:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(I)V

    .line 546
    :cond_2
    :goto_0
    return-void

    .line 520
    :cond_3
    const-string v0, "ShareBox"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 521
    const-string v0, "ShareBox"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Loaded settings: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    :cond_4
    sget v0, Lcom/google/android/gms/j;->ll:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 524
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/plus/sharebox/aq;->a(Lcom/google/android/gms/plus/model/posts/Settings;)V

    .line 525
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/plus/sharebox/al;->a(Lcom/google/android/gms/plus/model/posts/Settings;)V

    .line 528
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c:Lcom/google/android/gms/plus/sharebox/bg;

    iget v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->i:I

    packed-switch v0, :pswitch_data_0

    .line 536
    invoke-virtual {p2}, Lcom/google/android/gms/plus/model/posts/Settings;->f()Z

    move-result v0

    .line 539
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v2

    .line 540
    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    :goto_2
    invoke-virtual {v2, v1}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 541
    invoke-virtual {v2}, Landroid/support/v4/app/aj;->b()I

    .line 543
    if-eqz v0, :cond_2

    .line 544
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->G:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_0

    .line 530
    :pswitch_0
    const/4 v0, 0x1

    .line 531
    goto :goto_1

    .line 533
    :pswitch_1
    const/4 v0, 0x0

    .line 534
    goto :goto_1

    .line 540
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    goto :goto_2

    .line 528
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->h:Lcom/google/android/gms/plus/audience/bg;

    invoke-virtual {v0, p1, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    .line 435
    return-void
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->h:Lcom/google/android/gms/plus/audience/bg;

    iget-object v0, v0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->g:Lcom/google/android/gms/common/people/data/Audience;

    .line 419
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->g:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/aq;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    .line 420
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->g:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(Lcom/google/android/gms/common/people/data/Audience;)V

    .line 421
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 900
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/plus/sharebox/au;->b(Ljava/lang/String;)V

    .line 901
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 959
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->q:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/common/analytics/v;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 962
    if-eqz p1, :cond_0

    .line 963
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->r:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/common/analytics/v;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 965
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b(Z)V

    .line 968
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/v;->d:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/common/analytics/y;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 973
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->setResult(I)V

    .line 974
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->finish()V

    .line 975
    return-void
.end method

.method public final a([Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->f:Lcom/google/android/gms/plus/sharebox/bh;

    if-eqz v0, :cond_0

    .line 482
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->f:Lcom/google/android/gms/plus/sharebox/bh;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/plus/sharebox/bh;->a([Ljava/lang/String;I)V

    .line 484
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 914
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 915
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u()V

    .line 916
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->i:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->i:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->f()Lcom/google/android/gms/common/people/data/Audience;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a(Lcom/google/android/gms/common/people/data/Audience;Lcom/google/android/gms/common/people/data/Audience;)V

    .line 445
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->i:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->i:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/plus/sharebox/AddToCircleData;)V

    .line 448
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 787
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    .line 788
    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    .line 792
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 793
    new-instance v2, Lcom/google/android/gms/plus/sharebox/aq;

    invoke-direct {v2}, Lcom/google/android/gms/plus/sharebox/aq;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    .line 794
    sget v2, Lcom/google/android/gms/j;->pC:I

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    const-string v4, "share_fragment"

    invoke-virtual {v1, v2, v3, v4}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 795
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 798
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 799
    new-instance v2, Lcom/google/android/gms/plus/sharebox/al;

    invoke-direct {v2}, Lcom/google/android/gms/plus/sharebox/al;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    .line 800
    sget v2, Lcom/google/android/gms/j;->pC:I

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    const-string v4, "acl_fragment"

    invoke-virtual {v1, v2, v3, v4}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 801
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 804
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 805
    invoke-static {p1}, Lcom/google/android/gms/plus/sharebox/au;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    .line 806
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    const-string v3, "share_worker_fragment"

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 809
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->j:Ljava/lang/String;

    invoke-static {p0, p1, v2}, Lcom/google/android/gms/plus/sharebox/at;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    .line 811
    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I

    .line 814
    :goto_0
    invoke-virtual {v0}, Landroid/support/v4/app/v;->e()I

    move-result v1

    if-lez v1, :cond_1

    .line 815
    invoke-virtual {v0}, Landroid/support/v4/app/v;->e()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->c(I)Landroid/support/v4/app/w;

    move-result-object v1

    invoke-interface {v1}, Landroid/support/v4/app/w;->c()Ljava/lang/String;

    move-result-object v1

    .line 817
    const-string v2, "com.google.android.gms.plus.sharebox.show_compose"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "com.google.android.gms.plus.sharebox.show_acl_picker"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 820
    :cond_0
    invoke-virtual {v0}, Landroid/support/v4/app/v;->d()Z

    goto :goto_0

    .line 826
    :cond_1
    sget v0, Lcom/google/android/gms/j;->ll:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    .line 827
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->f:Lcom/google/android/gms/plus/sharebox/bh;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/plus/sharebox/bh;->a(I)V

    .line 828
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 905
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->f()V

    .line 906
    return-void
.end method

.method public final d()Lcom/google/android/gms/common/people/data/Audience;
    .locals 1

    .prologue
    .line 429
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->g:Lcom/google/android/gms/common/people/data/Audience;

    return-object v0
.end method

.method public final e()Lcom/google/android/gms/plus/sharebox/AddToCircleData;
    .locals 1

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->i:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    return-object v0
.end method

.method public final f()Lcom/google/android/gms/plus/sharebox/au;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    return-object v0
.end method

.method public final g()Lcom/google/android/gms/plus/audience/bg;
    .locals 1

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->h:Lcom/google/android/gms/plus/audience/bg;

    return-object v0
.end method

.method public getCallingPackage()Ljava/lang/String;
    .locals 1

    .prologue
    .line 462
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final h()Lcom/google/android/gms/plus/sharebox/bh;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->f:Lcom/google/android/gms/plus/sharebox/bh;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 1024
    const/4 v0, 0x0

    return v0
.end method

.method public final j()Lcom/google/android/gms/plus/internal/PlusCommonExtras;
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->k:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    return-object v0
.end method

.method public final k()Lcom/google/android/gms/plus/sharebox/bg;
    .locals 1

    .prologue
    .line 472
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c:Lcom/google/android/gms/plus/sharebox/bg;

    return-object v0
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 727
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    .line 728
    const-string v0, "create_circle_fragment"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/p;

    .line 730
    if-nez v0, :cond_0

    .line 731
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->i:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;->g()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/p;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/plus/sharebox/p;

    move-result-object v0

    .line 733
    :cond_0
    const-string v2, "create_circle_fragment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/p;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 734
    return-void
.end method

.method public final m()V
    .locals 3

    .prologue
    .line 738
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/plus/sharebox/AddToCircleActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 739
    const-string v1, "add_to_circle_data"

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/aq;->i()Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 741
    const-string v1, "calling_package_name"

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getCallingPackage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 742
    const-string v1, "client_application_id"

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c:Lcom/google/android/gms/plus/sharebox/bg;

    iget-object v2, v2, Lcom/google/android/gms/plus/sharebox/bg;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 744
    const/4 v1, 0x5

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 745
    return-void
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 749
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 750
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 751
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 752
    const-string v1, "com.google.android.gms.plus.sharebox.show_acl_picker"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 753
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 754
    return-void
.end method

.method public final o()V
    .locals 3

    .prologue
    .line 758
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "underage_warning_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/a;

    .line 760
    if-nez v0, :cond_0

    .line 761
    sget v0, Lcom/google/android/gms/p;->vQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->vP:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/sharebox/a;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/sharebox/a;

    move-result-object v0

    .line 764
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "underage_warning_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/a;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 765
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/v;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/common/analytics/v;->h:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 768
    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v0, -0x1

    .line 354
    packed-switch p1, :pswitch_data_0

    .line 410
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/q;->onActivityResult(IILandroid/content/Intent;)V

    .line 414
    :goto_1
    return-void

    .line 358
    :pswitch_0
    if-ne p2, v0, :cond_1

    .line 361
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/au;->n()V

    goto :goto_1

    .line 363
    :cond_1
    const-string v0, "ShareBox"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 364
    const-string v0, "ShareBox"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to resolve connection/account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    :cond_2
    invoke-virtual {p0, p2}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->setResult(I)V

    .line 367
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->finish()V

    goto :goto_1

    .line 372
    :pswitch_1
    if-ne p2, v0, :cond_3

    .line 373
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->s:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/common/analytics/v;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 379
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/v;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/common/analytics/y;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 381
    invoke-virtual {p0, p2}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->setResult(I)V

    .line 382
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->finish()V

    goto :goto_1

    .line 376
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->t:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v2, Lcom/google/android/gms/common/analytics/v;->e:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_2

    .line 386
    :pswitch_2
    if-eq p2, v0, :cond_4

    if-eq p2, v2, :cond_4

    if-ne p2, v2, :cond_5

    .line 389
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    new-instance v2, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v4, v4, v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;-><init>(ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/plus/circles/AddToCircleConsentData;)V

    .line 392
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->q()V

    goto :goto_1

    .line 395
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->g()V

    .line 397
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->k()V

    goto :goto_1

    .line 402
    :pswitch_3
    if-ne p2, v0, :cond_0

    if-eqz p3, :cond_0

    .line 403
    const-string v0, "add_to_circle_data"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->i:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    .line 405
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->i:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/aq;->a(Lcom/google/android/gms/plus/sharebox/AddToCircleData;Z)V

    goto/16 :goto_0

    .line 354
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 3

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/al;->isHidden()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/al;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    :goto_0
    return-void

    .line 316
    :cond_0
    const/4 v0, 0x0

    .line 317
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    .line 318
    invoke-virtual {v1}, Landroid/support/v4/app/v;->e()I

    move-result v2

    .line 319
    if-lez v2, :cond_1

    .line 320
    add-int/lit8 v0, v2, -0x1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->c(I)Landroid/support/v4/app/w;

    move-result-object v0

    .line 324
    :cond_1
    if-eqz v0, :cond_2

    const-string v1, "com.google.android.gms.plus.sharebox.show_compose"

    invoke-interface {v0}, Landroid/support/v4/app/w;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "com.google.android.gms.plus.sharebox.show_acl_picker"

    invoke-interface {v0}, Landroid/support/v4/app/w;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 327
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 328
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 330
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->d:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_0

    .line 333
    :cond_3
    const-string v1, "com.google.android.gms.plus.sharebox.show_acl_picker"

    invoke-interface {v0}, Landroid/support/v4/app/w;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 335
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->h:Lcom/google/android/gms/plus/audience/bg;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->g:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {v0, v1, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/Object;)V

    .line 337
    :cond_4
    invoke-super {p0}, Landroid/support/v4/app/q;->onBackPressed()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 173
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 177
    invoke-static {p0}, Lcom/google/android/gms/plus/sharebox/at;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->j:Ljava/lang/String;

    .line 178
    invoke-static {p0}, Lcom/google/android/gms/common/util/ak;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 179
    sget v0, Lcom/google/android/gms/p;->vI:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(I)V

    .line 286
    :cond_0
    :goto_0
    return-void

    .line 184
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/PlusCommonExtras;->b(Landroid/content/Intent;)Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->k:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->k:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    const-string v1, "gpsb0"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/plus/k;->a(Landroid/app/Activity;Lcom/google/android/gms/plus/internal/PlusCommonExtras;Ljava/lang/String;)V

    .line 188
    new-instance v0, Lcom/google/android/gms/plus/sharebox/bg;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/sharebox/bg;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c:Lcom/google/android/gms/plus/sharebox/bg;

    .line 189
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/at;->c(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 190
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    .line 191
    invoke-static {p0, v0}, Lcom/google/android/gms/plus/sharebox/at;->a(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 192
    const-string v0, "ShareBox"

    const-string v1, "Invalid share action"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u()V

    goto :goto_0

    .line 196
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c:Lcom/google/android/gms/plus/sharebox/bg;

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/plus/sharebox/bg;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    .line 198
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/at;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 199
    invoke-static {p0}, Lcom/google/android/gms/plus/sharebox/at;->c(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 200
    const-string v0, "ShareBox"

    const-string v1, "Invalid deep link"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 201
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u()V

    goto :goto_0

    .line 205
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/at;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 206
    invoke-static {p0}, Lcom/google/android/gms/plus/sharebox/at;->b(Landroid/app/Activity;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 207
    const-string v0, "ShareBox"

    const-string v1, "Invalid interactive post"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->u()V

    goto :goto_0

    .line 214
    :cond_5
    sget v0, Lcom/google/android/gms/l;->eN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->setContentView(I)V

    .line 216
    sget v0, Lcom/google/android/gms/j;->pC:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/gms/g;->bI:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/e;->d:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/plus/sharebox/at;->a(Landroid/view/View;IZ)V

    .line 223
    if-eqz p1, :cond_c

    .line 224
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->g:Lcom/google/android/gms/common/people/data/Audience;

    .line 225
    const-string v0, "addToCircleData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->i:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    .line 226
    const-string v0, "share_box_hidden"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 227
    const-string v0, "acl_hidden"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    move v3, v1

    move v1, v0

    .line 234
    :goto_1
    new-instance v0, Lcom/google/android/gms/plus/audience/bg;

    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->g:Lcom/google/android/gms/common/people/data/Audience;

    invoke-direct {v0, v4}, Lcom/google/android/gms/plus/audience/bg;-><init>(Lcom/google/android/gms/common/people/data/Audience;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->h:Lcom/google/android/gms/plus/audience/bg;

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->h:Lcom/google/android/gms/plus/audience/bg;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/audience/bg;->a(Lcom/google/android/gms/plus/audience/bh;)V

    .line 238
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v4

    .line 239
    invoke-virtual {v4}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v5

    .line 240
    const-string v0, "share_worker_fragment"

    invoke-virtual {v4, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/au;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    if-nez v0, :cond_6

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c:Lcom/google/android/gms/plus/sharebox/bg;

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/sharebox/au;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    const-string v6, "share_worker_fragment"

    invoke-virtual {v5, v0, v6}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 246
    :cond_6
    const-string v0, "title_fragment"

    invoke-virtual {v4, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/bh;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->f:Lcom/google/android/gms/plus/sharebox/bh;

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->f:Lcom/google/android/gms/plus/sharebox/bh;

    if-nez v0, :cond_7

    .line 248
    new-instance v0, Lcom/google/android/gms/plus/sharebox/bh;

    invoke-direct {v0}, Lcom/google/android/gms/plus/sharebox/bh;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->f:Lcom/google/android/gms/plus/sharebox/bh;

    .line 249
    sget v0, Lcom/google/android/gms/j;->sS:I

    iget-object v6, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->f:Lcom/google/android/gms/plus/sharebox/bh;

    const-string v7, "title_fragment"

    invoke-virtual {v5, v0, v6, v7}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 251
    :cond_7
    const-string v0, "share_fragment"

    invoke-virtual {v4, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/aq;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    if-nez v0, :cond_8

    .line 253
    new-instance v0, Lcom/google/android/gms/plus/sharebox/aq;

    invoke-direct {v0}, Lcom/google/android/gms/plus/sharebox/aq;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    .line 254
    sget v0, Lcom/google/android/gms/j;->pC:I

    iget-object v6, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    const-string v7, "share_fragment"

    invoke-virtual {v5, v0, v6, v7}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 256
    :cond_8
    const-string v0, "acl_fragment"

    invoke-virtual {v4, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/al;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    .line 257
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    if-nez v0, :cond_9

    .line 258
    new-instance v0, Lcom/google/android/gms/plus/sharebox/al;

    invoke-direct {v0}, Lcom/google/android/gms/plus/sharebox/al;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    .line 259
    sget v0, Lcom/google/android/gms/j;->pC:I

    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    const-string v6, "acl_fragment"

    invoke-virtual {v5, v0, v4, v6}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 261
    :cond_9
    if-eqz v3, :cond_d

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v5, v0}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 266
    :goto_2
    if-eqz v1, :cond_e

    .line 267
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    invoke-virtual {v5, v0}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 271
    :goto_3
    invoke-virtual {v5}, Landroid/support/v4/app/aj;->a()I

    .line 274
    if-nez p1, :cond_0

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c:Lcom/google/android/gms/plus/sharebox/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->f()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c:Lcom/google/android/gms/plus/sharebox/bg;

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->o:Lcom/google/android/gms/plus/sharebox/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/a/b;->d()Z

    move-result v0

    if-eqz v0, :cond_a

    .line 277
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->n:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 279
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c:Lcom/google/android/gms/plus/sharebox/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->g()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->m:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 282
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c:Lcom/google/android/gms/plus/sharebox/bg;

    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/bg;->q:Lcom/google/android/gms/common/people/data/Audience;

    if-eqz v1, :cond_f

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->q:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v0}, Lcom/google/android/gms/common/people/data/g;->a(Lcom/google/android/gms/common/people/data/Audience;)Z

    move-result v0

    if-nez v0, :cond_f

    :goto_4
    if-eqz v2, :cond_0

    .line 283
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/analytics/u;->o:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto/16 :goto_0

    .line 229
    :cond_c
    sget-object v0, Lcom/google/android/gms/common/people/data/a;->a:Lcom/google/android/gms/common/people/data/Audience;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->g:Lcom/google/android/gms/common/people/data/Audience;

    .line 230
    new-instance v0, Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-static {p0}, Lcom/google/android/gms/plus/sharebox/j;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/google/android/gms/plus/sharebox/j;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/plus/sharebox/AddToCircleData;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->i:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    move v1, v2

    move v3, v2

    goto/16 :goto_1

    .line 264
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v5, v0}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    goto :goto_2

    .line 269
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    invoke-virtual {v5, v0}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    goto :goto_3

    .line 282
    :cond_f
    const/4 v2, 0x0

    goto :goto_4
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 290
    invoke-super {p0}, Landroid/support/v4/app/q;->onResume()V

    .line 291
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "confirm_action_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/n;

    .line 293
    if-eqz v0, :cond_0

    .line 294
    invoke-virtual {v0, p0}, Lcom/google/android/gms/plus/sharebox/n;->a(Lcom/google/android/gms/plus/sharebox/o;)V

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/au;->f()Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 297
    sget v0, Lcom/google/android/gms/j;->ll:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 299
    :cond_1
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 303
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 304
    const-string v0, "audience"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->g:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 305
    const-string v0, "addToCircleData"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->i:Lcom/google/android/gms/plus/sharebox/AddToCircleData;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 306
    const-string v0, "share_box_hidden"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/aq;->isHidden()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 307
    const-string v0, "acl_hidden"

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/al;->isHidden()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 308
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    .prologue
    .line 342
    sget v0, Lcom/google/android/gms/j;->pC:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 343
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p1, v0}, Lcom/google/android/gms/common/util/d;->a(Landroid/view/MotionEvent;Landroid/view/View;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 345
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->onBackPressed()V

    .line 346
    invoke-static {p0, v0}, Lcom/google/android/gms/common/util/ae;->b(Landroid/content/Context;Landroid/view/View;)Z

    .line 347
    const/4 v0, 0x1

    .line 349
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public final p()V
    .locals 1

    .prologue
    .line 772
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 773
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/au;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 775
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->k()V

    .line 778
    :cond_1
    return-void
.end method

.method public final q()V
    .locals 6

    .prologue
    .line 833
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 834
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/au;->h()Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    move-result-object v4

    .line 835
    invoke-virtual {v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 836
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/au;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->c:Lcom/google/android/gms/plus/sharebox/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/plus/circles/AddToCircleConsentData;->d()Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x50

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/plus/circles/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 864
    :goto_0
    return-void

    .line 847
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->h()V

    .line 851
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->g:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v0}, Lcom/google/android/gms/common/people/data/g;->a(Lcom/google/android/gms/common/people/data/Audience;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 852
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->n()V

    goto :goto_0

    .line 857
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->c()Lcom/google/android/gms/plus/model/posts/Post;

    move-result-object v0

    .line 858
    if-eqz v0, :cond_3

    .line 859
    sget v1, Lcom/google/android/gms/p;->vK:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/plus/f/e;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/plus/f/e;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v2

    const-string v3, "progress_dialog"

    invoke-virtual {v2, v1, v3}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    invoke-virtual {v2}, Landroid/support/v4/app/aj;->b()I

    .line 860
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->e:Lcom/google/android/gms/plus/sharebox/au;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/model/posts/Post;)V

    goto :goto_0

    .line 862
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->t()V

    goto :goto_0
.end method

.method public final r()V
    .locals 4

    .prologue
    .line 868
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    .line 869
    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    .line 870
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 871
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 872
    invoke-virtual {v0}, Landroid/support/v4/app/v;->e()I

    move-result v2

    if-lez v2, :cond_0

    const-string v2, "com.google.android.gms.plus.sharebox.show_acl_picker"

    invoke-virtual {v0}, Landroid/support/v4/app/v;->e()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Landroid/support/v4/app/v;->c(I)Landroid/support/v4/app/w;

    move-result-object v3

    invoke-interface {v3}, Landroid/support/v4/app/w;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 877
    const-string v2, "com.google.android.gms.plus.sharebox.show_acl_picker"

    invoke-virtual {v0, v2}, Landroid/support/v4/app/v;->b(Ljava/lang/String;)V

    .line 884
    :goto_0
    invoke-virtual {v1}, Landroid/support/v4/app/aj;->a()I

    .line 885
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->h:Lcom/google/android/gms/plus/audience/bg;

    iget-object v0, v0, Lcom/google/android/gms/plus/audience/bg;->a:Lcom/google/android/gms/common/people/data/Audience;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a(Lcom/google/android/gms/common/people/data/Audience;)V

    .line 886
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->a:Lcom/google/android/gms/plus/sharebox/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/aq;->a()V

    .line 887
    return-void

    .line 882
    :cond_0
    const-string v0, "com.google.android.gms.plus.sharebox.show_compose"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/aj;->a(Ljava/lang/String;)Landroid/support/v4/app/aj;

    goto :goto_0
.end method

.method public final s()V
    .locals 1

    .prologue
    .line 891
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/ShareBoxActivity;->b:Lcom/google/android/gms/plus/sharebox/al;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/al;->b()V

    .line 892
    return-void
.end method
