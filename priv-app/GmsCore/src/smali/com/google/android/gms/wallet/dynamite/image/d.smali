.class public abstract Lcom/google/android/gms/wallet/dynamite/image/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Z

.field protected final b:Landroid/content/Context;

.field protected final c:Landroid/content/res/Resources;

.field protected d:Lcom/google/android/gms/wallet/dynamite/image/a;

.field protected e:Landroid/graphics/Bitmap;

.field protected f:Z

.field private final g:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->g:Landroid/util/SparseArray;

    .line 36
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->a:Z

    .line 41
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->f:Z

    .line 49
    iput-object p1, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->b:Landroid/content/Context;

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->c:Landroid/content/res/Resources;

    .line 51
    return-void
.end method

.method public static a(Landroid/widget/ImageView;)Lcom/google/android/gms/wallet/dynamite/image/f;
    .locals 2

    .prologue
    .line 213
    if-eqz p0, :cond_0

    .line 214
    invoke-virtual {p0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 215
    instance-of v1, v0, Lcom/google/android/gms/wallet/dynamite/image/e;

    if-eqz v1, :cond_0

    .line 217
    check-cast v0, Lcom/google/android/gms/wallet/dynamite/image/e;

    .line 218
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/dynamite/image/e;->a()Lcom/google/android/gms/wallet/dynamite/image/f;

    move-result-object v0

    .line 221
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/dynamite/image/d;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Landroid/graphics/drawable/TransitionDrawable;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    const/4 v2, 0x0

    new-instance v3, Landroid/graphics/drawable/ColorDrawable;

    const v4, 0x106000d

    invoke-direct {v3, v4}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    aput-object v3, v1, v2

    const/4 v2, 0x1

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->b:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/16 v1, 0xc8

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/TransitionDrawable;->startTransition(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Ljava/lang/Object;)Landroid/graphics/Bitmap;
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 143
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->f:Z

    .line 144
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/dynamite/image/a;)V
    .locals 0

    .prologue
    .line 131
    iput-object p1, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->d:Lcom/google/android/gms/wallet/dynamite/image/a;

    .line 132
    return-void
.end method

.method public final a(Ljava/lang/Object;Landroid/widget/ImageView;)V
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->e:Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/wallet/dynamite/image/d;->a(Ljava/lang/Object;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    .line 62
    return-void
.end method

.method protected final a(Ljava/lang/Object;Landroid/widget/ImageView;I)V
    .locals 3

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 76
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->c:Landroid/content/res/Resources;

    invoke-static {v0, p3}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 84
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->g:Landroid/util/SparseArray;

    invoke-virtual {v1, p3, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->g:Landroid/util/SparseArray;

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/wallet/dynamite/image/d;->a(Ljava/lang/Object;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V

    .line 87
    return-void

    .line 77
    :catch_0
    move-exception v0

    .line 81
    const-string v1, "ImageWorker"

    const-string v2, "Ignored error on decodeResource"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 82
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Object;Landroid/widget/ImageView;Landroid/graphics/Bitmap;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 99
    const/4 v0, 0x0

    .line 100
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 101
    iget-object v4, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->d:Lcom/google/android/gms/wallet/dynamite/image/a;

    if-eqz v4, :cond_0

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->d:Lcom/google/android/gms/wallet/dynamite/image/a;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/dynamite/image/a;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 105
    :cond_0
    if-eqz v0, :cond_2

    .line 106
    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 114
    :cond_1
    :goto_0
    return-void

    .line 107
    :cond_2
    invoke-static {p2}, Lcom/google/android/gms/wallet/dynamite/image/d;->a(Landroid/widget/ImageView;)Lcom/google/android/gms/wallet/dynamite/image/f;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/image/f;->a(Lcom/google/android/gms/wallet/dynamite/image/f;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/image/f;->a(Lcom/google/android/gms/wallet/dynamite/image/f;)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/dynamite/image/f;->cancel(Z)Z

    :cond_3
    move v0, v2

    :goto_1
    if-eqz v0, :cond_1

    .line 108
    new-instance v0, Lcom/google/android/gms/wallet/dynamite/image/f;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gms/wallet/dynamite/image/f;-><init>(Lcom/google/android/gms/wallet/dynamite/image/d;Landroid/widget/ImageView;)V

    .line 109
    new-instance v3, Lcom/google/android/gms/wallet/dynamite/image/e;

    iget-object v4, p0, Lcom/google/android/gms/wallet/dynamite/image/d;->c:Landroid/content/res/Resources;

    invoke-direct {v3, v4, p3, v0}, Lcom/google/android/gms/wallet/dynamite/image/e;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Lcom/google/android/gms/wallet/dynamite/image/f;)V

    .line 110
    invoke-virtual {p2, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 112
    new-array v2, v2, [Ljava/lang/Object;

    aput-object p1, v2, v1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/dynamite/image/f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0

    :cond_4
    move v0, v1

    .line 107
    goto :goto_1
.end method
