.class public final Lcom/google/android/gms/drive/database/model/be;
.super Lcom/google/android/gms/drive/database/model/ad;
.source "SourceFile"


# instance fields
.field public final a:J

.field public b:Ljava/lang/String;

.field public c:I

.field private final d:Lcom/google/android/gms/drive/database/model/EntrySpec;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;JLjava/lang/String;Lcom/google/android/gms/drive/database/model/EntrySpec;)V
    .locals 8

    .prologue
    .line 37
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/database/model/be;-><init>(Lcom/google/android/gms/drive/database/i;JLjava/lang/String;Lcom/google/android/gms/drive/database/model/EntrySpec;I)V

    .line 38
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/database/i;JLjava/lang/String;Lcom/google/android/gms/drive/database/model/EntrySpec;I)V
    .locals 2

    .prologue
    .line 28
    invoke-static {}, Lcom/google/android/gms/drive/database/model/bg;->a()Lcom/google/android/gms/drive/database/model/bg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/drive/database/model/ad;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/ae;Landroid/net/Uri;)V

    .line 29
    iput-wide p2, p0, Lcom/google/android/gms/drive/database/model/be;->a:J

    .line 30
    const-string v0, "null payload"

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/be;->b:Ljava/lang/String;

    .line 31
    iput-object p5, p0, Lcom/google/android/gms/drive/database/model/be;->d:Lcom/google/android/gms/drive/database/model/EntrySpec;

    .line 32
    iput p6, p0, Lcom/google/android/gms/drive/database/model/be;->c:I

    .line 33
    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/database/r;)Lcom/google/android/gms/drive/database/a/c;
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/google/android/gms/drive/database/model/bf;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/database/model/bf;-><init>(Lcom/google/android/gms/drive/database/r;)V

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/content/ContentValues;)V
    .locals 4

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/gms/drive/database/model/bh;->a:Lcom/google/android/gms/drive/database/model/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bh;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/database/model/be;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 43
    sget-object v0, Lcom/google/android/gms/drive/database/model/bh;->b:Lcom/google/android/gms/drive/database/model/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bh;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/database/model/be;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    sget-object v0, Lcom/google/android/gms/drive/database/model/bh;->c:Lcom/google/android/gms/drive/database/model/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bh;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/drive/database/model/be;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 45
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 76
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "PendingAction[accountSqlId=%d, payload=%s, sqlId=%s]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/be;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/database/model/be;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
