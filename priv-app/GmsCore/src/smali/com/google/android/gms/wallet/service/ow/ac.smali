.class final Lcom/google/android/gms/wallet/service/ow/ac;
.super Lcom/google/android/gms/wallet/service/o;
.source "SourceFile"


# instance fields
.field final synthetic c:Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;

.field final synthetic d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field final synthetic e:Lcom/google/android/gms/wallet/service/ow/y;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/service/ow/y;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V
    .locals 0

    .prologue
    .line 230
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/ac;->e:Lcom/google/android/gms/wallet/service/ow/y;

    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ow/ac;->c:Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;

    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ow/ac;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/wallet/service/o;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/a/b;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 7

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ac;->c:Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->c()Lcom/google/aa/b/a/a/a/a/c;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    move-object v2, v0

    check-cast v2, Lcom/google/aa/b/a/a/a/a/c;

    .line 237
    invoke-static {}, Lcom/google/android/gms/wallet/service/ow/ao;->a()Lcom/google/aa/b/a/a/a/a/t;

    move-result-object v0

    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ac;->e:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->a(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/common/ab;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ac;->a:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ow/ac;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/wallet/common/ab;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 241
    if-eqz v0, :cond_0

    .line 242
    iput-object v0, v2, Lcom/google/aa/b/a/a/a/a/c;->b:Ljava/lang/String;

    .line 244
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/ac;->e:Lcom/google/android/gms/wallet/service/ow/y;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/y;->c(Lcom/google/android/gms/wallet/service/ow/y;)Lcom/google/android/gms/wallet/service/ow/ak;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/ac;->d:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v1

    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ow/ac;->c:Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->b()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string v6, "request"

    invoke-static {v2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/util/m;->b([B)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v6, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string v5, "cvn"

    invoke-direct {v2, v5, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v2, "/online-secure/v2/sdk/v1/authenticateInstrument?s7e=cvn"

    const/16 v5, 0x17

    const-string v6, "authenticate_instrument"

    move-object v3, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/ak;->a(ILjava/lang/String;Lcom/google/android/gms/wallet/a/b;Ljava/util/ArrayList;ILjava/lang/String;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method
