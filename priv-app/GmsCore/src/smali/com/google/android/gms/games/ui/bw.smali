.class public Lcom/google/android/gms/games/ui/bw;
.super Lcom/google/android/gms/games/ui/ac;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/a;


# instance fields
.field private c:Ljava/util/ArrayList;

.field private final d:Landroid/util/SparseArray;

.field private e:I

.field private f:I

.field private final h:Landroid/support/v7/widget/ap;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/ac;-><init>()V

    .line 187
    new-instance v0, Lcom/google/android/gms/games/ui/bx;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/bx;-><init>(Lcom/google/android/gms/games/ui/bw;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/bw;->h:Landroid/support/v7/widget/ap;

    .line 60
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/bw;->d:Landroid/util/SparseArray;

    .line 61
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/bw;->e:I

    .line 62
    return-void
.end method

.method private constructor <init>(Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/bw;-><init>()V

    .line 51
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/bw;->a(Ljava/util/ArrayList;)V

    .line 52
    return-void
.end method

.method synthetic constructor <init>(Ljava/util/ArrayList;B)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bw;-><init>(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/bw;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/google/android/gms/games/ui/bw;->e:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/bw;Lcom/google/android/gms/games/ui/bz;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 20
    if-eqz p1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    move v2, v0

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ac;

    iget-object v4, p1, Lcom/google/android/gms/games/ui/bz;->a:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget v0, p1, Lcom/google/android/gms/games/ui/bz;->b:I

    add-int/2addr v0, v2

    :goto_1
    return v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ac;->a()I

    move-result v0

    add-int/2addr v2, v0

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/bw;I)Lcom/google/android/gms/games/ui/bz;
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bw;->f(I)Lcom/google/android/gms/games/ui/bz;

    move-result-object v0

    return-object v0
.end method

.method private static e(II)I
    .locals 3

    .prologue
    .line 123
    if-lez p0, :cond_0

    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    move v0, p1

    move v1, p0

    :goto_1
    if-eqz v0, :cond_1

    rem-int/2addr v1, v0

    move v2, v1

    move v1, v0

    move v0, v2

    goto :goto_1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    div-int v0, p1, v1

    mul-int/2addr v0, p0

    return v0
.end method

.method private f(I)Lcom/google/android/gms/games/ui/bz;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 243
    if-gez p1, :cond_0

    move-object v0, v1

    .line 260
    :goto_0
    return-object v0

    .line 247
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v2, v0

    move v4, v0

    .line 251
    :goto_1
    if-ge v4, v5, :cond_2

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ac;

    .line 253
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ac;->a()I

    move-result v3

    add-int/2addr v3, v2

    .line 254
    if-ge p1, v3, :cond_1

    .line 255
    new-instance v1, Lcom/google/android/gms/games/ui/bz;

    sub-int v2, p1, v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/games/ui/bz;-><init>(Lcom/google/android/gms/games/ui/ac;I)V

    move-object v0, v1

    goto :goto_0

    .line 258
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v2, v3

    move v4, v0

    .line 259
    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 260
    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 149
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bw;->g()Z

    move-result v1

    if-nez v1, :cond_0

    .line 157
    :goto_0
    return v0

    .line 154
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_1
    if-ge v2, v3, :cond_1

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ac;->a()I

    move-result v0

    add-int/2addr v1, v0

    .line 154
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move v0, v1

    .line 157
    goto :goto_0
.end method

.method public final a(I)I
    .locals 3

    .prologue
    .line 172
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bw;->f(I)Lcom/google/android/gms/games/ui/bz;

    move-result-object v1

    .line 173
    if-eqz v1, :cond_0

    .line 174
    iget-object v0, v1, Lcom/google/android/gms/games/ui/bz;->a:Lcom/google/android/gms/games/ui/ac;

    iget v2, v1, Lcom/google/android/gms/games/ui/bz;->b:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/ac;->a(I)I

    move-result v0

    .line 175
    iget-object v2, p0, Lcom/google/android/gms/games/ui/bw;->d:Landroid/util/SparseArray;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/bz;->a:Lcom/google/android/gms/games/ui/ac;

    invoke-virtual {v2, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 178
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/ac;->a(I)I

    move-result v0

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/games/ui/ac;)I
    .locals 4

    .prologue
    .line 211
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bw;->t()I

    move-result v1

    .line 212
    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ac;

    .line 214
    if-ne v0, p1, :cond_0

    move v0, v1

    .line 221
    :goto_1
    return v0

    .line 217
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ac;->a()I

    move-result v0

    add-int/2addr v1, v0

    .line 212
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 220
    :cond_1
    const-string v0, "Given adapter not nested within this adapter."

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    .line 221
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public final synthetic a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/cs;
    .locals 2

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bw;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ac;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/ui/ac;->a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/cs;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ae;

    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unknown item view type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final synthetic a(Landroid/support/v7/widget/cs;I)V
    .locals 2

    .prologue
    .line 19
    check-cast p1, Lcom/google/android/gms/games/ui/ae;

    invoke-direct {p0, p2}, Lcom/google/android/gms/games/ui/bw;->f(I)Lcom/google/android/gms/games/ui/bz;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/games/ui/bz;->a:Lcom/google/android/gms/games/ui/ac;

    iget v0, v0, Lcom/google/android/gms/games/ui/bz;->b:I

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/games/ui/ac;->a(Landroid/support/v7/widget/cs;I)V

    :cond_0
    return-void
.end method

.method protected final a(Ljava/util/ArrayList;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "The adapters have already been set"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 77
    iput-object p1, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    .line 79
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v2

    :goto_1
    if-ge v4, v5, :cond_3

    .line 80
    invoke-virtual {p1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ac;

    .line 81
    if-eqz v0, :cond_2

    move v3, v1

    :goto_2
    const-string v6, "The given adapters must be not null"

    invoke-static {v3, v6}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 82
    new-instance v3, Lcom/google/android/gms/games/ui/ca;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gms/games/ui/ca;-><init>(Lcom/google/android/gms/games/ui/bw;Lcom/google/android/gms/games/ui/ac;)V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/ac;->a(Landroid/support/v7/widget/bx;)V

    .line 83
    iput-object p0, v0, Lcom/google/android/gms/games/ui/ac;->g:Lcom/google/android/gms/games/ui/ac;

    .line 84
    iget v3, p0, Lcom/google/android/gms/games/ui/bw;->e:I

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ac;->r()I

    move-result v6

    invoke-static {v3, v6}, Lcom/google/android/gms/games/ui/bw;->e(II)I

    move-result v3

    iput v3, p0, Lcom/google/android/gms/games/ui/bw;->e:I

    .line 93
    if-nez v4, :cond_0

    .line 95
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ac;->u()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/bw;->f:I

    .line 79
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_1
    move v0, v2

    .line 76
    goto :goto_0

    :cond_2
    move v3, v2

    .line 81
    goto :goto_2

    .line 98
    :cond_3
    return-void
.end method

.method public final b(I)J
    .locals 2

    .prologue
    .line 162
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bw;->f(I)Lcom/google/android/gms/games/ui/bz;

    move-result-object v0

    .line 163
    if-eqz v0, :cond_0

    .line 164
    iget-object v1, v0, Lcom/google/android/gms/games/ui/bz;->a:Lcom/google/android/gms/games/ui/ac;

    iget v0, v0, Lcom/google/android/gms/games/ui/bz;->b:I

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/ac;->b(I)J

    move-result-wide v0

    .line 166
    :goto_0
    return-wide v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/ac;->b(I)J

    move-result-wide v0

    goto :goto_0
.end method

.method public b()V
    .locals 4

    .prologue
    .line 226
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ac;

    .line 228
    instance-of v3, v0, Lcom/google/android/gms/games/ui/a;

    if-eqz v3, :cond_0

    .line 229
    check-cast v0, Lcom/google/android/gms/games/ui/a;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/a;->b()V

    .line 226
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 232
    :cond_1
    return-void
.end method

.method final b(Lcom/google/android/gms/games/ui/ac;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 101
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 102
    new-instance v0, Lcom/google/android/gms/games/ui/ca;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/games/ui/ca;-><init>(Lcom/google/android/gms/games/ui/bw;Lcom/google/android/gms/games/ui/ac;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/ui/ac;->a(Landroid/support/v7/widget/bx;)V

    .line 103
    iget v0, p0, Lcom/google/android/gms/games/ui/bw;->e:I

    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/ac;->r()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/bw;->e(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/bw;->e:I

    .line 104
    invoke-virtual {p1}, Lcom/google/android/gms/games/ui/ac;->a()I

    move-result v0

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/games/ui/bw;->c(II)V

    .line 105
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 333
    const/4 v0, 0x1

    return v0
.end method

.method public final p()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_1

    .line 341
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bw;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ac;

    .line 342
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ac;->l()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 343
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ac;->a()I

    move-result v4

    invoke-virtual {v0, v2, v4}, Lcom/google/android/gms/games/ui/ac;->a(II)V

    .line 340
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 346
    :cond_1
    return-void
.end method

.method public final q()Landroid/support/v7/widget/ap;
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bw;->h:Landroid/support/v7/widget/ap;

    return-object v0
.end method

.method public final r()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/google/android/gms/games/ui/bw;->e:I

    return v0
.end method

.method public final u()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/google/android/gms/games/ui/bw;->f:I

    return v0
.end method
