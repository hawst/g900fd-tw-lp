.class public final Lcom/google/android/gms/ads/internal/a/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/a/p;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/ads/internal/p/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;

    invoke-direct {v1}, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;-><init>()V

    const/4 v4, 0x0

    move-object v0, p1

    move v3, v2

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/p/a;->a(Landroid/content/Context;Lcom/google/android/gms/ads/internal/client/AdSizeParcel;ZZLcom/google/android/a/w;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;)Lcom/google/android/gms/ads/internal/p/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/u;->a:Lcom/google/android/gms/ads/internal/p/a;

    .line 36
    return-void
.end method

.method private static a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 39
    invoke-static {}, Lcom/google/android/gms/ads/internal/util/client/a;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-interface {p0}, Ljava/lang/Runnable;->run()V

    .line 44
    :goto_0
    return-void

    .line 42
    :cond_0
    sget-object v0, Lcom/google/android/gms/ads/internal/util/client/a;->a:Landroid/os/Handler;

    invoke-virtual {v0, p0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/u;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->destroy()V

    .line 131
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/a/q;)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/u;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->f()Lcom/google/android/gms/ads/internal/p/c;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/ads/internal/a/y;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/ads/internal/a/y;-><init>(Lcom/google/android/gms/ads/internal/a/u;Lcom/google/android/gms/ads/internal/a/q;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/ads/internal/p/c;->a(Lcom/google/android/gms/ads/internal/p/e;)V

    .line 93
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/a;Lcom/google/android/gms/ads/internal/overlay/i;Lcom/google/android/gms/ads/internal/g/b;Lcom/google/android/gms/ads/internal/overlay/n;Lcom/google/android/gms/ads/internal/g/n;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/u;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->f()Lcom/google/android/gms/ads/internal/p/c;

    move-result-object v0

    new-instance v7, Lcom/google/android/gms/ads/internal/i;

    invoke-direct {v7, v5}, Lcom/google/android/gms/ads/internal/i;-><init>(B)V

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/ads/internal/p/c;->a(Lcom/google/android/gms/ads/internal/a;Lcom/google/android/gms/ads/internal/overlay/i;Lcom/google/android/gms/ads/internal/g/b;Lcom/google/android/gms/ads/internal/overlay/n;ZLcom/google/android/gms/ads/internal/g/n;Lcom/google/android/gms/ads/internal/i;)V

    .line 126
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 57
    const-string v0, "<!DOCTYPE html><html><head><script src=\"%s\"></script></head><body></body></html>"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 58
    new-instance v1, Lcom/google/android/gms/ads/internal/a/w;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/ads/internal/a/w;-><init>(Lcom/google/android/gms/ads/internal/a/u;Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/gms/ads/internal/a/u;->a(Ljava/lang/Runnable;)V

    .line 63
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/u;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->f()Lcom/google/android/gms/ads/internal/p/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/ads/internal/p/c;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 77
    return-void
.end method

.method public final a(Ljava/lang/String;Lorg/json/JSONObject;)V
    .locals 1

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/gms/ads/internal/a/v;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/ads/internal/a/v;-><init>(Lcom/google/android/gms/ads/internal/a/u;Ljava/lang/String;Lorg/json/JSONObject;)V

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/a/u;->a(Ljava/lang/Runnable;)V

    .line 53
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 67
    new-instance v0, Lcom/google/android/gms/ads/internal/a/x;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/ads/internal/a/x;-><init>(Lcom/google/android/gms/ads/internal/a/u;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/a/u;->a(Ljava/lang/Runnable;)V

    .line 72
    return-void
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/u;->a:Lcom/google/android/gms/ads/internal/p/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/p/a;->f()Lcom/google/android/gms/ads/internal/p/c;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/ads/internal/p/c;->b(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 82
    return-void
.end method
