.class public final Lcom/google/android/gms/fitness/service/az;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/fitness/service/az;

.field private static b:Lcom/google/android/gms/fitness/service/az;


# instance fields
.field private final c:Z

.field private final d:Lcom/google/android/gms/fitness/data/Device;

.field private final e:I

.field private final f:Lcom/google/android/gms/common/a/d;

.field private final g:Lcom/google/android/gms/common/a/d;

.field private h:Ljava/lang/String;

.field private i:[Ljava/lang/String;

.field private j:[Ljava/lang/String;

.field private k:[J


# direct methods
.method private constructor <init>(Lcom/google/android/gms/fitness/data/Device;ZLcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;I)V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lcom/google/android/gms/fitness/service/az;->d:Lcom/google/android/gms/fitness/data/Device;

    .line 117
    iput-boolean p2, p0, Lcom/google/android/gms/fitness/service/az;->c:Z

    .line 118
    iput-object p3, p0, Lcom/google/android/gms/fitness/service/az;->f:Lcom/google/android/gms/common/a/d;

    .line 119
    iput-object p4, p0, Lcom/google/android/gms/fitness/service/az;->g:Lcom/google/android/gms/common/a/d;

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/az;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/az;->h:Ljava/lang/String;

    .line 121
    iput p5, p0, Lcom/google/android/gms/fitness/service/az;->e:I

    .line 122
    invoke-direct {p0}, Lcom/google/android/gms/fitness/service/az;->a()V

    .line 123
    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/gms/fitness/service/az;
    .locals 7

    .prologue
    .line 60
    const-class v6, Lcom/google/android/gms/fitness/service/az;

    monitor-enter v6

    :try_start_0
    sget-object v0, Lcom/google/android/gms/fitness/service/az;->a:Lcom/google/android/gms/fitness/service/az;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lcom/google/android/gms/fitness/service/az;

    invoke-static {p0}, Lcom/google/android/gms/fitness/data/Device;->a(Landroid/content/Context;)Lcom/google/android/gms/fitness/data/Device;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "android.hardware.sensor.heartrate.ecg"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    sget-object v3, Lcom/google/android/gms/fitness/g/c;->ao:Lcom/google/android/gms/common/a/d;

    sget-object v4, Lcom/google/android/gms/fitness/g/c;->ap:Lcom/google/android/gms/common/a/d;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/fitness/service/az;-><init>(Lcom/google/android/gms/fitness/data/Device;ZLcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;I)V

    sput-object v0, Lcom/google/android/gms/fitness/service/az;->a:Lcom/google/android/gms/fitness/service/az;

    .line 68
    :cond_0
    sget-object v0, Lcom/google/android/gms/fitness/service/az;->a:Lcom/google/android/gms/fitness/service/az;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v6

    return-object v0

    .line 60
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method private a()V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v1, 0x0

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/az;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 184
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/az;->i:[Ljava/lang/String;

    .line 185
    new-array v0, v1, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/az;->j:[Ljava/lang/String;

    .line 186
    new-array v0, v1, [J

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/az;->k:[J

    .line 214
    :cond_0
    return-void

    .line 190
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/az;->h:Ljava/lang/String;

    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 191
    array-length v0, v2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/az;->i:[Ljava/lang/String;

    .line 192
    array-length v0, v2

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/az;->j:[Ljava/lang/String;

    .line 193
    array-length v0, v2

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/az;->k:[J

    move v0, v1

    .line 195
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 196
    aget-object v3, v2, v0

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 197
    array-length v4, v3

    packed-switch v4, :pswitch_data_0

    .line 211
    const-string v3, "Invalid rule #%d: %s"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    aget-object v5, v2, v0

    aput-object v5, v4, v8

    invoke-static {v3, v4}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 195
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 200
    :pswitch_0
    iget-object v4, p0, Lcom/google/android/gms/fitness/service/az;->i:[Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v5, v4, v0

    .line 201
    iget-object v4, p0, Lcom/google/android/gms/fitness/service/az;->j:[Ljava/lang/String;

    aget-object v5, v3, v1

    aput-object v5, v4, v0

    .line 202
    iget-object v4, p0, Lcom/google/android/gms/fitness/service/az;->k:[J

    aget-object v3, v3, v8

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    aput-wide v6, v4, v0

    goto :goto_1

    .line 206
    :pswitch_1
    iget-object v4, p0, Lcom/google/android/gms/fitness/service/az;->i:[Ljava/lang/String;

    aget-object v5, v3, v1

    aput-object v5, v4, v0

    .line 207
    iget-object v4, p0, Lcom/google/android/gms/fitness/service/az;->j:[Ljava/lang/String;

    aget-object v5, v3, v8

    aput-object v5, v4, v0

    .line 208
    iget-object v4, p0, Lcom/google/android/gms/fitness/service/az;->k:[J

    aget-object v3, v3, v9

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    aput-wide v6, v4, v0

    goto :goto_1

    .line 197
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static declared-synchronized b(Landroid/content/Context;)Lcom/google/android/gms/fitness/service/az;
    .locals 7

    .prologue
    .line 74
    const-class v6, Lcom/google/android/gms/fitness/service/az;

    monitor-enter v6

    :try_start_0
    sget-object v0, Lcom/google/android/gms/fitness/service/az;->b:Lcom/google/android/gms/fitness/service/az;

    if-nez v0, :cond_0

    .line 75
    new-instance v0, Lcom/google/android/gms/fitness/service/az;

    invoke-static {p0}, Lcom/google/android/gms/fitness/data/Device;->a(Landroid/content/Context;)Lcom/google/android/gms/fitness/data/Device;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "android.hardware.sensor.heartrate.ecg"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    sget-object v3, Lcom/google/android/gms/fitness/g/c;->aq:Lcom/google/android/gms/common/a/d;

    sget-object v4, Lcom/google/android/gms/fitness/g/c;->ar:Lcom/google/android/gms/common/a/d;

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/fitness/service/az;-><init>(Lcom/google/android/gms/fitness/data/Device;ZLcom/google/android/gms/common/a/d;Lcom/google/android/gms/common/a/d;I)V

    sput-object v0, Lcom/google/android/gms/fitness/service/az;->b:Lcom/google/android/gms/fitness/service/az;

    .line 82
    :cond_0
    sget-object v0, Lcom/google/android/gms/fitness/service/az;->b:Lcom/google/android/gms/fitness/service/az;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v6

    return-object v0

    .line 74
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/DataType;)J
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 131
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/az;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/fitness/service/az;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/az;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/az;->h:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/fitness/service/az;->a()V

    .line 134
    :cond_0
    iget v0, p0, Lcom/google/android/gms/fitness/service/az;->e:I

    if-ne v0, v2, :cond_1

    if-eqz p1, :cond_1

    sget-object v0, Lcom/google/android/gms/fitness/data/DataType;->k:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->a()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/fitness/data/DataType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/az;->d:Lcom/google/android/gms/fitness/data/Device;

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->f()Lcom/google/android/gms/fitness/data/Device;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/fitness/data/Device;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/az;->d:Lcom/google/android/gms/fitness/data/Device;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Device;->e()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_1
    :pswitch_0
    move v0, v1

    :goto_0
    if-eqz v0, :cond_6

    .line 135
    const-wide/16 v0, -0x1

    .line 147
    :goto_1
    monitor-exit p0

    return-wide v0

    .line 134
    :pswitch_1
    :try_start_1
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->ag:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v2

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/gms/fitness/service/az;->c:Z

    if-eqz v0, :cond_4

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->af:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v2

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->ae:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    move v0, v2

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v0, v1

    .line 139
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/az;->k:[J

    array-length v1, v1

    if-ge v0, v1, :cond_9

    .line 141
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/az;->j:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p2}, Lcom/google/android/gms/fitness/data/DataType;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/az;->i:[Ljava/lang/String;

    aget-object v1, v1, v0

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/az;->i:[Ljava/lang/String;

    aget-object v1, v1, v0

    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 144
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/fitness/service/az;->k:[J

    aget-wide v0, v1, v0

    goto :goto_1

    .line 139
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 147
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/az;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    goto :goto_1

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 134
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
