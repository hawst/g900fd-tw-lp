.class final Lcom/google/android/gms/audiomodem/b/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/os/IBinder;

.field final synthetic b:Lcom/google/android/gms/audiomodem/a/q;

.field final synthetic c:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

.field final synthetic d:Lcom/google/android/gms/audiomodem/a/e;

.field final synthetic e:Lcom/google/android/gms/audiomodem/b/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/audiomodem/b/a;Landroid/os/IBinder;Lcom/google/android/gms/audiomodem/a/q;Lcom/google/android/gms/audiomodem/TokenReceiver$Params;Lcom/google/android/gms/audiomodem/a/e;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/b/b;->e:Lcom/google/android/gms/audiomodem/b/a;

    iput-object p2, p0, Lcom/google/android/gms/audiomodem/b/b;->a:Landroid/os/IBinder;

    iput-object p3, p0, Lcom/google/android/gms/audiomodem/b/b;->b:Lcom/google/android/gms/audiomodem/a/q;

    iput-object p4, p0, Lcom/google/android/gms/audiomodem/b/b;->c:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    iput-object p5, p0, Lcom/google/android/gms/audiomodem/b/b;->d:Lcom/google/android/gms/audiomodem/a/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/b/b;->e:Lcom/google/android/gms/audiomodem/b/a;

    invoke-static {v0}, Lcom/google/android/gms/audiomodem/b/a;->a(Lcom/google/android/gms/audiomodem/b/a;)Lcom/google/android/gms/audiomodem/n;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/audiomodem/b/b;->a:Landroid/os/IBinder;

    iget-object v3, p0, Lcom/google/android/gms/audiomodem/b/b;->b:Lcom/google/android/gms/audiomodem/a/q;

    iget-object v4, p0, Lcom/google/android/gms/audiomodem/b/b;->c:Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    iget-object v5, p0, Lcom/google/android/gms/audiomodem/b/b;->d:Lcom/google/android/gms/audiomodem/a/e;

    invoke-virtual {v2}, Lcom/google/android/gms/audiomodem/n;->a()Z

    move-result v6

    if-nez v6, :cond_1

    const/16 v0, 0x1964

    invoke-static {v5, v0}, Lcom/google/android/gms/audiomodem/n;->a(Lcom/google/android/gms/audiomodem/a/e;I)V

    .line 59
    :cond_0
    :goto_0
    return-void

    .line 58
    :cond_1
    new-instance v6, Lcom/google/android/gms/audiomodem/q;

    invoke-direct {v6, v3}, Lcom/google/android/gms/audiomodem/q;-><init>(Lcom/google/android/gms/audiomodem/a/q;)V

    iget-object v3, v2, Lcom/google/android/gms/audiomodem/n;->a:Lcom/google/android/gms/audiomodem/a;

    invoke-virtual {v3, v6, v0}, Lcom/google/android/gms/audiomodem/a;->a(Lcom/google/android/gms/audiomodem/aq;Landroid/os/IBinder;)Lcom/google/android/gms/audiomodem/c;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    invoke-virtual {v4}, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->c()[Lcom/google/android/gms/audiomodem/Encoding;

    move-result-object v4

    array-length v7, v4

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_2

    aget-object v8, v4, v0

    new-instance v9, Lcom/google/android/gms/audiomodem/bi;

    invoke-direct {v9, v6, v8}, Lcom/google/android/gms/audiomodem/bi;-><init>(Lcom/google/android/gms/audiomodem/q;Lcom/google/android/gms/audiomodem/Encoding;)V

    invoke-interface {v3, v9}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/audiomodem/n;->a(Lcom/google/android/gms/audiomodem/q;Ljava/util/Set;)V

    invoke-static {v5, v1}, Lcom/google/android/gms/audiomodem/n;->a(Lcom/google/android/gms/audiomodem/a/e;I)V

    goto :goto_0
.end method
