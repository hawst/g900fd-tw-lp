.class final Lcom/google/android/gms/games/ui/common/players/f;
.super Lcom/google/android/gms/games/ui/cx;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final k:Landroid/view/View;

.field private final n:Landroid/widget/TextView;

.field private final o:Landroid/widget/TextView;

.field private final p:Landroid/widget/TextView;

.field private final q:Landroid/widget/ProgressBar;

.field private final r:Landroid/widget/TextView;

.field private final s:Landroid/widget/TextView;

.field private final t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

.field private final u:Landroid/view/View;

.field private final v:Landroid/widget/TextView;

.field private final w:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 130
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/cx;-><init>(Landroid/view/View;)V

    .line 132
    sget v0, Lcom/google/android/gms/j;->oL:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->k:Landroid/view/View;

    .line 134
    sget v0, Lcom/google/android/gms/j;->oO:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->n:Landroid/widget/TextView;

    .line 135
    sget v0, Lcom/google/android/gms/j;->oQ:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->o:Landroid/widget/TextView;

    .line 137
    sget v0, Lcom/google/android/gms/j;->mx:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->p:Landroid/widget/TextView;

    .line 138
    sget v0, Lcom/google/android/gms/j;->my:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->q:Landroid/widget/ProgressBar;

    .line 140
    sget v0, Lcom/google/android/gms/j;->S:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->r:Landroid/widget/TextView;

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->r:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->r:Landroid/widget/TextView;

    const/high16 v1, 0x3fc00000    # 1.5f

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/widget/TextView;F)V

    .line 144
    sget v0, Lcom/google/android/gms/j;->ab:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->s:Landroid/widget/TextView;

    .line 146
    sget v0, Lcom/google/android/gms/j;->bD:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/g;->af:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->c(I)V

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/g;->ag:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(I)V

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/g;->ae:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->e(I)V

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    const/16 v1, 0x10

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setImportantForAccessibility(I)V

    .line 155
    :cond_0
    sget v0, Lcom/google/android/gms/j;->cb:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->u:Landroid/view/View;

    .line 156
    sget v0, Lcom/google/android/gms/j;->dn:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->v:Landroid/widget/TextView;

    .line 157
    sget v0, Lcom/google/android/gms/j;->do:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->w:Landroid/widget/TextView;

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->a:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 160
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/ui/ac;I)V
    .locals 20

    .prologue
    .line 164
    invoke-super/range {p0 .. p2}, Lcom/google/android/gms/games/ui/cx;->a(Lcom/google/android/gms/games/ui/ac;I)V

    .line 166
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/players/f;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v2, Lcom/google/android/gms/games/ui/common/players/d;

    .line 167
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/players/f;->l:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 169
    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v5

    .line 170
    const/4 v3, 0x1

    .line 171
    if-eqz v5, :cond_0

    .line 172
    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v3

    .line 174
    :cond_0
    invoke-static {v4, v3}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/res/Resources;I)I

    move-result v3

    const/4 v4, 0x3

    new-array v4, v4, [F

    invoke-static {v3, v4}, Landroid/graphics/Color;->colorToHSV(I[F)V

    const/4 v3, 0x2

    aget v6, v4, v3

    const/high16 v7, 0x3f000000    # 0.5f

    mul-float/2addr v6, v7

    aput v6, v4, v3

    invoke-static {v4}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v3

    .line 175
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/common/players/f;->k:Landroid/view/View;

    invoke-virtual {v4, v3}, Landroid/view/View;->setBackgroundColor(I)V

    .line 177
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/players/f;->n:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/games/Player;->v_()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 179
    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->m()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 180
    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->m()Ljava/lang/String;

    move-result-object v3

    .line 181
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/common/players/f;->o:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 182
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/players/f;->o:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/Player;)V

    .line 189
    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x15

    invoke-static {v4}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v4

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a()Lcom/google/android/gms/common/images/internal/LoadingImageView;

    move-result-object v4

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "avatar"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setTransitionName(Ljava/lang/String;)V

    .line 191
    :cond_1
    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/players/d;->b(Lcom/google/android/gms/games/ui/common/players/d;)I

    move-result v2

    if-nez v2, :cond_c

    .line 192
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/players/f;->l:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/players/f;->r:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/players/f;->s:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    if-eqz v5, :cond_6

    const/4 v2, 0x1

    move v3, v2

    :goto_1
    if-eqz v3, :cond_3

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/ui/common/players/f;->q:Landroid/widget/ProgressBar;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/games/ui/common/players/f;->p:Landroid/widget/TextView;

    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v8

    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevelInfo;->e()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v9

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {v2}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v10

    if-nez v5, :cond_7

    const/4 v2, 0x1

    :goto_2
    invoke-static {v4, v2}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/res/Resources;I)I

    move-result v11

    invoke-virtual {v9, v8}, Lcom/google/android/gms/games/PlayerLevel;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    invoke-virtual {v8}, Lcom/google/android/gms/games/PlayerLevel;->d()J

    move-result-wide v12

    invoke-virtual {v8}, Lcom/google/android/gms/games/PlayerLevel;->c()J

    move-result-wide v14

    sub-long/2addr v12, v14

    long-to-int v9, v12

    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevelInfo;->b()J

    move-result-wide v12

    invoke-virtual {v8}, Lcom/google/android/gms/games/PlayerLevel;->c()J

    move-result-wide v14

    sub-long/2addr v12, v14

    long-to-int v8, v12

    new-instance v2, Landroid/graphics/drawable/ShapeDrawable;

    invoke-direct {v2}, Landroid/graphics/drawable/ShapeDrawable;-><init>()V

    invoke-virtual {v2}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v12

    invoke-virtual {v12, v11}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v12, Landroid/graphics/drawable/ClipDrawable;

    const/4 v13, 0x3

    const/4 v14, 0x1

    invoke-direct {v12, v2, v13, v14}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    const/4 v2, 0x0

    if-lez v9, :cond_2

    const-wide v14, 0x40c3880000000000L    # 10000.0

    int-to-double v0, v8

    move-wide/from16 v16, v0

    int-to-double v0, v9

    move-wide/from16 v18, v0

    div-double v16, v16, v18

    mul-double v14, v14, v16

    double-to-int v2, v14

    :cond_2
    invoke-virtual {v12, v2}, Landroid/graphics/drawable/ClipDrawable;->setLevel(I)Z

    invoke-virtual {v6, v12}, Landroid/widget/ProgressBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v6, v9}, Landroid/widget/ProgressBar;->setMax(I)V

    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/widget/ProgressBar;->setProgress(I)V

    invoke-virtual {v6, v8}, Landroid/widget/ProgressBar;->setProgress(I)V

    sget v2, Lcom/google/android/gms/p;->lR:I

    const/4 v12, 0x1

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    sub-int v8, v9, v8

    int-to-long v8, v8

    invoke-virtual {v10, v8, v9}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v12, v13

    invoke-virtual {v4, v2, v12}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :cond_3
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/players/f;->u:Landroid/view/View;

    const/4 v6, 0x0

    invoke-virtual {v2, v6}, Landroid/view/View;->setVisibility(I)V

    if-eqz v3, :cond_4

    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevelInfo;->c()J

    move-result-wide v2

    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevelInfo;->c()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    cmp-long v6, v6, v8

    if-lez v6, :cond_9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v2

    sget-object v2, Lcom/google/android/gms/games/ui/n;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v6, v2

    if-gtz v2, :cond_9

    const/4 v2, 0x1

    :goto_4
    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/players/f;->v:Landroid/widget/TextView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/games/ui/common/players/f;->w:Landroid/widget/TextView;

    new-instance v7, Landroid/graphics/drawable/ShapeDrawable;

    new-instance v2, Lcom/google/android/gms/games/ui/widget/a;

    invoke-direct {v2}, Lcom/google/android/gms/games/ui/widget/a;-><init>()V

    invoke-direct {v7, v2}, Landroid/graphics/drawable/ShapeDrawable;-><init>(Landroid/graphics/drawable/shapes/Shape;)V

    invoke-virtual {v7}, Landroid/graphics/drawable/ShapeDrawable;->getPaint()Landroid/graphics/Paint;

    move-result-object v8

    if-nez v5, :cond_a

    const/4 v2, 0x1

    :goto_5
    invoke-static {v4, v2}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/res/Resources;I)I

    move-result v2

    invoke-virtual {v8, v2}, Landroid/graphics/Paint;->setColor(I)V

    const/16 v2, 0x10

    invoke-static {v2}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_6
    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v2, Lcom/google/android/gms/p;->lQ:I

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setText(I)V

    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 196
    :cond_4
    :goto_7
    return-void

    .line 184
    :cond_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/players/f;->o:Landroid/widget/TextView;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_0

    .line 192
    :cond_6
    const/4 v2, 0x0

    move v3, v2

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v2

    goto/16 :goto_2

    :cond_8
    sget v2, Lcom/google/android/gms/p;->lP:I

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevelInfo;->b()J

    move-result-wide v12

    invoke-virtual {v10, v12, v13}, Ljava/text/NumberFormat;->format(J)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v4, v2, v8}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v7, v11}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v2, 0x0

    invoke-virtual {v7, v2}, Landroid/widget/TextView;->setVisibility(I)V

    const/16 v2, 0x8

    invoke-virtual {v6, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto/16 :goto_3

    :cond_9
    const/4 v2, 0x0

    goto/16 :goto_4

    :cond_a
    invoke-virtual {v5}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v2

    goto :goto_5

    :cond_b
    invoke-virtual {v3, v7}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_6

    .line 194
    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/players/f;->u:Landroid/view/View;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/ui/common/players/f;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v2, Lcom/google/android/gms/games/ui/common/players/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/ui/common/players/f;->r:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/players/d;->c(Lcom/google/android/gms/games/ui/common/players/d;)Z

    move-result v3

    if-eqz v3, :cond_d

    const/16 v3, 0x8

    :goto_8
    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/ui/common/players/f;->s:Landroid/widget/TextView;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/common/players/d;->c(Lcom/google/android/gms/games/ui/common/players/d;)Z

    move-result v2

    if-eqz v2, :cond_e

    const/4 v2, 0x0

    :goto_9
    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_7

    :cond_d
    const/4 v3, 0x0

    goto :goto_8

    :cond_e
    const/16 v2, 0x8

    goto :goto_9
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/f;->m:Lcom/google/android/gms/games/ui/ac;

    check-cast v0, Lcom/google/android/gms/games/ui/common/players/d;

    .line 248
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->d(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/ui/common/players/e;

    move-result-object v1

    if-nez v1, :cond_0

    .line 260
    :goto_0
    return-void

    .line 252
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/f;->r:Landroid/widget/TextView;

    if-ne p1, v1, :cond_1

    .line 253
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->d(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/ui/common/players/e;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/common/players/e;->a(Lcom/google/android/gms/games/Player;)V

    goto :goto_0

    .line 255
    :cond_1
    new-instance v1, Landroid/util/Pair;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/f;->t:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a()Lcom/google/android/gms/common/images/internal/LoadingImageView;

    move-result-object v2

    const-string v3, "avatar"

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 258
    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->d(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/ui/common/players/e;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/ui/common/players/d;)Lcom/google/android/gms/games/Player;

    move-result-object v0

    const/4 v3, 0x1

    new-array v3, v3, [Landroid/util/Pair;

    const/4 v4, 0x0

    aput-object v1, v3, v4

    invoke-interface {v2, v0, v3}, Lcom/google/android/gms/games/ui/common/players/e;->a(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    goto :goto_0
.end method
