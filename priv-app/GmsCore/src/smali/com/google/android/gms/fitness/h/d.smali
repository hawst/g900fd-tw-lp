.class public final Lcom/google/android/gms/fitness/h/d;
.super Lcom/google/android/gms/fitness/h/f;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Set;

.field private static b:Lcom/google/android/gms/fitness/h/f;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/location/b/at;

.field private final e:Ljava/util/List;

.field private final f:Lcom/google/android/gms/fitness/d/h;

.field private final g:Lcom/google/android/gms/fitness/l/a/b;

.field private h:Landroid/os/Handler;

.field private i:Landroid/os/Handler;

.field private j:Lcom/google/android/gms/fitness/service/aq;

.field private k:Lcom/google/k/k/a/ai;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/google/android/gms/fitness/data/DataType;->b:Lcom/google/android/gms/fitness/data/DataType;

    invoke-static {v0}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/h/d;->a:Ljava/util/Set;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 14

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 102
    invoke-direct {p0}, Lcom/google/android/gms/fitness/h/f;-><init>()V

    .line 103
    iput-object p1, p0, Lcom/google/android/gms/fitness/h/d;->c:Landroid/content/Context;

    .line 104
    new-instance v0, Lcom/google/android/location/b/at;

    iget-object v1, p0, Lcom/google/android/gms/fitness/h/d;->c:Landroid/content/Context;

    const-class v2, Lcom/google/android/gms/fitness/service/BrokeredFitnessService;

    invoke-direct {v0, v1, v2}, Lcom/google/android/location/b/at;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/h/d;->d:Lcom/google/android/location/b/at;

    .line 105
    new-instance v0, Lcom/google/android/gms/fitness/l/a/b;

    iget-object v1, p0, Lcom/google/android/gms/fitness/h/d;->c:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/h/d;->d()Landroid/os/Handler;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/fitness/l/a/b;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/h/d;->g:Lcom/google/android/gms/fitness/l/a/b;

    .line 108
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/h/d;->e:Ljava/util/List;

    .line 109
    iget-object v1, p0, Lcom/google/android/gms/fitness/h/d;->c:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/fitness/h/d;->d:Lcom/google/android/location/b/at;

    const-string v0, "sensor"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/hardware/SensorManager;

    new-instance v5, Lcom/google/android/gms/fitness/sensors/local/h;

    sget-object v0, Lcom/google/android/gms/fitness/h/d;->a:Ljava/util/Set;

    invoke-direct {v5, v0}, Lcom/google/android/gms/fitness/sensors/local/h;-><init>(Ljava/util/Set;)V

    const-class v0, Lcom/google/android/gms/fitness/sensors/local/d;

    invoke-static {v0}, Ljava/util/EnumSet;->allOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v2

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->Q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/fitness/sensors/local/d;->c:Lcom/google/android/gms/fitness/sensors/local/d;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_0
    new-instance v0, Lcom/google/android/gms/fitness/sensors/local/a;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/fitness/sensors/local/a;-><init>(Landroid/content/Context;Ljava/util/Set;Landroid/hardware/SensorManager;Lcom/google/android/location/b/at;Lcom/google/android/gms/fitness/sensors/local/h;)V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->k:Lcom/google/android/gms/fitness/data/DataType;

    new-instance v4, Lcom/google/android/gms/fitness/sensors/sample/d;

    const/16 v8, 0xf

    const/4 v9, 0x5

    sget-object v10, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v12, 0x3c

    invoke-virtual {v10, v12, v13}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v10

    invoke-direct {v4, v8, v9, v10, v11}, Lcom/google/android/gms/fitness/sensors/sample/d;-><init>(IIJ)V

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/fitness/sensors/sample/c;->a(Landroid/content/Context;Lcom/google/android/gms/fitness/sensors/a;Ljava/util/Map;)Lcom/google/android/gms/fitness/sensors/sample/c;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/fitness/sensors/e/f;

    const-string v1, "Local HW"

    invoke-direct {v2, v0, v1}, Lcom/google/android/gms/fitness/sensors/e/f;-><init>(Lcom/google/android/gms/fitness/sensors/a;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/fitness/sensors/local/i;

    sget-object v0, Lcom/google/android/gms/fitness/h/d;->a:Ljava/util/Set;

    new-instance v3, Lcom/google/android/gms/fitness/h/e;

    invoke-direct {v3, p0}, Lcom/google/android/gms/fitness/h/e;-><init>(Lcom/google/android/gms/fitness/h/d;)V

    invoke-direct {v1, v0, v2, v5, v3}, Lcom/google/android/gms/fitness/sensors/local/i;-><init>(Ljava/util/Set;Lcom/google/android/gms/fitness/sensors/a;Lcom/google/android/gms/fitness/sensors/local/h;Lcom/google/k/a/bc;)V

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->c:Landroid/content/Context;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lcom/google/android/gms/fitness/sensors/activity/b;

    invoke-static {v0}, Lcom/google/android/gms/fitness/sensors/activity/a;->a(Landroid/content/Context;)Lcom/google/android/gms/fitness/sensors/activity/a;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/google/android/gms/fitness/sensors/activity/b;-><init>(Lcom/google/android/gms/fitness/sensors/activity/a;)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/google/android/gms/fitness/sensors/d/d;

    invoke-direct {v2, v0}, Lcom/google/android/gms/fitness/sensors/d/d;-><init>(Landroid/content/Context;)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v2, Lcom/google/android/gms/fitness/data/DataType;->b:Lcom/google/android/gms/fitness/data/DataType;

    invoke-interface {v1, v2}, Lcom/google/android/gms/fitness/sensors/a;->b(Lcom/google/android/gms/fitness/data/DataType;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Lcom/google/android/gms/fitness/sensors/c/f;

    invoke-virtual {p0}, Lcom/google/android/gms/fitness/h/d;->b()Landroid/os/Handler;

    move-result-object v4

    invoke-direct {v2, v0, v4}, Lcom/google/android/gms/fitness/sensors/c/f;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v2, Lcom/google/android/gms/fitness/sensors/e/f;

    new-instance v0, Lcom/google/android/gms/fitness/sensors/b/b;

    invoke-direct {v0, v3}, Lcom/google/android/gms/fitness/sensors/b/b;-><init>(Ljava/util/List;)V

    const-string v3, "Local SW"

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/fitness/sensors/e/f;-><init>(Lcom/google/android/gms/fitness/sensors/a;Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 113
    iget-object v3, p0, Lcom/google/android/gms/fitness/h/d;->e:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/gms/fitness/h/d;->c:Landroid/content/Context;

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lcom/google/android/gms/fitness/data/DataType;->b:Lcom/google/android/gms/fitness/data/DataType;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/sensors/a;->b(Lcom/google/android/gms/fitness/data/DataType;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v6

    :goto_0
    if-eqz v0, :cond_4

    move-object v0, v1

    :goto_1
    new-array v8, v6, [Lcom/google/android/gms/fitness/sensors/c/b;

    new-instance v9, Lcom/google/android/gms/fitness/sensors/c/a;

    invoke-direct {v9, v4}, Lcom/google/android/gms/fitness/sensors/c/a;-><init>(Landroid/content/Context;)V

    aput-object v9, v8, v7

    invoke-static {v8}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    new-instance v8, Lcom/google/android/gms/fitness/sensors/c/c;

    sget-boolean v9, Lcom/google/android/gms/fitness/sensors/c/c;->b:Z

    invoke-direct {v8, v0, v4, v9}, Lcom/google/android/gms/fitness/sensors/c/c;-><init>(Lcom/google/android/gms/fitness/sensors/a;Ljava/util/List;Z)V

    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/fitness/sensors/e/f;

    new-instance v4, Lcom/google/android/gms/fitness/sensors/b/b;

    invoke-direct {v4, v5}, Lcom/google/android/gms/fitness/sensors/b/b;-><init>(Ljava/util/List;)V

    const-string v5, "Derived"

    invoke-direct {v0, v4, v5}, Lcom/google/android/gms/fitness/sensors/e/f;-><init>(Lcom/google/android/gms/fitness/sensors/a;Ljava/lang/String;)V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->J:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 117
    new-instance v0, Lcom/google/android/gms/fitness/sensors/g/j;

    iget-object v3, p0, Lcom/google/android/gms/fitness/h/d;->c:Landroid/content/Context;

    new-instance v4, Lcom/google/android/gms/fitness/sensors/b/b;

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/android/gms/fitness/sensors/a;

    aput-object v1, v5, v7

    aput-object v2, v5, v6

    invoke-direct {v4, v5}, Lcom/google/android/gms/fitness/sensors/b/b;-><init>([Lcom/google/android/gms/fitness/sensors/a;)V

    invoke-direct {v0, v3, v4}, Lcom/google/android/gms/fitness/sensors/g/j;-><init>(Landroid/content/Context;Lcom/google/android/gms/fitness/sensors/a;)V

    .line 120
    invoke-static {v0}, Lcom/google/android/location/wearable/LocationWearableListenerService;->a(Ljava/lang/Object;)V

    .line 121
    iget-object v1, p0, Lcom/google/android/gms/fitness/h/d;->e:Ljava/util/List;

    new-instance v2, Lcom/google/android/gms/fitness/sensors/e/f;

    const-string v3, "Wear"

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/fitness/sensors/e/f;-><init>(Lcom/google/android/gms/fitness/sensors/a;Ljava/lang/String;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->e:Ljava/util/List;

    new-instance v1, Lcom/google/android/gms/fitness/sensors/e/f;

    new-instance v2, Lcom/google/android/gms/fitness/sensors/f/a;

    iget-object v3, p0, Lcom/google/android/gms/fitness/h/d;->c:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/google/android/gms/fitness/sensors/f/a;-><init>(Landroid/content/Context;)V

    const-string v3, "App"

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/fitness/sensors/e/f;-><init>(Lcom/google/android/gms/fitness/sensors/a;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 126
    invoke-static {p1}, Lcom/google/android/gms/fitness/data/Device;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 127
    new-instance v0, Lcom/google/android/gms/fitness/d/t;

    iget-object v1, p0, Lcom/google/android/gms/fitness/h/d;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/fitness/d/t;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/h/d;->f:Lcom/google/android/gms/fitness/d/h;

    .line 131
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->f:Lcom/google/android/gms/fitness/d/h;

    invoke-static {v0}, Lcom/google/android/location/wearable/LocationWearableListenerService;->a(Ljava/lang/Object;)V

    .line 132
    return-void

    :cond_3
    move v0, v7

    .line 113
    goto/16 :goto_0

    :cond_4
    move-object v0, v2

    goto/16 :goto_1

    .line 129
    :cond_5
    new-instance v0, Lcom/google/android/gms/fitness/d/j;

    iget-object v1, p0, Lcom/google/android/gms/fitness/h/d;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/fitness/d/j;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/h/d;->f:Lcom/google/android/gms/fitness/d/h;

    goto :goto_2
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/gms/fitness/h/f;
    .locals 2

    .prologue
    .line 95
    const-class v1, Lcom/google/android/gms/fitness/h/d;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/fitness/h/d;->b:Lcom/google/android/gms/fitness/h/f;

    if-nez v0, :cond_0

    .line 96
    new-instance v0, Lcom/google/android/gms/fitness/h/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/fitness/h/d;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/fitness/h/d;->b:Lcom/google/android/gms/fitness/h/f;

    .line 98
    :cond_0
    sget-object v0, Lcom/google/android/gms/fitness/h/d;->b:Lcom/google/android/gms/fitness/h/f;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a(Landroid/os/Handler;)Lcom/google/android/gms/fitness/service/aq;
    .locals 2

    .prologue
    .line 194
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->j:Lcom/google/android/gms/fitness/service/aq;

    if-nez v0, :cond_0

    .line 195
    new-instance v0, Lcom/google/android/gms/fitness/service/aq;

    iget-object v1, p0, Lcom/google/android/gms/fitness/h/d;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/fitness/service/aq;-><init>(Landroid/content/Context;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/h/d;->j:Lcom/google/android/gms/fitness/service/aq;

    .line 197
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->j:Lcom/google/android/gms/fitness/service/aq;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 194
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Ljava/util/List;
    .locals 1

    .prologue
    .line 153
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->e:Ljava/util/List;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 202
    invoke-static {p1}, Lcom/google/android/location/b/at;->a(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->d:Lcom/google/android/location/b/at;

    invoke-virtual {v0, p1}, Lcom/google/android/location/b/at;->c(Landroid/content/Intent;)V

    .line 205
    :cond_0
    return-void
.end method

.method public final declared-synchronized b()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->h:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 159
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "FitnessServiceForeground"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 160
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/HandlerThread;->setDaemon(Z)V

    .line 161
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 162
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/gms/fitness/h/d;->h:Landroid/os/Handler;

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->h:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()Lcom/google/k/k/a/ai;
    .locals 3

    .prologue
    .line 169
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->k:Lcom/google/k/k/a/ai;

    if-nez v0, :cond_0

    .line 170
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/h/d;->b()Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/fitness/f/f;

    const/4 v2, 0x0

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/fitness/f/f;-><init>(Landroid/os/Handler;B)V

    invoke-static {v1}, Lcom/google/k/k/a/ak;->a(Ljava/util/concurrent/ExecutorService;)Lcom/google/k/k/a/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/h/d;->k:Lcom/google/k/k/a/ai;

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->k:Lcom/google/k/k/a/ai;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Landroid/os/Handler;
    .locals 2

    .prologue
    .line 177
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->i:Landroid/os/Handler;

    if-nez v0, :cond_0

    .line 178
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "FitnessServiceBackground"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 179
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/HandlerThread;->setDaemon(Z)V

    .line 180
    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/os/HandlerThread;->setPriority(I)V

    .line 181
    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 182
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/gms/fitness/h/d;->i:Landroid/os/Handler;

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->i:Landroid/os/Handler;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 177
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()Lcom/google/android/gms/fitness/d/h;
    .locals 1

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->f:Lcom/google/android/gms/fitness/d/h;

    return-object v0
.end method

.method public final f()Lcom/google/android/gms/fitness/l/a/b;
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/gms/fitness/h/d;->g:Lcom/google/android/gms/fitness/l/a/b;

    return-object v0
.end method
