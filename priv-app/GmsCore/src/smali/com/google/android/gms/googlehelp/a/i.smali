.class public abstract Lcom/google/android/gms/googlehelp/a/i;
.super Lcom/google/android/gms/googlehelp/a/j;
.source "SourceFile"


# instance fields
.field protected final f:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field private i:[B


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Landroid/accounts/Account;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 7

    .prologue
    .line 63
    const/4 v3, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/googlehelp/a/j;-><init>(Landroid/content/Context;Landroid/accounts/Account;ILjava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 64
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/a/i;->f:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 65
    return-void
.end method

.method private t()V
    .locals 6

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/i;->i:[B

    if-eqz v0, :cond_0

    .line 97
    :goto_0
    return-void

    .line 89
    :cond_0
    new-instance v1, Lcom/google/android/gms/googlehelp/a/l;

    invoke-direct {v1}, Lcom/google/android/gms/googlehelp/a/l;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/i;->g:Landroid/content/Context;

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/i;->f:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->b:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/googlehelp/a/i;->a(Lcom/google/android/gms/googlehelp/a/l;)V

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->a:Landroid/content/Context;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The context is null, but must be set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->b:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The app package name is empty, but must be set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->b:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "The HelpConfig is null, but must be set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    new-instance v2, Lcom/google/android/gms/googlehelp/e/k;

    invoke-direct {v2}, Lcom/google/android/gms/googlehelp/e/k;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/a/l;->a()Lcom/google/android/gms/googlehelp/e/l;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/googlehelp/e/k;->a:Lcom/google/android/gms/googlehelp/e/l;

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->a:Landroid/content/Context;

    const-string v3, "phone"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    new-instance v3, Lcom/google/android/gms/googlehelp/e/m;

    invoke-direct {v3}, Lcom/google/android/gms/googlehelp/e/m;-><init>()V

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/googlehelp/e/m;->d:Ljava/lang/String;

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/googlehelp/e/m;->b:Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/googlehelp/e/m;->a:Ljava/lang/String;

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/googlehelp/e/m;->c:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->e:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/googlehelp/e/m;->e:Ljava/lang/String;

    :cond_4
    iput-object v3, v2, Lcom/google/android/gms/googlehelp/e/k;->b:Lcom/google/android/gms/googlehelp/e/m;

    new-instance v3, Lcom/google/android/gms/googlehelp/e/o;

    invoke-direct {v3}, Lcom/google/android/gms/googlehelp/e/o;-><init>()V

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->b:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->d()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->b:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/googlehelp/e/o;->c:Ljava/lang/String;

    :cond_5
    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->b:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/googlehelp/e/o;->d:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->b:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->i()Ljava/util/List;

    move-result-object v0

    const/4 v4, 0x0

    new-array v4, v4, [Lcom/google/android/gms/googlehelp/e/n;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/googlehelp/e/n;

    iput-object v0, v3, Lcom/google/android/gms/googlehelp/e/o;->e:[Lcom/google/android/gms/googlehelp/e/n;

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->c:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/android/gms/googlehelp/e/o;->a:Ljava/lang/String;

    :cond_6
    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->d:Ljava/util/List;

    if-eqz v0, :cond_7

    new-instance v4, Lcom/google/android/gms/googlehelp/e/j;

    invoke-direct {v4}, Lcom/google/android/gms/googlehelp/e/j;-><init>()V

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->d:Ljava/util/List;

    iget-object v5, v1, Lcom/google/android/gms/googlehelp/a/l;->d:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Lcom/google/android/gms/googlehelp/e/n;

    invoke-interface {v0, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/googlehelp/e/n;

    iput-object v0, v4, Lcom/google/android/gms/googlehelp/e/j;->a:[Lcom/google/android/gms/googlehelp/e/n;

    iput-object v4, v3, Lcom/google/android/gms/googlehelp/e/o;->f:Lcom/google/android/gms/googlehelp/e/j;

    :cond_7
    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/android/gms/googlehelp/e/h;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/e/h;-><init>()V

    iget-object v4, v1, Lcom/google/android/gms/googlehelp/a/l;->f:Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/gms/googlehelp/e/h;->a:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/android/gms/googlehelp/a/l;->g:Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/gms/googlehelp/e/h;->b:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/android/gms/googlehelp/a/l;->h:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_8

    iget-object v4, v1, Lcom/google/android/gms/googlehelp/a/l;->h:Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/gms/googlehelp/e/h;->c:Ljava/lang/String;

    :cond_8
    iput-object v0, v3, Lcom/google/android/gms/googlehelp/e/o;->g:Lcom/google/android/gms/googlehelp/e/h;

    :cond_9
    :goto_1
    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->i:Lcom/google/android/gms/googlehelp/e/i;

    if-eqz v0, :cond_a

    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->i:Lcom/google/android/gms/googlehelp/e/i;

    iput-object v0, v3, Lcom/google/android/gms/googlehelp/e/o;->k:Lcom/google/android/gms/googlehelp/e/i;

    :cond_a
    const v0, 0x6768a8

    iput v0, v3, Lcom/google/android/gms/googlehelp/e/o;->l:I

    const-string v0, "6.7.77 (1747363-000)"

    iput-object v0, v3, Lcom/google/android/gms/googlehelp/e/o;->m:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/gms/googlehelp/e/k;->c:Lcom/google/android/gms/googlehelp/e/o;

    invoke-static {v2}, Lcom/google/android/gms/googlehelp/e/k;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/a/i;->i:[B

    .line 92
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/i;->i:[B

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/common/s;->a([B)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/a/i;->i:[B

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/i;->h:Ljava/util/HashMap;

    const-string v1, "Content-Encoding"

    const-string v2, "gzip"

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 94
    :catch_0
    move-exception v0

    .line 95
    const-string v1, "GOOGLEHELP_GoogleHelpBasePostRequest"

    const-string v2, "Gzip HelpMobileRequest bytes failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 89
    :cond_b
    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->j:Ljava/lang/String;

    if-eqz v0, :cond_f

    new-instance v0, Lcom/google/android/gms/googlehelp/e/g;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/e/g;-><init>()V

    iget-object v4, v1, Lcom/google/android/gms/googlehelp/a/l;->j:Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/gms/googlehelp/e/g;->a:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/android/gms/googlehelp/a/l;->k:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_c

    iget-object v4, v1, Lcom/google/android/gms/googlehelp/a/l;->k:Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/gms/googlehelp/e/g;->d:Ljava/lang/String;

    :cond_c
    iget-object v4, v1, Lcom/google/android/gms/googlehelp/a/l;->l:Ljava/lang/Long;

    if-eqz v4, :cond_d

    iget-object v4, v1, Lcom/google/android/gms/googlehelp/a/l;->l:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/android/gms/googlehelp/e/g;->b:J

    :cond_d
    iget-boolean v4, v1, Lcom/google/android/gms/googlehelp/a/l;->m:Z

    if-eqz v4, :cond_e

    iget-boolean v4, v1, Lcom/google/android/gms/googlehelp/a/l;->m:Z

    iput-boolean v4, v0, Lcom/google/android/gms/googlehelp/e/g;->c:Z

    :cond_e
    iput-object v0, v3, Lcom/google/android/gms/googlehelp/e/o;->j:Lcom/google/android/gms/googlehelp/e/g;

    goto :goto_1

    :cond_f
    iget-object v0, v1, Lcom/google/android/gms/googlehelp/a/l;->n:Ljava/lang/String;

    if-eqz v0, :cond_9

    new-instance v0, Lcom/google/android/gms/googlehelp/e/p;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/e/p;-><init>()V

    iget-object v4, v1, Lcom/google/android/gms/googlehelp/a/l;->n:Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/gms/googlehelp/e/p;->a:Ljava/lang/String;

    iget-object v4, v1, Lcom/google/android/gms/googlehelp/a/l;->o:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_10

    iget-object v4, v1, Lcom/google/android/gms/googlehelp/a/l;->o:Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/gms/googlehelp/e/p;->d:Ljava/lang/String;

    :cond_10
    iget-object v4, v1, Lcom/google/android/gms/googlehelp/a/l;->p:Ljava/lang/Long;

    if-eqz v4, :cond_11

    iget-object v4, v1, Lcom/google/android/gms/googlehelp/a/l;->p:Ljava/lang/Long;

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/android/gms/googlehelp/e/p;->b:J

    :cond_11
    iget-boolean v4, v1, Lcom/google/android/gms/googlehelp/a/l;->q:Z

    if-eqz v4, :cond_12

    iget-boolean v4, v1, Lcom/google/android/gms/googlehelp/a/l;->q:Z

    iput-boolean v4, v0, Lcom/google/android/gms/googlehelp/e/p;->c:Z

    :cond_12
    iput-object v0, v3, Lcom/google/android/gms/googlehelp/e/o;->i:Lcom/google/android/gms/googlehelp/e/p;

    goto/16 :goto_1
.end method


# virtual methods
.method protected a(Lcom/google/android/gms/googlehelp/a/l;)V
    .locals 0

    .prologue
    .line 108
    return-void
.end method

.method public final i()Ljava/util/Map;
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/a/i;->t()V

    .line 70
    invoke-super {p0}, Lcom/google/android/gms/googlehelp/a/j;->i()Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    const-string v0, "application/protobuf"

    return-object v0
.end method

.method public final m()[B
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/a/i;->t()V

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/a/i;->i:[B

    return-object v0
.end method
