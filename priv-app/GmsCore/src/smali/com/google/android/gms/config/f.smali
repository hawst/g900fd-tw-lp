.class public final Lcom/google/android/gms/config/f;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/config/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    return-void
.end method

.method private a()Lcom/google/android/gms/config/b/b;
    .locals 11

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 161
    new-instance v4, Lcom/google/android/gms/config/b/b;

    invoke-direct {v4}, Lcom/google/android/gms/config/b/b;-><init>()V

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v0, v0, Lcom/google/android/gms/config/g;->b:Lcom/google/android/gms/config/ConfigFetchService;

    invoke-virtual {v0}, Lcom/google/android/gms/config/ConfigFetchService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "android_id"

    invoke-static {v0, v2, v8, v9}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v6

    .line 165
    cmp-long v0, v6, v8

    if-nez v0, :cond_0

    .line 166
    const-string v0, "ConfigFetchTask"

    const-string v1, "no android id"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 264
    :goto_0
    return-object v0

    .line 169
    :cond_0
    invoke-virtual {v4, v6, v7}, Lcom/google/android/gms/config/b/b;->a(J)Lcom/google/android/gms/config/b/b;

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v0, v0, Lcom/google/android/gms/config/g;->b:Lcom/google/android/gms/config/ConfigFetchService;

    invoke-static {v0}, Lcom/google/android/gms/checkin/i;->a(Landroid/content/Context;)J

    move-result-wide v6

    .line 172
    cmp-long v0, v6, v8

    if-nez v0, :cond_1

    .line 173
    const-string v0, "ConfigFetchTask"

    const-string v1, "no security token"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 174
    goto :goto_0

    .line 176
    :cond_1
    invoke-virtual {v4, v6, v7}, Lcom/google/android/gms/config/b/b;->b(J)Lcom/google/android/gms/config/b/b;

    .line 180
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v0, v0, Lcom/google/android/gms/config/g;->b:Lcom/google/android/gms/config/ConfigFetchService;

    invoke-virtual {v0}, Lcom/google/android/gms/config/ConfigFetchService;->b()Lcom/google/android/gms/config/a/d;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v5, v0

    .line 183
    :goto_1
    if-nez v5, :cond_2

    .line 184
    const-string v0, "ConfigFetchTask"

    const-string v1, "failed to connect to config service"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 185
    goto :goto_0

    :catch_0
    move-exception v0

    move-object v5, v3

    goto :goto_1

    .line 189
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v0, v0, Lcom/google/android/gms/config/g;->b:Lcom/google/android/gms/config/ConfigFetchService;

    invoke-static {v0}, Lcom/google/android/gms/checkin/CheckinService;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    const-string v6, "ConfigFetchTask"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v0, "ConfigFetchTask getDeviceDataVersionInfo(): "

    invoke-direct {v7, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-nez v2, :cond_5

    const-string v0, "no version info"

    :goto_2
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 190
    if-eqz v2, :cond_3

    .line 191
    invoke-virtual {v4, v2}, Lcom/google/android/gms/config/b/b;->a(Ljava/lang/String;)Lcom/google/android/gms/config/b/b;

    .line 192
    const-string v0, "ConfigFetchTask"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 193
    const-string v0, "ConfigFetchTask"

    const-string v2, "added deviceDataVersionInfo to the request"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    .line 204
    :cond_3
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v0, v0, Lcom/google/android/gms/config/g;->c:Ljava/util/Set;

    if-nez v0, :cond_6

    .line 210
    :try_start_2
    invoke-interface {v5}, Lcom/google/android/gms/config/a/d;->a()Ljava/util/Map;
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v2

    .line 216
    iget-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v0, v0, Lcom/google/android/gms/config/g;->a:Landroid/content/pm/PackageManager;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v0

    .line 217
    if-eqz v0, :cond_a

    .line 218
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 219
    new-instance v3, Lcom/google/android/gms/config/b/e;

    invoke-direct {v3}, Lcom/google/android/gms/config/b/e;-><init>()V

    .line 220
    iget-object v5, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/google/android/gms/config/b/e;->a(Ljava/lang/String;)Lcom/google/android/gms/config/b/e;

    .line 221
    iget v5, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v3, v5}, Lcom/google/android/gms/config/b/e;->a(I)Lcom/google/android/gms/config/b/e;

    .line 222
    if-eqz v2, :cond_4

    .line 223
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 224
    if-eqz v0, :cond_4

    invoke-static {v0}, Lcom/google/protobuf/a/a;->a([B)Lcom/google/protobuf/a/a;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/config/b/e;->a(Lcom/google/protobuf/a/a;)Lcom/google/android/gms/config/b/e;

    .line 226
    :cond_4
    invoke-virtual {v4, v3}, Lcom/google/android/gms/config/b/b;->a(Lcom/google/android/gms/config/b/e;)Lcom/google/android/gms/config/b/b;

    goto :goto_4

    :cond_5
    move-object v0, v2

    .line 189
    goto :goto_2

    .line 196
    :catch_1
    move-exception v0

    .line 199
    const-string v2, "ConfigFetchTask"

    const-string v6, "failed to get the device data version info from checkin service"

    invoke-static {v2, v6, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_3

    .line 212
    :catch_2
    move-exception v0

    const-string v0, "ConfigFetchTask"

    const-string v1, "failed to get digests from config service"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 213
    goto/16 :goto_0

    .line 230
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v0, v0, Lcom/google/android/gms/config/g;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_7

    move-object v0, v3

    goto/16 :goto_0

    .line 232
    :cond_7
    new-instance v6, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v0, v0, Lcom/google/android/gms/config/g;->c:Ljava/util/Set;

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 233
    :try_start_3
    invoke-interface {v5, v6}, Lcom/google/android/gms/config/a/d;->a(Ljava/util/List;)Ljava/util/List;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_3

    move-result-object v7

    .line 241
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v5, v1

    move v2, v1

    .line 243
    :goto_5
    if-ge v5, v8, :cond_9

    .line 244
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 245
    invoke-interface {v7, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 249
    :try_start_4
    iget-object v9, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v9, v9, Lcom/google/android/gms/config/g;->a:Landroid/content/pm/PackageManager;

    const/4 v10, 0x0

    invoke-virtual {v9, v0, v10}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_4

    move-result-object v0

    .line 252
    :goto_6
    if-eqz v0, :cond_b

    .line 254
    new-instance v9, Lcom/google/android/gms/config/b/e;

    invoke-direct {v9}, Lcom/google/android/gms/config/b/e;-><init>()V

    .line 255
    iget-object v10, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v9, v10}, Lcom/google/android/gms/config/b/e;->a(Ljava/lang/String;)Lcom/google/android/gms/config/b/e;

    .line 256
    iget v0, v0, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v9, v0}, Lcom/google/android/gms/config/b/e;->a(I)Lcom/google/android/gms/config/b/e;

    .line 257
    if-eqz v1, :cond_8

    invoke-static {v1}, Lcom/google/protobuf/a/a;->a([B)Lcom/google/protobuf/a/a;

    move-result-object v0

    invoke-virtual {v9, v0}, Lcom/google/android/gms/config/b/e;->a(Lcom/google/protobuf/a/a;)Lcom/google/android/gms/config/b/e;

    .line 258
    :cond_8
    invoke-virtual {v4, v9}, Lcom/google/android/gms/config/b/b;->a(Lcom/google/android/gms/config/b/e;)Lcom/google/android/gms/config/b/b;

    .line 259
    add-int/lit8 v0, v2, 0x1

    .line 243
    :goto_7
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    move v2, v0

    goto :goto_5

    .line 237
    :catch_3
    move-exception v0

    const-string v0, "ConfigFetchTask"

    const-string v1, "failed to get some digests from config service"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v3

    .line 238
    goto/16 :goto_0

    :catch_4
    move-exception v0

    move-object v0, v3

    goto :goto_6

    .line 261
    :cond_9
    if-nez v2, :cond_a

    move-object v0, v3

    goto/16 :goto_0

    :cond_a
    move-object v0, v4

    .line 264
    goto/16 :goto_0

    :cond_b
    move v0, v2

    goto :goto_7
.end method

.method private a(Lcom/google/android/gms/config/b/b;)Lcom/google/android/gms/config/b/c;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 94
    :try_start_0
    new-instance v1, Ljava/net/URL;

    const-string v2, "https://android.clients.google.com/config"

    invoke-direct {v1, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 100
    new-instance v5, Lcom/google/android/gms/http/GoogleHttpClient;

    iget-object v2, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v2, v2, Lcom/google/android/gms/config/g;->b:Lcom/google/android/gms/config/ConfigFetchService;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, "ConfigFetch-"

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v6, 0x6768a8

    invoke-static {v6}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v6, "/1.0"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v5, v2, v4, v7}, Lcom/google/android/gms/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 107
    :try_start_1
    invoke-virtual {v5}, Lcom/google/android/gms/http/GoogleHttpClient;->getConnectionFactory()Lcom/google/android/gms/http/e;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/http/e;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v2

    .line 108
    const-string v1, "POST"

    invoke-virtual {v2, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 109
    const-string v1, "Content-Type"

    const-string v4, "application/x-protobuffer"

    invoke-virtual {v2, v1, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 110
    const/4 v1, 0x1

    invoke-virtual {v2, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 111
    const v1, 0x1d4c0

    invoke-virtual {v2, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 112
    const v1, 0x1d4c0

    invoke-virtual {v2, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 113
    instance-of v1, v2, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v1, :cond_0

    .line 114
    move-object v0, v2

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    move-object v1, v0

    invoke-virtual {v5}, Lcom/google/android/gms/http/GoogleHttpClient;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 117
    :cond_0
    invoke-static {}, Lcom/google/k/e/f;->a()Lcom/google/k/e/f;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v4

    .line 119
    :try_start_2
    new-instance v1, Ljava/util/zip/GZIPOutputStream;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v6

    invoke-direct {v1, v6}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v4, v1}, Lcom/google/k/e/f;->a(Ljava/io/Closeable;)Ljava/io/Closeable;

    move-result-object v1

    check-cast v1, Ljava/util/zip/GZIPOutputStream;

    .line 121
    invoke-static {v1}, Lcom/google/protobuf/a/c;->a(Ljava/io/OutputStream;)Lcom/google/protobuf/a/c;

    move-result-object v1

    .line 122
    invoke-virtual {p1, v1}, Lcom/google/android/gms/config/b/b;->a(Lcom/google/protobuf/a/c;)V

    .line 123
    invoke-virtual {v1}, Lcom/google/protobuf/a/c;->a()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 127
    :try_start_3
    invoke-virtual {v4}, Lcom/google/k/e/f;->close()V

    .line 130
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    .line 132
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    .line 133
    const/16 v4, 0xc8

    if-eq v1, v4, :cond_1

    .line 134
    const-string v4, "ConfigFetchTask"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "bad response from server: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, " "

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v4, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 135
    invoke-virtual {v5}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    .line 148
    invoke-static {v3}, Lcom/google/k/e/e;->a(Ljava/io/InputStream;)V

    move-object v1, v3

    :goto_0
    return-object v1

    .line 96
    :catch_0
    move-exception v1

    const-string v1, "ConfigFetchTask"

    const-string v2, "bad config fetch url https://android.clients.google.com/config"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v3

    .line 97
    goto :goto_0

    .line 124
    :catch_1
    move-exception v1

    .line 125
    :try_start_4
    invoke-virtual {v4, v1}, Lcom/google/k/e/f;->a(Ljava/lang/Throwable;)Ljava/lang/RuntimeException;

    move-result-object v1

    throw v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 127
    :catchall_0
    move-exception v1

    :try_start_5
    invoke-virtual {v4}, Lcom/google/k/e/f;->close()V

    throw v1
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 143
    :catch_2
    move-exception v1

    move-object v2, v3

    .line 144
    :goto_1
    :try_start_6
    const-string v4, "ConfigFetchTask"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "exception on config fetch: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_3

    .line 145
    invoke-virtual {v5}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    .line 148
    invoke-static {v2}, Lcom/google/k/e/e;->a(Ljava/io/InputStream;)V

    move-object v1, v3

    goto :goto_0

    .line 138
    :cond_1
    :try_start_7
    new-instance v4, Ljava/util/zip/GZIPInputStream;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    .line 139
    :try_start_8
    new-instance v1, Lcom/google/android/gms/config/b/c;

    invoke-direct {v1}, Lcom/google/android/gms/config/b/c;-><init>()V

    .line 140
    invoke-static {v4}, Lcom/google/protobuf/a/b;->a(Ljava/io/InputStream;)Lcom/google/protobuf/a/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/config/b/c;->b(Lcom/google/protobuf/a/b;)Lcom/google/android/gms/config/b/c;
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 147
    invoke-virtual {v5}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    .line 148
    invoke-static {v4}, Lcom/google/k/e/e;->a(Ljava/io/InputStream;)V

    goto :goto_0

    .line 147
    :catchall_1
    move-exception v1

    :goto_2
    invoke-virtual {v5}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    .line 148
    invoke-static {v3}, Lcom/google/k/e/e;->a(Ljava/io/InputStream;)V

    throw v1

    .line 147
    :catchall_2
    move-exception v1

    move-object v3, v4

    goto :goto_2

    :catchall_3
    move-exception v1

    move-object v3, v2

    goto :goto_2

    .line 143
    :catch_3
    move-exception v1

    move-object v2, v4

    goto :goto_1
.end method

.method private a(Lcom/google/android/gms/config/b/c;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 268
    if-nez p1, :cond_1

    .line 298
    :cond_0
    :goto_0
    return-void

    .line 272
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v0, v0, Lcom/google/android/gms/config/g;->b:Lcom/google/android/gms/config/ConfigFetchService;

    invoke-virtual {v0}, Lcom/google/android/gms/config/ConfigFetchService;->b()Lcom/google/android/gms/config/a/d;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    move-object v4, v0

    .line 275
    :goto_1
    if-nez v4, :cond_2

    .line 276
    const-string v0, "ConfigFetchTask"

    const-string v1, "failed to connect to config service"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v4, v3

    goto :goto_1

    .line 280
    :cond_2
    iget-object v0, p1, Lcom/google/android/gms/config/b/c;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/config/b/f;

    .line 281
    const-string v1, "ConfigFetchTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "updating config table for "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, Lcom/google/android/gms/config/b/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    iget-object v1, v0, Lcom/google/android/gms/config/b/f;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_4

    .line 285
    new-instance v2, Ljava/util/TreeMap;

    invoke-direct {v2}, Ljava/util/TreeMap;-><init>()V

    .line 286
    iget-object v1, v0, Lcom/google/android/gms/config/b/f;->b:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/config/b/d;

    .line 287
    iget-object v7, v1, Lcom/google/android/gms/config/b/d;->a:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/android/gms/config/b/d;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {v1}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v1

    invoke-virtual {v2, v7, v1}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_3
    move-object v1, v2

    .line 292
    :goto_4
    :try_start_1
    iget-object v2, v0, Lcom/google/android/gms/config/b/f;->a:Ljava/lang/String;

    invoke-interface {v4, v2, v1}, Lcom/google/android/gms/config/a/d;->a(Ljava/lang/String;Ljava/util/Map;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 294
    :catch_1
    move-exception v1

    const-string v1, "ConfigFetchTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "failed to update config service with new values for "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/config/b/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :cond_4
    move-object v1, v3

    goto :goto_4
.end method

.method private b()V
    .locals 10

    .prologue
    const/4 v3, 0x0

    .line 305
    iget-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v0, v0, Lcom/google/android/gms/config/g;->d:Landroid/content/SharedPreferences;

    if-nez v0, :cond_1

    .line 356
    :cond_0
    :goto_0
    return-void

    .line 306
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v0, v0, Lcom/google/android/gms/config/g;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 307
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 313
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x36ee80

    sub-long/2addr v4, v6

    .line 314
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v2, v3

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 315
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 316
    cmp-long v1, v8, v4

    if-gez v1, :cond_6

    .line 317
    if-nez v2, :cond_5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 318
    :goto_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    move-object v2, v1

    .line 320
    goto :goto_1

    .line 321
    :cond_2
    if-eqz v2, :cond_0

    .line 325
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v0, v0, Lcom/google/android/gms/config/g;->b:Lcom/google/android/gms/config/ConfigFetchService;

    invoke-virtual {v0}, Lcom/google/android/gms/config/ConfigFetchService;->b()Lcom/google/android/gms/config/a/d;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v3

    .line 328
    :goto_4
    if-nez v3, :cond_3

    .line 329
    const-string v0, "ConfigFetchTask"

    const-string v1, "failed to connect to config service"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 334
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v0, v0, Lcom/google/android/gms/config/g;->d:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 335
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 338
    :try_start_2
    iget-object v4, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v4, v4, Lcom/google/android/gms/config/g;->a:Landroid/content/pm/PackageManager;

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 348
    :goto_6
    :try_start_3
    invoke-interface {v1, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_5

    .line 352
    :catch_0
    move-exception v0

    const-string v0, "ConfigFetchTask"

    const-string v1, "failed to purge config service"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 342
    :catch_1
    move-exception v4

    :try_start_4
    const-string v4, "ConfigFetchTask"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "  purging config for "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    const/4 v4, 0x0

    invoke-interface {v3, v0, v4}, Lcom/google/android/gms/config/a/d;->a(Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_6

    .line 350
    :cond_4
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0

    goto/16 :goto_0

    :catch_2
    move-exception v0

    goto :goto_4

    :cond_5
    move-object v1, v2

    goto :goto_2

    :cond_6
    move-object v1, v2

    goto :goto_3
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 41
    check-cast p1, [Lcom/google/android/gms/config/g;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    const-string v4, "ConfigFetchTask"

    invoke-virtual {v3, v4}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    array-length v3, p1

    if-eq v3, v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Must be one Params object"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    aget-object v3, p1, v1

    iput-object v3, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    invoke-direct {p0}, Lcom/google/android/gms/config/f;->a()Lcom/google/android/gms/config/b/b;

    move-result-object v3

    if-nez v3, :cond_1

    const-string v0, "ConfigFetchTask"

    const-string v3, "failed to build request; aborting config fetch"

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/config/f;->b()V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-direct {p0, v3}, Lcom/google/android/gms/config/f;->a(Lcom/google/android/gms/config/b/b;)Lcom/google/android/gms/config/b/c;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gms/config/f;->a(Lcom/google/android/gms/config/b/c;)V

    if-eqz v3, :cond_2

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 41
    check-cast p1, Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/gms/config/f;->a:Lcom/google/android/gms/config/g;

    iget-object v0, v0, Lcom/google/android/gms/config/g;->b:Lcom/google/android/gms/config/ConfigFetchService;

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/config/ConfigFetchService;->a(Z)V

    return-void
.end method
