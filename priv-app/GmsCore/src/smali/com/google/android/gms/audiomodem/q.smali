.class public final Lcom/google/android/gms/audiomodem/q;
.super Lcom/google/android/gms/audiomodem/aq;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/audiomodem/a/q;)V
    .locals 0

    .prologue
    .line 293
    invoke-direct {p0, p1}, Lcom/google/android/gms/audiomodem/aq;-><init>(Landroid/os/IInterface;)V

    .line 294
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 298
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/q;->d:Landroid/os/IInterface;

    check-cast v0, Lcom/google/android/gms/audiomodem/a/q;

    invoke-interface {v0, p1}, Lcom/google/android/gms/audiomodem/a/q;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 302
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 306
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 307
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 308
    new-instance v3, Lcom/google/android/gms/audiomodem/DecodedToken;

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/whispernet/Data$DecodedToken;

    iget-object v0, v0, Lcom/google/whispernet/Data$DecodedToken;->token:[B

    invoke-direct {v3, v0, v1}, Lcom/google/android/gms/audiomodem/DecodedToken;-><init>([BI)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 307
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 311
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/q;->d:Landroid/os/IInterface;

    check-cast v0, Lcom/google/android/gms/audiomodem/a/q;

    invoke-interface {v0, v2}, Lcom/google/android/gms/audiomodem/a/q;->a(Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 314
    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1
.end method
