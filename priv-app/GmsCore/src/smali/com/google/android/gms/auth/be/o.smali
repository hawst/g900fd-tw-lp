.class final Lcom/google/android/gms/auth/be/o;
.super Lcom/google/android/gms/auth/firstparty/dataservice/aw;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/be/GoogleAccountDataService;

.field private final b:Lcom/google/android/gms/auth/a/k;

.field private final c:Lcom/google/android/gms/auth/be/p;

.field private final d:Lcom/google/android/gms/auth/be/s;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/be/GoogleAccountDataService;Lcom/google/android/gms/auth/a/k;Lcom/google/android/gms/auth/be/p;Lcom/google/android/gms/auth/be/s;)V
    .locals 0

    .prologue
    .line 75
    iput-object p1, p0, Lcom/google/android/gms/auth/be/o;->a:Lcom/google/android/gms/auth/be/GoogleAccountDataService;

    invoke-direct {p0}, Lcom/google/android/gms/auth/firstparty/dataservice/aw;-><init>()V

    .line 76
    iput-object p2, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    .line 77
    iput-object p3, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    .line 78
    iput-object p4, p0, Lcom/google/android/gms/auth/be/o;->d:Lcom/google/android/gms/auth/be/s;

    .line 79
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/auth/AccountChangeEventsRequest;)Lcom/google/android/gms/auth/AccountChangeEventsResponse;
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/AccountChangeEventsRequest;)Lcom/google/android/gms/auth/AccountChangeEventsResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckResponse;
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckRequest;->d()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckRequest;->e()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountNameCheckResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 270
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-static {}, Lcom/google/android/gms/auth/be/p;->a()Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryDataRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;
    .locals 2

    .prologue
    .line 279
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-static {p1}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryDataRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidanceRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidance;
    .locals 2

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 290
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-static {p1}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidanceRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryGuidance;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateResult;
    .locals 2

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 300
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-static {p1}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryUpdateResult;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;
    .locals 2

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRemovalResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceResponse;
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 362
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameResponse;
    .locals 3

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameRequest;->a()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameRequest;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/CheckRealNameResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/ClearTokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/GplusInfoRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/GplusInfoResponse;
    .locals 3

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GplusInfoRequest;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GplusInfoRequest;->b()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/be/p;->a(Ljava/lang/String;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/GplusInfoResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/OtpRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/OtpResponse;
    .locals 2

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->b(I)V

    .line 346
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/dataservice/OtpRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/OtpResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckResponse;
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckRequest;->e()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/PasswordCheckResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;
    .locals 2

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->d:Lcom/google/android/gms/auth/be/s;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/be/s;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/ReauthSettingsResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 5

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;->a()Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;->b()Z

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;->c()Z

    move-result v3

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;->d()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;ZZLcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;->a()Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;->b()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 4

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->n()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->o()Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->p()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v3

    invoke-virtual {v0, v1, v2, p1, v3}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0xf
    .end annotation

    .prologue
    .line 196
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 197
    const-string v0, "TokenRequest cannot be null!"

    invoke-static {v0, p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->m()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;->n()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/shared/AppDescription;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v1

    .line 202
    sget-object v0, Lcom/google/android/gms/common/a/b;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 203
    if-eqz v0, :cond_0

    .line 204
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "auth.getToken.response.status."

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->b()Lcom/google/android/gms/auth/firstparty/shared/k;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/auth/firstparty/shared/k;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 207
    invoke-static {v0}, Lcom/google/android/gms/common/status/StatusService;->a(Ljava/util/List;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 209
    :cond_0
    return-object v1

    .line 210
    :catch_0
    move-exception v0

    .line 211
    const-string v1, "GLSUser"

    const-string v2, "Exception thrown while executing getToken."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 212
    const/16 v1, 0xf

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 213
    new-instance v1, Landroid/os/RemoteException;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/os/RemoteException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 215
    :cond_1
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/UpdateCredentialsRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/UpdateCredentialsRequest;->a()Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/UpdateCredentialsRequest;->b()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/be/p;->b(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;)Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;
    .locals 2

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 384
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;)Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;
    .locals 2

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 333
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->d:Lcom/google/android/gms/auth/be/s;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/be/s;->a(Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/VerifyPinResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfigRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/WebSetupConfig;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 321
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/be/p;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 314
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/auth/be/p;->a(Ljava/lang/String;Landroid/os/Bundle;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountData;
    .locals 2

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/be/p;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountData;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->n()Lcom/google/android/gms/auth/firstparty/shared/AppDescription;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->o()Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;->p()Lcom/google/android/gms/auth/firstparty/shared/CaptchaSolution;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/auth/be/p;->a(Lcom/google/android/gms/auth/firstparty/shared/AccountCredentials;Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountSetupRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    move-result-object v0

    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 355
    new-instance v0, Lcom/google/android/gms/auth/frp/a;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/o;->a:Lcom/google/android/gms/auth/be/GoogleAccountDataService;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/frp/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/auth/frp/a;->a()Z

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 306
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/be/p;->c(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 371
    new-instance v0, Lcom/google/android/gms/auth/frp/a;

    iget-object v1, p0, Lcom/google/android/gms/auth/be/o;->a:Lcom/google/android/gms/auth/be/GoogleAccountDataService;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/frp/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/auth/frp/a;->b()Z

    move-result v0

    return v0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->b:Lcom/google/android/gms/auth/a/k;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/k;->a(I)V

    .line 377
    iget-object v0, p0, Lcom/google/android/gms/auth/be/o;->c:Lcom/google/android/gms/auth/be/p;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/p;->b()V

    .line 378
    return-void
.end method
