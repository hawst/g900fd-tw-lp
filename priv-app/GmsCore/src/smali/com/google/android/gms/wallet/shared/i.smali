.class final Lcom/google/android/gms/wallet/shared/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 429
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 429
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    const/4 v0, -0x1

    if-ne v3, v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    new-instance v1, Lcom/google/android/gms/wallet/shared/ProtoUtils$ProtoListParcelable;

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils$ProtoListParcelable;-><init>([[BB)V

    return-object v1

    :cond_1
    new-array v0, v3, [[B

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_0

    invoke-virtual {p1}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v4

    aput-object v4, v0, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final bridge synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 429
    new-array v0, p1, [Lcom/google/android/gms/wallet/shared/ProtoUtils$ProtoListParcelable;

    return-object v0
.end method
