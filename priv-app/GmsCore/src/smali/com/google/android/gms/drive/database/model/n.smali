.class public final enum Lcom/google/android/gms/drive/database/model/n;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ak;


# static fields
.field public static final enum a:Lcom/google/android/gms/drive/database/model/n;

.field public static final enum b:Lcom/google/android/gms/drive/database/model/n;

.field public static final enum c:Lcom/google/android/gms/drive/database/model/n;

.field public static final enum d:Lcom/google/android/gms/drive/database/model/n;

.field private static final synthetic f:[Lcom/google/android/gms/drive/database/model/n;


# instance fields
.field private final e:Lcom/google/android/gms/drive/database/model/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x0

    const/16 v8, 0x2b

    const/16 v6, 0x2a

    const/4 v7, 0x1

    .line 45
    new-instance v0, Lcom/google/android/gms/drive/database/model/n;

    const-string v1, "APP_ID"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/m;->d()Lcom/google/android/gms/drive/database/model/m;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "appId"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {}, Lcom/google/android/gms/drive/database/model/g;->a()Lcom/google/android/gms/drive/database/model/g;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Lcom/google/android/gms/drive/database/model/ae;Lcom/google/android/gms/drive/database/model/ab;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v9, v2}, Lcom/google/android/gms/drive/database/model/n;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/n;->a:Lcom/google/android/gms/drive/database/model/n;

    .line 54
    new-instance v0, Lcom/google/android/gms/drive/database/model/n;

    const-string v1, "NETWORK_TYPE_PREFERENCE"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/m;->d()Lcom/google/android/gms/drive/database/model/m;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "dataConnectionPreference"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/google/android/gms/drive/database/model/ac;->a(I)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "networkTypePreference"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v3

    invoke-virtual {v2, v8, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v7, v2}, Lcom/google/android/gms/drive/database/model/n;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/n;->b:Lcom/google/android/gms/drive/database/model/n;

    .line 64
    new-instance v0, Lcom/google/android/gms/drive/database/model/n;

    const-string v1, "BATTERY_USAGE_PREFERENCE"

    invoke-static {}, Lcom/google/android/gms/drive/database/model/m;->d()Lcom/google/android/gms/drive/database/model/m;

    new-instance v2, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v2}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v3, Lcom/google/android/gms/drive/database/model/au;

    const-string v4, "batteryUsagePreference"

    sget-object v5, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    iput-boolean v7, v3, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v2, v6, v3}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v2

    invoke-direct {v0, v1, v10, v2}, Lcom/google/android/gms/drive/database/model/n;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/n;->c:Lcom/google/android/gms/drive/database/model/n;

    .line 71
    new-instance v0, Lcom/google/android/gms/drive/database/model/n;

    const-string v1, "ROAMING_PREFERENCE"

    const/4 v2, 0x3

    invoke-static {}, Lcom/google/android/gms/drive/database/model/m;->d()Lcom/google/android/gms/drive/database/model/m;

    new-instance v3, Lcom/google/android/gms/drive/database/model/ac;

    invoke-direct {v3}, Lcom/google/android/gms/drive/database/model/ac;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/database/model/au;

    const-string v5, "roamingAllowed"

    sget-object v6, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/database/model/au;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/database/model/aw;)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/google/android/gms/drive/database/model/au;->a(Ljava/lang/Object;)Lcom/google/android/gms/drive/database/model/au;

    move-result-object v4

    iput-boolean v7, v4, Lcom/google/android/gms/drive/database/model/au;->g:Z

    invoke-virtual {v3, v8, v4}, Lcom/google/android/gms/drive/database/model/ac;->a(ILcom/google/android/gms/drive/database/model/au;)Lcom/google/android/gms/drive/database/model/ac;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/model/n;-><init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V

    sput-object v0, Lcom/google/android/gms/drive/database/model/n;->d:Lcom/google/android/gms/drive/database/model/n;

    .line 41
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/drive/database/model/n;

    sget-object v1, Lcom/google/android/gms/drive/database/model/n;->a:Lcom/google/android/gms/drive/database/model/n;

    aput-object v1, v0, v9

    sget-object v1, Lcom/google/android/gms/drive/database/model/n;->b:Lcom/google/android/gms/drive/database/model/n;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/android/gms/drive/database/model/n;->c:Lcom/google/android/gms/drive/database/model/n;

    aput-object v1, v0, v10

    const/4 v1, 0x3

    sget-object v2, Lcom/google/android/gms/drive/database/model/n;->d:Lcom/google/android/gms/drive/database/model/n;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/drive/database/model/n;->f:[Lcom/google/android/gms/drive/database/model/n;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILcom/google/android/gms/drive/database/model/ac;)V
    .locals 1

    .prologue
    .line 78
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 79
    invoke-virtual {p3}, Lcom/google/android/gms/drive/database/model/ac;->a()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/model/n;->e:Lcom/google/android/gms/drive/database/model/ab;

    .line 80
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/n;
    .locals 1

    .prologue
    .line 41
    const-class v0, Lcom/google/android/gms/drive/database/model/n;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/model/n;

    return-object v0
.end method

.method public static values()[Lcom/google/android/gms/drive/database/model/n;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/gms/drive/database/model/n;->f:[Lcom/google/android/gms/drive/database/model/n;

    invoke-virtual {v0}, [Lcom/google/android/gms/drive/database/model/n;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/drive/database/model/n;

    return-object v0
.end method


# virtual methods
.method public final bridge synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/n;->e:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/drive/database/model/ab;
    .locals 1

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/drive/database/model/n;->e:Lcom/google/android/gms/drive/database/model/ab;

    return-object v0
.end method
