.class final Lcom/google/android/gms/location/copresence/internal/d;
.super Lcom/google/android/gms/location/copresence/internal/j;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;

.field private final b:Lcom/google/android/gms/common/api/m;

.field private final c:Lcom/google/android/gms/common/api/m;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 382
    invoke-direct {p0}, Lcom/google/android/gms/location/copresence/internal/j;-><init>()V

    .line 383
    iput-object p1, p0, Lcom/google/android/gms/location/copresence/internal/d;->a:Lcom/google/android/gms/common/api/m;

    .line 384
    iput-object v0, p0, Lcom/google/android/gms/location/copresence/internal/d;->b:Lcom/google/android/gms/common/api/m;

    .line 385
    iput-object v0, p0, Lcom/google/android/gms/location/copresence/internal/d;->c:Lcom/google/android/gms/common/api/m;

    .line 386
    return-void
.end method

.method public static a(Lcom/google/android/gms/common/api/m;)Lcom/google/android/gms/location/copresence/internal/d;
    .locals 1

    .prologue
    .line 389
    new-instance v0, Lcom/google/android/gms/location/copresence/internal/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/location/copresence/internal/d;-><init>(Lcom/google/android/gms/common/api/m;)V

    return-object v0
.end method


# virtual methods
.method public final a(ILcom/google/android/gms/location/copresence/CopresenceSettings;)V
    .locals 3

    .prologue
    .line 409
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    .line 410
    iget-object v1, p0, Lcom/google/android/gms/location/copresence/internal/d;->b:Lcom/google/android/gms/common/api/m;

    new-instance v2, Lcom/google/android/gms/location/copresence/internal/f;

    invoke-direct {v2, v0, p2}, Lcom/google/android/gms/location/copresence/internal/f;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/location/copresence/CopresenceSettings;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 411
    return-void
.end method

.method public final a(ILcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeResponse;)V
    .locals 3

    .prologue
    .line 415
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    .line 416
    iget-object v1, p0, Lcom/google/android/gms/location/copresence/internal/d;->c:Lcom/google/android/gms/common/api/m;

    new-instance v2, Lcom/google/android/gms/location/copresence/internal/e;

    invoke-direct {v2, v0, p2}, Lcom/google/android/gms/location/copresence/internal/e;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/location/copresence/debug/CopresenceDebugPokeResponse;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 417
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 1

    .prologue
    .line 404
    iget-object v0, p0, Lcom/google/android/gms/location/copresence/internal/d;->a:Lcom/google/android/gms/common/api/m;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 405
    return-void
.end method
