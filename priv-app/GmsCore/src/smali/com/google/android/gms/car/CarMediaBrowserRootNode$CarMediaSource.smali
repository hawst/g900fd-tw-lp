.class public Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/gms/car/fk;

    invoke-direct {v0}, Lcom/google/android/gms/car/fk;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->a:I

    .line 64
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;[B)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput p1, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->a:I

    .line 57
    iput-object p2, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->b:Ljava/lang/String;

    .line 58
    iput-object p3, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->c:Ljava/lang/String;

    .line 59
    iput-object p4, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->d:[B

    .line 60
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 80
    iget v0, p0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 73
    invoke-static {p0, p1}, Lcom/google/android/gms/car/fk;->a(Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;Landroid/os/Parcel;)V

    .line 74
    return-void
.end method
