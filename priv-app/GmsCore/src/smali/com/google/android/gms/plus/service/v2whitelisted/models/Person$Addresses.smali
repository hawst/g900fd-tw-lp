.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/ap;

.field private static final n:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Ljava/lang/String;

.field f:Ljava/lang/String;

.field g:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

.field h:Ljava/lang/String;

.field i:Ljava/lang/String;

.field j:Ljava/lang/String;

.field k:Ljava/lang/String;

.field l:Ljava/lang/String;

.field m:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 1510
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/ap;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/ap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/ap;

    .line 1574
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1577
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n:Ljava/util/HashMap;

    const-string v1, "city"

    const-string v2, "city"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1578
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n:Ljava/util/HashMap;

    const-string v1, "country"

    const-string v2, "country"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1579
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n:Ljava/util/HashMap;

    const-string v1, "countryCode"

    const-string v2, "countryCode"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1580
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n:Ljava/util/HashMap;

    const-string v1, "formattedType"

    const-string v2, "formattedType"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1581
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n:Ljava/util/HashMap;

    const-string v1, "metadata"

    const-string v2, "metadata"

    const/4 v3, 0x6

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1584
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n:Ljava/util/HashMap;

    const-string v1, "poBox"

    const-string v2, "poBox"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1585
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n:Ljava/util/HashMap;

    const-string v1, "postalCode"

    const-string v2, "postalCode"

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1586
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n:Ljava/util/HashMap;

    const-string v1, "region"

    const-string v2, "region"

    const/16 v3, 0x9

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1587
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n:Ljava/util/HashMap;

    const-string v1, "streetAddress"

    const-string v2, "streetAddress"

    const/16 v3, 0xa

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1588
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    const/16 v3, 0xb

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1589
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n:Ljava/util/HashMap;

    const-string v1, "value"

    const-string v2, "value"

    const/16 v3, 0xc

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1590
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1679
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 1680
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->b:I

    .line 1681
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->a:Ljava/util/Set;

    .line 1682
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1699
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 1700
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->a:Ljava/util/Set;

    .line 1701
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->b:I

    .line 1702
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->c:Ljava/lang/String;

    .line 1703
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->d:Ljava/lang/String;

    .line 1704
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->e:Ljava/lang/String;

    .line 1705
    iput-object p6, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->f:Ljava/lang/String;

    .line 1706
    iput-object p7, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->g:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    .line 1707
    iput-object p8, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->h:Ljava/lang/String;

    .line 1708
    iput-object p9, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->i:Ljava/lang/String;

    .line 1709
    iput-object p10, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->j:Ljava/lang/String;

    .line 1710
    iput-object p11, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->k:Ljava/lang/String;

    .line 1711
    iput-object p12, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->l:Ljava/lang/String;

    .line 1712
    iput-object p13, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->m:Ljava/lang/String;

    .line 1713
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 1594
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 2020
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 2021
    packed-switch v0, :pswitch_data_0

    .line 2026
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 2023
    :pswitch_0
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->g:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    .line 2030
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2031
    return-void

    .line 2021
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1978
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 1979
    packed-switch v0, :pswitch_data_0

    .line 2011
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 1981
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->c:Ljava/lang/String;

    .line 2014
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 2015
    return-void

    .line 1984
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->d:Ljava/lang/String;

    goto :goto_0

    .line 1987
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->e:Ljava/lang/String;

    goto :goto_0

    .line 1990
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->f:Ljava/lang/String;

    goto :goto_0

    .line 1993
    :pswitch_5
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->h:Ljava/lang/String;

    goto :goto_0

    .line 1996
    :pswitch_6
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->i:Ljava/lang/String;

    goto :goto_0

    .line 1999
    :pswitch_7
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->j:Ljava/lang/String;

    goto :goto_0

    .line 2002
    :pswitch_8
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->k:Ljava/lang/String;

    goto :goto_0

    .line 2005
    :pswitch_9
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->l:Ljava/lang/String;

    goto :goto_0

    .line 2008
    :pswitch_a
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->m:Ljava/lang/String;

    goto :goto_0

    .line 1979
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 1929
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;
    .locals 1

    .prologue
    .line 1812
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->g:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    return-object v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 1934
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 1958
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1936
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->c:Ljava/lang/String;

    .line 1956
    :goto_0
    return-object v0

    .line 1938
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->d:Ljava/lang/String;

    goto :goto_0

    .line 1940
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->e:Ljava/lang/String;

    goto :goto_0

    .line 1942
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->f:Ljava/lang/String;

    goto :goto_0

    .line 1944
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->g:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    goto :goto_0

    .line 1946
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->h:Ljava/lang/String;

    goto :goto_0

    .line 1948
    :pswitch_6
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->i:Ljava/lang/String;

    goto :goto_0

    .line 1950
    :pswitch_7
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->j:Ljava/lang/String;

    goto :goto_0

    .line 1952
    :pswitch_8
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->k:Ljava/lang/String;

    goto :goto_0

    .line 1954
    :pswitch_9
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->l:Ljava/lang/String;

    goto :goto_0

    .line 1956
    :pswitch_a
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->m:Ljava/lang/String;

    goto :goto_0

    .line 1934
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1891
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1906
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 1919
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/ap;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2048
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;

    if-nez v0, :cond_0

    move v0, v1

    .line 2079
    :goto_0
    return v0

    .line 2053
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 2054
    goto :goto_0

    .line 2057
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;

    .line 2058
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 2059
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 2060
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 2062
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 2064
    goto :goto_0

    :cond_3
    move v0, v1

    .line 2069
    goto :goto_0

    .line 2072
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 2074
    goto :goto_0

    :cond_5
    move v0, v2

    .line 2079
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 2035
    const/4 v0, 0x0

    .line 2036
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->n:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 2037
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2038
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 2039
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 2041
    goto :goto_0

    .line 2042
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1924
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/ap;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/ap;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Addresses;Landroid/os/Parcel;I)V

    .line 1925
    return-void
.end method
