.class final Lcom/google/android/gms/plus/internal/bd;
.super Lcom/google/android/gms/plus/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/internal/ay;

.field private final b:Lcom/google/android/gms/plus/internal/af;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/af;)V
    .locals 0

    .prologue
    .line 1088
    iput-object p1, p0, Lcom/google/android/gms/plus/internal/bd;->a:Lcom/google/android/gms/plus/internal/ay;

    invoke-direct {p0}, Lcom/google/android/gms/plus/internal/b;-><init>()V

    .line 1089
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/bd;->b:Lcom/google/android/gms/plus/internal/af;

    .line 1090
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V
    .locals 7

    .prologue
    .line 1095
    if-nez p2, :cond_0

    const/4 v0, 0x0

    .line 1097
    :goto_0
    new-instance v3, Lcom/google/android/gms/common/c;

    invoke-direct {v3, p1, v0}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    .line 1098
    iget-object v6, p0, Lcom/google/android/gms/plus/internal/bd;->a:Lcom/google/android/gms/plus/internal/ay;

    new-instance v0, Lcom/google/android/gms/plus/internal/be;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/bd;->a:Lcom/google/android/gms/plus/internal/ay;

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/bd;->b:Lcom/google/android/gms/plus/internal/af;

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/plus/internal/be;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/af;Lcom/google/android/gms/common/c;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/plus/internal/ay;->a(Lcom/google/android/gms/common/internal/al;)V

    .line 1099
    return-void

    .line 1095
    :cond_0
    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    goto :goto_0
.end method
