.class public final Lcom/google/android/gms/people/profile/AvatarActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/people/profile/h;


# instance fields
.field private final a:Lcom/google/android/gms/plus/internal/ad;

.field private b:Landroid/support/v4/app/m;

.field private c:Lcom/google/android/gms/common/api/v;

.field private d:Ljava/lang/String;

.field private e:I

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Landroid/net/Uri;

.field private j:Landroid/net/Uri;

.field private k:Landroid/net/Uri;

.field private l:Landroid/net/Uri;

.field private m:Z

.field private final n:Lcom/google/android/gms/common/api/aq;

.field private final o:Lcom/google/android/gms/common/api/aq;

.field private final p:Lcom/google/android/gms/common/api/aq;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 168
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;-><init>(Lcom/google/android/gms/plus/internal/ad;)V

    .line 169
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/plus/internal/ad;)V
    .locals 1

    .prologue
    .line 172
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 133
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->e:I

    .line 406
    new-instance v0, Lcom/google/android/gms/people/profile/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/profile/a;-><init>(Lcom/google/android/gms/people/profile/AvatarActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->n:Lcom/google/android/gms/common/api/aq;

    .line 452
    new-instance v0, Lcom/google/android/gms/people/profile/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/profile/b;-><init>(Lcom/google/android/gms/people/profile/AvatarActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->o:Lcom/google/android/gms/common/api/aq;

    .line 525
    new-instance v0, Lcom/google/android/gms/people/profile/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/profile/c;-><init>(Lcom/google/android/gms/people/profile/AvatarActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lcom/google/android/gms/common/api/aq;

    .line 173
    iput-object p1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->a:Lcom/google/android/gms/plus/internal/ad;

    .line 174
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/people/profile/AvatarActivity;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    return-object p1
.end method

.method private a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 561
    new-instance v0, Lcom/google/android/gms/common/server/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/analytics/j;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/y;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/server/y;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    .line 566
    if-eqz p2, :cond_0

    .line 567
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;-><init>()V

    iput-object p2, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->c:Ljava/lang/String;

    iget-object v2, v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->d:Ljava/util/Set;

    const/16 v3, 0x41

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/aa;->a()Lcom/google/android/gms/plus/service/v1whitelisted/models/z;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)Lcom/google/android/gms/common/server/y;

    .line 571
    :cond_0
    invoke-static {p0, v1}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/y;)V

    .line 572
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/people/profile/AvatarActivity;I)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/profile/AvatarActivity;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/people/profile/AvatarActivity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/people/profile/AvatarActivity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/people/profile/AvatarActivity;)Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->h:Z

    return v0
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 553
    const/4 v0, 0x0

    invoke-static {p0, p1, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 554
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/people/profile/AvatarActivity;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 66
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->i:Landroid/net/Uri;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->m:Z

    if-nez v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    invoke-static {}, Lcom/google/android/gms/people/profile/g;->b()Lcom/google/android/gms/people/profile/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "source_dialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/profile/g;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->i:Landroid/net/Uri;

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    if-eqz v2, :cond_4

    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->m:Z

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->l:Landroid/net/Uri;

    if-nez v2, :cond_4

    :goto_2
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->i:Landroid/net/Uri;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->i:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Landroid/net/Uri;)V

    goto :goto_1

    :cond_4
    move v0, v1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Landroid/net/Uri;)V

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/gms/people/profile/AvatarActivity;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->e()V

    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/people/profile/AvatarActivity;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    return-object v0
.end method

.method private d()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 503
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 504
    const-string v0, "People.Avatar"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setAvatar "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->l:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->i:Landroid/net/Uri;

    if-eqz v0, :cond_1

    move v5, v6

    .line 511
    :goto_0
    sget-object v0, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->g:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->l:Landroid/net/Uri;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->p:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 514
    iput-boolean v6, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->m:Z

    .line 519
    invoke-static {}, Lcom/google/android/gms/people/profile/d;->b()Lcom/google/android/gms/people/profile/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->b:Landroid/support/v4/app/m;

    .line 520
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 521
    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->b:Landroid/support/v4/app/m;

    const-string v2, "progress_dialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    .line 522
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    .line 523
    return-void

    .line 509
    :cond_1
    const/4 v5, 0x0

    goto :goto_0
.end method

.method private e()V
    .locals 1

    .prologue
    .line 575
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(I)V

    .line 576
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->finish()V

    .line 577
    return-void
.end method

.method static synthetic e(Lcom/google/android/gms/people/profile/AvatarActivity;)Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->m:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/people/profile/AvatarActivity;)Landroid/support/v4/app/m;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->b:Landroid/support/v4/app/m;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 580
    sget-object v0, Lcom/google/android/gms/common/analytics/q;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    const-string v1, "2"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    .line 581
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(I)V

    .line 582
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->finish()V

    .line 583
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 422
    const-string v0, "camera-avatar.jpg"

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/people/profile/f;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->i:Landroid/net/Uri;

    .line 424
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->i:Landroid/net/Uri;

    if-nez v0, :cond_0

    .line 425
    const-string v0, "People.Avatar"

    const-string v1, "Failed to create temp file to take photo"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    sget v0, Lcom/google/android/gms/p;->rH:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->b(I)V

    .line 427
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->f()V

    .line 437
    :goto_0
    return-void

    .line 430
    :cond_0
    invoke-static {v3}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 431
    const-string v0, "People.Avatar"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onTakePhoto "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->i:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 433
    :cond_1
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 434
    const-string v1, "output"

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->i:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 435
    invoke-virtual {v0, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 436
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 3

    .prologue
    .line 495
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/people/profile/AvatarPreviewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 496
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 497
    const-string v1, "com.google.android.gms.people.profile.EXTRA_ACCOUNT"

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 498
    const-string v1, "com.google.android.gms.people.profile.EXTRA_URI"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 499
    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 500
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 401
    sget v0, Lcom/google/android/gms/p;->rH:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->b(I)V

    .line 402
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->e()V

    .line 403
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 441
    sget-object v0, Landroid/net/Uri;->EMPTY:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    .line 442
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.PICK"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 443
    const-string v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 444
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 445
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 384
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->h:Z

    if-nez v0, :cond_1

    .line 385
    sget-object v0, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->f:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->n:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 393
    :cond_0
    :goto_0
    return-void

    .line 387
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->k:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 388
    sget-object v0, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->k:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->o:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0

    .line 390
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->i:Landroid/net/Uri;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    if-eqz v0, :cond_4

    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->m:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->l:Landroid/net/Uri;

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_0

    .line 391
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->d()V

    goto :goto_0

    .line 390
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 449
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->f()V

    .line 450
    return-void
.end method

.method public final f_(I)V
    .locals 0

    .prologue
    .line 397
    return-void
.end method

.method protected final onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v7, 0x2

    const/4 v6, -0x1

    .line 324
    invoke-static {v7}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 325
    const-string v2, "People.Avatar"

    const-string v3, "onActivityResult requestCode=%d resultCode=%d"

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 328
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 380
    :cond_1
    :goto_0
    return-void

    .line 330
    :pswitch_0
    if-ne p2, v6, :cond_8

    .line 331
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    .line 332
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 333
    :cond_2
    const-string v0, "People.Avatar"

    const-string v1, "Empty data returned from pick photo"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    sget v0, Lcom/google/android/gms/p;->rH:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->b(I)V

    .line 335
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->f()V

    goto :goto_0

    .line 338
    :cond_3
    invoke-static {v7}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 339
    const-string v2, "People.Avatar"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pick photo returned "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    if-eqz v2, :cond_6

    const-string v3, "http"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    const-string v3, "https"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    :cond_5
    :goto_1
    if-eqz v0, :cond_7

    .line 342
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->k:Landroid/net/Uri;

    .line 343
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    .line 344
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 345
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->k:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    .line 346
    sget-object v1, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->o:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto/16 :goto_0

    :cond_6
    move v0, v1

    .line 341
    goto :goto_1

    .line 350
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 353
    :cond_8
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->f()V

    goto/16 :goto_0

    .line 359
    :pswitch_1
    if-ne p2, v6, :cond_9

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->i:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 362
    :cond_9
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->f()V

    goto/16 :goto_0

    .line 368
    :pswitch_2
    if-ne p2, v6, :cond_a

    .line 369
    const-string v0, "com.google.android.gms.people.profile.EXTRA_URI"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->l:Landroid/net/Uri;

    .line 370
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 371
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->d()V

    goto/16 :goto_0

    .line 374
    :cond_a
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->f()V

    goto/16 :goto_0

    .line 328
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 178
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 179
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 180
    const-string v0, "People.Avatar"

    const-string v1, "This activity is not available for restricted profile."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    invoke-virtual {p0, v3}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(I)V

    .line 182
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->finish()V

    .line 255
    :cond_0
    :goto_0
    return-void

    .line 188
    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->c(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->d:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 196
    if-eqz p1, :cond_2

    .line 197
    const-string v0, "app_id"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->e:I

    .line 198
    const-string v0, "account_name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->f:Ljava/lang/String;

    .line 199
    const-string v0, "page_gaia_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->g:Ljava/lang/String;

    .line 200
    const-string v0, "owner_loaded"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->h:Z

    .line 201
    const-string v0, "take_photo_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->i:Landroid/net/Uri;

    .line 202
    const-string v0, "pick_photo_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    .line 203
    const-string v0, "remote_pick_photo_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->k:Landroid/net/Uri;

    .line 204
    const-string v0, "cropped_photo_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->l:Landroid/net/Uri;

    .line 205
    const-string v0, "result_pending"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->m:Z

    .line 209
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->f:Ljava/lang/String;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->g:Ljava/lang/String;

    if-nez v0, :cond_3

    .line 210
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 211
    if-eqz v0, :cond_3

    .line 212
    const-string v1, "com.google.android.gms.people.profile.EXTRA_ACCOUNT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->f:Ljava/lang/String;

    .line 213
    const-string v1, "com.google.android.gms.people.profile.EXTRA_PAGE_ID"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->g:Ljava/lang/String;

    .line 217
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 218
    const-string v0, "People.Avatar"

    const-string v1, "Profile image account name is unspecified"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    invoke-virtual {p0, v3}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(I)V

    .line 220
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->finish()V

    goto/16 :goto_0

    .line 190
    :catch_0
    move-exception v0

    const-string v0, "People.Avatar"

    const-string v1, "Not allowed"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 191
    invoke-virtual {p0, v3}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(I)V

    .line 192
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->finish()V

    goto/16 :goto_0

    .line 225
    :cond_4
    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->e:I

    if-ne v0, v2, :cond_6

    .line 226
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 227
    if-eqz v0, :cond_5

    .line 228
    const-string v1, "com.google.android.gms.people.profile.EXTRA_APP_ID"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->e:I

    .line 231
    :cond_5
    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->e:I

    if-ne v0, v2, :cond_6

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->d:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->e:I

    .line 237
    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->e:I

    if-ne v0, v2, :cond_6

    .line 238
    const-string v0, "People.Avatar"

    const-string v1, "EXTRA_SOCIAL_CLIENT_APP_ID must be set"

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 239
    invoke-virtual {p0, v3}, Lcom/google/android/gms/people/profile/AvatarActivity;->setResult(I)V

    .line 240
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarActivity;->finish()V

    goto/16 :goto_0

    .line 246
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->a:Lcom/google/android/gms/plus/internal/ad;

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->e:I

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->d:Ljava/lang/String;

    invoke-interface {v0, p0, v1, v2}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;ILjava/lang/String;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/y;)V

    .line 252
    if-nez p1, :cond_0

    .line 253
    sget-object v0, Lcom/google/android/gms/common/analytics/q;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    const-string v1, "2"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarActivity;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 294
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 295
    const-string v0, "app_id"

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->e:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 296
    const-string v0, "account_name"

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    const-string v0, "page_gaia_id"

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 298
    const-string v0, "owner_loaded"

    iget-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 299
    const-string v0, "take_photo_uri"

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->i:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 300
    const-string v0, "pick_photo_uri"

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->j:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 301
    const-string v0, "remote_pick_photo_uri"

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->k:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 302
    const-string v0, "cropped_photo_uri"

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->l:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 303
    const-string v0, "result_pending"

    iget-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->m:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 304
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 308
    invoke-super {p0}, Landroid/support/v4/app/q;->onStart()V

    .line 309
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 312
    :cond_0
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 316
    invoke-super {p0}, Landroid/support/v4/app/q;->onStop()V

    .line 317
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarActivity;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 320
    :cond_1
    return-void
.end method
