.class public final Lcom/google/android/gms/icing/c/a/u;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/icing/c/a/u;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/android/gms/icing/c/a/w;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1273
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1274
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/u;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/icing/c/a/u;->b:Lcom/google/android/gms/icing/c/a/w;

    iput-object v1, p0, Lcom/google/android/gms/icing/c/a/u;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/u;->cachedSize:I

    .line 1275
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/c/a/u;
    .locals 2

    .prologue
    .line 1256
    sget-object v0, Lcom/google/android/gms/icing/c/a/u;->c:[Lcom/google/android/gms/icing/c/a/u;

    if-nez v0, :cond_1

    .line 1257
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1259
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/c/a/u;->c:[Lcom/google/android/gms/icing/c/a/u;

    if-nez v0, :cond_0

    .line 1260
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/c/a/u;

    sput-object v0, Lcom/google/android/gms/icing/c/a/u;->c:[Lcom/google/android/gms/icing/c/a/u;

    .line 1262
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1264
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/c/a/u;->c:[Lcom/google/android/gms/icing/c/a/u;

    return-object v0

    .line 1262
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1338
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1339
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/u;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1340
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/u;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1343
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/u;->b:Lcom/google/android/gms/icing/c/a/w;

    if-eqz v1, :cond_1

    .line 1344
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/u;->b:Lcom/google/android/gms/icing/c/a/w;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1347
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1287
    if-ne p1, p0, :cond_1

    .line 1288
    const/4 v0, 0x1

    .line 1310
    :cond_0
    :goto_0
    return v0

    .line 1290
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/u;

    if-eqz v1, :cond_0

    .line 1293
    check-cast p1, Lcom/google/android/gms/icing/c/a/u;

    .line 1294
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/u;->a:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 1295
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/u;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1301
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/u;->b:Lcom/google/android/gms/icing/c/a/w;

    if-nez v1, :cond_5

    .line 1302
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/u;->b:Lcom/google/android/gms/icing/c/a/w;

    if-nez v1, :cond_0

    .line 1310
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/u;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1298
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/u;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/u;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1306
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/u;->b:Lcom/google/android/gms/icing/c/a/w;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/u;->b:Lcom/google/android/gms/icing/c/a/w;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/c/a/w;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1315
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/u;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1318
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/u;->b:Lcom/google/android/gms/icing/c/a/w;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1320
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/u;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1321
    return v0

    .line 1315
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/u;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1318
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/u;->b:Lcom/google/android/gms/icing/c/a/w;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/c/a/w;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1250
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/u;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/u;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/u;->b:Lcom/google/android/gms/icing/c/a/w;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/c/a/w;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/w;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/u;->b:Lcom/google/android/gms/icing/c/a/w;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/u;->b:Lcom/google/android/gms/icing/c/a/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1327
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/u;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1328
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/u;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1330
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/u;->b:Lcom/google/android/gms/icing/c/a/w;

    if-eqz v0, :cond_1

    .line 1331
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/u;->b:Lcom/google/android/gms/icing/c/a/w;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1333
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1334
    return-void
.end method
