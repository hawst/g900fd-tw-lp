.class public final Lcom/google/android/gms/people/c/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/people/c/a;

.field public final b:Landroid/database/sqlite/SQLiteDatabase;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/people/c/f;

.field private final e:Z

.field private f:I

.field private g:Ljava/util/BitSet;

.field private h:Z


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/people/c/f;Lcom/google/android/gms/people/c/a;Landroid/database/sqlite/SQLiteDatabase;Z)V
    .locals 1

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/c/e;->g:Ljava/util/BitSet;

    .line 53
    iput-object p1, p0, Lcom/google/android/gms/people/c/e;->c:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lcom/google/android/gms/people/c/e;->d:Lcom/google/android/gms/people/c/f;

    .line 55
    iput-object p3, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    .line 56
    iput-object p4, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    .line 57
    iput-boolean p5, p0, Lcom/google/android/gms/people/c/e;->e:Z

    .line 58
    return-void
.end method

.method private f()V
    .locals 3

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/google/android/gms/people/c/e;->e:Z

    if-nez v0, :cond_0

    .line 84
    const-string v0, "PeopleDatabase"

    const-string v1, "Write detected on readonly db"

    new-instance v2, Lcom/google/android/gms/people/internal/bb;

    invoke-direct {v2}, Lcom/google/android/gms/people/internal/bb;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 87
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 309
    invoke-direct {p0}, Lcom/google/android/gms/people/c/e;->f()V

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v0, v0, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 312
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 314
    iget-object v1, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v1, v1, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v1, v1, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 331
    invoke-direct {p0}, Lcom/google/android/gms/people/c/e;->f()V

    .line 332
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v0, v0, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 334
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 336
    iget-object v1, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v1, v1, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v1, v1, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Landroid/content/ContentValues;)J
    .locals 3

    .prologue
    .line 276
    invoke-direct {p0}, Lcom/google/android/gms/people/c/e;->f()V

    .line 277
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v0, v0, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 279
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 281
    iget-object v2, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v2, v2, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-wide v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v1, v1, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/String;J)J
    .locals 1

    .prologue
    .line 394
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0, p1, p2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide p3

    .line 396
    :goto_0
    return-wide p3

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 409
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0, p1, p2}, Landroid/database/DatabaseUtils;->stringForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteDoneException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p3

    .line 411
    :goto_0
    return-object p3

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 90
    invoke-direct {p0}, Lcom/google/android/gms/people/c/e;->f()V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v0, v0, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->g:Ljava/util/BitSet;

    iget v1, p0, Lcom/google/android/gms/people/c/e;->f:I

    invoke-virtual {v0, v1, v3}, Ljava/util/BitSet;->set(IZ)V

    .line 96
    iget v0, p0, Lcom/google/android/gms/people/c/e;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/people/c/e;->f:I

    .line 98
    const-string v0, "PeopleTx"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    const-string v0, "PeopleDatabase"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Tx #"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/people/c/e;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " start"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 102
    :cond_0
    iget v0, p0, Lcom/google/android/gms/people/c/e;->f:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 104
    iput-boolean v3, p0, Lcom/google/android/gms/people/c/e;->h:Z

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->d:Lcom/google/android/gms/people/c/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->j()V

    .line 109
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 227
    invoke-direct {p0}, Lcom/google/android/gms/people/c/e;->f()V

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v0, v0, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 230
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v0, v0, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 233
    return-void

    .line 232
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v1, v1, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/google/android/gms/people/c/e;->f()V

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v0, v0, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 240
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v0, v0, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 243
    return-void

    .line 242
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v1, v1, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    .line 185
    invoke-direct {p0}, Lcom/google/android/gms/people/c/e;->f()V

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/a;->a()V

    .line 189
    iget v0, p0, Lcom/google/android/gms/people/c/e;->f:I

    .line 190
    const-string v1, "PeopleTx"

    invoke-static {v1, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 191
    const-string v1, "PeopleDatabase"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Tx #"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " finishing"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    :cond_0
    iget v1, p0, Lcom/google/android/gms/people/c/e;->f:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/google/android/gms/people/c/e;->f:I

    .line 196
    iget-object v1, p0, Lcom/google/android/gms/people/c/e;->g:Ljava/util/BitSet;

    iget v2, p0, Lcom/google/android/gms/people/c/e;->f:I

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 198
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/people/c/e;->h:Z

    .line 199
    if-nez p1, :cond_1

    .line 200
    const-string v1, "PeopleDatabase"

    const-string v2, "Transaction rolling back"

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 201
    iget-object v1, p0, Lcom/google/android/gms/people/c/e;->c:Landroid/content/Context;

    const-string v2, "PeopleDatabase"

    const-string v3, "Transaction rolling back"

    new-instance v4, Lcom/google/android/gms/people/internal/bb;

    invoke-direct {v4}, Lcom/google/android/gms/people/internal/bb;-><init>()V

    invoke-static {v1, v2, v3, v4}, Lcom/google/android/gms/people/debug/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 205
    :cond_1
    iget v1, p0, Lcom/google/android/gms/people/c/e;->f:I

    if-nez v1, :cond_3

    .line 207
    iget-boolean v1, p0, Lcom/google/android/gms/people/c/e;->h:Z

    if-nez v1, :cond_4

    .line 208
    const-string v1, "PeopleTx"

    invoke-static {v1, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 209
    const-string v1, "PeopleDatabase"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Tx #"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " COMMIT"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->d:Lcom/google/android/gms/people/c/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->l()V

    .line 221
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 223
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v0, v0, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 224
    return-void

    .line 215
    :cond_4
    const-string v1, "PeopleTx"

    invoke-static {v1, v5}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 216
    const-string v1, "PeopleDatabase"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Tx #"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ROLLBACK"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->d:Lcom/google/android/gms/people/c/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->m()V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Landroid/content/ContentValues;)J
    .locals 3

    .prologue
    .line 287
    invoke-direct {p0}, Lcom/google/android/gms/people/c/e;->f()V

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v0, v0, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 290
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 292
    iget-object v2, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v2, v2, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    return-wide v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    iget-object v1, v1, Lcom/google/android/gms/people/c/a;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method public final b(Ljava/lang/String;[Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-static {v0, p1, p2}, Landroid/database/DatabaseUtils;->longForQuery(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b()Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 124
    invoke-direct {p0}, Lcom/google/android/gms/people/c/e;->f()V

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/a;->a()V

    .line 128
    iget v0, p0, Lcom/google/android/gms/people/c/e;->f:I

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->g:Ljava/util/BitSet;

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    const-string v3, "Trying to yield after setTransactionSuccessful"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 131
    iget-boolean v0, p0, Lcom/google/android/gms/people/c/e;->h:Z

    if-nez v0, :cond_0

    move v2, v1

    :cond_0
    const-string v0, "Trying to yield on failed transaction."

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 133
    const-string v0, "PeopleTx"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 134
    const-string v0, "PeopleDatabase"

    const-string v2, "Tx yielding"

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->d:Lcom/google/android/gms/people/c/f;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->k()V

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 141
    cmp-long v0, v4, v4

    if-lez v0, :cond_2

    .line 143
    const-wide/16 v2, 0x0

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 147
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 149
    return v1

    :cond_3
    move v0, v2

    .line 128
    goto :goto_0

    :cond_4
    move v0, v2

    .line 129
    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/android/gms/people/c/e;->f()V

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->a:Lcom/google/android/gms/people/c/a;

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/a;->a()V

    .line 157
    const-string v0, "PeopleTx"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 158
    const-string v0, "PeopleDatabase"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "  Tx #"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/people/c/e;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " success"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->g:Ljava/util/BitSet;

    iget v1, p0, Lcom/google/android/gms/people/c/e;->f:I

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/BitSet;->set(IZ)V

    .line 163
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 363
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 364
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/gms/people/c/e;->b:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->inTransaction()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 369
    return-void

    .line 368
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
