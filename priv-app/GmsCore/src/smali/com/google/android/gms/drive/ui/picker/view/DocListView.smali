.class public Lcom/google/android/gms/drive/ui/picker/view/DocListView;
.super Lcom/google/android/gms/drive/ui/picker/view/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/ui/picker/a/t;
.implements Lcom/google/android/gms/drive/ui/picker/view/i;


# instance fields
.field private c:Landroid/support/v4/app/Fragment;

.field private d:Lcom/google/android/gms/drive/ui/picker/a/t;

.field private e:Ljava/lang/String;

.field private final f:Lcom/google/android/gms/drive/g/q;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/ListView;

.field private i:Ljava/lang/String;

.field private j:Lcom/google/android/gms/drive/DriveId;

.field private k:Lcom/google/android/gms/drive/ui/picker/view/h;

.field private l:Lcom/google/android/gms/drive/ui/picker/a/k;

.field private m:Lcom/google/android/gms/drive/ui/picker/view/e;

.field private n:Z

.field private o:Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

.field private final p:Ljava/lang/Thread;

.field private q:Lcom/google/android/gms/drive/ui/picker/a/a/t;

.field private r:Lcom/google/android/gms/drive/ui/picker/a/a/g;

.field private final s:J

.field private final t:Ljava/util/Set;

.field private u:Lcom/google/android/gms/drive/ui/picker/a/o;

.field private v:Lcom/google/android/gms/drive/ui/picker/a/s;

.field private w:Lcom/google/android/gms/common/api/am;

.field private x:Lcom/google/android/gms/common/api/am;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    .line 144
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/ui/picker/view/j;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 108
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->j:Lcom/google/android/gms/drive/DriveId;

    .line 110
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/view/h;->c:Lcom/google/android/gms/drive/ui/picker/view/h;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->k:Lcom/google/android/gms/drive/ui/picker/view/h;

    .line 132
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->t:Ljava/util/Set;

    .line 146
    new-instance v0, Lcom/google/android/gms/drive/g/q;

    invoke-direct {v0}, Lcom/google/android/gms/drive/g/q;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->f:Lcom/google/android/gms/drive/g/q;

    .line 148
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->p:Ljava/lang/Thread;

    .line 150
    const/4 v1, 0x0

    sget-object v0, Lcom/google/android/gms/drive/ai;->X:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->s:J

    .line 152
    return-void
.end method

.method private a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 516
    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->h:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->h:Landroid/widget/ListView;

    if-ne p1, v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ListView;->setVisibility(I)V

    .line 517
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->g:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->g:Landroid/view/View;

    if-ne p1, v3, :cond_1

    :goto_1
    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 518
    return-void

    :cond_0
    move v0, v2

    .line 516
    goto :goto_0

    :cond_1
    move v1, v2

    .line 517
    goto :goto_1
.end method

.method private a(Lcom/google/android/gms/common/api/am;)V
    .locals 1

    .prologue
    .line 386
    if-eqz p1, :cond_0

    .line 387
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l()Lcom/google/android/gms/drive/ui/picker/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/ui/picker/j;->b(Lcom/google/android/gms/common/api/am;)V

    .line 389
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/view/DocListView;Lcom/google/android/gms/drive/ui/picker/a/k;ZLcom/google/android/gms/drive/k;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 67
    invoke-interface {p3}, Lcom/google/android/gms/drive/k;->c()Z

    move-result v0

    iput-boolean v0, p1, Lcom/google/android/gms/drive/ui/picker/a/k;->b:Z

    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/x;

    invoke-interface {p3}, Lcom/google/android/gms/drive/k;->b()Lcom/google/android/gms/drive/ak;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/ui/picker/a/x;-><init>(Lcom/google/android/gms/drive/ak;)V

    const-string v1, "DocListView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Query returns "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/l;->h()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " items"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->r:Lcom/google/android/gms/drive/ui/picker/a/a/g;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/g;->a(Lcom/google/android/gms/drive/ui/picker/a/l;)V

    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->k()Lcom/google/android/gms/drive/ui/picker/a/n;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->r:Lcom/google/android/gms/drive/ui/picker/a/a/g;

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/drive/ui/picker/a/n;->a(Lcom/google/android/gms/drive/ui/picker/a/l;Lcom/google/android/gms/drive/ui/picker/a/a/g;)V

    if-eqz p2, :cond_1

    invoke-virtual {p0, v5}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Z)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->m:Lcom/google/android/gms/drive/ui/picker/view/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->m:Lcom/google/android/gms/drive/ui/picker/view/e;

    iget-object v2, v0, Lcom/google/android/gms/drive/ui/picker/view/e;->b:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-static {v2}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->e(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)V

    iput-boolean v5, v0, Lcom/google/android/gms/drive/ui/picker/view/e;->a:Z

    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/view/e;

    invoke-direct {v0, p0, v4}, Lcom/google/android/gms/drive/ui/picker/view/e;-><init>(Lcom/google/android/gms/drive/ui/picker/view/DocListView;B)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->m:Lcom/google/android/gms/drive/ui/picker/view/e;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->o:Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->b()Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->h:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->r:Lcom/google/android/gms/drive/ui/picker/a/a/g;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/j;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    const/4 v2, 0x2

    const/high16 v3, 0x41d00000    # 26.0f

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v2, v3, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/l;->d(I)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->r:Lcom/google/android/gms/drive/ui/picker/a/a/g;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/j;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->a:Lcom/google/android/gms/drive/ui/picker/a/a/l;

    const/16 v2, 0x12c

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/drive/ui/picker/a/a/l;->a(ILandroid/content/res/Resources;)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->i()V

    invoke-direct {p0, v4}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->b(Z)V

    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->j()V

    return-void

    :cond_1
    invoke-interface {v1}, Lcom/google/android/gms/drive/ui/picker/a/n;->b()V

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Lcom/google/android/gms/drive/ui/picker/a/n;->a()V

    goto :goto_1
.end method

.method private a(Lcom/google/android/gms/drive/ui/picker/view/f;)V
    .locals 5

    .prologue
    .line 306
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->m()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 307
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 308
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/ui/picker/view/f;->a(Z)V

    .line 335
    :goto_0
    return-void

    .line 312
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l:Lcom/google/android/gms/drive/ui/picker/a/k;

    .line 313
    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/gms/drive/ui/picker/a/k;->c:Z

    .line 314
    iget-object v2, v1, Lcom/google/android/gms/drive/ui/picker/a/k;->a:Lcom/google/android/gms/drive/query/Query;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/query/Query;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->x:Lcom/google/android/gms/common/api/am;

    .line 315
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l()Lcom/google/android/gms/drive/ui/picker/j;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->x:Lcom/google/android/gms/common/api/am;

    new-instance v3, Lcom/google/android/gms/drive/ui/picker/view/b;

    invoke-direct {v3, p0, v1, p1}, Lcom/google/android/gms/drive/ui/picker/view/b;-><init>(Lcom/google/android/gms/drive/ui/picker/view/DocListView;Lcom/google/android/gms/drive/ui/picker/a/k;Lcom/google/android/gms/drive/ui/picker/view/f;)V

    new-instance v4, Lcom/google/android/gms/drive/ui/picker/view/c;

    invoke-direct {v4, p0, v1}, Lcom/google/android/gms/drive/ui/picker/view/c;-><init>(Lcom/google/android/gms/drive/ui/picker/view/DocListView;Lcom/google/android/gms/drive/ui/picker/a/k;)V

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/gms/drive/ui/picker/j;->a(Lcom/google/android/gms/common/api/am;Lcom/google/android/gms/common/api/aq;Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->y:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)V
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->b(Z)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->g:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->i()V

    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->k()Lcom/google/android/gms/drive/ui/picker/a/n;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/n;->b()V

    return-void
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x8

    .line 546
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->f()Lcom/google/android/gms/drive/ui/picker/a/q;

    .line 549
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l:Lcom/google/android/gms/drive/ui/picker/a/k;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l:Lcom/google/android/gms/drive/ui/picker/a/k;

    iget-boolean v0, v0, Lcom/google/android/gms/drive/ui/picker/a/k;->c:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l:Lcom/google/android/gms/drive/ui/picker/a/k;

    iget-boolean v0, v0, Lcom/google/android/gms/drive/ui/picker/a/k;->b:Z

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 555
    :goto_0
    if-eqz p1, :cond_2

    .line 556
    sget v0, Lcom/google/android/gms/j;->qr:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 557
    sget v0, Lcom/google/android/gms/j;->fr:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 558
    sget v0, Lcom/google/android/gms/j;->fs:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 567
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 549
    goto :goto_0

    .line 559
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->y:Z

    if-eqz v2, :cond_4

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->k:Lcom/google/android/gms/drive/ui/picker/view/h;

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/view/h;->c:Lcom/google/android/gms/drive/ui/picker/view/h;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/ui/picker/view/h;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 561
    :cond_3
    sget v0, Lcom/google/android/gms/j;->fr:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 562
    sget v0, Lcom/google/android/gms/j;->fs:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 563
    sget v0, Lcom/google/android/gms/j;->qr:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    .line 565
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->k:Lcom/google/android/gms/drive/ui/picker/view/h;

    sget v2, Lcom/google/android/gms/j;->qr:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    sget v2, Lcom/google/android/gms/j;->fr:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    sget v2, Lcom/google/android/gms/j;->fs:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/drive/ui/picker/view/h;->a:Lcom/google/android/gms/drive/ui/picker/view/h;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/ui/picker/view/h;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    sget v0, Lcom/google/android/gms/p;->gN:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    :goto_2
    sget v0, Lcom/google/android/gms/j;->eE:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_5
    sget v0, Lcom/google/android/gms/p;->gO:I

    invoke-virtual {v1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_2
.end method

.method static synthetic c(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)Lcom/google/android/gms/drive/ui/picker/a/k;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l:Lcom/google/android/gms/drive/ui/picker/a/k;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)V
    .locals 2

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->p:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)Lcom/google/android/gms/drive/ui/picker/a/n;
    .locals 1

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->k()Lcom/google/android/gms/drive/ui/picker/a/n;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->j()V

    return-void
.end method

.method private j()V
    .locals 1

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->y:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->k()Lcom/google/android/gms/drive/ui/picker/a/n;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/n;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    .line 182
    :goto_0
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->g:Landroid/view/View;

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Landroid/view/View;)V

    .line 183
    return-void

    .line 181
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 182
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->h:Landroid/widget/ListView;

    goto :goto_1
.end method

.method private k()Lcom/google/android/gms/drive/ui/picker/a/n;
    .locals 4

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->u:Lcom/google/android/gms/drive/ui/picker/a/o;

    if-nez v0, :cond_0

    .line 207
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/o;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->h:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->v:Lcom/google/android/gms/drive/ui/picker/a/s;

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/google/android/gms/drive/ui/picker/a/o;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/ui/picker/view/DocListView;Landroid/widget/ListView;Lcom/google/android/gms/drive/ui/picker/a/s;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->u:Lcom/google/android/gms/drive/ui/picker/a/o;

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->u:Lcom/google/android/gms/drive/ui/picker/a/o;

    return-object v0
.end method

.method private l()Lcom/google/android/gms/drive/ui/picker/j;
    .locals 1

    .prologue
    .line 298
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->e()Lcom/google/android/gms/drive/ui/picker/j;

    move-result-object v0

    return-object v0
.end method

.method private m()Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 302
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/h;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/h;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 155
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->c:Landroid/support/v4/app/Fragment;

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->c:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/Fragment;->registerForContextMenu(Landroid/view/View;)V

    .line 157
    return-void
.end method

.method public final a(Landroid/view/View;ILcom/google/android/gms/drive/DriveId;)V
    .locals 2

    .prologue
    .line 704
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->d:Lcom/google/android/gms/drive/ui/picker/a/t;

    if-eqz v0, :cond_0

    .line 705
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->o:Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->h:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->a(Landroid/os/Parcelable;)V

    .line 706
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->d:Lcom/google/android/gms/drive/ui/picker/a/t;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/drive/ui/picker/a/t;->a(Landroid/view/View;ILcom/google/android/gms/drive/DriveId;)V

    .line 708
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/DriveId;)V
    .locals 1

    .prologue
    .line 595
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->j:Lcom/google/android/gms/drive/DriveId;

    .line 596
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->b:Lcom/google/android/gms/drive/ui/picker/view/CustomListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/j;->b:Lcom/google/android/gms/drive/ui/picker/view/CustomListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/view/CustomListView;->invalidateViews()V

    .line 597
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 395
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->o:Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->x:Lcom/google/android/gms/common/api/am;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/common/api/am;)V

    .line 399
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->o:Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    .line 400
    invoke-virtual {p1}, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->a()Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;->a()Lcom/google/android/gms/drive/query/Query;

    move-result-object v3

    .line 401
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->q:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->q:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    .line 402
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/gms/drive/ui/picker/a/a/t;->a(Landroid/content/Context;)Lcom/google/android/gms/drive/ui/picker/a/a/g;

    move-result-object v0

    .line 404
    iput-boolean v1, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->y:Z

    .line 405
    invoke-direct {p0, v1}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->b(Z)V

    .line 406
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->j()V

    .line 408
    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->r:Lcom/google/android/gms/drive/ui/picker/a/a/g;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l:Lcom/google/android/gms/drive/ui/picker/a/k;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l:Lcom/google/android/gms/drive/ui/picker/a/k;

    iget-object v0, v0, Lcom/google/android/gms/drive/ui/picker/a/k;->a:Lcom/google/android/gms/drive/query/Query;

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_1
    move v0, v2

    :goto_1
    new-instance v4, Lcom/google/android/gms/drive/query/f;

    invoke-direct {v4, v3}, Lcom/google/android/gms/drive/query/f;-><init>(Lcom/google/android/gms/drive/query/Query;)V

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->r:Lcom/google/android/gms/drive/ui/picker/a/a/g;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/ui/picker/a/a/g;->a()Lcom/google/android/gms/drive/query/SortOrder;

    move-result-object v3

    iput-object v3, v4, Lcom/google/android/gms/drive/query/f;->a:Lcom/google/android/gms/drive/query/SortOrder;

    const/16 v3, 0x9

    new-array v3, v3, [Lcom/google/android/gms/drive/metadata/f;

    sget-object v5, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    aput-object v5, v3, v1

    sget-object v5, Lcom/google/android/gms/drive/metadata/internal/a/a;->a:Lcom/google/android/gms/drive/metadata/f;

    aput-object v5, v3, v2

    const/4 v2, 0x2

    sget-object v5, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    aput-object v5, v3, v2

    const/4 v2, 0x3

    sget-object v5, Lcom/google/android/gms/drive/metadata/internal/a/a;->o:Lcom/google/android/gms/drive/metadata/f;

    aput-object v5, v3, v2

    const/4 v2, 0x4

    sget-object v5, Lcom/google/android/gms/drive/metadata/internal/a/a;->m:Lcom/google/android/gms/drive/metadata/internal/a/d;

    aput-object v5, v3, v2

    const/4 v2, 0x5

    sget-object v5, Lcom/google/android/gms/drive/metadata/internal/a/l;->c:Lcom/google/android/gms/drive/metadata/internal/a/p;

    aput-object v5, v3, v2

    const/4 v2, 0x6

    sget-object v5, Lcom/google/android/gms/drive/metadata/internal/a/l;->e:Lcom/google/android/gms/drive/metadata/internal/a/q;

    aput-object v5, v3, v2

    const/4 v2, 0x7

    sget-object v5, Lcom/google/android/gms/drive/metadata/internal/a/a;->y:Lcom/google/android/gms/drive/metadata/internal/a/h;

    aput-object v5, v3, v2

    const/16 v2, 0x8

    sget-object v5, Lcom/google/android/gms/drive/metadata/internal/a/l;->b:Lcom/google/android/gms/drive/metadata/internal/a/n;

    aput-object v5, v3, v2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, v4, Lcom/google/android/gms/drive/query/f;->b:Ljava/util/List;

    array-length v2, v3

    :goto_2
    if-ge v1, v2, :cond_4

    aget-object v5, v3, v1

    iget-object v6, v4, Lcom/google/android/gms/drive/query/f;->b:Ljava/util/List;

    invoke-interface {v5}, Lcom/google/android/gms/drive/metadata/f;->a()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 401
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->f()Lcom/google/android/gms/drive/ui/picker/a/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/a/q;->a()Lcom/google/android/gms/drive/ui/picker/a/a/t;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    move v0, v1

    .line 408
    goto :goto_1

    :cond_4
    invoke-virtual {v4}, Lcom/google/android/gms/drive/query/f;->a()Lcom/google/android/gms/drive/query/Query;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/drive/ui/picker/a/k;

    invoke-direct {v2, v1}, Lcom/google/android/gms/drive/ui/picker/a/k;-><init>(Lcom/google/android/gms/drive/query/Query;)V

    iput-object v2, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l:Lcom/google/android/gms/drive/ui/picker/a/k;

    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->k()Lcom/google/android/gms/drive/ui/picker/a/n;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/drive/ui/picker/a/n;->b()V

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->x:Lcom/google/android/gms/common/api/am;

    invoke-direct {p0, v3}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/common/api/am;)V

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->w:Lcom/google/android/gms/common/api/am;

    invoke-direct {p0, v3}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/common/api/am;)V

    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->m()Lcom/google/android/gms/common/api/v;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v4

    if-nez v4, :cond_6

    const-string v0, "DocListView"

    const-string v1, "updateDatabaseQuery client not connected, deferring to onClientConnected"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    :goto_3
    return-void

    .line 408
    :cond_6
    sget-object v4, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    invoke-interface {v4, v3, v1}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/query/Query;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->w:Lcom/google/android/gms/common/api/am;

    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l()Lcom/google/android/gms/drive/ui/picker/j;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->w:Lcom/google/android/gms/common/api/am;

    new-instance v4, Lcom/google/android/gms/drive/ui/picker/view/d;

    invoke-direct {v4, p0, v2, v0}, Lcom/google/android/gms/drive/ui/picker/view/d;-><init>(Lcom/google/android/gms/drive/ui/picker/view/DocListView;Lcom/google/android/gms/drive/ui/picker/a/k;Z)V

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/drive/ui/picker/j;->a(Lcom/google/android/gms/common/api/am;Lcom/google/android/gms/common/api/aq;)V

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/drive/ui/picker/a/a/t;)V
    .locals 1

    .prologue
    .line 530
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->q:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    .line 531
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/ui/picker/a/s;)V
    .locals 0

    .prologue
    .line 725
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->v:Lcom/google/android/gms/drive/ui/picker/a/s;

    .line 726
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/ui/picker/a/t;)V
    .locals 0

    .prologue
    .line 271
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->d:Lcom/google/android/gms/drive/ui/picker/a/t;

    .line 272
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/ui/picker/view/h;)V
    .locals 1

    .prologue
    .line 534
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->k:Lcom/google/android/gms/drive/ui/picker/view/h;

    .line 535
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->b(Z)V

    .line 536
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 257
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->i:Ljava/lang/String;

    .line 258
    return-void
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 338
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->i:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 341
    sget-object v0, Lcom/google/android/gms/drive/af;->c:Lcom/google/android/gms/drive/af;

    invoke-static {v0}, Lcom/google/android/gms/drive/ag;->a(Lcom/google/android/gms/drive/af;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 378
    :goto_0
    return-void

    .line 345
    :cond_0
    if-eqz p1, :cond_1

    .line 346
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l:Lcom/google/android/gms/drive/ui/picker/a/k;

    iget-boolean v0, v0, Lcom/google/android/gms/drive/ui/picker/a/k;->c:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l:Lcom/google/android/gms/drive/ui/picker/a/k;

    iget-boolean v0, v0, Lcom/google/android/gms/drive/ui/picker/a/k;->b:Z

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/drive/ai;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->k()Lcom/google/android/gms/drive/ui/picker/a/n;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/n;->d()I

    move-result v3

    sget-object v0, Lcom/google/android/gms/drive/ai;->K:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v3, v0, :cond_2

    move v0, v1

    .line 352
    :goto_1
    if-eqz v0, :cond_1

    .line 354
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/view/f;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/drive/ui/picker/view/f;-><init>(Lcom/google/android/gms/drive/ui/picker/view/DocListView;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/drive/ui/picker/view/f;)V

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l:Lcom/google/android/gms/drive/ui/picker/a/k;

    iget-boolean v3, v0, Lcom/google/android/gms/drive/ui/picker/a/k;->c:Z

    .line 359
    if-nez v3, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l:Lcom/google/android/gms/drive/ui/picker/a/k;

    iget-boolean v0, v0, Lcom/google/android/gms/drive/ui/picker/a/k;->b:Z

    if-nez v0, :cond_3

    .line 360
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->k()Lcom/google/android/gms/drive/ui/picker/a/n;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/n;->b()V

    .line 374
    :goto_2
    invoke-direct {p0, v2}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->b(Z)V

    .line 375
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->j()V

    .line 376
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->h:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->requestLayout()V

    .line 377
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->h:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->invalidate()V

    goto :goto_0

    :cond_2
    move v0, v2

    .line 346
    goto :goto_1

    .line 363
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 364
    if-eqz v3, :cond_4

    .line 365
    sget v4, Lcom/google/android/gms/p;->gM:I

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 372
    :goto_3
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->k()Lcom/google/android/gms/drive/ui/picker/a/n;

    move-result-object v4

    if-nez v3, :cond_5

    :goto_4
    invoke-interface {v4, v1, v0}, Lcom/google/android/gms/drive/ui/picker/a/n;->a(ZLjava/lang/String;)V

    goto :goto_2

    .line 370
    :cond_4
    sget v4, Lcom/google/android/gms/p;->gK:I

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    :cond_5
    move v1, v2

    .line 372
    goto :goto_4
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 381
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/view/f;

    const/4 v1, 0x1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/drive/ui/picker/view/f;-><init>(Lcom/google/android/gms/drive/ui/picker/view/DocListView;Z)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/drive/ui/picker/view/f;)V

    .line 382
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Z)V

    .line 383
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 604
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->y:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->o:Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    if-eqz v0, :cond_1

    .line 606
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->z:Z

    .line 607
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->o:Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;)V

    .line 611
    :cond_0
    :goto_0
    return-void

    .line 608
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->z:Z

    if-eqz v0, :cond_0

    .line 609
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->d()V

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 619
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->l:Lcom/google/android/gms/drive/ui/picker/a/k;

    if-nez v0, :cond_0

    .line 620
    iput-boolean v3, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->z:Z

    .line 631
    :goto_0
    return-void

    .line 623
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->m()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 624
    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->m:Lcom/google/android/gms/drive/ui/picker/view/e;

    if-nez v0, :cond_2

    .line 625
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->z:Z

    goto :goto_0

    .line 627
    :cond_2
    iput-boolean v3, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->z:Z

    .line 628
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/h;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/h;->getSupportLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->m:Lcom/google/android/gms/drive/ui/picker/view/e;

    invoke-virtual {v0, v3, v1, v2}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    goto :goto_0
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 680
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/picker/view/j;->e()V

    .line 682
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->x:Lcom/google/android/gms/common/api/am;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/common/api/am;)V

    .line 683
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->w:Lcom/google/android/gms/common/api/am;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/common/api/am;)V

    .line 684
    return-void
.end method

.method public final f()Lcom/google/android/gms/drive/ui/picker/a/q;
    .locals 2

    .prologue
    .line 687
    const/4 v0, 0x0

    .line 688
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->o:Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    if-eqz v1, :cond_0

    .line 689
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->o:Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->a()Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;->b()Lcom/google/android/gms/drive/ui/picker/a/q;

    move-result-object v0

    .line 691
    :cond_0
    if-nez v0, :cond_1

    .line 692
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/q;->a:Lcom/google/android/gms/drive/ui/picker/a/q;

    .line 694
    :cond_1
    return-object v0
.end method

.method public final g()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 699
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->j:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final h()V
    .locals 1

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->r:Lcom/google/android/gms/drive/ui/picker/a/a/g;

    if-eqz v0, :cond_0

    .line 712
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->r:Lcom/google/android/gms/drive/ui/picker/a/a/g;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/a/a/g;->e()V

    .line 714
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 161
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/picker/view/j;->onAttachedToWindow()V

    .line 162
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->n:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 163
    return-void

    .line 162
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->n:Z

    .line 168
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/picker/view/j;->onDetachedFromWindow()V

    .line 169
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 187
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/picker/view/j;->onFinishInflate()V

    .line 188
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->h:Landroid/widget/ListView;

    .line 189
    sget v0, Lcom/google/android/gms/j;->fq:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->g:Landroid/view/View;

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->h:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->h:Landroid/widget/ListView;

    new-instance v1, Lcom/google/android/gms/drive/ui/picker/view/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/ui/picker/view/a;-><init>(Lcom/google/android/gms/drive/ui/picker/view/DocListView;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 203
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 508
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_0

    .line 509
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 510
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result p2

    .line 512
    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/drive/ui/picker/view/j;->onMeasure(II)V

    .line 513
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const-wide/16 v8, 0x0

    .line 216
    invoke-super {p0, p1, p2, p3, p4}, Lcom/google/android/gms/drive/ui/picker/view/j;->onScroll(Landroid/widget/AbsListView;III)V

    .line 217
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$OnScrollListener;

    .line 218
    invoke-interface {v0, p1, p2, p3, p4}, Landroid/widget/AbsListView$OnScrollListener;->onScroll(Landroid/widget/AbsListView;III)V

    goto :goto_0

    .line 220
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->f:Lcom/google/android/gms/drive/g/q;

    iget-wide v4, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->s:J

    cmp-long v0, v4, v8

    if-ltz v0, :cond_3

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    cmp-long v0, v4, v8

    if-eqz v0, :cond_2

    iget-object v0, v3, Lcom/google/android/gms/drive/g/q;->a:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v6

    add-long/2addr v4, v6

    cmp-long v0, v4, v8

    if-ltz v0, :cond_4

    :goto_2
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    :cond_1
    iget-object v0, v3, Lcom/google/android/gms/drive/g/q;->b:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    cmp-long v2, v0, v4

    if-gez v2, :cond_2

    iget-object v2, v3, Lcom/google/android/gms/drive/g/q;->b:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2, v0, v1, v4, v5}, Ljava/util/concurrent/atomic/AtomicLong;->compareAndSet(JJ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 221
    :cond_2
    return-void

    :cond_3
    move v0, v2

    .line 220
    goto :goto_1

    :cond_4
    move v1, v2

    goto :goto_2
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    .prologue
    .line 225
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/drive/ui/picker/view/j;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    .line 226
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/AbsListView$OnScrollListener;

    .line 227
    invoke-interface {v0, p1, p2}, Landroid/widget/AbsListView$OnScrollListener;->onScrollStateChanged(Landroid/widget/AbsListView;I)V

    goto :goto_0

    .line 229
    :cond_0
    return-void
.end method

.method public setBackgroundResource(I)V
    .locals 0

    .prologue
    .line 250
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/picker/view/j;->setBackgroundResource(I)V

    .line 251
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 587
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "%s[mainFilter=%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->f()Lcom/google/android/gms/drive/ui/picker/a/q;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
