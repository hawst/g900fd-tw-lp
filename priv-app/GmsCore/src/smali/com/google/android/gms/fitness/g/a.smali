.class public final Lcom/google/android/gms/fitness/g/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/k/a/at;

.field private static final b:Lcom/google/k/a/az;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    const/16 v0, 0x2c

    invoke-static {v0}, Lcom/google/k/a/at;->a(C)Lcom/google/k/a/at;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/g/a;->a:Lcom/google/k/a/at;

    .line 22
    const/16 v0, 0x7c

    invoke-static {v0}, Lcom/google/k/a/at;->a(C)Lcom/google/k/a/at;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Lcom/google/k/a/at;->b(Ljava/lang/String;)Lcom/google/k/a/az;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/fitness/g/a;->b:Lcom/google/k/a/az;

    return-void
.end method

.method public static a(Ljava/lang/String;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 26
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 27
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    .line 29
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/fitness/g/a;->b:Lcom/google/k/a/az;

    invoke-virtual {v0, p0}, Lcom/google/k/a/az;->a(Ljava/lang/CharSequence;)Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Ljava/util/Map;
    .locals 2

    .prologue
    .line 34
    invoke-static {p0}, Lcom/google/android/gms/fitness/g/a;->a(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/fitness/g/b;

    invoke-direct {v1}, Lcom/google/android/gms/fitness/g/b;-><init>()V

    invoke-static {v0, v1}, Lcom/google/k/c/cv;->a(Ljava/util/Map;Lcom/google/k/a/u;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Ljava/util/Set;
    .locals 3

    .prologue
    .line 43
    invoke-virtual {p0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 48
    :goto_0
    return-object v0

    .line 46
    :cond_0
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 47
    sget-object v0, Lcom/google/android/gms/fitness/g/a;->a:Lcom/google/k/a/at;

    invoke-virtual {v0, p0}, Lcom/google/k/a/at;->a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;

    move-result-object v0

    instance-of v2, v0, Ljava/util/Collection;

    if-eqz v2, :cond_1

    invoke-static {v0}, Lcom/google/k/c/ap;->a(Ljava/lang/Iterable;)Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->addAll(Ljava/util/Collection;)Z

    :goto_1
    move-object v0, v1

    .line 48
    goto :goto_0

    .line 47
    :cond_1
    invoke-static {v0}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Iterable;

    invoke-interface {v0}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/k/c/cj;->a(Ljava/util/Collection;Ljava/util/Iterator;)Z

    goto :goto_1
.end method
