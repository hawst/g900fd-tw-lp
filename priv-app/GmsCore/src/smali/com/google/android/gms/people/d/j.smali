.class public final Lcom/google/android/gms/people/d/j;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 65
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 66
    iput v0, p0, Lcom/google/android/gms/people/d/j;->a:I

    iput v0, p0, Lcom/google/android/gms/people/d/j;->b:I

    iput v0, p0, Lcom/google/android/gms/people/d/j;->c:I

    const/16 v0, 0x6e

    iput v0, p0, Lcom/google/android/gms/people/d/j;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/d/j;->cachedSize:I

    .line 67
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 132
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 133
    iget v1, p0, Lcom/google/android/gms/people/d/j;->a:I

    if-eqz v1, :cond_0

    .line 134
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/people/d/j;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_0
    iget v1, p0, Lcom/google/android/gms/people/d/j;->b:I

    if-eqz v1, :cond_1

    .line 138
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/people/d/j;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    :cond_1
    iget v1, p0, Lcom/google/android/gms/people/d/j;->c:I

    if-eqz v1, :cond_2

    .line 142
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/people/d/j;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    :cond_2
    iget v1, p0, Lcom/google/android/gms/people/d/j;->d:I

    const/16 v2, 0x6e

    if-eq v1, v2, :cond_3

    .line 146
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/people/d/j;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 80
    if-ne p1, p0, :cond_1

    .line 99
    :cond_0
    :goto_0
    return v0

    .line 83
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/people/d/j;

    if-nez v2, :cond_2

    move v0, v1

    .line 84
    goto :goto_0

    .line 86
    :cond_2
    check-cast p1, Lcom/google/android/gms/people/d/j;

    .line 87
    iget v2, p0, Lcom/google/android/gms/people/d/j;->a:I

    iget v3, p1, Lcom/google/android/gms/people/d/j;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 88
    goto :goto_0

    .line 90
    :cond_3
    iget v2, p0, Lcom/google/android/gms/people/d/j;->b:I

    iget v3, p1, Lcom/google/android/gms/people/d/j;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 91
    goto :goto_0

    .line 93
    :cond_4
    iget v2, p0, Lcom/google/android/gms/people/d/j;->c:I

    iget v3, p1, Lcom/google/android/gms/people/d/j;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 94
    goto :goto_0

    .line 96
    :cond_5
    iget v2, p0, Lcom/google/android/gms/people/d/j;->d:I

    iget v3, p1, Lcom/google/android/gms/people/d/j;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 97
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 104
    iget v0, p0, Lcom/google/android/gms/people/d/j;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 106
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/j;->b:I

    add-int/2addr v0, v1

    .line 107
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/j;->c:I

    add-int/2addr v0, v1

    .line 108
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/people/d/j;->d:I

    add-int/2addr v0, v1

    .line 109
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 36
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    iput v0, p0, Lcom/google/android/gms/people/d/j;->a:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/d/j;->b:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    goto :goto_0

    :sswitch_5
    iput v0, p0, Lcom/google/android/gms/people/d/j;->c:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_3

    goto :goto_0

    :sswitch_7
    iput v0, p0, Lcom/google/android/gms/people/d/j;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_3
        0x18 -> :sswitch_4
        0x20 -> :sswitch_6
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_2
        0x6 -> :sswitch_2
        0x7 -> :sswitch_2
        0x8 -> :sswitch_2
        0x9 -> :sswitch_2
        0x64 -> :sswitch_2
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x0 -> :sswitch_5
        0x1 -> :sswitch_5
        0x2 -> :sswitch_5
        0xa -> :sswitch_5
    .end sparse-switch

    :sswitch_data_3
    .sparse-switch
        0x14 -> :sswitch_7
        0x1e -> :sswitch_7
        0x28 -> :sswitch_7
        0x32 -> :sswitch_7
        0x5a -> :sswitch_7
        0x64 -> :sswitch_7
        0x6e -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 115
    iget v0, p0, Lcom/google/android/gms/people/d/j;->a:I

    if-eqz v0, :cond_0

    .line 116
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/people/d/j;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 118
    :cond_0
    iget v0, p0, Lcom/google/android/gms/people/d/j;->b:I

    if-eqz v0, :cond_1

    .line 119
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/people/d/j;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 121
    :cond_1
    iget v0, p0, Lcom/google/android/gms/people/d/j;->c:I

    if-eqz v0, :cond_2

    .line 122
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/people/d/j;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 124
    :cond_2
    iget v0, p0, Lcom/google/android/gms/people/d/j;->d:I

    const/16 v1, 0x6e

    if-eq v0, v1, :cond_3

    .line 125
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/people/d/j;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 127
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 128
    return-void
.end method
