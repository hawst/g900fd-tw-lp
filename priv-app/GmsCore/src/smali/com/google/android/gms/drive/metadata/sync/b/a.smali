.class public abstract Lcom/google/android/gms/drive/metadata/sync/b/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/metadata/sync/b/f;


# instance fields
.field final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field private b:Lcom/google/android/gms/drive/g/f;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/a;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/metadata/sync/b/g;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 26
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/a;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v2, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->compareAndSet(II)Z

    move-result v0

    if-nez v0, :cond_0

    .line 27
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t run in state "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/b/a;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 29
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/sync/b/a;->a()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Already finished"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 30
    if-ltz p2, :cond_2

    :goto_1
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 32
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/b/b;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Running "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1, p2, p1}, Lcom/google/android/gms/drive/metadata/sync/b/b;-><init>(Lcom/google/android/gms/drive/metadata/sync/b/a;Ljava/lang/String;ILcom/google/android/gms/drive/metadata/sync/b/g;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/a;->b:Lcom/google/android/gms/drive/g/f;

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/b/a;->b:Lcom/google/android/gms/drive/g/f;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/f;->start()V

    .line 46
    return-void

    :cond_1
    move v0, v2

    .line 29
    goto :goto_0

    :cond_2
    move v1, v2

    .line 30
    goto :goto_1
.end method

.method protected abstract a(I)Z
.end method
