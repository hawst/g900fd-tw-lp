.class public final Lcom/google/android/gms/backup/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/content/Intent;


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.backup.BackupAccountManagerService"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/backup/b;->a:Landroid/content/Intent;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/backup/b;->b:Landroid/content/Context;

    .line 37
    return-void
.end method

.method private a(Lcom/google/android/gms/backup/e;)Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 63
    new-instance v2, Lcom/google/android/gms/common/b;

    invoke-direct {v2}, Lcom/google/android/gms/common/b;-><init>()V

    .line 64
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/backup/b;->b:Landroid/content/Context;

    sget-object v4, Lcom/google/android/gms/backup/b;->a:Landroid/content/Intent;

    const/4 v5, 0x1

    invoke-virtual {v1, v3, v4, v2, v5}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/backup/al;->a(Landroid/os/IBinder;)Lcom/google/android/gms/backup/ak;

    move-result-object v1

    .line 69
    invoke-interface {p1, v1}, Lcom/google/android/gms/backup/e;->a(Lcom/google/android/gms/backup/ak;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 77
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/backup/b;->b:Landroid/content/Context;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 80
    :cond_0
    :goto_0
    return-object v0

    .line 70
    :catch_0
    move-exception v1

    .line 71
    :try_start_1
    const-string v3, "BackupAccountManagerServiceClient"

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 72
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/backup/b;->b:Landroid/content/Context;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    goto :goto_0

    .line 73
    :catch_1
    move-exception v1

    .line 74
    :try_start_2
    const-string v3, "BackupAccountManagerServiceClient"

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 75
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/backup/b;->b:Landroid/content/Context;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/backup/b;->b:Landroid/content/Context;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    throw v0
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/gms/backup/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/backup/c;-><init>(Lcom/google/android/gms/backup/b;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/backup/b;->a(Lcom/google/android/gms/backup/e;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    return-object v0
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/gms/backup/d;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/backup/d;-><init>(Lcom/google/android/gms/backup/b;Landroid/accounts/Account;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/backup/b;->a(Lcom/google/android/gms/backup/e;)Ljava/lang/Object;

    .line 56
    return-void
.end method
