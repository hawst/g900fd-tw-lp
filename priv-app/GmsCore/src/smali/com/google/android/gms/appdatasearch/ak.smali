.class public final Lcom/google/android/gms/appdatasearch/ak;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:I

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/util/List;

.field private i:Ljava/util/BitSet;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 307
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 308
    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/ak;->g:Ljava/lang/String;

    .line 309
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/appdatasearch/ak;->c:I

    .line 310
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/ak;->h:Ljava/util/List;

    .line 311
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 401
    const/4 v8, 0x0

    .line 402
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/ak;->i:Ljava/util/BitSet;

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/ak;->i:Ljava/util/BitSet;

    invoke-virtual {v0}, Ljava/util/BitSet;->cardinality()I

    move-result v0

    new-array v8, v0, [I

    .line 405
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/ak;->i:Ljava/util/BitSet;

    invoke-virtual {v0, v1}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    .line 406
    :goto_0
    if-ltz v0, :cond_0

    .line 408
    add-int/lit8 v2, v1, 0x1

    aput v0, v8, v1

    .line 407
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/ak;->i:Ljava/util/BitSet;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->nextSetBit(I)I

    move-result v0

    move v1, v2

    goto :goto_0

    .line 411
    :cond_0
    new-instance v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/ak;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/ak;->a:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/google/android/gms/appdatasearch/ak;->b:Z

    iget v4, p0, Lcom/google/android/gms/appdatasearch/ak;->c:I

    iget-boolean v5, p0, Lcom/google/android/gms/appdatasearch/ak;->d:Z

    iget-object v6, p0, Lcom/google/android/gms/appdatasearch/ak;->e:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/appdatasearch/ak;->h:Ljava/util/List;

    iget-object v9, p0, Lcom/google/android/gms/appdatasearch/ak;->h:Ljava/util/List;

    invoke-interface {v9}, Ljava/util/List;->size()I

    move-result v9

    new-array v9, v9, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-interface {v7, v9}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Lcom/google/android/gms/appdatasearch/Feature;

    iget-object v9, p0, Lcom/google/android/gms/appdatasearch/ak;->f:Ljava/lang/String;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;-><init>(Ljava/lang/String;Ljava/lang/String;ZIZLjava/lang/String;[Lcom/google/android/gms/appdatasearch/Feature;[ILjava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/Feature;)Lcom/google/android/gms/appdatasearch/ak;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/ak;->h:Ljava/util/List;

    invoke-static {v0, p1}, Lcom/google/android/gms/appdatasearch/Feature;->a(Ljava/util/List;Lcom/google/android/gms/appdatasearch/Feature;)V

    .line 373
    return-object p0
.end method
