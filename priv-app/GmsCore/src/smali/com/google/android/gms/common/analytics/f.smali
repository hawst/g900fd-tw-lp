.class public final Lcom/google/android/gms/common/analytics/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 66
    new-instance v1, Ljava/util/HashSet;

    sget-object v0, Lcom/google/android/gms/common/a/b;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    sput-object v1, Lcom/google/android/gms/common/analytics/f;->a:Ljava/util/Set;

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 115
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v0, v1, Landroid/content/pm/ApplicationInfo;->flags:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 117
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public static a()Lcom/google/android/gms/common/analytics/a/c;
    .locals 6

    .prologue
    .line 73
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    .line 74
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 76
    new-instance v2, Lcom/google/android/gms/common/analytics/a/c;

    invoke-direct {v2}, Lcom/google/android/gms/common/analytics/a/c;-><init>()V

    .line 77
    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/common/analytics/a/c;->a:Ljava/lang/String;

    .line 78
    invoke-static {}, Lcom/google/android/gms/common/util/ay;->d()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/common/analytics/a/c;->b:Ljava/lang/Long;

    .line 79
    invoke-static {}, Lcom/google/android/gms/common/util/ay;->e()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/common/analytics/a/c;->c:Ljava/lang/Integer;

    .line 80
    invoke-static {}, Lcom/google/android/gms/common/util/ay;->f()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/common/analytics/a/c;->d:Ljava/lang/Integer;

    .line 81
    invoke-static {}, Lcom/google/android/gms/common/util/ay;->g()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/common/analytics/a/c;->e:Ljava/lang/Integer;

    .line 82
    new-instance v3, Lcom/google/android/gms/common/analytics/a/g;

    invoke-direct {v3}, Lcom/google/android/gms/common/analytics/a/g;-><init>()V

    const-string v4, "java.vm.version"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    iput-object v4, v3, Lcom/google/android/gms/common/analytics/a/g;->a:Ljava/lang/String;

    :cond_0
    const-string v4, "java.vm.vendor"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_1

    iput-object v4, v3, Lcom/google/android/gms/common/analytics/a/g;->b:Ljava/lang/String;

    :cond_1
    const-string v4, "java.vm.name"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_2

    iput-object v4, v3, Lcom/google/android/gms/common/analytics/a/g;->c:Ljava/lang/String;

    :cond_2
    const-string v4, "java.vm.specification.version"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_3

    iput-object v4, v3, Lcom/google/android/gms/common/analytics/a/g;->d:Ljava/lang/String;

    :cond_3
    const-string v4, "java.vm.specification.vendor"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_4

    iput-object v4, v3, Lcom/google/android/gms/common/analytics/a/g;->e:Ljava/lang/String;

    :cond_4
    const-string v4, "java.vm.specification.name"

    invoke-static {v4}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_5

    iput-object v4, v3, Lcom/google/android/gms/common/analytics/a/g;->f:Ljava/lang/String;

    :cond_5
    iput-object v3, v2, Lcom/google/android/gms/common/analytics/a/c;->f:Lcom/google/android/gms/common/analytics/a/g;

    .line 83
    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/a;->f(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/common/analytics/a/c;->g:Ljava/lang/Integer;

    .line 85
    invoke-static {v0, v1}, Lcom/google/android/gms/common/analytics/f;->b(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/common/analytics/a/c;->h:Ljava/lang/Integer;

    .line 86
    invoke-static {v0, v1}, Lcom/google/android/gms/common/analytics/f;->a(Landroid/content/Context;Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/common/analytics/a/c;->i:Ljava/lang/Integer;

    .line 87
    invoke-static {v0, v1}, Lcom/google/android/gms/common/analytics/f;->c(Landroid/content/Context;Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/common/analytics/a/c;->j:Ljava/lang/Integer;

    .line 110
    return-object v2
.end method

.method public static a(J)V
    .locals 6

    .prologue
    .line 269
    const-string v0, "system_health"

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/google/android/gms/common/receiver/InternalBroadcastReceiver;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "com.google.android.gms.common.receiver.LOG_CORE_ANALYTICS"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "type"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    const-string v0, "alarm"

    invoke-virtual {v1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v1, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    add-long/2addr v4, p0

    invoke-virtual {v0, v1, v4, v5, v2}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 270
    return-void
.end method

.method public static a(Lcom/google/android/gms/f/a/f;)V
    .locals 2

    .prologue
    .line 238
    new-instance v0, Lcom/google/android/gms/common/analytics/a/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/analytics/a/e;-><init>()V

    .line 239
    invoke-static {}, Lcom/google/android/gms/common/analytics/f;->a()Lcom/google/android/gms/common/analytics/a/c;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/common/analytics/a/e;->a:Lcom/google/android/gms/common/analytics/a/c;

    .line 240
    iput-object p0, v0, Lcom/google/android/gms/common/analytics/a/e;->b:Lcom/google/android/gms/f/a/f;

    .line 241
    const-string v1, "system_health"

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/common/analytics/f;->a(Ljava/lang/String;[B)V

    .line 242
    return-void
.end method

.method private static a(Ljava/lang/String;[B)V
    .locals 3

    .prologue
    .line 276
    new-instance v0, Lcom/google/android/gms/playlog/a;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/playlog/a;-><init>(Landroid/content/Context;I)V

    .line 278
    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0, p0, p1, v1}, Lcom/google/android/gms/playlog/a;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    .line 279
    invoke-virtual {v0}, Lcom/google/android/gms/playlog/a;->a()V

    .line 280
    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 123
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    .line 124
    invoke-static {}, Lcom/google/android/gms/common/ey;->a()Lcom/google/android/gms/common/ey;

    .line 125
    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/google/android/gms/common/ey;->a(Landroid/content/pm/PackageInfo;Z)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 133
    :goto_0
    return v0

    .line 127
    :cond_0
    const/4 v0, 0x1

    invoke-static {v2, v0}, Lcom/google/android/gms/common/ey;->a(Landroid/content/pm/PackageInfo;Z)Z
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_1

    .line 128
    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    move v0, v1

    .line 130
    goto :goto_0

    .line 133
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 189
    new-instance v0, Lcom/google/android/gms/common/analytics/a/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/analytics/a/e;-><init>()V

    .line 190
    invoke-static {}, Lcom/google/android/gms/common/analytics/f;->a()Lcom/google/android/gms/common/analytics/a/c;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/common/analytics/a/e;->a:Lcom/google/android/gms/common/analytics/a/c;

    .line 191
    const-string v1, "install"

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/common/analytics/f;->a(Ljava/lang/String;[B)V

    .line 194
    invoke-static {}, Lcom/google/android/gms/playlog/uploader/a;->a()Lcom/google/android/gms/playlog/uploader/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/uploader/a;->c()V

    .line 195
    return-void
.end method

.method private static c(Landroid/content/Context;Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 166
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 175
    iget v3, v2, Landroid/content/pm/ApplicationInfo;->flags:I

    and-int/lit16 v3, v3, 0x80

    if-eqz v3, :cond_0

    move v0, v1

    .line 176
    :cond_0
    new-instance v3, Ljava/io/File;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->nativeLibraryDir:Ljava/lang/String;

    const-string v4, "libgmscore.so"

    invoke-direct {v3, v2, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 177
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 178
    const/16 v1, 0xe

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v0, :cond_3

    .line 179
    :cond_1
    const/4 v1, 0x3

    .line 184
    :cond_2
    :goto_0
    return v1

    .line 168
    :catch_0
    move-exception v1

    move v1, v0

    goto :goto_0

    .line 181
    :cond_3
    const/4 v1, 0x2

    goto :goto_0
.end method

.method public static c()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 198
    sget v1, Lcom/google/android/gms/common/stats/g;->a:I

    sget-object v0, Lcom/google/android/gms/common/stats/d;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v1, v0, :cond_0

    .line 223
    :goto_0
    return-void

    .line 204
    :cond_0
    new-instance v0, Lcom/google/android/gms/clearcut/a;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/google/android/gms/clearcut/a;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 206
    new-instance v1, Lcom/google/android/gms/common/analytics/a/e;

    invoke-direct {v1}, Lcom/google/android/gms/common/analytics/a/e;-><init>()V

    .line 207
    invoke-static {}, Lcom/google/android/gms/common/analytics/f;->a()Lcom/google/android/gms/common/analytics/a/c;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/common/analytics/a/e;->a:Lcom/google/android/gms/common/analytics/a/c;

    .line 208
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v3, "service.connections.internal"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v3

    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v3}, Lcom/google/android/gms/common/stats/f;->a(Ljava/io/File;)Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_1

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    new-array v4, v4, [Lcom/google/android/gms/common/analytics/a/d;

    iput-object v4, v1, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    iget-object v4, v1, Lcom/google/android/gms/common/analytics/a/e;->e:[Lcom/google/android/gms/common/analytics/a/d;

    invoke-interface {v3, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    :cond_1
    const-string v3, "service.connections.internal"

    invoke-virtual {v2, v3}, Landroid/content/Context;->deleteFile(Ljava/lang/String;)Z

    .line 209
    :cond_2
    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/a;->a(Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/clearcut/c;

    move-result-object v0

    const-string v1, "internal_service_connections"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/c;->a(Ljava/lang/String;)Lcom/google/android/gms/clearcut/c;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/c;->a(I)Lcom/google/android/gms/clearcut/c;

    move-result-object v0

    .line 212
    new-instance v1, Lcom/google/android/gms/common/api/w;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/clearcut/a;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 215
    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->b()V

    .line 216
    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/c;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/common/analytics/g;

    invoke-direct {v2, v1}, Lcom/google/android/gms/common/analytics/g;-><init>(Lcom/google/android/gms/common/api/v;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public static d()V
    .locals 14

    .prologue
    const-wide/16 v12, 0xa

    const/16 v9, 0x12

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 230
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v5

    new-instance v6, Lcom/google/android/gms/f/a/f;

    invoke-direct {v6}, Lcom/google/android/gms/f/a/f;-><init>()V

    const-string v0, "location"

    invoke-virtual {v5, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/f/a/f;->a:Ljava/lang/Boolean;

    const-string v0, "location"

    invoke-virtual {v5, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    if-nez v0, :cond_2

    move v0, v2

    :goto_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/f/a/f;->b:Ljava/lang/Boolean;

    new-instance v7, Lcom/google/android/gms/f/a/e;

    invoke-direct {v7}, Lcom/google/android/gms/f/a/e;-><init>()V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v0, v9, :cond_4

    const-string v0, "user"

    invoke-virtual {v5, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/os/UserManager;->getApplicationRestrictions(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v3, "true"

    const-string v4, "restricted_profile"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/gms/f/a/e;->o:Ljava/lang/Boolean;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.google"

    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    sget v8, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v8, v9, :cond_6

    invoke-virtual {v4, v3, v0}, Landroid/accounts/AccountManager;->getAccountsByTypeForPackage(Ljava/lang/String;Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    :goto_4
    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    new-array v0, v9, [I

    iput-object v0, v7, Lcom/google/android/gms/f/a/e;->p:[I

    move v4, v2

    :goto_5
    if-ge v4, v9, :cond_7

    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    const-string v3, "com.android.contacts"

    invoke-static {v0, v3}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_b

    const/4 v3, 0x2

    :goto_6
    const-string v10, "com.google.android.gms.people"

    invoke-static {v0, v10}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    or-int/lit8 v3, v3, 0x1

    :cond_0
    iget-object v0, v7, Lcom/google/android/gms/f/a/e;->p:[I

    aput v3, v0, v4

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_5

    :cond_1
    move v0, v2

    goto/16 :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_3

    move v0, v2

    goto/16 :goto_1

    :cond_3
    const-string v3, "network"

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_3

    :cond_6
    invoke-virtual {v4, v3}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    goto :goto_4

    :cond_7
    iput-object v7, v6, Lcom/google/android/gms/f/a/f;->c:Lcom/google/android/gms/f/a/e;

    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, v5}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/usagereporting/a;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v2

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v12, v13, v0}, Lcom/google/android/gms/common/api/v;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/google/android/gms/usagereporting/a;->b:Lcom/google/android/gms/usagereporting/d;

    invoke-interface {v0, v2}, Lcom/google/android/gms/usagereporting/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v12, v13, v3}, Lcom/google/android/gms/common/api/am;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/usagereporting/g;

    invoke-interface {v0}, Lcom/google/android/gms/usagereporting/g;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v0}, Lcom/google/android/gms/usagereporting/g;->b()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v6, Lcom/google/android/gms/f/a/f;->d:Ljava/lang/Boolean;

    :cond_8
    invoke-interface {v2}, Lcom/google/android/gms/common/api/v;->d()V

    :cond_9
    new-instance v0, Lcom/google/android/gms/f/a/b;

    invoke-direct {v0}, Lcom/google/android/gms/f/a/b;-><init>()V

    const-string v1, "Cannot gather auth health data from main thread"

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-ne v2, v3, :cond_a

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_a
    invoke-static {v5}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-static {v5, v1, v2, v0}, Lcom/google/android/gms/f/b;->a(Landroid/content/Context;Landroid/accounts/AccountManager;Ljava/lang/String;Lcom/google/android/gms/f/a/b;)V

    const-string v2, "com.sidewinder"

    invoke-static {v5, v1, v2, v0}, Lcom/google/android/gms/f/b;->a(Landroid/content/Context;Landroid/accounts/AccountManager;Ljava/lang/String;Lcom/google/android/gms/f/a/b;)V

    iput-object v0, v6, Lcom/google/android/gms/f/a/f;->e:Lcom/google/android/gms/f/a/b;

    invoke-static {v6}, Lcom/google/android/gms/common/analytics/f;->a(Lcom/google/android/gms/f/a/f;)V

    .line 231
    return-void

    :cond_b
    move v3, v2

    goto/16 :goto_6
.end method

.method public static e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 246
    new-instance v0, Lcom/google/android/gms/clearcut/a;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2, v3, v3}, Lcom/google/android/gms/clearcut/a;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    .line 248
    new-instance v1, Lcom/google/android/gms/common/analytics/a/e;

    invoke-direct {v1}, Lcom/google/android/gms/common/analytics/a/e;-><init>()V

    .line 249
    invoke-static {}, Lcom/google/android/gms/common/analytics/f;->a()Lcom/google/android/gms/common/analytics/a/c;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/common/analytics/a/e;->a:Lcom/google/android/gms/common/analytics/a/c;

    .line 250
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/common/analytics/f;->a:Ljava/util/Set;

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/common/internal/cb;->a(Landroid/content/Context;Lcom/google/android/gms/common/analytics/a/e;Ljava/util/Set;)V

    .line 252
    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/a;->a(Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/clearcut/c;

    move-result-object v0

    const-string v1, "service_connections"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/c;->a(Ljava/lang/String;)Lcom/google/android/gms/clearcut/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/c;->a(I)Lcom/google/android/gms/clearcut/c;

    move-result-object v0

    .line 255
    new-instance v1, Lcom/google/android/gms/common/api/w;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/clearcut/a;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 258
    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->b()V

    .line 259
    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/c;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/common/analytics/h;

    invoke-direct {v2, v1}, Lcom/google/android/gms/common/analytics/h;-><init>(Lcom/google/android/gms/common/api/v;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 266
    return-void
.end method
