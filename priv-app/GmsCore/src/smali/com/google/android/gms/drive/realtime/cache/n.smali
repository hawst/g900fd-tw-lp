.class public final Lcom/google/android/gms/drive/realtime/cache/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/drive/realtime/e;

.field private final b:Lcom/google/android/gms/drive/realtime/cache/a/g;

.field private final c:Lcom/google/android/gms/drive/realtime/cache/k;

.field private final d:Lcom/google/android/gms/drive/database/model/br;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/util/Map;

.field private g:Z

.field private h:Z

.field private i:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/realtime/e;Lcom/google/android/gms/drive/realtime/cache/a/g;Lcom/google/android/gms/drive/realtime/cache/k;Lcom/google/android/gms/drive/database/model/br;Ljava/lang/String;Ljava/util/Map;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->g:Z

    .line 31
    iput-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->h:Z

    .line 32
    iput v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->i:I

    .line 38
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/cache/n;->a:Lcom/google/android/gms/drive/realtime/e;

    .line 39
    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/cache/n;->b:Lcom/google/android/gms/drive/realtime/cache/a/g;

    .line 40
    iput-object p3, p0, Lcom/google/android/gms/drive/realtime/cache/n;->c:Lcom/google/android/gms/drive/realtime/cache/k;

    .line 41
    iput-object p4, p0, Lcom/google/android/gms/drive/realtime/cache/n;->d:Lcom/google/android/gms/drive/database/model/br;

    .line 42
    iput-object p5, p0, Lcom/google/android/gms/drive/realtime/cache/n;->e:Ljava/lang/String;

    .line 43
    iput-object p6, p0, Lcom/google/android/gms/drive/realtime/cache/n;->f:Ljava/util/Map;

    .line 44
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Z
    .locals 2

    .prologue
    .line 58
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->h:Z

    if-eqz v0, :cond_0

    .line 59
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to interact with a closed cache."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 61
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->b:Lcom/google/android/gms/drive/realtime/cache/a/g;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/cache/a/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 62
    invoke-static {}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a()Lcom/google/android/gms/drive/realtime/cache/a/a;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/realtime/cache/a/a;->a(Landroid/database/sqlite/SQLiteDatabase;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 63
    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final declared-synchronized b()Lcom/google/c/a/a/b/a/d;
    .locals 2

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->h:Z

    if-eqz v0, :cond_0

    .line 73
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to interact with a closed cache."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 76
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->c:Lcom/google/android/gms/drive/realtime/cache/k;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/n;->b:Lcom/google/android/gms/drive/realtime/cache/a/g;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/realtime/cache/a/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/realtime/cache/k;->a(Landroid/database/sqlite/SQLiteDatabase;)Lcom/google/c/a/a/b/a/d;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized c()Lcom/google/c/a/a/b/a/f;
    .locals 2

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->h:Z

    if-eqz v0, :cond_0

    .line 85
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to interact with a closed cache."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 87
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->c:Lcom/google/android/gms/drive/realtime/cache/k;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/n;->b:Lcom/google/android/gms/drive/realtime/cache/a/g;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/realtime/cache/a/g;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/realtime/cache/k;->b(Landroid/database/sqlite/SQLiteDatabase;)Lcom/google/c/a/a/b/a/f;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized d()V
    .locals 2

    .prologue
    .line 94
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->g:Z

    if-eqz v0, :cond_0

    .line 95
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot add pending changes in the closing state."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 94
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 97
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->i:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 98
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized e()V
    .locals 1

    .prologue
    .line 105
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->i:I

    .line 106
    iget v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->i:I

    if-nez v0, :cond_0

    .line 107
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 109
    :cond_0
    monitor-exit p0

    return-void

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    .prologue
    .line 117
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->g:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()Z
    .locals 1

    .prologue
    .line 125
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->h:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()V
    .locals 3

    .prologue
    .line 135
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->g:Z

    .line 136
    iget v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->i:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 138
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 143
    :cond_0
    :try_start_2
    iget-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->h:Z

    if-nez v0, :cond_1

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->h:Z

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->b:Lcom/google/android/gms/drive/realtime/cache/a/g;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/cache/a/g;->close()V

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->f:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/n;->a:Lcom/google/android/gms/drive/realtime/e;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 148
    :cond_1
    monitor-exit p0

    return-void

    .line 139
    :catch_0
    move-exception v0

    .line 140
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Interrupted while closing cache."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final i()Landroid/database/sqlite/SQLiteDatabase;
    .locals 2

    .prologue
    .line 151
    iget-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->h:Z

    if-eqz v0, :cond_0

    .line 152
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to interact with a closed cache."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->b:Lcom/google/android/gms/drive/realtime/cache/a/g;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/cache/a/g;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    return-object v0
.end method

.method final j()Lcom/google/android/gms/drive/database/model/br;
    .locals 2

    .prologue
    .line 158
    iget-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->h:Z

    if-eqz v0, :cond_0

    .line 159
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Trying to interact with a closed cache."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/n;->d:Lcom/google/android/gms/drive/database/model/br;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 166
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/n;->a:Lcom/google/android/gms/drive/realtime/e;

    iget-object v1, v1, Lcom/google/android/gms/drive/realtime/e;->a:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/n;->a:Lcom/google/android/gms/drive/realtime/e;

    iget-object v1, v1, Lcom/google/android/gms/drive/realtime/e;->b:Lcom/google/android/gms/drive/auth/g;

    iget-wide v2, v1, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/n;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
