.class public final Lcom/google/android/gms/drive/g/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/al;


# static fields
.field public static final a:Lcom/google/android/gms/drive/g/am;


# instance fields
.field private final b:Ljava/lang/Runnable;

.field private final c:Ljava/lang/Runnable;

.field private final d:Ljava/lang/Runnable;

.field private final e:I

.field private final f:Ljava/util/concurrent/ExecutorService;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/util/concurrent/Executor;

.field private i:J

.field private j:Z

.field private k:Lcom/google/android/gms/drive/g/as;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/drive/g/ao;

    invoke-direct {v0}, Lcom/google/android/gms/drive/g/ao;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/g/an;->a:Lcom/google/android/gms/drive/g/am;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 104
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v2, Lcom/google/android/gms/drive/g/ap;

    invoke-direct {v2, p0}, Lcom/google/android/gms/drive/g/ap;-><init>(Lcom/google/android/gms/drive/g/an;)V

    iput-object v2, p0, Lcom/google/android/gms/drive/g/an;->b:Ljava/lang/Runnable;

    .line 62
    new-instance v2, Lcom/google/android/gms/drive/g/aq;

    invoke-direct {v2, p0}, Lcom/google/android/gms/drive/g/aq;-><init>(Lcom/google/android/gms/drive/g/an;)V

    iput-object v2, p0, Lcom/google/android/gms/drive/g/an;->c:Ljava/lang/Runnable;

    .line 80
    invoke-static {v0}, Lcom/google/android/gms/drive/g/u;->a(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/drive/g/an;->f:Ljava/util/concurrent/ExecutorService;

    .line 85
    const-wide/16 v2, 0x0

    iput-wide v2, p0, Lcom/google/android/gms/drive/g/an;->i:J

    .line 88
    iput-boolean v1, p0, Lcom/google/android/gms/drive/g/an;->j:Z

    .line 91
    sget-object v2, Lcom/google/android/gms/drive/g/as;->a:Lcom/google/android/gms/drive/g/as;

    iput-object v2, p0, Lcom/google/android/gms/drive/g/an;->k:Lcom/google/android/gms/drive/g/as;

    .line 105
    if-lez p2, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 106
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/google/android/gms/drive/g/an;->d:Ljava/lang/Runnable;

    .line 107
    iput p2, p0, Lcom/google/android/gms/drive/g/an;->e:I

    .line 108
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/g/an;->g:Ljava/lang/String;

    .line 109
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/gms/drive/g/an;->h:Ljava/util/concurrent/Executor;

    .line 110
    return-void

    :cond_0
    move v0, v1

    .line 105
    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/drive/g/an;J)J
    .locals 1

    .prologue
    .line 20
    iput-wide p1, p0, Lcom/google/android/gms/drive/g/an;->i:J

    return-wide p1
.end method

.method static synthetic a(Lcom/google/android/gms/drive/g/an;Lcom/google/android/gms/drive/g/as;)Lcom/google/android/gms/drive/g/as;
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/google/android/gms/drive/g/an;->k:Lcom/google/android/gms/drive/g/as;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/drive/g/an;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/gms/drive/g/an;->d:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/drive/g/an;)Lcom/google/android/gms/drive/g/as;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/gms/drive/g/an;->k:Lcom/google/android/gms/drive/g/as;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/drive/g/an;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/gms/drive/g/an;->c:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/drive/g/an;)Z
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/g/an;->j:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/drive/g/an;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/gms/drive/g/an;->b:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/drive/g/an;)Ljava/util/concurrent/Executor;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/gms/drive/g/an;->h:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/drive/g/an;)I
    .locals 1

    .prologue
    .line 20
    iget v0, p0, Lcom/google/android/gms/drive/g/an;->e:I

    return v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 6

    .prologue
    .line 120
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/g/an;->j:Z

    if-nez v0, :cond_1

    .line 121
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 122
    iget-wide v2, p0, Lcom/google/android/gms/drive/g/an;->i:J

    sub-long v2, v0, v2

    .line 123
    iget v4, p0, Lcom/google/android/gms/drive/g/an;->e:I

    int-to-long v4, v4

    cmp-long v4, v2, v4

    if-gez v4, :cond_0

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/g/an;->j:Z

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/drive/g/an;->f:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/gms/drive/g/ar;

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/gms/drive/g/ar;-><init>(Lcom/google/android/gms/drive/g/an;J)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    :goto_0
    monitor-exit p0

    return-void

    .line 133
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/drive/g/an;->c:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 134
    iput-wide v0, p0, Lcom/google/android/gms/drive/g/an;->i:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 120
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 137
    :cond_1
    :try_start_2
    const-string v0, "RateLimitedExecutorImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Rate limited: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/g/an;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/drive/g/an;->f:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 144
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 8

    .prologue
    .line 148
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "RateLimitedExecutorImpl[owner=%s, scheduled=%s, lastUpdated=%s, lapseSinceLastUpdate=%s, interval=%d]"

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/g/an;->g:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/google/android/gms/drive/g/an;->j:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/google/android/gms/drive/g/an;->i:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/gms/drive/g/an;->i:J

    sub-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x4

    iget v4, p0, Lcom/google/android/gms/drive/g/an;->e:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
