.class public final Lcom/google/android/gms/udc/e/c;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile f:[Lcom/google/android/gms/udc/e/c;


# instance fields
.field public a:Lcom/google/android/gms/udc/e/s;

.field public b:Lcom/google/android/gms/udc/e/k;

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 39
    iput-object v1, p0, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    iput-object v1, p0, Lcom/google/android/gms/udc/e/c;->b:Lcom/google/android/gms/udc/e/k;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/udc/e/c;->c:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/android/gms/udc/e/c;->d:Z

    iput v2, p0, Lcom/google/android/gms/udc/e/c;->e:I

    iput-object v1, p0, Lcom/google/android/gms/udc/e/c;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/udc/e/c;->cachedSize:I

    .line 40
    return-void
.end method

.method public static a()[Lcom/google/android/gms/udc/e/c;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/android/gms/udc/e/c;->f:[Lcom/google/android/gms/udc/e/c;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/android/gms/udc/e/c;->f:[Lcom/google/android/gms/udc/e/c;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/udc/e/c;

    sput-object v0, Lcom/google/android/gms/udc/e/c;->f:[Lcom/google/android/gms/udc/e/c;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/android/gms/udc/e/c;->f:[Lcom/google/android/gms/udc/e/c;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 134
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 135
    iget-object v1, p0, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    if-eqz v1, :cond_0

    .line 136
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/udc/e/c;->b:Lcom/google/android/gms/udc/e/k;

    if-eqz v1, :cond_1

    .line 140
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/udc/e/c;->b:Lcom/google/android/gms/udc/e/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/udc/e/c;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 144
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/udc/e/c;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 147
    :cond_2
    iget v1, p0, Lcom/google/android/gms/udc/e/c;->e:I

    if-eqz v1, :cond_3

    .line 148
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/udc/e/c;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 151
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/udc/e/c;->d:Z

    if-eqz v1, :cond_4

    .line 152
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/gms/udc/e/c;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 155
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 55
    if-ne p1, p0, :cond_1

    .line 56
    const/4 v0, 0x1

    .line 93
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/udc/e/c;

    if-eqz v1, :cond_0

    .line 61
    check-cast p1, Lcom/google/android/gms/udc/e/c;

    .line 62
    iget-object v1, p0, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_5

    .line 63
    iget-object v1, p1, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    if-nez v1, :cond_0

    .line 71
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/c;->b:Lcom/google/android/gms/udc/e/k;

    if-nez v1, :cond_6

    .line 72
    iget-object v1, p1, Lcom/google/android/gms/udc/e/c;->b:Lcom/google/android/gms/udc/e/k;

    if-nez v1, :cond_0

    .line 80
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/udc/e/c;->c:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 81
    iget-object v1, p1, Lcom/google/android/gms/udc/e/c;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 87
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gms/udc/e/c;->d:Z

    iget-boolean v2, p1, Lcom/google/android/gms/udc/e/c;->d:Z

    if-ne v1, v2, :cond_0

    .line 90
    iget v1, p0, Lcom/google/android/gms/udc/e/c;->e:I

    iget v2, p1, Lcom/google/android/gms/udc/e/c;->e:I

    if-ne v1, v2, :cond_0

    .line 93
    invoke-virtual {p0, p1}, Lcom/google/android/gms/udc/e/c;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 67
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 76
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/udc/e/c;->b:Lcom/google/android/gms/udc/e/k;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/c;->b:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/e/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 84
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/udc/e/c;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/udc/e/c;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 101
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/udc/e/c;->b:Lcom/google/android/gms/udc/e/k;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 103
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/udc/e/c;->c:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 105
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/udc/e/c;->d:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x4cf

    :goto_3
    add-int/2addr v0, v1

    .line 106
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/udc/e/c;->e:I

    add-int/2addr v0, v1

    .line 107
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/udc/e/c;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    return v0

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/s;->hashCode()I

    move-result v0

    goto :goto_0

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/c;->b:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/e/k;->hashCode()I

    move-result v0

    goto :goto_1

    .line 103
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/udc/e/c;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 105
    :cond_3
    const/16 v0, 0x4d5

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/udc/e/c;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/udc/e/s;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/c;->b:Lcom/google/android/gms/udc/e/k;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/udc/e/k;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/k;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/e/c;->b:Lcom/google/android/gms/udc/e/k;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/e/c;->b:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/e/c;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/udc/e/c;->e:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/udc/e/c;->d:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    if-eqz v0, :cond_0

    .line 115
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/c;->b:Lcom/google/android/gms/udc/e/k;

    if-eqz v0, :cond_1

    .line 118
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/udc/e/c;->b:Lcom/google/android/gms/udc/e/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/e/c;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 121
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/udc/e/c;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 123
    :cond_2
    iget v0, p0, Lcom/google/android/gms/udc/e/c;->e:I

    if-eqz v0, :cond_3

    .line 124
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/gms/udc/e/c;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 126
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/udc/e/c;->d:Z

    if-eqz v0, :cond_4

    .line 127
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/gms/udc/e/c;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 129
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 130
    return-void
.end method
