.class public final Lcom/google/android/gms/reminders/service/a/f;
.super Lcom/google/android/gms/reminders/service/a/a;
.source "SourceFile"


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/reminders/model/TaskEntity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/model/TaskEntity;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/gms/reminders/service/a/a;-><init>(Lcom/google/android/gms/reminders/internal/b;)V

    .line 31
    iput-object p2, p0, Lcom/google/android/gms/reminders/service/a/f;->b:Ljava/lang/String;

    .line 32
    iput-object p3, p0, Lcom/google/android/gms/reminders/service/a/f;->c:Lcom/google/android/gms/reminders/model/TaskEntity;

    .line 33
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 24
    check-cast p1, Lcom/google/android/gms/reminders/service/RemindersIntentService;

    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/f;->c:Lcom/google/android/gms/reminders/model/TaskEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->d()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/f;->c:Lcom/google/android/gms/reminders/model/TaskEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/reminders/model/TaskEntity;->d()Lcom/google/android/gms/reminders/model/TaskId;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/reminders/model/TaskId;->a()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "server_assigned_id=?"

    new-array v0, v4, [Ljava/lang/String;

    invoke-interface {v2}, Lcom/google/android/gms/reminders/model/TaskId;->a()Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/reminders/service/a/f;->c:Lcom/google/android/gms/reminders/model/TaskEntity;

    invoke-static {v2}, Lcom/google/android/gms/reminders/c/b;->a(Lcom/google/android/gms/reminders/model/TaskEntity;)Landroid/content/ContentValues;

    move-result-object v2

    const-string v3, "server_assigned_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v3, "client_assigned_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v3, "client_assigned_thread_id"

    invoke-virtual {v2, v3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    const-string v3, "is_dirty"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {p1}, Lcom/google/android/gms/reminders/service/RemindersIntentService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/reminders/internal/a/k;->a:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2, v1, v0}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/f;->a:Lcom/google/android/gms/reminders/internal/b;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v1, v5}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/reminders/internal/b;->a(Lcom/google/android/gms/common/api/Status;)V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/f;->b:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/gms/reminders/a/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/reminders/a/d;

    move-result-object v3

    if-nez v3, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/f;->a:Lcom/google/android/gms/reminders/internal/b;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x1770

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/reminders/internal/b;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_1

    :cond_1
    const-string v1, "client_assigned_id=? AND account_id=?"

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v2}, Lcom/google/android/gms/reminders/model/TaskId;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v5

    iget-wide v2, v3, Lcom/google/android/gms/reminders/a/d;->a:J

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    goto :goto_0
.end method
