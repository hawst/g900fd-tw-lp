.class public Lcom/google/android/gms/wallet/ia/IaRootActivity;
.super Lcom/google/android/gms/wallet/common/ui/ds;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/de;
.implements Lcom/google/android/gms/wallet/ia/u;


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/bb;

.field private b:Landroid/accounts/Account;

.field private c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private d:Lcom/google/android/gms/wallet/common/ui/TopBarView;

.field private e:Z

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ds;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 58
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/wallet/ia/IaRootActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 61
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 62
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 63
    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 64
    return-object v0
.end method

.method private a(Landroid/accounts/Account;)V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->b:Landroid/accounts/Account;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 186
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->b:Landroid/accounts/Account;

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->d:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Landroid/accounts/Account;)V

    .line 189
    :cond_0
    return-void
.end method

.method private a(Landroid/support/v4/app/Fragment;)V
    .locals 3

    .prologue
    .line 145
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gr:I

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, p1, v2}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 148
    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 276
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 277
    const-string v1, "com.google.android.libraries.inapp.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    const/4 v1, 0x1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->setResult(ILandroid/content/Intent;)V

    .line 279
    const/4 v0, 0x4

    const-string v1, "accountNotSet"

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->f:Ljava/lang/String;

    invoke-static {p0, v0, p2, v1, v2}, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 281
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->finish()V

    .line 282
    return-void
.end method

.method private h()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 198
    invoke-static {p0}, Lcom/google/android/gms/wallet/shared/common/d/b;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "inapp.RootActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 206
    :goto_0
    return-void

    .line 203
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->e:Z

    if-nez v0, :cond_2

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->e:Z

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->b:Landroid/accounts/Account;

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/wallet/shared/f;->a(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/dc;->a(Landroid/accounts/Account;[Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ui/dc;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    const-string v2, "RetrieveAuthTokensFragment"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 205
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->d:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a()V

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.gms.wallet.ACTION_START_BILLING_ENROLLMENT"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "com.google.android.gms.wallet.pcid"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.gms.wallet.infoText"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->b:Landroid/accounts/Account;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/wallet/ia/q;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/ia/q;

    move-result-object v0

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a(Landroid/support/v4/app/Fragment;)V

    goto :goto_0

    :cond_3
    const-string v1, "com.google.android.libraries.inapp.EXTRA_JWT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->b:Landroid/accounts/Account;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/ia/q;->a(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/ia/q;

    move-result-object v0

    goto :goto_1
.end method

.method private i()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 285
    invoke-virtual {p0, v3}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->setResult(I)V

    .line 286
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->b:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->f:Ljava/lang/String;

    invoke-static {p0, v0, v3, v1, v2}, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 288
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->finish()V

    .line 289
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    const-string v1, "accountNotSet"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->f:Ljava/lang/String;

    .line 246
    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 247
    const-string v0, "com.google.android.libraries.inapp.ERROR_CODE_NETWORK_ERROR"

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a(Ljava/lang/String;I)V

    .line 251
    :goto_0
    return-void

    .line 249
    :cond_0
    const-string v0, "com.google.android.libraries.inapp.ERROR_CODE_CANNOT_AUTHENTICATE"

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 255
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    .line 256
    packed-switch p1, :pswitch_data_0

    .line 262
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->i()V

    .line 268
    :goto_0
    return-void

    .line 258
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->h()V

    goto :goto_0

    .line 266
    :cond_0
    const-string v0, "RootActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 256
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->d:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/TopBarView;->a(Z)V

    .line 153
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->b:Landroid/accounts/Account;

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->f:Ljava/lang/String;

    invoke-static {p0, v0, v3, v1, v2}, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 155
    new-instance v0, Lcom/google/android/gms/wallet/ia/m;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/ia/m;-><init>()V

    .line 156
    invoke-virtual {v0, p2}, Lcom/google/android/gms/wallet/ia/m;->a(Ljava/lang/String;)V

    .line 157
    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/ia/m;->a(Ljava/lang/CharSequence;)V

    .line 158
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a(Landroid/support/v4/app/Fragment;)V

    .line 159
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 163
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    .line 164
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 165
    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, ""

    if-eqz p1, :cond_3

    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v3, v2

    :goto_0
    if-eqz v3, :cond_4

    const-string v0, "jwt"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "request"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v1, "response"

    invoke-virtual {v3, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    :goto_1
    invoke-static {v2}, Lcom/google/android/gms/wallet/common/k;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v3

    if-eqz v2, :cond_0

    const-string v5, "rawJson"

    invoke-virtual {v2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v5, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {v1}, Lcom/google/android/gms/wallet/common/k;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v2

    if-eqz v1, :cond_1

    const-string v5, "rawJson"

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v5, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const-string v1, "com.google.android.libraries.inapp.EXTRA_JWT"

    invoke-virtual {v4, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v0, "com.google.android.libraries.inapp.EXTRA_REQUEST"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v0, "com.google.android.libraries.inapp.EXTRA_RESPONSE"

    invoke-virtual {v4, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 167
    :cond_2
    const/4 v0, -0x1

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->setResult(ILandroid/content/Intent;)V

    .line 169
    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->b:Landroid/accounts/Account;

    iget-object v2, v2, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->f:Ljava/lang/String;

    invoke-static {p0, v0, v1, v2, v3}, Lcom/google/android/gms/wallet/analytics/events/IaPaymentClosedEvent;->a(Landroid/content/Context;IILjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    .line 171
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->finish()V

    .line 172
    return-void

    .line 165
    :catch_0
    move-exception v2

    const-string v2, "JwtUtils"

    const-string v3, "Invalid JSON in JWT body"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    move-object v3, v1

    goto :goto_0

    :cond_4
    move-object v2, v1

    goto :goto_1
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 226
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "RetrieveAuthTokensFragment"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 228
    if-eqz v0, :cond_0

    .line 229
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 233
    :cond_0
    return-void
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 237
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->i()V

    .line 238
    return-void
.end method

.method public final g()V
    .locals 0

    .prologue
    .line 176
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->i()V

    .line 177
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 141
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->i()V

    .line 142
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 85
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ds;->onCreate(Landroid/os/Bundle;)V

    .line 89
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "buyFlowConfig"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/dt;->c:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 94
    sget v0, Lcom/google/android/gms/l;->gn:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->setContentView(I)V

    .line 95
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/Window;)V

    .line 102
    :goto_0
    sget v0, Lcom/google/android/gms/j;->tc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/TopBarView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->d:Lcom/google/android/gms/wallet/common/ui/TopBarView;

    .line 104
    if-eqz p1, :cond_1

    .line 105
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a(Landroid/accounts/Account;)V

    .line 106
    const-string v0, "hasAuthTokens"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->e:Z

    .line 107
    const-string v0, "analyticsSessionId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->f:Ljava/lang/String;

    .line 117
    :goto_1
    return-void

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/dt;->b:Lcom/google/android/gms/wallet/common/ui/dz;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/app/Activity;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/common/ui/dz;)V

    .line 99
    sget v0, Lcom/google/android/gms/l;->gm:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->setContentView(I)V

    goto :goto_0

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c()Landroid/accounts/Account;

    move-result-object v0

    .line 111
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 112
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a(Landroid/accounts/Account;)V

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v1, v0}, Lcom/google/android/gms/wallet/analytics/events/IaPaymentLaunchedEvent;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->f:Ljava/lang/String;

    .line 115
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->h()V

    goto :goto_1
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 121
    invoke-super {p0}, Lcom/google/android/gms/wallet/common/ui/ds;->onResume()V

    .line 122
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ia/IaRootActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "inapp.RootActivity.NETWORK_ERROR_DIALOG"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->a:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 127
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 131
    const-string v0, "RootActivity"

    const-string v1, "Saving instance state..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ds;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 134
    const-string v0, "account"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->b:Landroid/accounts/Account;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 135
    const-string v0, "hasAuthTokens"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 136
    const-string v0, "analyticsSessionId"

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/IaRootActivity;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 137
    return-void
.end method
