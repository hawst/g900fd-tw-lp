.class public final Lcom/google/android/gms/games/ui/common/leaderboards/o;
.super Lcom/google/android/gms/games/ui/cw;
.source "SourceFile"


# static fields
.field private static final c:I


# instance fields
.field private final f:Lcom/google/android/gms/games/ui/common/leaderboards/p;

.field private h:I

.field private i:I

.field private j:Lcom/google/android/gms/games/e/k;

.field private k:Ljava/lang/String;

.field private l:Lcom/google/android/gms/games/Player;

.field private m:Landroid/net/Uri;

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    sget v0, Lcom/google/android/gms/l;->aY:I

    sput v0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->c:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IILcom/google/android/gms/games/ui/common/leaderboards/p;)V
    .locals 1

    .prologue
    .line 83
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/cw;-><init>(Landroid/content/Context;)V

    .line 85
    iput p3, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->h:I

    .line 86
    iput p2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->i:I

    .line 88
    iput-object p4, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->f:Lcom/google/android/gms/games/ui/common/leaderboards/p;

    .line 91
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->n:I

    .line 92
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/leaderboards/o;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->n:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/leaderboards/o;I)I
    .locals 0

    .prologue
    .line 38
    iput p1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->i:I

    return p1
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/leaderboards/o;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->i:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/leaderboards/o;I)I
    .locals 0

    .prologue
    .line 38
    iput p1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->h:I

    return p1
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->m:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->k:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/common/leaderboards/o;)I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->h:I

    return v0
.end method

.method static synthetic f(I)I
    .locals 3

    .prologue
    .line 38
    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid timeSpan: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Lcom/google/android/gms/games/e/k;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->j:Lcom/google/android/gms/games/e/k;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Lcom/google/android/gms/games/Player;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->l:Lcom/google/android/gms/games/Player;

    return-object v0
.end method

.method static synthetic h(I)I
    .locals 3

    .prologue
    .line 38
    packed-switch p0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid position: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic h(Lcom/google/android/gms/games/ui/common/leaderboards/o;)Lcom/google/android/gms/games/ui/common/leaderboards/p;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->f:Lcom/google/android/gms/games/ui/common/leaderboards/p;

    return-object v0
.end method


# virtual methods
.method protected final a(Landroid/view/ViewGroup;)Lcom/google/android/gms/games/ui/cx;
    .locals 4

    .prologue
    .line 134
    new-instance v0, Lcom/google/android/gms/games/ui/common/leaderboards/q;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->e:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/games/ui/common/leaderboards/o;->c:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/common/leaderboards/q;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/Player;)V
    .locals 0

    .prologue
    .line 123
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->l:Lcom/google/android/gms/games/Player;

    .line 124
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->c()V

    .line 125
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/e/k;)V
    .locals 0

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->j:Lcom/google/android/gms/games/e/k;

    .line 107
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->c()V

    .line 108
    return-void
.end method

.method public final a(Ljava/lang/String;ILandroid/net/Uri;)V
    .locals 0

    .prologue
    .line 111
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->k:Ljava/lang/String;

    .line 112
    iput p2, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->h:I

    .line 113
    iput-object p3, p0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->m:Landroid/net/Uri;

    .line 114
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/leaderboards/o;->c()V

    .line 115
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 129
    sget v0, Lcom/google/android/gms/games/ui/common/leaderboards/o;->c:I

    return v0
.end method
