.class public abstract Lcom/google/android/gms/googlehelp/c/a/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/w;
.implements Lcom/android/volley/x;


# instance fields
.field protected final a:Landroid/content/Context;

.field protected b:Lcom/google/android/gms/googlehelp/common/a;

.field final c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field final d:Lcom/google/android/gms/googlehelp/c/a/p;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;Lcom/google/android/gms/googlehelp/c/a/p;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/a/n;->a:Landroid/content/Context;

    .line 55
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/c/a/n;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 56
    iput-object p3, p0, Lcom/google/android/gms/googlehelp/c/a/n;->b:Lcom/google/android/gms/googlehelp/common/a;

    .line 57
    iput-object p4, p0, Lcom/google/android/gms/googlehelp/c/a/n;->d:Lcom/google/android/gms/googlehelp/c/a/p;

    .line 58
    return-void
.end method

.method private a()V
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->d:Lcom/google/android/gms/googlehelp/c/a/p;

    invoke-interface {v0}, Lcom/google/android/gms/googlehelp/c/a/p;->b()V

    .line 114
    return-void
.end method

.method private a(I)V
    .locals 1

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/googlehelp/c/a/n;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;I)V

    .line 180
    return-void
.end method

.method private b()I
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/a/n;->b(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I

    move-result v0

    return v0
.end method

.method private c()V
    .locals 8

    .prologue
    .line 126
    const/4 v0, 0x1

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/n;->b()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 127
    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-le v1, v0, :cond_0

    .line 128
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/n;->a()V

    .line 151
    :goto_0
    return-void

    .line 132
    :cond_0
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/a/n;->c(Lcom/google/android/gms/googlehelp/common/HelpConfig;)J

    move-result-wide v4

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/a/n;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I

    move-result v0

    .line 140
    int-to-long v6, v1

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    :goto_1
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    mul-long/2addr v0, v6

    .line 143
    new-instance v3, Lcom/google/android/gms/googlehelp/c/a/o;

    invoke-direct {v3, p0, v4, v5}, Lcom/google/android/gms/googlehelp/c/a/o;-><init>(Lcom/google/android/gms/googlehelp/c/a/n;J)V

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 140
    :cond_1
    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    goto :goto_1
.end method


# virtual methods
.method abstract a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I
.end method

.method abstract a(Lcom/google/protobuf/nano/j;)I
.end method

.method public final a(Lcom/android/volley/ac;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    .line 80
    :goto_0
    sparse-switch v0, :sswitch_data_0

    .line 106
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/n;->a()V

    .line 109
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->d:Lcom/google/android/gms/googlehelp/c/a/p;

    invoke-interface {v0}, Lcom/google/android/gms/googlehelp/c/a/p;->a()V

    .line 110
    :goto_2
    return-void

    .line 77
    :cond_0
    iget-object v0, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    goto :goto_0

    .line 84
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->d:Lcom/google/android/gms/googlehelp/c/a/p;

    invoke-interface {v0}, Lcom/google/android/gms/googlehelp/c/a/p;->c()V

    goto :goto_1

    .line 94
    :sswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/n;->b()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/c/a/n;->a(I)V

    .line 95
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/n;->c()V

    goto :goto_2

    .line 80
    nop

    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_1
        0xcd -> :sswitch_0
        0x1f4 -> :sswitch_1
        0x1f7 -> :sswitch_1
    .end sparse-switch
.end method

.method abstract a(Lcom/google/android/gms/googlehelp/common/HelpConfig;I)V
.end method

.method abstract a(Lcom/google/android/gms/googlehelp/common/HelpConfig;J)V
.end method

.method abstract a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/protobuf/nano/j;)V
.end method

.method public final a(Lcom/google/android/gms/googlehelp/common/a;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/c/a/n;->b:Lcom/google/android/gms/googlehelp/common/a;

    .line 172
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 21
    check-cast p1, Lcom/google/protobuf/nano/j;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/googlehelp/c/a/n;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I

    move-result v1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->a:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->c:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/googlehelp/c/a/n;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/protobuf/nano/j;)V

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/n;->b()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/c/a/n;->a(I)V

    :cond_0
    sget-object v0, Lcom/google/android/gms/googlehelp/b/a;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/c/a/n;->c()V

    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/c/a/n;->a(Lcom/google/protobuf/nano/j;)I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->d:Lcom/google/android/gms/googlehelp/c/a/p;

    invoke-interface {v0}, Lcom/google/android/gms/googlehelp/c/a/p;->d()V

    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/android/gms/googlehelp/c/a/n;->a(Lcom/google/protobuf/nano/j;)I

    move-result v0

    if-lt v0, v1, :cond_3

    const/4 v0, -0x1

    if-ne v1, v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/c/a/n;->d:Lcom/google/android/gms/googlehelp/c/a/p;

    invoke-interface {v0}, Lcom/google/android/gms/googlehelp/c/a/p;->a()V

    :cond_4
    return-void
.end method

.method abstract b(Lcom/google/android/gms/googlehelp/common/HelpConfig;)I
.end method

.method abstract c(Lcom/google/android/gms/googlehelp/common/HelpConfig;)J
.end method
