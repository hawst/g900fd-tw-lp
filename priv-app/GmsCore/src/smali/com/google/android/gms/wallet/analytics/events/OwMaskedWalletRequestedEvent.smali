.class public Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;
.super Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/m;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/m;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>(Landroid/os/Parcel;)V

    .line 40
    sget-object v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p1}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 41
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;-><init>()V

    .line 32
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;->j:Ljava/lang/String;

    .line 33
    iput-object p2, p0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;->a:Ljava/lang/String;

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 35
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)V

    .line 36
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;

    invoke-direct {v0, p2, p1}, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;)V

    .line 47
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 48
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 58
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/AnalyticsSessionStartEndEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->writeToParcel(Landroid/os/Parcel;I)V

    .line 60
    return-void
.end method
