.class public final Lcom/google/android/gms/analytics/internal/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static A:Lcom/google/android/gms/common/a/d;

.field public static B:Lcom/google/android/gms/common/a/d;

.field public static C:Lcom/google/android/gms/common/a/d;

.field public static D:Lcom/google/android/gms/common/a/d;

.field public static E:Lcom/google/android/gms/common/a/d;

.field public static a:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static c:Lcom/google/android/gms/common/a/d;

.field public static d:Lcom/google/android/gms/common/a/d;

.field public static e:Lcom/google/android/gms/common/a/d;

.field public static f:Lcom/google/android/gms/common/a/d;

.field public static g:Lcom/google/android/gms/common/a/d;

.field public static h:Lcom/google/android/gms/common/a/d;

.field public static i:Lcom/google/android/gms/common/a/d;

.field public static j:Lcom/google/android/gms/common/a/d;

.field public static k:Lcom/google/android/gms/common/a/d;

.field public static l:Lcom/google/android/gms/common/a/d;

.field public static m:Lcom/google/android/gms/common/a/d;

.field public static n:Lcom/google/android/gms/common/a/d;

.field public static o:Lcom/google/android/gms/common/a/d;

.field public static p:Lcom/google/android/gms/common/a/d;

.field public static q:Lcom/google/android/gms/common/a/d;

.field public static r:Lcom/google/android/gms/common/a/d;

.field public static s:Lcom/google/android/gms/common/a/d;

.field public static t:Lcom/google/android/gms/common/a/d;

.field public static u:Lcom/google/android/gms/common/a/d;

.field public static v:Lcom/google/android/gms/common/a/d;

.field public static w:Lcom/google/android/gms/common/a/d;

.field public static x:Lcom/google/android/gms/common/a/d;

.field public static y:Lcom/google/android/gms/common/a/d;

.field public static z:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v6, 0x1c00

    const/16 v5, 0x14

    const/4 v4, 0x0

    .line 15
    const-string v0, "analytics.service_enabled"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->a:Lcom/google/android/gms/common/a/d;

    .line 18
    const-string v0, "analytics.log_tag"

    const-string v1, "GA-SERVICE"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->b:Lcom/google/android/gms/common/a/d;

    .line 31
    const-string v0, "analytics.max_tokens"

    const-wide/16 v2, 0x3c

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->c:Lcom/google/android/gms/common/a/d;

    .line 33
    const-string v0, "analytics.tokens_per_sec"

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->d:Lcom/google/android/gms/common/a/d;

    .line 37
    const-string v0, "analytics.stale_hits_sec"

    const-wide v2, 0x9a7ec800L

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->e:Lcom/google/android/gms/common/a/d;

    .line 39
    const-string v0, "analytics.max_stored_hits"

    const/16 v1, 0x4e20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->f:Lcom/google/android/gms/common/a/d;

    .line 41
    const-string v0, "analytics.max_stored_hits_per_app"

    const/16 v1, 0x7d0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->g:Lcom/google/android/gms/common/a/d;

    .line 45
    const-string v0, "analytics.dispatch_period_sec"

    const/16 v1, 0x78

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->h:Lcom/google/android/gms/common/a/d;

    .line 47
    const-string v0, "analytics.max_hits_per_dispatch"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->i:Lcom/google/android/gms/common/a/d;

    .line 49
    const-string v0, "analytics.insecure_host"

    const-string v1, "http://www.google-analytics.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->j:Lcom/google/android/gms/common/a/d;

    .line 51
    const-string v0, "analytics.secure_host"

    const-string v1, "https://ssl.google-analytics.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->k:Lcom/google/android/gms/common/a/d;

    .line 53
    const-string v0, "analytics.simple_endpoint"

    const-string v1, "/collect"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->l:Lcom/google/android/gms/common/a/d;

    .line 55
    const-string v0, "analytics.batching_endpoint"

    const-string v1, "/batch"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->m:Lcom/google/android/gms/common/a/d;

    .line 57
    const-string v0, "analytics.max_get_length"

    const/16 v1, 0x7f4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->n:Lcom/google/android/gms/common/a/d;

    .line 61
    const-string v0, "analytics.batching_strategy.k"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->o:Lcom/google/android/gms/common/a/d;

    .line 63
    const-string v0, "analytics.compression_strategy.k"

    const-string v1, "NONE"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->p:Lcom/google/android/gms/common/a/d;

    .line 65
    const-string v0, "analytics.max_hits_per_request.k"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->q:Lcom/google/android/gms/common/a/d;

    .line 67
    const-string v0, "analytics.max_hit_length.k"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->r:Lcom/google/android/gms/common/a/d;

    .line 69
    const-string v0, "analytics.max_post_length.k"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->s:Lcom/google/android/gms/common/a/d;

    .line 71
    const-string v0, "analytics.fallback_responses.k"

    const-string v1, "302,404,502"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->t:Lcom/google/android/gms/common/a/d;

    .line 73
    const-string v0, "analytics.batch_retry_interval.seconds.k"

    const/16 v1, 0xe10

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->u:Lcom/google/android/gms/common/a/d;

    .line 89
    const-string v0, "analytics.dispatch_monitoring"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->v:Lcom/google/android/gms/common/a/d;

    .line 91
    const-string v0, "analytics.service_monitor_interval"

    const-wide/32 v2, 0x5265c00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->w:Lcom/google/android/gms/common/a/d;

    .line 95
    const-string v0, "analytics.use_ssaid_for_admob_join"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->x:Lcom/google/android/gms/common/a/d;

    .line 97
    const-string v0, "analytics.disable_http_retry"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->y:Lcom/google/android/gms/common/a/d;

    .line 101
    const-string v0, "analytics.first_party_experiment_id"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->z:Lcom/google/android/gms/common/a/d;

    .line 103
    const-string v0, "analytics.first_party_experiment_variant"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->A:Lcom/google/android/gms/common/a/d;

    .line 110
    const-string v0, "analytics.test.disable_receiver"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->B:Lcom/google/android/gms/common/a/d;

    .line 113
    const-string v0, "analytics.test.client_path"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->C:Lcom/google/android/gms/common/a/d;

    .line 115
    const-string v0, "analytics.test.client_host"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->D:Lcom/google/android/gms/common/a/d;

    .line 117
    const-string v0, "analytics.test.debug"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/analytics/internal/g;->E:Lcom/google/android/gms/common/a/d;

    return-void
.end method
