.class public final Lcom/google/android/gms/car/CarCall$Details;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:Landroid/net/Uri;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:J

.field public f:Landroid/net/Uri;

.field public g:Landroid/net/Uri;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 123
    new-instance v0, Lcom/google/android/gms/car/cs;

    invoke-direct {v0}, Lcom/google/android/gms/car/cs;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarCall$Details;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;JLandroid/net/Uri;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    iput p1, p0, Lcom/google/android/gms/car/CarCall$Details;->a:I

    .line 169
    iput-object p2, p0, Lcom/google/android/gms/car/CarCall$Details;->b:Landroid/net/Uri;

    .line 170
    iput-object p3, p0, Lcom/google/android/gms/car/CarCall$Details;->c:Ljava/lang/String;

    .line 171
    iput-object p4, p0, Lcom/google/android/gms/car/CarCall$Details;->d:Ljava/lang/String;

    .line 172
    iput-wide p5, p0, Lcom/google/android/gms/car/CarCall$Details;->e:J

    .line 173
    iput-object p7, p0, Lcom/google/android/gms/car/CarCall$Details;->f:Landroid/net/Uri;

    .line 174
    iput-object p8, p0, Lcom/google/android/gms/car/CarCall$Details;->g:Landroid/net/Uri;

    .line 175
    return-void
.end method

.method constructor <init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;JLandroid/net/Uri;Landroid/net/Uri;)V
    .locals 10

    .prologue
    .line 150
    const/4 v2, 0x1

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-wide v6, p4

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/car/CarCall$Details;-><init>(ILandroid/net/Uri;Ljava/lang/String;Ljava/lang/String;JLandroid/net/Uri;Landroid/net/Uri;)V

    .line 157
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/google/android/gms/car/CarCall$Details;->a:I

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 179
    iget v0, p0, Lcom/google/android/gms/car/CarCall$Details;->a:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v2, 0x27

    .line 193
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Details{handle="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall$Details;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", callerDisplayName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall$Details;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", disconnectCause=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall$Details;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", connectTimeMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/android/gms/car/CarCall$Details;->e:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gatewayInfoOriginal="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall$Details;->f:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", gatewayInfoGateway="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/CarCall$Details;->g:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 184
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/cs;->a(Lcom/google/android/gms/car/CarCall$Details;Landroid/os/Parcel;I)V

    .line 185
    return-void
.end method
