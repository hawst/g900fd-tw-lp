.class public final Lcom/google/android/gms/wallet/ow/d;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/dp;


# instance fields
.field a:Landroid/view/View;

.field b:Landroid/widget/ProgressBar;

.field c:Landroid/widget/TextView;

.field d:Landroid/widget/ImageView;

.field e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field f:Lcom/google/android/gms/wallet/common/ui/ax;

.field g:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

.field h:Lcom/google/android/gms/wallet/service/k;

.field i:Lcom/google/android/gms/wallet/common/ui/bb;

.field j:Lcom/google/android/gms/wallet/common/ui/bb;

.field k:I

.field l:Lcom/google/android/gms/wallet/payform/f;

.field protected final m:Lcom/google/android/gms/wallet/service/l;

.field private n:Lcom/google/android/gms/wallet/common/ui/aw;

.field private o:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private p:Z

.field private q:Landroid/accounts/Account;

.field private r:Lcom/google/checkout/inapp/proto/j;

.field private s:Lcom/google/aa/b/a/a/a/a/i;

.field private t:Lcom/google/android/gms/wallet/shared/common/b/a;

.field private u:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 126
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/d;->p:Z

    .line 138
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ow/d;->k:I

    .line 530
    new-instance v0, Lcom/google/android/gms/wallet/ow/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/ow/e;-><init>(Lcom/google/android/gms/wallet/ow/d;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->m:Lcom/google/android/gms/wallet/service/l;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/d;)Lcom/google/aa/b/a/a/a/a/i;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->s:Lcom/google/aa/b/a/a/a/a/i;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/d;Lcom/google/aa/b/a/a/a/a/j;)Lcom/google/android/gms/wallet/FullWallet;
    .locals 9

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->r:Lcom/google/checkout/inapp/proto/j;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/android/gms/wallet/Address;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->r:Lcom/google/checkout/inapp/proto/j;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v2, v0, Lcom/google/t/a/b;->t:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v0, v0, Lcom/google/t/a/b;->t:Ljava/lang/String;

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/d;->r:Lcom/google/checkout/inapp/proto/j;

    iget-object v2, v2, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v2, v0}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object v2

    invoke-static {}, Lcom/google/android/gms/wallet/FullWallet;->a()Lcom/google/android/gms/wallet/f;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/wallet/ProxyCard;

    iget-object v5, p1, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/wallet/ow/d;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v6}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    iget v7, p1, Lcom/google/aa/b/a/a/a/a/j;->c:I

    iget v8, p1, Lcom/google/aa/b/a/a/a/a/j;->d:I

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/google/android/gms/wallet/ProxyCard;-><init>(Ljava/lang/String;Ljava/lang/String;II)V

    invoke-virtual {v3, v4}, Lcom/google/android/gms/wallet/f;->a(Lcom/google/android/gms/wallet/ProxyCard;)Lcom/google/android/gms/wallet/f;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/d;->s:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v4, v4, Lcom/google/aa/b/a/a/a/a/i;->f:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/wallet/f;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/d;->s:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v4, v4, Lcom/google/aa/b/a/a/a/a/i;->g:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/wallet/dynamite/service/a/h;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/wallet/f;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/google/android/gms/wallet/f;->a(Lcom/google/android/gms/wallet/Address;)Lcom/google/android/gms/wallet/f;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/f;->a(Lcom/google/android/gms/identity/intents/model/UserAddress;)Lcom/google/android/gms/wallet/f;

    move-result-object v1

    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/f;->a([Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/f;->c(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    move-result-object v1

    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_0

    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/android/gms/wallet/Address;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/f;->b(Lcom/google/android/gms/wallet/Address;)Lcom/google/android/gms/wallet/f;

    move-result-object v2

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v3, v0}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)Lcom/google/android/gms/identity/intents/model/UserAddress;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/f;->b(Lcom/google/android/gms/identity/intents/model/UserAddress;)Lcom/google/android/gms/wallet/f;

    :cond_0
    iget-object v0, v1, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/j;Lcom/google/aa/b/a/a/a/a/i;ZLjava/lang/String;)Lcom/google/android/gms/wallet/ow/d;
    .locals 3

    .prologue
    .line 159
    new-instance v0, Lcom/google/android/gms/wallet/ow/d;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/ow/d;-><init>()V

    .line 160
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 161
    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 162
    const-string v2, "account"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 163
    const-string v2, "instrument"

    invoke-static {v1, v2, p2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 164
    const-string v2, "protoRequest"

    invoke-static {v1, v2, p3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 165
    const-string v2, "localMode"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 166
    const-string v2, "sessionId"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 167
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ow/d;->setArguments(Landroid/os/Bundle;)V

    .line 168
    return-object v0
.end method

.method private a()V
    .locals 2

    .prologue
    .line 321
    iget v0, p0, Lcom/google/android/gms/wallet/ow/d;->k:I

    if-gez v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->h:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/d;->m:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/d;->k:I

    .line 326
    :cond_0
    return-void
.end method

.method private a(I)V
    .locals 3

    .prologue
    .line 445
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 446
    const-string v1, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 447
    invoke-static {}, Lcom/google/android/gms/wallet/FullWallet;->a()Lcom/google/android/gms/wallet/f;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/d;->s:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v2, v2, Lcom/google/aa/b/a/a/a/a/i;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/f;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/d;->s:Lcom/google/aa/b/a/a/a/a/i;

    iget-object v2, v2, Lcom/google/aa/b/a/a/a/a/i;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/f;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    .line 451
    const-string v2, "com.google.android.gms.wallet.EXTRA_FULL_WALLET"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 452
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/ow/d;->a(ILandroid/content/Intent;)V

    .line 453
    return-void
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 440
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/q;->setResult(ILandroid/content/Intent;)V

    .line 441
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->finish()V

    .line 442
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/d;I)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/ow/d;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/d;ILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/ow/d;->a(ILandroid/content/Intent;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/d;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/d;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    invoke-static {p1, p2, p3}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "AuthenticateInstrumentF.OwErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 429
    if-eqz p1, :cond_0

    .line 430
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->b:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 434
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/d;->g:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    if-nez p1, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {v3, v0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 435
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-nez p1, :cond_2

    :goto_2
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 436
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/ow/d;->u:Z

    .line 437
    return-void

    .line 432
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->b:Landroid/widget/ProgressBar;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 434
    goto :goto_1

    :cond_2
    move v1, v2

    .line 435
    goto :goto_2
.end method

.method private b()V
    .locals 1

    .prologue
    .line 413
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->n:Lcom/google/android/gms/wallet/common/ui/aw;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/aw;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 414
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->g()Z

    .line 418
    :goto_0
    return-void

    .line 416
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/d;->c()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ow/d;)V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/d;->a(Z)V

    return-void
.end method

.method private c()V
    .locals 3

    .prologue
    .line 421
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/ow/d;->a(Z)V

    .line 422
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 423
    new-instance v1, Lcom/google/aa/b/a/a/a/a/c;

    invoke-direct {v1}, Lcom/google/aa/b/a/a/a/a/c;-><init>()V

    .line 424
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/d;->r:Lcom/google/checkout/inapp/proto/j;

    iget-object v2, v2, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/aa/b/a/a/a/a/c;->a:Ljava/lang/String;

    .line 425
    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/d;->h:Lcom/google/android/gms/wallet/service/k;

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/wallet/service/k;->a(Ljava/lang/String;Lcom/google/aa/b/a/a/a/a/c;)V

    .line 426
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/ow/d;)Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/d;->p:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/ow/d;)Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->q:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/ow/d;)V
    .locals 3

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/d;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "AuthenticateInstrumentF.NetworkErrorDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 364
    const/16 v0, 0x3e8

    if-ne p2, v0, :cond_0

    .line 365
    packed-switch p1, :pswitch_data_0

    .line 374
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/ow/d;->a(Z)V

    .line 380
    :goto_0
    return-void

    .line 367
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/d;->c()V

    goto :goto_0

    .line 370
    :pswitch_1
    const/4 v0, 0x0

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/ow/d;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 378
    :cond_0
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/ow/d;->a(I)V

    goto :goto_0

    .line 365
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 344
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->n:Lcom/google/android/gms/wallet/common/ui/aw;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/aw;->e()Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->n:Lcom/google/android/gms/wallet/common/ui/aw;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/aw;->f()Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->n:Lcom/google/android/gms/wallet/common/ui/aw;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/aw;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 355
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->clearFocus()V

    .line 356
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    .line 357
    const/4 v0, 0x1

    .line 359
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    .line 175
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 177
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 178
    const-string v0, "buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->o:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 179
    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->q:Landroid/accounts/Account;

    .line 180
    const-string v0, "instrument"

    const-class v2, Lcom/google/checkout/inapp/proto/j;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->r:Lcom/google/checkout/inapp/proto/j;

    .line 183
    const-string v0, "protoRequest"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 184
    const-string v0, "protoRequest"

    const-class v2, Lcom/google/aa/b/a/a/a/a/i;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/i;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->s:Lcom/google/aa/b/a/a/a/a/i;

    .line 186
    const-string v0, "localMode"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/ow/d;->p:Z

    .line 188
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->h:Lcom/google/android/gms/wallet/service/k;

    if-nez v0, :cond_0

    .line 189
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/d;->p:Z

    if-eqz v0, :cond_1

    .line 190
    const-string v0, "sessionId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 191
    new-instance v1, Lcom/google/android/gms/wallet/service/d;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/d;->o:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/d;->q:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4, v0}, Lcom/google/android/gms/wallet/service/d;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/ow/d;->h:Lcom/google/android/gms/wallet/service/k;

    .line 199
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->h:Lcom/google/android/gms/wallet/service/k;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/service/k;->a()V

    .line 202
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/wallet/payform/f;

    if-eqz v0, :cond_2

    .line 203
    check-cast p1, Lcom/google/android/gms/wallet/payform/f;

    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/d;->l:Lcom/google/android/gms/wallet/payform/f;

    .line 207
    :goto_1
    return-void

    .line 194
    :cond_1
    new-instance v0, Lcom/google/android/gms/wallet/service/e;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/d;->o:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/d;->q:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/wallet/service/e;-><init>(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->h:Lcom/google/android/gms/wallet/service/k;

    goto :goto_0

    .line 205
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->l:Lcom/google/android/gms/wallet/payform/f;

    goto :goto_1
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 384
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->pB:I

    if-ne v0, v1, :cond_0

    .line 385
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/d;->b()V

    .line 387
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 218
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 219
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/ow/d;->setRetainInstance(Z)V

    .line 221
    new-instance v0, Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/wallet/shared/common/b/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->t:Lcom/google/android/gms/wallet/shared/common/b/a;

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->t:Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/dynamite/image/a;->a(Landroid/content/Context;)Lcom/google/android/gms/wallet/dynamite/image/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/common/b/a;->a(Lcom/google/android/gms/wallet/dynamite/image/a;)V

    .line 224
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->d(Landroid/app/Activity;)V

    .line 226
    if-nez p1, :cond_1

    .line 228
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/d;->o:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/d;->o:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "authenticate_instrument"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 236
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->l:Lcom/google/android/gms/wallet/payform/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->l:Lcom/google/android/gms/wallet/payform/f;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/payform/f;->a(Z)V

    .line 237
    :cond_0
    return-void

    .line 232
    :cond_1
    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/ow/d;->k:I

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 266
    invoke-static {}, Lcom/google/android/gms/wallet/common/ui/dt;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 267
    sget v0, Lcom/google/android/gms/l;->gu:I

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 273
    :goto_0
    sget v0, Lcom/google/android/gms/j;->bv:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->a:Landroid/view/View;

    .line 274
    sget v0, Lcom/google/android/gms/j;->pN:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->b:Landroid/widget/ProgressBar;

    .line 275
    sget v0, Lcom/google/android/gms/j;->cx:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->c:Landroid/widget/TextView;

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->c:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/d;->r:Lcom/google/checkout/inapp/proto/j;

    iget-object v2, v2, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 277
    sget v0, Lcom/google/android/gms/j;->cz:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->d:Landroid/widget/ImageView;

    .line 278
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->d:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ow/d;->r:Lcom/google/checkout/inapp/proto/j;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/d;->t:Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/wallet/common/w;->a(Landroid/widget/ImageView;Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/common/b/a;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 284
    :goto_1
    sget v0, Lcom/google/android/gms/j;->dT:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->r:Lcom/google/checkout/inapp/proto/j;

    iget v2, v0, Lcom/google/checkout/inapp/proto/j;->c:I

    .line 286
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/aw;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/d;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v0, v3, v4, v2}, Lcom/google/android/gms/wallet/common/ui/aw;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->n:Lcom/google/android/gms/wallet/common/ui/aw;

    .line 288
    new-instance v0, Landroid/text/InputFilter$LengthFilter;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/w;->a(I)I

    move-result v3

    invoke-direct {v0, v3}, Landroid/text/InputFilter$LengthFilter;-><init>(I)V

    .line 290
    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/d;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-array v4, v5, [Landroid/text/InputFilter;

    aput-object v0, v4, v6

    invoke-virtual {v3, v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setFilters([Landroid/text/InputFilter;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v3, p0, Lcom/google/android/gms/wallet/ow/d;->n:Lcom/google/android/gms/wallet/common/ui/aw;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/dn;)V

    .line 292
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    .line 293
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v3, Lcom/google/android/gms/j;->dZ:I

    invoke-virtual {v0, v3}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ax;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->f:Lcom/google/android/gms/wallet/common/ui/ax;

    .line 295
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->f:Lcom/google/android/gms/wallet/common/ui/ax;

    if-nez v0, :cond_0

    .line 296
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/ax;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->f:Lcom/google/android/gms/wallet/common/ui/ax;

    .line 297
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v3, Lcom/google/android/gms/j;->dZ:I

    iget-object v4, p0, Lcom/google/android/gms/wallet/ow/d;->f:Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-virtual {v0, v3, v4}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->f:Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/ax;->a(I)V

    .line 302
    sget v0, Lcom/google/android/gms/j;->cj:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->g:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    .line 303
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->g:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Landroid/view/View$OnClickListener;)V

    .line 304
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->g:Lcom/google/android/gms/wallet/common/ui/ButtonBar;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/ButtonBar;->a(Z)V

    .line 306
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ow/d;->u:Z

    if-eqz v0, :cond_1

    .line 307
    invoke-direct {p0, v5}, Lcom/google/android/gms/wallet/ow/d;->a(Z)V

    .line 310
    :cond_1
    return-object v1

    .line 270
    :cond_2
    sget v0, Lcom/google/android/gms/l;->gt:I

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_0

    .line 282
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->d:Landroid/widget/ImageView;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_1
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 212
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 213
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->l:Lcom/google/android/gms/wallet/payform/f;

    .line 214
    return-void
.end method

.method public final onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 391
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/d;->e:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-ne p1, v1, :cond_0

    .line 392
    if-nez p3, :cond_1

    .line 393
    packed-switch p2, :pswitch_data_0

    .line 409
    :cond_0
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return v0

    .line 395
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/d;->b()V

    goto :goto_1

    .line 399
    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 401
    :pswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    packed-switch v1, :pswitch_data_2

    goto :goto_0

    .line 403
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/d;->b()V

    goto :goto_1

    .line 393
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch

    .line 399
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
    .end packed-switch

    .line 401
    :pswitch_data_2
    .packed-switch 0x42
        :pswitch_2
    .end packed-switch
.end method

.method public final onPause()V
    .locals 0

    .prologue
    .line 330
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 331
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/d;->a()V

    .line 332
    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    .line 241
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 243
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "AuthenticateInstrumentF.NetworkErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->i:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 251
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ow/d;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v1, "AuthenticateInstrumentF.OwErrorDialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 254
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    .line 256
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->j:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 259
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/d;->h:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/d;->m:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/ow/d;->k:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/ow/d;->k:I

    .line 260
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 336
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 338
    invoke-direct {p0}, Lcom/google/android/gms/wallet/ow/d;->a()V

    .line 339
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/ow/d;->k:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 340
    return-void
.end method
