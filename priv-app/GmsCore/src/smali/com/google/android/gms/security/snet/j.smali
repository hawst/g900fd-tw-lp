.class public Lcom/google/android/gms/security/snet/j;
.super Lcom/android/volley/p;
.source "SourceFile"


# static fields
.field private static final f:Ljava/lang/String;


# instance fields
.field private final g:Landroid/content/Context;

.field private final h:Lcom/android/volley/x;

.field private final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    const-class v0, Lcom/google/android/gms/security/snet/j;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/security/snet/j;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;I)V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2, p4}, Lcom/android/volley/p;-><init>(ILjava/lang/String;Lcom/android/volley/w;)V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/security/snet/j;->g:Landroid/content/Context;

    .line 28
    iput-object p3, p0, Lcom/google/android/gms/security/snet/j;->h:Lcom/android/volley/x;

    .line 29
    iput p5, p0, Lcom/google/android/gms/security/snet/j;->i:I

    .line 30
    return-void
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 3

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/gms/security/snet/a;

    iget-object v1, p0, Lcom/google/android/gms/security/snet/j;->g:Landroid/content/Context;

    iget v2, p0, Lcom/google/android/gms/security/snet/j;->i:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/security/snet/a;-><init>(Landroid/content/Context;I)V

    .line 43
    iget-object v1, p1, Lcom/android/volley/m;->b:[B

    invoke-virtual {v0, v1}, Lcom/google/android/gms/security/snet/a;->a([B)Z

    move-result v0

    .line 44
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {p1}, Lcom/android/volley/toolbox/i;->a(Lcom/android/volley/m;)Lcom/android/volley/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 15
    check-cast p1, Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/google/android/gms/security/snet/j;->h:Lcom/android/volley/x;

    invoke-interface {v0, p1}, Lcom/android/volley/x;->a(Ljava/lang/Object;)V

    return-void
.end method
