.class public final Lcom/google/android/gms/wallet/service/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Landroid/support/v4/g/h;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/wallet/service/a/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/gms/wallet/service/a/c;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/service/a/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/service/a/b;->a:Landroid/support/v4/g/h;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/wallet/service/a/a;)V
    .locals 0

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/a/b;->b:Ljava/lang/String;

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/a/b;->c:Lcom/google/android/gms/wallet/service/a/a;

    .line 50
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/checkout/a/a/a/c;
    .locals 2

    .prologue
    .line 175
    sget-object v0, Lcom/google/android/gms/wallet/service/a/b;->a:Landroid/support/v4/g/h;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/a/d;

    .line 181
    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/service/a/d;->c(Ljava/lang/String;)Lcom/google/checkout/a/a/a/c;

    move-result-object v0

    .line 182
    if-nez v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/a/b;->c:Lcom/google/android/gms/wallet/service/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/service/a/a;->a(Ljava/lang/String;)Lcom/google/checkout/a/a/a/c;

    move-result-object v0

    .line 185
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/checkout/inapp/proto/a/b;Z)Lcom/google/checkout/inapp/proto/a/b;
    .locals 2

    .prologue
    .line 85
    sget-object v0, Lcom/google/android/gms/wallet/service/a/b;->a:Landroid/support/v4/g/h;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/a/d;

    .line 88
    if-eqz p2, :cond_0

    .line 89
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->c:Lcom/google/android/gms/wallet/service/a/a;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/wallet/service/a/a;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    .line 91
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    .line 92
    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/service/a/d;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    .line 93
    return-object p1
.end method

.method public final a(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/a/a/a/c;Z)Lcom/google/checkout/inapp/proto/j;
    .locals 2

    .prologue
    .line 141
    sget-object v0, Lcom/google/android/gms/wallet/service/a/b;->a:Landroid/support/v4/g/h;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/a/d;

    .line 144
    if-eqz p3, :cond_0

    .line 145
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->c:Lcom/google/android/gms/wallet/service/a/a;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/gms/wallet/service/a/a;->a(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/a/a/a/c;)V

    .line 147
    :cond_0
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    .line 150
    monitor-enter v0

    .line 151
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/service/a/d;->a(Lcom/google/checkout/inapp/proto/j;)V

    .line 152
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/wallet/service/a/d;->a(Ljava/lang/String;Lcom/google/checkout/a/a/a/c;)V

    .line 153
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 154
    return-object p1

    .line 153
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final a()Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 67
    sget-object v0, Lcom/google/android/gms/wallet/service/a/b;->a:Landroid/support/v4/g/h;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/a/d;

    .line 70
    monitor-enter v0

    .line 71
    :try_start_0
    iget-boolean v1, v0, Lcom/google/android/gms/wallet/service/a/d;->a:Z

    if-nez v1, :cond_1

    .line 72
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/a/d;->a()V

    .line 73
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->c:Lcom/google/android/gms/wallet/service/a/a;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/service/a/a;->a()Ljava/util/ArrayList;

    move-result-object v1

    .line 74
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/checkout/inapp/proto/a/b;

    .line 75
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/service/a/d;->a(Lcom/google/checkout/inapp/proto/a/b;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 81
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    .line 77
    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, v0, Lcom/google/android/gms/wallet/service/a/d;->a:Z

    .line 80
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/a/d;->d()Ljava/util/ArrayList;

    move-result-object v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1
.end method

.method public final b(Ljava/lang/String;)Lcom/google/checkout/inapp/proto/j;
    .locals 2

    .prologue
    .line 189
    sget-object v0, Lcom/google/android/gms/wallet/service/a/b;->a:Landroid/support/v4/g/h;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/a/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/service/a/d;->a(Ljava/lang/String;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/util/ArrayList;
    .locals 3

    .prologue
    .line 121
    sget-object v0, Lcom/google/android/gms/wallet/service/a/b;->a:Landroid/support/v4/g/h;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/a/d;

    .line 124
    monitor-enter v0

    .line 125
    :try_start_0
    iget-boolean v1, v0, Lcom/google/android/gms/wallet/service/a/d;->b:Z

    if-nez v1, :cond_1

    .line 126
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/a/d;->b()V

    .line 127
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->c:Lcom/google/android/gms/wallet/service/a/a;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/service/a/a;->b()Ljava/util/ArrayList;

    move-result-object v1

    .line 129
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/checkout/inapp/proto/j;

    .line 130
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/service/a/d;->a(Lcom/google/checkout/inapp/proto/j;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 136
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1

    .line 132
    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, v0, Lcom/google/android/gms/wallet/service/a/d;->b:Z

    .line 135
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/a/d;->c()Ljava/util/ArrayList;

    move-result-object v1

    monitor-exit v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v1
.end method

.method public final b(Lcom/google/checkout/inapp/proto/a/b;Z)V
    .locals 3

    .prologue
    .line 97
    sget-object v0, Lcom/google/android/gms/wallet/service/a/b;->a:Landroid/support/v4/g/h;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/a/d;

    .line 100
    if-eqz p2, :cond_0

    .line 101
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->c:Lcom/google/android/gms/wallet/service/a/a;

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, p1}, Lcom/google/android/gms/wallet/service/a/a;->a(Ljava/lang/String;Lcom/google/checkout/inapp/proto/a/b;)V

    .line 103
    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/service/a/d;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    .line 104
    return-void
.end method

.method public final b(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/a/a/a/c;Z)V
    .locals 3

    .prologue
    .line 159
    sget-object v0, Lcom/google/android/gms/wallet/service/a/b;->a:Landroid/support/v4/g/h;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/a/d;

    .line 162
    if-eqz p3, :cond_0

    .line 163
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->c:Lcom/google/android/gms/wallet/service/a/a;

    iget-object v2, p1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, p1, p2}, Lcom/google/android/gms/wallet/service/a/a;->a(Ljava/lang/String;Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/a/a/a/c;)V

    .line 168
    :cond_0
    monitor-enter v0

    .line 169
    :try_start_0
    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/service/a/d;->a(Lcom/google/checkout/inapp/proto/j;)V

    .line 170
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/wallet/service/a/d;->a(Ljava/lang/String;Lcom/google/checkout/a/a/a/c;)V

    .line 171
    monitor-exit v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method public final c(Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/b;
    .locals 2

    .prologue
    .line 193
    sget-object v0, Lcom/google/android/gms/wallet/service/a/b;->a:Landroid/support/v4/g/h;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/a/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/service/a/d;->b(Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    return-object v0
.end method
