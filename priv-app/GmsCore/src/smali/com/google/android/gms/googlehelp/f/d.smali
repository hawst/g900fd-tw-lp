.class public final Lcom/google/android/gms/googlehelp/f/d;
.super Lcom/google/android/gms/googlehelp/f/a;
.source "SourceFile"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field private final d:Lcom/google/android/gms/googlehelp/common/HelpConfig;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/gms/googlehelp/f/a;-><init>(Lcom/google/android/gms/googlehelp/f/n;)V

    .line 34
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->g()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/d;->b:Landroid/content/Context;

    .line 35
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/f/d;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 36
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->e()Lcom/google/android/gms/googlehelp/common/HelpConfig;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/f/d;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 38
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 20
    check-cast p1, Lcom/google/android/gms/googlehelp/e/e;

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/d;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->c()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/gms/googlehelp/common/d;->b:Lcom/google/android/gms/googlehelp/common/d;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/d;->a()Lcom/google/android/gms/googlehelp/e/c;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/googlehelp/e/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/common/s;->a(Ljava/lang/String;I)Lcom/google/android/gms/googlehelp/e/c;

    move-result-object v2

    invoke-static {}, Lcom/google/android/gms/googlehelp/common/d;->d()Ljava/util/Map;

    move-result-object v3

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/google/android/gms/googlehelp/common/d;->a:Lcom/google/android/gms/googlehelp/common/d;

    iget v1, p1, Lcom/google/android/gms/googlehelp/e/e;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v5, p1, Lcom/google/android/gms/googlehelp/e/e;->b:[Lcom/google/android/gms/googlehelp/e/b;

    array-length v6, v5

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_1

    aget-object v7, v5, v1

    iget-object v0, v7, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/googlehelp/e/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/googlehelp/common/d;->b:Lcom/google/android/gms/googlehelp/common/d;

    :goto_1
    iget-object v7, v7, Lcom/google/android/gms/googlehelp/e/b;->b:Lcom/google/android/gms/googlehelp/e/d;

    iget-object v7, v7, Lcom/google/android/gms/googlehelp/e/d;->a:Ljava/lang/String;

    invoke-interface {v4, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, v7, Lcom/google/android/gms/googlehelp/e/b;->a:Lcom/google/android/gms/googlehelp/e/c;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/d;

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/google/android/gms/googlehelp/common/g;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/d;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/f/d;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/googlehelp/common/g;-><init>(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/g;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/d;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->R()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/common/h;->b(Ljava/util/Map;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/util/Map;)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/d;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Ljava/util/Map;)Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/d;->c:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->h()V

    :cond_2
    return-void
.end method

.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/d;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ak;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/f/d;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/f/d;->d:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/a/f;->a(Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)Lcom/google/android/gms/googlehelp/e/e;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
