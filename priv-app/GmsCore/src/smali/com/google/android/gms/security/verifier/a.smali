.class public final Lcom/google/android/gms/security/verifier/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/security/verifier/InternalApkUploadService;

.field final b:Ljava/io/File;

.field final c:[B

.field final d:Ljava/util/concurrent/BlockingQueue;

.field volatile e:Z

.field f:Lcom/android/volley/s;

.field g:Ljava/lang/String;

.field h:I

.field i:Ljava/io/InputStream;

.field j:I

.field private k:[B


# direct methods
.method public constructor <init>(Lcom/google/android/gms/security/verifier/InternalApkUploadService;Ljava/io/File;[B)V
    .locals 2

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/a;->d:Ljava/util/concurrent/BlockingQueue;

    .line 48
    iput-object p1, p0, Lcom/google/android/gms/security/verifier/a;->a:Lcom/google/android/gms/security/verifier/InternalApkUploadService;

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/security/verifier/a;->b:Ljava/io/File;

    .line 50
    iput-object p3, p0, Lcom/google/android/gms/security/verifier/a;->c:[B

    .line 51
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/security/verifier/a;)V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 23
    new-array v0, v1, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/gms/security/verifier/a;->j:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    sget-object v0, Lcom/google/android/gms/security/verifier/x;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget v3, p0, Lcom/google/android/gms/security/verifier/a;->h:I

    iget v4, p0, Lcom/google/android/gms/security/verifier/a;->j:I

    sub-int/2addr v3, v4

    if-gt v3, v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/security/verifier/a;->h:I

    iget v3, p0, Lcom/google/android/gms/security/verifier/a;->j:I

    sub-int/2addr v0, v3

    move v6, v1

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/security/verifier/a;->k:[B

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/security/verifier/a;->k:[B

    array-length v3, v3

    if-eq v3, v0, :cond_1

    :cond_0
    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/gms/security/verifier/a;->k:[B

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/security/verifier/a;->e:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/a;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    :goto_1
    return-void

    :cond_2
    move v6, v2

    goto :goto_0

    :cond_3
    iget v4, p0, Lcom/google/android/gms/security/verifier/a;->j:I

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/a;->k:[B

    iget-object v3, p0, Lcom/google/android/gms/security/verifier/a;->i:Ljava/io/InputStream;

    invoke-static {v0, v3}, Lcom/google/android/gms/security/verifier/ag;->a([BLjava/io/InputStream;)V

    iget v0, p0, Lcom/google/android/gms/security/verifier/a;->j:I

    iget-object v3, p0, Lcom/google/android/gms/security/verifier/a;->k:[B

    array-length v3, v3

    add-int/2addr v0, v3

    iput v0, p0, Lcom/google/android/gms/security/verifier/a;->j:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    iget-boolean v0, p0, Lcom/google/android/gms/security/verifier/a;->e:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/a;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v3, "Error occurred while reading apk"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v3, v2}, Lcom/google/android/gms/security/verifier/ah;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/a;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/security/verifier/a;->a:Lcom/google/android/gms/security/verifier/InternalApkUploadService;

    invoke-virtual {v0}, Lcom/google/android/gms/security/verifier/InternalApkUploadService;->b()Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "Aborting upload: network state changed or service was disabled"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/security/verifier/ah;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/security/verifier/a;->d:Ljava/util/concurrent/BlockingQueue;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/BlockingQueue;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    iget-object v7, p0, Lcom/google/android/gms/security/verifier/a;->f:Lcom/android/volley/s;

    new-instance v0, Lcom/google/android/gms/security/verifier/f;

    iget-object v1, p0, Lcom/google/android/gms/security/verifier/a;->g:Ljava/lang/String;

    new-instance v2, Lcom/google/android/gms/security/verifier/d;

    invoke-direct {v2, p0}, Lcom/google/android/gms/security/verifier/d;-><init>(Lcom/google/android/gms/security/verifier/a;)V

    new-instance v3, Lcom/google/android/gms/security/verifier/e;

    invoke-direct {v3, p0, v6}, Lcom/google/android/gms/security/verifier/e;-><init>(Lcom/google/android/gms/security/verifier/a;Z)V

    iget-object v5, p0, Lcom/google/android/gms/security/verifier/a;->k:[B

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/security/verifier/f;-><init>(Ljava/lang/String;Lcom/android/volley/w;Lcom/android/volley/x;I[BZ)V

    invoke-virtual {v7, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    goto :goto_1
.end method


# virtual methods
.method public final a(I)I
    .locals 6

    .prologue
    .line 141
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    int-to-long v2, p1

    const-wide/32 v4, 0xf4240

    mul-long/2addr v2, v4

    add-long/2addr v2, v0

    .line 144
    :goto_0
    :try_start_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    .line 145
    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    .line 146
    new-instance v0, Ljava/util/concurrent/TimeoutException;

    invoke-direct {v0}, Ljava/util/concurrent/TimeoutException;-><init>()V

    throw v0

    .line 151
    :catch_0
    move-exception v0

    goto :goto_0

    .line 148
    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/security/verifier/a;->d:Ljava/util/concurrent/BlockingQueue;

    sub-long v0, v2, v0

    sget-object v5, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4, v0, v1, v5}, Ljava/util/concurrent/BlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0
.end method
