.class final Lcom/google/android/gms/cast/service/c;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/service/CastOperationService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/service/CastOperationService;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 237
    iput-object p1, p0, Lcom/google/android/gms/cast/service/c;->a:Lcom/google/android/gms/cast/service/CastOperationService;

    .line 238
    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 239
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 245
    :goto_0
    :try_start_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 246
    invoke-static {}, Lcom/google/android/gms/cast/service/CastOperationService;->a()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "Thread was interrupted"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    :goto_1
    return-void

    .line 250
    :cond_0
    const/4 v1, 0x0

    .line 252
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/cast/service/CastOperationService;->b()Ljava/util/concurrent/LinkedBlockingQueue;

    move-result-object v0

    const-wide/16 v2, 0x4e20

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/concurrent/LinkedBlockingQueue;->poll(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/service/b;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    .line 257
    :goto_2
    if-nez v0, :cond_1

    .line 258
    :try_start_2
    invoke-static {}, Lcom/google/android/gms/cast/service/CastOperationService;->a()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "No operations for a while, stop the service"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/cast/service/c;->a:Lcom/google/android/gms/cast/service/CastOperationService;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/service/CastOperationService;->stopSelf()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_1

    .line 286
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/android/gms/cast/service/CastOperationService;->a()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "Unexpected thread termination"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->e(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/cast/service/c;->a:Lcom/google/android/gms/cast/service/CastOperationService;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/service/CastOperationService;->stopSelf()V

    goto :goto_1

    .line 254
    :catch_1
    move-exception v0

    :try_start_3
    invoke-static {}, Lcom/google/android/gms/cast/service/CastOperationService;->a()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v2, "Queue wait was interrupted"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    move-object v0, v1

    goto :goto_2

    .line 270
    :cond_1
    :try_start_4
    invoke-virtual {v0}, Lcom/google/android/gms/cast/service/b;->a()V
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 276
    :try_start_5
    invoke-virtual {v0}, Lcom/google/android/gms/cast/service/b;->b()V

    goto :goto_0

    :catch_2
    move-exception v1

    invoke-virtual {v0}, Lcom/google/android/gms/cast/service/b;->b()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lcom/google/android/gms/cast/service/b;->b()V

    throw v1
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0
.end method
