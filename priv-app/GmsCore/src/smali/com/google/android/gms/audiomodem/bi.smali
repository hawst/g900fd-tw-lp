.class public final Lcom/google/android/gms/audiomodem/bi;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/audiomodem/q;

.field b:Lcom/google/android/gms/audiomodem/Encoding;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/audiomodem/q;Lcom/google/android/gms/audiomodem/Encoding;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/bi;->a:Lcom/google/android/gms/audiomodem/q;

    .line 112
    iput-object p2, p0, Lcom/google/android/gms/audiomodem/bi;->b:Lcom/google/android/gms/audiomodem/Encoding;

    .line 113
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 124
    if-eqz p1, :cond_0

    instance-of v1, p1, Lcom/google/android/gms/audiomodem/bi;

    if-nez v1, :cond_1

    .line 128
    :cond_0
    :goto_0
    return v0

    .line 127
    :cond_1
    check-cast p1, Lcom/google/android/gms/audiomodem/bi;

    .line 128
    iget-object v1, p0, Lcom/google/android/gms/audiomodem/bi;->a:Lcom/google/android/gms/audiomodem/q;

    iget-object v2, p1, Lcom/google/android/gms/audiomodem/bi;->a:Lcom/google/android/gms/audiomodem/q;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/audiomodem/bi;->b:Lcom/google/android/gms/audiomodem/Encoding;

    iget-object v2, p1, Lcom/google/android/gms/audiomodem/bi;->b:Lcom/google/android/gms/audiomodem/Encoding;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 133
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/bi;->a:Lcom/google/android/gms/audiomodem/q;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/audiomodem/bi;->b:Lcom/google/android/gms/audiomodem/Encoding;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
