.class public final Lcom/google/android/gms/plus/apps/ba;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# static fields
.field private static a:Lcom/google/android/gms/plus/apps/ba;


# instance fields
.field private final b:Landroid/os/Handler;

.field private final c:Ljava/util/concurrent/ExecutorService;

.field private final d:Ljava/util/HashSet;

.field private final e:Lcom/google/android/gms/common/server/n;

.field private final f:Landroid/content/Context;

.field private final g:Landroid/graphics/BitmapFactory$Options;

.field private final h:Landroid/content/res/Resources;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->b:Landroid/os/Handler;

    .line 52
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->c:Ljava/util/concurrent/ExecutorService;

    .line 53
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->d:Ljava/util/HashSet;

    .line 54
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->f:Landroid/content/Context;

    .line 55
    new-instance v0, Lcom/google/android/gms/common/server/n;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ba;->f:Landroid/content/Context;

    const/4 v5, 0x0

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->e:Lcom/google/android/gms/common/server/n;

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->f:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->h:Landroid/content/res/Resources;

    .line 59
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->g:Landroid/graphics/BitmapFactory$Options;

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->g:Landroid/graphics/BitmapFactory$Options;

    const/16 v1, 0x140

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inDensity:I

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->g:Landroid/graphics/BitmapFactory$Options;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ba;->h:Landroid/content/res/Resources;

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inTargetDensity:I

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->g:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v4, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/apps/ba;)Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->h:Landroid/content/res/Resources;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/plus/apps/ba;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/google/android/gms/plus/apps/ba;->a:Lcom/google/android/gms/plus/apps/ba;

    if-nez v0, :cond_0

    .line 71
    new-instance v0, Lcom/google/android/gms/plus/apps/ba;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/apps/ba;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/plus/apps/ba;->a:Lcom/google/android/gms/plus/apps/ba;

    .line 73
    :cond_0
    sget-object v0, Lcom/google/android/gms/plus/apps/ba;->a:Lcom/google/android/gms/plus/apps/ba;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/apps/ba;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->f:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/plus/apps/ba;)Lcom/google/android/gms/common/server/n;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->e:Lcom/google/android/gms/common/server/n;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/apps/ba;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->b:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/plus/apps/ba;)Landroid/graphics/BitmapFactory$Options;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->g:Landroid/graphics/BitmapFactory$Options;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/plus/apps/bc;)V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 146
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->c:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/gms/plus/apps/bb;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/apps/bb;-><init>(Lcom/google/android/gms/plus/apps/ba;Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 84
    return-void
.end method

.method public final b(Lcom/google/android/gms/plus/apps/bc;)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/plus/apps/ba;->d:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 150
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 123
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 140
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 125
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/plus/apps/bb;

    .line 126
    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ba;->h:Landroid/content/res/Resources;

    invoke-static {v0}, Lcom/google/android/gms/plus/apps/bb;->a(Lcom/google/android/gms/plus/apps/bb;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-direct {v3, v1, v4}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    .line 127
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ba;->d:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/apps/bc;

    .line 128
    invoke-static {v0}, Lcom/google/android/gms/plus/apps/bb;->b(Lcom/google/android/gms/plus/apps/bb;)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v1, v5, v3}, Lcom/google/android/gms/plus/apps/bc;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_0
    move v0, v2

    .line 130
    goto :goto_0

    .line 133
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/plus/apps/bb;

    .line 134
    iget-object v1, p0, Lcom/google/android/gms/plus/apps/ba;->d:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/apps/bc;

    .line 135
    invoke-static {v0}, Lcom/google/android/gms/plus/apps/bb;->b(Lcom/google/android/gms/plus/apps/bb;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-interface {v1, v4, v5}, Lcom/google/android/gms/plus/apps/bc;->a(Ljava/lang/String;Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_1
    move v0, v2

    .line 137
    goto :goto_0

    .line 123
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
