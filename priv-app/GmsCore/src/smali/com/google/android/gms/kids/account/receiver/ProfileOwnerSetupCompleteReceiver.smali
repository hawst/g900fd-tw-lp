.class public Lcom/google/android/gms/kids/account/receiver/ProfileOwnerSetupCompleteReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 29
    const-string v0, "is_remove_account_key"

    invoke-virtual {p2, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 30
    const-string v1, "account_to_operate_on_key"

    invoke-virtual {p2, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 31
    const-string v2, "ProfileOwnerSetupCompleteReceiver"

    const-string v3, "onReceive: account=%s; isRemove=%s"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v6

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/kids/common/f;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 33
    if-eqz v0, :cond_0

    .line 34
    const-string v0, "ProfileOwnerSetupCompleteReceiver"

    const-string v2, "removing: %s"

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v1, v3, v6

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/kids/common/f;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/kids/common/d;->a(Landroid/content/Context;)Lcom/google/android/gms/kids/account/b;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/kids/account/b;->a(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/kids/account/receiver/a;

    invoke-direct {v3, p0, p1, v1}, Lcom/google/android/gms/kids/account/receiver/a;-><init>(Lcom/google/android/gms/kids/account/receiver/ProfileOwnerSetupCompleteReceiver;Landroid/content/Context;Ljava/lang/String;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Landroid/accounts/AccountManager;->removeAccount(Landroid/accounts/Account;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 36
    :cond_0
    const-class v0, Lcom/google/android/gms/kids/account/receiver/ProfileOwnerSetupCompleteReceiver;

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/a;->b(Landroid/content/Context;Ljava/lang/Class;)V

    .line 37
    return-void
.end method
