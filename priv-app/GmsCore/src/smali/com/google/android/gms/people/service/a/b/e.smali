.class public final Lcom/google/android/gms/people/service/a/b/e;
.super Lcom/google/android/gms/people/service/a/b/c;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 44
    move-object v0, p0

    move-object v1, p1

    move-object v2, p5

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/people/service/a/b/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/people/internal/f;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method private static a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 70
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move v0, v1

    .line 81
    :goto_0
    return v0

    .line 74
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;->b()Ljava/util/List;

    move-result-object v3

    .line 75
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_3

    .line 76
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;

    .line 77
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 78
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->f()Z

    move-result v0

    goto :goto_0

    .line 75
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    .line 81
    goto :goto_0
.end method

.method private static b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 86
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;->c()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 97
    :goto_0
    return-object v0

    .line 90
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;->b()Ljava/util/List;

    move-result-object v3

    .line 91
    const/4 v0, 0x0

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_3

    .line 92
    invoke-interface {v3, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;

    .line 93
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->h()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 94
    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fe;->k()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 91
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 97
    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/service/e;Lcom/google/android/gms/people/e/a;)Landroid/util/Pair;
    .locals 6

    .prologue
    .line 50
    invoke-static {p1}, Lcom/google/android/gms/plus/e/d;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, p3, v0}, Lcom/google/android/gms/people/service/e;->e(Lcom/google/android/gms/people/e/a;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;

    move-result-object v0

    .line 52
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 53
    const-string v2, "circles.firstTimeAdd.needConsent"

    invoke-static {v0, v2}, Lcom/google/android/gms/people/service/a/b/e;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;Ljava/lang/String;)Z

    move-result v2

    .line 55
    const-string v3, "circles.firstTimeAdd.text"

    invoke-static {v0, v3}, Lcom/google/android/gms/people/service/a/b/e;->b(Lcom/google/android/gms/plus/service/v1whitelisted/models/Settings;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 57
    sget v3, Lcom/google/android/gms/p;->rP:I

    invoke-virtual {p1, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 59
    sget v4, Lcom/google/android/gms/p;->rO:I

    invoke-virtual {p1, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 61
    const-string v5, "circles.first_time_add_need_consent"

    invoke-virtual {v1, v5, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 62
    const-string v2, "circles.first_time_add_text"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    const-string v0, "circles.first_time_add_title_text"

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 64
    const-string v0, "circles.first_time_add_ok_text"

    invoke-virtual {v1, v0, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    sget-object v0, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method
