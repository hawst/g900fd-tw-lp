.class public final Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/plus/service/pos/n;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/pos/s;

.field private static final a:Ljava/util/HashMap;


# instance fields
.field private final b:Ljava/util/Set;

.field private final c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 659
    new-instance v0, Lcom/google/android/gms/plus/service/pos/s;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/pos/s;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->CREATOR:Lcom/google/android/gms/plus/service/pos/s;

    .line 684
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 687
    sput-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->a:Ljava/util/HashMap;

    const-string v1, "displayName"

    const-string v2, "displayName"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 688
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->a:Ljava/util/HashMap;

    const-string v1, "id"

    const-string v2, "id"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 689
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->a:Ljava/util/HashMap;

    const-string v1, "profileUrl"

    const-string v2, "profileUrl"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 690
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->a:Ljava/util/HashMap;

    const-string v1, "thumbnailUrl"

    const-string v2, "thumbnailUrl"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 691
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 734
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 735
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->c:I

    .line 736
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->b:Ljava/util/Set;

    .line 737
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 747
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 748
    iput-object p1, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->b:Ljava/util/Set;

    .line 749
    iput p2, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->c:I

    .line 750
    iput-object p3, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->d:Ljava/lang/String;

    .line 751
    iput-object p4, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->e:Ljava/lang/String;

    .line 752
    iput-object p5, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->f:Ljava/lang/String;

    .line 753
    iput-object p6, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->g:Ljava/lang/String;

    .line 754
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 695
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 923
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 924
    packed-switch v0, :pswitch_data_0

    .line 938
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 926
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->d:Ljava/lang/String;

    .line 941
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->b:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 942
    return-void

    .line 929
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->e:Ljava/lang/String;

    goto :goto_0

    .line 932
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->f:Ljava/lang/String;

    goto :goto_0

    .line 935
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->g:Ljava/lang/String;

    goto :goto_0

    .line 924
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 877
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->b:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 882
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 892
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 884
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->d:Ljava/lang/String;

    .line 890
    :goto_0
    return-object v0

    .line 886
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->e:Ljava/lang/String;

    goto :goto_0

    .line 888
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->f:Ljava/lang/String;

    goto :goto_0

    .line 890
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->g:Ljava/lang/String;

    goto :goto_0

    .line 882
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 867
    const/4 v0, 0x0

    return-object v0
.end method

.method public final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 655
    return-object p0
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 872
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 792
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 856
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->CREATOR:Lcom/google/android/gms/plus/service/pos/s;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 843
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 970
    instance-of v0, p1, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;

    if-nez v0, :cond_0

    move v0, v1

    .line 1001
    :goto_0
    return v0

    .line 975
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 976
    goto :goto_0

    .line 979
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;

    .line 980
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 981
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 982
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 984
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 986
    goto :goto_0

    :cond_3
    move v0, v1

    .line 991
    goto :goto_0

    .line 994
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 996
    goto :goto_0

    :cond_5
    move v0, v2

    .line 1001
    goto :goto_0
.end method

.method final f()Ljava/util/Set;
    .locals 1

    .prologue
    .line 776
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->b:Ljava/util/Set;

    return-object v0
.end method

.method final g()I
    .locals 1

    .prologue
    .line 783
    iget v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->c:I

    return v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 809
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 957
    const/4 v0, 0x0

    .line 958
    sget-object v1, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 959
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 960
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 961
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 963
    goto :goto_0

    .line 964
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 826
    iget-object v0, p0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 861
    sget-object v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;->CREATOR:Lcom/google/android/gms/plus/service/pos/s;

    invoke-static {p0, p1}, Lcom/google/android/gms/plus/service/pos/s;->a(Lcom/google/android/gms/plus/service/pos/PlusonesEntity$MetadataEntity$GlobalCountsEntity$PersonEntity;Landroid/os/Parcel;)V

    .line 862
    return-void
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 952
    const/4 v0, 0x1

    return v0
.end method
