.class public Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"


# instance fields
.field private a:[Landroid/view/View;

.field private b:[Landroid/widget/TextView;

.field private c:[Landroid/view/View;

.field private d:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

.field private e:Lcom/google/android/gms/googlehelp/common/t;

.field private f:Lcom/google/android/gms/googlehelp/d/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 33
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->b:[Landroid/widget/TextView;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;)Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->d:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    return-object v0
.end method

.method private a(Z)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->getFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 152
    if-eqz p1, :cond_0

    .line 153
    invoke-virtual {v0, p0}, Landroid/support/v4/app/aj;->c(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 157
    :goto_0
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 158
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    .line 162
    :goto_1
    return-void

    .line 155
    :cond_0
    invoke-virtual {v0, p0}, Landroid/support/v4/app/aj;->b(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    goto :goto_0

    .line 160
    :cond_1
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;)Lcom/google/android/gms/googlehelp/common/t;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->e:Lcom/google/android/gms/googlehelp/common/t;

    return-object v0
.end method

.method private b()V
    .locals 8

    .prologue
    const/16 v7, 0x8

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->e:Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/t;->a()I

    move-result v0

    invoke-static {v6, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    move v1, v2

    .line 91
    :goto_0
    if-ge v1, v0, :cond_3

    .line 93
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->e:Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/googlehelp/common/t;->a(I)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v3

    .line 94
    iget-object v4, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->b:[Landroid/widget/TextView;

    aget-object v4, v4, v1

    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/common/k;->k()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/googlehelp/common/v;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 96
    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/common/k;->g()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 97
    iget-object v4, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->a:[Landroid/view/View;

    aget-object v4, v4, v1

    new-instance v5, Lcom/google/android/gms/googlehelp/fragments/p;

    invoke-direct {v5, p0, v3, v1}, Lcom/google/android/gms/googlehelp/fragments/p;-><init>(Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;Lcom/google/android/gms/googlehelp/common/k;I)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 123
    :cond_0
    :goto_1
    if-lez v1, :cond_1

    .line 124
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->c:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 126
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->a:[Landroid/view/View;

    aget-object v3, v3, v1

    invoke-virtual {v3, v2}, Landroid/view/View;->setVisibility(I)V

    .line 91
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 108
    :cond_2
    invoke-virtual {v3}, Lcom/google/android/gms/googlehelp/common/k;->i()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 109
    iget-object v4, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->a:[Landroid/view/View;

    aget-object v4, v4, v1

    new-instance v5, Lcom/google/android/gms/googlehelp/fragments/q;

    invoke-direct {v5, p0, v3, v1}, Lcom/google/android/gms/googlehelp/fragments/q;-><init>(Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;Lcom/google/android/gms/googlehelp/common/k;I)V

    invoke-virtual {v4, v5}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    .line 128
    :cond_3
    :goto_2
    if-ge v0, v6, :cond_5

    .line 129
    if-lez v0, :cond_4

    .line 130
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->c:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 132
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->a:[Landroid/view/View;

    aget-object v1, v1, v0

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 128
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 134
    :cond_5
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/googlehelp/common/t;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->e:Lcom/google/android/gms/googlehelp/common/t;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/googlehelp/common/k;)V
    .locals 5

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->e:Lcom/google/android/gms/googlehelp/common/t;

    if-nez v0, :cond_0

    .line 138
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->a(Z)V

    .line 139
    const-string v0, "recent_articles:"

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/common/k;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v2, Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v0

    const-string v3, "RECENT_ARTICLE_CLICKED"

    const-string v4, ""

    invoke-direct {v2, v0, v1, v3, v4}, Lcom/google/android/gms/googlehelp/common/t;-><init>(Ljava/lang/String;Ljava/util/Map;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->e:Lcom/google/android/gms/googlehelp/common/t;

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->e:Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/t;->e()Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, p1, v2}, Lcom/google/android/gms/googlehelp/common/k;->a(Lcom/google/android/gms/googlehelp/common/k;I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, v0, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    invoke-interface {v2, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/t;->b:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/k;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 143
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->b()V

    .line 145
    new-instance v0, Lcom/google/android/gms/googlehelp/f/p;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->f:Lcom/google/android/gms/googlehelp/d/b;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->e:Lcom/google/android/gms/googlehelp/common/t;

    invoke-virtual {v2}, Lcom/google/android/gms/googlehelp/common/t;->e()Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/googlehelp/f/p;-><init>(Lcom/google/android/gms/googlehelp/d/b;Lcom/google/android/gms/googlehelp/common/k;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/googlehelp/f/p;->a([Ljava/lang/Object;)V

    .line 147
    return-void
.end method

.method public final a(Lcom/google/android/gms/googlehelp/common/t;)V
    .locals 1

    .prologue
    .line 74
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/t;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->a(Z)V

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->e:Lcom/google/android/gms/googlehelp/common/t;

    .line 82
    :goto_0
    return-void

    .line 78
    :cond_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->a(Z)V

    .line 79
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->e:Lcom/google/android/gms/googlehelp/common/t;

    .line 80
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->b()V

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 66
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->d:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->d:Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/helpactivities/HelpFragment;->d()Lcom/google/android/gms/googlehelp/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->f:Lcom/google/android/gms/googlehelp/d/b;

    .line 70
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->a(Z)V

    .line 71
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x2

    const/4 v2, 0x0

    .line 42
    sget v0, Lcom/google/android/gms/l;->cl:I

    invoke-virtual {p1, v0, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 43
    sget v0, Lcom/google/android/gms/j;->jb:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->oM:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 46
    new-array v0, v6, [Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->iR:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v2

    sget v1, Lcom/google/android/gms/j;->iS:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v7

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->a:[Landroid/view/View;

    move v1, v2

    .line 51
    :goto_0
    if-ge v1, v6, :cond_0

    .line 52
    iget-object v4, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->b:[Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->a:[Landroid/view/View;

    aget-object v0, v0, v1

    sget v5, Lcom/google/android/gms/j;->hD:I

    invoke-virtual {v0, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    aput-object v0, v4, v1

    .line 51
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 56
    :cond_0
    new-array v0, v6, [Landroid/view/View;

    const/4 v1, 0x0

    aput-object v1, v0, v2

    sget v1, Lcom/google/android/gms/j;->iT:I

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    aput-object v1, v0, v7

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/fragments/RecentlyViewedFragment;->c:[Landroid/view/View;

    .line 61
    return-object v3
.end method
