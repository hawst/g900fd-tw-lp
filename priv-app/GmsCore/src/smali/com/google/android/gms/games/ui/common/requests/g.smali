.class public final Lcom/google/android/gms/games/ui/common/requests/g;
.super Lcom/google/android/gms/games/ui/card/b;
.source "SourceFile"


# instance fields
.field private final h:Lcom/google/android/gms/games/ui/common/requests/h;

.field private i:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/requests/h;)V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/common/requests/g;-><init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/requests/h;I)V

    .line 57
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/requests/h;I)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/card/b;-><init>(Landroid/content/Context;)V

    .line 63
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/common/requests/h;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/g;->h:Lcom/google/android/gms/games/ui/common/requests/h;

    .line 65
    sget v0, Lcom/google/android/gms/k;->m:I

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/ui/common/requests/g;->e(II)V

    .line 66
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/common/requests/g;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/g;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/common/requests/g;)Lcom/google/android/gms/games/ui/common/requests/h;
    .locals 1

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/requests/g;->h:Lcom/google/android/gms/games/ui/common/requests/h;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 79
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/google/android/gms/common/data/x;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 80
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/card/b;->a(Lcom/google/android/gms/common/data/d;)V

    .line 81
    return-void

    .line 79
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;ZLjava/util/ArrayList;)V
    .locals 7

    .prologue
    const/4 v3, -0x1

    .line 99
    const/4 v0, 0x0

    .line 102
    iget-object v1, p0, Lcom/google/android/gms/games/ui/d;->d:Lcom/google/android/gms/common/data/d;

    .line 103
    const/4 v2, 0x0

    invoke-interface {v1}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v4

    :goto_0
    if-ge v2, v4, :cond_5

    .line 104
    invoke-interface {v1, v2}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    .line 105
    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move-object v4, v0

    .line 111
    :goto_1
    if-eq v2, v3, :cond_1

    .line 141
    :goto_2
    return-void

    .line 103
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 116
    :cond_1
    if-eqz p2, :cond_2

    move-object v0, v1

    .line 118
    check-cast v0, Lcom/google/android/gms/common/data/x;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/data/x;->b(Ljava/lang/Object;)V

    goto :goto_2

    .line 122
    :cond_2
    invoke-virtual {v4}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->d()Ljava/util/ArrayList;

    move-result-object v3

    .line 124
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_3
    if-ltz v1, :cond_4

    .line 125
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->e()Ljava/lang/String;

    move-result-object v0

    .line 126
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 130
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 134
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 124
    :cond_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_3

    .line 138
    :cond_4
    invoke-virtual {v4, v3}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->a(Ljava/util/ArrayList;)V

    .line 139
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/common/requests/g;->c(I)V

    goto :goto_2

    :cond_5
    move v2, v3

    move-object v4, v0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/gms/games/ui/common/requests/g;->i:Ljava/lang/String;

    .line 75
    return-void
.end method

.method protected final b(Landroid/view/View;)Lcom/google/android/gms/games/ui/card/c;
    .locals 1

    .prologue
    .line 144
    new-instance v0, Lcom/google/android/gms/games/ui/common/requests/i;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/common/requests/i;-><init>(Landroid/view/View;)V

    return-object v0
.end method

.method protected final w()I
    .locals 1

    .prologue
    .line 149
    sget v0, Lcom/google/android/gms/j;->gI:I

    return v0
.end method

.method protected final x()I
    .locals 1

    .prologue
    .line 154
    const/4 v0, 0x3

    return v0
.end method
