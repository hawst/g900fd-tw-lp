.class public final Lcom/google/android/gms/ads/internal/l/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/gms/ads/internal/l/e;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/l/e;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 94
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/l/f;->c:Lcom/google/android/gms/ads/internal/l/e;

    iput-object p2, p0, Lcom/google/android/gms/ads/internal/l/f;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/ads/internal/l/f;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/f;->c:Lcom/google/android/gms/ads/internal/l/e;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/l/e;->c:Landroid/content/Context;

    const-string v1, "download"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 100
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/l/f;->c:Lcom/google/android/gms/ads/internal/l/e;

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/l/f;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/l/f;->b:Ljava/lang/String;

    new-instance v3, Landroid/app/DownloadManager$Request;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-direct {v3, v1}, Landroid/app/DownloadManager$Request;-><init>(Landroid/net/Uri;)V

    sget-object v1, Landroid/os/Environment;->DIRECTORY_PICTURES:Ljava/lang/String;

    invoke-virtual {v3, v1, v2}, Landroid/app/DownloadManager$Request;->setDestinationInExternalPublicDir(Ljava/lang/String;Ljava/lang/String;)Landroid/app/DownloadManager$Request;

    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v3}, Landroid/app/DownloadManager$Request;->allowScanningByMediaScanner()V

    const/4 v1, 0x1

    invoke-virtual {v3, v1}, Landroid/app/DownloadManager$Request;->setNotificationVisibility(I)Landroid/app/DownloadManager$Request;

    .line 101
    :goto_0
    invoke-virtual {v0, v3}, Landroid/app/DownloadManager;->enqueue(Landroid/app/DownloadManager$Request;)J

    .line 105
    :goto_1
    return-void

    .line 100
    :cond_0
    const/4 v1, 0x1

    invoke-virtual {v3, v1}, Landroid/app/DownloadManager$Request;->setShowRunningNotification(Z)Landroid/app/DownloadManager$Request;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 103
    :catch_0
    move-exception v0

    const-string v0, "Could not store picture."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;)V

    goto :goto_1
.end method
