.class public final Lcom/google/android/gms/appstate/service/AppStateIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 109
    const-string v0, "AppStateIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 110
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 64
    sget-object v6, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Lcom/google/android/gms/appstate/service/a/h;

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/appstate/service/a/h;-><init>(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/google/android/gms/appstate/service/c;)V

    .line 66
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;)V
    .locals 2

    .prologue
    .line 101
    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lcom/google/android/gms/appstate/service/a/i;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/appstate/service/a/i;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/google/android/gms/appstate/service/c;)V

    .line 102
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 88
    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lcom/google/android/gms/appstate/service/a/d;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/gms/appstate/service/a/d;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/google/android/gms/appstate/service/c;)V

    .line 90
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lcom/google/android/gms/appstate/service/a/c;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/android/gms/appstate/service/a/c;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;I)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/google/android/gms/appstate/service/c;)V

    .line 78
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;ILjava/lang/String;[B)V
    .locals 8

    .prologue
    .line 95
    sget-object v7, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Lcom/google/android/gms/appstate/service/a/f;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appstate/service/a/f;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;ILjava/lang/String;[B)V

    invoke-static {p0, v7, v0}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/google/android/gms/appstate/service/c;)V

    .line 97
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;I[B)V
    .locals 7

    .prologue
    .line 70
    sget-object v6, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v0, Lcom/google/android/gms/appstate/service/a/g;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/appstate/service/a/g;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;I[B)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/google/android/gms/appstate/service/c;)V

    .line 72
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 105
    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lcom/google/android/gms/appstate/service/a/b;

    invoke-direct {v1, p1}, Lcom/google/android/gms/appstate/service/a/b;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/google/android/gms/appstate/service/c;)V

    .line 106
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/google/android/gms/appstate/service/c;)V
    .locals 1

    .prologue
    .line 57
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    .line 58
    invoke-virtual {p1, p2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 59
    const-string v0, "com.google.android.gms.appstate.service.INTENT"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 60
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 82
    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v1, Lcom/google/android/gms/appstate/service/a/e;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/android/gms/appstate/service/a/e;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/appstate/c/h;Ljava/lang/String;I)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a(Landroid/content/Context;Ljava/util/concurrent/ConcurrentLinkedQueue;Lcom/google/android/gms/appstate/service/c;)V

    .line 84
    return-void
.end method


# virtual methods
.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 114
    sget-object v0, Lcom/google/android/gms/appstate/service/AppStateIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appstate/service/c;

    .line 115
    if-nez v0, :cond_0

    .line 116
    const-string v0, "AppStateIntentService"

    const-string v1, "operation missing"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 117
    const-string v0, "operation missing"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    .line 135
    :goto_0
    return-void

    .line 121
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/appstate/a/e;->a(Landroid/content/Context;)Lcom/google/android/gms/appstate/a/e;

    move-result-object v1

    .line 123
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 127
    invoke-interface {v0, p0, v1}, Lcom/google/android/gms/appstate/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/appstate/a/e;)V

    .line 128
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    invoke-virtual {v1}, Lcom/google/android/gms/appstate/a/e;->a()V

    goto :goto_0

    .line 131
    :catch_0
    move-exception v0

    .line 132
    :try_start_1
    const-string v2, "AppStateIntentService"

    const-string v3, "Auth error executing an operation: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 134
    invoke-virtual {v1}, Lcom/google/android/gms/appstate/a/e;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/appstate/a/e;->a()V

    throw v0
.end method
