.class public final Lcom/google/android/gms/wallet/common/ui/ai;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/am;


# instance fields
.field private final a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/ai;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    .line 17
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 23
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ai;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a()I

    move-result v1

    const/4 v2, 0x1

    if-eq v1, v2, :cond_0

    .line 26
    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ai;->a:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method
