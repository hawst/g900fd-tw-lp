.class public final Lcom/google/android/gms/fitness/b/c/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Comparator;


# static fields
.field private static final a:[I


# instance fields
.field private final b:Z

.field private final c:Z

.field private final d:Z

.field private final e:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/fitness/b/c/an;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x3
        0x1
        0x2
    .end array-data
.end method

.method private constructor <init>(Lcom/google/android/gms/fitness/b/c/ao;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iget-boolean v0, p1, Lcom/google/android/gms/fitness/b/c/ao;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/fitness/b/c/an;->b:Z

    .line 31
    iget-boolean v0, p1, Lcom/google/android/gms/fitness/b/c/ao;->c:Z

    iput-boolean v0, p0, Lcom/google/android/gms/fitness/b/c/an;->d:Z

    .line 32
    iget-boolean v0, p1, Lcom/google/android/gms/fitness/b/c/ao;->b:Z

    iput-boolean v0, p0, Lcom/google/android/gms/fitness/b/c/an;->c:Z

    .line 33
    iget-object v0, p1, Lcom/google/android/gms/fitness/b/c/ao;->d:[I

    if-nez v0, :cond_0

    .line 34
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Device type order must be provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 36
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/fitness/b/c/ao;->d:[I

    iput-object v0, p0, Lcom/google/android/gms/fitness/b/c/an;->e:[I

    .line 37
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/fitness/b/c/ao;B)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/b/c/an;-><init>(Lcom/google/android/gms/fitness/b/c/ao;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/fitness/b/i;)I
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 137
    if-nez p1, :cond_1

    .line 138
    const/16 v0, 0x64

    .line 142
    :cond_0
    :goto_0
    return v0

    .line 141
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/fitness/b/c/an;->e:[I

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/i;->a()I

    move-result v3

    const/4 v0, 0x0

    :goto_1
    array-length v4, v2

    if-ge v0, v4, :cond_3

    aget v4, v2, v0

    if-ne v4, v3, :cond_2

    .line 142
    :goto_2
    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/an;->e:[I

    array-length v0, v0

    goto :goto_0

    .line 141
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method private a(Lcom/google/android/gms/fitness/b/i;Lcom/google/android/gms/fitness/b/i;)I
    .locals 2

    .prologue
    .line 129
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/b/c/an;->a(Lcom/google/android/gms/fitness/b/i;)I

    move-result v0

    invoke-direct {p0, p2}, Lcom/google/android/gms/fitness/b/c/an;->a(Lcom/google/android/gms/fitness/b/i;)I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method static synthetic a()[I
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/google/android/gms/fitness/b/c/an;->a:[I

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/b/e;Lcom/google/android/gms/fitness/b/e;)I
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    const/4 v0, -0x1

    .line 41
    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->b()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p2}, Lcom/google/android/gms/fitness/b/e;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    const-string v4, "Cannot compare sensors of dissimilar data type"

    invoke-static {v3, v4}, Lcom/google/k/a/ah;->a(ZLjava/lang/Object;)V

    .line 45
    invoke-static {p1}, Lcom/google/android/gms/fitness/b/c/w;->d(Lcom/google/android/gms/fitness/b/e;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {p2}, Lcom/google/android/gms/fitness/b/c/w;->d(Lcom/google/android/gms/fitness/b/e;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 46
    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->f()Lcom/google/android/gms/fitness/b/i;

    move-result-object v0

    invoke-interface {p2}, Lcom/google/android/gms/fitness/b/e;->f()Lcom/google/android/gms/fitness/b/i;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/fitness/b/c/an;->a(Lcom/google/android/gms/fitness/b/i;Lcom/google/android/gms/fitness/b/i;)I

    move-result v0

    .line 111
    :cond_0
    :goto_0
    return v0

    .line 48
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/fitness/b/c/w;->d(Lcom/google/android/gms/fitness/b/e;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 51
    invoke-static {p2}, Lcom/google/android/gms/fitness/b/c/w;->d(Lcom/google/android/gms/fitness/b/e;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v1

    .line 52
    goto :goto_0

    .line 55
    :cond_2
    iget-boolean v3, p0, Lcom/google/android/gms/fitness/b/c/an;->b:Z

    if-eqz v3, :cond_4

    .line 57
    invoke-static {p1}, Lcom/google/android/gms/fitness/b/c/w;->a(Lcom/google/android/gms/fitness/b/e;)Z

    move-result v3

    .line 58
    invoke-static {p2}, Lcom/google/android/gms/fitness/b/c/w;->a(Lcom/google/android/gms/fitness/b/e;)Z

    move-result v4

    .line 60
    if-eqz v3, :cond_3

    if-nez v4, :cond_3

    move v0, v1

    .line 61
    goto :goto_0

    .line 63
    :cond_3
    if-eqz v4, :cond_4

    if-eqz v3, :cond_0

    .line 68
    :cond_4
    iget-boolean v3, p0, Lcom/google/android/gms/fitness/b/c/an;->c:Z

    if-eqz v3, :cond_6

    .line 70
    invoke-static {p1}, Lcom/google/android/gms/fitness/b/c/w;->b(Lcom/google/android/gms/fitness/b/e;)Z

    move-result v3

    .line 71
    invoke-static {p2}, Lcom/google/android/gms/fitness/b/c/w;->b(Lcom/google/android/gms/fitness/b/e;)Z

    move-result v4

    .line 73
    if-eqz v3, :cond_5

    if-eqz v4, :cond_5

    move v0, v2

    .line 74
    goto :goto_0

    .line 77
    :cond_5
    if-nez v3, :cond_0

    .line 80
    if-eqz v4, :cond_6

    move v0, v1

    .line 81
    goto :goto_0

    .line 85
    :cond_6
    iget-boolean v3, p0, Lcom/google/android/gms/fitness/b/c/an;->d:Z

    if-eqz v3, :cond_8

    .line 87
    invoke-static {p1}, Lcom/google/android/gms/fitness/b/c/w;->c(Lcom/google/android/gms/fitness/b/e;)Z

    move-result v3

    .line 88
    invoke-static {p2}, Lcom/google/android/gms/fitness/b/c/w;->c(Lcom/google/android/gms/fitness/b/e;)Z

    move-result v4

    .line 89
    if-eqz v3, :cond_7

    if-eqz v4, :cond_7

    move v0, v2

    .line 90
    goto :goto_0

    .line 92
    :cond_7
    if-nez v3, :cond_0

    .line 95
    if-eqz v4, :cond_8

    move v0, v1

    .line 96
    goto :goto_0

    .line 101
    :cond_8
    sget-object v2, Lcom/google/android/gms/fitness/b/h;->a:Lcom/google/android/gms/fitness/b/h;

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->e()Lcom/google/android/gms/fitness/b/h;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/h;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->e()Lcom/google/android/gms/fitness/b/h;

    move-result-object v2

    invoke-interface {p2}, Lcom/google/android/gms/fitness/b/e;->e()Lcom/google/android/gms/fitness/b/h;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/h;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 103
    const-string v2, "com.google.activity.segment"

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->d()Ljava/lang/String;

    move-result-object v2

    const-string v3, "detailed"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {p2}, Lcom/google/android/gms/fitness/b/e;->d()Ljava/lang/String;

    move-result-object v2

    const-string v3, "default"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_9
    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->d()Ljava/lang/String;

    move-result-object v0

    const-string v2, "default"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {p2}, Lcom/google/android/gms/fitness/b/e;->d()Ljava/lang/String;

    move-result-object v0

    const-string v2, "detailed"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_a

    move v0, v1

    goto/16 :goto_0

    :cond_a
    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->f()Lcom/google/android/gms/fitness/b/i;

    move-result-object v0

    invoke-interface {p2}, Lcom/google/android/gms/fitness/b/e;->f()Lcom/google/android/gms/fitness/b/i;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/fitness/b/c/an;->a(Lcom/google/android/gms/fitness/b/i;Lcom/google/android/gms/fitness/b/i;)I

    move-result v0

    goto/16 :goto_0

    .line 104
    :cond_b
    sget-object v2, Lcom/google/android/gms/fitness/b/h;->a:Lcom/google/android/gms/fitness/b/h;

    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->e()Lcom/google/android/gms/fitness/b/h;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/b/h;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 106
    sget-object v0, Lcom/google/android/gms/fitness/b/h;->a:Lcom/google/android/gms/fitness/b/h;

    invoke-interface {p2}, Lcom/google/android/gms/fitness/b/e;->e()Lcom/google/android/gms/fitness/b/h;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/fitness/b/h;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    move v0, v1

    .line 107
    goto/16 :goto_0

    .line 111
    :cond_c
    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/e;->f()Lcom/google/android/gms/fitness/b/i;

    move-result-object v0

    invoke-interface {p2}, Lcom/google/android/gms/fitness/b/e;->f()Lcom/google/android/gms/fitness/b/i;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/fitness/b/c/an;->a(Lcom/google/android/gms/fitness/b/i;Lcom/google/android/gms/fitness/b/i;)I

    move-result v0

    goto/16 :goto_0
.end method

.method public final synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/gms/fitness/b/e;

    check-cast p2, Lcom/google/android/gms/fitness/b/e;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/fitness/b/c/an;->a(Lcom/google/android/gms/fitness/b/e;Lcom/google/android/gms/fitness/b/e;)I

    move-result v0

    return v0
.end method
