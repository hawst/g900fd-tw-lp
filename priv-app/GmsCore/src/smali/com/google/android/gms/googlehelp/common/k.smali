.class public final Lcom/google/android/gms/googlehelp/common/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/googlehelp/common/k;

.field public static final b:Lcom/google/android/gms/googlehelp/common/k;


# instance fields
.field private final c:Ljava/lang/String;

.field private final d:I

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:J

.field private k:Z

.field private l:Z

.field private final m:Ljava/util/LinkedList;

.field private n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/gms/googlehelp/common/l;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/common/l;-><init>()V

    const/4 v1, 0x5

    iput v1, v0, Lcom/google/android/gms/googlehelp/common/l;->b:I

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/l;->a()Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/common/k;->a:Lcom/google/android/gms/googlehelp/common/k;

    .line 60
    new-instance v0, Lcom/google/android/gms/googlehelp/common/l;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/common/l;-><init>()V

    const/4 v1, 0x6

    iput v1, v0, Lcom/google/android/gms/googlehelp/common/l;->b:I

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/l;->a()Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/common/k;->b:Lcom/google/android/gms/googlehelp/common/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 230
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    .line 231
    iput p2, p0, Lcom/google/android/gms/googlehelp/common/k;->d:I

    .line 232
    iput-object p3, p0, Lcom/google/android/gms/googlehelp/common/k;->e:Ljava/lang/String;

    .line 233
    iput-object p4, p0, Lcom/google/android/gms/googlehelp/common/k;->f:Ljava/lang/String;

    .line 234
    iput-object p5, p0, Lcom/google/android/gms/googlehelp/common/k;->g:Ljava/lang/String;

    .line 235
    iput-object p6, p0, Lcom/google/android/gms/googlehelp/common/k;->h:Ljava/lang/String;

    .line 236
    iput-object p7, p0, Lcom/google/android/gms/googlehelp/common/k;->i:Ljava/lang/String;

    .line 237
    iput-wide p8, p0, Lcom/google/android/gms/googlehelp/common/k;->j:J

    .line 239
    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/common/k;->k:Z

    .line 240
    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/common/k;->l:Z

    .line 241
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->m:Ljava/util/LinkedList;

    .line 242
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->n:Ljava/lang/String;

    .line 243
    return-void
.end method

.method synthetic constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JB)V
    .locals 0

    .prologue
    .line 44
    invoke-direct/range {p0 .. p9}, Lcom/google/android/gms/googlehelp/common/k;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)V

    return-void
.end method

.method public static a()Lcom/google/android/gms/googlehelp/common/k;
    .locals 2

    .prologue
    .line 639
    const-string v0, "notification_root"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/googlehelp/common/k;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/googlehelp/OfflineSuggestion;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 2

    .prologue
    .line 419
    new-instance v0, Lcom/google/android/gms/googlehelp/common/l;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/common/l;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/OfflineSuggestion;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/l;->a:Ljava/lang/String;

    const/4 v1, 0x4

    iput v1, v0, Lcom/google/android/gms/googlehelp/common/l;->b:I

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/OfflineSuggestion;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/l;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/OfflineSuggestion;->c()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/l;->d:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/OfflineSuggestion;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/l;->e:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/l;->a()Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 4

    .prologue
    .line 480
    new-instance v0, Lcom/google/android/gms/googlehelp/common/l;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/common/l;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/l;->a:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/gms/googlehelp/common/k;->d:I

    iput v1, v0, Lcom/google/android/gms/googlehelp/common/l;->b:I

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/k;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/l;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/common/k;->l()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/l;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/k;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/l;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/k;->h:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/l;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/k;->i:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/googlehelp/common/l;->g:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/android/gms/googlehelp/common/k;->j:J

    iput-wide v2, v0, Lcom/google/android/gms/googlehelp/common/l;->h:J

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/l;->a()Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    .line 491
    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/common/k;->k:Z

    iput-boolean v1, v0, Lcom/google/android/gms/googlehelp/common/k;->k:Z

    .line 492
    iget-boolean v1, p0, Lcom/google/android/gms/googlehelp/common/k;->l:Z

    iput-boolean v1, v0, Lcom/google/android/gms/googlehelp/common/k;->l:Z

    .line 493
    return-object v0
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 2

    .prologue
    .line 250
    new-instance v0, Lcom/google/android/gms/googlehelp/common/l;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/common/l;-><init>()V

    iput-object p0, v0, Lcom/google/android/gms/googlehelp/common/l;->a:Ljava/lang/String;

    const/4 v1, 0x0

    iput v1, v0, Lcom/google/android/gms/googlehelp/common/l;->b:I

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/l;->a()Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/googlehelp/common/HelpConfig;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 13

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v6, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 318
    :try_start_0
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 324
    invoke-virtual {v5}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    .line 325
    if-eqz v0, :cond_0

    const-string v1, "support.google.com"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v6

    .line 406
    :goto_0
    return-object v0

    .line 319
    :catch_0
    move-exception v0

    .line 320
    const-string v1, "HelpResponse"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v6

    .line 321
    goto :goto_0

    .line 329
    :cond_1
    invoke-virtual {v5}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v7

    .line 330
    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v8

    .line 331
    invoke-virtual {v5}, Landroid/net/Uri;->getFragment()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/common/k;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 334
    const-string v4, ""

    .line 336
    if-lez v8, :cond_2

    if-eqz v1, :cond_2

    .line 340
    const-string v0, "/"

    invoke-interface {v7, v3, v8}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    move-object v5, v1

    move v1, v2

    .line 396
    :goto_1
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-eqz v7, :cond_d

    move-object v0, v6

    .line 397
    goto :goto_0

    .line 342
    :cond_2
    if-lt v8, v11, :cond_c

    .line 354
    add-int/lit8 v0, v8, -0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "answer.py"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    add-int/lit8 v0, v8, -0x2

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "bin"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    add-int/lit8 v0, v8, -0x2

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "answer"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_3
    move v1, v2

    .line 360
    :goto_2
    add-int/lit8 v0, v8, -0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v9, "topic.py"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    add-int/lit8 v0, v8, -0x2

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v9, "bin"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    add-int/lit8 v0, v8, -0x2

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v9, "topic"

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_4
    move v0, v2

    .line 363
    :goto_3
    if-eqz v1, :cond_7

    .line 365
    const-string v0, "answer"

    invoke-virtual {v5, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    move v0, v3

    .line 390
    :goto_4
    const-string v5, "/"

    add-int/lit8 v8, v8, -0x2

    invoke-interface {v7, v3, v8}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    move-object v12, v5

    move-object v5, v1

    move v1, v0

    move-object v0, v12

    .line 392
    goto/16 :goto_1

    :cond_5
    move v1, v3

    .line 354
    goto :goto_2

    :cond_6
    move v0, v3

    .line 360
    goto :goto_3

    .line 366
    :cond_7
    if-eqz v0, :cond_8

    .line 368
    const-string v0, "topic"

    invoke-virtual {v5, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    move v0, v2

    .line 369
    goto :goto_4

    .line 370
    :cond_8
    add-int/lit8 v0, v8, -0x2

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "answer"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 372
    add-int/lit8 v0, v8, -0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 374
    const-string v1, "cc"

    invoke-virtual {v5, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 375
    if-eqz v1, :cond_9

    .line 376
    const-string v4, "&extra_param=%s.%s"

    new-array v5, v10, [Ljava/lang/Object;

    const-string v9, "cc"

    aput-object v9, v5, v3

    aput-object v1, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move-object v1, v0

    move v0, v3

    goto :goto_4

    .line 378
    :cond_9
    const-string v1, "dc"

    invoke-virtual {v5, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 379
    if-eqz v1, :cond_f

    .line 380
    const-string v4, "&extra_param=%s.%s"

    new-array v5, v10, [Ljava/lang/Object;

    const-string v9, "dc"

    aput-object v9, v5, v3

    aput-object v1, v5, v2

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_5
    move-object v4, v1

    move-object v1, v0

    move v0, v3

    .line 383
    goto :goto_4

    :cond_a
    add-int/lit8 v0, v8, -0x2

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "topic"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 385
    add-int/lit8 v0, v8, -0x1

    invoke-interface {v7, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v1, v0

    move v0, v2

    .line 386
    goto/16 :goto_4

    :cond_b
    move-object v0, v6

    .line 388
    goto/16 :goto_0

    :cond_c
    move-object v0, v6

    .line 393
    goto/16 :goto_0

    .line 400
    :cond_d
    if-eqz v1, :cond_e

    sget-object v1, Lcom/google/android/gms/googlehelp/common/d;->e:Lcom/google/android/gms/googlehelp/common/d;

    :goto_6
    invoke-virtual {p1, v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->a(Lcom/google/android/gms/googlehelp/common/d;)Ljava/lang/String;

    move-result-object v1

    .line 403
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    new-array v7, v11, [Ljava/lang/Object;

    aput-object v5, v7, v3

    aput-object v0, v7, v2

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v7, v10

    invoke-static {v1, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 406
    new-instance v1, Lcom/google/android/gms/googlehelp/common/l;

    invoke-direct {v1}, Lcom/google/android/gms/googlehelp/common/l;-><init>()V

    iput-object v5, v1, Lcom/google/android/gms/googlehelp/common/l;->a:Ljava/lang/String;

    iput v2, v1, Lcom/google/android/gms/googlehelp/common/l;->b:I

    iput-object p0, v1, Lcom/google/android/gms/googlehelp/common/l;->e:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/common/l;->f:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/l;->a()Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    goto/16 :goto_0

    .line 400
    :cond_e
    sget-object v1, Lcom/google/android/gms/googlehelp/common/d;->d:Lcom/google/android/gms/googlehelp/common/d;

    goto :goto_6

    :cond_f
    move-object v1, v4

    goto :goto_5
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 2

    .prologue
    .line 467
    new-instance v0, Lcom/google/android/gms/googlehelp/common/l;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/common/l;-><init>()V

    iput-object p0, v0, Lcom/google/android/gms/googlehelp/common/l;->a:Ljava/lang/String;

    const/4 v1, 0x3

    iput v1, v0, Lcom/google/android/gms/googlehelp/common/l;->b:I

    iput-object p1, v0, Lcom/google/android/gms/googlehelp/common/l;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/l;->a()Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 2

    .prologue
    .line 452
    new-instance v0, Lcom/google/android/gms/googlehelp/common/l;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/common/l;-><init>()V

    iput-object p0, v0, Lcom/google/android/gms/googlehelp/common/l;->a:Ljava/lang/String;

    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/gms/googlehelp/common/l;->b:I

    iput-object p1, v0, Lcom/google/android/gms/googlehelp/common/l;->c:Ljava/lang/String;

    iput-object p3, v0, Lcom/google/android/gms/googlehelp/common/l;->d:Ljava/lang/String;

    iput-object p2, v0, Lcom/google/android/gms/googlehelp/common/l;->e:Ljava/lang/String;

    iput-object p4, v0, Lcom/google/android/gms/googlehelp/common/l;->g:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/l;->a()Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/gms/googlehelp/common/k;
    .locals 2

    .prologue
    .line 280
    new-instance v0, Lcom/google/android/gms/googlehelp/common/l;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/common/l;-><init>()V

    iput-object p0, v0, Lcom/google/android/gms/googlehelp/common/l;->a:Ljava/lang/String;

    const/4 v1, 0x1

    iput v1, v0, Lcom/google/android/gms/googlehelp/common/l;->b:I

    iput-object p1, v0, Lcom/google/android/gms/googlehelp/common/l;->c:Ljava/lang/String;

    iput-object p2, v0, Lcom/google/android/gms/googlehelp/common/l;->d:Ljava/lang/String;

    iput-object p3, v0, Lcom/google/android/gms/googlehelp/common/l;->e:Ljava/lang/String;

    iput-object p4, v0, Lcom/google/android/gms/googlehelp/common/l;->f:Ljava/lang/String;

    iput-object p5, v0, Lcom/google/android/gms/googlehelp/common/l;->g:Ljava/lang/String;

    iput-wide p6, v0, Lcom/google/android/gms/googlehelp/common/l;->h:J

    invoke-virtual {v0}, Lcom/google/android/gms/googlehelp/common/l;->a()Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lorg/json/JSONObject;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 6

    .prologue
    .line 590
    const-string v0, "type"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "type"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v1, "HELP_CENTER_LINK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "title"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "renderingUrl"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 594
    :cond_0
    const/4 v0, 0x0

    .line 597
    :goto_0
    return-object v0

    :cond_1
    const-string v0, "answerId"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "title"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v0, "snippet"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "snippet"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    const-string v3, "url"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "renderingUrl"

    invoke-virtual {p0, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/googlehelp/common/l;

    invoke-direct {v5}, Lcom/google/android/gms/googlehelp/common/l;-><init>()V

    iput-object v1, v5, Lcom/google/android/gms/googlehelp/common/l;->a:Ljava/lang/String;

    const/4 v1, 0x1

    iput v1, v5, Lcom/google/android/gms/googlehelp/common/l;->b:I

    iput-object v2, v5, Lcom/google/android/gms/googlehelp/common/l;->c:Ljava/lang/String;

    iput-object v0, v5, Lcom/google/android/gms/googlehelp/common/l;->d:Ljava/lang/String;

    iput-object v3, v5, Lcom/google/android/gms/googlehelp/common/l;->e:Ljava/lang/String;

    iput-object v4, v5, Lcom/google/android/gms/googlehelp/common/l;->f:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/google/android/gms/googlehelp/common/l;->a()Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method public static a(Lorg/json/JSONObject;Lcom/google/android/gms/googlehelp/common/k;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 5

    .prologue
    .line 619
    const-string v0, "html"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 620
    const/4 v0, 0x0

    .line 623
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "answer_id:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v0, "title"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "title"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v3, p1, Lcom/google/android/gms/googlehelp/common/k;->g:Ljava/lang/String;

    const-string v1, "html"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v1, "etag"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "etag"

    invoke-virtual {p0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-static {v2, v0, v3, v4, v1}, Lcom/google/android/gms/googlehelp/common/k;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/googlehelp/common/k;->e:Ljava/lang/String;

    goto :goto_1

    :cond_2
    const-string v1, ""

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/util/Map;
    .locals 6

    .prologue
    .line 545
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 546
    invoke-static {p0}, Lcom/google/android/gms/googlehelp/common/k;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v2

    .line 547
    invoke-interface {v1, p0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 549
    const-string v0, "recommendations"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v3

    .line 550
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v3}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_1

    .line 551
    invoke-virtual {v3, v0}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/googlehelp/common/k;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v4

    .line 552
    if-eqz v4, :cond_0

    .line 553
    invoke-virtual {v2, v4}, Lcom/google/android/gms/googlehelp/common/k;->c(Lcom/google/android/gms/googlehelp/common/k;)V

    .line 556
    iget-object v5, v4, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    invoke-interface {v1, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 550
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 559
    :cond_1
    return-object v1
.end method

.method public static b(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;
    .locals 1

    .prologue
    .line 647
    const-string v0, "notification_message"

    invoke-static {v0, p0}, Lcom/google/android/gms/googlehelp/common/k;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;Lorg/json/JSONObject;)Ljava/util/Map;
    .locals 3

    .prologue
    .line 574
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 575
    invoke-static {p0}, Lcom/google/android/gms/googlehelp/common/k;->a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v1

    .line 576
    invoke-interface {v0, p0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 578
    invoke-static {p1}, Lcom/google/android/gms/googlehelp/common/k;->a(Lorg/json/JSONObject;)Lcom/google/android/gms/googlehelp/common/k;

    move-result-object v2

    .line 579
    if-nez v2, :cond_0

    .line 580
    const/4 v0, 0x0

    .line 585
    :goto_0
    return-object v0

    .line 582
    :cond_0
    invoke-virtual {v1, v2}, Lcom/google/android/gms/googlehelp/common/k;->c(Lcom/google/android/gms/googlehelp/common/k;)V

    .line 583
    iget-object v1, v2, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static e(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 429
    if-nez p0, :cond_1

    .line 438
    :cond_0
    :goto_0
    return-object v0

    .line 433
    :cond_1
    const-string v1, "&"

    invoke-virtual {p0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 434
    const-string v5, "topic="

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 435
    const/4 v0, 0x6

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 433
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private w()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 683
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    const-string v1, "id"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "type"

    iget v2, p0, Lcom/google/android/gms/googlehelp/common/k;->d:I

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "title"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "snippet"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "url"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "apiUrl"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "etag"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "visited_time"

    iget-wide v2, p0, Lcom/google/android/gms/googlehelp/common/k;->j:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "has_latest_leaf_answer_in_database"

    iget-boolean v2, p0, Lcom/google/android/gms/googlehelp/common/k;->k:Z

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "child_ids"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->m:Ljava/util/LinkedList;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "parent_id"

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->n:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 695
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Converting to JSONObject failed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/common/k;I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 786
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->m:Ljava/util/LinkedList;

    iget-object v1, p1, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 787
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->m:Ljava/util/LinkedList;

    iget-object v1, p1, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 788
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/android/gms/googlehelp/common/k;->n:Ljava/lang/String;

    .line 789
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->m:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeLast()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(J)V
    .locals 1

    .prologue
    .line 755
    iput-wide p1, p0, Lcom/google/android/gms/googlehelp/common/k;->j:J

    .line 756
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 760
    iput-boolean p1, p0, Lcom/google/android/gms/googlehelp/common/k;->k:Z

    .line 761
    return-void
.end method

.method public final b()Ljava/lang/String;
    .locals 5

    .prologue
    .line 656
    :try_start_0
    new-instance v2, Ljava/net/URI;

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->g:Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    .line 660
    invoke-virtual {v2}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v0

    .line 661
    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 663
    const/4 v1, 0x0

    const-string v3, "/"

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 665
    :goto_0
    invoke-virtual {v2}, Ljava/net/URI;->getScheme()Ljava/lang/String;

    move-result-object v0

    .line 666
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 667
    const-string v0, "http"

    .line 669
    :cond_0
    new-instance v3, Ljava/net/URI;

    invoke-virtual {v2}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x0

    invoke-direct {v3, v0, v2, v1, v4}, Ljava/net/URI;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 673
    :goto_1
    return-object v0

    .line 671
    :catch_0
    move-exception v0

    const-string v0, "http"

    goto :goto_1

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 0

    .prologue
    .line 765
    iput-boolean p1, p0, Lcom/google/android/gms/googlehelp/common/k;->l:Z

    .line 766
    return-void
.end method

.method public final b(Lcom/google/android/gms/googlehelp/common/k;)Z
    .locals 2

    .prologue
    .line 732
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/common/k;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 734
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->m:Ljava/util/LinkedList;

    iget-object v1, p1, Lcom/google/android/gms/googlehelp/common/k;->m:Ljava/util/LinkedList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 736
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->e:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/googlehelp/common/k;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->g:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/googlehelp/common/k;->g:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->h:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/googlehelp/common/k;->h:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 795
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/googlehelp/common/k;)V
    .locals 2

    .prologue
    .line 770
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->m:Ljava/util/LinkedList;

    iget-object v1, p1, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->addLast(Ljava/lang/Object;)V

    .line 771
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    iput-object v0, p1, Lcom/google/android/gms/googlehelp/common/k;->n:Ljava/lang/String;

    .line 772
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 745
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/common/k;->e:Ljava/lang/String;

    .line 746
    return-void
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 800
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "answer_id:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 750
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/common/k;->i:Ljava/lang/String;

    .line 751
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 805
    iget v0, p0, Lcom/google/android/gms/googlehelp/common/k;->d:I

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 701
    if-ne p0, p1, :cond_1

    .line 709
    :cond_0
    :goto_0
    return v0

    .line 704
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/googlehelp/common/k;

    if-nez v2, :cond_2

    move v0, v1

    .line 705
    goto :goto_0

    .line 708
    :cond_2
    check-cast p1, Lcom/google/android/gms/googlehelp/common/k;

    .line 709
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/google/android/gms/googlehelp/common/k;->d:I

    iget v3, p1, Lcom/google/android/gms/googlehelp/common/k;->d:I

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/common/k;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/common/k;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/common/k;->g:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/common/k;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/common/k;->i:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/google/android/gms/googlehelp/common/k;->j:J

    iget-wide v4, p1, Lcom/google/android/gms/googlehelp/common/k;->j:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-boolean v2, p0, Lcom/google/android/gms/googlehelp/common/k;->k:Z

    iget-boolean v3, p1, Lcom/google/android/gms/googlehelp/common/k;->k:Z

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/gms/googlehelp/common/k;->l:Z

    iget-boolean v3, p1, Lcom/google/android/gms/googlehelp/common/k;->l:Z

    if-ne v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->m:Ljava/util/LinkedList;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/common/k;->m:Ljava/util/LinkedList;

    invoke-virtual {v2, v3}, Ljava/util/LinkedList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->n:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/googlehelp/common/k;->n:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 810
    iget v0, p0, Lcom/google/android/gms/googlehelp/common/k;->d:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 815
    iget v1, p0, Lcom/google/android/gms/googlehelp/common/k;->d:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 825
    iget v0, p0, Lcom/google/android/gms/googlehelp/common/k;->d:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 725
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/googlehelp/common/k;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/googlehelp/common/k;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->f:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->g:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->h:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->i:Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/android/gms/googlehelp/common/k;->j:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/android/gms/googlehelp/common/k;->k:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/android/gms/googlehelp/common/k;->l:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->m:Ljava/util/LinkedList;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/common/k;->n:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 830
    iget v0, p0, Lcom/google/android/gms/googlehelp/common/k;->d:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 835
    iget v0, p0, Lcom/google/android/gms/googlehelp/common/k;->d:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 840
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 2

    .prologue
    .line 846
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/common/k;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 856
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 861
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final o()Ljava/lang/String;
    .locals 1

    .prologue
    .line 871
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->h:Ljava/lang/String;

    return-object v0
.end method

.method public final p()Ljava/lang/String;
    .locals 1

    .prologue
    .line 876
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final q()J
    .locals 2

    .prologue
    .line 886
    iget-wide v0, p0, Lcom/google/android/gms/googlehelp/common/k;->j:J

    return-wide v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 891
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/common/k;->k:Z

    return v0
.end method

.method public final s()Z
    .locals 1

    .prologue
    .line 896
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/common/k;->l:Z

    return v0
.end method

.method public final t()Ljava/util/List;
    .locals 1

    .prologue
    .line 901
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->m:Ljava/util/LinkedList;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 678
    invoke-direct {p0}, Lcom/google/android/gms/googlehelp/common/k;->w()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final u()Ljava/lang/String;
    .locals 1

    .prologue
    .line 906
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final v()Z
    .locals 1

    .prologue
    .line 911
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/common/k;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method
