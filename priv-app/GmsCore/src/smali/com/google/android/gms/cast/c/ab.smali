.class abstract Lcom/google/android/gms/cast/c/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I

.field private static final d:I

.field private static final e:I

.field private static final f:I

.field private static final g:Lcom/google/android/gms/cast/internal/k;

.field private static final u:[I


# instance fields
.field private h:I

.field private final i:[Ljava/lang/String;

.field private j:Ljava/net/MulticastSocket;

.field private k:Ljava/net/InetSocketAddress;

.field private final l:[B

.field private m:Ljava/lang/Thread;

.field private n:Ljava/lang/Thread;

.field private volatile o:Z

.field private final p:Ljava/net/NetworkInterface;

.field private final q:Lcom/google/android/gms/cast/c/ar;

.field private final r:Lcom/google/android/gms/cast/c/as;

.field private s:I

.field private t:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/gms/cast/a/c;->c:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/c/ab;->a:I

    .line 33
    sget-object v0, Lcom/google/android/gms/cast/a/c;->d:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/c/ab;->b:I

    .line 35
    sget-object v0, Lcom/google/android/gms/cast/a/c;->e:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/c/ab;->c:I

    .line 37
    sget-object v0, Lcom/google/android/gms/cast/a/c;->f:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/c/ab;->d:I

    .line 39
    sget-object v0, Lcom/google/android/gms/cast/a/c;->g:Lcom/google/android/gms/common/a/d;

    invoke-static {v0}, Lcom/google/android/gms/common/c/a;->c(Lcom/google/android/gms/common/a/d;)I

    move-result v0

    sput v0, Lcom/google/android/gms/cast/c/ab;->e:I

    .line 45
    sget v0, Lcom/google/android/gms/cast/c/ab;->b:I

    sget v1, Lcom/google/android/gms/cast/c/ab;->e:I

    sget v2, Lcom/google/android/gms/cast/c/ab;->c:I

    mul-int/2addr v1, v2

    add-int/2addr v0, v1

    sput v0, Lcom/google/android/gms/cast/c/ab;->f:I

    .line 48
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "MdnsClient"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/c/ab;->g:Lcom/google/android/gms/cast/internal/k;

    .line 61
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/16 v2, 0xc

    aput v2, v0, v1

    sput-object v0, Lcom/google/android/gms/cast/c/ab;->u:[I

    return-void
.end method

.method public constructor <init>([Ljava/lang/String;Ljava/net/NetworkInterface;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput v1, p0, Lcom/google/android/gms/cast/c/ab;->h:I

    .line 53
    const/high16 v0, 0x10000

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/gms/cast/c/ab;->l:[B

    .line 59
    iput v1, p0, Lcom/google/android/gms/cast/c/ab;->s:I

    .line 60
    iput-boolean v1, p0, Lcom/google/android/gms/cast/c/ab;->t:Z

    .line 70
    iput-object p1, p0, Lcom/google/android/gms/cast/c/ab;->i:[Ljava/lang/String;

    .line 71
    iput-object p2, p0, Lcom/google/android/gms/cast/c/ab;->p:Ljava/net/NetworkInterface;

    .line 72
    new-instance v0, Lcom/google/android/gms/cast/c/ar;

    invoke-direct {v0}, Lcom/google/android/gms/cast/c/ar;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/ab;->q:Lcom/google/android/gms/cast/c/ar;

    .line 73
    new-instance v0, Lcom/google/android/gms/cast/c/as;

    sget-object v1, Lcom/google/android/gms/cast/c/ae;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/c/as;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/ab;->r:Lcom/google/android/gms/cast/c/as;

    .line 75
    :try_start_0
    invoke-virtual {p2}, Ljava/net/NetworkInterface;->getMTU()I
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 82
    :goto_0
    add-int/lit8 v0, v0, -0x14

    add-int/lit8 v0, v0, -0x8

    iput v0, p0, Lcom/google/android/gms/cast/c/ab;->s:I

    .line 83
    return-void

    .line 79
    :catch_0
    move-exception v0

    const/16 v0, 0x5dc

    goto :goto_0
.end method

.method private a([Ljava/lang/String;[IZZ)Ljava/net/DatagramPacket;
    .locals 11

    .prologue
    .line 205
    new-instance v3, Lcom/google/android/gms/cast/c/an;

    iget v0, p0, Lcom/google/android/gms/cast/c/ab;->s:I

    invoke-direct {v3, v0}, Lcom/google/android/gms/cast/c/an;-><init>(I)V

    .line 207
    iget-object v1, p0, Lcom/google/android/gms/cast/c/ab;->q:Lcom/google/android/gms/cast/c/ar;

    monitor-enter v1

    .line 208
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->q:Lcom/google/android/gms/cast/c/ar;

    sget v2, Lcom/google/android/gms/cast/c/ab;->f:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/c/ar;->a(I)V

    .line 209
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 210
    sget-object v0, Lcom/google/android/gms/cast/c/ab;->g:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "number of cached responses: %d\n"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/cast/c/ab;->q:Lcom/google/android/gms/cast/c/ar;

    invoke-virtual {v5}, Lcom/google/android/gms/cast/c/ar;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 213
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 214
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 215
    array-length v0, p2

    invoke-virtual {v3, v0}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 216
    iget v4, v3, Lcom/google/android/gms/cast/c/an;->b:I

    .line 217
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 218
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 219
    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 222
    const/4 v0, 0x0

    :goto_0
    array-length v1, p2

    if-ge v0, v1, :cond_1

    .line 223
    invoke-virtual {v3, p1}, Lcom/google/android/gms/cast/c/an;->a([Ljava/lang/String;)V

    .line 224
    aget v1, p2, v0

    invoke-virtual {v3, v1}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 225
    if-eqz p3, :cond_0

    const v1, 0x8000

    :goto_1
    or-int/lit8 v1, v1, 0x1

    invoke-virtual {v3, v1}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 222
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 225
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 229
    :cond_1
    const/4 v0, 0x0

    .line 231
    if-eqz p4, :cond_7

    .line 232
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    .line 235
    iget-object v5, p0, Lcom/google/android/gms/cast/c/ab;->q:Lcom/google/android/gms/cast/c/ar;

    monitor-enter v5

    .line 236
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/cast/c/ab;->q:Lcom/google/android/gms/cast/c/ar;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/c/ar;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v0

    .line 237
    :goto_2
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 238
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/aq;

    .line 239
    iget v9, v3, Lcom/google/android/gms/cast/c/an;->b:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 241
    const/4 v1, 0x0

    :try_start_2
    iget-object v10, v0, Lcom/google/android/gms/cast/c/aq;->a:Lcom/google/android/gms/cast/c/ao;

    if-eqz v10, :cond_2

    iget-object v1, v0, Lcom/google/android/gms/cast/c/aq;->a:Lcom/google/android/gms/cast/c/ao;

    invoke-virtual {v1, v3, v6, v7}, Lcom/google/android/gms/cast/c/ao;->a(Lcom/google/android/gms/cast/c/an;J)V

    const/4 v1, 0x1

    :cond_2
    iget-object v10, v0, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    if-eqz v10, :cond_3

    iget-object v10, v0, Lcom/google/android/gms/cast/c/aq;->b:Lcom/google/android/gms/cast/c/at;

    invoke-virtual {v10, v3, v6, v7}, Lcom/google/android/gms/cast/c/at;->a(Lcom/google/android/gms/cast/c/an;J)V

    add-int/lit8 v1, v1, 0x1

    :cond_3
    iget-object v10, v0, Lcom/google/android/gms/cast/c/aq;->c:Lcom/google/android/gms/cast/c/au;

    if-eqz v10, :cond_4

    iget-object v10, v0, Lcom/google/android/gms/cast/c/aq;->c:Lcom/google/android/gms/cast/c/au;

    invoke-virtual {v10, v3, v6, v7}, Lcom/google/android/gms/cast/c/au;->a(Lcom/google/android/gms/cast/c/an;J)V

    add-int/lit8 v1, v1, 0x1

    :cond_4
    iget-object v10, v0, Lcom/google/android/gms/cast/c/aq;->d:Lcom/google/android/gms/cast/c/ak;

    if-eqz v10, :cond_5

    iget-object v10, v0, Lcom/google/android/gms/cast/c/aq;->d:Lcom/google/android/gms/cast/c/ak;

    invoke-virtual {v10, v3, v6, v7}, Lcom/google/android/gms/cast/c/ak;->a(Lcom/google/android/gms/cast/c/an;J)V

    add-int/lit8 v1, v1, 0x1

    :cond_5
    iget-object v10, v0, Lcom/google/android/gms/cast/c/aq;->e:Lcom/google/android/gms/cast/c/al;

    if-eqz v10, :cond_8

    iget-object v0, v0, Lcom/google/android/gms/cast/c/aq;->e:Lcom/google/android/gms/cast/c/al;

    invoke-virtual {v0, v3, v6, v7}, Lcom/google/android/gms/cast/c/al;->a(Lcom/google/android/gms/cast/c/an;J)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    add-int/lit8 v0, v1, 0x1

    :goto_3
    add-int/2addr v0, v2

    move v2, v0

    .line 251
    goto :goto_2

    .line 243
    :catch_0
    move-exception v0

    :try_start_3
    sget-object v0, Lcom/google/android/gms/cast/c/ab;->g:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "PACKET OVERFLOW; truncating to %d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v0, v1, v6}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 248
    invoke-virtual {v3, v9}, Lcom/google/android/gms/cast/c/an;->a(I)V

    .line 249
    const/4 v0, -0x1

    iput v0, v3, Lcom/google/android/gms/cast/c/an;->c:I

    .line 250
    :cond_6
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 255
    if-lez v2, :cond_7

    .line 257
    invoke-virtual {v3, v4}, Lcom/google/android/gms/cast/c/an;->a(I)V

    .line 258
    invoke-virtual {v3, v2}, Lcom/google/android/gms/cast/c/an;->b(I)V

    .line 259
    invoke-virtual {v3}, Lcom/google/android/gms/cast/c/an;->a()V

    .line 263
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->k:Ljava/net/InetSocketAddress;

    new-instance v1, Ljava/net/DatagramPacket;

    iget-object v2, v3, Lcom/google/android/gms/cast/c/an;->a:[B

    iget v3, v3, Lcom/google/android/gms/cast/c/an;->b:I

    invoke-direct {v1, v2, v3, v0}, Ljava/net/DatagramPacket;-><init>([BILjava/net/SocketAddress;)V

    return-object v1

    .line 250
    :catchall_1
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_8
    move v0, v1

    goto :goto_3
.end method

.method static synthetic a(Lcom/google/android/gms/cast/c/ab;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 30
    new-instance v1, Ljava/net/DatagramPacket;

    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->l:[B

    iget-object v2, p0, Lcom/google/android/gms/cast/c/ab;->l:[B

    array-length v2, v2

    invoke-direct {v1, v0, v2}, Ljava/net/DatagramPacket;-><init>([BI)V

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/cast/c/ab;->o:Z

    if-nez v0, :cond_3

    :try_start_0
    sget-object v0, Lcom/google/android/gms/cast/c/ab;->g:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "waiting for an mDNS packet"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->l:[B

    array-length v0, v0

    invoke-virtual {v1, v0}, Ljava/net/DatagramPacket;->setLength(I)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->j:Ljava/net/MulticastSocket;

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->receive(Ljava/net/DatagramPacket;)V

    sget-object v0, Lcom/google/android/gms/cast/c/ab;->g:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "received a packet of length %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v1}, Ljava/net/DatagramPacket;->getLength()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget v0, p0, Lcom/google/android/gms/cast/c/ab;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/cast/c/ab;->h:I

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/cast/c/ab;->r:Lcom/google/android/gms/cast/c/as;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/gms/cast/c/as;->a(Ljava/net/DatagramPacket;Ljava/util/List;)I

    move-result v2

    if-nez v2, :cond_2

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/aq;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/c/aq;->f()[Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/cast/c/ab;->g:Lcom/google/android/gms/cast/internal/k;

    const-string v5, "mDNS response: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v3}, Lcom/google/android/gms/cast/c/ap;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/ab;->a(Lcom/google/android/gms/cast/c/aq;)V

    iget-object v3, p0, Lcom/google/android/gms/cast/c/ab;->q:Lcom/google/android/gms/cast/c/ar;

    monitor-enter v3
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/cast/c/ab;->q:Lcom/google/android/gms/cast/c/ar;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/c/aq;->f()[Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Lcom/google/android/gms/cast/c/ar;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3

    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :catch_0
    move-exception v0

    iget-boolean v2, p0, Lcom/google/android/gms/cast/c/ab;->o:Z

    if-nez v2, :cond_0

    sget-object v2, Lcom/google/android/gms/cast/c/ab;->g:Lcom/google/android/gms/cast/internal/k;

    const-string v3, "while receiving packet"

    new-array v4, v8, [Ljava/lang/Object;

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    :cond_2
    if-eq v2, v9, :cond_0

    :try_start_3
    sget-object v0, Lcom/google/android/gms/cast/c/ab;->g:Lcom/google/android/gms/cast/internal/k;

    const-string v3, "Error while decoding packet (%d): %d"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, p0, Lcom/google/android/gms/cast/c/ab;->h:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto/16 :goto_0

    :cond_3
    return-void
.end method

.method private static a(Ljava/lang/Thread;)V
    .locals 1

    .prologue
    .line 165
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Thread;->interrupt()V

    .line 166
    invoke-virtual {p0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 167
    return-void

    .line 168
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/cast/c/ab;)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 30
    iget-boolean v0, p0, Lcom/google/android/gms/cast/c/ab;->t:Z

    if-eqz v0, :cond_0

    sget v0, Lcom/google/android/gms/cast/c/ab;->b:I

    :goto_0
    sget v3, Lcom/google/android/gms/cast/c/ab;->c:I

    move v4, v0

    move v5, v2

    move v0, v1

    :goto_1
    iget-boolean v6, p0, Lcom/google/android/gms/cast/c/ab;->o:Z

    if-nez v6, :cond_2

    :try_start_0
    iget-object v6, p0, Lcom/google/android/gms/cast/c/ab;->i:[Ljava/lang/String;

    sget-object v7, Lcom/google/android/gms/cast/c/ab;->u:[I

    const/4 v8, 0x1

    invoke-direct {p0, v6, v7, v5, v8}, Lcom/google/android/gms/cast/c/ab;->a([Ljava/lang/String;[IZZ)Ljava/net/DatagramPacket;

    move-result-object v6

    sget-object v7, Lcom/google/android/gms/cast/c/ab;->g:Lcom/google/android/gms/cast/internal/k;

    const-string v8, "sending mDNS discovery query packet (len %d)"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v6}, Ljava/net/DatagramPacket;->getLength()I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-virtual {v7, v8, v9}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v7, p0, Lcom/google/android/gms/cast/c/ab;->j:Ljava/net/MulticastSocket;

    invoke-virtual {v7, v6}, Ljava/net/MulticastSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3

    move v5, v1

    :goto_2
    add-int/lit8 v0, v0, 0x1

    if-ne v0, v3, :cond_1

    if-eqz v2, :cond_4

    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/gms/cast/c/ab;->t:Z

    if-eqz v0, :cond_4

    sget v3, Lcom/google/android/gms/cast/c/ab;->d:I
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    move v0, v3

    :goto_3
    int-to-long v2, v4

    :try_start_2
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V

    sget v2, Lcom/google/android/gms/cast/c/ab;->b:I

    if-ge v4, v2, :cond_3

    mul-int/lit8 v2, v4, 0x2

    sget v3, Lcom/google/android/gms/cast/c/ab;->b:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v2

    move v3, v0

    move v4, v2

    move v0, v1

    move v2, v1

    goto :goto_1

    :cond_0
    sget v0, Lcom/google/android/gms/cast/c/ab;->a:I

    goto :goto_0

    :cond_1
    :try_start_3
    sget v6, Lcom/google/android/gms/cast/c/ab;->e:I

    int-to-long v6, v6

    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    :catch_0
    move-exception v6

    goto :goto_1

    :cond_2
    return-void

    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_1

    :catch_2
    move-exception v2

    move v2, v1

    move v3, v0

    move v0, v1

    goto :goto_1

    :catch_3
    move-exception v6

    goto :goto_2

    :cond_3
    move v2, v1

    move v3, v0

    move v0, v1

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_3
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 132
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->j:Ljava/net/MulticastSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 156
    :goto_0
    monitor-exit p0

    return-void

    .line 136
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/cast/c/ab;->o:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 139
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->j:Ljava/net/MulticastSocket;

    iget-object v1, p0, Lcom/google/android/gms/cast/c/ab;->k:Ljava/net/InetSocketAddress;

    iget-object v2, p0, Lcom/google/android/gms/cast/c/ab;->p:Ljava/net/NetworkInterface;

    invoke-virtual {v0, v1, v2}, Ljava/net/MulticastSocket;->leaveGroup(Ljava/net/SocketAddress;Ljava/net/NetworkInterface;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 143
    :goto_1
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->j:Ljava/net/MulticastSocket;

    invoke-virtual {v0}, Ljava/net/MulticastSocket;->close()V

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->m:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->m:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/ab;->a(Ljava/lang/Thread;)V

    .line 147
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/c/ab;->m:Ljava/lang/Thread;

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->n:Ljava/lang/Thread;

    if-eqz v0, :cond_2

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->n:Ljava/lang/Thread;

    invoke-static {v0}, Lcom/google/android/gms/cast/c/ab;->a(Ljava/lang/Thread;)V

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/c/ab;->n:Ljava/lang/Thread;

    .line 155
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/c/ab;->j:Ljava/net/MulticastSocket;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method protected abstract a(Lcom/google/android/gms/cast/c/aq;)V
.end method

.method public final declared-synchronized a(Z)V
    .locals 3

    .prologue
    .line 92
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->j:Ljava/net/MulticastSocket;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 126
    :goto_0
    monitor-exit p0

    return-void

    .line 96
    :cond_0
    :try_start_1
    iput-boolean p1, p0, Lcom/google/android/gms/cast/c/ab;->t:Z

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/cast/c/ab;->o:Z

    .line 101
    new-instance v0, Ljava/net/MulticastSocket;

    const/16 v1, 0x14e9

    invoke-direct {v0, v1}, Ljava/net/MulticastSocket;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/ab;->j:Ljava/net/MulticastSocket;

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->j:Ljava/net/MulticastSocket;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->setTimeToLive(I)V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->p:Ljava/net/NetworkInterface;

    if-eqz v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->j:Ljava/net/MulticastSocket;

    iget-object v1, p0, Lcom/google/android/gms/cast/c/ab;->p:Ljava/net/NetworkInterface;

    invoke-virtual {v0, v1}, Ljava/net/MulticastSocket;->setNetworkInterface(Ljava/net/NetworkInterface;)V

    .line 107
    :cond_1
    new-instance v0, Ljava/net/InetSocketAddress;

    sget-object v1, Lcom/google/android/gms/cast/c/ae;->a:Ljava/net/InetAddress;

    const/16 v2, 0x14e9

    invoke-direct {v0, v1, v2}, Ljava/net/InetSocketAddress;-><init>(Ljava/net/InetAddress;I)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/ab;->k:Ljava/net/InetSocketAddress;

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->j:Ljava/net/MulticastSocket;

    iget-object v1, p0, Lcom/google/android/gms/cast/c/ab;->k:Ljava/net/InetSocketAddress;

    iget-object v2, p0, Lcom/google/android/gms/cast/c/ab;->p:Ljava/net/NetworkInterface;

    invoke-virtual {v0, v1, v2}, Ljava/net/MulticastSocket;->joinGroup(Ljava/net/SocketAddress;Ljava/net/NetworkInterface;)V

    .line 111
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/gms/cast/c/ac;

    invoke-direct {v1, p0}, Lcom/google/android/gms/cast/c/ac;-><init>(Lcom/google/android/gms/cast/c/ab;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/ab;->m:Ljava/lang/Thread;

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->m:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 119
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/gms/cast/c/ad;

    invoke-direct {v1, p0}, Lcom/google/android/gms/cast/c/ad;-><init>(Lcom/google/android/gms/cast/c/ab;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/ab;->n:Ljava/lang/Thread;

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->n:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 92
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a([Ljava/lang/String;[I)V
    .locals 19

    .prologue
    .line 179
    const/4 v3, 0x0

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/cast/c/ab;->a([Ljava/lang/String;[IZZ)Ljava/net/DatagramPacket;

    move-result-object v10

    .line 180
    sget-object v3, Lcom/google/android/gms/cast/c/ab;->g:Lcom/google/android/gms/cast/internal/k;

    const-string v4, "sending mDNS resolve query packet %s (len %d)"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/cast/c/ap;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-virtual {v10}, Ljava/net/DatagramPacket;->getLength()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 182
    sget-object v3, Lcom/google/android/gms/cast/c/ab;->g:Lcom/google/android/gms/cast/internal/k;

    invoke-virtual {v3}, Lcom/google/android/gms/cast/internal/k;->c()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 183
    sget-object v11, Lcom/google/android/gms/cast/c/ab;->g:Lcom/google/android/gms/cast/internal/k;

    const-string v12, "resolve packet:\n%s"

    const/4 v3, 0x1

    new-array v13, v3, [Ljava/lang/Object;

    const/4 v14, 0x0

    invoke-virtual {v10}, Ljava/net/DatagramPacket;->getData()[B

    move-result-object v15

    invoke-virtual {v10}, Ljava/net/DatagramPacket;->getLength()I

    move-result v8

    if-eqz v15, :cond_0

    array-length v3, v15

    if-eqz v3, :cond_0

    if-lez v8, :cond_0

    add-int/lit8 v3, v8, 0x0

    array-length v4, v15

    if-le v3, v4, :cond_2

    :cond_0
    const/4 v3, 0x0

    :goto_0
    aput-object v3, v13, v14

    invoke-virtual {v11, v12, v13}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 187
    :cond_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/cast/c/ab;->j:Ljava/net/MulticastSocket;

    invoke-virtual {v3, v10}, Ljava/net/MulticastSocket;->send(Ljava/net/DatagramPacket;)V

    .line 188
    return-void

    .line 183
    :cond_2
    add-int/lit8 v3, v8, 0x10

    add-int/lit8 v3, v3, -0x1

    div-int/lit8 v3, v3, 0x10

    mul-int/lit8 v3, v3, 0x4b

    new-instance v16, Ljava/lang/StringBuilder;

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v5, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    move v6, v5

    move v5, v8

    :goto_1
    if-lez v5, :cond_c

    if-nez v6, :cond_6

    const/high16 v3, 0x10000

    if-ge v8, v3, :cond_5

    const-string v3, "%04X:"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v7, v9

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v3, v4

    :cond_3
    :goto_2
    const-string v7, " %02X"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/16 v17, 0x0

    aget-byte v18, v15, v4

    move/from16 v0, v18

    and-int/lit16 v0, v0, 0xff

    move/from16 v18, v0

    invoke-static/range {v18 .. v18}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v9, v17

    invoke-static {v7, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v9, v5, -0x1

    add-int/lit8 v6, v6, 0x1

    const/16 v5, 0x10

    if-eq v6, v5, :cond_4

    if-nez v9, :cond_a

    :cond_4
    rsub-int/lit8 v7, v6, 0x10

    if-lez v7, :cond_7

    const/4 v5, 0x0

    :goto_3
    if-ge v5, v7, :cond_7

    const-string v17, "   "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v5, v5, 0x1

    goto :goto_3

    :cond_5
    const-string v3, "%08X:"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    aput-object v17, v7, v9

    invoke-static {v3, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v3, v4

    goto :goto_2

    :cond_6
    const/16 v7, 0x8

    if-ne v6, v7, :cond_3

    const-string v7, " -"

    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_7
    const/16 v5, 0x8

    if-lt v7, v5, :cond_8

    const-string v5, "  "

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_8
    const-string v5, "  "

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/4 v5, 0x0

    move v7, v5

    :goto_4
    if-ge v7, v6, :cond_a

    add-int v5, v3, v7

    aget-byte v5, v15, v5

    int-to-char v5, v5

    const/16 v17, 0x20

    move/from16 v0, v17

    if-lt v5, v0, :cond_9

    const/16 v17, 0x7e

    move/from16 v0, v17

    if-gt v5, v0, :cond_9

    :goto_5
    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v5, v7, 0x1

    move v7, v5

    goto :goto_4

    :cond_9
    const/16 v5, 0x2e

    goto :goto_5

    :cond_a
    const/16 v5, 0x10

    if-eq v6, v5, :cond_b

    if-nez v9, :cond_d

    :cond_b
    const/16 v5, 0xa

    move-object/from16 v0, v16

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const/4 v5, 0x0

    :goto_6
    add-int/lit8 v4, v4, 0x1

    move v6, v5

    move v5, v9

    goto/16 :goto_1

    :cond_c
    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_0

    :cond_d
    move v5, v6

    goto :goto_6
.end method

.method public final b()Ljava/net/NetworkInterface;
    .locals 1

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/gms/cast/c/ab;->p:Ljava/net/NetworkInterface;

    return-object v0
.end method
