.class public final Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Lcom/android/volley/x;
.implements Lcom/google/android/gms/wallet/common/ui/bf;
.implements Lcom/google/android/gms/wallet/common/ui/dp;


# static fields
.field static final a:Ljava/util/concurrent/atomic/AtomicInteger;

.field static final b:I

.field private static final q:Landroid/util/SparseBooleanArray;

.field private static final r:Landroid/util/SparseBooleanArray;

.field private static final s:Landroid/util/SparseBooleanArray;

.field private static final t:Landroid/util/SparseBooleanArray;

.field private static final u:Ljava/util/Comparator;


# instance fields
.field private final A:Ljava/util/HashSet;

.field private B:Ljava/util/ArrayList;

.field private C:I

.field private D:Z

.field private E:Ljava/lang/String;

.field private F:Lcom/google/t/a/b;

.field private G:Lcom/google/android/gms/wallet/common/ui/cr;

.field private H:Lcom/google/android/gms/wallet/common/ui/validator/d;

.field private I:Lcom/google/android/gms/wallet/common/ui/validator/o;

.field c:Lcom/google/android/gms/wallet/common/ui/cv;

.field d:Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

.field e:Landroid/view/ViewGroup;

.field f:Landroid/widget/ProgressBar;

.field g:Lcom/google/android/gms/wallet/common/ui/bb;

.field h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field i:[I

.field j:I

.field k:I

.field l:Lorg/json/JSONObject;

.field m:Ljava/lang/String;

.field n:Ljava/util/ArrayList;

.field o:Lcom/android/volley/s;

.field p:Ljava/lang/String;

.field private v:Landroid/view/View;

.field private w:I

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/16 v5, 0x43

    const/16 v4, 0x32

    const/16 v3, 0x31

    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 133
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 139
    new-instance v0, Landroid/util/SparseBooleanArray;

    const/16 v2, 0x8

    invoke-direct {v0, v2}, Landroid/util/SparseBooleanArray;-><init>(I)V

    .line 141
    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x53

    invoke-virtual {v0, v2, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 142
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x52

    invoke-virtual {v0, v2, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 143
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v5, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 144
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x4e

    invoke-virtual {v0, v2, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 145
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v3, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 146
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v4, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 147
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x5a

    invoke-virtual {v0, v2, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 148
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x58

    invoke-virtual {v0, v2, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 154
    new-instance v0, Landroid/util/SparseBooleanArray;

    const/4 v2, 0x3

    invoke-direct {v0, v2}, Landroid/util/SparseBooleanArray;-><init>(I)V

    .line 156
    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->r:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x4f

    invoke-virtual {v0, v2, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 157
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->r:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x50

    invoke-virtual {v0, v2, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 158
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->r:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x4e

    invoke-virtual {v0, v2, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 164
    new-instance v0, Landroid/util/SparseBooleanArray;

    const/4 v2, 0x6

    invoke-direct {v0, v2}, Landroid/util/SparseBooleanArray;-><init>(I)V

    .line 166
    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->s:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v3, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 167
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->s:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v4, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 168
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->s:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x33

    invoke-virtual {v0, v2, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 169
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->s:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, v5, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 170
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->s:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x53

    invoke-virtual {v0, v2, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 171
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->s:Landroid/util/SparseBooleanArray;

    const/16 v2, 0x58

    invoke-virtual {v0, v2, v6}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 177
    new-instance v0, Landroid/util/SparseBooleanArray;

    sget-object v2, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    invoke-direct {v0, v2}, Landroid/util/SparseBooleanArray;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->t:Landroid/util/SparseBooleanArray;

    .line 178
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    .line 179
    sget-object v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v3

    .line 180
    sget-object v4, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->t:Landroid/util/SparseBooleanArray;

    sget-object v5, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q:Landroid/util/SparseBooleanArray;

    invoke-virtual {v5, v3}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v5

    invoke-virtual {v4, v3, v5}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 178
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 182
    :cond_0
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->s:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v2

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_1

    .line 183
    sget-object v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->s:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v3

    .line 184
    sget-object v4, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->t:Landroid/util/SparseBooleanArray;

    invoke-virtual {v4, v3, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 182
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 188
    :cond_1
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/g;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->u:Ljava/util/Comparator;

    .line 206
    sput v6, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b:I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 97
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 225
    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    .line 230
    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->x:Z

    .line 231
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->y:Z

    .line 232
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->z:Z

    .line 233
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->A:Ljava/util/HashSet;

    .line 234
    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->k:I

    .line 236
    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->B:Ljava/util/ArrayList;

    .line 237
    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    .line 239
    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->C:I

    .line 240
    iput-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->D:Z

    .line 242
    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    .line 245
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    .line 2451
    return-void
.end method

.method public static a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;)Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;
    .locals 3

    .prologue
    .line 491
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;-><init>()V

    .line 493
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 494
    const-string v2, "params"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 495
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->setArguments(Landroid/os/Bundle;)V

    .line 497
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)Lcom/google/t/a/b;
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Lcom/google/t/a/b;)Lcom/google/t/a/b;
    .locals 0

    .prologue
    .line 97
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    return-object p1
.end method

.method private static a(Landroid/view/View;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 510
    if-nez p0, :cond_0

    .line 511
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 512
    :cond_0
    instance-of v0, p0, Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 513
    check-cast p0, Landroid/widget/TextView;

    invoke-virtual {p0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    .line 521
    :goto_0
    return-object v0

    .line 514
    :cond_1
    instance-of v0, p0, Landroid/widget/Spinner;

    if-eqz v0, :cond_4

    .line 515
    check-cast p0, Landroid/widget/Spinner;

    invoke-virtual {p0}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v0

    .line 516
    instance-of v1, v0, Lcom/google/android/gms/wallet/common/ui/t;

    if-eqz v1, :cond_2

    .line 517
    check-cast v0, Lcom/google/android/gms/wallet/common/ui/t;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/t;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 518
    :cond_2
    if-eqz v0, :cond_3

    .line 519
    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 521
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 524
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown input type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(C)V
    .locals 7

    .prologue
    .line 1297
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(C)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 1299
    if-eqz v0, :cond_0

    .line 1300
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->Bi:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(C)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 1304
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f()Z

    .line 1308
    :goto_0
    return-void

    .line 1306
    :cond_0
    const-string v0, "AddressEntryFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Field to mark invalid not found: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private a(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 8

    .prologue
    .line 858
    new-instance v0, Lcom/google/android/gms/wallet/common/a/i;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->u()Lcom/android/volley/s;

    move-result-object v2

    new-instance v6, Lcom/google/android/gms/wallet/common/ui/i;

    invoke-direct {v6, p0, p4}, Lcom/google/android/gms/wallet/common/ui/i;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Ljava/util/ArrayList;)V

    new-instance v7, Lcom/google/android/gms/wallet/common/ui/j;

    invoke-direct {v7, p0}, Lcom/google/android/gms/wallet/common/ui/j;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)V

    move v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/wallet/common/a/i;-><init>(Landroid/content/Context;Lcom/android/volley/s;ILjava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 930
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(Z)V

    .line 931
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/a/i;->a()V

    .line 932
    return-void
.end method

.method private a(Landroid/util/SparseArray;)V
    .locals 4

    .prologue
    .line 1201
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 1214
    :cond_0
    return-void

    .line 1205
    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1206
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1207
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    .line 1208
    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {p1, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1210
    if-eqz v0, :cond_2

    .line 1211
    invoke-static {v3, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 1205
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static a(Landroid/view/View;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v6, 0x0

    .line 538
    if-nez p0, :cond_0

    .line 539
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Input must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 540
    :cond_0
    instance-of v0, p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_2

    .line 541
    check-cast p0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Ljava/lang/CharSequence;)V

    .line 572
    :cond_1
    :goto_0
    return-void

    .line 542
    :cond_2
    instance-of v0, p0, Landroid/widget/TextView;

    if-eqz v0, :cond_3

    .line 543
    check-cast p0, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 544
    :cond_3
    instance-of v0, p0, Landroid/widget/Spinner;

    if-eqz v0, :cond_7

    .line 545
    check-cast p0, Landroid/widget/Spinner;

    .line 546
    if-nez p1, :cond_4

    .line 548
    invoke-static {p0, v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/widget/Spinner;I)V

    goto :goto_0

    .line 549
    :cond_4
    invoke-virtual {p0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    instance-of v0, v0, Landroid/widget/ArrayAdapter;

    if-eqz v0, :cond_1

    .line 550
    invoke-virtual {p0}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v0

    check-cast v0, Landroid/widget/ArrayAdapter;

    .line 551
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 553
    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v7

    move v5, v6

    move v3, v6

    :goto_1
    if-ge v5, v7, :cond_9

    .line 554
    invoke-virtual {v0, v5}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    .line 555
    instance-of v1, v2, Lcom/google/android/gms/wallet/common/ui/t;

    if-eqz v1, :cond_5

    move-object v1, v2

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/t;

    invoke-interface {v1}, Lcom/google/android/gms/wallet/common/ui/t;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v4

    .line 561
    :goto_2
    if-eqz v1, :cond_6

    .line 562
    invoke-static {p0, v5}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/widget/Spinner;I)V

    move v0, v1

    .line 567
    :goto_3
    if-nez v0, :cond_1

    .line 568
    invoke-static {p0, v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/widget/Spinner;I)V

    goto :goto_0

    .line 558
    :cond_5
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    move v1, v4

    .line 559
    goto :goto_2

    .line 553
    :cond_6
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move v3, v1

    goto :goto_1

    .line 573
    :cond_7
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown input type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    move v1, v3

    goto :goto_2

    :cond_9
    move v0, v3

    goto :goto_3
.end method

.method private static a(Landroid/widget/Spinner;I)V
    .locals 1

    .prologue
    .line 578
    instance-of v0, p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    if-eqz v0, :cond_0

    .line 579
    check-cast p0, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->a(I)V

    .line 583
    :goto_0
    return-void

    .line 581
    :cond_0
    invoke-virtual {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;I)V
    .locals 3

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/ui/bb;->b(I)Lcom/google/android/gms/wallet/common/ui/bb;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->p:Ljava/lang/String;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "AddressEntryFragment.NetworkErrorDialog."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->p:Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 97
    if-eqz p1, :cond_1

    const/16 v0, 0x82

    invoke-virtual {p1, v0}, Landroid/view/View;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->o()Z

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Landroid/view/View;Lcom/google/t/a/b;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 97
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v7

    move v5, v3

    move v0, v3

    :goto_0
    if-ge v5, v7, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1, v5}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-nez v0, :cond_b

    if-ne v1, p1, :cond_b

    move v4, v2

    :goto_1
    if-eqz v4, :cond_3

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-static {p2, v0}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;C)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_0

    iget-object v9, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    invoke-static {v0, v9}, Lcom/google/android/gms/wallet/common/a/e;->a(CLorg/json/JSONObject;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    invoke-static {v1, v8}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/view/View;Ljava/lang/String;)V

    instance-of v0, v1, Lcom/google/android/gms/wallet/common/ui/dn;

    if-eqz v0, :cond_1

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dn;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    if-nez v8, :cond_3

    :cond_2
    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    move v0, v4

    goto :goto_0

    :cond_4
    if-eqz v0, :cond_6

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->p()V

    const/16 v0, 0x5a

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(C)Landroid/view/View;

    move-result-object v1

    instance-of v0, v1, Lcom/google/android/gms/wallet/common/ui/dn;

    if-eqz v0, :cond_5

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dn;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_5
    :goto_2
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->focusSearch(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    move v0, v2

    :goto_3
    if-nez v0, :cond_6

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->o()Z

    :cond_6
    :goto_4
    return-void

    :cond_7
    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_8
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eq v0, v1, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v4, v1}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v4

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v5, v0}, Landroid/view/ViewGroup;->indexOfChild(Landroid/view/View;)I

    move-result v0

    if-ge v4, v0, :cond_5

    invoke-virtual {v6, v3, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_2

    :cond_9
    move v0, v3

    goto :goto_3

    :cond_a
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    instance-of v1, v0, Landroid/widget/EditText;

    if-eqz v1, :cond_6

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setSelection(I)V

    goto :goto_4

    :cond_b
    move v4, v0

    goto/16 :goto_1
.end method

.method private a([I)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 1362
    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->y:Z

    if-nez v1, :cond_0

    .line 1401
    :goto_0
    return-void

    .line 1366
    :cond_0
    if-nez p1, :cond_1

    .line 1367
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Received null country list"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1370
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/a/e;->a([I)[I

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    .line 1371
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    array-length v1, v1

    .line 1373
    if-nez v1, :cond_2

    .line 1374
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No countries available"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1376
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->A:Ljava/util/HashSet;

    const/16 v3, 0x52

    invoke-static {v3}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    if-ne v1, v0, :cond_4

    :cond_3
    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(Z)V

    .line 1379
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/cv;

    if-eqz v0, :cond_5

    .line 1381
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/cv;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cv;->a([I)V

    .line 1385
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l()Z

    .line 1388
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/cv;

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/k;

    invoke-direct {v1, p0}, Lcom/google/android/gms/wallet/common/ui/k;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cv;->a(Lcom/google/android/gms/wallet/common/ui/cw;)V

    goto :goto_0

    .line 1376
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 1398
    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l()Z

    goto :goto_0
.end method

.method private b(C)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1841
    const/16 v0, 0x4e

    if-ne p1, v0, :cond_0

    .line 1842
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->k:I

    if-eqz v0, :cond_0

    .line 1843
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->k:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1846
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/wallet/common/a/e;->a(Landroid/content/Context;CLorg/json/JSONObject;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Lcom/google/t/a/b;)V
    .locals 4

    .prologue
    .line 1218
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1227
    :cond_0
    return-void

    .line 1222
    :cond_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 1223
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    .line 1224
    invoke-virtual {v3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    .line 1225
    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/view/View;Ljava/lang/String;)V

    .line 1222
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private c(C)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2152
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 2163
    :goto_0
    return-object v0

    .line 2156
    :cond_0
    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v4

    move v3, v0

    :goto_1
    if-ge v3, v4, :cond_2

    .line 2157
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    .line 2158
    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    .line 2159
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    if-ne v0, p1, :cond_1

    move-object v0, v2

    .line 2160
    goto :goto_0

    .line 2156
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 2163
    goto :goto_0
.end method

.method private c(Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v4, 0x0

    .line 936
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

    .line 937
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/cv;

    .line 939
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->dw:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 941
    if-eqz p1, :cond_0

    .line 943
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->hD:I

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

    .line 945
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

    .line 951
    :goto_0
    const/16 v2, 0x52

    invoke-static {v2}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 953
    :goto_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v3, 0x2

    if-le v2, v3, :cond_1

    .line 954
    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->removeViewAt(I)V

    goto :goto_1

    .line 947
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->hB:I

    invoke-virtual {v1, v2, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/RegionCodeSelectorSpinner;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/cv;

    .line 949
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/cv;

    check-cast v1, Landroid/view/View;

    goto :goto_0

    .line 956
    :cond_1
    invoke-virtual {v0, v1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    .line 959
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->D:Z

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e(Z)V

    .line 960
    return-void
.end method

.method private d(Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1241
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->isHidden()Z

    move-result v0

    if-eqz v0, :cond_1

    move v2, v1

    .line 1272
    :cond_0
    :goto_0
    return v2

    .line 1243
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->D:Z

    if-eqz v0, :cond_0

    .line 1245
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->r()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1247
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 1249
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    if-eqz v0, :cond_0

    .line 1254
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    move v4, v2

    move v3, v1

    :goto_1
    if-ge v4, v5, :cond_5

    .line 1255
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 1256
    instance-of v6, v0, Lcom/google/android/gms/wallet/common/ui/dn;

    if-eqz v6, :cond_4

    .line 1257
    if-eqz p1, :cond_3

    .line 1258
    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dn;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v3, :cond_2

    move v0, v1

    .line 1254
    :goto_2
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    goto :goto_1

    :cond_2
    move v0, v2

    .line 1258
    goto :goto_2

    .line 1259
    :cond_3
    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dn;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/dn;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_4
    move v0, v3

    goto :goto_2

    .line 1264
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v4, "params"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    .line 1265
    iget-boolean v0, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->j:Z

    if-eqz v0, :cond_6

    .line 1266
    if-eqz p1, :cond_8

    .line 1267
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f()Z

    move-result v0

    if-eqz v0, :cond_7

    if-eqz v3, :cond_7

    :goto_3
    move v3, v1

    :cond_6
    move v2, v3

    .line 1272
    goto :goto_0

    :cond_7
    move v1, v2

    .line 1267
    goto :goto_3

    .line 1268
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v0

    if-nez v0, :cond_6

    goto :goto_0
.end method

.method private e(Z)V
    .locals 3

    .prologue
    .line 2092
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    if-nez v0, :cond_1

    .line 2103
    :cond_0
    :goto_0
    return-void

    .line 2096
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/cv;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/cv;

    invoke-interface {v0, p1}, Lcom/google/android/gms/wallet/common/ui/cv;->setEnabled(Z)V

    .line 2097
    :cond_2
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    :goto_1
    if-ge v0, v1, :cond_3

    .line 2098
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 2097
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2100
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_0

    .line 2101
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    goto :goto_0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 796
    new-instance v0, Lcom/google/android/gms/wallet/common/a/a;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    const-string v2, "data"

    new-instance v3, Lcom/google/android/gms/wallet/common/ui/h;

    invoke-direct {v3, p0}, Lcom/google/android/gms/wallet/common/ui/h;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)V

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/google/android/gms/wallet/common/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 812
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(Z)V

    .line 813
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->u()Lcom/android/volley/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 814
    return-void
.end method

.method private k()Landroid/util/SparseArray;
    .locals 5

    .prologue
    .line 1179
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1180
    const/4 v0, 0x0

    .line 1197
    :goto_0
    return-object v0

    .line 1183
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    .line 1184
    new-instance v1, Landroid/util/SparseArray;

    invoke-direct {v1, v3}, Landroid/util/SparseArray;-><init>(I)V

    .line 1185
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_1

    .line 1186
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 1187
    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Character;

    .line 1188
    invoke-static {v4}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/view/View;)Ljava/lang/String;

    move-result-object v4

    .line 1189
    invoke-virtual {v0}, Ljava/lang/Character;->charValue()C

    move-result v0

    invoke-virtual {v1, v0, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1185
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1193
    :cond_1
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    if-eqz v0, :cond_2

    .line 1194
    const/16 v0, 0x52

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_2
    move-object v0, v1

    .line 1197
    goto :goto_0
.end method

.method private l()Z
    .locals 5

    .prologue
    const/16 v4, 0x2b3

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1411
    iget-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->y:Z

    if-eqz v2, :cond_6

    .line 1413
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    array-length v2, v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 1440
    :goto_0
    return v0

    .line 1417
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    iget v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1418
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m()V

    goto :goto_0

    .line 1419
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    iget v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->w:I

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1420
    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->w:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(I)V

    goto :goto_0

    .line 1422
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/util/ag;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->b(Ljava/lang/String;)I

    move-result v2

    .line 1424
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    invoke-static {v3, v2}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1425
    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(I)V

    goto :goto_0

    .line 1426
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    invoke-static {v2, v4}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 1427
    invoke-virtual {p0, v4}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(I)V

    goto :goto_0

    .line 1429
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    aget v1, v2, v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(I)V

    goto :goto_0

    .line 1433
    :cond_6
    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    if-eqz v2, :cond_7

    .line 1434
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m()V

    goto :goto_0

    .line 1436
    :cond_7
    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->w:I

    if-eqz v2, :cond_8

    .line 1437
    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->w:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(I)V

    goto :goto_0

    :cond_8
    move v0, v1

    .line 1440
    goto :goto_0
.end method

.method private m()V
    .locals 4

    .prologue
    .line 1445
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    if-nez v0, :cond_0

    .line 1446
    const-string v0, "AddressEntryFragment"

    const-string v1, "Resetting selected country"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1447
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l()Z

    .line 1472
    :goto_0
    return-void

    .line 1451
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->y:Z

    if-eqz v0, :cond_1

    .line 1452
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/cv;

    if-eqz v0, :cond_3

    .line 1453
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c:Lcom/google/android/gms/wallet/common/ui/cv;

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cv;->g_(I)V

    .line 1459
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->I:Lcom/google/android/gms/wallet/common/ui/validator/o;

    if-eqz v0, :cond_2

    .line 1460
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->I:Lcom/google/android/gms/wallet/common/ui/validator/o;

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/validator/o;->a(I)V

    .line 1463
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/a/e;->b(Lorg/json/JSONObject;)I

    move-result v0

    .line 1465
    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    if-ne v0, v1, :cond_4

    .line 1466
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lorg/json/JSONObject;)V

    goto :goto_0

    .line 1454
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

    if-eqz v0, :cond_1

    .line 1455
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d:Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;->a(IZ)V

    goto :goto_1

    .line 1469
    :cond_4
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->t()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private n()V
    .locals 17

    .prologue
    .line 1679
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 1681
    const/4 v1, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/wallet/common/a/e;->d(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    const-string v2, "lfmt"

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    const-string v2, "fmt"

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_1
    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/e;->a(Ljava/lang/String;)[C

    move-result-object v5

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->x:Z

    if-eqz v1, :cond_5

    move-object/from16 v0, p0

    iget v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/e;->b(I)Z

    move-result v1

    if-eqz v1, :cond_5

    const/4 v1, 0x1

    :goto_0
    array-length v2, v5

    new-array v6, v2, [C

    const/4 v3, 0x0

    array-length v7, v5

    const/4 v2, 0x0

    move v4, v2

    move v2, v3

    :goto_1
    if-ge v4, v7, :cond_6

    aget-char v8, v5, v4

    sget-object v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v8}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-eqz v3, :cond_4

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->z:Z

    if-nez v3, :cond_2

    sget-object v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->r:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v8}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-nez v3, :cond_4

    :cond_2
    if-eqz v1, :cond_3

    sget-object v3, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->s:Landroid/util/SparseBooleanArray;

    invoke-virtual {v3, v8}, Landroid/util/SparseBooleanArray;->get(I)Z

    move-result v3

    if-nez v3, :cond_4

    :cond_3
    add-int/lit8 v3, v2, 0x1

    aput-char v8, v6, v2

    move v2, v3

    :cond_4
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    :cond_5
    const/4 v1, 0x0

    goto :goto_0

    :cond_6
    new-array v7, v2, [C

    const/4 v1, 0x0

    :goto_2
    if-ge v1, v2, :cond_7

    aget-char v3, v6, v1

    aput-char v3, v7, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1682
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    const-string v2, "require"

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 1684
    const/4 v1, 0x0

    move v11, v1

    :goto_3
    array-length v1, v7

    if-ge v11, v1, :cond_1a

    .line 1685
    aget-char v6, v7, v11

    .line 1686
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->A:Ljava/util/HashSet;

    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->hE:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v10, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 1688
    :goto_4
    invoke-static {v6}, Lcom/google/android/gms/wallet/common/a/e;->a(C)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setId(I)V

    .line 1689
    invoke-static {v6}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 1690
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->D:Z

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    .line 1691
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 1692
    const/4 v1, 0x0

    aput-char v1, v7, v11

    .line 1684
    add-int/lit8 v1, v11, 0x1

    move v11, v1

    goto :goto_3

    .line 1686
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    invoke-static {v6, v1}, Lcom/google/android/gms/wallet/common/a/e;->a(CLorg/json/JSONObject;)Z

    move-result v12

    const/16 v1, 0x53

    if-ne v6, v1, :cond_15

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/a/e;->d(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v13

    const/4 v2, 0x0

    if-eqz v13, :cond_9

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    const-string v2, "sub_lnames"

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/a/e;->b(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    :cond_9
    if-nez v2, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    const-string v2, "sub_keys"

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/a/e;->b(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    :cond_a
    if-eqz v2, :cond_b

    array-length v1, v2

    if-nez v1, :cond_c

    :cond_b
    const/4 v1, 0x0

    move-object v2, v1

    :goto_5
    if-eqz v2, :cond_15

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_15

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v3, Lcom/google/android/gms/l;->hu:I

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v10, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;

    invoke-virtual {v1, v12}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->a(Z)V

    new-instance v3, Lcom/google/android/gms/wallet/common/ui/o;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    sget v5, Lcom/google/android/gms/l;->gT:I

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(C)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v3, v4, v5, v2, v9}, Lcom/google/android/gms/wallet/common/ui/o;-><init>(Landroid/content/Context;ILjava/util/List;Ljava/lang/String;)V

    sget v2, Lcom/google/android/gms/l;->gU:I

    invoke-virtual {v3, v2}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    invoke-virtual {v1, v3}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    new-instance v2, Lcom/google/android/gms/wallet/common/ui/l;

    move-object/from16 v0, p0

    invoke-direct {v2, v0}, Lcom/google/android/gms/wallet/common/ui/l;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/AutoAdvanceFormSpinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto/16 :goto_4

    :cond_c
    if-eqz v13, :cond_11

    move-object v1, v2

    :goto_6
    if-eqz v1, :cond_d

    array-length v3, v1

    array-length v4, v2

    if-eq v3, v4, :cond_e

    :cond_d
    move-object v1, v2

    :cond_e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    const-string v4, "sub_zips"

    invoke-static {v3, v4}, Lcom/google/android/gms/wallet/common/a/e;->b(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_f

    array-length v4, v3

    array-length v5, v2

    if-eq v4, v5, :cond_10

    :cond_f
    const/4 v3, 0x0

    :cond_10
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    :goto_7
    array-length v5, v2

    if-ge v4, v5, :cond_13

    new-instance v14, Lcom/google/android/gms/wallet/common/ui/p;

    aget-object v15, v2, v4

    aget-object v16, v1, v4

    if-eqz v3, :cond_12

    aget-object v5, v3, v4

    :goto_8
    move-object/from16 v0, v16

    invoke-direct {v14, v15, v0, v5}, Lcom/google/android/gms/wallet/common/ui/p;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v14}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    :cond_11
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    const-string v3, "sub_names"

    invoke-static {v1, v3}, Lcom/google/android/gms/wallet/common/a/e;->b(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    goto :goto_6

    :cond_12
    const/4 v5, 0x0

    goto :goto_8

    :cond_13
    if-eqz v13, :cond_14

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->u:Ljava/util/Comparator;

    invoke-static {v9, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_14
    move-object v2, v9

    goto/16 :goto_5

    :cond_15
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->n:Ljava/util/ArrayList;

    if-eqz v1, :cond_16

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_17

    :cond_16
    const/4 v1, 0x0

    :goto_9
    if-eqz v1, :cond_18

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/w;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_18

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->hw:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v10, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/ab;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    sget v3, Lcom/google/android/gms/l;->gQ:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->n:Ljava/util/ArrayList;

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/wallet/common/ui/ab;-><init>(Landroid/content/Context;IILjava/lang/String;C[CLjava/lang/String;Ljava/util/List;)V

    invoke-virtual {v10, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setThreshold(I)V

    new-instance v2, Lcom/google/android/gms/wallet/common/ui/m;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v10, v1}, Lcom/google/android/gms/wallet/common/ui/m;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/ab;)V

    invoke-virtual {v10, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :goto_a
    invoke-virtual {v10, v12}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Z)V

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v10, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setHint(Ljava/lang/CharSequence;)V

    invoke-virtual {v10}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setSingleLine()V

    const/4 v1, 0x1

    sparse-switch v6, :sswitch_data_0

    :goto_b
    invoke-virtual {v10, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setInputType(I)V

    move-object v1, v10

    goto/16 :goto_4

    :cond_17
    sparse-switch v6, :sswitch_data_1

    const/4 v1, 0x1

    goto :goto_9

    :sswitch_0
    const/4 v1, 0x0

    goto :goto_9

    :cond_18
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Lcom/google/android/gms/l;->hw:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v10, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    move-object v10, v1

    goto :goto_a

    :sswitch_1
    const/16 v1, 0x2061

    sget v2, Lcom/google/android/gms/p;->zU:I

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/wallet/common/ui/validator/p;

    const-string v4, ".*\\S+\\s+\\S+.*"

    invoke-direct {v3, v2, v4}, Lcom/google/android/gms/wallet/common/ui/validator/p;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {v10, v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    goto :goto_b

    :sswitch_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/e;->d(Lorg/json/JSONObject;)Z

    move-result v1

    if-eqz v1, :cond_19

    const/4 v1, 0x3

    goto :goto_b

    :cond_19
    const/16 v1, 0x1001

    goto :goto_b

    :sswitch_3
    const/16 v1, 0x2071

    goto :goto_b

    :sswitch_4
    const/16 v1, 0x2001

    goto :goto_b

    :sswitch_5
    const/16 v1, 0x2001

    goto :goto_b

    .line 1695
    :cond_1a
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->p()V

    .line 1696
    return-void

    .line 1686
    nop

    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_3
        0x32 -> :sswitch_3
        0x33 -> :sswitch_3
        0x41 -> :sswitch_3
        0x43 -> :sswitch_5
        0x4e -> :sswitch_1
        0x53 -> :sswitch_4
        0x5a -> :sswitch_2
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x32 -> :sswitch_0
        0x33 -> :sswitch_0
        0x58 -> :sswitch_0
    .end sparse-switch
.end method

.method private o()Z
    .locals 2

    .prologue
    .line 1945
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->findFocus()Landroid/view/View;

    move-result-object v0

    .line 1946
    if-eqz v0, :cond_0

    .line 1947
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/common/util/ae;->b(Landroid/content/Context;Landroid/view/View;)Z

    .line 1948
    const/4 v0, 0x1

    .line 1950
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    const/16 v6, 0x5a

    const/4 v2, 0x0

    .line 1974
    invoke-direct {p0, v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(C)Landroid/view/View;

    move-result-object v0

    .line 1978
    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/google/android/gms/wallet/common/ui/dn;

    if-nez v1, :cond_2

    .line 1979
    :cond_0
    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->H:Lcom/google/android/gms/wallet/common/ui/validator/d;

    .line 2032
    :cond_1
    :goto_0
    return-void

    .line 1983
    :cond_2
    check-cast v0, Lcom/google/android/gms/wallet/common/ui/do;

    .line 1984
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->H:Lcom/google/android/gms/wallet/common/ui/validator/d;

    if-eqz v1, :cond_3

    .line 1985
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->H:Lcom/google/android/gms/wallet/common/ui/validator/d;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/do;->c(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 1986
    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->H:Lcom/google/android/gms/wallet/common/ui/validator/d;

    .line 1989
    :cond_3
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/a;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/common/ui/validator/a;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->H:Lcom/google/android/gms/wallet/common/ui/validator/d;

    .line 1990
    sget v1, Lcom/google/android/gms/p;->Bi:I

    new-array v3, v8, [Ljava/lang/Object;

    invoke-direct {p0, v6}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(C)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {p0, v1, v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1992
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/e;->c(Lorg/json/JSONObject;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 1993
    if-eqz v1, :cond_4

    .line 1994
    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->H:Lcom/google/android/gms/wallet/common/ui/validator/d;

    new-instance v5, Lcom/google/android/gms/wallet/common/ui/validator/l;

    invoke-direct {v5, v3, v1}, Lcom/google/android/gms/wallet/common/ui/validator/l;-><init>(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)V

    invoke-virtual {v4, v5}, Lcom/google/android/gms/wallet/common/ui/validator/d;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 1996
    :cond_4
    const/16 v1, 0x53

    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(C)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_6

    instance-of v4, v1, Landroid/widget/Spinner;

    if-eqz v4, :cond_6

    check-cast v1, Landroid/widget/Spinner;

    invoke-virtual {v1}, Landroid/widget/Spinner;->getSelectedItem()Ljava/lang/Object;

    move-result-object v1

    instance-of v4, v1, Lcom/google/android/gms/wallet/common/ui/p;

    if-eqz v4, :cond_6

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/p;

    .line 1997
    :goto_1
    if-eqz v1, :cond_5

    .line 1998
    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/p;->b:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/a/e;->b(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    .line 2000
    if-eqz v1, :cond_5

    .line 2001
    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->H:Lcom/google/android/gms/wallet/common/ui/validator/d;

    new-instance v5, Lcom/google/android/gms/wallet/common/ui/validator/l;

    invoke-direct {v5, v3, v1}, Lcom/google/android/gms/wallet/common/ui/validator/l;-><init>(Ljava/lang/CharSequence;Ljava/util/regex/Pattern;)V

    invoke-virtual {v4, v5}, Lcom/google/android/gms/wallet/common/ui/validator/d;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 2007
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->H:Lcom/google/android/gms/wallet/common/ui/validator/d;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/validator/d;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 2008
    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->H:Lcom/google/android/gms/wallet/common/ui/validator/d;

    goto :goto_0

    :cond_6
    move-object v1, v2

    .line 1996
    goto :goto_1

    .line 2014
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    invoke-static {v6, v1}, Lcom/google/android/gms/wallet/common/a/e;->a(CLorg/json/JSONObject;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 2015
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/k;

    const/4 v2, 0x2

    new-array v2, v2, [Lcom/google/android/gms/wallet/common/ui/validator/r;

    new-instance v4, Lcom/google/android/gms/wallet/common/ui/validator/i;

    invoke-direct {v4}, Lcom/google/android/gms/wallet/common/ui/validator/i;-><init>()V

    aput-object v4, v2, v7

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->H:Lcom/google/android/gms/wallet/common/ui/validator/d;

    aput-object v4, v2, v8

    invoke-direct {v1, v3, v2}, Lcom/google/android/gms/wallet/common/ui/validator/k;-><init>(Ljava/lang/CharSequence;[Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->H:Lcom/google/android/gms/wallet/common/ui/validator/d;

    .line 2019
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->H:Lcom/google/android/gms/wallet/common/ui/validator/d;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/do;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 2022
    instance-of v1, v0, Landroid/widget/TextView;

    if-eqz v1, :cond_a

    move-object v1, v0

    .line 2023
    check-cast v1, Landroid/widget/TextView;

    .line 2024
    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-virtual {v1}, Landroid/widget/TextView;->getError()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2026
    :cond_9
    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/do;->f()Z

    goto/16 :goto_0

    .line 2030
    :cond_a
    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/do;->f()Z

    goto/16 :goto_0
.end method

.method private q()Ljava/lang/String;
    .locals 2

    .prologue
    .line 2063
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/a/e;->d(Lorg/json/JSONObject;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2064
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    .line 2066
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    const-string v1, "lang"

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private r()Z
    .locals 1

    .prologue
    .line 2122
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->C:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private s()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2144
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->D:Z

    if-eqz v0, :cond_0

    .line 2145
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->r()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e(Z)V

    .line 2147
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->f:Landroid/widget/ProgressBar;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->r()Z

    move-result v2

    if-eqz v2, :cond_2

    :goto_1
    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 2148
    return-void

    :cond_1
    move v0, v1

    .line 2145
    goto :goto_0

    .line 2147
    :cond_2
    const/16 v1, 0x8

    goto :goto_1
.end method

.method private t()Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 2177
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2178
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2179
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->B:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 2180
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->B:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 2182
    :cond_0
    return-object v0
.end method

.method private u()Lcom/android/volley/s;
    .locals 1

    .prologue
    .line 2188
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->o:Lcom/android/volley/s;

    if-nez v0, :cond_0

    .line 2189
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->o:Lcom/android/volley/s;

    .line 2191
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->o:Lcom/android/volley/s;

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_0

    .line 1013
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1015
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->E:Ljava/lang/String;

    goto :goto_0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 1342
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    if-eq p1, v0, :cond_0

    .line 1343
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    .line 1344
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m()V

    .line 1347
    if-eqz p1, :cond_0

    .line 1348
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->G:Lcom/google/android/gms/wallet/common/ui/cr;

    if-eqz v0, :cond_0

    .line 1349
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->G:Lcom/google/android/gms/wallet/common/ui/cr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/wallet/common/ui/cr;->b(I)V

    .line 1353
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 4

    .prologue
    const/4 v1, 0x2

    const/4 v0, 0x1

    .line 2378
    if-eq p2, v0, :cond_1

    if-eq p2, v1, :cond_1

    .line 2380
    const-string v0, "AddressEntryFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown error dialog error code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2400
    :cond_0
    :goto_0
    return-void

    .line 2382
    :cond_1
    if-ne p1, v1, :cond_2

    .line 2383
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 2384
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setResult(I)V

    .line 2385
    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    goto :goto_0

    .line 2386
    :cond_2
    if-ne p1, v0, :cond_0

    .line 2387
    if-ne p2, v0, :cond_3

    .line 2388
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j()V

    goto :goto_0

    .line 2389
    :cond_3
    if-ne p2, v1, :cond_0

    .line 2390
    const/4 v0, 0x0

    .line 2391
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    if-eqz v1, :cond_4

    .line 2392
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/a/e;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2395
    :cond_4
    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->t()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/cr;)V
    .locals 1

    .prologue
    .line 1002
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    if-eqz v0, :cond_0

    .line 1003
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    invoke-interface {p1, v0}, Lcom/google/android/gms/wallet/common/ui/cr;->b(I)V

    .line 1005
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->G:Lcom/google/android/gms/wallet/common/ui/cr;

    .line 1006
    return-void
.end method

.method public final a(Lcom/google/t/a/b;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1110
    if-nez p1, :cond_2

    .line 1113
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    if-nez v0, :cond_1

    .line 1114
    iput-object v5, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    .line 1115
    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    .line 1116
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    .line 1159
    :cond_0
    :goto_0
    return-void

    .line 1120
    :cond_1
    new-instance p1, Lcom/google/t/a/b;

    invoke-direct {p1}, Lcom/google/t/a/b;-><init>()V

    .line 1124
    :cond_2
    iget-object v0, p1, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/wallet/dynamite/common/a/c;->b(Ljava/lang/String;)I

    move-result v3

    .line 1125
    if-eqz v3, :cond_4

    const/16 v0, 0x35a

    if-eq v3, v0, :cond_4

    move v0, v1

    .line 1127
    :goto_1
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    .line 1128
    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    iget-object v4, v4, Lcom/google/t/a/b;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 1129
    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    iget-object v4, v4, Lcom/google/t/a/b;->c:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    .line 1132
    :cond_3
    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    if-nez v4, :cond_5

    .line 1133
    if-eqz v0, :cond_0

    .line 1135
    iput v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    goto :goto_0

    :cond_4
    move v0, v2

    .line 1125
    goto :goto_1

    .line 1141
    :cond_5
    if-eqz v0, :cond_6

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    if-ne v3, v0, :cond_9

    .line 1142
    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->r()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1144
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/gms/wallet/common/a/e;->e(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->t()Ljava/util/ArrayList;

    move-result-object v4

    invoke-direct {p0, v2, v3, v0, v4}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    :goto_2
    if-nez v1, :cond_0

    .line 1148
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(Lcom/google/t/a/b;)V

    .line 1149
    iput-object v5, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    goto :goto_0

    .line 1144
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    const-string v3, "id"

    invoke-static {v0, v3}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    const-string v3, "--"

    invoke-virtual {v0, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    const-string v3, "lang"

    invoke-static {v0, v3}, Lcom/google/android/gms/wallet/common/a/e;->a(Lorg/json/JSONObject;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/gms/wallet/dynamite/common/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->t()Ljava/util/ArrayList;

    move-result-object v3

    invoke-direct {p0, v0, v2, v5, v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(ILjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_2

    :cond_8
    move v1, v2

    goto :goto_2

    .line 1151
    :cond_9
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    if-nez v0, :cond_a

    .line 1153
    iput v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    goto/16 :goto_0

    .line 1156
    :cond_a
    invoke-virtual {p0, v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(I)V

    goto/16 :goto_0
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 97
    check-cast p1, Lorg/json/JSONObject;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(Z)V

    const-string v0, "countries"

    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/common/a/e;->b(Lorg/json/JSONObject;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/a/e;->a([Ljava/lang/String;)[I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a([I)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1165
    if-nez p1, :cond_0

    .line 1166
    const-string p1, ""

    .line 1170
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-nez v0, :cond_1

    .line 1171
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->E:Ljava/lang/String;

    .line 1176
    :goto_0
    return-void

    .line 1173
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Ljava/lang/CharSequence;)V

    .line 1174
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->E:Ljava/lang/String;

    goto :goto_0
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 1506
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/a/e;->b(Lorg/json/JSONObject;)I

    move-result v0

    .line 1507
    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    if-eq v0, v1, :cond_0

    .line 1562
    :goto_0
    return-void

    .line 1514
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    if-nez v0, :cond_b

    .line 1515
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->k()Landroid/util/SparseArray;

    move-result-object v0

    .line 1517
    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_1
    if-ltz v1, :cond_2

    .line 1518
    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    int-to-char v3, v3

    .line 1519
    invoke-direct {p0, v3}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->c(C)Landroid/view/View;

    move-result-object v5

    instance-of v5, v5, Landroid/widget/Spinner;

    if-eqz v5, :cond_1

    .line 1520
    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->remove(I)V

    .line 1517
    :cond_1
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    :cond_2
    move-object v1, v0

    .line 1528
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->findFocus()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_7

    const/4 v0, 0x1

    move v3, v0

    .line 1530
    :goto_3
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    .line 1532
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->n:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->B:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->B:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->n:Ljava/util/ArrayList;

    new-instance v5, Lcom/google/android/gms/wallet/common/a/t;

    iget-object v6, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->B:Ljava/util/ArrayList;

    invoke-direct {v5, v6}, Lcom/google/android/gms/wallet/common/a/t;-><init>(Ljava/util/ArrayList;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/gms/wallet/common/a/m;

    if-eqz v0, :cond_8

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/a/m;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/a/m;->a()Lcom/google/android/gms/wallet/common/a/k;

    move-result-object v0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_4
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/a/r;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->n:Ljava/util/ArrayList;

    new-instance v5, Lcom/google/android/gms/wallet/common/a/r;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/gms/wallet/common/a/r;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1535
    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->n()V

    .line 1537
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Landroid/util/SparseArray;)V

    .line 1540
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    if-eqz v0, :cond_5

    .line 1541
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b(Lcom/google/t/a/b;)V

    .line 1542
    iput-object v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    .line 1545
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->gl:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 1547
    if-eqz v1, :cond_a

    .line 1549
    invoke-virtual {v1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 1550
    instance-of v2, v0, Landroid/view/ViewGroup;

    if-eqz v2, :cond_9

    .line 1551
    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 1561
    :cond_6
    :goto_5
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i()V

    goto/16 :goto_0

    :cond_7
    move v3, v4

    .line 1528
    goto :goto_3

    .line 1532
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->n:Ljava/util/ArrayList;

    new-instance v5, Lcom/google/android/gms/wallet/common/a/k;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/gms/wallet/common/a/k;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 1554
    :cond_9
    invoke-virtual {v1, v4}, Landroid/view/View;->setFocusable(Z)V

    goto :goto_5

    .line 1556
    :cond_a
    if-eqz v3, :cond_6

    .line 1558
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    const/16 v1, 0x82

    invoke-virtual {v0, v1}, Landroid/view/View;->requestFocus(I)Z

    goto :goto_5

    :cond_b
    move-object v1, v2

    goto/16 :goto_2
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 2080
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->D:Z

    if-eq v0, p1, :cond_0

    .line 2081
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->D:Z

    .line 2084
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->r()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2085
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e(Z)V

    .line 2088
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v0, :cond_0

    .line 1020
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1021
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->Bv:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 1024
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f()Z

    .line 1027
    :cond_0
    return-void
.end method

.method final b(Z)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 2127
    const/4 v2, 0x0

    iget v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->C:I

    if-eqz p1, :cond_3

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->C:I

    .line 2131
    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->C:I

    if-eq v0, v1, :cond_1

    :cond_0
    if-nez p1, :cond_2

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->C:I

    if-nez v0, :cond_2

    .line 2133
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->s()V

    .line 2135
    :cond_2
    return-void

    .line 2127
    :cond_3
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 1030
    const/16 v0, 0x43

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(C)V

    .line 1031
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 1034
    const/16 v0, 0x5a

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(C)V

    .line 1035
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 1236
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d(Z)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 1231
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->d(Z)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1277
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v5

    move v4, v3

    :goto_0
    if-ge v4, v5, :cond_1

    .line 1278
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    .line 1279
    instance-of v0, v1, Lcom/google/android/gms/wallet/common/ui/dn;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 1280
    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dn;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/dn;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1281
    invoke-virtual {v1}, Landroid/view/View;->clearFocus()V

    .line 1282
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    move v0, v2

    .line 1293
    :goto_1
    return v0

    .line 1277
    :cond_0
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 1287
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    .line 1288
    iget-boolean v0, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->j:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1289
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->clearFocus()V

    .line 1290
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    move v0, v2

    .line 1291
    goto :goto_1

    :cond_2
    move v0, v3

    .line 1293
    goto :goto_1
.end method

.method public final h()Lcom/google/t/a/b;
    .locals 6

    .prologue
    .line 1052
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    if-nez v0, :cond_0

    .line 1053
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    .line 1099
    :goto_0
    return-object v0

    .line 1056
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->k()Landroid/util/SparseArray;

    move-result-object v3

    .line 1058
    new-instance v1, Lcom/google/t/a/b;

    invoke-direct {v1}, Lcom/google/t/a/b;-><init>()V

    .line 1059
    const/4 v0, 0x0

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v4

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_2

    .line 1060
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    int-to-char v5, v0

    .line 1061
    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1063
    sparse-switch v5, :sswitch_data_0

    .line 1059
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1065
    :sswitch_0
    iput-object v0, v1, Lcom/google/t/a/b;->a:Ljava/lang/String;

    goto :goto_2

    .line 1069
    :sswitch_1
    iget-object v5, v1, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    invoke-static {v5, v0}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v1, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    goto :goto_2

    .line 1072
    :sswitch_2
    iput-object v0, v1, Lcom/google/t/a/b;->d:Ljava/lang/String;

    goto :goto_2

    .line 1075
    :sswitch_3
    iput-object v0, v1, Lcom/google/t/a/b;->f:Ljava/lang/String;

    goto :goto_2

    .line 1078
    :sswitch_4
    if-eqz v0, :cond_1

    .line 1079
    invoke-virtual {v0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    .line 1081
    :cond_1
    iput-object v0, v1, Lcom/google/t/a/b;->k:Ljava/lang/String;

    goto :goto_2

    .line 1084
    :sswitch_5
    iput-object v0, v1, Lcom/google/t/a/b;->m:Ljava/lang/String;

    goto :goto_2

    .line 1087
    :sswitch_6
    iput-object v0, v1, Lcom/google/t/a/b;->s:Ljava/lang/String;

    goto :goto_2

    .line 1090
    :sswitch_7
    iput-object v0, v1, Lcom/google/t/a/b;->r:Ljava/lang/String;

    goto :goto_2

    .line 1094
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->q()Ljava/lang/String;

    move-result-object v0

    .line 1096
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1097
    iput-object v0, v1, Lcom/google/t/a/b;->c:Ljava/lang/String;

    :cond_3
    move-object v0, v1

    .line 1099
    goto :goto_0

    .line 1063
    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_1
        0x32 -> :sswitch_1
        0x43 -> :sswitch_3
        0x4e -> :sswitch_6
        0x4f -> :sswitch_7
        0x52 -> :sswitch_0
        0x53 -> :sswitch_2
        0x58 -> :sswitch_5
        0x5a -> :sswitch_4
    .end sparse-switch
.end method

.method protected final i()V
    .locals 1

    .prologue
    .line 2036
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    if-nez v0, :cond_0

    .line 2041
    :goto_0
    return-void

    .line 2040
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->p()V

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 668
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 670
    const/4 v1, 0x0

    .line 672
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 673
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    .line 674
    if-eqz v0, :cond_0

    .line 675
    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->b:[I

    move-object v1, v0

    .line 679
    :cond_0
    if-eqz p1, :cond_6

    .line 680
    const-string v0, "enabled"

    const/4 v2, 0x1

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->D:Z

    .line 681
    const-string v0, "regionCodes"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    .line 682
    const-string v0, "pendingAddress"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 683
    const-string v0, "pendingAddress"

    const-class v2, Lcom/google/t/a/b;

    invoke-static {p1, v0, v2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/t/a/b;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lcom/google/t/a/b;)V

    .line 688
    :cond_1
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    if-nez v0, :cond_2

    .line 689
    const-string v0, "selectedCountry"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    .line 691
    :cond_2
    const-string v0, "countryData"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 693
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    const-string v2, "countryData"

    invoke-virtual {p1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    .line 699
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/a/e;->b(Lorg/json/JSONObject;)I

    move-result v0

    .line 700
    if-eqz v0, :cond_3

    const/16 v2, 0x35a

    if-eq v0, v2, :cond_3

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    if-eq v0, v2, :cond_3

    .line 702
    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    .line 703
    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    .line 704
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Lorg/json/JSONObject;)V

    .line 705
    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 711
    :cond_3
    :goto_0
    const-string v0, "languageCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 712
    const-string v0, "languageCode"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    .line 714
    :cond_4
    const-string v0, "networkErrorDialogTag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 715
    const-string v0, "networkErrorDialogTag"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->p:Ljava/lang/String;

    .line 717
    :cond_5
    const-string v0, "pendingPhoneNumber"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->E:Ljava/lang/String;

    .line 720
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->E:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 721
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->E:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a(Ljava/lang/String;)V

    .line 725
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->D:Z

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e(Z)V

    .line 728
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->y:Z

    if-eqz v0, :cond_b

    .line 729
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    array-length v0, v0

    if-eqz v0, :cond_8

    .line 731
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a([I)V

    .line 746
    :goto_1
    return-void

    .line 708
    :catch_0
    move-exception v0

    const-string v0, "AddressEntryFragment"

    const-string v2, "Could not construct JSONObject from KEY_COUNTRY_DATA json string"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 732
    :cond_8
    if-nez v1, :cond_9

    .line 734
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j()V

    goto :goto_1

    .line 735
    :cond_9
    array-length v0, v1

    if-nez v0, :cond_a

    .line 736
    const-string v0, "AddressEntryFragment"

    const-string v1, "No countries available"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 737
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "List of countries must either be non-empty or null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 740
    :cond_a
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a([I)V

    goto :goto_1

    .line 744
    :cond_b
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l()Z

    goto :goto_1
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 587
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 589
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 590
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "params"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    .line 591
    if-eqz v0, :cond_2

    .line 592
    iget v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->c:I

    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->w:I

    .line 593
    const/16 v2, 0x35a

    iget v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->w:I

    if-ne v2, v3, :cond_0

    .line 594
    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->w:I

    .line 596
    :cond_0
    iget-boolean v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->a:Z

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->x:Z

    .line 597
    iget-boolean v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->d:Z

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->y:Z

    .line 598
    iget-boolean v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->e:Z

    iput-boolean v2, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->z:Z

    .line 599
    iget-object v2, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->f:[C

    .line 600
    if-eqz v2, :cond_1

    .line 601
    array-length v3, v2

    :goto_0
    if-ge v1, v3, :cond_1

    aget-char v4, v2, v1

    .line 602
    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->A:Ljava/util/HashSet;

    invoke-static {v4}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 601
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 605
    :cond_1
    iget v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->g:I

    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->k:I

    .line 606
    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->h:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->B:Ljava/util/ArrayList;

    .line 609
    :cond_2
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 614
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "params"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;

    .line 617
    sget v1, Lcom/google/android/gms/l;->gs:I

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    .line 619
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->ar:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->e:Landroid/view/ViewGroup;

    .line 621
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->pN:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->f:Landroid/widget/ProgressBar;

    .line 622
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->nQ:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 625
    iget-boolean v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->j:Z

    if-eqz v1, :cond_5

    .line 626
    iget-boolean v1, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->k:Z

    .line 628
    new-instance v3, Lcom/google/android/gms/wallet/common/ui/validator/o;

    sget v4, Lcom/google/android/gms/p;->Bv:I

    invoke-virtual {p0, v4}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/wallet/common/ui/validator/o;-><init>(Ljava/lang/CharSequence;)V

    iput-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->I:Lcom/google/android/gms/wallet/common/ui/validator/o;

    .line 630
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->I:Lcom/google/android/gms/wallet/common/ui/validator/o;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 632
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 633
    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment$Params;->i:Ljava/util/Collection;

    .line 634
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 635
    new-instance v4, Lcom/google/android/gms/wallet/common/a/u;

    invoke-direct {v4, v0}, Lcom/google/android/gms/wallet/common/a/u;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 637
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/gms/wallet/common/a/q;

    if-eqz v0, :cond_4

    .line 638
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/a/q;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/a/q;->b()Lcom/google/android/gms/wallet/common/a/n;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 644
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 645
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const/4 v4, 0x1

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setThreshold(I)V

    .line 646
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v4, Lcom/google/android/gms/wallet/common/ui/ct;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v5

    sget v6, Lcom/google/android/gms/l;->hj:I

    invoke-direct {v4, v5, v6, v3}, Lcom/google/android/gms/wallet/common/ui/ct;-><init>(Landroid/content/Context;ILjava/util/List;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 649
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 650
    if-nez p3, :cond_2

    if-nez v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 652
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/u;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v0, v4, v3}, Lcom/google/android/gms/wallet/common/ui/u;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Ljava/util/ArrayList;)V

    new-array v3, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/u;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 654
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    move v0, v1

    .line 659
    :goto_1
    if-nez p3, :cond_3

    if-eqz v0, :cond_3

    .line 660
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    .line 663
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->v:Landroid/view/View;

    return-object v0

    .line 641
    :cond_4
    new-instance v0, Lcom/google/android/gms/wallet/common/a/n;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    invoke-direct {v0, v4}, Lcom/google/android/gms/wallet/common/a/n;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 656
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    move v0, v2

    goto :goto_1
.end method

.method public final onDestroyView()V
    .locals 1

    .prologue
    .line 986
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 988
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->C:I

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->s()V

    .line 989
    return-void
.end method

.method public final onDetach()V
    .locals 2

    .prologue
    .line 993
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 996
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->G:Lcom/google/android/gms/wallet/common/ui/cr;

    .line 997
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->u()Lcom/android/volley/s;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/n;

    invoke-direct {v1, p0}, Lcom/google/android/gms/wallet/common/ui/n;-><init>(Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;)V

    invoke-virtual {v0, v1}, Lcom/android/volley/s;->a(Lcom/android/volley/u;)V

    .line 998
    return-void
.end method

.method public final onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    .prologue
    .line 2404
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    if-ne v0, v1, :cond_5

    .line 2405
    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/a/w;

    .line 2407
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2408
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    const/16 v2, 0x82

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->focusSearch(I)Landroid/view/View;

    move-result-object v1

    .line 2409
    if-eqz v1, :cond_0

    .line 2410
    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    .line 2417
    :cond_0
    invoke-virtual {p1}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/wallet/common/ui/ct;

    .line 2419
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/a/w;->c()I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    and-int/lit8 v6, v4, 0x4

    if-eqz v6, :cond_1

    const-string v6, "best"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "|"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    and-int/lit8 v6, v4, 0x2

    if-eqz v6, :cond_2

    const-string v6, "contact"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v6, "|"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_3

    const-string v4, "phone"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v4, "|"

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-lez v4, :cond_4

    sget v6, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->b:I

    sub-int v6, v4, v6

    invoke-virtual {v5, v6, v4}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    :cond_4
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/a/w;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    .line 2422
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    const-string v4, "phone_number_entry"

    const-string v5, "autocomplete_phone_number"

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/ct;->a()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_6

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/ui/ct;->a()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    int-to-long v0, v0

    :goto_0
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v3, v4, v5, v2, v0}, Lcom/google/android/gms/wallet/common/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;Ljava/lang/Long;)V

    .line 2427
    :cond_5
    return-void

    .line 2422
    :cond_6
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final onResume()V
    .locals 2

    .prologue
    .line 750
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 752
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 753
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/bb;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    .line 756
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    if-eqz v0, :cond_1

    .line 757
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->g:Lcom/google/android/gms/wallet/common/ui/bb;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/bb;->a(Lcom/google/android/gms/wallet/common/ui/bf;)V

    .line 759
    :cond_1
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 964
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 966
    const-string v0, "selectedCountry"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->j:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 967
    const-string v0, "enabled"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->D:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 968
    const-string v0, "regionCodes"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->i:[I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 969
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    if-eqz v0, :cond_0

    .line 970
    const-string v0, "pendingAddress"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->F:Lcom/google/t/a/b;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 972
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->E:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 973
    const-string v0, "pendingPhoneNumber"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->E:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 975
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    if-eqz v0, :cond_2

    .line 976
    const-string v0, "countryData"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->l:Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 978
    :cond_2
    const-string v0, "languageCode"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 979
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->p:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 980
    const-string v0, "networkErrorDialogTag"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/AddressEntryFragment;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 982
    :cond_3
    return-void
.end method
