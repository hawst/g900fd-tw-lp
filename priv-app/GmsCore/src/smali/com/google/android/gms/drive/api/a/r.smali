.class public final Lcom/google/android/gms/drive/api/a/r;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/api/f;

.field private final f:Lcom/google/android/gms/drive/internal/DisconnectRequest;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/DisconnectRequest;Lcom/google/android/gms/drive/internal/ca;Lcom/google/android/gms/drive/api/f;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1, p3}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;)V

    .line 29
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/r;->f:Lcom/google/android/gms/drive/internal/DisconnectRequest;

    .line 30
    iput-object p4, p0, Lcom/google/android/gms/drive/api/a/r;->e:Lcom/google/android/gms/drive/api/f;

    .line 31
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/r;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/api/d;->b()Lcom/google/android/gms/drive/c/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/b;->b()V

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/r;->e:Lcom/google/android/gms/drive/api/f;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/api/f;->a(Z)V

    .line 41
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/r;->a:Lcom/google/android/gms/drive/api/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/api/d;->b()Lcom/google/android/gms/drive/c/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/b;->b()V

    .line 46
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/api/a/b;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 47
    return-void
.end method
