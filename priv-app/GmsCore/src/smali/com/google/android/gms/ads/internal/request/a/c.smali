.class final Lcom/google/android/gms/ads/internal/request/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/util/a/e;


# instance fields
.field final synthetic a:Lcom/google/android/gms/ads/internal/request/a/b;


# direct methods
.method constructor <init>(Lcom/google/android/gms/ads/internal/request/a/b;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/request/a/c;->a:Lcom/google/android/gms/ads/internal/request/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 155
    check-cast p1, Lcom/google/android/gms/ads/internal/a/p;

    const-string v0, "/invalidRequest"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/a/c;->a:Lcom/google/android/gms/ads/internal/request/a/b;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/a/b;->b:Lcom/google/android/gms/ads/internal/request/a/k;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/a/k;->g:Lcom/google/android/gms/ads/internal/g/m;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/ads/internal/a/p;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    const-string v0, "/loadAdURL"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/request/a/c;->a:Lcom/google/android/gms/ads/internal/request/a/b;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/a/b;->b:Lcom/google/android/gms/ads/internal/request/a/k;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/request/a/k;->h:Lcom/google/android/gms/ads/internal/g/m;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/ads/internal/a/p;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    :try_start_0
    const-string v0, "AFMA_buildAdURL"

    new-instance v1, Lorg/json/JSONObject;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/request/a/c;->a:Lcom/google/android/gms/ads/internal/request/a/b;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/request/a/b;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/ads/internal/a/p;->a(Ljava/lang/String;Lorg/json/JSONObject;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Error requesting an ad url"

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
