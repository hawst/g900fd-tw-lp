.class public final Lcom/google/android/gms/games/g/ao;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/games/g/an;

.field public b:Lcom/google/android/gms/games/g/s;

.field public c:I

.field public d:Lcom/google/android/gms/games/g/d;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5620
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5621
    iput-object v1, p0, Lcom/google/android/gms/games/g/ao;->a:Lcom/google/android/gms/games/g/an;

    iput-object v1, p0, Lcom/google/android/gms/games/g/ao;->b:Lcom/google/android/gms/games/g/s;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/g/ao;->c:I

    iput-object v1, p0, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/ao;->cachedSize:I

    .line 5622
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5708
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 5709
    iget-object v1, p0, Lcom/google/android/gms/games/g/ao;->a:Lcom/google/android/gms/games/g/an;

    if-eqz v1, :cond_0

    .line 5710
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/g/ao;->a:Lcom/google/android/gms/games/g/an;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5713
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/ao;->b:Lcom/google/android/gms/games/g/s;

    if-eqz v1, :cond_1

    .line 5714
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/g/ao;->b:Lcom/google/android/gms/games/g/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5717
    :cond_1
    iget v1, p0, Lcom/google/android/gms/games/g/ao;->c:I

    if-eqz v1, :cond_2

    .line 5718
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/games/g/ao;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5721
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    if-eqz v1, :cond_3

    .line 5722
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5725
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5635
    if-ne p1, p0, :cond_1

    .line 5672
    :cond_0
    :goto_0
    return v0

    .line 5638
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/ao;

    if-nez v2, :cond_2

    move v0, v1

    .line 5639
    goto :goto_0

    .line 5641
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/ao;

    .line 5642
    iget-object v2, p0, Lcom/google/android/gms/games/g/ao;->a:Lcom/google/android/gms/games/g/an;

    if-nez v2, :cond_3

    .line 5643
    iget-object v2, p1, Lcom/google/android/gms/games/g/ao;->a:Lcom/google/android/gms/games/g/an;

    if-eqz v2, :cond_4

    move v0, v1

    .line 5644
    goto :goto_0

    .line 5647
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/ao;->a:Lcom/google/android/gms/games/g/an;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ao;->a:Lcom/google/android/gms/games/g/an;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/an;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 5648
    goto :goto_0

    .line 5651
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/ao;->b:Lcom/google/android/gms/games/g/s;

    if-nez v2, :cond_5

    .line 5652
    iget-object v2, p1, Lcom/google/android/gms/games/g/ao;->b:Lcom/google/android/gms/games/g/s;

    if-eqz v2, :cond_6

    move v0, v1

    .line 5653
    goto :goto_0

    .line 5656
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/g/ao;->b:Lcom/google/android/gms/games/g/s;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ao;->b:Lcom/google/android/gms/games/g/s;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/s;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 5657
    goto :goto_0

    .line 5660
    :cond_6
    iget v2, p0, Lcom/google/android/gms/games/g/ao;->c:I

    iget v3, p1, Lcom/google/android/gms/games/g/ao;->c:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 5661
    goto :goto_0

    .line 5663
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    if-nez v2, :cond_8

    .line 5664
    iget-object v2, p1, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    if-eqz v2, :cond_0

    move v0, v1

    .line 5665
    goto :goto_0

    .line 5668
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 5669
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5677
    iget-object v0, p0, Lcom/google/android/gms/games/g/ao;->a:Lcom/google/android/gms/games/g/an;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 5680
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/ao;->b:Lcom/google/android/gms/games/g/s;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 5682
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/games/g/ao;->c:I

    add-int/2addr v0, v2

    .line 5683
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 5685
    return v0

    .line 5677
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/ao;->a:Lcom/google/android/gms/games/g/an;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/an;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5680
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/ao;->b:Lcom/google/android/gms/games/g/s;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/s;->hashCode()I

    move-result v0

    goto :goto_1

    .line 5683
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    invoke-virtual {v1}, Lcom/google/android/gms/games/g/d;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5585
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/ao;->a:Lcom/google/android/gms/games/g/an;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/g/an;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/an;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/ao;->a:Lcom/google/android/gms/games/g/an;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/ao;->a:Lcom/google/android/gms/games/g/an;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/ao;->b:Lcom/google/android/gms/games/g/s;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/games/g/s;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/ao;->b:Lcom/google/android/gms/games/g/s;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/ao;->b:Lcom/google/android/gms/games/g/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/games/g/ao;->c:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/games/g/d;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5691
    iget-object v0, p0, Lcom/google/android/gms/games/g/ao;->a:Lcom/google/android/gms/games/g/an;

    if-eqz v0, :cond_0

    .line 5692
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/g/ao;->a:Lcom/google/android/gms/games/g/an;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5694
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/ao;->b:Lcom/google/android/gms/games/g/s;

    if-eqz v0, :cond_1

    .line 5695
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/g/ao;->b:Lcom/google/android/gms/games/g/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5697
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/g/ao;->c:I

    if-eqz v0, :cond_2

    .line 5698
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/games/g/ao;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5700
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    if-eqz v0, :cond_3

    .line 5701
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/games/g/ao;->d:Lcom/google/android/gms/games/g/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5703
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5704
    return-void
.end method
