.class public final Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/gencode/authzen/server/api/bg;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/bi;

.field private static final f:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Z

.field d:Z

.field e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 27
    new-instance v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/bi;

    invoke-direct {v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/bi;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/bi;

    .line 53
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 56
    sput-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->f:Ljava/util/HashMap;

    const-string v1, "applyToAll"

    const-string v2, "applyToAll"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->f:Ljava/util/HashMap;

    const-string v1, "enable"

    const-string v2, "enable"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->f:Ljava/util/HashMap;

    const-string v1, "publicKey"

    const-string v2, "publicKey"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 103
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->b:I

    .line 104
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->a:Ljava/util/Set;

    .line 105
    return-void
.end method

.method constructor <init>(Ljava/util/Set;IZZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 115
    iput-object p1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->a:Ljava/util/Set;

    .line 116
    iput p2, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->b:I

    .line 117
    iput-boolean p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->c:Z

    .line 118
    iput-boolean p4, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->d:Z

    .line 119
    iput-object p5, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->e:Ljava/lang/String;

    .line 120
    return-void
.end method

.method public constructor <init>(Ljava/util/Set;ZZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 128
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 129
    iput-object p1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->a:Ljava/util/Set;

    .line 130
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->b:I

    .line 131
    iput-boolean p2, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->c:Z

    .line 132
    iput-boolean p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->d:Z

    .line 133
    iput-object p4, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->e:Ljava/lang/String;

    .line 134
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->f:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 259
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 260
    packed-switch v0, :pswitch_data_0

    .line 265
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 262
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->e:Ljava/lang/String;

    .line 268
    iget-object v1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 269
    return-void

    .line 260
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 241
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 242
    packed-switch v0, :pswitch_data_0

    .line 250
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a boolean."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 244
    :pswitch_0
    iput-boolean p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->c:Z

    .line 253
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 254
    return-void

    .line 247
    :pswitch_1
    iput-boolean p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->d:Z

    goto :goto_0

    .line 242
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 213
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 221
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 215
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 219
    :goto_0
    return-object v0

    .line 217
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 219
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->e:Ljava/lang/String;

    goto :goto_0

    .line 213
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 23
    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 198
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/bi;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 297
    instance-of v0, p1, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;

    if-nez v0, :cond_0

    move v0, v1

    .line 328
    :goto_0
    return v0

    .line 302
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 303
    goto :goto_0

    .line 306
    :cond_1
    check-cast p1, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;

    .line 307
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 308
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 309
    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 311
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 313
    goto :goto_0

    :cond_3
    move v0, v1

    .line 318
    goto :goto_0

    .line 321
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 323
    goto :goto_0

    :cond_5
    move v0, v2

    .line 328
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 284
    const/4 v0, 0x0

    .line 285
    sget-object v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->f:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 286
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 287
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 288
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 290
    goto :goto_0

    .line 291
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 203
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/bi;

    invoke-static {p0, p1}, Lcom/google/android/gms/auth/gencode/authzen/server/api/bi;->a(Lcom/google/android/gms/auth/gencode/authzen/server/api/ToggleEasyUnlockRequestEntity;Landroid/os/Parcel;)V

    .line 204
    return-void
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x1

    return v0
.end method
