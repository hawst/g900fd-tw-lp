.class public final Lcom/google/android/gms/drive/g/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/ae;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/drive/g/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 39
    sget-object v0, Lcom/google/android/gms/drive/g/j;->a:Lcom/google/android/gms/drive/g/j;

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/drive/g/aj;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/g/i;)V

    .line 40
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/g/i;)V
    .locals 0

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    iput-object p1, p0, Lcom/google/android/gms/drive/g/aj;->a:Landroid/content/Context;

    .line 45
    iput-object p2, p0, Lcom/google/android/gms/drive/g/aj;->b:Lcom/google/android/gms/drive/g/i;

    .line 46
    return-void
.end method

.method private a(J)J
    .locals 3

    .prologue
    .line 173
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/aj;->d()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "databaseInstanceId"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 174
    return-wide p1
.end method

.method private d()Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x0

    .line 64
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 65
    const/4 v0, 0x4

    .line 67
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/drive/g/aj;->a:Landroid/content/Context;

    const-string v2, "drive.shared_prefs"

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method private static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 204
    const-string v0, "ActivityLevel.%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 116
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "bucket_rate_limiter_num_tokens."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 118
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/aj;->d()Landroid/content/SharedPreferences;

    move-result-object v1

    const/4 v2, -0x1

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 119
    return v0
.end method

.method public final a(Ljava/lang/String;IJ)V
    .locals 3

    .prologue
    .line 134
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "bucket_rate_limiter_num_tokens."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "bucket_rate_limiter_last_token_time."

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 138
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/aj;->d()Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2, v0, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1, p3, p4}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 143
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 96
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/aj;->d()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "drive_preferences.sync_over_wifi_only"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 97
    return-void
.end method

.method public final a()Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 102
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/aj;->d()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 103
    const-string v1, "drive_preferences.sync_over_wifi_only"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 104
    iget-object v1, p0, Lcom/google/android/gms/drive/g/aj;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 105
    const-string v2, "drive_preferences.sync_over_wifi_only"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 107
    const-string v2, "drive_preferences.sync_over_wifi_only"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/drive/g/aj;->a(Z)V

    .line 111
    :cond_0
    const-string v1, "drive_preferences.sync_over_wifi_only"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/drive/metadata/sync/c/c;)Z
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 194
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/aj;->d()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-static {p1}, Lcom/google/android/gms/drive/g/aj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4}, Lorg/json/JSONArray;-><init>()V

    iget-object v5, p2, Lcom/google/android/gms/drive/metadata/sync/c/c;->b:[J

    array-length v6, v5

    move v1, v0

    :goto_0
    if-ge v1, v6, :cond_0

    aget-wide v8, v5, v1

    invoke-virtual {v4, v8, v9}, Lorg/json/JSONArray;->put(J)Lorg/json/JSONArray;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    const-string v5, "activeLevel"

    iget-object v6, p2, Lcom/google/android/gms/drive/metadata/sync/c/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    const-string v5, "activityHistory"

    invoke-virtual {v1, v5, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    invoke-virtual {v1}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v3, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 199
    :goto_1
    return v0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method public final b()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 148
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/aj;->d()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 149
    const-string v1, "databaseInstanceId"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 150
    iget-object v1, p0, Lcom/google/android/gms/drive/g/aj;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 151
    const-string v2, "databaseInstanceId"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 153
    const-string v2, "databaseInstanceId"

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/drive/g/aj;->a(J)J

    .line 158
    :cond_0
    const-string v1, "databaseInstanceId"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 159
    cmp-long v2, v0, v4

    if-nez v2, :cond_1

    .line 160
    const-string v0, "PreferenceUtilsImpl"

    const-string v1, "Database creation timestamp not found! Re-initing to current timestamp"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    invoke-virtual {p0}, Lcom/google/android/gms/drive/g/aj;->c()J

    move-result-wide v0

    .line 163
    :cond_1
    return-wide v0
.end method

.method public final b(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 125
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "bucket_rate_limiter_last_token_time."

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/aj;->d()Landroid/content/SharedPreferences;

    move-result-object v1

    const-wide/16 v2, -0x1

    invoke-interface {v1, v0, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 128
    return-wide v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aj;->b:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v0

    .line 169
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/drive/g/aj;->a(J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/c/c;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 180
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/aj;->d()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/drive/g/aj;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_1

    .line 183
    :try_start_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2, v0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    const-string v0, "activeLevel"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "activityHistory"

    invoke-virtual {v2, v0}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v2

    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v0

    new-array v4, v0, [J

    const/4 v0, 0x0

    :goto_0
    array-length v5, v4

    if-ge v0, v5, :cond_0

    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v6

    aput-wide v6, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/c/c;

    invoke-direct {v0, v3, v4}, Lcom/google/android/gms/drive/metadata/sync/c/c;-><init>(Ljava/lang/String;[J)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method
