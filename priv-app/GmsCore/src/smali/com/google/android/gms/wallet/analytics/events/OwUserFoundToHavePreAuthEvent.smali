.class public Lcom/google/android/gms/wallet/analytics/events/OwUserFoundToHavePreAuthEvent;
.super Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/r;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/r;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/OwUserFoundToHavePreAuthEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>(Landroid/os/Parcel;)V

    .line 32
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/OwUserFoundToHavePreAuthEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/wallet/analytics/events/OwUserFoundToHavePreAuthEvent;->j:Ljava/lang/String;

    .line 28
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/OwUserFoundToHavePreAuthEvent;

    invoke-direct {v0, p1}, Lcom/google/android/gms/wallet/analytics/events/OwUserFoundToHavePreAuthEvent;-><init>(Ljava/lang/String;)V

    .line 37
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 38
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/OwUserFoundToHavePreAuthEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method
