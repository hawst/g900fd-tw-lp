.class public final Lcom/google/android/gms/ads/internal/g/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/g/m;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Landroid/util/DisplayMetrics;Ljava/util/Map;Ljava/lang/String;I)I
    .locals 3

    .prologue
    .line 60
    invoke-interface {p1, p2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 62
    if-eqz v0, :cond_0

    .line 64
    :try_start_0
    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    invoke-static {p0, v1}, Lcom/google/android/gms/ads/internal/util/client/a;->a(Landroid/util/DisplayMetrics;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result p3

    .line 69
    :cond_0
    :goto_0
    return p3

    .line 66
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not parse "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in a video GMSG: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V
    .locals 9

    .prologue
    const/4 v7, -0x1

    const/4 v4, 0x0

    .line 75
    const-string v0, "action"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 76
    if-nez v0, :cond_0

    .line 77
    const-string v0, "Action missing from video GMSG."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    .line 172
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/ads/internal/p/a;->d()Lcom/google/android/gms/ads/internal/overlay/c;

    move-result-object v1

    .line 83
    if-nez v1, :cond_1

    .line 84
    const-string v0, "Could not get ad overlay for a video GMSG."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto :goto_0

    .line 89
    :cond_1
    const-string v2, "new"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    .line 90
    const-string v3, "position"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    .line 91
    if-nez v2, :cond_2

    if-eqz v3, :cond_4

    .line 93
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/ads/internal/p/a;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 95
    const-string v3, "x"

    invoke-static {v0, p2, v3, v4}, Lcom/google/android/gms/ads/internal/g/s;->a(Landroid/util/DisplayMetrics;Ljava/util/Map;Ljava/lang/String;I)I

    move-result v3

    .line 96
    const-string v5, "y"

    invoke-static {v0, p2, v5, v4}, Lcom/google/android/gms/ads/internal/g/s;->a(Landroid/util/DisplayMetrics;Ljava/util/Map;Ljava/lang/String;I)I

    move-result v4

    .line 97
    const-string v5, "w"

    invoke-static {v0, p2, v5, v7}, Lcom/google/android/gms/ads/internal/g/s;->a(Landroid/util/DisplayMetrics;Ljava/util/Map;Ljava/lang/String;I)I

    move-result v5

    .line 102
    const-string v6, "h"

    invoke-static {v0, p2, v6, v7}, Lcom/google/android/gms/ads/internal/g/s;->a(Landroid/util/DisplayMetrics;Ljava/util/Map;Ljava/lang/String;I)I

    move-result v0

    .line 108
    if-eqz v2, :cond_3

    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/overlay/c;->b()Lcom/google/android/gms/ads/internal/overlay/j;

    move-result-object v2

    if-nez v2, :cond_3

    .line 109
    invoke-virtual {v1, v3, v4, v5, v0}, Lcom/google/android/gms/ads/internal/overlay/c;->b(IIII)V

    goto :goto_0

    .line 111
    :cond_3
    invoke-virtual {v1, v3, v4, v5, v0}, Lcom/google/android/gms/ads/internal/overlay/c;->a(IIII)V

    goto :goto_0

    .line 115
    :cond_4
    invoke-virtual {v1}, Lcom/google/android/gms/ads/internal/overlay/c;->b()Lcom/google/android/gms/ads/internal/overlay/j;

    move-result-object v8

    .line 116
    if-nez v8, :cond_5

    .line 118
    const-string v0, "no_video_view"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/ads/internal/overlay/j;->a(Lcom/google/android/gms/ads/internal/p/a;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 123
    :cond_5
    const-string v1, "click"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 124
    invoke-virtual {p1}, Lcom/google/android/gms/ads/internal/p/a;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    .line 126
    const-string v1, "x"

    invoke-static {v0, p2, v1, v4}, Lcom/google/android/gms/ads/internal/g/s;->a(Landroid/util/DisplayMetrics;Ljava/util/Map;Ljava/lang/String;I)I

    move-result v2

    .line 127
    const-string v1, "y"

    invoke-static {v0, p2, v1, v4}, Lcom/google/android/gms/ads/internal/g/s;->a(Landroid/util/DisplayMetrics;Ljava/util/Map;Ljava/lang/String;I)I

    move-result v3

    .line 130
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 131
    int-to-float v5, v2

    int-to-float v6, v3

    move-wide v2, v0

    move v7, v4

    invoke-static/range {v0 .. v7}, Landroid/view/MotionEvent;->obtain(JJIFFI)Landroid/view/MotionEvent;

    move-result-object v0

    .line 133
    invoke-virtual {v8, v0}, Lcom/google/android/gms/ads/internal/overlay/j;->a(Landroid/view/MotionEvent;)V

    .line 134
    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    goto/16 :goto_0

    .line 135
    :cond_6
    const-string v1, "controls"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 136
    const-string v0, "enabled"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 137
    if-nez v0, :cond_7

    .line 138
    const-string v0, "Enabled parameter missing from controls video GMSG."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 141
    :cond_7
    invoke-static {v0}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v8, v0}, Lcom/google/android/gms/ads/internal/overlay/j;->a(Z)V

    goto/16 :goto_0

    .line 142
    :cond_8
    const-string v1, "currentTime"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 143
    const-string v0, "time"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 144
    if-nez v0, :cond_9

    .line 145
    const-string v0, "Time parameter missing from currentTime video GMSG."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 149
    :cond_9
    :try_start_0
    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    .line 150
    const/high16 v2, 0x447a0000    # 1000.0f

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 151
    invoke-virtual {v8, v1}, Lcom/google/android/gms/ads/internal/overlay/j;->a(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 153
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not parse time parameter from currentTime video GMSG: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 156
    :cond_a
    const-string v1, "hide"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 157
    const/4 v0, 0x4

    invoke-virtual {v8, v0}, Lcom/google/android/gms/ads/internal/overlay/j;->setVisibility(I)V

    goto/16 :goto_0

    .line 158
    :cond_b
    const-string v1, "load"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 159
    invoke-virtual {v8}, Lcom/google/android/gms/ads/internal/overlay/j;->b()V

    goto/16 :goto_0

    .line 160
    :cond_c
    const-string v1, "pause"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 161
    invoke-virtual {v8}, Lcom/google/android/gms/ads/internal/overlay/j;->c()V

    goto/16 :goto_0

    .line 162
    :cond_d
    const-string v1, "play"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_e

    .line 163
    invoke-virtual {v8}, Lcom/google/android/gms/ads/internal/overlay/j;->d()V

    goto/16 :goto_0

    .line 164
    :cond_e
    const-string v1, "show"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 165
    invoke-virtual {v8, v4}, Lcom/google/android/gms/ads/internal/overlay/j;->setVisibility(I)V

    goto/16 :goto_0

    .line 166
    :cond_f
    const-string v1, "src"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_10

    .line 167
    const-string v0, "src"

    invoke-interface {p2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v8, v0}, Lcom/google/android/gms/ads/internal/overlay/j;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 169
    :cond_10
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown video action: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->e(Ljava/lang/String;)V

    goto/16 :goto_0
.end method
