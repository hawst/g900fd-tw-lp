.class public abstract Lcom/google/android/gms/people/service/a/a/a;
.super Lcom/google/android/gms/people/service/a/a/c;
.source "SourceFile"


# instance fields
.field protected final g:Lcom/google/android/gms/people/model/AvatarReference;

.field protected final h:Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;Lcom/google/android/gms/people/model/AvatarReference;Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/people/service/a/a/c;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/service/d;)V

    .line 20
    iput-object p5, p0, Lcom/google/android/gms/people/service/a/a/a;->g:Lcom/google/android/gms/people/model/AvatarReference;

    .line 21
    iput-object p6, p0, Lcom/google/android/gms/people/service/a/a/a;->h:Lcom/google/android/gms/people/internal/ParcelableLoadImageOptions;

    .line 22
    return-void
.end method


# virtual methods
.method protected final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 26
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[avatar ref="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/a/a;->g:Lcom/google/android/gms/people/model/AvatarReference;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
