.class final Lcom/google/android/gms/droidguard/d/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/droidguard/d/m;


# instance fields
.field final synthetic a:Lcom/google/android/gms/droidguard/d/s;

.field final synthetic b:Lcom/google/android/gms/droidguard/d/h;


# direct methods
.method constructor <init>(Lcom/google/android/gms/droidguard/d/h;Lcom/google/android/gms/droidguard/d/s;)V
    .locals 0

    .prologue
    .line 76
    iput-object p1, p0, Lcom/google/android/gms/droidguard/d/i;->b:Lcom/google/android/gms/droidguard/d/h;

    iput-object p2, p0, Lcom/google/android/gms/droidguard/d/i;->a:Lcom/google/android/gms/droidguard/d/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;)V
    .locals 9

    .prologue
    .line 76
    move-object v1, p1

    check-cast v1, Ljava/lang/Class;

    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/i;->b:Lcom/google/android/gms/droidguard/d/h;

    iget-object v6, v0, Lcom/google/android/gms/droidguard/d/h;->d:Lcom/google/android/gms/droidguard/d/e;

    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/i;->b:Lcom/google/android/gms/droidguard/d/h;

    iget-object v3, v0, Lcom/google/android/gms/droidguard/d/h;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/i;->a:Lcom/google/android/gms/droidguard/d/s;

    iget-object v7, v0, Lcom/google/android/gms/droidguard/d/s;->b:Lcom/google/android/gms/droidguard/d/ag;

    iget-object v4, p0, Lcom/google/android/gms/droidguard/d/i;->a:Lcom/google/android/gms/droidguard/d/s;

    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/i;->b:Lcom/google/android/gms/droidguard/d/h;

    iget-object v5, v0, Lcom/google/android/gms/droidguard/d/h;->b:Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/i;->b:Lcom/google/android/gms/droidguard/d/h;

    iget-object v8, v0, Lcom/google/android/gms/droidguard/d/h;->c:Lcom/google/android/gms/droidguard/d/k;

    :try_start_0
    new-instance v0, Lcom/google/android/gms/droidguard/d/ah;

    iget-object v2, v6, Lcom/google/android/gms/droidguard/d/e;->a:Landroid/content/Context;

    iget-object v4, v4, Lcom/google/android/gms/droidguard/d/s;->a:[B

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/droidguard/d/ah;-><init>(Ljava/lang/Class;Landroid/content/Context;Ljava/lang/String;[BLjava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    iget-object v1, v6, Lcom/google/android/gms/droidguard/d/e;->c:Lcom/google/android/gms/droidguard/d/a;

    invoke-virtual {v1, v7}, Lcom/google/android/gms/droidguard/d/a;->b(Ljava/lang/Object;)V
    :try_end_1
    .catch Lcom/google/android/gms/droidguard/d/b; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    :try_start_2
    invoke-virtual {v8, v0}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/Object;)Lcom/google/android/gms/droidguard/d/k;

    :goto_1
    return-void

    :catch_0
    move-exception v1

    const-string v2, "DG"

    const-string v3, "Failed to touch cache."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v8, v0}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/Throwable;)Lcom/google/android/gms/droidguard/d/k;

    goto :goto_1
.end method

.method public final a(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/droidguard/d/i;->b:Lcom/google/android/gms/droidguard/d/h;

    iget-object v0, v0, Lcom/google/android/gms/droidguard/d/h;->c:Lcom/google/android/gms/droidguard/d/k;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/droidguard/d/k;->a(Ljava/lang/Throwable;)Lcom/google/android/gms/droidguard/d/k;

    .line 87
    return-void
.end method
