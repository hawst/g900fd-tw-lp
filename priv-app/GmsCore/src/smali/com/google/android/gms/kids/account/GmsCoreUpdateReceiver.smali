.class public Lcom/google/android/gms/kids/account/GmsCoreUpdateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 23
    const-string v0, "GmsCoreUpdateReceiver"

    const-string v1, "onReceive: action=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    invoke-static {}, Lcom/google/android/gms/kids/common/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 26
    const-string v0, "GmsCoreUpdateReceiver"

    const-string v1, "Not supported framework version. No reportDeviceInfo."

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 36
    :goto_0
    return-void

    .line 30
    :cond_0
    new-instance v0, Lcom/google/android/gms/kids/account/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/kids/account/d;-><init>(Lcom/google/android/gms/kids/account/GmsCoreUpdateReceiver;)V

    invoke-static {p1, v0}, Lcom/google/android/gms/kids/common/service/KidsApiService;->a(Landroid/content/Context;Lcom/google/android/gms/kids/common/c/b;)V

    goto :goto_0
.end method
