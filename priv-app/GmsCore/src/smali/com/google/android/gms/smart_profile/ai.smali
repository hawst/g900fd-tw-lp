.class final Lcom/google/android/gms/smart_profile/ai;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V
    .locals 0

    .prologue
    .line 242
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/ai;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;B)V
    .locals 0

    .prologue
    .line 242
    invoke-direct {p0, p1}, Lcom/google/android/gms/smart_profile/ai;-><init>(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/people/identity/b;Z)V
    .locals 4

    .prologue
    .line 269
    const/4 v1, 0x0

    .line 271
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/people/identity/b;->c()Lcom/google/android/gms/common/data/d;

    move-result-object v1

    .line 272
    if-eqz v1, :cond_0

    invoke-interface {v1}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v0

    if-lez v0, :cond_0

    .line 273
    iget-object v2, p0, Lcom/google/android/gms/smart_profile/ai;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-interface {p1}, Lcom/google/android/gms/people/identity/b;->c()Lcom/google/android/gms/common/data/d;

    move-result-object v0

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    invoke-static {v2, v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;Lcom/google/android/gms/smart_profile/SmartProfilePerson;)Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    .line 274
    const-string v0, "IdentityPersonUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Loaded person:\n"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/ai;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v3}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->k(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/smart_profile/bc;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 277
    :cond_0
    if-eqz v1, :cond_1

    if-eqz p2, :cond_1

    .line 278
    invoke-interface {v1}, Lcom/google/android/gms/common/data/d;->w_()V

    .line 281
    :cond_1
    return-void

    .line 277
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_2

    if-eqz p2, :cond_2

    .line 278
    invoke-interface {v1}, Lcom/google/android/gms/common/data/d;->w_()V

    :cond_2
    throw v0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 2

    .prologue
    .line 242
    check-cast p1, Lcom/google/android/gms/people/identity/b;

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/ai;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a:Ljava/util/List;

    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/people/identity/b;->d()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/people/identity/b;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/people/identity/b;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/smart_profile/ai;->a(Lcom/google/android/gms/people/identity/b;Z)V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/ai;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->q(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/people/identity/b;->e()Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/ai;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->r(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)Z

    if-eqz p1, :cond_2

    invoke-interface {p1}, Lcom/google/android/gms/people/identity/b;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1}, Lcom/google/android/gms/people/identity/b;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/smart_profile/ai;->a(Lcom/google/android/gms/people/identity/b;Z)V

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/ai;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->q(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/ai;->a:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->s(Lcom/google/android/gms/smart_profile/IdentityPersonUtil;)V

    goto :goto_0
.end method
