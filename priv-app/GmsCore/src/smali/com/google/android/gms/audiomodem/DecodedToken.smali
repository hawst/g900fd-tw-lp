.class public Lcom/google/android/gms/audiomodem/DecodedToken;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:[B

.field private final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/audiomodem/ag;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/ag;-><init>()V

    sput-object v0, Lcom/google/android/gms/audiomodem/DecodedToken;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(I[BI)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p1, p0, Lcom/google/android/gms/audiomodem/DecodedToken;->a:I

    .line 38
    iput-object p2, p0, Lcom/google/android/gms/audiomodem/DecodedToken;->b:[B

    .line 39
    iput p3, p0, Lcom/google/android/gms/audiomodem/DecodedToken;->c:I

    .line 40
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/audiomodem/DecodedToken;-><init>(I[BI)V

    .line 46
    return-void
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 49
    iget v0, p0, Lcom/google/android/gms/audiomodem/DecodedToken;->a:I

    return v0
.end method

.method public final b()[B
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/DecodedToken;->b:[B

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 57
    iget v0, p0, Lcom/google/android/gms/audiomodem/DecodedToken;->c:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 67
    invoke-static {p0, p1}, Lcom/google/android/gms/audiomodem/ag;->a(Lcom/google/android/gms/audiomodem/DecodedToken;Landroid/os/Parcel;)V

    .line 68
    return-void
.end method
