.class public final Lcom/google/android/gms/reminders/service/a/e;
.super Lcom/google/android/gms/reminders/service/a/a;
.source "SourceFile"


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/reminders/LoadRemindersOptions;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/reminders/internal/b;Ljava/lang/String;Lcom/google/android/gms/reminders/LoadRemindersOptions;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/gms/reminders/service/a/a;-><init>(Lcom/google/android/gms/reminders/internal/b;)V

    .line 34
    iput-object p2, p0, Lcom/google/android/gms/reminders/service/a/e;->b:Ljava/lang/String;

    .line 35
    iput-object p3, p0, Lcom/google/android/gms/reminders/service/a/e;->c:Lcom/google/android/gms/reminders/LoadRemindersOptions;

    .line 36
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v0, 0x0

    .line 27
    check-cast p1, Lcom/google/android/gms/reminders/service/RemindersIntentService;

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/reminders/service/a/e;->b:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/google/android/gms/reminders/a/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/reminders/a/d;

    move-result-object v4

    if-nez v4, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/reminders/service/a/e;->a:Lcom/google/android/gms/reminders/internal/b;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x1770

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/reminders/internal/b;->a(Lcom/google/android/gms/common/api/Status;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string v1, "account_id=? AND deleted=?"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, v4, Lcom/google/android/gms/reminders/a/d;->a:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v5

    const/4 v4, 0x1

    const-string v5, "0"

    aput-object v5, v2, v4

    iget-object v4, p0, Lcom/google/android/gms/reminders/service/a/e;->c:Lcom/google/android/gms/reminders/LoadRemindersOptions;

    invoke-virtual {v4}, Lcom/google/android/gms/reminders/LoadRemindersOptions;->b()Ljava/util/List;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/reminders/service/a/e;->c:Lcom/google/android/gms/reminders/LoadRemindersOptions;

    invoke-virtual {v5}, Lcom/google/android/gms/reminders/LoadRemindersOptions;->a()Ljava/util/List;

    move-result-object v5

    if-nez v4, :cond_2

    if-eqz v5, :cond_c

    :cond_2
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_3

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "server_assigned_id IN ("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v8, ","

    invoke-static {v8, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, ")"

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_b

    :goto_1
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    if-ge v0, v5, :cond_5

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-eqz v5, :cond_4

    const-string v5, " OR "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_4
    const-string v5, "client_assigned_id=? "

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    :goto_2
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Lcom/google/android/gms/reminders/c/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    array-length v1, v2

    if-nez v1, :cond_7

    :goto_3
    move-object v2, v0

    move-object v0, v4

    :goto_4
    iget-object v1, p0, Lcom/google/android/gms/reminders/service/a/e;->c:Lcom/google/android/gms/reminders/LoadRemindersOptions;

    invoke-virtual {v1}, Lcom/google/android/gms/reminders/LoadRemindersOptions;->c()Ljava/util/List;

    move-result-object v1

    if-eqz v1, :cond_6

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "task_list IN ("

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ","

    iget-object v5, p0, Lcom/google/android/gms/reminders/service/a/e;->c:Lcom/google/android/gms/reminders/LoadRemindersOptions;

    invoke-virtual {v5}, Lcom/google/android/gms/reminders/LoadRemindersOptions;->c()Ljava/util/List;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, ")"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/reminders/c/d;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_6
    sget-object v1, Lcom/google/android/gms/reminders/internal/a/k;->a:Landroid/net/Uri;

    invoke-static {p1, v1, v0, v2}, Lcom/google/android/gms/common/e/c;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/reminders/service/a/e;->a:Lcom/google/android/gms/reminders/internal/b;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/reminders/internal/b;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/api/Status;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto/16 :goto_0

    :cond_7
    if-eqz v0, :cond_8

    :try_start_2
    array-length v1, v0

    if-nez v1, :cond_9

    :cond_8
    move-object v0, v2

    goto :goto_3

    :cond_9
    array-length v1, v2

    array-length v5, v0

    add-int/2addr v1, v5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    array-length v7, v2

    invoke-static {v2, v5, v1, v6, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v5, 0x0

    array-length v2, v2

    array-length v6, v0

    invoke-static {v0, v5, v1, v2, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-object v0, v1

    goto :goto_3

    :catch_0
    move-exception v0

    move-object v0, v3

    :goto_5
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/reminders/service/a/e;->a:Lcom/google/android/gms/reminders/internal/b;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/reminders/internal/b;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/api/Status;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :goto_6
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :goto_7
    if-eqz v3, :cond_a

    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_a
    throw v0

    :catchall_1
    move-exception v1

    move-object v3, v0

    move-object v0, v1

    goto :goto_7

    :catch_1
    move-exception v1

    goto :goto_6

    :catch_2
    move-exception v1

    goto :goto_5

    :cond_b
    move-object v0, v3

    goto/16 :goto_2

    :cond_c
    move-object v0, v1

    goto :goto_4
.end method
