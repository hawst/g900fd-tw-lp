.class public final Lcom/google/android/gms/games/ui/d/p;
.super Landroid/os/Handler;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:I

.field private final b:Landroid/view/View;

.field private final c:Landroid/view/View;

.field private final d:Landroid/view/View;

.field private final e:Landroid/view/View;

.field private final f:Landroid/view/View;

.field private final g:Landroid/view/View;

.field private final h:Ljava/util/ArrayList;

.field private i:I

.field private final j:Lcom/google/android/gms/games/ui/d/q;

.field private final k:Lcom/google/android/gms/games/ui/d/r;

.field private final l:Lcom/google/android/gms/games/ui/d/s;


# direct methods
.method public constructor <init>(Landroid/view/View;IIIIILcom/google/android/gms/games/ui/d/r;Lcom/google/android/gms/games/ui/d/q;Lcom/google/android/gms/games/ui/d/s;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 160
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 91
    iput v2, p0, Lcom/google/android/gms/games/ui/d/p;->a:I

    .line 103
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/d/p;->i:I

    .line 161
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 162
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    if-ne v0, v3, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Must be called from UI thread"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 165
    invoke-virtual {p1, p3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->c:Landroid/view/View;

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->c:Landroid/view/View;

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->c:Landroid/view/View;

    invoke-static {v0, p10}, Lcom/google/android/gms/games/ui/d/p;->a(Landroid/view/View;I)V

    .line 170
    const v0, 0x102000a

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->b:Landroid/view/View;

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->b:Landroid/view/View;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 175
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->h:Ljava/util/ArrayList;

    .line 178
    invoke-virtual {p1, p4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->d:Landroid/view/View;

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->d:Landroid/view/View;

    if-eqz v0, :cond_4

    move v0, v1

    :goto_3
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->h:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/p;->d:Landroid/view/View;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 183
    invoke-virtual {p1, p5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->e:Landroid/view/View;

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->e:Landroid/view/View;

    if-eqz v0, :cond_5

    move v0, v1

    :goto_4
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->h:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/p;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    invoke-virtual {p1, p6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->f:Landroid/view/View;

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->f:Landroid/view/View;

    if-eqz v0, :cond_6

    move v0, v1

    :goto_5
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->h:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/d/p;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 192
    invoke-virtual {p0, p10}, Lcom/google/android/gms/games/ui/d/p;->a(I)V

    .line 195
    invoke-static {p7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/d/r;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->k:Lcom/google/android/gms/games/ui/d/r;

    .line 198
    iput-object p8, p0, Lcom/google/android/gms/games/ui/d/p;->j:Lcom/google/android/gms/games/ui/d/q;

    .line 201
    iput-object p9, p0, Lcom/google/android/gms/games/ui/d/p;->l:Lcom/google/android/gms/games/ui/d/s;

    .line 204
    sget v0, Lcom/google/android/gms/j;->fD:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 206
    if-eqz v0, :cond_7

    move v3, v1

    :goto_6
    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 207
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    sget v0, Lcom/google/android/gms/j;->ha:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 212
    if-eqz v0, :cond_8

    :goto_7
    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 213
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 216
    sget v0, Lcom/google/android/gms/j;->fp:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 217
    if-eqz v0, :cond_0

    .line 218
    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 221
    :cond_0
    sget v0, Lcom/google/android/gms/j;->sm:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->g:Landroid/view/View;

    .line 223
    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/d/p;->b(I)V

    .line 224
    return-void

    :cond_1
    move v0, v2

    .line 162
    goto/16 :goto_0

    :cond_2
    move v0, v2

    .line 166
    goto/16 :goto_1

    :cond_3
    move v0, v2

    .line 171
    goto/16 :goto_2

    :cond_4
    move v0, v2

    .line 179
    goto/16 :goto_3

    :cond_5
    move v0, v2

    .line 184
    goto :goto_4

    :cond_6
    move v0, v2

    .line 189
    goto :goto_5

    :cond_7
    move v3, v2

    .line 206
    goto :goto_6

    :cond_8
    move v1, v2

    .line 212
    goto :goto_7
.end method

.method private constructor <init>(Landroid/view/View;IIIILcom/google/android/gms/games/ui/d/r;Lcom/google/android/gms/games/ui/d/q;Lcom/google/android/gms/games/ui/d/s;)V
    .locals 11

    .prologue
    .line 137
    const v2, 0x102000a

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/games/ui/d/p;-><init>(Landroid/view/View;IIIIILcom/google/android/gms/games/ui/d/r;Lcom/google/android/gms/games/ui/d/q;Lcom/google/android/gms/games/ui/d/s;I)V

    .line 139
    return-void
.end method

.method public constructor <init>(Landroid/view/View;Lcom/google/android/gms/games/ui/d/r;Lcom/google/android/gms/games/ui/d/q;Lcom/google/android/gms/games/ui/d/s;)V
    .locals 9

    .prologue
    .line 238
    sget v2, Lcom/google/android/gms/j;->ln:I

    sget v3, Lcom/google/android/gms/j;->fw:I

    sget v4, Lcom/google/android/gms/j;->mv:I

    sget v5, Lcom/google/android/gms/j;->hc:I

    move-object v0, p0

    move-object v1, p1

    move-object v6, p2

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/games/ui/d/p;-><init>(Landroid/view/View;IIIILcom/google/android/gms/games/ui/d/r;Lcom/google/android/gms/games/ui/d/q;Lcom/google/android/gms/games/ui/d/s;)V

    .line 241
    return-void
.end method

.method private static a(Landroid/view/View;I)V
    .locals 4

    .prologue
    .line 263
    if-eqz p1, :cond_0

    .line 264
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 265
    instance-of v1, v0, Landroid/widget/LinearLayout$LayoutParams;

    if-eqz v1, :cond_1

    .line 266
    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 267
    iget v1, v0, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    iget v2, v0, Landroid/widget/LinearLayout$LayoutParams;->rightMargin:I

    iget v3, v0, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    .line 268
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 279
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    instance-of v1, v0, Landroid/widget/RelativeLayout$LayoutParams;

    if-eqz v1, :cond_2

    .line 270
    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    .line 271
    iget v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v2, v0, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    iget v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    .line 272
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 273
    :cond_2
    instance-of v1, v0, Landroid/widget/FrameLayout$LayoutParams;

    if-eqz v1, :cond_0

    .line 274
    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 275
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    iget v2, v0, Landroid/widget/FrameLayout$LayoutParams;->rightMargin:I

    iget v3, v0, Landroid/widget/FrameLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1, p1, v2, v3}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 276
    invoke-virtual {p0, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 365
    iget v0, p0, Lcom/google/android/gms/games/ui/d/p;->a:I

    return v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 244
    iget v0, p0, Lcom/google/android/gms/games/ui/d/p;->i:I

    if-eq p1, v0, :cond_1

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 247
    invoke-static {v0, p1}, Lcom/google/android/gms/games/ui/d/p;->a(Landroid/view/View;I)V

    .line 248
    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    .line 249
    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    goto :goto_0

    .line 251
    :cond_0
    iput p1, p0, Lcom/google/android/gms/games/ui/d/p;->i:I

    .line 253
    :cond_1
    return-void
.end method

.method public final a(IIZ)V
    .locals 2

    .prologue
    const/4 v0, 0x2

    .line 380
    invoke-static {p1}, Lcom/google/android/gms/games/ui/d/al;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p2, :cond_0

    .line 381
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/d/p;->b(I)V

    .line 389
    :goto_0
    return-void

    .line 382
    :cond_0
    if-eqz p1, :cond_1

    if-nez p2, :cond_1

    .line 383
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/d/p;->b(I)V

    goto :goto_0

    .line 384
    :cond_1
    if-nez p2, :cond_3

    .line 385
    if-eqz p3, :cond_2

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/d/p;->b(I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x3

    goto :goto_1

    .line 387
    :cond_3
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/d/p;->b(I)V

    goto :goto_0
.end method

.method public final b(I)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v4, 0x8

    .line 288
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v3

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Must be called from UI thread"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 291
    iget v0, p0, Lcom/google/android/gms/games/ui/d/p;->a:I

    if-eq v0, p1, :cond_1

    move v0, v1

    .line 292
    :goto_1
    iput p1, p0, Lcom/google/android/gms/games/ui/d/p;->a:I

    .line 298
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/d/p;->removeMessages(I)V

    .line 300
    packed-switch p1, :pswitch_data_0

    .line 357
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid state!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v2

    .line 288
    goto :goto_0

    :cond_1
    move v0, v2

    .line 291
    goto :goto_1

    .line 302
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/p;->b:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 303
    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/p;->g:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 304
    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/p;->c:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 305
    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/p;->d:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 306
    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/p;->e:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 307
    iget-object v1, p0, Lcom/google/android/gms/games/ui/d/p;->f:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 308
    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->l:Lcom/google/android/gms/games/ui/d/s;

    if-eqz v0, :cond_2

    .line 309
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->l:Lcom/google/android/gms/games/ui/d/s;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/d/s;->x()V

    .line 359
    :cond_2
    :goto_2
    return-void

    .line 314
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 315
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 317
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 318
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 319
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 321
    const-wide/16 v2, 0x1f4

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/gms/games/ui/d/p;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_2

    .line 324
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 325
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 328
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 332
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 333
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 334
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 335
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 336
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 337
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto :goto_2

    .line 340
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 341
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 342
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 343
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 344
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 345
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 349
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->b:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 350
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->g:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->c:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 352
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->d:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->e:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    .line 354
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->f:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    .line 300
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_5
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 393
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 412
    const-string v0, "LoadingDataViewManager"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleMessage: unexpected code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 415
    :goto_0
    return-void

    .line 400
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 401
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->c:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/ap;->a(Landroid/view/View;)V

    goto :goto_0

    .line 400
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    .line 393
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 419
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->fD:I

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->ha:I

    if-ne v0, v1, :cond_2

    .line 421
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->k:Lcom/google/android/gms/games/ui/d/r;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/d/r;->B()V

    .line 426
    :cond_1
    :goto_0
    return-void

    .line 422
    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->fp:I

    if-ne v0, v1, :cond_1

    .line 424
    iget-object v0, p0, Lcom/google/android/gms/games/ui/d/p;->j:Lcom/google/android/gms/games/ui/d/q;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/d/q;->w()V

    goto :goto_0
.end method
