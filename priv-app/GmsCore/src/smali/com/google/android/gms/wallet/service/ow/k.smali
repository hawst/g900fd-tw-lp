.class public final Lcom/google/android/gms/wallet/service/ow/k;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/wallet/service/ow/k;


# instance fields
.field private final b:Landroid/support/v4/g/h;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/k;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/service/ow/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/service/ow/k;->a:Lcom/google/android/gms/wallet/service/ow/k;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Landroid/support/v4/g/h;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Landroid/support/v4/g/h;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/k;->b:Landroid/support/v4/g/h;

    .line 29
    return-void
.end method

.method public static a()Lcom/google/android/gms/wallet/service/ow/k;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/gms/wallet/service/ow/k;->a:Lcom/google/android/gms/wallet/service/ow/k;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/service/ow/l;)Lcom/google/android/gms/wallet/service/ow/j;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/k;->b:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/service/ow/j;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/service/ow/l;Lcom/google/android/gms/wallet/service/ow/j;)V
    .locals 1

    .prologue
    .line 39
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/j;->b()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/k;->b:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    return-void
.end method
