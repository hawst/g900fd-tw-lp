.class public final Lcom/google/android/gms/wallet/common/w;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 62
    const-string v0, "([\\S]{1,4})?([\\S]{1,6})?([\\S]{1,5})?"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/w;->a:Ljava/util/regex/Pattern;

    .line 66
    const-string v0, "((?:[\\S]{4})|(?:[\\S]{1,4}$))"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/w;->b:Ljava/util/regex/Pattern;

    .line 69
    const-string v0, "[^\\d]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/w;->c:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static a(I)I
    .locals 1

    .prologue
    const/4 v0, 0x4

    .line 1063
    packed-switch p0, :pswitch_data_0

    .line 1074
    :goto_0
    :pswitch_0
    return v0

    .line 1068
    :pswitch_1
    const/4 v0, 0x3

    goto :goto_0

    .line 1063
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static a(III)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, -0x1

    const/4 v2, 0x1

    .line 1104
    new-instance v3, Ljava/util/GregorianCalendar;

    invoke-direct {v3}, Ljava/util/GregorianCalendar;-><init>()V

    .line 1106
    if-lez p0, :cond_0

    const/16 v4, 0xc

    if-le p0, v4, :cond_2

    .line 1108
    :cond_0
    const/4 v0, 0x2

    .line 1142
    :cond_1
    :goto_0
    return v0

    .line 1112
    :cond_2
    invoke-virtual {v3, v2}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v4

    if-ge p1, v4, :cond_3

    move v0, v1

    .line 1113
    goto :goto_0

    .line 1119
    :cond_3
    const/16 v4, 0xb

    if-le p0, v4, :cond_4

    .line 1120
    add-int/lit8 p1, p1, 0x1

    move p0, v0

    .line 1123
    :cond_4
    new-instance v4, Ljava/util/GregorianCalendar;

    invoke-direct {v4, p1, p0, v2}, Ljava/util/GregorianCalendar;-><init>(III)V

    .line 1128
    invoke-virtual {v4, v3}, Ljava/util/GregorianCalendar;->compareTo(Ljava/util/Calendar;)I

    move-result v5

    if-gtz v5, :cond_5

    move v0, v1

    .line 1129
    goto :goto_0

    .line 1132
    :cond_5
    if-lez p2, :cond_1

    .line 1134
    invoke-virtual {v3, v2, p2}, Ljava/util/GregorianCalendar;->roll(II)V

    .line 1137
    invoke-virtual {v4, v3}, Ljava/util/GregorianCalendar;->compareTo(Ljava/util/Calendar;)I

    move-result v1

    if-lez v1, :cond_1

    move v0, v2

    .line 1138
    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v1, 0x3

    const/4 v0, 0x0

    const/4 v2, 0x4

    .line 310
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 337
    :cond_0
    :goto_0
    return v0

    .line 314
    :cond_1
    const-string v3, "4"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 315
    const/4 v0, 0x1

    goto :goto_0

    .line 316
    :cond_2
    const-string v3, "51"

    const-string v4, "55"

    invoke-static {p0, v3, v4}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 317
    const/4 v0, 0x2

    goto :goto_0

    .line 318
    :cond_3
    const-string v3, "34"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    move v0, v1

    .line 319
    goto :goto_0

    .line 320
    :cond_4
    const-string v3, "37"

    invoke-virtual {p0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    move v0, v1

    .line 321
    goto :goto_0

    .line 322
    :cond_5
    const-string v1, "3528"

    const-string v3, "358"

    invoke-static {p0, v1, v3}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 323
    const/4 v0, 0x5

    goto :goto_0

    .line 324
    :cond_6
    const-string v1, "60110"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    move v0, v2

    .line 325
    goto :goto_0

    .line 326
    :cond_7
    const-string v1, "60112"

    const-string v3, "60114"

    invoke-static {p0, v1, v3}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    move v0, v2

    .line 327
    goto :goto_0

    .line 328
    :cond_8
    const-string v1, "601174"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    move v0, v2

    .line 329
    goto :goto_0

    .line 330
    :cond_9
    const-string v1, "601177"

    const-string v3, "601179"

    invoke-static {p0, v1, v3}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    move v0, v2

    .line 331
    goto :goto_0

    .line 332
    :cond_a
    const-string v1, "601186"

    const-string v3, "60119"

    invoke-static {p0, v1, v3}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    move v0, v2

    .line 333
    goto :goto_0

    .line 334
    :cond_b
    const-string v1, "644"

    const-string v3, "65"

    invoke-static {p0, v1, v3}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v2

    .line 335
    goto :goto_0
.end method

.method public static a([Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 630
    .line 632
    array-length v4, p0

    const/4 v0, 0x0

    move v3, v0

    move-object v0, v2

    :goto_0
    if-ge v3, v4, :cond_3

    aget-object v1, p0, v3

    .line 633
    iget-boolean v5, v1, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    if-eqz v5, :cond_1

    .line 634
    if-nez v0, :cond_0

    move-object v0, v1

    .line 637
    :cond_0
    iget-boolean v5, v1, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    if-eqz v5, :cond_1

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 643
    :goto_1
    if-eqz v0, :cond_2

    :goto_2
    return-object v0

    .line 632
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 643
    goto :goto_2

    :cond_3
    move-object v1, v0

    move-object v0, v2

    goto :goto_1
.end method

.method public static a([Lcom/google/checkout/inapp/proto/a/b;Lcom/google/checkout/inapp/proto/a/b;)Lcom/google/checkout/inapp/proto/a/b;
    .locals 1

    .prologue
    .line 748
    if-nez p1, :cond_0

    .line 749
    const/4 v0, 0x0

    .line 751
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([Lcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/b;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 759
    if-nez p1, :cond_1

    .line 767
    :cond_0
    :goto_0
    return-object v0

    .line 762
    :cond_1
    array-length v3, p0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v1, p0, v2

    .line 763
    iget-object v4, v1, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v0, v1

    .line 764
    goto :goto_0

    .line 762
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/d;
    .locals 4

    .prologue
    .line 855
    :try_start_0
    new-instance v0, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    .line 856
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->e(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/checkout/inapp/proto/a/d;->a:J

    .line 857
    iput-object p0, v0, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    .line 860
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/checkout/inapp/proto/t;)Lcom/google/checkout/inapp/proto/j;
    .locals 2

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 592
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    .line 594
    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 600
    :goto_0
    return-object v0

    .line 597
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 600
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 562
    .line 564
    array-length v4, p0

    const/4 v0, 0x0

    move v3, v0

    move-object v0, v2

    :goto_0
    if-ge v3, v4, :cond_3

    aget-object v1, p0, v3

    .line 565
    invoke-static {v1}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 566
    if-nez v0, :cond_0

    move-object v0, v1

    .line 569
    :cond_0
    iget-boolean v5, v1, Lcom/google/checkout/inapp/proto/j;->f:Z

    if-eqz v5, :cond_1

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 575
    :goto_1
    if-eqz v0, :cond_2

    :goto_2
    return-object v0

    .line 564
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 575
    goto :goto_2

    :cond_3
    move-object v1, v0

    move-object v0, v2

    goto :goto_1
.end method

.method public static a([Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;
    .locals 1

    .prologue
    .line 691
    if-nez p1, :cond_0

    .line 692
    const/4 v0, 0x0

    .line 694
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/common/util/w;)Lcom/google/checkout/inapp/proto/j;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 721
    if-nez p1, :cond_1

    .line 739
    :cond_0
    return-object v3

    .line 724
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/x;->a(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v0

    if-nez v0, :cond_2

    move-object v6, v3

    .line 725
    :goto_0
    if-eqz v6, :cond_0

    .line 729
    const/high16 v1, -0x80000000

    .line 730
    array-length v7, p0

    move v5, v4

    :goto_1
    if-ge v5, v7, :cond_0

    aget-object v2, p0, v5

    .line 731
    invoke-static {v2}, Lcom/google/android/gms/wallet/common/x;->a(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget v0, v6, Lcom/google/android/gms/wallet/common/x;->b:I

    iget v8, v2, Lcom/google/checkout/inapp/proto/j;->c:I

    if-ne v0, v8, :cond_3

    iget-object v0, v6, Lcom/google/android/gms/wallet/common/x;->a:Ljava/lang/String;

    iget-object v8, v2, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_2
    if-eqz v0, :cond_4

    .line 732
    invoke-interface {p2, v2}, Lcom/google/android/gms/common/util/w;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 733
    if-le v0, v1, :cond_4

    move-object v1, v2

    .line 730
    :goto_3
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    move-object v3, v1

    move v1, v0

    goto :goto_1

    .line 724
    :cond_2
    new-instance v0, Lcom/google/android/gms/wallet/common/x;

    iget-object v1, p1, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    iget v2, p1, Lcom/google/checkout/inapp/proto/j;->c:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/common/x;-><init>(Ljava/lang/String;I)V

    move-object v6, v0

    goto :goto_0

    :cond_3
    move v0, v4

    .line 731
    goto :goto_2

    :cond_4
    move v0, v1

    move-object v1, v3

    goto :goto_3
.end method

.method public static a([Lcom/google/checkout/inapp/proto/j;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/j;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 703
    if-nez p1, :cond_1

    .line 711
    :cond_0
    :goto_0
    return-object v0

    .line 706
    :cond_1
    array-length v3, p0

    const/4 v1, 0x0

    move v2, v1

    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v1, p0, v2

    .line 707
    iget-object v4, v1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    move-object v0, v1

    .line 708
    goto :goto_0

    .line 706
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method public static a([Lcom/google/checkout/inapp/proto/n;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/n;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 655
    if-eqz p0, :cond_0

    if-nez p1, :cond_2

    :cond_0
    move-object v0, v1

    .line 667
    :cond_1
    :goto_0
    return-object v0

    .line 659
    :cond_2
    array-length v3, p0

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_4

    aget-object v0, p0, v2

    .line 661
    iget-object v4, v0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    if-eqz v4, :cond_3

    iget-object v4, v0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    iget-object v4, v4, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    invoke-virtual {p1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 659
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 667
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 544
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 545
    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v0

    .line 546
    if-eqz v0, :cond_0

    array-length v1, v0

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 547
    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 549
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/checkout/inapp/proto/j;ZZZ[I[I[Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 234
    iget v0, p1, Lcom/google/checkout/inapp/proto/j;->l:I

    .line 235
    iget v1, p1, Lcom/google/checkout/inapp/proto/j;->c:I

    .line 236
    invoke-static {p6, v0}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 237
    packed-switch v0, :pswitch_data_0

    .line 243
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected instrument category: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 239
    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->Aj:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 284
    :goto_0
    return-object v0

    .line 241
    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->Ai:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 246
    :cond_0
    invoke-static {p5, v1}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 248
    packed-switch v1, :pswitch_data_1

    .line 252
    sget v0, Lcom/google/android/gms/p;->Ah:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 250
    :pswitch_2
    sget v0, Lcom/google/android/gms/p;->Ag:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 254
    :cond_1
    iget v0, p1, Lcom/google/checkout/inapp/proto/j;->h:I

    if-ne v0, v5, :cond_2

    .line 255
    sget v0, Lcom/google/android/gms/p;->Ax:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 256
    :cond_2
    if-eqz p7, :cond_3

    array-length v0, p7

    if-lez v0, :cond_3

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v0, v0, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-static {p7, v0}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 261
    sget v0, Lcom/google/android/gms/p;->CR:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 262
    :cond_3
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v0, v0

    if-eqz v0, :cond_7

    .line 263
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v0, v0

    if-ne v0, v3, :cond_6

    .line 264
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->g:[I

    aget v0, v0, v4

    if-ne v0, v5, :cond_5

    .line 265
    if-eqz p2, :cond_4

    .line 266
    sget v0, Lcom/google/android/gms/p;->Bz:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 268
    :cond_4
    sget v0, Lcom/google/android/gms/p;->By:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 270
    :cond_5
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->g:[I

    aget v0, v0, v4

    if-ne v0, v3, :cond_6

    .line 271
    sget v0, Lcom/google/android/gms/p;->BN:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 274
    :cond_6
    sget v0, Lcom/google/android/gms/p;->BO:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 275
    :cond_7
    if-eqz p3, :cond_8

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_8

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-boolean v0, v0, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    if-eqz v0, :cond_8

    .line 277
    sget v0, Lcom/google/android/gms/p;->BX:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 278
    :cond_8
    if-eqz p4, :cond_9

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_9

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 280
    sget v0, Lcom/google/android/gms/p;->Cb:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 281
    :cond_9
    iget v0, p1, Lcom/google/checkout/inapp/proto/j;->h:I

    if-eq v0, v3, :cond_a

    .line 282
    sget v0, Lcom/google/android/gms/p;->BO:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 284
    :cond_a
    const-string v0, ""

    goto/16 :goto_0

    .line 237
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 248
    :pswitch_data_1
    .packed-switch 0x3
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Landroid/content/res/Resources;I[I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 204
    invoke-static {p2, p1}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 205
    packed-switch p1, :pswitch_data_0

    .line 212
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 207
    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->Aj:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 209
    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->Ai:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 205
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 871
    invoke-static {}, Lcom/google/android/gms/wallet/common/s;->a()Lcom/google/android/gms/wallet/common/t;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/t;->a()Lcom/google/android/gms/wallet/common/s;

    move-result-object v0

    iget-wide v2, p0, Lcom/google/checkout/inapp/proto/a/d;->a:J

    const/4 v1, 0x6

    invoke-static {v2, v3, v1}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v1

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/s;->a(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/checkout/inapp/proto/j;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 160
    const/4 v0, 0x0

    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/j;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/checkout/inapp/proto/j;Z)Ljava/lang/String;
    .locals 2

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    .line 178
    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->d:I

    if-nez v1, :cond_0

    .line 179
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0, p1}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/j;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 182
    :cond_0
    return-object v0
.end method

.method public static a([Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 933
    const-string v0, "<br />"

    invoke-static {v0, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(J)Ljava/math/BigDecimal;
    .locals 2

    .prologue
    .line 844
    const/4 v0, 0x6

    invoke-static {p0, p1, v0}, Ljava/math/BigDecimal;->valueOf(JI)Ljava/math/BigDecimal;

    move-result-object v0

    return-object v0
.end method

.method public static a([Lcom/google/checkout/inapp/proto/aj;)Ljava/util/ArrayList;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 990
    if-nez p0, :cond_0

    .line 999
    :goto_0
    return-object v1

    .line 994
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    array-length v0, p0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 996
    array-length v4, p0

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v4, :cond_2

    aget-object v5, p0, v3

    .line 997
    if-nez v5, :cond_1

    move-object v0, v1

    :goto_2
    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 996
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 997
    :cond_1
    new-instance v0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    iget-object v6, v5, Lcom/google/checkout/inapp/proto/aj;->a:Ljava/lang/String;

    iget-object v7, v5, Lcom/google/checkout/inapp/proto/aj;->b:Ljava/lang/String;

    iget-object v5, v5, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    invoke-direct {v0, v6, v7, v5}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;-><init>(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    move-object v1, v2

    .line 999
    goto :goto_0
.end method

.method public static a(Lcom/google/checkout/inapp/proto/d;)V
    .locals 9

    .prologue
    .line 894
    const-wide/16 v2, 0x0

    .line 895
    const/4 v1, 0x0

    .line 897
    iget-object v4, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    array-length v5, v4

    const/4 v0, 0x0

    move v8, v0

    move-object v0, v1

    move v1, v8

    :goto_0
    if-ge v1, v5, :cond_2

    aget-object v6, v4, v1

    .line 899
    iget-object v7, v6, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v7, :cond_1

    .line 900
    iget-object v0, v6, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;

    .line 901
    iget-object v6, v6, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    iget-wide v6, v6, Lcom/google/checkout/inapp/proto/a/d;->a:J

    add-long/2addr v2, v6

    .line 897
    :cond_0
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 902
    :cond_1
    iget-object v7, v6, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v7, :cond_0

    .line 903
    iget-object v0, v6, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;

    .line 904
    iget-object v6, v6, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    iget-wide v6, v6, Lcom/google/checkout/inapp/proto/a/d;->a:J

    add-long/2addr v2, v6

    goto :goto_1

    .line 909
    :cond_2
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/ar;->a:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v1, :cond_3

    .line 910
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/ar;->a:Lcom/google/checkout/inapp/proto/a/d;

    iget-wide v0, v0, Lcom/google/checkout/inapp/proto/a/d;->a:J

    add-long/2addr v2, v0

    .line 911
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/ar;->a:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;

    .line 915
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    iget-object v1, v1, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v1, :cond_4

    .line 916
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    iget-wide v0, v0, Lcom/google/checkout/inapp/proto/a/d;->a:J

    add-long/2addr v2, v0

    .line 917
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;

    .line 920
    :cond_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 921
    new-instance v1, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v1}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    .line 922
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    iput-wide v2, v1, Lcom/google/checkout/inapp/proto/a/d;->a:J

    .line 923
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    iput-object v0, v1, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;

    .line 925
    :cond_5
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 1256
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->i(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    sget-object v0, Lcom/google/android/gms/wallet/b/a;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v1, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/widget/ImageView;Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/common/b/a;)Z
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1033
    const-string v0, "imageView is required"

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1034
    if-eqz p1, :cond_1

    iget v0, p1, Lcom/google/checkout/inapp/proto/j;->c:I

    sparse-switch v0, :sswitch_data_0

    move v1, v3

    .line 1036
    :goto_0
    if-eqz p1, :cond_2

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    move-object v4, v0

    .line 1038
    :goto_1
    if-eqz v4, :cond_4

    .line 1039
    invoke-virtual {p0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v5

    if-eqz v1, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "=w"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "-h"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "-n"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1041
    new-instance v5, Lcom/google/android/gms/wallet/shared/common/b/c;

    invoke-direct {v5}, Lcom/google/android/gms/wallet/shared/common/b/c;-><init>()V

    new-instance v6, Lcom/google/android/gms/wallet/shared/common/b/d;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v6, v5, v3}, Lcom/google/android/gms/wallet/shared/common/b/d;-><init>(Lcom/google/android/gms/wallet/shared/common/b/c;B)V

    iget-object v3, v6, Lcom/google/android/gms/wallet/shared/common/b/d;->a:Lcom/google/android/gms/wallet/shared/common/b/c;

    iput-object p0, v3, Lcom/google/android/gms/wallet/shared/common/b/c;->c:Landroid/widget/ImageView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v3, v6, Lcom/google/android/gms/wallet/shared/common/b/d;->a:Lcom/google/android/gms/wallet/shared/common/b/c;

    iput-object v0, v3, Lcom/google/android/gms/wallet/shared/common/b/c;->a:Ljava/lang/String;

    iget-object v0, v6, Lcom/google/android/gms/wallet/shared/common/b/d;->a:Lcom/google/android/gms/wallet/shared/common/b/c;

    iput-boolean v2, v0, Lcom/google/android/gms/wallet/shared/common/b/c;->b:Z

    iget-object v0, v6, Lcom/google/android/gms/wallet/shared/common/b/d;->a:Lcom/google/android/gms/wallet/shared/common/b/c;

    iput v1, v0, Lcom/google/android/gms/wallet/shared/common/b/c;->d:I

    iget-object v0, v6, Lcom/google/android/gms/wallet/shared/common/b/d;->a:Lcom/google/android/gms/wallet/shared/common/b/c;

    iget-object v0, v0, Lcom/google/android/gms/wallet/shared/common/b/c;->a:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v6, Lcom/google/android/gms/wallet/shared/common/b/d;->a:Lcom/google/android/gms/wallet/shared/common/b/c;

    iget-object v0, v0, Lcom/google/android/gms/wallet/shared/common/b/c;->c:Landroid/widget/ImageView;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, v6, Lcom/google/android/gms/wallet/shared/common/b/d;->a:Lcom/google/android/gms/wallet/shared/common/b/c;

    .line 1047
    if-nez p2, :cond_0

    .line 1048
    new-instance p2, Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-virtual {p0}, Landroid/widget/ImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {p2, v1}, Lcom/google/android/gms/wallet/shared/common/b/a;-><init>(Landroid/content/Context;)V

    .line 1050
    :cond_0
    invoke-virtual {p2, v0}, Lcom/google/android/gms/wallet/shared/common/b/a;->a(Lcom/google/android/gms/wallet/shared/common/b/c;)V

    move v0, v2

    .line 1055
    :goto_3
    return v0

    .line 1034
    :sswitch_0
    sget v1, Lcom/google/android/gms/h;->dB:I

    goto/16 :goto_0

    :sswitch_1
    sget v1, Lcom/google/android/gms/h;->dx:I

    goto/16 :goto_0

    :sswitch_2
    sget v1, Lcom/google/android/gms/h;->dw:I

    goto/16 :goto_0

    :sswitch_3
    sget v1, Lcom/google/android/gms/h;->dz:I

    goto/16 :goto_0

    :sswitch_4
    sget v1, Lcom/google/android/gms/h;->dy:I

    goto/16 :goto_0

    :sswitch_5
    sget v1, Lcom/google/android/gms/h;->dA:I

    goto/16 :goto_0

    :sswitch_6
    sget v1, Lcom/google/android/gms/h;->dC:I

    goto/16 :goto_0

    :cond_1
    move v1, v3

    goto/16 :goto_0

    .line 1036
    :cond_2
    const/4 v0, 0x0

    move-object v4, v0

    goto/16 :goto_1

    .line 1039
    :cond_3
    sget v0, Lcom/google/android/gms/h;->dB:I

    goto/16 :goto_2

    .line 1053
    :cond_4
    invoke-static {p0}, Lcom/google/android/gms/wallet/dynamite/image/d;->a(Landroid/widget/ImageView;)Lcom/google/android/gms/wallet/dynamite/image/f;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/dynamite/image/f;->cancel(Z)Z

    .line 1054
    :cond_5
    invoke-virtual {p0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 1055
    if-eqz v1, :cond_6

    move v0, v2

    goto :goto_3

    :cond_6
    move v0, v3

    goto :goto_3

    .line 1034
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_3
        0x3 -> :sswitch_2
        0x4 -> :sswitch_1
        0x1b -> :sswitch_4
        0x20 -> :sswitch_6
        0x21 -> :sswitch_5
    .end sparse-switch
.end method

.method public static a(Lcom/google/checkout/inapp/proto/a/b;)Z
    .locals 1

    .prologue
    .line 609
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/checkout/inapp/proto/j;[I[I)Z
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v0, 0x1

    .line 293
    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->l:I

    invoke-static {p2, v1}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 300
    :cond_0
    :goto_0
    return v0

    .line 296
    :cond_1
    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->c:I

    if-ne v1, v2, :cond_2

    invoke-static {p1, v2}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v1

    if-nez v1, :cond_0

    .line 300
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 352
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    .line 353
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 354
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    .line 358
    if-ge v1, v2, :cond_1

    if-ge v1, v3, :cond_1

    .line 359
    add-int/lit8 v4, v1, -0x1

    invoke-virtual {p1, v4}, Ljava/lang/String;->charAt(I)C

    move-result v4

    add-int/lit8 v5, v1, -0x1

    invoke-virtual {p2, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    if-ne v4, v5, :cond_1

    .line 366
    :cond_0
    :goto_0
    return v0

    .line 363
    :cond_1
    invoke-static {v2, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 364
    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 366
    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v2

    if-gtz v2, :cond_0

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Z)Z
    .locals 5

    .prologue
    const/16 v4, 0x10

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1275
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 1276
    invoke-static {v2}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;)I

    move-result v3

    .line 1277
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    .line 1278
    packed-switch v3, :pswitch_data_0

    .line 1288
    if-eqz p1, :cond_2

    .line 1289
    const/16 v3, 0xc

    if-lt v2, v3, :cond_1

    const/16 v3, 0x13

    if-gt v2, v3, :cond_1

    .line 1292
    :cond_0
    :goto_0
    return v0

    .line 1280
    :pswitch_0
    const/16 v3, 0xf

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 1285
    :pswitch_1
    if-eq v2, v4, :cond_0

    move v0, v1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 1289
    goto :goto_0

    .line 1292
    :cond_2
    if-eq v2, v4, :cond_0

    move v0, v1

    goto :goto_0

    .line 1278
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public static a([Landroid/accounts/Account;)[Lcom/google/android/gms/wallet/common/ui/a;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1084
    if-nez p0, :cond_0

    .line 1092
    :goto_0
    return-object v0

    .line 1087
    :cond_0
    array-length v3, p0

    .line 1088
    new-array v1, v3, [Lcom/google/android/gms/wallet/common/ui/a;

    .line 1089
    const/4 v2, 0x0

    :goto_1
    if-ge v2, v3, :cond_1

    .line 1090
    new-instance v4, Lcom/google/android/gms/wallet/common/ui/a;

    aget-object v5, p0, v2

    invoke-direct {v4, v5, v0}, Lcom/google/android/gms/wallet/common/ui/a;-><init>(Landroid/accounts/Account;Ljava/lang/String;)V

    aput-object v4, v1, v2

    .line 1089
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 1092
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/checkout/inapp/proto/j;Z)[Ljava/lang/String;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 133
    sget-object v0, Lcom/google/android/gms/wallet/b/h;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 135
    invoke-static {p1, v3}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/j;Z)Ljava/lang/String;

    move-result-object v1

    .line 136
    if-nez p2, :cond_0

    .line 137
    new-array v0, v3, [Ljava/lang/String;

    aput-object v1, v0, v4

    .line 144
    :goto_0
    return-object v0

    .line 139
    :cond_0
    if-eqz v0, :cond_1

    .line 140
    sget v0, Lcom/google/android/gms/p;->Df:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 142
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    aput-object v1, v0, v4

    aput-object v2, v0, v3

    goto :goto_0

    .line 144
    :cond_1
    new-array v0, v3, [Ljava/lang/String;

    sget v2, Lcom/google/android/gms/p;->De:I

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v4

    invoke-virtual {p0, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v0, v4

    goto :goto_0
.end method

.method public static a([Lcom/google/aa/b/a/a/a/a/s;)[Ljava/lang/String;
    .locals 5

    .prologue
    .line 1218
    if-nez p0, :cond_0

    .line 1219
    const/4 v0, 0x0

    .line 1227
    :goto_0
    return-object v0

    .line 1221
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    array-length v0, p0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1222
    array-length v2, p0

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, p0, v0

    .line 1223
    iget-object v4, v3, Lcom/google/aa/b/a/a/a/a/s;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1224
    iget-object v3, v3, Lcom/google/aa/b/a/a/a/a/s;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1222
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1227
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method

.method public static b(Lcom/google/checkout/inapp/proto/j;)Lcom/google/aa/b/a/g;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 168
    iget v0, p0, Lcom/google/checkout/inapp/proto/j;->d:I

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 169
    new-instance v0, Lcom/google/aa/b/a/g;

    invoke-direct {v0}, Lcom/google/aa/b/a/g;-><init>()V

    .line 170
    invoke-static {p0, v1}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/j;Z)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/aa/b/a/g;->a:Ljava/lang/String;

    .line 171
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/aa/b/a/g;->b:Ljava/lang/String;

    .line 172
    return-object v0

    .line 168
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1167
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/w;->d(Lcom/google/checkout/inapp/proto/a/d;)I

    move-result v0

    .line 1168
    if-gtz v0, :cond_0

    .line 1169
    const-string v0, "\\d*"

    .line 1173
    :goto_0
    return-object v0

    .line 1171
    :cond_0
    invoke-static {}, Ljava/text/DecimalFormatSymbols;->getInstance()Ljava/text/DecimalFormatSymbols;

    move-result-object v1

    invoke-virtual {v1}, Ljava/text/DecimalFormatSymbols;->getMonetaryDecimalSeparator()C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v1

    .line 1173
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "\\d*(%s\\d{0,%d})?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static b(Lcom/google/checkout/inapp/proto/j;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/google/checkout/inapp/proto/j;->c:I

    sparse-switch v0, :sswitch_data_0

    .line 198
    const-string v0, "XXX"

    :goto_0
    return-object v0

    .line 188
    :sswitch_0
    if-eqz p1, :cond_0

    const-string v0, "VISA"

    goto :goto_0

    :cond_0
    const-string v0, "Visa"

    goto :goto_0

    .line 190
    :sswitch_1
    if-eqz p1, :cond_1

    const-string v0, "AMEX"

    goto :goto_0

    :cond_1
    const-string v0, "Amex"

    goto :goto_0

    .line 192
    :sswitch_2
    if-eqz p1, :cond_2

    const-string v0, "MASTERCARD"

    goto :goto_0

    :cond_2
    const-string v0, "MasterCard"

    goto :goto_0

    .line 194
    :sswitch_3
    if-eqz p1, :cond_3

    const-string v0, "DISCOVER"

    goto :goto_0

    :cond_3
    const-string v0, "Discover"

    goto :goto_0

    .line 196
    :sswitch_4
    const-string v0, "JCB"

    goto :goto_0

    .line 186
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_2
        0x3 -> :sswitch_1
        0x4 -> :sswitch_3
        0x1b -> :sswitch_4
    .end sparse-switch
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 341
    sget-object v0, Lcom/google/android/gms/wallet/b/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 342
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v1, v0, :cond_0

    .line 343
    const/4 v1, 0x0

    invoke-virtual {p0, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 345
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 946
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "<a href=\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</a>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/google/checkout/inapp/proto/a/b;)Z
    .locals 1

    .prologue
    .line 619
    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    return v0
.end method

.method public static b(Ljava/lang/String;Z)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1310
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1311
    invoke-static {v6, p1}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1315
    :cond_0
    :goto_0
    return v2

    :cond_1
    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    move v4, v2

    move v5, v2

    :goto_1
    if-ltz v3, :cond_4

    add-int/lit8 v0, v3, 0x1

    invoke-virtual {v6, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    if-eqz v4, :cond_2

    mul-int/lit8 v0, v0, 0x2

    const/16 v7, 0x9

    if-le v0, v7, :cond_2

    add-int/lit8 v0, v0, -0x9

    :cond_2
    add-int/2addr v5, v0

    if-nez v4, :cond_3

    move v0, v1

    :goto_2
    add-int/lit8 v3, v3, -0x1

    move v4, v0

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_2

    :cond_4
    rem-int/lit8 v0, v5, 0xa

    if-nez v0, :cond_0

    move v2, v1

    goto :goto_0
.end method

.method public static c(Lcom/google/checkout/inapp/proto/a/d;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 1197
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/w;->d(Lcom/google/checkout/inapp/proto/a/d;)I

    move-result v0

    .line 1198
    if-gtz v0, :cond_0

    .line 1199
    const/4 v0, 0x0

    .line 1203
    :goto_0
    return-object v0

    .line 1201
    :cond_0
    invoke-static {}, Ljava/text/DecimalFormatSymbols;->getInstance()Ljava/text/DecimalFormatSymbols;

    move-result-object v1

    invoke-virtual {v1}, Ljava/text/DecimalFormatSymbols;->getMonetaryDecimalSeparator()C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v1

    .line 1203
    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v3, "\\d*%s\\d{%d}"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    const/4 v1, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v4, v1

    invoke-static {v2, v3, v4}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 374
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/wallet/common/w;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v0, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Lcom/google/checkout/inapp/proto/a/b;)Z
    .locals 1

    .prologue
    .line 682
    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    return v0
.end method

.method public static c(Lcom/google/checkout/inapp/proto/j;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 511
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v1, v1

    if-nez v1, :cond_0

    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->h:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Lcom/google/checkout/inapp/proto/a/d;)I
    .locals 4

    .prologue
    .line 1365
    const/4 v0, 0x0

    iget-wide v2, p0, Lcom/google/checkout/inapp/proto/a/d;->a:J

    long-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->log10(D)D

    move-result-wide v2

    double-to-int v1, v2

    rsub-int/lit8 v1, v1, 0x6

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static d(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 383
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 384
    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;)I

    move-result v1

    .line 385
    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 386
    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 389
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static d(Lcom/google/checkout/inapp/proto/j;)Z
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x0

    .line 522
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 536
    :cond_0
    :goto_0
    return v0

    .line 525
    :cond_1
    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->h:I

    if-eq v1, v5, :cond_0

    .line 528
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v1, v1

    if-lez v1, :cond_0

    .line 529
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_2

    aget v4, v2, v1

    .line 530
    if-ne v4, v5, :cond_0

    .line 529
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 534
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static e(Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 823
    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v1

    .line 824
    instance-of v0, v1, Ljava/text/DecimalFormat;

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 825
    check-cast v0, Ljava/text/DecimalFormat;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/text/DecimalFormat;->setParseBigDecimal(Z)V

    .line 827
    :cond_0
    invoke-virtual {v1, p0}, Ljava/text/NumberFormat;->parse(Ljava/lang/String;)Ljava/lang/Number;

    move-result-object v0

    .line 828
    instance-of v1, v0, Ljava/math/BigDecimal;

    if-eqz v1, :cond_1

    .line 829
    check-cast v0, Ljava/math/BigDecimal;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->scaleByPowerOfTen(I)Ljava/math/BigDecimal;

    move-result-object v0

    invoke-virtual {v0}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v0

    .line 833
    :goto_0
    return-wide v0

    :cond_1
    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v0

    const-wide v2, 0x412e848000000000L    # 1000000.0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    goto :goto_0
.end method

.method public static e(Lcom/google/checkout/inapp/proto/j;)Z
    .locals 2

    .prologue
    .line 674
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v0

    return v0
.end method

.method public static f(Lcom/google/checkout/inapp/proto/j;)Lcom/google/checkout/inapp/proto/j;
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 1012
    if-nez p0, :cond_1

    .line 1013
    const/4 p0, 0x0

    .line 1021
    :cond_0
    :goto_0
    return-object p0

    .line 1015
    :cond_1
    iget v0, p0, Lcom/google/checkout/inapp/proto/j;->h:I

    if-eq v0, v1, :cond_0

    .line 1019
    invoke-static {p0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 1020
    iput v1, v0, Lcom/google/checkout/inapp/proto/j;->h:I

    move-object p0, v0

    .line 1021
    goto :goto_0
.end method

.method public static f(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 881
    invoke-static {p0}, Ljava/util/Currency;->getInstance(Ljava/lang/String;)Ljava/util/Currency;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Currency;->getSymbol()Ljava/lang/String;

    move-result-object v0

    .line 882
    invoke-static {}, Lcom/google/android/gms/wallet/common/s;->a()Lcom/google/android/gms/wallet/common/t;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/common/t;->a()Lcom/google/android/gms/wallet/common/s;

    move-result-object v1

    sget-object v2, Ljava/math/BigDecimal;->ZERO:Ljava/math/BigDecimal;

    invoke-virtual {v1, v2, p0}, Lcom/google/android/gms/wallet/common/s;->a(Ljava/math/BigDecimal;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 884
    invoke-virtual {v1, v0}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1231
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1233
    const/4 v0, 0x1

    .line 1235
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/wallet/b/g;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {p0, v1}, Ljava/lang/String;->toUpperCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    goto :goto_0
.end method

.method public static h(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 1240
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1242
    const/4 v0, 0x1

    .line 1246
    :goto_0
    return v0

    .line 1244
    :cond_0
    sget-object v0, Lcom/google/android/gms/wallet/b/g;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, ","

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 1246
    invoke-static {v0, p0}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public static i(Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v6, 0x3

    .line 1324
    invoke-static {p0}, Lcom/google/android/gms/wallet/common/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 1325
    invoke-static {v1}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;)I

    move-result v2

    .line 1326
    if-ne v2, v6, :cond_1

    const/16 v0, 0xf

    .line 1328
    :goto_0
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v4, v0, -0x4

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 1330
    new-array v4, v3, [C

    .line 1331
    const/16 v5, 0x2022

    invoke-static {v4, v5}, Ljava/util/Arrays;->fill([CC)V

    .line 1332
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append([C)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1333
    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 1334
    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1336
    :cond_0
    if-ne v2, v6, :cond_2

    .line 1337
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->k(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1339
    :goto_1
    return-object v0

    .line 1326
    :cond_1
    const/16 v0, 0x10

    goto :goto_0

    .line 1339
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->j(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private static j(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    const/16 v1, 0x10

    .line 401
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 402
    const/4 v0, 0x0

    invoke-virtual {p0, v0, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 405
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 406
    sget-object v1, Lcom/google/android/gms/wallet/common/w;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 407
    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 408
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 411
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static k(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v2, 0xf

    const/4 v0, 0x0

    .line 424
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    if-le v1, v2, :cond_0

    .line 425
    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    .line 427
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 428
    sget-object v2, Lcom/google/android/gms/wallet/common/w;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    .line 430
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 433
    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 434
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    .line 435
    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v2, v3}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 433
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 440
    :cond_2
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
