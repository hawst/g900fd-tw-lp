.class final Lcom/google/android/gms/fitness/b/c/y;
.super Lcom/google/android/gms/fitness/b/c/r;
.source "SourceFile"


# instance fields
.field final synthetic f:Lcom/google/android/gms/fitness/b/e;

.field final synthetic g:Lcom/google/android/gms/fitness/b/c/x;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/b/c/x;Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/e;JLcom/google/android/gms/fitness/b/e;)V
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/fitness/b/c/y;->g:Lcom/google/android/gms/fitness/b/c/x;

    iput-object p6, p0, Lcom/google/android/gms/fitness/b/c/y;->f:Lcom/google/android/gms/fitness/b/e;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/gms/fitness/b/c/r;-><init>(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/e;J)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/fitness/b/d;IJJ)V
    .locals 7

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/y;->g:Lcom/google/android/gms/fitness/b/c/x;

    int-to-float v0, p2

    invoke-static {v0, p3, p4, p5, p6}, Lcom/google/android/gms/fitness/b/c/x;->a(FJJ)F

    move-result v0

    .line 71
    iget-object v1, p0, Lcom/google/android/gms/fitness/b/c/y;->g:Lcom/google/android/gms/fitness/b/c/x;

    invoke-static {v1}, Lcom/google/android/gms/fitness/b/c/x;->a(Lcom/google/android/gms/fitness/b/c/x;)F

    move-result v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/fitness/b/c/y;->f:Lcom/google/android/gms/fitness/b/e;

    invoke-static {v0}, Lcom/google/android/gms/fitness/b/c/w;->b(Lcom/google/android/gms/fitness/b/e;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 72
    int-to-float v0, p2

    iget-object v1, p0, Lcom/google/android/gms/fitness/b/c/y;->g:Lcom/google/android/gms/fitness/b/c/x;

    invoke-static {v1}, Lcom/google/android/gms/fitness/b/c/x;->a(Lcom/google/android/gms/fitness/b/c/x;)F

    move-result v1

    div-float/2addr v0, v1

    .line 73
    sub-long v2, p5, p3

    .line 74
    invoke-static {}, Lcom/google/android/gms/fitness/b/c/x;->c()J

    move-result-wide v4

    long-to-float v1, v4

    mul-float/2addr v0, v1

    float-to-long v0, v0

    .line 75
    invoke-static {}, Lcom/google/android/gms/fitness/b/c/x;->c()J

    move-result-wide v4

    .line 76
    invoke-static {v0, v1, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    sub-long p3, p5, v0

    .line 79
    :cond_0
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, p3, p4, v0}, Lcom/google/android/gms/fitness/b/d;->b(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, p5, p6, v1}, Lcom/google/android/gms/fitness/b/d;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/b/d;

    .line 82
    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/d;->a()Lcom/google/android/gms/fitness/b/k;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/gms/fitness/b/k;->a(I)Lcom/google/android/gms/fitness/b/k;

    .line 83
    return-void
.end method
