.class final Lcom/google/android/gms/feedback/am;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/gms/feedback/ShowTextActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/feedback/ShowTextActivity;Landroid/app/Activity;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/gms/feedback/am;->c:Lcom/google/android/gms/feedback/ShowTextActivity;

    iput-object p2, p0, Lcom/google/android/gms/feedback/am;->a:Landroid/app/Activity;

    iput-object p3, p0, Lcom/google/android/gms/feedback/am;->b:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()[Ljava/lang/String;
    .locals 3

    .prologue
    .line 122
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/am;->a:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/gms/feedback/am;->b:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/feedback/w;->a(Landroid/content/Context;Ljava/lang/String;Z)Landroid/os/AsyncTask;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/AsyncTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 126
    :goto_0
    return-object v0

    .line 123
    :catch_0
    move-exception v0

    :goto_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    .line 126
    const/4 v0, 0x0

    goto :goto_0

    .line 123
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/feedback/am;->a()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 110
    check-cast p1, [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/feedback/am;->a:Landroid/app/Activity;

    sget v1, Lcom/google/android/gms/j;->hq:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/am;->a:Landroid/app/Activity;

    sget v1, Lcom/google/android/gms/j;->hh:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/feedback/am;->c:Lcom/google/android/gms/feedback/ShowTextActivity;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/feedback/ShowTextActivity;->a([Ljava/lang/String;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 2

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/gms/feedback/am;->a:Landroid/app/Activity;

    sget v1, Lcom/google/android/gms/j;->hq:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/feedback/am;->a:Landroid/app/Activity;

    sget v1, Lcom/google/android/gms/j;->hh:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 117
    return-void
.end method
