.class public final Lcom/google/android/gms/photos/autobackup/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field private final a:Lcom/google/android/gms/photos/autobackup/a/ac;

.field private final b:Ljava/lang/String;

.field private final c:Lcom/google/android/gms/photos/autobackup/model/SyncSettings;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/photos/autobackup/a/ac;Ljava/lang/String;Lcom/google/android/gms/photos/autobackup/model/SyncSettings;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/ae;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/ae;->b:Ljava/lang/String;

    .line 29
    iput-object p3, p0, Lcom/google/android/gms/photos/autobackup/ae;->c:Lcom/google/android/gms/photos/autobackup/model/SyncSettings;

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ae;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/a/ac;->d(I)V

    .line 57
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 2

    .prologue
    .line 18
    check-cast p1, Lcom/google/android/gms/photos/autobackup/AutoBackupWorkService;

    const-class v0, Lcom/google/android/libraries/social/account/b;

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/a/a;->a(Landroid/content/Context;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/social/account/b;

    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ae;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/social/account/b;->b(Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ae;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/16 v1, 0x8

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/a/ac;->d(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ae;->c:Lcom/google/android/gms/photos/autobackup/model/SyncSettings;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/model/SyncSettings;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/libraries/social/autobackup/s;->b()V

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/photos/autobackup/ae;->c:Lcom/google/android/gms/photos/autobackup/model/SyncSettings;

    invoke-virtual {v1}, Lcom/google/android/gms/photos/autobackup/model/SyncSettings;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p1, v0}, Lcom/google/android/libraries/social/autobackup/s;->d(Landroid/content/Context;I)V

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/ae;->a:Lcom/google/android/gms/photos/autobackup/a/ac;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/photos/autobackup/a/ac;->d(I)V

    goto :goto_0
.end method
