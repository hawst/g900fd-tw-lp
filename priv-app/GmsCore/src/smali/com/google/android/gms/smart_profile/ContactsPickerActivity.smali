.class public Lcom/google/android/gms/smart_profile/ContactsPickerActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 24
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 26
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/ContactsPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 27
    const-string v1, "com.google.android.gms.people.smart_profile.VIEWER_ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 28
    const-string v2, "com.google.android.gms.people.smart_profile.VIEWER_PAGE_ID"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 29
    const-string v3, "com.google.android.gms.people.smart_profile.QUALIFIED_ID"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 30
    const-string v4, "com.google.android.gms.people.smart_profile.APPLICATION_ID"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 32
    sget v5, Lcom/google/android/gms/l;->fi:I

    invoke-virtual {p0, v5}, Lcom/google/android/gms/smart_profile/ContactsPickerActivity;->setContentView(I)V

    .line 34
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v5

    invoke-virtual {v5}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v5

    .line 35
    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/support/v7/app/a;->b(Z)V

    .line 36
    sget v6, Lcom/google/android/gms/p;->wO:I

    invoke-virtual {v5, v6}, Landroid/support/v7/app/a;->c(I)V

    .line 37
    invoke-virtual {v5}, Landroid/support/v7/app/a;->f()V

    .line 38
    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/smart_profile/bf;->a(Landroid/os/Bundle;)Ljava/lang/Integer;

    move-result-object v0

    .line 39
    if-nez v0, :cond_1

    .line 40
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/ContactsPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget-object v6, Landroid/graphics/drawable/GradientDrawable$Orientation;->BOTTOM_TOP:Landroid/graphics/drawable/GradientDrawable$Orientation;

    invoke-static {v6, v0}, Lcom/google/android/gms/smart_profile/bf;->a(Landroid/graphics/drawable/GradientDrawable$Orientation;Landroid/content/res/Resources;)Landroid/graphics/drawable/GradientDrawable;

    move-result-object v0

    invoke-virtual {v5, v0}, Landroid/support/v7/app/a;->b(Landroid/graphics/drawable/Drawable;)V

    .line 46
    :goto_0
    if-nez p1, :cond_0

    .line 47
    invoke-static {v1, v2, v3, v4}, Lcom/google/android/gms/smart_profile/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/smart_profile/h;

    move-result-object v0

    .line 49
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/ContactsPickerActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    sget v2, Lcom/google/android/gms/j;->gq:I

    const-string v3, "ContactsPickerFragment"

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 53
    :cond_0
    return-void

    .line 43
    :cond_1
    new-instance v6, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v6, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v5, v6}, Landroid/support/v7/app/a;->b(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
