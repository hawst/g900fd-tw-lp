.class final Lcom/google/android/gms/auth/setup/d2d/an;
.super Lcom/google/android/gms/auth/setup/d2d/a/e;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/ab/b/a/e/h;

.field final synthetic b:Lcom/google/android/gms/auth/setup/d2d/am;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/setup/d2d/am;Landroid/bluetooth/BluetoothAdapter;Ljava/util/UUID;Ljava/lang/String;Lcom/google/ab/b/a/e/h;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/android/gms/auth/setup/d2d/an;->b:Lcom/google/android/gms/auth/setup/d2d/am;

    iput-object p5, p0, Lcom/google/android/gms/auth/setup/d2d/an;->a:Lcom/google/ab/b/a/e/h;

    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/gms/auth/setup/d2d/a/e;-><init>(Landroid/bluetooth/BluetoothAdapter;Ljava/util/UUID;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 131
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/am;->a()Lcom/google/android/gms/auth/d/a;

    move-result-object v0

    const-string v1, "Bluetooth connection failed"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/an;->b:Lcom/google/android/gms/auth/setup/d2d/am;

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/am;->b(Lcom/google/android/gms/auth/setup/d2d/am;)Lcom/google/android/gms/auth/setup/d2d/ap;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/auth/setup/d2d/ap;->k()V

    .line 133
    return-void
.end method

.method public final a(Lcom/google/android/gms/auth/setup/d2d/a/b;)V
    .locals 2

    .prologue
    .line 116
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/am;->a()Lcom/google/android/gms/auth/d/a;

    move-result-object v0

    const-string v1, "Bluetooth connection created."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 117
    invoke-static {}, Lcom/google/android/gms/auth/setup/d2d/TargetActivity;->h()Lcom/google/android/gms/auth/setup/d2d/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/auth/setup/d2d/ao;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/setup/d2d/ao;-><init>(Lcom/google/android/gms/auth/setup/d2d/an;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/setup/d2d/a;->a(Lcom/google/android/gms/auth/setup/d2d/c;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/auth/setup/d2d/an;->b:Lcom/google/android/gms/auth/setup/d2d/am;

    invoke-static {v0}, Lcom/google/android/gms/auth/setup/d2d/am;->b(Lcom/google/android/gms/auth/setup/d2d/am;)Lcom/google/android/gms/auth/setup/d2d/ap;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/setup/d2d/an;->a:Lcom/google/ab/b/a/e/h;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/auth/setup/d2d/ap;->a(Lcom/google/android/gms/auth/setup/d2d/a/b;Lcom/google/ab/b/a/e/h;)V

    .line 128
    return-void
.end method
