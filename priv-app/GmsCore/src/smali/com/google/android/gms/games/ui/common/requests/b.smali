.class public final Lcom/google/android/gms/games/ui/common/requests/b;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static varargs a([Lcom/google/android/gms/common/data/d;)[Lcom/google/android/gms/games/request/GameRequest;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 24
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 25
    :goto_0
    array-length v0, p0

    if-ge v1, v0, :cond_2

    .line 26
    aget-object v5, p0, v1

    .line 27
    invoke-interface {v5}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v6

    move v3, v2

    .line 28
    :goto_1
    if-ge v3, v6, :cond_1

    .line 29
    invoke-interface {v5, v3}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    .line 30
    instance-of v7, v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    if-eqz v7, :cond_0

    .line 31
    check-cast v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->d()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 28
    :goto_2
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 33
    :cond_0
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 25
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 39
    :cond_2
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/games/request/GameRequest;

    .line 40
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 41
    return-object v0
.end method
