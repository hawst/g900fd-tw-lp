.class public final Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/c;

.field private b:[B

.field private c:Ljava/security/KeyPair;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/auth/be/proximity/authorization/a/c/c;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 405
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 397
    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->b:[B

    .line 401
    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->c:Ljava/security/KeyPair;

    .line 406
    iput-object p1, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->a:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/c;

    .line 407
    return-void
.end method

.method private static a(Lcom/google/protobuf/a/a;)Ljava/security/PublicKey;
    .locals 1

    .prologue
    .line 507
    invoke-virtual {p0}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/ab/b/a/f/m;->a([B)Lcom/google/ab/b/a/f/m;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ab/b/a/f/d;->a(Lcom/google/ab/b/a/f/m;)Ljava/security/PublicKey;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized b()[B
    .locals 2

    .prologue
    .line 421
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->b:[B

    if-nez v0, :cond_0

    .line 422
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v1, "Missing transport binding data."

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 421
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 424
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->b:[B
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()Ljava/security/KeyPair;
    .locals 1

    .prologue
    .line 410
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->c:Ljava/security/KeyPair;

    if-nez v0, :cond_0

    .line 411
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->a:Lcom/google/android/gms/auth/be/proximity/authorization/a/c/c;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/c;->a()Ljava/security/KeyPair;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->c:Ljava/security/KeyPair;

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->c:Ljava/security/KeyPair;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 410
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a([B)V
    .locals 1

    .prologue
    .line 417
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->b:[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418
    monitor-exit p0

    return-void

    .line 417
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b([B)[B
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 428
    array-length v1, p1

    if-lez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 431
    :try_start_0
    new-instance v0, Lcom/google/ab/b/a/f/h;

    invoke-direct {v0}, Lcom/google/ab/b/a/f/h;-><init>()V

    new-instance v1, Lcom/google/ab/b/a/e/o;

    invoke-direct {v1}, Lcom/google/ab/b/a/e/o;-><init>()V

    sget-object v2, Lcom/google/ab/b/a/e/u;->n:Lcom/google/ab/b/a/e/u;

    invoke-virtual {v2}, Lcom/google/ab/b/a/e/u;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/ab/b/a/e/o;->a(I)Lcom/google/ab/b/a/e/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ab/b/a/e/o;->g()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ab/b/a/f/h;->a([B)Lcom/google/ab/b/a/f/h;

    move-result-object v0

    iput-object p1, v0, Lcom/google/ab/b/a/f/h;->b:[B

    invoke-virtual {p0}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->a()Ljava/security/KeyPair;

    move-result-object v1

    invoke-virtual {v1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v1

    sget-object v2, Lcom/google/ab/b/a/f/c;->b:Lcom/google/ab/b/a/f/c;

    const/4 v3, 0x0

    new-array v3, v3, [B

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/ab/b/a/f/h;->a(Ljava/security/Key;Lcom/google/ab/b/a/f/c;[B)Lcom/google/ab/b/a/f/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ab/b/a/f/q;->g()[B
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 442
    :catch_0
    move-exception v0

    .line 443
    :goto_0
    new-instance v1, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Error when signing the message."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 442
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method public final c([B)[B
    .locals 5

    .prologue
    .line 449
    :try_start_0
    invoke-static {p1}, Lcom/google/ab/b/a/f/q;->a([B)Lcom/google/ab/b/a/f/q;

    move-result-object v0

    .line 450
    iget-boolean v1, v0, Lcom/google/ab/b/a/f/q;->a:Z

    if-nez v1, :cond_0

    .line 451
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v1, "Missing header and body in encrypted message wrapper"

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/security/SignatureException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_4

    .line 497
    :catch_0
    move-exception v0

    .line 499
    :goto_0
    new-instance v1, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Error when decrypting the message."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 454
    :cond_0
    :try_start_1
    iget-boolean v1, v0, Lcom/google/ab/b/a/f/q;->c:Z

    if-nez v1, :cond_1

    .line 455
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v1, "Missing transport binding signature"

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 497
    :catch_1
    move-exception v0

    goto :goto_0

    .line 457
    :cond_1
    iget-object v1, v0, Lcom/google/ab/b/a/f/q;->d:Lcom/google/protobuf/a/a;

    invoke-virtual {v1}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v1

    .line 459
    iget-object v0, v0, Lcom/google/ab/b/a/f/q;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/ab/b/a/f/q;->a([B)Lcom/google/ab/b/a/f/q;

    move-result-object v0

    .line 461
    invoke-static {v0}, Lcom/google/ab/b/a/f/i;->a(Lcom/google/ab/b/a/f/q;)Lcom/google/ab/b/a/f/n;

    move-result-object v2

    .line 463
    iget-boolean v3, v2, Lcom/google/ab/b/a/f/n;->g:Z

    if-nez v3, :cond_2

    .line 464
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v1, "Missing decryptionKeyId in encrypted message"

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 497
    :catch_2
    move-exception v0

    goto :goto_0

    .line 467
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->a()Ljava/security/KeyPair;

    move-result-object v3

    invoke-virtual {v3}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v3

    iget-object v2, v2, Lcom/google/ab/b/a/f/n;->h:Lcom/google/protobuf/a/a;

    invoke-static {v2}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->a(Lcom/google/protobuf/a/a;)Ljava/security/PublicKey;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/ab/b/a/e/i;->a(Ljava/security/PrivateKey;Ljava/security/PublicKey;)Ljavax/crypto/SecretKey;

    move-result-object v2

    .line 470
    sget-object v3, Lcom/google/ab/b/a/f/c;->a:Lcom/google/ab/b/a/f/c;

    sget-object v4, Lcom/google/ab/b/a/f/b;->b:Lcom/google/ab/b/a/f/b;

    invoke-static {v0, v2, v3, v2, v4}, Lcom/google/ab/b/a/f/i;->a(Lcom/google/ab/b/a/f/q;Ljava/security/Key;Lcom/google/ab/b/a/f/c;Ljava/security/Key;Lcom/google/ab/b/a/f/b;)Lcom/google/ab/b/a/f/o;

    move-result-object v0

    .line 476
    iget-boolean v2, v0, Lcom/google/ab/b/a/f/o;->c:Z

    if-nez v2, :cond_3

    .line 477
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v1, "Missing body in encrypted message"

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 497
    :catch_3
    move-exception v0

    goto :goto_0

    .line 479
    :cond_3
    iget-object v0, v0, Lcom/google/ab/b/a/f/o;->d:Lcom/google/protobuf/a/a;

    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/ab/b/a/f/o;->a([B)Lcom/google/ab/b/a/f/o;

    move-result-object v0

    .line 481
    iget-boolean v2, v0, Lcom/google/ab/b/a/f/o;->a:Z

    if-eqz v2, :cond_4

    iget-boolean v2, v0, Lcom/google/ab/b/a/f/o;->c:Z

    if-nez v2, :cond_5

    .line 482
    :cond_4
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v1, "Missing header or body in decrypted message"

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/security/SignatureException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Lcom/google/protobuf/a/e; {:try_start_1 .. :try_end_1} :catch_4

    .line 500
    :catch_4
    move-exception v0

    .line 501
    new-instance v1, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v2, "Error when parsing encrypted message."

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 484
    :cond_5
    :try_start_2
    iget-object v2, v0, Lcom/google/ab/b/a/f/o;->b:Lcom/google/ab/b/a/f/n;

    .line 485
    iget-boolean v3, v2, Lcom/google/ab/b/a/f/n;->e:Z

    if-nez v3, :cond_6

    .line 486
    new-instance v0, Lcom/google/android/gms/auth/firstparty/proximity/g;

    const-string v1, "Missing verification key in decrypted message"

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/firstparty/proximity/g;-><init>(Ljava/lang/String;)V

    throw v0

    .line 489
    :cond_6
    iget-object v2, v2, Lcom/google/ab/b/a/f/n;->f:Lcom/google/protobuf/a/a;

    invoke-static {v2}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->a(Lcom/google/protobuf/a/a;)Ljava/security/PublicKey;

    move-result-object v2

    .line 491
    invoke-static {v1}, Lcom/google/ab/b/a/f/q;->a([B)Lcom/google/ab/b/a/f/q;

    move-result-object v1

    sget-object v3, Lcom/google/ab/b/a/f/c;->c:Lcom/google/ab/b/a/f/c;

    invoke-direct {p0}, Lcom/google/android/gms/auth/be/proximity/authorization/a/c/d;->b()[B

    move-result-object v4

    invoke-static {v1, v2, v3, v4}, Lcom/google/ab/b/a/f/i;->a(Lcom/google/ab/b/a/f/q;Ljava/security/Key;Lcom/google/ab/b/a/f/c;[B)Lcom/google/ab/b/a/f/o;

    .line 496
    iget-object v0, v0, Lcom/google/ab/b/a/f/o;->d:Lcom/google/protobuf/a/a;

    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->b()[B
    :try_end_2
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/security/SignatureException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/google/protobuf/a/e; {:try_start_2 .. :try_end_2} :catch_4

    move-result-object v0

    return-object v0
.end method
