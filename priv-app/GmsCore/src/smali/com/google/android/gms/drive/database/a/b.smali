.class public final Lcom/google/android/gms/drive/database/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/database/a/c;


# static fields
.field private static final a:Lcom/google/android/gms/drive/database/a/b;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 15
    new-instance v0, Lcom/google/android/gms/drive/database/a/b;

    invoke-direct {v0}, Lcom/google/android/gms/drive/database/a/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/database/a/b;->a:Lcom/google/android/gms/drive/database/a/b;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    return-void
.end method

.method public static a()Lcom/google/android/gms/drive/database/a/b;
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/google/android/gms/drive/database/a/b;->a:Lcom/google/android/gms/drive/database/a/b;

    return-object v0
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 14
    new-instance v1, Lcom/google/android/gms/drive/database/model/y;

    sget-object v0, Lcom/google/android/gms/drive/database/model/aa;->a:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;

    new-instance v0, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    sget-object v2, Lcom/google/android/gms/drive/database/model/aa;->c:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    sget-object v5, Lcom/google/android/gms/drive/database/model/aa;->d:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v5

    invoke-virtual {v5, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->intValue()I

    move-result v5

    invoke-direct {v0, v2, v5}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    sget-object v2, Lcom/google/android/gms/drive/database/model/aa;->e:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v2

    invoke-virtual {v2, p2}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v0, v2}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;-><init>(Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/drive/database/model/aa;->b:Lcom/google/android/gms/drive/database/model/aa;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/aa;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/drive/database/model/ab;->b(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v5

    invoke-static {}, Lcom/google/android/gms/drive/database/model/z;->a()Lcom/google/android/gms/drive/database/model/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/z;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {p2, v0}, Lcom/google/android/gms/drive/database/model/ab;->a(Landroid/database/Cursor;Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-object v2, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/drive/database/model/y;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/metadata/internal/CustomProperty;Ljava/lang/Long;J)V

    return-object v1
.end method
