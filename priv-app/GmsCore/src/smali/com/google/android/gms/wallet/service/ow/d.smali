.class public final Lcom/google/android/gms/wallet/service/ow/d;
.super Lcom/google/android/gms/wallet/service/ow/w;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/common/util/ap;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/wallet/service/ow/w;

.field private final d:Lcom/google/android/gms/wallet/service/r;

.field private final e:Lcom/google/android/gms/wallet/cache/j;

.field private final f:Lcom/google/android/gms/wallet/cache/h;

.field private final g:Lcom/google/android/gms/wallet/cache/f;

.field private final h:Lcom/google/android/gms/wallet/service/ow/k;

.field private final i:Lcom/google/android/gms/wallet/service/ow/a;

.field private final j:Lcom/google/android/gms/wallet/service/ow/ar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/e;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/service/ow/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/service/ow/d;->a:Lcom/google/android/gms/common/util/ap;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/w;Lcom/google/android/gms/wallet/service/r;Lcom/google/android/gms/wallet/cache/j;Lcom/google/android/gms/wallet/cache/h;Lcom/google/android/gms/wallet/cache/f;Lcom/google/android/gms/wallet/service/ow/a;Lcom/google/android/gms/wallet/service/ow/ar;Lcom/google/android/gms/wallet/service/ow/k;)V
    .locals 0

    .prologue
    .line 84
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ow/w;-><init>()V

    .line 85
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/d;->b:Landroid/content/Context;

    .line 86
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/d;->c:Lcom/google/android/gms/wallet/service/ow/w;

    .line 87
    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ow/d;->d:Lcom/google/android/gms/wallet/service/r;

    .line 88
    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ow/d;->e:Lcom/google/android/gms/wallet/cache/j;

    .line 89
    iput-object p5, p0, Lcom/google/android/gms/wallet/service/ow/d;->f:Lcom/google/android/gms/wallet/cache/h;

    .line 90
    iput-object p6, p0, Lcom/google/android/gms/wallet/service/ow/d;->g:Lcom/google/android/gms/wallet/cache/f;

    .line 91
    iput-object p9, p0, Lcom/google/android/gms/wallet/service/ow/d;->h:Lcom/google/android/gms/wallet/service/ow/k;

    .line 92
    iput-object p7, p0, Lcom/google/android/gms/wallet/service/ow/d;->i:Lcom/google/android/gms/wallet/service/ow/a;

    .line 93
    iput-object p8, p0, Lcom/google/android/gms/wallet/service/ow/d;->j:Lcom/google/android/gms/wallet/service/ow/ar;

    .line 94
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
    .locals 9

    .prologue
    .line 522
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/d;->c:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    move-result-object v7

    .line 523
    invoke-virtual {v7}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->b()Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    .line 524
    instance-of v1, v0, Lcom/google/aa/b/a/a/a/a/j;

    if-eqz v1, :cond_1

    .line 525
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v8

    .line 526
    check-cast v0, Lcom/google/aa/b/a/a/a/a/j;

    .line 529
    iget-object v1, v0, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v1, :cond_1

    .line 531
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v2

    .line 532
    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    invoke-static {v0}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/p;)Lcom/google/aa/a/a/a/f;

    move-result-object v3

    .line 535
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/d;->e:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v1

    iget-object v4, v8, Lcom/google/aa/b/a/a/a/a/i;->c:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_2

    const/4 v4, 0x1

    :goto_0
    iget-boolean v5, v8, Lcom/google/aa/b/a/a/a/a/i;->i:Z

    iget-boolean v6, v8, Lcom/google/aa/b/a/a/a/a/i;->h:Z

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/cache/j;Landroid/accounts/Account;ILcom/google/aa/a/a/a/f;ZZZ)V

    .line 543
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/d;->g:Lcom/google/android/gms/wallet/cache/f;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/cache/f;Ljava/lang/String;ILcom/google/aa/a/a/a/f;)V

    .line 548
    iget-boolean v0, v8, Lcom/google/aa/b/a/a/a/a/i;->t:Z

    if-eqz v0, :cond_0

    .line 549
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/d;->f:Lcom/google/android/gms/wallet/cache/h;

    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v4, v2, v3}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/cache/h;Landroid/accounts/Account;Ljava/lang/String;ILcom/google/aa/a/a/a/f;)V

    .line 556
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 558
    invoke-static {v8}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/i;)Lcom/google/aa/b/a/a/a/a/u;

    move-result-object v1

    .line 560
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/d;->d:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/gms/wallet/service/r;->b(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    .line 563
    :cond_1
    return-object v7

    .line 535
    :cond_2
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 5

    .prologue
    .line 569
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;->c()Lcom/google/aa/b/a/a/a/a/c;

    move-result-object v0

    .line 570
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/d;->c:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v1, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v1

    .line 571
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v2

    .line 572
    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ow/d;->d:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    .line 573
    return-object v1
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 585
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/d;->c:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 1

    .prologue
    .line 579
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/d;->c:Lcom/google/android/gms/wallet/service/ow/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 24

    .prologue
    .line 278
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/k;

    move-result-object v18

    .line 280
    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    move-object/from16 v19, v0

    .line 282
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    .line 286
    move-object/from16 v0, v18

    iget-boolean v3, v0, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    .line 287
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_a

    const/4 v2, 0x1

    .line 290
    :goto_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/wallet/service/ow/d;->d:Lcom/google/android/gms/wallet/service/r;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v4

    .line 294
    if-nez v3, :cond_0

    if-nez v2, :cond_0

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v2, :cond_0

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    if-nez v2, :cond_0

    if-nez v4, :cond_b

    .line 297
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->c:Lcom/google/android/gms/wallet/service/ow/w;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v3

    .line 298
    invoke-virtual {v3}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v2

    .line 299
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/wallet/service/ow/d;->d:Lcom/google/android/gms/wallet/service/r;

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    move-object/from16 v17, v3

    .line 360
    :goto_1
    instance-of v3, v2, Lcom/google/aa/b/a/a/a/a/l;

    if-eqz v3, :cond_c

    move-object/from16 v16, v2

    .line 361
    check-cast v16, Lcom/google/aa/b/a/a/a/a/l;

    .line 363
    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    move-object/from16 v21, v0

    .line 364
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v22

    .line 366
    if-eqz v21, :cond_7

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    if-eqz v2, :cond_7

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    array-length v2, v2

    if-nez v2, :cond_7

    .line 369
    move-object/from16 v0, v21

    iget-object v13, v0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    .line 370
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->j:Lcom/google/android/gms/wallet/service/ow/ar;

    invoke-virtual {v2, v13}, Lcom/google/android/gms/wallet/service/ow/ar;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/service/ow/as;

    move-result-object v2

    .line 372
    if-eqz v2, :cond_19

    iget-boolean v2, v2, Lcom/google/android/gms/wallet/service/ow/as;->i:Z

    move v10, v2

    .line 374
    :goto_2
    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    iget-object v3, v2, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    .line 375
    move-object/from16 v0, v16

    iget-boolean v0, v0, Lcom/google/aa/b/a/a/a/a/l;->e:Z

    move/from16 v23, v0

    .line 376
    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_1a

    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v4, v2, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    .line 378
    :goto_3
    move-object/from16 v0, v19

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v5

    move/from16 v0, v23

    invoke-static {v2, v13, v5, v0}, Lcom/google/android/gms/wallet/service/ow/ar;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/aa/a/a/a/d;

    move-result-object v12

    .line 382
    new-instance v2, Lcom/google/android/gms/wallet/service/ow/as;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/k;

    move-result-object v6

    iget-object v6, v6, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->d()Z

    move-result v7

    move-object/from16 v0, v18

    iget-boolean v8, v0, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    move-object/from16 v0, v18

    iget-object v9, v0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    if-nez v10, :cond_1

    move-object/from16 v0, v18

    iget-object v10, v0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    if-eqz v10, :cond_1b

    :cond_1
    const/4 v10, 0x1

    :goto_4
    const/4 v11, 0x0

    invoke-direct/range {v2 .. v12}, Lcom/google/android/gms/wallet/service/ow/as;-><init>(Ljava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/u;ZZ[Ljava/lang/String;ZZLcom/google/aa/a/a/a/d;)V

    .line 394
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/wallet/service/ow/d;->j:Lcom/google/android/gms/wallet/service/ow/ar;

    invoke-virtual {v5, v13, v2}, Lcom/google/android/gms/wallet/service/ow/ar;->a(Ljava/lang/String;Lcom/google/android/gms/wallet/service/ow/as;)V

    .line 398
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    if-eqz v5, :cond_3

    .line 399
    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    invoke-static {v5}, Lcom/google/android/gms/wallet/common/z;->a(Lcom/google/aa/b/a/a/a/a/w;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v5

    .line 402
    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/google/checkout/inapp/proto/j;->f:Z

    .line 403
    new-instance v6, Lcom/google/aa/a/a/a/b;

    invoke-direct {v6}, Lcom/google/aa/a/a/a/b;-><init>()V

    .line 404
    const/4 v7, 0x1

    new-array v7, v7, [Lcom/google/checkout/inapp/proto/j;

    const/4 v8, 0x0

    aput-object v5, v7, v8

    iput-object v7, v6, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    .line 405
    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v5, :cond_2

    .line 406
    iget-object v5, v6, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    move-object/from16 v0, v21

    iget-object v7, v0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v5, v7}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Lcom/google/checkout/inapp/proto/a/b;

    iput-object v5, v6, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    .line 409
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/wallet/service/ow/d;->e:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v8

    invoke-virtual {v5, v7, v8, v6}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;ILcom/google/aa/a/a/a/b;)V

    .line 414
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/wallet/service/ow/d;->i:Lcom/google/android/gms/wallet/service/ow/a;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/wallet/service/ow/a;->a(Ljava/lang/String;)Lcom/google/aa/b/a/a/a/a/b;

    move-result-object v5

    .line 416
    if-eqz v5, :cond_4

    .line 418
    move-object/from16 v0, v19

    iput-object v5, v0, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    .line 420
    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    move-object/from16 v0, v19

    iput-object v5, v0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    .line 422
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/wallet/service/ow/d;->b:Landroid/content/Context;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->c()Lcom/google/android/gms/wallet/Cart;

    move-result-object v9

    iget-object v10, v2, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    iget-object v11, v2, Lcom/google/android/gms/wallet/service/ow/as;->c:Ljava/lang/String;

    iget-boolean v12, v2, Lcom/google/android/gms/wallet/service/ow/as;->i:Z

    iget-boolean v13, v2, Lcom/google/android/gms/wallet/service/ow/as;->j:Z

    iget-object v6, v2, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    iget-boolean v15, v6, Lcom/google/aa/a/a/a/d;->d:Z

    move-object/from16 v6, p0

    move-object/from16 v8, v19

    move-object/from16 v14, p1

    invoke-static/range {v5 .. v15}, Lcom/google/android/gms/wallet/service/ow/j;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/w;Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/u;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/gms/wallet/shared/BuyFlowConfig;Z)Lcom/google/android/gms/wallet/service/ow/j;

    move-result-object v5

    .line 428
    new-instance v6, Lcom/google/android/gms/wallet/service/ow/l;

    iget-object v7, v2, Lcom/google/android/gms/wallet/service/ow/as;->b:Ljava/lang/String;

    iget-object v8, v2, Lcom/google/android/gms/wallet/service/ow/as;->k:Lcom/google/aa/a/a/a/d;

    iget-boolean v8, v8, Lcom/google/aa/a/a/a/d;->d:Z

    iget-object v2, v2, Lcom/google/android/gms/wallet/service/ow/as;->c:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v6, v0, v7, v8, v2}, Lcom/google/android/gms/wallet/service/ow/l;-><init>(Lcom/google/aa/b/a/a/a/a/u;Ljava/lang/String;ZLjava/lang/String;)V

    .line 431
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->h:Lcom/google/android/gms/wallet/service/ow/k;

    invoke-virtual {v2, v6, v5}, Lcom/google/android/gms/wallet/service/ow/k;->a(Lcom/google/android/gms/wallet/service/ow/l;Lcom/google/android/gms/wallet/service/ow/j;)V

    .line 432
    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-static {v5, v2}, Lcom/google/android/gms/common/util/e;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 436
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->f:Lcom/google/android/gms/wallet/cache/h;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v6

    move/from16 v0, v22

    invoke-virtual {v2, v0, v5, v6}, Lcom/google/android/gms/wallet/cache/h;->a(ILandroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/i;

    move-result-object v2

    .line 438
    iput-object v3, v2, Lcom/google/android/gms/wallet/cache/i;->d:Ljava/lang/String;

    .line 440
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_5

    .line 441
    iput-object v4, v2, Lcom/google/android/gms/wallet/cache/i;->f:Ljava/lang/String;

    .line 443
    :cond_5
    move/from16 v0, v23

    iput-boolean v0, v2, Lcom/google/android/gms/wallet/cache/i;->e:Z

    .line 444
    move-object/from16 v0, v18

    iget-boolean v3, v0, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    if-eqz v3, :cond_6

    .line 445
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/ow/d;->b:Landroid/content/Context;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_6

    invoke-virtual {v3}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/16 v5, 0x40

    :try_start_0
    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget-object v4, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    array-length v4, v4

    if-lez v4, :cond_6

    iget-object v3, v3, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    const-string v4, "SHA-256"

    invoke-static {v3, v4}, Lcom/google/android/gms/wallet/service/a;->a(Landroid/content/pm/Signature;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/wallet/cache/i;->a:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 447
    :cond_6
    :goto_5
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/ow/d;->f:Lcom/google/android/gms/wallet/cache/h;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v5

    move/from16 v0, v22

    invoke-virtual {v3, v0, v4, v5, v2}, Lcom/google/android/gms/wallet/cache/h;->a(ILandroid/accounts/Account;Ljava/lang/String;Lcom/google/android/gms/wallet/cache/i;)V

    .line 451
    :cond_7
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v2, :cond_9

    .line 453
    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    invoke-static {v2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/p;)Lcom/google/aa/a/a/a/f;

    move-result-object v5

    .line 456
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->e:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    move-object/from16 v0, v19

    iget-boolean v6, v0, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    move-object/from16 v0, v19

    iget-boolean v7, v0, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    move-object/from16 v0, v19

    iget-boolean v8, v0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    move/from16 v4, v22

    invoke-static/range {v2 .. v8}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/cache/j;Landroid/accounts/Account;ILcom/google/aa/a/a/a/f;ZZZ)V

    .line 464
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->g:Lcom/google/android/gms/wallet/cache/f;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v3

    move/from16 v0, v22

    invoke-static {v2, v3, v0, v5}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/cache/f;Ljava/lang/String;ILcom/google/aa/a/a/a/f;)V

    .line 469
    move-object/from16 v0, v19

    iget-boolean v2, v0, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    if-eqz v2, :cond_8

    .line 470
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->f:Lcom/google/android/gms/wallet/cache/h;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v4

    move/from16 v0, v22

    invoke-static {v2, v3, v4, v0, v5}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/cache/h;Landroid/accounts/Account;Ljava/lang/String;ILcom/google/aa/a/a/a/f;)V

    .line 479
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->d:Lcom/google/android/gms/wallet/service/r;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v2, v0, v1, v5}, Lcom/google/android/gms/wallet/service/r;->b(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    .line 482
    :cond_9
    invoke-static {}, Lcom/google/aa/b/a/i;->a()[Lcom/google/aa/b/a/i;

    move-result-object v2

    move-object/from16 v0, v21

    iput-object v2, v0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    .line 483
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1e

    .line 484
    const/4 v2, 0x0

    .line 485
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->d()Ljava/lang/String;

    move-result-object v3

    const-string v4, "dont_send_loyalty_wob_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1c

    .line 487
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    array-length v5, v3

    .line 488
    const/4 v3, 0x0

    :goto_6
    if-ge v3, v5, :cond_1c

    .line 489
    move-object/from16 v0, v21

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    aget-object v2, v2, v3

    .line 490
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v6, v2, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    invoke-static {v4, v6}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1c

    .line 492
    const/4 v4, 0x0

    .line 488
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move-object v2, v4

    goto :goto_6

    .line 287
    :cond_a
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 302
    :cond_b
    invoke-virtual {v4}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/aa/a/a/a/f;

    .line 304
    iget-object v3, v2, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/k;->c:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/j;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v6

    .line 307
    if-nez v6, :cond_d

    .line 308
    const-string v2, "CachedOwInternalService"

    const-string v3, "Could not find selected instrument in cached wallet items."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 309
    sget-object v17, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    .line 516
    :cond_c
    :goto_7
    return-object v17

    .line 312
    :cond_d
    const/4 v3, 0x0

    .line 313
    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1f

    .line 314
    iget-object v3, v2, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    move-object/from16 v0, v18

    iget-object v4, v0, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/gms/wallet/common/w;->a([Lcom/google/checkout/inapp/proto/a/b;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v3

    .line 317
    if-nez v3, :cond_e

    .line 318
    const-string v2, "CachedOwInternalService"

    const-string v3, "Could not find selected address in cached wallet items."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 319
    sget-object v17, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->d:Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    goto :goto_7

    :cond_e
    move-object v5, v3

    .line 323
    :goto_8
    if-nez v6, :cond_12

    const/4 v3, 0x0

    .line 328
    :goto_9
    new-instance v4, Lcom/google/aa/b/a/a/a/a/v;

    invoke-direct {v4}, Lcom/google/aa/b/a/a/a/a/v;-><init>()V

    .line 330
    move-object/from16 v0, v18

    iget-object v7, v0, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    iget-object v7, v7, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    iput-object v7, v4, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    .line 332
    iput-object v3, v4, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    .line 333
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/ow/d;->b:Landroid/content/Context;

    move-object/from16 v0, v18

    iget-boolean v7, v0, Lcom/google/aa/b/a/a/a/a/k;->e:Z

    invoke-static {v3, v6, v7}, Lcom/google/android/gms/wallet/common/w;->a(Landroid/content/Context;Lcom/google/checkout/inapp/proto/j;Z)[Ljava/lang/String;

    move-result-object v3

    iput-object v3, v4, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    .line 336
    const/4 v3, 0x1

    new-array v3, v3, [Lcom/google/aa/b/a/g;

    const/4 v7, 0x0

    invoke-static {v6}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/j;)Lcom/google/aa/b/a/g;

    move-result-object v6

    aput-object v6, v3, v7

    iput-object v3, v4, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    .line 339
    iget-object v2, v2, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    .line 340
    iget-object v3, v2, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    array-length v3, v3

    if-lez v3, :cond_f

    .line 341
    iget-object v3, v2, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    iput-object v3, v4, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    .line 343
    :cond_f
    iget-object v3, v2, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    array-length v3, v3

    if-lez v3, :cond_10

    .line 344
    iget-object v2, v2, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    iput-object v2, v4, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    .line 346
    :cond_10
    if-eqz v5, :cond_11

    .line 347
    iput-object v5, v4, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    .line 350
    :cond_11
    new-instance v2, Lcom/google/aa/b/a/a/a/a/l;

    invoke-direct {v2}, Lcom/google/aa/b/a/a/a/a/l;-><init>()V

    .line 352
    iput-object v4, v2, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    .line 353
    move-object/from16 v0, v18

    iget-boolean v3, v0, Lcom/google/aa/b/a/a/a/a/k;->e:Z

    iput-boolean v3, v2, Lcom/google/aa/b/a/a/a/a/l;->e:Z

    .line 356
    new-instance v3, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v4, 0xe

    invoke-direct {v3, v4, v2}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    move-object/from16 v17, v3

    goto/16 :goto_1

    .line 323
    :cond_12
    new-instance v3, Lcom/google/aa/b/a/a/a/a/w;

    invoke-direct {v3}, Lcom/google/aa/b/a/a/a/a/w;-><init>()V

    iget-object v4, v6, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v4, :cond_13

    iget-object v4, v6, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v4, v3, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    :cond_13
    iget-object v4, v6, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_14

    iget-object v4, v6, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    :cond_14
    iget-object v4, v6, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_15

    iget-object v4, v6, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    :cond_15
    iget-object v4, v6, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_16

    iget-object v4, v6, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    :cond_16
    iget-object v4, v6, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_17

    iget-object v4, v6, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    :cond_17
    iget v4, v6, Lcom/google/checkout/inapp/proto/j;->h:I

    packed-switch v4, :pswitch_data_0

    :goto_a
    iget-object v7, v6, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v8, v7

    const/4 v4, 0x0

    :goto_b
    if-ge v4, v8, :cond_18

    aget v9, v7, v4

    packed-switch v9, :pswitch_data_1

    :pswitch_0
    const/4 v9, 0x7

    iput v9, v3, Lcom/google/aa/b/a/a/a/a/w;->i:I

    :goto_c
    add-int/lit8 v4, v4, 0x1

    goto :goto_b

    :pswitch_1
    const/4 v4, 0x2

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->i:I

    goto :goto_a

    :pswitch_2
    const/4 v4, 0x3

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->i:I

    goto :goto_a

    :pswitch_3
    const/4 v4, 0x7

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->i:I

    goto :goto_a

    :pswitch_4
    const/4 v9, 0x7

    iput v9, v3, Lcom/google/aa/b/a/a/a/a/w;->i:I

    goto :goto_c

    :pswitch_5
    const/4 v9, 0x5

    iput v9, v3, Lcom/google/aa/b/a/a/a/a/w;->i:I

    goto :goto_c

    :pswitch_6
    const/4 v9, 0x7

    iput v9, v3, Lcom/google/aa/b/a/a/a/a/w;->i:I

    goto :goto_c

    :pswitch_7
    const/4 v9, 0x4

    iput v9, v3, Lcom/google/aa/b/a/a/a/a/w;->i:I

    goto :goto_c

    :pswitch_8
    const/4 v9, 0x4

    iput v9, v3, Lcom/google/aa/b/a/a/a/a/w;->i:I

    goto :goto_c

    :cond_18
    iget v4, v6, Lcom/google/checkout/inapp/proto/j;->c:I

    sparse-switch v4, :sswitch_data_0

    const/4 v4, 0x0

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->b:I

    :goto_d
    iget v4, v6, Lcom/google/checkout/inapp/proto/j;->l:I

    packed-switch v4, :pswitch_data_2

    const/4 v4, 0x0

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->l:I

    goto/16 :goto_9

    :sswitch_0
    const/4 v4, 0x3

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->b:I

    goto :goto_d

    :sswitch_1
    const/4 v4, 0x4

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->b:I

    goto :goto_d

    :sswitch_2
    const/4 v4, 0x6

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->b:I

    goto :goto_d

    :sswitch_3
    const/4 v4, 0x2

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->b:I

    goto :goto_d

    :sswitch_4
    const/4 v4, 0x5

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->b:I

    goto :goto_d

    :sswitch_5
    const/4 v4, 0x7

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->b:I

    goto :goto_d

    :sswitch_6
    const/4 v4, 0x0

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->b:I

    goto :goto_d

    :sswitch_7
    const/4 v4, 0x1

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->b:I

    goto :goto_d

    :sswitch_8
    const/16 v4, 0x8

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->b:I

    goto :goto_d

    :pswitch_9
    const/4 v4, 0x1

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->l:I

    goto/16 :goto_9

    :pswitch_a
    const/4 v4, 0x2

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->l:I

    goto/16 :goto_9

    :pswitch_b
    const/4 v4, 0x3

    iput v4, v3, Lcom/google/aa/b/a/a/a/a/w;->l:I

    goto/16 :goto_9

    .line 372
    :cond_19
    const/4 v2, 0x0

    move v10, v2

    goto/16 :goto_2

    .line 376
    :cond_1a
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 382
    :cond_1b
    const/4 v10, 0x0

    goto/16 :goto_4

    .line 498
    :cond_1c
    invoke-static {}, Lcom/google/aa/b/a/h;->a()[Lcom/google/aa/b/a/h;

    move-result-object v3

    move-object/from16 v0, v21

    iput-object v3, v0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    .line 499
    if-eqz v2, :cond_1d

    .line 500
    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    invoke-static {v3, v2}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/aa/b/a/h;

    move-object/from16 v0, v21

    iput-object v2, v0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    .line 506
    :cond_1d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->f:Lcom/google/android/gms/wallet/cache/h;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v4

    move/from16 v0, v22

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/wallet/cache/h;->a(ILandroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/i;

    move-result-object v2

    .line 508
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->d()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/wallet/cache/i;->b:Ljava/lang/String;

    .line 509
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/ow/d;->f:Lcom/google/android/gms/wallet/cache/h;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v5

    move/from16 v0, v22

    invoke-virtual {v3, v0, v4, v5, v2}, Lcom/google/android/gms/wallet/cache/h;->a(ILandroid/accounts/Account;Ljava/lang/String;Lcom/google/android/gms/wallet/cache/i;)V

    goto/16 :goto_7

    .line 513
    :cond_1e
    invoke-static {}, Lcom/google/aa/b/a/h;->a()[Lcom/google/aa/b/a/h;

    move-result-object v2

    move-object/from16 v0, v21

    iput-object v2, v0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    goto/16 :goto_7

    :catch_0
    move-exception v3

    goto/16 :goto_5

    :cond_1f
    move-object v5, v3

    goto/16 :goto_8

    .line 323
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_6
        0x1 -> :sswitch_7
        0x2 -> :sswitch_3
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x5 -> :sswitch_4
        0x6 -> :sswitch_2
        0x7 -> :sswitch_5
        0x1b -> :sswitch_8
    .end sparse-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    .locals 18

    .prologue
    .line 99
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/o;

    move-result-object v2

    .line 100
    iget-object v9, v2, Lcom/google/aa/b/a/a/a/a/o;->a:Lcom/google/aa/b/a/a/a/a/u;

    .line 101
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->b()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/wallet/service/r;->a(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 103
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->d:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {v2, v3, v9}, Lcom/google/android/gms/wallet/service/r;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v2

    .line 105
    if-eqz v2, :cond_0

    .line 272
    :goto_0
    return-object v2

    .line 108
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->d()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b()I

    move-result v4

    .line 109
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->f:Lcom/google/android/gms/wallet/cache/h;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v4, v5, v6}, Lcom/google/android/gms/wallet/cache/h;->a(ILandroid/accounts/Account;Ljava/lang/String;)Lcom/google/android/gms/wallet/cache/i;

    move-result-object v11

    .line 111
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->b:Landroid/content/Context;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v11, v2, v5}, Lcom/google/android/gms/wallet/cache/i;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v6

    .line 113
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->g:Lcom/google/android/gms/wallet/cache/f;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/wallet/cache/f;->a(ILjava/lang/String;)Lcom/google/android/gms/wallet/cache/g;

    move-result-object v10

    .line 115
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->c()Z

    move-result v2

    if-eqz v2, :cond_e

    .line 116
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->e:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v5

    invoke-virtual {v2, v5, v4}, Lcom/google/android/gms/wallet/cache/j;->a(Landroid/accounts/Account;I)Lcom/google/aa/a/a/a/b;

    move-result-object v5

    .line 117
    if-eqz v5, :cond_d

    .line 120
    new-instance v7, Lcom/google/aa/a/a/a/f;

    invoke-direct {v7}, Lcom/google/aa/a/a/a/f;-><init>()V

    .line 121
    const/4 v2, 0x2

    iput v2, v7, Lcom/google/aa/a/a/a/f;->d:I

    .line 122
    iget-object v2, v5, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    iget-object v3, v5, Lcom/google/aa/a/a/a/b;->a:[Lcom/google/checkout/inapp/proto/j;

    sget-object v4, Lcom/google/android/gms/wallet/service/ow/d;->a:Lcom/google/android/gms/common/util/ap;

    invoke-static {v3, v4}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;Lcom/google/android/gms/common/util/ap;)I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/checkout/inapp/proto/j;

    iput-object v2, v7, Lcom/google/aa/a/a/a/f;->b:[Lcom/google/checkout/inapp/proto/j;

    .line 125
    iget-object v2, v5, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v8, v2

    .line 126
    new-array v2, v8, [Lcom/google/checkout/inapp/proto/a/b;

    iput-object v2, v7, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    .line 127
    const/4 v2, 0x0

    move v4, v2

    :goto_1
    if-ge v4, v8, :cond_4

    .line 129
    iget-object v2, v5, Lcom/google/aa/a/a/a/b;->b:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v12, v2, v4

    .line 130
    iget-object v13, v9, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    .line 132
    iget-boolean v2, v12, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    if-eqz v2, :cond_2

    .line 133
    const/4 v2, 0x0

    .line 134
    iget-object v3, v12, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v3, :cond_1

    .line 135
    iget-object v3, v12, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v14, v3, Lcom/google/t/a/b;->a:Ljava/lang/String;

    .line 136
    const/4 v3, 0x0

    array-length v15, v13

    .line 137
    :goto_2
    if-ge v3, v15, :cond_1

    .line 138
    aget-object v16, v13, v3

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/x;->a:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v0, v14}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v16

    if-eqz v16, :cond_3

    .line 140
    const/4 v2, 0x1

    .line 146
    :cond_1
    if-nez v2, :cond_2

    .line 147
    const/4 v2, 0x0

    iput-boolean v2, v12, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    .line 148
    iget-object v2, v12, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/h;->b([II)[I

    move-result-object v2

    iput-object v2, v12, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    .line 153
    :cond_2
    iget-object v2, v7, Lcom/google/aa/a/a/a/f;->c:[Lcom/google/checkout/inapp/proto/a/b;

    aput-object v12, v2, v4

    .line 127
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 137
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 157
    :cond_4
    new-instance v8, Lcom/google/aa/b/a/a/a/a/p;

    invoke-direct {v8}, Lcom/google/aa/b/a/a/a/a/p;-><init>()V

    .line 158
    iget-object v2, v9, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    .line 159
    iget-object v2, v9, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    iput-object v2, v8, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    .line 163
    :cond_5
    :goto_3
    iget-boolean v2, v10, Lcom/google/android/gms/wallet/cache/g;->b:Z

    iput-boolean v2, v8, Lcom/google/aa/b/a/a/a/a/p;->i:Z

    .line 164
    iget-object v2, v11, Lcom/google/android/gms/wallet/cache/i;->c:[Lcom/google/aa/b/a/h;

    if-eqz v2, :cond_9

    iget-object v2, v11, Lcom/google/android/gms/wallet/cache/i;->c:[Lcom/google/aa/b/a/h;

    array-length v2, v2

    if-lez v2, :cond_9

    .line 168
    iget-object v2, v11, Lcom/google/android/gms/wallet/cache/i;->b:Ljava/lang/String;

    const-string v3, "dont_send_loyalty_wob_id"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v12

    .line 171
    iget-object v2, v11, Lcom/google/android/gms/wallet/cache/i;->c:[Lcom/google/aa/b/a/h;

    array-length v13, v2

    .line 172
    const/4 v2, 0x0

    .line 173
    add-int/lit8 v3, v13, 0x1

    new-array v3, v3, [Lcom/google/aa/b/a/h;

    iput-object v3, v8, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    .line 174
    if-eqz v12, :cond_6

    .line 175
    iget-object v3, v8, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    const/4 v4, 0x0

    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/wallet/service/ow/d;->b:Landroid/content/Context;

    invoke-static {v14}, Lcom/google/android/gms/wallet/common/ai;->a(Landroid/content/Context;)Lcom/google/aa/b/a/h;

    move-result-object v14

    aput-object v14, v3, v4

    .line 178
    :cond_6
    const/4 v3, 0x0

    move/from16 v17, v3

    move v3, v2

    move/from16 v2, v17

    :goto_4
    if-ge v2, v13, :cond_8

    .line 179
    iget-object v14, v8, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    add-int/lit8 v4, v3, 0x1

    iget-object v15, v11, Lcom/google/android/gms/wallet/cache/i;->c:[Lcom/google/aa/b/a/h;

    aget-object v15, v15, v2

    aput-object v15, v14, v3

    .line 178
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    goto :goto_4

    .line 160
    :cond_7
    iget-object v2, v10, Lcom/google/android/gms/wallet/cache/g;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 161
    iget-object v2, v10, Lcom/google/android/gms/wallet/cache/g;->c:Ljava/lang/String;

    iput-object v2, v8, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    goto :goto_3

    .line 182
    :cond_8
    if-nez v12, :cond_9

    .line 183
    iget-object v2, v8, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/wallet/service/ow/d;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/wallet/common/ai;->a(Landroid/content/Context;)Lcom/google/aa/b/a/h;

    move-result-object v4

    aput-object v4, v2, v3

    .line 188
    :cond_9
    iget-boolean v2, v10, Lcom/google/android/gms/wallet/cache/g;->e:Z

    if-eqz v2, :cond_b

    iget-boolean v2, v9, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    if-eqz v2, :cond_a

    sget-object v2, Lcom/google/android/gms/wallet/b/h;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_b

    :cond_a
    const/4 v2, 0x1

    :goto_5
    iput-boolean v2, v8, Lcom/google/aa/b/a/a/a/a/p;->o:Z

    .line 192
    iget-object v2, v5, Lcom/google/aa/a/a/a/b;->c:Lcom/google/checkout/inapp/proto/a/d;

    iput-object v2, v8, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    .line 194
    iget-boolean v2, v10, Lcom/google/android/gms/wallet/cache/g;->d:Z

    if-eqz v2, :cond_c

    iget-boolean v2, v9, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    if-nez v2, :cond_c

    const/4 v2, 0x1

    .line 196
    :goto_6
    iput-boolean v2, v8, Lcom/google/aa/b/a/a/a/a/p;->j:Z

    .line 197
    iput-boolean v6, v8, Lcom/google/aa/b/a/a/a/a/p;->k:Z

    .line 198
    iput-object v8, v7, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    .line 199
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->b:Landroid/content/Context;

    const-string v3, "wallet_items_cache"

    const-string v4, "cache_hit"

    const-string v5, "persistent_cache"

    const-wide/16 v8, 0x0

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v2, v3, v4, v5, v6}, Lcom/google/android/gms/wallet/common/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 204
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->b:Landroid/content/Context;

    move-object/from16 v0, p1

    invoke-static {v2, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v3

    const-string v4, "wallet_items_persistent_cache_hit"

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    new-instance v2, Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    const/16 v3, 0x13

    invoke-direct {v2, v3, v7}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;-><init>(ILcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    .line 188
    :cond_b
    const/4 v2, 0x0

    goto :goto_5

    .line 194
    :cond_c
    const/4 v2, 0x0

    goto :goto_6

    .line 211
    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->b:Landroid/content/Context;

    const-string v5, "wallet_items_cache"

    const-string v7, "cache_miss"

    const-string v8, "persistent_cache"

    const-wide/16 v12, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-static {v2, v5, v7, v8, v12}, Lcom/google/android/gms/wallet/common/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    .line 219
    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->c:Lcom/google/android/gms/wallet/service/ow/w;

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v12

    .line 220
    invoke-virtual {v12}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v5

    .line 221
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->d:Lcom/google/android/gms/wallet/service/r;

    invoke-virtual {v2, v3, v9, v5}, Lcom/google/android/gms/wallet/service/r;->b(Ljava/lang/String;Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;)V

    .line 222
    instance-of v2, v5, Lcom/google/aa/a/a/a/f;

    if-eqz v2, :cond_12

    .line 223
    check-cast v5, Lcom/google/aa/a/a/a/f;

    .line 224
    iget-object v13, v5, Lcom/google/aa/a/a/a/f;->a:Lcom/google/aa/b/a/a/a/a/p;

    .line 225
    iget-boolean v2, v13, Lcom/google/aa/b/a/a/a/a/p;->k:Z

    if-eqz v2, :cond_10

    if-eqz v6, :cond_10

    const/4 v2, 0x1

    :goto_7
    iput-boolean v2, v13, Lcom/google/aa/b/a/a/a/a/p;->k:Z

    .line 227
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->e:Lcom/google/android/gms/wallet/cache/j;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v3

    iget-boolean v6, v9, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    iget-boolean v7, v9, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    iget-boolean v8, v9, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    invoke-static/range {v2 .. v8}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/cache/j;Landroid/accounts/Account;ILcom/google/aa/a/a/a/f;ZZZ)V

    .line 235
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/wallet/service/ow/d;->g:Lcom/google/android/gms/wallet/cache/f;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v4, v5, v10}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/cache/f;Ljava/lang/String;ILcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/g;)V

    .line 241
    iget-boolean v2, v9, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    if-eqz v2, :cond_f

    .line 242
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/wallet/service/ow/d;->f:Lcom/google/android/gms/wallet/cache/h;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;->a()Landroid/accounts/Account;

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->e()Ljava/lang/String;

    move-result-object v8

    move v9, v4

    move-object v10, v5

    invoke-static/range {v6 .. v11}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/cache/h;Landroid/accounts/Account;Ljava/lang/String;ILcom/google/aa/a/a/a/f;Lcom/google/android/gms/wallet/cache/i;)V

    .line 251
    :cond_f
    iget-object v2, v13, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    array-length v2, v2

    if-lez v2, :cond_12

    .line 254
    iget-object v2, v11, Lcom/google/android/gms/wallet/cache/i;->b:Ljava/lang/String;

    const-string v3, "dont_send_loyalty_wob_id"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_11

    .line 256
    iget-object v5, v13, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    .line 258
    array-length v2, v5

    add-int/lit8 v2, v2, 0x1

    new-array v2, v2, [Lcom/google/aa/b/a/h;

    iput-object v2, v13, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    .line 260
    iget-object v2, v13, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    const/4 v4, 0x0

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/wallet/service/ow/d;->b:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/gms/wallet/common/ai;->a(Landroid/content/Context;)Lcom/google/aa/b/a/h;

    move-result-object v6

    aput-object v6, v2, v4

    .line 262
    array-length v6, v5

    const/4 v2, 0x0

    :goto_8
    if-ge v2, v6, :cond_12

    aget-object v7, v5, v2

    .line 263
    iget-object v8, v13, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    add-int/lit8 v4, v3, 0x1

    aput-object v7, v8, v3

    .line 262
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    goto :goto_8

    .line 225
    :cond_10
    const/4 v2, 0x0

    goto :goto_7

    .line 266
    :cond_11
    iget-object v2, v13, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/wallet/service/ow/d;->b:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/ai;->a(Landroid/content/Context;)Lcom/google/aa/b/a/h;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/aa/b/a/h;

    iput-object v2, v13, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    :cond_12
    move-object v2, v12

    .line 272
    goto/16 :goto_0
.end method
