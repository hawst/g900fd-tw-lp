.class public abstract Lcom/google/android/gms/common/ui/k;
.super Landroid/support/v4/a/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/common/api/v;

.field b:Z

.field public c:Lcom/google/android/gms/common/api/Status;

.field private d:Lcom/google/android/gms/common/data/d;

.field private final e:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0, p1}, Landroid/support/v4/a/j;-><init>(Landroid/content/Context;)V

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/ui/k;->e:Ljava/util/ArrayList;

    .line 36
    return-void
.end method

.method private a(Lcom/google/android/gms/common/data/d;Z)V
    .locals 2

    .prologue
    .line 128
    iget-boolean v0, p0, Landroid/support/v4/a/j;->r:Z

    if-eqz v0, :cond_1

    .line 130
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    .line 131
    invoke-interface {p1}, Lcom/google/android/gms/common/data/d;->w_()V

    .line 149
    :cond_0
    :goto_0
    return-void

    .line 135
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->d:Lcom/google/android/gms/common/data/d;

    .line 136
    iput-object p1, p0, Lcom/google/android/gms/common/ui/k;->d:Lcom/google/android/gms/common/data/d;

    .line 138
    iget-boolean v1, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v1, :cond_2

    .line 139
    invoke-super {p0, p1}, Landroid/support/v4/a/j;->b(Ljava/lang/Object;)V

    .line 142
    :cond_2
    if-eqz v0, :cond_0

    if-eq v0, p1, :cond_0

    .line 143
    iget-object v1, p0, Lcom/google/android/gms/common/ui/k;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    if-eqz p2, :cond_0

    .line 146
    invoke-direct {p0}, Lcom/google/android/gms/common/ui/k;->c()V

    goto :goto_0
.end method

.method private c()V
    .locals 3

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 153
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->w_()V

    .line 153
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 157
    return-void
.end method


# virtual methods
.method protected abstract a(Landroid/content/Context;)Lcom/google/android/gms/common/api/v;
.end method

.method public a()V
    .locals 1

    .prologue
    .line 90
    invoke-super {p0}, Landroid/support/v4/a/j;->a()V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->a:Lcom/google/android/gms/common/api/v;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/ui/k;->a(Lcom/google/android/gms/common/api/v;)V

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/common/ui/k;->b:Z

    if-nez v0, :cond_0

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 166
    iput-object p1, p0, Lcom/google/android/gms/common/ui/k;->c:Lcom/google/android/gms/common/api/Status;

    .line 167
    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Lcom/google/android/gms/common/ui/k;->a(Lcom/google/android/gms/common/data/d;Z)V

    .line 168
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/common/data/d;Z)V
    .locals 1

    .prologue
    .line 177
    iput-object p1, p0, Lcom/google/android/gms/common/ui/k;->c:Lcom/google/android/gms/common/api/Status;

    .line 178
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/gms/common/ui/k;->a(Lcom/google/android/gms/common/data/d;Z)V

    .line 179
    return-void
.end method

.method protected abstract a(Lcom/google/android/gms/common/api/v;)V
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 25
    check-cast p1, Lcom/google/android/gms/common/data/d;

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/common/ui/k;->a(Lcom/google/android/gms/common/data/d;Z)V

    return-void
.end method

.method protected final b()Z
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->a:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->a:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_0

    .line 48
    iget-object v0, p0, Landroid/support/v4/a/j;->o:Landroid/content/Context;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/ui/k;->a(Landroid/content/Context;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/ui/k;->a:Lcom/google/android/gms/common/api/v;

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->a:Lcom/google/android/gms/common/api/v;

    new-instance v1, Lcom/google/android/gms/common/ui/l;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/ui/l;-><init>(Lcom/google/android/gms/common/ui/k;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->a:Lcom/google/android/gms/common/api/v;

    new-instance v1, Lcom/google/android/gms/common/ui/m;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/ui/m;-><init>(Lcom/google/android/gms/common/ui/k;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/y;)V

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->d:Lcom/google/android/gms/common/data/d;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->d:Lcom/google/android/gms/common/data/d;

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/ui/k;->a(Lcom/google/android/gms/common/data/d;Z)V

    .line 83
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/k;->i()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->d:Lcom/google/android/gms/common/data/d;

    if-nez v0, :cond_3

    .line 84
    :cond_2
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    .line 86
    :cond_3
    return-void
.end method

.method public f()V
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->a:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/common/ui/k;->b:Z

    if-eqz v0, :cond_2

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 103
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/ui/k;->b:Z

    .line 105
    :cond_2
    return-void
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 109
    invoke-super {p0}, Landroid/support/v4/a/j;->g()V

    .line 111
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/k;->f()V

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->d:Lcom/google/android/gms/common/data/d;

    if-eqz v0, :cond_0

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/common/ui/k;->d:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->w_()V

    .line 115
    invoke-direct {p0}, Lcom/google/android/gms/common/ui/k;->c()V

    .line 117
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/ui/k;->d:Lcom/google/android/gms/common/data/d;

    .line 118
    return-void
.end method
