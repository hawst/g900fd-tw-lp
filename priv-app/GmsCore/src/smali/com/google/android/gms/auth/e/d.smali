.class public final Lcom/google/android/gms/auth/e/d;
.super Lcom/google/android/gms/auth/e/a;
.source "SourceFile"


# instance fields
.field public d:J

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/e/a;-><init>(Landroid/content/Context;)V

    .line 29
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 55
    sget-object v0, Lcom/google/android/gms/auth/b/a;->r:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v1

    sget-object v0, Lcom/google/android/gms/auth/b/a;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpl-float v0, v1, v0

    if-ltz v0, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 60
    :cond_1
    new-instance v0, Lcom/google/android/gms/auth/f/h;

    invoke-direct {v0}, Lcom/google/android/gms/auth/f/h;-><init>()V

    .line 61
    iget-wide v2, p0, Lcom/google/android/gms/auth/e/d;->d:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/auth/f/h;->a(J)Lcom/google/android/gms/auth/f/h;

    .line 62
    iget-boolean v1, p0, Lcom/google/android/gms/auth/e/d;->e:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/h;->a(Z)Lcom/google/android/gms/auth/f/h;

    .line 63
    iget-boolean v1, p0, Lcom/google/android/gms/auth/e/d;->f:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/h;->b(Z)Lcom/google/android/gms/auth/f/h;

    .line 64
    iget-boolean v1, p0, Lcom/google/android/gms/auth/e/d;->g:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/h;->c(Z)Lcom/google/android/gms/auth/f/h;

    .line 65
    iget-boolean v1, p0, Lcom/google/android/gms/auth/e/d;->h:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/h;->d(Z)Lcom/google/android/gms/auth/f/h;

    .line 67
    new-instance v1, Lcom/google/android/gms/auth/f/j;

    invoke-direct {v1}, Lcom/google/android/gms/auth/f/j;-><init>()V

    .line 68
    iget-object v2, p0, Lcom/google/android/gms/auth/e/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/f/j;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/f/j;

    .line 69
    iget-object v2, p0, Lcom/google/android/gms/auth/e/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/f/j;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/f/j;

    .line 70
    iget-object v2, p0, Lcom/google/android/gms/auth/e/d;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/f/j;->c(Ljava/lang/String;)Lcom/google/android/gms/auth/f/j;

    .line 71
    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/f/h;->a(Lcom/google/android/gms/auth/f/j;)Lcom/google/android/gms/auth/f/h;

    .line 73
    new-instance v1, Lcom/google/android/gms/auth/f/b;

    invoke-direct {v1}, Lcom/google/android/gms/auth/f/b;-><init>()V

    .line 74
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/f/b;->a(I)Lcom/google/android/gms/auth/f/b;

    .line 75
    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/f/b;->a(Lcom/google/android/gms/auth/f/h;)Lcom/google/android/gms/auth/f/b;

    .line 76
    const-string v0, "GetToken"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/e/d;->a(Ljava/lang/String;Lcom/google/android/gms/auth/f/b;)V

    goto :goto_0
.end method
