.class final Lcom/google/android/gms/wallet/common/ui/dl;
.super Lcom/google/android/gms/wallet/service/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V
    .locals 0

    .prologue
    .line 389
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(ILandroid/content/Intent;)V

    .line 467
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 411
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->d(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    .line 413
    iget-object v1, p1, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->c:[I

    array-length v1, v1

    if-lez v1, :cond_2

    .line 416
    iget-object v5, p1, Lcom/google/checkout/inapp/proto/Service$UpdateInstrumentPostResponse;->c:[I

    array-length v6, v5

    move v4, v0

    move v1, v0

    move v3, v0

    :goto_0
    if-ge v4, v6, :cond_3

    aget v0, v5, v4

    .line 417
    packed-switch v0, :pswitch_data_0

    .line 447
    :pswitch_0
    const-string v1, "UpdateInstrumentActivit"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unexpected error code: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    move v1, v3

    .line 451
    :goto_1
    if-eqz v0, :cond_1

    .line 452
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->e(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    .line 462
    :goto_2
    return-void

    .line 420
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cb;

    instance-of v0, v0, Lcom/google/android/gms/wallet/common/ui/dm;

    if-eqz v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cb;

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dm;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dm;->b()V

    .line 425
    :cond_0
    if-nez v3, :cond_4

    .line 426
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f:Lcom/google/android/gms/wallet/common/ui/cb;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/cb;->g()Z

    move v0, v1

    move v1, v2

    .line 428
    goto :goto_1

    .line 432
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    sget v7, Lcom/google/android/gms/p;->Bo:I

    invoke-static {v0, v7}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;I)V

    .line 433
    if-nez v3, :cond_4

    .line 434
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    move v0, v1

    move v1, v2

    .line 435
    goto :goto_1

    .line 439
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    sget v7, Lcom/google/android/gms/p;->Bn:I

    invoke-static {v0, v7}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;I)V

    .line 441
    if-nez v3, :cond_4

    .line 442
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    move v0, v1

    move v1, v2

    .line 443
    goto :goto_1

    .line 416
    :cond_1
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 457
    :cond_2
    const-string v0, "UpdateInstrumentActivit"

    const-string v1, "Unexpected update instrument response."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->e(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    goto :goto_2

    .line 461
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->f(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    goto :goto_2

    :cond_4
    move v0, v1

    move v1, v3

    goto :goto_1

    .line 417
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(ILandroid/content/Intent;)V

    .line 472
    return-void
.end method

.method public final b(Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/service/ServerResponse;)V
    .locals 3

    .prologue
    .line 394
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 395
    const-string v1, "com.google.android.gms.wallet.instrument"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 397
    const-string v1, "com.google.android.gms.wallet.priorInstrumentId"

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->b(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)Lcom/google/checkout/inapp/proto/j;

    move-result-object v2

    iget-object v2, v2, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 402
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->c(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 403
    const-string v1, "serverResponse"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 405
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->a(ILandroid/content/Intent;)V

    .line 406
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->g(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    .line 477
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dl;->a:Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;->e(Lcom/google/android/gms/wallet/common/ui/UpdateInstrumentActivity;)V

    .line 482
    return-void
.end method
