.class public final Lcom/google/android/gms/drive/events/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/events/k;


# instance fields
.field final a:Lcom/google/android/gms/drive/events/t;

.field final b:Lcom/google/android/gms/drive/g/i;

.field final c:I

.field final d:I

.field final e:Lcom/google/android/gms/drive/g/n;

.field final f:Lcom/google/android/gms/drive/database/r;

.field final g:Ljava/util/concurrent/Executor;

.field private final h:Landroid/content/Context;

.field private final i:Lcom/google/android/gms/drive/events/w;

.field private final j:Lcom/google/android/gms/drive/api/b;

.field private final k:Lcom/google/android/gms/drive/events/u;

.field private final l:Lcom/google/android/gms/drive/b/f;

.field private final m:I

.field private final n:I

.field private final o:Lcom/google/android/gms/drive/c/b;

.field private final p:Lcom/google/android/gms/drive/g/al;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/events/w;Lcom/google/android/gms/drive/api/b;Lcom/google/android/gms/drive/events/t;Lcom/google/android/gms/drive/events/u;Lcom/google/android/gms/drive/b/f;Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/c/b;Lcom/google/android/gms/drive/database/r;)V
    .locals 5

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    sget-object v0, Lcom/google/android/gms/drive/ai;->az:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/events/l;->m:I

    .line 64
    sget-object v0, Lcom/google/android/gms/drive/ai;->aA:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/events/l;->c:I

    .line 65
    sget-object v0, Lcom/google/android/gms/drive/ai;->aB:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/events/l;->d:I

    .line 66
    sget-object v0, Lcom/google/android/gms/drive/ai;->ay:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/events/l;->n:I

    .line 77
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/events/l;->g:Ljava/util/concurrent/Executor;

    .line 79
    new-instance v0, Lcom/google/android/gms/drive/g/an;

    new-instance v1, Lcom/google/android/gms/drive/events/m;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/events/m;-><init>(Lcom/google/android/gms/drive/events/l;)V

    iget v2, p0, Lcom/google/android/gms/drive/events/l;->c:I

    iget-object v3, p0, Lcom/google/android/gms/drive/events/l;->g:Ljava/util/concurrent/Executor;

    const-string v4, "EventDistributorImpl"

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/drive/g/an;-><init>(Ljava/lang/Runnable;ILjava/util/concurrent/Executor;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/events/l;->p:Lcom/google/android/gms/drive/g/al;

    .line 94
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/drive/events/l;->h:Landroid/content/Context;

    .line 95
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/events/w;

    iput-object v0, p0, Lcom/google/android/gms/drive/events/l;->i:Lcom/google/android/gms/drive/events/w;

    .line 96
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/api/b;

    iput-object v0, p0, Lcom/google/android/gms/drive/events/l;->j:Lcom/google/android/gms/drive/api/b;

    .line 97
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/events/t;

    iput-object v0, p0, Lcom/google/android/gms/drive/events/l;->a:Lcom/google/android/gms/drive/events/t;

    .line 98
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/events/u;

    iput-object v0, p0, Lcom/google/android/gms/drive/events/l;->k:Lcom/google/android/gms/drive/events/u;

    .line 99
    invoke-static {p7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/events/l;->b:Lcom/google/android/gms/drive/g/i;

    .line 100
    invoke-static {p6}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/b/f;

    iput-object v0, p0, Lcom/google/android/gms/drive/events/l;->l:Lcom/google/android/gms/drive/b/f;

    .line 101
    iput-object p8, p0, Lcom/google/android/gms/drive/events/l;->e:Lcom/google/android/gms/drive/g/n;

    .line 102
    invoke-static {p9}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/c/b;

    iput-object v0, p0, Lcom/google/android/gms/drive/events/l;->o:Lcom/google/android/gms/drive/c/b;

    .line 103
    invoke-static {p10}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/events/l;->f:Lcom/google/android/gms/drive/database/r;

    .line 104
    return-void
.end method

.method private a(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 1

    .prologue
    .line 404
    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/events/l;->l:Lcom/google/android/gms/drive/b/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/b/f;->a(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Ljava/lang/Throwable;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/bl;)V
    .locals 2

    .prologue
    .line 263
    if-eqz p1, :cond_0

    .line 264
    const-string v0, "EventDistributorImpl"

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 268
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/events/l;->h:Landroid/content/Context;

    const-string v1, "EventDistributorImpl"

    invoke-static {v0, v1, p2}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-virtual {p0, p3, v0, v1}, Lcom/google/android/gms/drive/events/l;->a(Lcom/google/android/gms/drive/database/model/bl;Lcom/google/android/gms/drive/events/f;I)V

    .line 270
    return-void

    .line 266
    :cond_0
    const-string v0, "EventDistributorImpl"

    invoke-static {v0, p2}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 164
    const-string v0, "EventDistributorImpl"

    const-string v1, "startingPersistedEventRetryLoop"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/drive/events/l;->p:Lcom/google/android/gms/drive/g/al;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/al;->a()V

    .line 166
    return-void
.end method

.method final a(Lcom/google/android/gms/drive/database/model/bl;)V
    .locals 13

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 180
    const-string v0, "EventDistributorImpl"

    const-string v1, "mNotifyIntervalMillis:%s, mSnoozeIntervalMillis:%s, mSnoozeGrowthBase: %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/gms/drive/events/l;->m:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    iget v3, p0, Lcom/google/android/gms/drive/events/l;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget v3, p0, Lcom/google/android/gms/drive/events/l;->d:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 182
    iget-wide v2, p1, Lcom/google/android/gms/drive/database/model/ad;->f:J

    .line 183
    const-wide/16 v0, 0x0

    cmp-long v0, v2, v0

    if-gez v0, :cond_0

    .line 184
    const-string v0, "EventDistributorImpl"

    const-string v1, "Event to raise is not persisted yet"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 259
    :goto_0
    return-void

    .line 188
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/events/l;->a:Lcom/google/android/gms/drive/events/t;

    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v0

    new-instance v1, Lorg/json/JSONObject;

    iget-object v4, p1, Lcom/google/android/gms/drive/database/model/bl;->c:Ljava/lang/String;

    invoke-direct {v1, v4}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v0

    iget-wide v4, p1, Lcom/google/android/gms/drive/database/model/bl;->b:J

    invoke-interface {v0, v4, v5}, Lcom/google/android/gms/drive/database/r;->f(J)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v4

    iget-object v5, v4, Lcom/google/android/gms/drive/database/model/f;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    iget-wide v6, v4, Lcom/google/android/gms/drive/database/model/f;->a:J

    invoke-interface {v0, v6, v7, v5}, Lcom/google/android/gms/drive/database/r;->a(JLcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/gms/drive/database/model/bl;->a:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-interface {v0, v4, v5}, Lcom/google/android/gms/drive/database/r;->b(Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/database/model/EntrySpec;)Lcom/google/android/gms/drive/database/model/ah;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/drive/a/u;

    invoke-direct {v0, v5}, Lcom/google/android/gms/drive/a/u;-><init>(Lcom/google/android/gms/drive/database/model/EntrySpec;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/drive/a/u; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_3

    .line 245
    :catch_0
    move-exception v0

    .line 246
    const-string v1, "IOException extracting completion event from persisted event"

    .line 247
    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/drive/events/l;->a(Ljava/lang/Throwable;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/bl;)V

    goto :goto_0

    .line 188
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/ah;->f()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/events/f;->a(Lorg/json/JSONObject;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/events/f;

    move-result-object v5

    .line 190
    iget v11, p1, Lcom/google/android/gms/drive/database/model/bl;->g:I

    .line 191
    iget v0, p0, Lcom/google/android/gms/drive/events/l;->n:I

    if-lt v11, v0, :cond_2

    .line 192
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v5, v0}, Lcom/google/android/gms/drive/events/l;->a(Lcom/google/android/gms/drive/database/model/bl;Lcom/google/android/gms/drive/events/f;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/gms/drive/a/u; {:try_start_1 .. :try_end_1} :catch_2
    .catch Landroid/database/SQLException; {:try_start_1 .. :try_end_1} :catch_3

    goto :goto_0

    .line 248
    :catch_1
    move-exception v0

    .line 249
    const-string v1, "JSONException extracting completion event from persisted event"

    .line 250
    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/drive/events/l;->a(Ljava/lang/Throwable;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/bl;)V

    goto :goto_0

    .line 196
    :cond_2
    :try_start_2
    new-instance v0, Lcom/google/android/gms/drive/events/o;

    move-object v1, p0

    move-object v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/events/o;-><init>(Lcom/google/android/gms/drive/events/l;JLcom/google/android/gms/drive/database/model/bl;Lcom/google/android/gms/drive/events/f;)V

    .line 221
    invoke-virtual {v5}, Lcom/google/android/gms/drive/events/f;->j()Ljava/lang/String;

    move-result-object v1

    .line 222
    if-nez v1, :cond_4

    .line 224
    iget-object v1, p0, Lcom/google/android/gms/drive/events/l;->f:Lcom/google/android/gms/drive/database/r;

    iget-wide v2, p1, Lcom/google/android/gms/drive/database/model/bl;->b:J

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/drive/database/r;->f(J)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v1

    .line 226
    if-eqz v1, :cond_3

    .line 227
    iget-object v1, v1, Lcom/google/android/gms/drive/database/model/f;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;

    move-result-object v1

    move-object v10, v1

    .line 234
    :goto_1
    invoke-interface {v0}, Lcom/google/android/gms/drive/internal/cg;->asBinder()Landroid/os/IBinder;

    move-result-object v9

    new-instance v0, Lcom/google/android/gms/drive/events/CompletionEvent;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/events/f;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-virtual {v5}, Lcom/google/android/gms/drive/events/f;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5}, Lcom/google/android/gms/drive/events/f;->d()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gms/drive/events/l;->a(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v3

    invoke-virtual {v5}, Lcom/google/android/gms/drive/events/f;->e()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/gms/drive/events/l;->a(Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;

    move-result-object v4

    invoke-virtual {v5}, Lcom/google/android/gms/drive/events/f;->f()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v12

    iget-object v6, p0, Lcom/google/android/gms/drive/events/l;->h:Landroid/content/Context;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/events/f;->g()Ljava/util/List;

    move-result-object v7

    invoke-virtual {v5}, Lcom/google/android/gms/drive/events/f;->i()I

    move-result v8

    move-object v5, v12

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/drive/events/CompletionEvent;-><init>(Lcom/google/android/gms/drive/DriveId;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Landroid/content/Context;Ljava/util/List;ILandroid/os/IBinder;)V

    .line 239
    const-string v1, "EventDistributorImpl"

    const-string v2, "Routing CompletionEvent: %s. To: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v4, 0x1

    aput-object v10, v3, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 240
    iget-object v1, p0, Lcom/google/android/gms/drive/events/l;->k:Lcom/google/android/gms/drive/events/u;

    invoke-virtual {v1, v10, v0}, Lcom/google/android/gms/drive/events/u;->a(Ljava/lang/String;Lcom/google/android/gms/drive/events/DriveEvent;)V

    .line 242
    add-int/lit8 v0, v11, 0x1

    iput v0, p1, Lcom/google/android/gms/drive/database/model/bl;->g:I

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/drive/events/l;->b:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v0

    iget v2, p0, Lcom/google/android/gms/drive/events/l;->m:I

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p1, Lcom/google/android/gms/drive/database/model/bl;->d:J

    .line 244
    invoke-virtual {p1}, Lcom/google/android/gms/drive/database/model/bl;->i()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/drive/a/u; {:try_start_2 .. :try_end_2} :catch_2
    .catch Landroid/database/SQLException; {:try_start_2 .. :try_end_2} :catch_3

    goto/16 :goto_0

    .line 251
    :catch_2
    move-exception v0

    .line 252
    const-string v1, "EntryNoLongerExistsException extracting completion event from persisted event"

    .line 254
    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/drive/events/l;->a(Ljava/lang/Throwable;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/bl;)V

    goto/16 :goto_0

    .line 229
    :cond_3
    :try_start_3
    const-string v0, "Unable to find executing app for persisted event."

    .line 230
    const/4 v1, 0x0

    invoke-direct {p0, v1, v0, p1}, Lcom/google/android/gms/drive/events/l;->a(Ljava/lang/Throwable;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/bl;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/gms/drive/a/u; {:try_start_3 .. :try_end_3} :catch_2
    .catch Landroid/database/SQLException; {:try_start_3 .. :try_end_3} :catch_3

    goto/16 :goto_0

    .line 255
    :catch_3
    move-exception v0

    .line 256
    const-string v1, "SQLException increasing persisted event attempts"

    .line 257
    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/drive/events/l;->a(Ljava/lang/Throwable;Ljava/lang/String;Lcom/google/android/gms/drive/database/model/bl;)V

    goto/16 :goto_0

    :cond_4
    move-object v10, v1

    goto :goto_1
.end method

.method final a(Lcom/google/android/gms/drive/database/model/bl;Lcom/google/android/gms/drive/events/f;I)V
    .locals 9

    .prologue
    const/4 v8, 0x1

    .line 303
    iget-wide v0, p1, Lcom/google/android/gms/drive/database/model/ad;->f:J

    .line 304
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 305
    const-string v0, "EventDistributorImpl"

    const-string v1, "Event to delete is not persisted yet"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    :goto_0
    return-void

    .line 310
    :cond_0
    iget v2, p1, Lcom/google/android/gms/drive/database/model/bl;->h:I

    .line 311
    iget-object v3, p0, Lcom/google/android/gms/drive/events/l;->o:Lcom/google/android/gms/drive/c/b;

    invoke-interface {v3}, Lcom/google/android/gms/drive/c/b;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/drive/c/a;->b()Lcom/google/android/gms/drive/c/a;

    move-result-object v3

    const/4 v4, 0x2

    const/16 v5, 0x1a

    invoke-interface {v3, v4, v5}, Lcom/google/android/gms/drive/c/a;->a(II)Lcom/google/android/gms/drive/c/a;

    move-result-object v3

    .line 315
    iget-object v4, p0, Lcom/google/android/gms/drive/events/l;->f:Lcom/google/android/gms/drive/database/r;

    iget-wide v6, p1, Lcom/google/android/gms/drive/database/model/bl;->b:J

    invoke-interface {v4, v6, v7}, Lcom/google/android/gms/drive/database/r;->f(J)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v4

    .line 318
    if-eqz v4, :cond_1

    .line 319
    new-instance v5, Lcom/google/android/gms/drive/auth/CallingAppInfo;

    invoke-direct {v5, v4}, Lcom/google/android/gms/drive/auth/CallingAppInfo;-><init>(Lcom/google/android/gms/drive/database/model/f;)V

    invoke-interface {v3, v5}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/auth/CallingAppInfo;)Lcom/google/android/gms/drive/c/a;

    .line 322
    :cond_1
    if-nez p2, :cond_2

    .line 323
    const/4 v4, 0x0

    invoke-interface {v3, v4, v2, p3}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/events/f;II)Lcom/google/android/gms/drive/c/a;

    .line 334
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/drive/events/l;->a:Lcom/google/android/gms/drive/events/t;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/events/t;->b(J)Z

    move-result v2

    .line 335
    if-eqz v2, :cond_4

    .line 338
    invoke-interface {v3}, Lcom/google/android/gms/drive/c/a;->a()V

    goto :goto_0

    .line 327
    :cond_2
    invoke-virtual {p2}, Lcom/google/android/gms/drive/events/f;->c()Ljava/lang/String;

    move-result-object v4

    const-string v5, "__unknown_account_name"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 329
    invoke-virtual {p2}, Lcom/google/android/gms/drive/events/f;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/google/android/gms/drive/c/a;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/c/a;

    .line 331
    :cond_3
    invoke-interface {v3, p2, v2, p3}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/events/f;II)Lcom/google/android/gms/drive/c/a;

    goto :goto_1

    .line 340
    :cond_4
    if-ne p3, v8, :cond_5

    .line 346
    iget-object v0, p0, Lcom/google/android/gms/drive/events/l;->h:Landroid/content/Context;

    const-string v1, "EventDistributorImpl"

    const-string v2, "Error deleting persisted event"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 348
    :cond_5
    const-string v2, "EventDistributorImpl"

    const-string v3, "Persisted event %d: already deleted."

    new-array v4, v8, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/events/ChangeEvent;Lcom/google/android/gms/drive/database/model/ah;)V
    .locals 4

    .prologue
    .line 108
    const-string v0, "EventDistributorImpl"

    const-string v1, "Raising change event %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/drive/events/l;->j:Lcom/google/android/gms/drive/api/b;

    iget-object v1, v0, Lcom/google/android/gms/drive/api/b;->a:Ljava/util/Set;

    monitor-enter v1

    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, v0, Lcom/google/android/gms/drive/api/b;->a:Ljava/util/Set;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/api/f;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/api/f;->a()Lcom/google/android/gms/drive/api/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/api/d;->a()Lcom/google/android/gms/drive/auth/g;

    move-result-object v2

    invoke-virtual {p2, v2}, Lcom/google/android/gms/drive/database/model/ah;->b(Lcom/google/android/gms/drive/auth/g;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/api/d;->a(Lcom/google/android/gms/drive/events/DriveEvent;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 114
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/events/l;->i:Lcom/google/android/gms/drive/events/w;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/drive/events/w;->a(Lcom/google/android/gms/drive/events/DriveEvent;Lcom/google/android/gms/drive/database/model/ah;)V

    .line 115
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/events/f;Lcom/google/android/gms/drive/auth/g;)V
    .locals 4

    .prologue
    .line 146
    const-string v0, "EventDistributorImpl"

    const-string v1, "Persisting completion event %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 148
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/events/l;->a:Lcom/google/android/gms/drive/events/t;

    invoke-static {p1, p2}, Lcom/google/android/gms/drive/events/t;->a(Lcom/google/android/gms/drive/events/f;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/bl;

    move-result-object v0

    .line 150
    iget-wide v0, v0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    iget-object v2, p0, Lcom/google/android/gms/drive/events/l;->g:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/google/android/gms/drive/events/n;

    invoke-direct {v3, p0, v0, v1}, Lcom/google/android/gms/drive/events/n;-><init>(Lcom/google/android/gms/drive/events/l;J)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_0
    .catch Landroid/database/SQLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 160
    :goto_0
    return-void

    .line 151
    :catch_0
    move-exception v0

    .line 152
    const-string v1, "SQLException persisting completion event"

    .line 153
    const-string v2, "EventDistributorImpl"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/drive/events/l;->h:Landroid/content/Context;

    const-string v2, "EventDistributorImpl"

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 155
    :catch_1
    move-exception v0

    .line 156
    const-string v1, "JSONException persisting completion event"

    .line 157
    const-string v2, "EventDistributorImpl"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)I

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/drive/events/l;->h:Landroid/content/Context;

    const-string v2, "EventDistributorImpl"

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/drive/internal/be;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
