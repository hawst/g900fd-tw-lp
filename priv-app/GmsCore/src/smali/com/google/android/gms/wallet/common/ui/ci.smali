.class public final Lcom/google/android/gms/wallet/common/ui/ci;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/ui/cb;
.implements Lcom/google/android/gms/wallet/common/ui/dp;


# instance fields
.field a:Lcom/google/android/gms/wallet/common/ui/cp;

.field b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->c:Z

    return-void
.end method

.method public static a([I)Lcom/google/android/gms/wallet/common/ui/ci;
    .locals 3

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ci;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/ci;-><init>()V

    .line 53
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 54
    const-string v2, "disallowedCreditCardTypes"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 55
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ci;->setArguments(Landroid/os/Bundle;)V

    .line 57
    return-object v0
.end method

.method private b()V
    .locals 2

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->a:Lcom/google/android/gms/wallet/common/ui/cp;

    if-eqz v0, :cond_0

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->a:Lcom/google/android/gms/wallet/common/ui/cp;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/ci;->c:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cp;->a(Z)V

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/ci;->c:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 123
    :cond_0
    return-void
.end method

.method private b(Z)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 72
    const/4 v0, 0x2

    new-array v4, v0, [Lcom/google/android/gms/wallet/common/ui/dn;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->a:Lcom/google/android/gms/wallet/common/ui/cp;

    aput-object v0, v4, v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v0, v4, v1

    .line 74
    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    .line 75
    if-eqz p1, :cond_2

    .line 76
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    .line 74
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 76
    goto :goto_1

    .line 77
    :cond_2
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->e()Z

    move-result v6

    if-nez v6, :cond_0

    .line 81
    :goto_2
    return v2

    :cond_3
    move v2, v0

    goto :goto_2
.end method


# virtual methods
.method public final a()Lcom/google/checkout/a/a/a/d;
    .locals 3

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->a:Lcom/google/android/gms/wallet/common/ui/cp;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/cp;->a()Lcom/google/checkout/a/a/a/d;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/a/a/a/d;

    .line 138
    iget-object v1, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    new-instance v2, Lcom/google/t/a/b;

    invoke-direct {v2}, Lcom/google/t/a/b;-><init>()V

    iput-object v2, v1, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    .line 139
    iget-object v1, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    iget-object v1, v1, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/ci;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/t/a/b;->s:Ljava/lang/String;

    .line 141
    return-object v0
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 114
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/ci;->c:Z

    .line 115
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ci;->b()V

    .line 116
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/ci;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/ci;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->clearFocus()V

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    .line 90
    const/4 v0, 0x1

    .line 94
    :goto_0
    return v0

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->a:Lcom/google/android/gms/wallet/common/ui/cp;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/cp;->g()Z

    move-result v0

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 153
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 155
    if-eqz p1, :cond_0

    .line 156
    const-string v0, "enabled"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->c:Z

    .line 158
    :cond_0
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    .line 163
    sget v0, Lcom/google/android/gms/l;->gB:I

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 165
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ci;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 166
    const-string v2, "disallowedCreditCardTypes"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v2

    .line 167
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ci;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v3, Lcom/google/android/gms/j;->kv:I

    invoke-virtual {v0, v3}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/cp;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->a:Lcom/google/android/gms/wallet/common/ui/cp;

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->a:Lcom/google/android/gms/wallet/common/ui/cp;

    if-nez v0, :cond_0

    .line 170
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/cp;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/cp;-><init>()V

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    const-string v4, "disallowedCreditCardTypes"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/cp;->setArguments(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->a:Lcom/google/android/gms/wallet/common/ui/cp;

    .line 174
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/ci;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v2, Lcom/google/android/gms/j;->kv:I

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/ci;->a:Lcom/google/android/gms/wallet/common/ui/cp;

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 183
    :cond_0
    sget v0, Lcom/google/android/gms/j;->cy:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ci;->b:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 185
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/ci;->b()V

    .line 187
    return-object v1
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 192
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 194
    const-string v0, "enabled"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/ci;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 195
    return-void
.end method
