.class public final Lcom/google/android/gms/ads/internal/l/a;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/ads/internal/p/a;

.field public final b:Landroid/content/Context;

.field c:Ljava/lang/String;

.field d:J

.field e:J

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field private final h:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/p/a;Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/l/a;->a:Lcom/google/android/gms/ads/internal/p/a;

    .line 65
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/l/a;->h:Ljava/util/Map;

    .line 66
    invoke-virtual {p1}, Lcom/google/android/gms/ads/internal/p/a;->k()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/l/a;->b:Landroid/content/Context;

    .line 67
    const-string v0, "description"

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/l/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/l/a;->c:Ljava/lang/String;

    const-string v0, "summary"

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/l/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/l/a;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/a;->h:Ljava/util/Map;

    const-string v1, "start"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/g;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/l/a;->d:J

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/a;->h:Ljava/util/Map;

    const-string v1, "end"

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/g;->d(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/l/a;->e:J

    const-string v0, "location"

    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/internal/l/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/l/a;->g:Ljava/lang/String;

    .line 68
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/a;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, ""

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/l/a;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method
