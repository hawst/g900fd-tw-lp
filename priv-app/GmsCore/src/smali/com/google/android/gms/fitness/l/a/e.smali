.class public final Lcom/google/android/gms/fitness/l/a/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/l/a/a;


# instance fields
.field private final a:Lcom/google/android/gms/fitness/l/z;

.field private final b:Z

.field private c:Ljava/util/List;

.field private d:Lcom/google/android/gms/fitness/data/DataPoint;

.field private e:Lcom/google/android/gms/fitness/data/DataPoint;

.field private f:Lcom/google/android/gms/fitness/data/DataPoint;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/fitness/l/z;)V
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->c:Ljava/util/List;

    .line 55
    iput-object p1, p0, Lcom/google/android/gms/fitness/l/a/e;->a:Lcom/google/android/gms/fitness/l/z;

    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/fitness/l/a/e;->b:Z

    .line 57
    return-void
.end method

.method private declared-synchronized b(Lcom/google/android/gms/fitness/data/DataPoint;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 93
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->e:Lcom/google/android/gms/fitness/data/DataPoint;

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_2

    .line 94
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/a/e;->c(Lcom/google/android/gms/fitness/data/DataPoint;)V

    .line 98
    :cond_0
    :goto_1
    iput-object p1, p0, Lcom/google/android/gms/fitness/l/a/e;->e:Lcom/google/android/gms/fitness/data/DataPoint;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 99
    monitor-exit p0

    return-void

    :cond_1
    move v0, v2

    .line 93
    goto :goto_0

    .line 95
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->d:Lcom/google/android/gms/fitness/data/DataPoint;

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/google/android/gms/fitness/l/a/e;->d(Lcom/google/android/gms/fitness/data/DataPoint;)I

    move-result v0

    iget-object v3, p0, Lcom/google/android/gms/fitness/l/a/e;->d:Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-static {v3}, Lcom/google/android/gms/fitness/l/a/e;->d(Lcom/google/android/gms/fitness/data/DataPoint;)I

    move-result v3

    sub-int v3, v0, v3

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->az:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-lt v3, v0, :cond_3

    move v0, v1

    :goto_2
    if-eqz v0, :cond_0

    .line 96
    invoke-direct {p0}, Lcom/google/android/gms/fitness/l/a/e;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->e:Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/l/a/e;->c(Lcom/google/android/gms/fitness/data/DataPoint;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move v0, v2

    .line 95
    goto :goto_2

    .line 96
    :cond_4
    :try_start_2
    const-string v0, "Data point has a cumulative value %d exceeding maximum delta %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Lcom/google/android/gms/fitness/data/DataPoint;->a(I)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/fitness/data/Value;->c()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    sget-object v3, Lcom/google/android/gms/fitness/g/c;->az:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/a/e;->c(Lcom/google/android/gms/fitness/data/DataPoint;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private b()Z
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->e:Lcom/google/android/gms/fitness/data/DataPoint;

    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a/e;->d:Lcom/google/android/gms/fitness/data/DataPoint;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/google/android/gms/fitness/data/DataPoint;)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    iput-object p1, p0, Lcom/google/android/gms/fitness/l/a/e;->d:Lcom/google/android/gms/fitness/data/DataPoint;

    .line 105
    return-void
.end method

.method private static d(Lcom/google/android/gms/fitness/data/DataPoint;)I
    .locals 1

    .prologue
    .line 146
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/data/DataPoint;->a(I)Lcom/google/android/gms/fitness/data/Value;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Value;->c()I

    move-result v0

    return v0
.end method

.method private static e(Lcom/google/android/gms/fitness/data/DataPoint;)Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 194
    if-nez p0, :cond_0

    .line 195
    const-string v0, "none"

    .line 203
    :goto_0
    return-object v0

    .line 198
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 199
    const-string v2, "%1$tF %1$tT"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v4}, Lcom/google/android/gms/fitness/data/DataPoint;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/data/DataPoint;->a()[Lcom/google/android/gms/fitness/data/Value;

    move-result-object v2

    array-length v3, v2

    :goto_1
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    .line 201
    const-string v5, " "

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v4}, Lcom/google/android/gms/fitness/data/Value;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 203
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 151
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->e:Lcom/google/android/gms/fitness/data/DataPoint;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/fitness/l/a/e;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a/e;->e:Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 153
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->e:Lcom/google/android/gms/fitness/data/DataPoint;

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->f:Lcom/google/android/gms/fitness/data/DataPoint;

    if-eqz v0, :cond_1

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->c:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a/e;->f:Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->f:Lcom/google/android/gms/fitness/data/DataPoint;

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_2

    .line 167
    :goto_0
    monitor-exit p0

    return-void

    .line 163
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->a:Lcom/google/android/gms/fitness/l/z;

    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a/e;->c:Ljava/util/List;

    const/4 v2, 0x0

    iget-boolean v3, p0, Lcom/google/android/gms/fitness/l/a/e;->b:Z

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/fitness/l/z;->a(Ljava/util/List;Lcom/google/android/gms/fitness/data/Application;Z)Ljava/util/Set;

    .line 165
    const-string v0, "Flushed %d DataPoints in buffer"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/fitness/l/a/e;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 166
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->c:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/fitness/data/DataPoint;)V
    .locals 3

    .prologue
    .line 61
    monitor-enter p0

    :try_start_0
    const-string v0, "Buffering %s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 64
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataPoint;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->b:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/l/a/e;->b(Lcom/google/android/gms/fitness/data/DataPoint;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 71
    :goto_0
    monitor-exit p0

    return-void

    .line 66
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataPoint;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/data/DataType;->l:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/fitness/data/DataType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 67
    sget-object v0, Lcom/google/android/gms/fitness/g/c;->aR:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-object p1, p0, Lcom/google/android/gms/fitness/l/a/e;->f:Lcom/google/android/gms/fitness/data/DataPoint;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 61
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 67
    :cond_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 69
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized a(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 180
    monitor-enter p0

    :try_start_0
    const-string v0, "DataPointBuffer:\n"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 181
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  size: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a/e;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    const-string v0, "  oldest: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/fitness/l/a/e;->c:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-static {v0}, Lcom/google/android/gms/fitness/l/a/e;->e(Lcom/google/android/gms/fitness/data/DataPoint;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 185
    :cond_0
    const-string v0, "  last location: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a/e;->f:Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-static {v1}, Lcom/google/android/gms/fitness/l/a/e;->e(Lcom/google/android/gms/fitness/data/DataPoint;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 186
    const-string v0, "  last step count: "

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/l/a/e;->e:Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-static {v1}, Lcom/google/android/gms/fitness/l/a/e;->e(Lcom/google/android/gms/fitness/data/DataPoint;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 189
    const-string v0, "Background Handler:\n"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 190
    monitor-exit p0

    return-void

    .line 180
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 171
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "mBuffer"

    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a/e;->c:Ljava/util/List;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "mLastStepCountCumulative"

    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a/e;->e:Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "mLastLocation"

    iget-object v2, p0, Lcom/google/android/gms/fitness/l/a/e;->f:Lcom/google/android/gms/fitness/data/DataPoint;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
