.class final Lcom/google/android/gms/auth/devicesignals/a;
.super Lcom/google/android/gms/auth/firstparty/a/b;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/auth/devicesignals/b;

.field private final b:Landroid/app/KeyguardManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 31
    new-instance v1, Lcom/google/android/gms/auth/devicesignals/b;

    invoke-direct {v1}, Lcom/google/android/gms/auth/devicesignals/b;-><init>()V

    const-string v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/gms/auth/devicesignals/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/devicesignals/b;Landroid/app/KeyguardManager;)V

    .line 35
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/auth/devicesignals/b;Landroid/app/KeyguardManager;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Lcom/google/android/gms/auth/firstparty/a/b;-><init>()V

    .line 37
    new-instance v0, Lcom/google/android/gms/auth/a/c;

    invoke-direct {v0, p1}, Lcom/google/android/gms/auth/a/c;-><init>(Landroid/content/Context;)V

    .line 38
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/a/c;->c(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    .line 41
    :cond_0
    iput-object p2, p0, Lcom/google/android/gms/auth/devicesignals/a;->a:Lcom/google/android/gms/auth/devicesignals/b;

    .line 42
    iput-object p3, p0, Lcom/google/android/gms/auth/devicesignals/a;->b:Landroid/app/KeyguardManager;

    .line 43
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/auth/devicesignals/a;->a:Lcom/google/android/gms/auth/devicesignals/b;

    iget-object v1, p0, Lcom/google/android/gms/auth/devicesignals/a;->b:Landroid/app/KeyguardManager;

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a(Lcom/google/android/gms/auth/devicesignals/b;Landroid/app/KeyguardManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/auth/devicesignals/a;->a:Lcom/google/android/gms/auth/devicesignals/b;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/devicesignals/b;->a()J

    move-result-wide v0

    .line 49
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/auth/devicesignals/a;->a:Lcom/google/android/gms/auth/devicesignals/b;

    iget-object v1, p0, Lcom/google/android/gms/auth/devicesignals/a;->b:Landroid/app/KeyguardManager;

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/devicesignals/LockScreenListener;->a(Lcom/google/android/gms/auth/devicesignals/b;Landroid/app/KeyguardManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/auth/devicesignals/a;->a:Lcom/google/android/gms/auth/devicesignals/b;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/devicesignals/b;->b()J

    move-result-wide v0

    .line 57
    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method
