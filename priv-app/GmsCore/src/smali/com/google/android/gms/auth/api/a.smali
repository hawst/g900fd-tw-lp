.class public final Lcom/google/android/gms/auth/api/a;
.super Lcom/android/volley/p;
.source "SourceFile"


# instance fields
.field private f:Ljava/util/Map;

.field private g:[B

.field private h:Lcom/android/volley/x;


# direct methods
.method public constructor <init>(ILjava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/util/Map;[B)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0, p1, p2, p4}, Lcom/android/volley/p;-><init>(ILjava/lang/String;Lcom/android/volley/w;)V

    .line 27
    iput-object p3, p0, Lcom/google/android/gms/auth/api/a;->h:Lcom/android/volley/x;

    .line 28
    iput-object p5, p0, Lcom/google/android/gms/auth/api/a;->f:Ljava/util/Map;

    .line 29
    iput-object p6, p0, Lcom/google/android/gms/auth/api/a;->g:[B

    .line 30
    return-void
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 4

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/gms/auth/api/GoogleAuthApiResponse;

    iget v1, p1, Lcom/android/volley/m;->a:I

    iget-object v2, p1, Lcom/android/volley/m;->c:Ljava/util/Map;

    iget-object v3, p1, Lcom/android/volley/m;->b:[B

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/auth/api/GoogleAuthApiResponse;-><init>(ILjava/util/Map;[B)V

    .line 37
    invoke-static {p1}, Lcom/android/volley/toolbox/i;->a(Lcom/android/volley/m;)Lcom/android/volley/c;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 15
    check-cast p1, Lcom/google/android/gms/auth/api/GoogleAuthApiResponse;

    iget-object v0, p0, Lcom/google/android/gms/auth/api/a;->h:Lcom/android/volley/x;

    invoke-interface {v0, p1}, Lcom/android/volley/x;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final i()Ljava/util/Map;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/auth/api/a;->f:Ljava/util/Map;

    return-object v0
.end method

.method public final m()[B
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/auth/api/a;->g:[B

    return-object v0
.end method
