.class public final Lcom/google/android/gms/drive/b/a/j;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:J

.field private final b:Lcom/google/android/gms/drive/b/a/d;

.field private final c:Lcom/google/android/gms/drive/b/a/m;

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/b/a/m;)V
    .locals 2

    .prologue
    .line 189
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    new-instance v0, Lcom/google/android/gms/drive/b/a/d;

    invoke-direct {v0}, Lcom/google/android/gms/drive/b/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/b/a/j;->b:Lcom/google/android/gms/drive/b/a/d;

    .line 187
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/drive/b/a/j;->a:J

    .line 190
    iput-object p1, p0, Lcom/google/android/gms/drive/b/a/j;->c:Lcom/google/android/gms/drive/b/a/m;

    .line 191
    invoke-interface {p1}, Lcom/google/android/gms/drive/b/a/m;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/drive/b/a/j;->d:I

    .line 192
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/b/a/i;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 215
    .line 217
    iget-wide v0, p0, Lcom/google/android/gms/drive/b/a/j;->a:J

    iget v2, p0, Lcom/google/android/gms/drive/b/a/j;->d:I

    int-to-long v2, v2

    rem-long/2addr v0, v2

    long-to-int v3, v0

    .line 218
    if-lez v3, :cond_0

    .line 220
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/j;->c:Lcom/google/android/gms/drive/b/a/m;

    invoke-interface {v0, v3}, Lcom/google/android/gms/drive/b/a/m;->a(I)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 221
    new-instance v5, Lcom/google/android/gms/drive/b/a/c;

    iget-wide v0, p0, Lcom/google/android/gms/drive/b/a/j;->a:J

    int-to-long v6, v3

    sub-long/2addr v0, v6

    iget-object v2, p0, Lcom/google/android/gms/drive/b/a/j;->c:Lcom/google/android/gms/drive/b/a/m;

    invoke-interface {v2, v3}, Lcom/google/android/gms/drive/b/a/m;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v0, v1, v2}, Lcom/google/android/gms/drive/b/a/c;-><init>(JLjava/lang/String;)V

    .line 224
    :goto_0
    new-instance v0, Lcom/google/android/gms/drive/b/a/i;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/j;->b:Lcom/google/android/gms/drive/b/a/d;

    iget v2, p0, Lcom/google/android/gms/drive/b/a/j;->d:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/b/a/i;-><init>(Lcom/google/android/gms/drive/b/a/d;IILjava/lang/Integer;Lcom/google/android/gms/drive/b/a/c;)V

    return-object v0

    :cond_0
    move-object v5, v4

    goto :goto_0
.end method

.method public final a(B)Lcom/google/android/gms/drive/b/a/j;
    .locals 8

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/j;->c:Lcom/google/android/gms/drive/b/a/m;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/b/a/m;->a(B)V

    .line 196
    iget-wide v0, p0, Lcom/google/android/gms/drive/b/a/j;->a:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/drive/b/a/j;->a:J

    .line 197
    iget-wide v0, p0, Lcom/google/android/gms/drive/b/a/j;->a:J

    iget-object v2, p0, Lcom/google/android/gms/drive/b/a/j;->c:Lcom/google/android/gms/drive/b/a/m;

    invoke-interface {v2}, Lcom/google/android/gms/drive/b/a/m;->a()I

    move-result v2

    int-to-long v2, v2

    rem-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_2

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/drive/b/a/j;->c:Lcom/google/android/gms/drive/b/a/m;

    invoke-interface {v0}, Lcom/google/android/gms/drive/b/a/m;->d()I

    move-result v0

    .line 200
    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/j;->c:Lcom/google/android/gms/drive/b/a/m;

    invoke-interface {v1}, Lcom/google/android/gms/drive/b/a/m;->e()Ljava/lang/String;

    move-result-object v1

    .line 201
    new-instance v2, Lcom/google/android/gms/drive/b/a/c;

    iget-wide v4, p0, Lcom/google/android/gms/drive/b/a/j;->a:J

    iget v3, p0, Lcom/google/android/gms/drive/b/a/j;->d:I

    int-to-long v6, v3

    sub-long/2addr v4, v6

    invoke-direct {v2, v4, v5, v1}, Lcom/google/android/gms/drive/b/a/c;-><init>(JLjava/lang/String;)V

    .line 202
    iget-object v1, p0, Lcom/google/android/gms/drive/b/a/j;->b:Lcom/google/android/gms/drive/b/a/d;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v0, v1, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/support/v4/g/t;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, v1, Lcom/google/android/gms/drive/b/a/d;->b:Landroid/support/v4/g/t;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v4, v3, v0}, Landroid/support/v4/g/t;->a(ILjava/lang/Object;)V

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/b/a/d;->a(Ljava/util/List;Lcom/google/android/gms/drive/b/a/c;)Z

    move-result v3

    if-nez v3, :cond_2

    :cond_1
    iget v3, v1, Lcom/google/android/gms/drive/b/a/d;->a:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v1, Lcom/google/android/gms/drive/b/a/d;->a:I

    invoke-interface {v0, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 204
    :cond_2
    return-object p0
.end method

.method public final a([BII)Lcom/google/android/gms/drive/b/a/j;
    .locals 2

    .prologue
    .line 208
    move v0, p2

    :goto_0
    add-int v1, p2, p3

    if-ge v0, v1, :cond_0

    .line 209
    aget-byte v1, p1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/drive/b/a/j;->a(B)Lcom/google/android/gms/drive/b/a/j;

    .line 208
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 211
    :cond_0
    return-object p0
.end method
