.class final Lcom/google/android/gms/update/af;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/update/UpdateFromSdCardService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/update/UpdateFromSdCardService;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/gms/update/af;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 228
    :try_start_0
    const-string v0, "UpdateFromSdCardService"

    const-string v1, "calling install package on recovery."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    iget-object v0, p0, Lcom/google/android/gms/update/af;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    iget-object v1, p0, Lcom/google/android/gms/update/af;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-static {v1}, Lcom/google/android/gms/update/UpdateFromSdCardService;->d(Lcom/google/android/gms/update/UpdateFromSdCardService;)Ljava/io/File;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/os/RecoverySystem;->installPackage(Landroid/content/Context;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 234
    :goto_0
    const-string v0, "UpdateFromSdCardService"

    const-string v1, "reboot to install failed"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    return-void

    .line 231
    :catch_0
    move-exception v0

    .line 232
    const-string v1, "UpdateFromSdCardService"

    const-string v2, "exception trying to install package"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
