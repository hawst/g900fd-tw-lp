.class public final Lcom/google/android/gms/wallet/common/r;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/o;
    .locals 1

    .prologue
    .line 41
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/o;->valueOf(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/o;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 43
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/gms/wallet/common/o;->c:Lcom/google/android/gms/wallet/common/o;

    goto :goto_0
.end method

.method static b(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/p;
    .locals 1

    .prologue
    .line 49
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/p;->valueOf(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/p;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 51
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/gms/wallet/common/p;->g:Lcom/google/android/gms/wallet/common/p;

    goto :goto_0
.end method

.method static c(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/q;
    .locals 1

    .prologue
    .line 57
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/q;->valueOf(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/q;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 59
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    sget-object v0, Lcom/google/android/gms/wallet/common/q;->b:Lcom/google/android/gms/wallet/common/q;

    goto :goto_0
.end method
