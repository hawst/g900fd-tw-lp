.class public final Lcom/google/android/gms/wallet/service/ow/j;
.super Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/wallet/service/ow/w;

.field private final c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private final d:Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

.field private final e:Z

.field private final f:J

.field private g:Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

.field private h:Ljava/lang/Runnable;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Lcom/google/android/apps/common/a/a/i;

.field private l:Lcom/google/android/apps/common/a/a/h;

.field private m:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/w;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 66
    invoke-virtual {p4}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->i:Z

    .line 58
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->j:Ljava/lang/String;

    .line 67
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ow/j;->a:Lcom/google/android/gms/wallet/service/ow/w;

    .line 68
    iput-object p3, p0, Lcom/google/android/gms/wallet/service/ow/j;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 69
    iput-object p4, p0, Lcom/google/android/gms/wallet/service/ow/j;->d:Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    .line 70
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->f:J

    .line 71
    iput-boolean p5, p0, Lcom/google/android/gms/wallet/service/ow/j;->e:Z

    .line 72
    if-eqz p5, :cond_0

    .line 73
    new-instance v0, Lcom/google/android/apps/common/a/a/i;

    const-string v1, "load_full_wallet"

    invoke-direct {v0, v1}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->k:Lcom/google/android/apps/common/a/a/i;

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->k:Lcom/google/android/apps/common/a/a/i;

    invoke-virtual {v0}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->l:Lcom/google/android/apps/common/a/a/h;

    .line 76
    :cond_0
    iput-object p6, p0, Lcom/google/android/gms/wallet/service/ow/j;->m:Ljava/lang/String;

    .line 77
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/w;Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/u;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/gms/wallet/shared/BuyFlowConfig;Z)Lcom/google/android/gms/wallet/service/ow/j;
    .locals 16

    .prologue
    .line 167
    invoke-static/range {p9 .. p9}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v2

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/wallet/shared/d;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/d;->a()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v12

    .line 170
    const/4 v8, 0x0

    .line 171
    const/4 v7, 0x0

    .line 172
    move-object/from16 v0, p3

    iget-boolean v2, v0, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    .line 173
    if-nez v2, :cond_0

    .line 174
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    .line 175
    invoke-static {v2}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 176
    if-eqz p4, :cond_2

    .line 177
    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/android/gms/wallet/Cart;)Lcom/google/aa/b/a/e;

    move-result-object v7

    :cond_0
    :goto_0
    move-object/from16 v2, p3

    move-object/from16 v3, p5

    move-object/from16 v4, p6

    move/from16 v5, p7

    move/from16 v6, p8

    move/from16 v9, p10

    .line 184
    invoke-static/range {v2 .. v9}, Lcom/google/android/gms/wallet/service/ow/ao;->a(Lcom/google/aa/b/a/a/a/a/u;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/aa/b/a/e;Ljava/lang/String;Z)Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v13

    .line 189
    new-instance v3, Ljava/math/BigDecimal;

    sget-object v2, Lcom/google/android/gms/wallet/b/c;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-double v4, v2

    const-wide/high16 v6, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v6

    invoke-direct {v3, v4, v5}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-virtual {v3}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v6

    .line 191
    new-instance v3, Ljava/math/BigDecimal;

    sget-object v2, Lcom/google/android/gms/wallet/b/c;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-double v4, v2

    const-wide/high16 v10, 0x4059000000000000L    # 100.0

    div-double/2addr v4, v10

    invoke-direct {v3, v4, v5}, Ljava/math/BigDecimal;-><init>(D)V

    invoke-virtual {v3}, Ljava/math/BigDecimal;->doubleValue()D

    move-result-wide v14

    .line 193
    const-wide/16 v10, 0x0

    .line 195
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 196
    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v8}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Ljava/math/BigDecimal;->scaleByPowerOfTen(I)Ljava/math/BigDecimal;

    move-result-object v2

    invoke-virtual {v2}, Ljava/math/BigDecimal;->longValue()J

    move-result-wide v10

    .line 200
    :cond_1
    move-object/from16 v0, p2

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    move-object/from16 v2, p0

    move-object v3, v12

    move-wide v8, v14

    invoke-static/range {v2 .. v11}, Lcom/google/android/gms/wallet/analytics/events/OwPreFetchFullWalletRequestedEvent;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Ljava/lang/String;Ljava/lang/String;DDJ)Ljava/lang/String;

    move-result-object v8

    .line 204
    new-instance v2, Lcom/google/android/gms/wallet/service/ow/j;

    new-instance v6, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    iget-object v3, v3, Lcom/google/aa/b/a/a/a/a/b;->a:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-direct {v6, v0, v13, v3}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;-><init>(Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/i;Ljava/lang/String;)V

    const/4 v7, 0x1

    move-object/from16 v3, p0

    move-object/from16 v4, p1

    move-object v5, v12

    invoke-direct/range {v2 .. v8}, Lcom/google/android/gms/wallet/service/ow/j;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/w;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;ZLjava/lang/String;)V

    return-object v2

    .line 179
    :cond_2
    new-instance v7, Lcom/google/aa/b/a/e;

    invoke-direct {v7}, Lcom/google/aa/b/a/e;-><init>()V

    .line 180
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    iput-object v3, v7, Lcom/google/aa/b/a/e;->b:Ljava/lang/String;

    .line 181
    iput-object v2, v7, Lcom/google/aa/b/a/e;->a:Ljava/lang/String;

    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/w;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/j;
    .locals 7

    .prologue
    .line 155
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/j;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/wallet/service/ow/j;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/service/ow/w;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;ZLjava/lang/String;)V

    return-object v0
.end method

.method private declared-synchronized j()V
    .locals 1

    .prologue
    .line 134
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->i:Z

    .line 135
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->h:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->h:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 138
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->h:Ljava/lang/Runnable;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    monitor-exit p0

    return-void

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized k()V
    .locals 5

    .prologue
    .line 142
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->j:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 151
    :goto_0
    monitor-exit p0

    return-void

    .line 145
    :cond_0
    :try_start_1
    new-instance v1, Lcom/google/android/gms/wallet/common/b;

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/j;->d:Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/wallet/common/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 147
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/ow/j;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "full_wallet_success"

    .line 149
    :goto_1
    const-string v2, "get_full_wallet"

    iget-object v3, p0, Lcom/google/android/gms/wallet/service/ow/j;->j:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 142
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 147
    :cond_1
    :try_start_2
    const-string v0, "full_wallet_failure"
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private varargs l()Ljava/lang/Void;
    .locals 3

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 233
    const-string v0, "PreFetchFullWalletTask"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "FetchFullWalletTask started,isPrefetch="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/service/ow/j;->e:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ",googTxnId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/j;->d:Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;->b()Lcom/google/aa/b/a/a/a/a/i;

    move-result-object v2

    iget-object v2, v2, Lcom/google/aa/b/a/a/a/a/i;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->a:Lcom/google/android/gms/wallet/service/ow/w;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/j;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/ow/j;->d:Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/service/ow/w;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->g:Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 242
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ow/j;->j()V

    .line 243
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ow/j;->k()V

    .line 244
    const-string v0, "PreFetchFullWalletTask"

    const-string v1, "FetchFullWalletTask completed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :goto_0
    const/4 v0, 0x0

    return-object v0

    .line 240
    :catch_0
    move-exception v0

    :try_start_1
    const-string v0, "PreFetchFullWalletTask"

    const-string v1, "Exception during fetching of full wallet"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 242
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ow/j;->j()V

    .line 243
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ow/j;->k()V

    .line 244
    const-string v0, "PreFetchFullWalletTask"

    const-string v1, "FetchFullWalletTask completed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 242
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ow/j;->j()V

    .line 243
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ow/j;->k()V

    .line 244
    const-string v1, "PreFetchFullWalletTask"

    const-string v2, "FetchFullWalletTask completed"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    throw v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 80
    iget-wide v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->f:J

    return-wide v0
.end method

.method protected final synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ow/j;->l()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final declared-synchronized a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 108
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/j;->h:Ljava/lang/Runnable;

    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/ow/j;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ow/j;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 112
    :cond_0
    monitor-exit p0

    return-void

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 115
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ow/j;->j:Ljava/lang/String;

    .line 116
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/service/ow/j;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/ow/j;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 119
    :cond_0
    monitor-exit p0

    return-void

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 84
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->e:Z

    return v0
.end method

.method public final c()Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->d:Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    return-object v0
.end method

.method public final e()Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->g:Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    return-object v0
.end method

.method public final declared-synchronized f()Z
    .locals 1

    .prologue
    .line 100
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final h()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->k:Lcom/google/android/apps/common/a/a/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->l:Lcom/google/android/apps/common/a/a/h;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->k:Lcom/google/android/apps/common/a/a/i;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/j;->l:Lcom/google/android/apps/common/a/a/h;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "prefetch_to_actual_service_call"

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/ow/j;->k:Lcom/google/android/apps/common/a/a/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 128
    iput-object v5, p0, Lcom/google/android/gms/wallet/service/ow/j;->k:Lcom/google/android/apps/common/a/a/i;

    .line 129
    iput-object v5, p0, Lcom/google/android/gms/wallet/service/ow/j;->l:Lcom/google/android/apps/common/a/a/h;

    .line 131
    :cond_0
    return-void
.end method

.method public final i()Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 215
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ow/j;->g:Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    .line 216
    if-nez v0, :cond_0

    move v0, v1

    .line 227
    :goto_0
    return v0

    .line 219
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->b()Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    .line 220
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->a()I

    move-result v2

    const/16 v3, 0x10

    if-eq v2, v3, :cond_1

    move v0, v1

    .line 221
    goto :goto_0

    .line 223
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/service/ServerResponse;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/a/a/a/j;

    .line 224
    iget-object v0, v0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 225
    goto :goto_0

    .line 227
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
