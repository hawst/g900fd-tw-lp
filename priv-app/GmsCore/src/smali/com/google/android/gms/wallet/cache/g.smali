.class public final Lcom/google/android/gms/wallet/cache/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Z

.field public c:Ljava/lang/String;

.field public d:Z

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/gms/wallet/cache/g;->a:I

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/cache/g;->b:Z

    .line 76
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/cache/g;->c:Ljava/lang/String;

    .line 77
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/cache/g;->d:Z

    .line 78
    iput-boolean v1, p0, Lcom/google/android/gms/wallet/cache/g;->e:Z

    return-void
.end method


# virtual methods
.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 82
    invoke-static {}, Lcom/google/android/gms/wallet/common/af;->a()Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/wallet/cache/g;->a:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(I)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/cache/g;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(Z)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/cache/g;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/cache/g;->d:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(Z)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/cache/g;->e:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ah;->a(Z)Lcom/google/android/gms/wallet/common/ah;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ah;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
