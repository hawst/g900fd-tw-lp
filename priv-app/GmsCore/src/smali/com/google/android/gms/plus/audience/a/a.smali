.class public final Lcom/google/android/gms/plus/audience/a/a;
.super Lcom/google/android/gms/plus/audience/a/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field private final d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    .line 26
    const/4 v6, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/plus/audience/a/e;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 29
    iput-boolean p6, p0, Lcom/google/android/gms/plus/audience/a/a;->d:Z

    .line 30
    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 2

    .prologue
    .line 18
    check-cast p1, Lcom/google/android/gms/people/e;

    invoke-interface {p1}, Lcom/google/android/gms/people/e;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/people/e;->c()Lcom/google/android/gms/people/model/e;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/plus/audience/a/a;->a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/common/data/d;)V

    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/api/v;)V
    .locals 5

    .prologue
    .line 34
    sget-object v0, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v1, p0, Lcom/google/android/gms/plus/audience/a/e;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/audience/a/e;->h:Ljava/lang/String;

    new-instance v3, Lcom/google/android/gms/people/d;

    invoke-direct {v3}, Lcom/google/android/gms/people/d;-><init>()V

    const/4 v4, -0x1

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/d;->a(I)Lcom/google/android/gms/people/d;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/audience/a/e;->i:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/d;->a(Ljava/lang/String;)Lcom/google/android/gms/people/d;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/gms/plus/audience/a/a;->d:Z

    invoke-virtual {v3, v4}, Lcom/google/android/gms/people/d;->a(Z)Lcom/google/android/gms/people/d;

    move-result-object v3

    invoke-interface {v0, p1, v1, v2, v3}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/d;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 40
    return-void
.end method
