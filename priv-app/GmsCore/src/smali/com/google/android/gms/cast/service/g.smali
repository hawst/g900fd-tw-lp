.class final Lcom/google/android/gms/cast/service/g;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/service/CastService;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/cast/service/CastService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/gms/cast/service/g;->a:Lcom/google/android/gms/cast/service/CastService;

    .line 72
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 73
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/cast/service/CastService;Landroid/content/Context;B)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/cast/service/g;-><init>(Lcom/google/android/gms/cast/service/CastService;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 15

    .prologue
    .line 79
    new-instance v14, Lcom/google/android/gms/cast/e/h;

    const-string v2, "CastService"

    invoke-direct {v14, v2}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    .line 80
    const-string v2, "instance-%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/android/gms/cast/service/CastService;->a()Ljava/util/concurrent/atomic/AtomicInteger;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v14, v2}, Lcom/google/android/gms/cast/e/h;->a(Ljava/lang/String;)V

    .line 83
    :try_start_0
    invoke-static/range {p5 .. p5}, Lcom/google/android/gms/cast/CastDevice;->b(Landroid/os/Bundle;)Lcom/google/android/gms/cast/CastDevice;

    move-result-object v6

    .line 87
    const-string v2, "last_application_id"

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 89
    const-string v2, "last_session_id"

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    .line 100
    const-string v2, "com.google.android.gms.cast.EXTRA_CAST_FLAGS"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p5

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    .line 102
    invoke-virtual {v14}, Lcom/google/android/gms/cast/e/h;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 103
    const-string v2, "%s, connecting to device with lastApplicationId=%s, lastSessionId=%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p3, v3, v4

    const/4 v4, 0x1

    aput-object v7, v3, v4

    const/4 v4, 0x2

    aput-object v8, v3, v4

    invoke-virtual {v14, v2, v3}, Lcom/google/android/gms/cast/e/h;->g(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 110
    :goto_0
    invoke-static/range {p4 .. p4}, Lcom/google/android/gms/cast/internal/h;->a(Landroid/os/IBinder;)Lcom/google/android/gms/cast/internal/g;

    move-result-object v9

    .line 113
    new-instance v3, Lcom/google/android/gms/cast/service/d;

    iget-object v4, p0, Lcom/google/android/gms/cast/service/g;->a:Lcom/google/android/gms/cast/service/CastService;

    move-object/from16 v5, p1

    move/from16 v10, p2

    move-object/from16 v11, p3

    invoke-direct/range {v3 .. v14}, Lcom/google/android/gms/cast/service/d;-><init>(Lcom/google/android/gms/cast/service/CastService;Lcom/google/android/gms/common/internal/bg;Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/cast/internal/g;ILjava/lang/String;JLcom/google/android/gms/cast/internal/k;)V

    .line 115
    :goto_1
    return-void

    .line 90
    :catch_0
    move-exception v2

    .line 91
    const-string v3, "Cast device was not valid."

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v14, v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 93
    const/16 v2, 0xa

    const/4 v3, 0x0

    const/4 v4, 0x0

    :try_start_1
    move-object/from16 v0, p1

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 95
    :catch_1
    move-exception v2

    const-string v2, "client died while brokering service"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v14, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_1

    .line 106
    :cond_0
    const-string v2, "connecting to device with lastApplicationId=%s, lastSessionId=%s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v7, v3, v4

    const/4 v4, 0x1

    aput-object v8, v3, v4

    invoke-virtual {v14, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
