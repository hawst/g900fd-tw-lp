.class public Lcom/google/android/gms/photos/PhotosModule;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/social/a/f;


# annotations
.annotation build Lcom/google/android/apps/common/proguard/UsedByReflection;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Ljava/lang/Class;Lcom/google/android/libraries/social/a/a;)V
    .locals 6

    .prologue
    .line 40
    const-class v0, Lcom/google/android/libraries/social/mediamonitor/a;

    if-ne p2, v0, :cond_1

    .line 41
    const-class v0, Lcom/google/android/libraries/social/mediamonitor/a;

    new-instance v1, Lcom/google/android/libraries/social/autobackup/q;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/autobackup/q;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 43
    :cond_1
    const-class v0, Lcom/google/android/libraries/social/autobackup/b;

    if-ne p2, v0, :cond_2

    .line 44
    const-class v0, Lcom/google/android/libraries/social/autobackup/b;

    new-instance v1, Lcom/google/android/gms/photos/d;

    invoke-direct {v1}, Lcom/google/android/gms/photos/d;-><init>()V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 46
    :cond_2
    const-class v0, Lcom/google/android/libraries/social/autobackup/c/a;

    if-ne p2, v0, :cond_3

    .line 47
    const-class v0, Lcom/google/android/libraries/social/autobackup/c/a;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/t;

    invoke-direct {v1}, Lcom/google/android/gms/photos/autobackup/t;-><init>()V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 48
    :cond_3
    const-class v0, Lcom/google/android/gms/photos/autobackup/a;

    if-ne p2, v0, :cond_4

    .line 49
    const-class v0, Lcom/google/android/gms/photos/autobackup/a;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/a;

    invoke-direct {v1, p1}, Lcom/google/android/gms/photos/autobackup/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 50
    :cond_4
    const-class v0, Lcom/google/android/gms/photos/autobackup/service/a/a;

    if-ne p2, v0, :cond_5

    .line 51
    const-class v0, Lcom/google/android/gms/photos/autobackup/service/a/a;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/service/a/a;

    invoke-direct {v1, p1}, Lcom/google/android/gms/photos/autobackup/service/a/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 53
    :cond_5
    const-class v0, Lcom/google/android/libraries/social/autobackup/i;

    if-ne p2, v0, :cond_6

    .line 54
    const-class v0, Lcom/google/android/libraries/social/autobackup/i;

    invoke-static {p1}, Lcom/google/android/libraries/social/autobackup/i;->a(Landroid/content/Context;)Lcom/google/android/libraries/social/autobackup/i;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 55
    :cond_6
    const-class v0, Lcom/google/android/libraries/social/autobackup/a;

    if-ne p2, v0, :cond_7

    .line 56
    const-class v0, Lcom/google/android/libraries/social/autobackup/a;

    new-instance v1, Lcom/google/android/gms/photos/c;

    invoke-direct {v1, p1}, Lcom/google/android/gms/photos/c;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 57
    :cond_7
    const-class v0, Lcom/google/android/libraries/social/rpc/p;

    if-ne p2, v0, :cond_8

    .line 58
    const-class v0, Lcom/google/android/libraries/social/rpc/p;

    new-instance v1, Lcom/google/android/gms/photos/a;

    invoke-direct {v1}, Lcom/google/android/gms/photos/a;-><init>()V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 59
    :cond_8
    const-class v0, Lcom/google/android/libraries/social/h/b/a;

    if-ne p2, v0, :cond_9

    .line 60
    const-class v0, Lcom/google/android/libraries/social/h/b/a;

    new-instance v1, Lcom/google/android/libraries/social/h/b/b;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/h/b/b;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto :goto_0

    .line 61
    :cond_9
    const-class v0, Lcom/google/android/libraries/social/h/a/b;

    if-ne p2, v0, :cond_a

    .line 62
    const-class v0, Lcom/google/android/libraries/social/h/a/b;

    new-instance v1, Lcom/google/android/libraries/social/h/a/a;

    invoke-direct {v1}, Lcom/google/android/libraries/social/h/a/a;-><init>()V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto/16 :goto_0

    .line 63
    :cond_a
    const-class v0, Le/a/a/i;

    if-ne p2, v0, :cond_b

    .line 64
    const-class v0, Le/a/a/i;

    new-instance v1, Le/a/a/i;

    invoke-direct {v1}, Le/a/a/i;-><init>()V

    sget-object v2, Le/a/a/k;->a:Le/a/a/k;

    const-wide/16 v4, -0x1

    invoke-virtual {v1, v2, v4, v5}, Le/a/a/i;->a(Le/a/a/k;J)Le/a/a/i;

    move-result-object v1

    invoke-virtual {v1}, Le/a/a/i;->b()Le/a/a/i;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto/16 :goto_0

    .line 67
    :cond_b
    const-class v0, Lcom/google/android/libraries/social/o/a;

    if-ne p2, v0, :cond_c

    .line 68
    const-class v0, Lcom/google/android/libraries/social/o/a;

    new-instance v1, Lcom/google/android/libraries/social/o/a/a;

    invoke-direct {v1}, Lcom/google/android/libraries/social/o/a/a;-><init>()V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto/16 :goto_0

    .line 69
    :cond_c
    const-class v0, Lcom/google/android/libraries/social/account/refresh/a/a;

    if-ne p2, v0, :cond_d

    .line 70
    const-class v0, Lcom/google/android/libraries/social/account/refresh/a/a;

    new-instance v1, Lcom/google/android/libraries/social/account/refresh/a/a/a;

    invoke-direct {v1, p1}, Lcom/google/android/libraries/social/account/refresh/a/a/a;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto/16 :goto_0

    .line 71
    :cond_d
    const-class v0, Lcom/google/android/gms/photos/e;

    if-ne p2, v0, :cond_e

    .line 72
    const-class v0, Lcom/google/android/gms/photos/e;

    new-instance v1, Lcom/google/android/gms/photos/e;

    invoke-direct {v1, p1}, Lcom/google/android/gms/photos/e;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;Ljava/lang/Object;)Lcom/google/android/libraries/social/a/a;

    goto/16 :goto_0

    .line 73
    :cond_e
    const-class v0, Lcom/google/android/libraries/social/account/a;

    if-ne p2, v0, :cond_f

    .line 74
    const-class v0, Lcom/google/android/libraries/social/account/a;

    const-class v1, Lcom/google/android/gms/photos/autobackup/a;

    invoke-virtual {p3, v1}, Lcom/google/android/libraries/social/a/a;->a(Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 76
    :cond_f
    const-class v0, Lcom/google/android/libraries/social/autobackup/h;

    if-ne p2, v0, :cond_0

    .line 77
    const-class v0, Lcom/google/android/libraries/social/autobackup/h;

    new-instance v1, Lcom/google/android/gms/photos/autobackup/service/d;

    invoke-direct {v1, p1}, Lcom/google/android/gms/photos/autobackup/service/d;-><init>(Landroid/content/Context;)V

    invoke-virtual {p3, v0, v1}, Lcom/google/android/libraries/social/a/a;->b(Ljava/lang/Class;Ljava/lang/Object;)V

    goto/16 :goto_0
.end method
