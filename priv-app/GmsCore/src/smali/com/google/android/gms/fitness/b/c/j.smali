.class public final Lcom/google/android/gms/fitness/b/c/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/b/ad;


# instance fields
.field private final a:Lcom/google/android/gms/fitness/b/b;

.field private final b:Z

.field private final c:Z

.field private final d:J

.field private final e:J


# direct methods
.method public constructor <init>(ZLcom/google/android/gms/fitness/b/b;JJ)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    iput-boolean v0, p0, Lcom/google/android/gms/fitness/b/c/j;->b:Z

    .line 62
    iput-object p2, p0, Lcom/google/android/gms/fitness/b/c/j;->a:Lcom/google/android/gms/fitness/b/b;

    .line 63
    iput-boolean v0, p0, Lcom/google/android/gms/fitness/b/c/j;->c:Z

    .line 64
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p3, p4}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/fitness/b/c/j;->d:J

    .line 65
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, p5, p6}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/fitness/b/c/j;->e:J

    .line 66
    return-void
.end method

.method private static a(III)I
    .locals 3

    .prologue
    const/4 v0, 0x6

    const/4 v1, 0x2

    .line 224
    const/4 v2, 0x1

    if-ne p1, v2, :cond_2

    .line 225
    if-nez p0, :cond_1

    if-nez p2, :cond_1

    const/4 p1, 0x0

    .line 234
    :cond_0
    :goto_0
    return p1

    :cond_1
    move p1, v0

    .line 225
    goto :goto_0

    .line 229
    :cond_2
    const/4 v2, 0x3

    if-ne p1, v2, :cond_0

    .line 230
    if-ne p0, v1, :cond_3

    if-ne p2, v1, :cond_3

    move p1, v1

    goto :goto_0

    :cond_3
    move p1, v0

    goto :goto_0
.end method

.method private static a(ILjava/util/ListIterator;)I
    .locals 2

    .prologue
    .line 173
    :cond_0
    invoke-interface {p1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 174
    invoke-interface {p1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/activity/b/b;

    .line 175
    iget v1, v0, Lcom/google/android/location/activity/b/b;->b:I

    if-eq v1, p0, :cond_0

    .line 176
    iget v0, v0, Lcom/google/android/location/activity/b/b;->b:I

    .line 179
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x7

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/d;JJIILcom/google/android/gms/fitness/b/e;)Lcom/google/android/gms/fitness/b/d;
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x7

    .line 196
    cmp-long v2, p2, p4

    if-nez v2, :cond_0

    move-object p1, v1

    .line 216
    :goto_0
    return-object p1

    .line 200
    :cond_0
    if-ne p6, v0, :cond_1

    move-object p1, v1

    .line 201
    goto :goto_0

    .line 204
    :cond_1
    if-ne p6, p7, :cond_2

    if-eqz p1, :cond_2

    .line 205
    sget-object v0, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {p1, p4, p5, v0}, Lcom/google/android/gms/fitness/b/d;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/b/d;

    goto :goto_0

    .line 209
    :cond_2
    invoke-interface {p0}, Lcom/google/android/gms/fitness/b/s;->b()Lcom/google/android/gms/fitness/b/d;

    move-result-object p1

    .line 210
    invoke-interface {p1, p8}, Lcom/google/android/gms/fitness/b/d;->a(Lcom/google/android/gms/fitness/b/e;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, p2, p3, v2}, Lcom/google/android/gms/fitness/b/d;->b(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, p4, p5, v2}, Lcom/google/android/gms/fitness/b/d;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/fitness/b/d;->a()Lcom/google/android/gms/fitness/b/k;

    move-result-object v1

    packed-switch p6, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown state: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x0

    :goto_1
    :pswitch_1
    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/b/k;->a(I)Lcom/google/android/gms/fitness/b/k;

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    goto :goto_1

    :pswitch_3
    const/16 v0, 0x8

    goto :goto_1

    :pswitch_4
    const/4 v0, 0x3

    goto :goto_1

    :pswitch_5
    const/4 v0, 0x4

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private a(Lcom/google/android/gms/fitness/b/f;Ljava/lang/String;Lcom/google/android/gms/fitness/b/i;)Lcom/google/android/gms/fitness/b/f;
    .locals 3

    .prologue
    .line 357
    const-string v0, "com.google.activity.segment"

    invoke-interface {p1, v0}, Lcom/google/android/gms/fitness/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/b/h;->a:Lcom/google/android/gms/fitness/b/h;

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/f;->a(Lcom/google/android/gms/fitness/b/h;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    const-string v1, "from_sample"

    iget-boolean v2, p0, Lcom/google/android/gms/fitness/b/c/j;->b:Z

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/b/c/v;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/google/android/gms/fitness/b/c/v;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/b/c/j;->a:Lcom/google/android/gms/fitness/b/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/f;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/f;

    .line 363
    if-eqz p3, :cond_0

    .line 364
    invoke-interface {p1, p3}, Lcom/google/android/gms/fitness/b/f;->a(Lcom/google/android/gms/fitness/b/i;)Lcom/google/android/gms/fitness/b/f;

    .line 366
    :cond_0
    return-object p1
.end method

.method private a(Lcom/google/android/gms/fitness/b/r;JJLcom/google/android/gms/fitness/b/t;)Lcom/google/android/gms/fitness/b/r;
    .locals 22

    .prologue
    .line 82
    invoke-interface/range {p6 .. p6}, Lcom/google/android/gms/fitness/b/t;->a()Lcom/google/android/gms/fitness/b/s;

    move-result-object v15

    .line 83
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/fitness/b/r;->c()Lcom/google/android/gms/fitness/b/e;

    move-result-object v10

    .line 84
    invoke-interface {v15}, Lcom/google/android/gms/fitness/b/s;->a()Lcom/google/android/gms/fitness/b/f;

    move-result-object v2

    invoke-interface {v10}, Lcom/google/android/gms/fitness/b/e;->c()Ljava/lang/String;

    move-result-object v3

    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/fitness/b/r;->c()Lcom/google/android/gms/fitness/b/e;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/fitness/b/e;->f()Lcom/google/android/gms/fitness/b/i;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/fitness/b/c/j;->a(Lcom/google/android/gms/fitness/b/f;Ljava/lang/String;Lcom/google/android/gms/fitness/b/i;)Lcom/google/android/gms/fitness/b/f;

    .line 89
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/fitness/b/r;->b()I

    move-result v2

    if-nez v2, :cond_0

    .line 90
    invoke-interface {v15}, Lcom/google/android/gms/fitness/b/s;->d()Lcom/google/android/gms/fitness/b/r;

    move-result-object v2

    .line 166
    :goto_0
    return-object v2

    .line 93
    :cond_0
    const/4 v13, 0x0

    .line 96
    const/4 v12, 0x7

    .line 97
    const/4 v11, 0x7

    .line 100
    new-instance v14, Lcom/google/android/location/activity/b/a;

    const/4 v2, 0x0

    invoke-direct {v14, v2}, Lcom/google/android/location/activity/b/a;-><init>(B)V

    .line 101
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/fitness/b/r;->a()Ljava/util/List;

    move-result-object v6

    new-instance v16, Ljava/util/ArrayList;

    const/4 v2, 0x1

    move-object/from16 v0, v16

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-static/range {v16 .. v16}, Lcom/google/android/gms/fitness/b/c/j;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    const-wide/16 v3, 0x0

    new-instance v5, Ljava/util/ArrayList;

    const/4 v7, 0x5

    invoke-direct {v5, v7}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v17

    move-object v9, v2

    :goto_1
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/google/android/gms/fitness/b/c;

    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v8, v2}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    cmp-long v2, v6, v3

    if-eqz v2, :cond_f

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/fitness/b/c/j;->a(JLjava/util/List;J)Lcom/google/android/location/activity/b/g;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_1
    sub-long v2, v6, v3

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gms/fitness/b/c/j;->d:J

    move-wide/from16 v18, v0

    cmp-long v2, v2, v18

    if-lez v2, :cond_2

    invoke-interface {v9}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-static/range {v16 .. v16}, Lcom/google/android/gms/fitness/b/c/j;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v9

    :cond_2
    invoke-interface {v5}, Ljava/util/List;->clear()V

    move-wide v3, v6

    move-object v2, v9

    :goto_2
    invoke-interface {v5, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object v9, v2

    goto :goto_1

    :cond_3
    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/fitness/b/c/j;->a(JLjava/util/List;J)Lcom/google/android/location/activity/b/g;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    :cond_4
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 104
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    .line 105
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/gms/fitness/b/c/j;->c:Z

    invoke-virtual {v14, v2, v5}, Lcom/google/android/location/activity/b/a;->a(Ljava/util/List;Z)Ljava/util/List;

    move-result-object v2

    invoke-interface {v3, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    .line 109
    :cond_5
    invoke-interface {v3}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v16

    move v9, v11

    move v8, v12

    move-wide/from16 v6, p2

    move-wide/from16 v4, p2

    move-object v3, v13

    .line 110
    :goto_4
    invoke-interface/range {v16 .. v16}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 111
    invoke-interface/range {v16 .. v16}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v11, v2

    check-cast v11, Lcom/google/android/location/activity/b/b;

    .line 113
    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    iget-wide v12, v11, Lcom/google/android/location/activity/b/b;->a:J

    invoke-virtual {v2, v12, v13}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v12

    .line 116
    cmp-long v2, v12, p4

    if-lez v2, :cond_8

    .line 118
    invoke-interface/range {v16 .. v16}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-wide/from16 v6, p4

    .line 154
    :cond_6
    cmp-long v2, v4, p2

    if-ltz v2, :cond_7

    cmp-long v2, v4, p4

    if-gez v2, :cond_7

    cmp-long v2, v6, p2

    if-ltz v2, :cond_7

    .line 159
    move-object/from16 v0, v16

    invoke-static {v8, v0}, Lcom/google/android/gms/fitness/b/c/j;->a(ILjava/util/ListIterator;)I

    move-result v2

    .line 160
    invoke-static {v9, v8, v2}, Lcom/google/android/gms/fitness/b/c/j;->a(III)I

    move-result v8

    move-object v2, v15

    .line 161
    invoke-static/range {v2 .. v10}, Lcom/google/android/gms/fitness/b/c/j;->a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/d;JJIILcom/google/android/gms/fitness/b/e;)Lcom/google/android/gms/fitness/b/d;

    .line 166
    :cond_7
    invoke-interface {v15}, Lcom/google/android/gms/fitness/b/s;->d()Lcom/google/android/gms/fitness/b/r;

    move-result-object v2

    goto/16 :goto_0

    .line 123
    :cond_8
    sub-long v18, v12, v6

    .line 124
    const/4 v2, 0x6

    if-ne v8, v2, :cond_c

    const/4 v2, 0x1

    .line 125
    :goto_5
    if-nez v2, :cond_d

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gms/fitness/b/c/j;->d:J

    move-wide/from16 v20, v0

    cmp-long v2, v18, v20

    if-lez v2, :cond_d

    const/4 v2, 0x1

    move v14, v2

    .line 128
    :goto_6
    iget v2, v11, Lcom/google/android/location/activity/b/b;->b:I

    if-ne v2, v8, :cond_9

    if-eqz v14, :cond_b

    .line 131
    :cond_9
    if-eqz v14, :cond_e

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/gms/fitness/b/c/j;->d:J

    move-wide/from16 v18, v0

    add-long v6, v6, v18

    .line 137
    :goto_7
    cmp-long v2, v6, p2

    if-lez v2, :cond_a

    .line 138
    iget v2, v11, Lcom/google/android/location/activity/b/b;->b:I

    invoke-static {v9, v8, v2}, Lcom/google/android/gms/fitness/b/c/j;->a(III)I

    move-result v8

    move-object v2, v15

    .line 139
    invoke-static/range {v2 .. v10}, Lcom/google/android/gms/fitness/b/c/j;->a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/d;JJIILcom/google/android/gms/fitness/b/e;)Lcom/google/android/gms/fitness/b/d;

    move-result-object v3

    .line 142
    if-eqz v14, :cond_a

    .line 143
    const/4 v3, 0x0

    .line 147
    :cond_a
    invoke-static {v4, v5, v12, v13}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    .line 149
    iget v2, v11, Lcom/google/android/location/activity/b/b;->b:I

    move v9, v8

    move v8, v2

    :cond_b
    move-wide v6, v12

    .line 151
    goto :goto_4

    .line 124
    :cond_c
    const/4 v2, 0x0

    goto :goto_5

    .line 125
    :cond_d
    const/4 v2, 0x0

    move v14, v2

    goto :goto_6

    :cond_e
    move-wide v6, v12

    .line 131
    goto :goto_7

    :cond_f
    move-object v2, v9

    goto/16 :goto_2
.end method

.method private a(JLjava/util/List;J)Lcom/google/android/location/activity/b/g;
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 289
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {p3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/c;

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/c;->d()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/b/j;

    invoke-interface {v1}, Lcom/google/android/gms/fitness/b/j;->a()I

    move-result v1

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/c;->d()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/j;

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/j;->c()F

    move-result v0

    float-to-int v0, v0

    new-instance v6, Lcom/google/android/location/activity/b/h;

    invoke-direct {v6, v1, v0}, Lcom/google/android/location/activity/b/h;-><init>(II)V

    invoke-interface {v4, v6}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 292
    :cond_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v2, :cond_2

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/location/activity/b/h;

    iget v0, v0, Lcom/google/android/location/activity/b/h;->a:I

    const/4 v5, 0x5

    if-ne v0, v5, :cond_2

    move v0, v2

    :goto_1
    if-eqz v0, :cond_1

    sub-long v2, p4, p1

    iget-wide v6, p0, Lcom/google/android/gms/fitness/b/c/j;->e:J

    cmp-long v0, v2, v6

    if-lez v0, :cond_1

    iget-wide v2, p0, Lcom/google/android/gms/fitness/b/c/j;->e:J

    sub-long p1, p4, v2

    :cond_1
    invoke-virtual {v1, p1, p2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    .line 294
    new-instance v2, Lcom/google/android/location/activity/b/g;

    new-instance v3, Lcom/google/android/location/activity/b/i;

    invoke-direct {v3, v4, v0, v1}, Lcom/google/android/location/activity/b/i;-><init>(Ljava/util/List;J)V

    invoke-direct {v2, v3}, Lcom/google/android/location/activity/b/g;-><init>(Lcom/google/android/location/activity/b/i;)V

    return-object v2

    :cond_2
    move v0, v3

    .line 292
    goto :goto_1
.end method

.method private static a(Ljava/util/List;)Ljava/util/List;
    .locals 2

    .prologue
    .line 279
    new-instance v0, Ljava/util/ArrayList;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 280
    invoke-interface {p0, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 281
    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 332
    const-string v0, "com.google.activity.segment"

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/fitness/b/g;Ljava/util/List;)Ljava/util/List;
    .locals 5

    .prologue
    .line 339
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 340
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/g;->a()Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    const-string v1, "{source_stream_id}"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/fitness/b/c/j;->a(Lcom/google/android/gms/fitness/b/f;Ljava/lang/String;Lcom/google/android/gms/fitness/b/i;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/f;->a()Lcom/google/android/gms/fitness/b/e;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 352
    :goto_0
    return-object v0

    .line 344
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 345
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/e;

    .line 346
    const-string v3, "com.google.activity.sample"

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/e;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 348
    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/g;->a()Lcom/google/android/gms/fitness/b/f;

    move-result-object v3

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/e;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/e;->f()Lcom/google/android/gms/fitness/b/i;

    move-result-object v0

    invoke-direct {p0, v3, v4, v0}, Lcom/google/android/gms/fitness/b/c/j;->a(Lcom/google/android/gms/fitness/b/f;Ljava/lang/String;Lcom/google/android/gms/fitness/b/i;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/f;->a()Lcom/google/android/gms/fitness/b/e;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 352
    goto :goto_0
.end method

.method public final a(Ljava/util/List;JJLcom/google/android/gms/fitness/b/t;)Ljava/util/List;
    .locals 10

    .prologue
    .line 71
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 72
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/fitness/b/r;

    move-object v0, p0

    move-wide v2, p2

    move-wide v4, p4

    move-object/from16 v6, p6

    .line 73
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/fitness/b/c/j;->a(Lcom/google/android/gms/fitness/b/r;JJLcom/google/android/gms/fitness/b/t;)Lcom/google/android/gms/fitness/b/r;

    move-result-object v0

    .line 75
    invoke-interface {v7, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 77
    :cond_0
    return-object v7
.end method

.method public final b()Ljava/util/List;
    .locals 4

    .prologue
    const/16 v3, 0xe10

    const/4 v2, 0x1

    .line 371
    new-instance v0, Lcom/google/android/gms/fitness/b/v;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/b/v;-><init>()V

    const-string v1, "com.google.activity.sample"

    iput-object v1, v0, Lcom/google/android/gms/fitness/b/v;->a:Ljava/lang/String;

    new-instance v1, Lcom/google/android/gms/fitness/b/c/k;

    invoke-direct {v1, p0}, Lcom/google/android/gms/fitness/b/c/k;-><init>(Lcom/google/android/gms/fitness/b/c/j;)V

    iput-object v1, v0, Lcom/google/android/gms/fitness/b/v;->b:Lcom/google/android/gms/fitness/b/o;

    iput v2, v0, Lcom/google/android/gms/fitness/b/v;->j:I

    iput v2, v0, Lcom/google/android/gms/fitness/b/v;->h:I

    iput v3, v0, Lcom/google/android/gms/fitness/b/v;->i:I

    iput v3, v0, Lcom/google/android/gms/fitness/b/v;->g:I

    iput-boolean v2, v0, Lcom/google/android/gms/fitness/b/v;->c:Z

    iget-boolean v1, p0, Lcom/google/android/gms/fitness/b/c/j;->b:Z

    iput-boolean v1, v0, Lcom/google/android/gms/fitness/b/v;->e:Z

    iput-boolean v2, v0, Lcom/google/android/gms/fitness/b/v;->f:Z

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/b/v;->a()Lcom/google/android/gms/fitness/b/u;

    move-result-object v0

    .line 389
    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
