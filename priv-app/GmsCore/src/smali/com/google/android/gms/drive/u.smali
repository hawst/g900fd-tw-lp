.class public Lcom/google/android/gms/drive/u;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/drive/u;


# instance fields
.field private final b:Lcom/google/android/gms/drive/database/s;

.field private final c:Lcom/google/android/gms/drive/d/b;

.field private final d:Lcom/google/android/gms/drive/a/a/c;

.field private final e:Lcom/google/android/gms/drive/realtime/cache/w;

.field private final f:Lcom/google/android/gms/drive/e/b;

.field private final g:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 46
    invoke-static {v1}, Lcom/google/android/gms/drive/g/aw;->a(Landroid/content/Context;)V

    .line 47
    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->a()Lcom/google/android/gms/drive/g/aw;

    move-result-object v2

    .line 48
    new-instance v0, Lcom/google/android/gms/drive/d/b;

    invoke-direct {v0, v2}, Lcom/google/android/gms/drive/d/b;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/u;->c:Lcom/google/android/gms/drive/d/b;

    .line 49
    new-instance v0, Lcom/google/android/gms/drive/database/s;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/gms/drive/g/aw;->n()Lcom/google/android/gms/drive/g/w;

    move-result-object v4

    invoke-direct {v0, v3, v4, v1}, Lcom/google/android/gms/drive/database/s;-><init>(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/w;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/u;->b:Lcom/google/android/gms/drive/database/s;

    .line 51
    new-instance v0, Lcom/google/android/gms/drive/a/a/c;

    invoke-direct {v0, v2}, Lcom/google/android/gms/drive/a/a/c;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/u;->d:Lcom/google/android/gms/drive/a/a/c;

    .line 52
    invoke-virtual {v2}, Lcom/google/android/gms/drive/g/aw;->D()Lcom/google/android/gms/drive/realtime/cache/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/u;->e:Lcom/google/android/gms/drive/realtime/cache/w;

    .line 53
    invoke-virtual {v2}, Lcom/google/android/gms/drive/g/aw;->u()Lcom/google/android/gms/drive/e/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/u;->f:Lcom/google/android/gms/drive/e/b;

    .line 55
    sget-object v0, Lcom/google/android/gms/drive/ai;->y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 56
    invoke-static {v1}, Lcom/google/android/gms/drive/metadata/sync/c/g;->a(Landroid/content/Context;)V

    .line 59
    :cond_0
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/drive/u;->g:Ljava/util/concurrent/CountDownLatch;

    .line 60
    new-instance v0, Lcom/google/android/gms/drive/v;

    const-string v1, "Background initialization thread"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/drive/v;-><init>(Lcom/google/android/gms/drive/u;Ljava/lang/String;Lcom/google/android/gms/drive/g/aw;)V

    .line 92
    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 93
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/database/s;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/drive/u;->b:Lcom/google/android/gms/drive/database/s;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 102
    const-class v1, Lcom/google/android/gms/drive/u;

    monitor-enter v1

    .line 103
    :try_start_0
    sget-object v0, Lcom/google/android/gms/drive/u;->a:Lcom/google/android/gms/drive/u;

    if-nez v0, :cond_0

    .line 104
    new-instance v0, Lcom/google/android/gms/drive/u;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/u;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/drive/u;->a:Lcom/google/android/gms/drive/u;

    .line 106
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/e/b;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/drive/u;->f:Lcom/google/android/gms/drive/e/b;

    return-object v0
.end method

.method public static b(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 115
    const-string v0, "Must not be called from UI thread"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    .line 116
    invoke-static {p0}, Lcom/google/android/gms/drive/u;->a(Landroid/content/Context;)V

    .line 117
    sget-object v0, Lcom/google/android/gms/drive/u;->a:Lcom/google/android/gms/drive/u;

    iget-object v1, v0, Lcom/google/android/gms/drive/u;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    const-string v1, "DriveInitializer"

    const-string v2, "Awaiting to be initialized"

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, v0, Lcom/google/android/gms/drive/u;->g:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 118
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/a/a/c;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/drive/u;->d:Lcom/google/android/gms/drive/a/a/c;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/d/b;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/drive/u;->c:Lcom/google/android/gms/drive/d/b;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/drive/u;)Lcom/google/android/gms/drive/realtime/cache/w;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/drive/u;->e:Lcom/google/android/gms/drive/realtime/cache/w;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/drive/u;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/drive/u;->g:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method
