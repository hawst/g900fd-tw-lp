.class public final Lcom/google/android/gms/drive/c/a/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/c/c;


# instance fields
.field private final a:Lcom/google/android/gms/drive/c/a/n;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/c/a/n;Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/c/a/n;

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/h;->a:Lcom/google/android/gms/drive/c/a/n;

    .line 24
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/h;->b:Landroid/content/Context;

    .line 25
    return-void
.end method

.method static a(I)Lcom/google/android/libraries/rocket/impressions/m;
    .locals 2

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/libraries/rocket/impressions/m;

    invoke-direct {v0}, Lcom/google/android/libraries/rocket/impressions/m;-><init>()V

    invoke-static {p0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/libraries/rocket/impressions/m;->a:Ljava/lang/Integer;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/c/b;
    .locals 3

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/gms/drive/c/a/a;

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/h;->a:Lcom/google/android/gms/drive/c/a/n;

    iget-object v2, p0, Lcom/google/android/gms/drive/c/a/h;->b:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/c/a/a;-><init>(Lcom/google/android/gms/drive/c/a/n;Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/auth/CallingAppInfo;Ljava/lang/String;)Lcom/google/android/gms/drive/c/b;
    .locals 3

    .prologue
    .line 30
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 31
    new-instance v0, Lcom/google/android/gms/drive/c/a/c;

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/h;->a:Lcom/google/android/gms/drive/c/a/n;

    iget-object v2, p0, Lcom/google/android/gms/drive/c/a/h;->b:Landroid/content/Context;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/google/android/gms/drive/c/a/c;-><init>(Lcom/google/android/gms/drive/auth/CallingAppInfo;Ljava/lang/String;Lcom/google/android/gms/drive/c/a/n;Landroid/content/Context;)V

    return-object v0
.end method

.method public final a(Landroid/os/Bundle;)Lcom/google/android/gms/drive/c/d;
    .locals 3

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/gms/drive/c/a/o;

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/h;->a:Lcom/google/android/gms/drive/c/a/n;

    iget-object v2, p0, Lcom/google/android/gms/drive/c/a/h;->b:Landroid/content/Context;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/drive/c/a/o;-><init>(Landroid/os/Bundle;Lcom/google/android/gms/drive/c/a/n;Landroid/content/Context;)V

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/drive/auth/CallingAppInfo;Ljava/lang/String;)Lcom/google/android/gms/drive/c/d;
    .locals 3

    .prologue
    .line 38
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    new-instance v0, Lcom/google/android/gms/drive/c/a/o;

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/h;->a:Lcom/google/android/gms/drive/c/a/n;

    iget-object v2, p0, Lcom/google/android/gms/drive/c/a/h;->b:Landroid/content/Context;

    invoke-direct {v0, p1, p2, v1, v2}, Lcom/google/android/gms/drive/c/a/o;-><init>(Lcom/google/android/gms/drive/auth/CallingAppInfo;Ljava/lang/String;Lcom/google/android/gms/drive/c/a/n;Landroid/content/Context;)V

    return-object v0
.end method
