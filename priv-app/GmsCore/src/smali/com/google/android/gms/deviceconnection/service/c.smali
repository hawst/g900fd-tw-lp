.class final Lcom/google/android/gms/deviceconnection/service/c;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/deviceconnection/a/a;

.field private final b:Ljava/util/Queue;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/deviceconnection/a/a;)V
    .locals 1

    .prologue
    .line 200
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 201
    iput-object p1, p0, Lcom/google/android/gms/deviceconnection/service/c;->a:Lcom/google/android/gms/deviceconnection/a/a;

    .line 202
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/deviceconnection/service/c;->b:Ljava/util/Queue;

    .line 203
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/deviceconnection/service/c;->c:Z

    .line 204
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 1

    .prologue
    .line 220
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/deviceconnection/service/c;->c:Z

    .line 221
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    monitor-exit p0

    return-void

    .line 220
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/deviceconnection/service/b;)V
    .locals 1

    .prologue
    .line 212
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/service/c;->b:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 213
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    monitor-exit p0

    return-void

    .line 212
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 227
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/service/c;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 228
    monitor-enter p0

    .line 230
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 232
    :goto_1
    :try_start_1
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 234
    iget-boolean v0, p0, Lcom/google/android/gms/deviceconnection/service/c;->c:Z

    if-eqz v0, :cond_0

    .line 235
    return-void

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 240
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/deviceconnection/service/c;->b:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/deviceconnection/service/b;

    iget-object v1, p0, Lcom/google/android/gms/deviceconnection/service/c;->a:Lcom/google/android/gms/deviceconnection/a/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/deviceconnection/service/b;->a(Lcom/google/android/gms/deviceconnection/a/a;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method
