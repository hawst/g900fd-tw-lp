.class abstract Lcom/google/android/gms/plus/audience/ao;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/Checkable;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field protected a:Landroid/widget/CheckBox;

.field private b:Ljava/lang/Object;

.field private c:Landroid/view/View;

.field private d:Lcom/google/android/gms/plus/audience/ap;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 36
    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/ao;->a(Lcom/google/android/gms/plus/audience/ap;)V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/gms/plus/audience/ao;->e()V

    .line 132
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/ao;->d(Z)V

    .line 133
    return-void
.end method

.method public a(Lcom/google/android/gms/plus/audience/ap;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/ao;->d:Lcom/google/android/gms/plus/audience/ap;

    .line 81
    return-void
.end method

.method public a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 113
    iput-object p1, p0, Lcom/google/android/gms/plus/audience/ao;->b:Ljava/lang/Object;

    .line 114
    return-void
.end method

.method public a(Z)V
    .locals 2

    .prologue
    .line 136
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/ao;->c:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 137
    return-void

    .line 136
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public b(Z)V
    .locals 0

    .prologue
    .line 121
    iput-boolean p1, p0, Lcom/google/android/gms/plus/audience/ao;->e:Z

    .line 122
    return-void
.end method

.method public c(Z)V
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ao;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 100
    if-nez p1, :cond_0

    .line 101
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/ao;->d(Z)V

    .line 103
    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 0

    .prologue
    .line 60
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->setClickable(Z)V

    .line 61
    invoke-virtual {p0, p1}, Lcom/google/android/gms/plus/audience/ao;->setFocusable(Z)V

    .line 62
    return-void
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/google/android/gms/plus/audience/ao;->e:Z

    return v0
.end method

.method public e()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ao;->a:Landroid/widget/CheckBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 96
    return-void
.end method

.method public f()V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ao;->a:Landroid/widget/CheckBox;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 92
    return-void
.end method

.method public g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ao;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public isChecked()Z
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ao;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    return v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ao;->d:Lcom/google/android/gms/plus/audience/ap;

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ao;->d:Lcom/google/android/gms/plus/audience/ap;

    invoke-interface {v0, p0, p2}, Lcom/google/android/gms/plus/audience/ap;->a(Lcom/google/android/gms/plus/audience/ao;Z)V

    .line 110
    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 85
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->aU:I

    if-eq v0, v1, :cond_0

    .line 86
    iget-object v1, p0, Lcom/google/android/gms/plus/audience/ao;->a:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ao;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 88
    :cond_0
    return-void

    .line 86
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 44
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 46
    sget v0, Lcom/google/android/gms/j;->td:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/ao;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/ao;->c:Landroid/view/View;

    .line 48
    sget v0, Lcom/google/android/gms/j;->aU:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/audience/ao;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/google/android/gms/plus/audience/ao;->a:Landroid/widget/CheckBox;

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ao;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 50
    invoke-virtual {p0, p0}, Lcom/google/android/gms/plus/audience/ao;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 51
    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ao;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 72
    return-void
.end method

.method public toggle()V
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/plus/audience/ao;->a:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    .line 77
    return-void
.end method
