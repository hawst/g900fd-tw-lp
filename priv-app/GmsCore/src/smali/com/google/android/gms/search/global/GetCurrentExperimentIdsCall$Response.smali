.class public Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Response;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/ap;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/search/global/b;


# instance fields
.field public a:Lcom/google/android/gms/common/api/Status;

.field public b:[I

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    new-instance v0, Lcom/google/android/gms/search/global/b;

    invoke-direct {v0}, Lcom/google/android/gms/search/global/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Response;->CREATOR:Lcom/google/android/gms/search/global/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Response;->c:I

    .line 104
    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;[I)V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    iput p1, p0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Response;->c:I

    .line 126
    iput-object p2, p0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    .line 127
    iput-object p3, p0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Response;->b:[I

    .line 128
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 134
    sget-object v0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Response;->CREATOR:Lcom/google/android/gms/search/global/b;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 140
    sget-object v0, Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Response;->CREATOR:Lcom/google/android/gms/search/global/b;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/search/global/b;->a(Lcom/google/android/gms/search/global/GetCurrentExperimentIdsCall$Response;Landroid/os/Parcel;I)V

    .line 141
    return-void
.end method
