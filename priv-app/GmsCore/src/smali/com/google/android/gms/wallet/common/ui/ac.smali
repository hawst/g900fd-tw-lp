.class final Lcom/google/android/gms/wallet/common/ui/ac;
.super Landroid/widget/Filter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/ab;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/ab;)V
    .locals 0

    .prologue
    .line 168
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/CharSequence;)Ljava/util/ArrayList;
    .locals 7

    .prologue
    .line 172
    if-eqz p1, :cond_1

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ab;->a(Lcom/google/android/gms/wallet/common/ui/ab;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/a/b;

    .line 176
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/ab;->b(Lcom/google/android/gms/wallet/common/ui/ab;)C

    move-result v2

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    iget-object v3, v1, Lcom/google/android/gms/wallet/common/ui/ab;->a:[C

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/ab;->c(Lcom/google/android/gms/wallet/common/ui/ab;)I

    move-result v4

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/ab;->d(Lcom/google/android/gms/wallet/common/ui/ab;)Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/a/b;->a(Ljava/lang/CharSequence;C[CILjava/lang/String;)Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 183
    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/a/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/wallet/common/ui/ac;->a(Ljava/util/List;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 185
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 190
    :goto_1
    return-object v0

    .line 179
    :catch_0
    move-exception v1

    .line 180
    const-string v2, "AddressSourceResultAdap"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not fetch addresses from "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/a/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 190
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_1
.end method

.method private a(Ljava/util/List;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 7

    .prologue
    const/4 v6, -0x1

    .line 202
    if-nez p1, :cond_0

    .line 203
    const/4 v0, 0x0

    .line 263
    :goto_0
    return-object v0

    .line 206
    :cond_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 210
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    .line 212
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/a/c;

    .line 214
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/a/c;->b()Lcom/google/t/a/b;

    move-result-object v4

    if-eqz v4, :cond_2

    .line 215
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/a/c;->b()Lcom/google/t/a/b;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/gms/wallet/common/ui/ac;->a(Lcom/google/t/a/b;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 216
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/a/c;->b()Lcom/google/t/a/b;

    move-result-object v4

    invoke-direct {p0, v4}, Lcom/google/android/gms/wallet/common/ui/ac;->b(Lcom/google/t/a/b;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 222
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/a/c;->a()Ljava/lang/String;

    move-result-object v0

    .line 223
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 225
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 233
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/a/c;->c()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 234
    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 240
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/a/c;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 241
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/a/c;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 246
    :cond_3
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    .line 248
    const/4 v0, 0x0

    .line 249
    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 250
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 251
    if-eq v1, v6, :cond_4

    .line 252
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 257
    new-instance v5, Lcom/google/android/gms/wallet/common/a/c;

    invoke-direct {v5, v0, p2}, Lcom/google/android/gms/wallet/common/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 258
    add-int v0, v1, v2

    invoke-virtual {v3, v0, v5}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 259
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    .line 260
    goto :goto_2

    :cond_5
    move-object v0, v3

    .line 263
    goto/16 :goto_0
.end method

.method private a(Lcom/google/t/a/b;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 278
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ab;->e(Lcom/google/android/gms/wallet/common/ui/ab;)[C

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/t/a/b;->q:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    .line 302
    :cond_1
    :goto_0
    return v0

    .line 282
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ab;->e(Lcom/google/android/gms/wallet/common/ui/ab;)[C

    move-result-object v3

    array-length v4, v3

    move v2, v1

    move v0, v1

    :goto_1
    if-ge v2, v4, :cond_1

    aget-char v5, v3, v2

    .line 283
    sparse-switch v5, :sswitch_data_0

    .line 291
    invoke-static {v5}, Lcom/google/android/gms/wallet/dynamite/common/a/a;->a(C)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 292
    const/4 v0, 0x1

    .line 295
    invoke-static {p1, v5}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;C)Ljava/lang/String;

    move-result-object v5

    .line 296
    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_3

    move v0, v1

    .line 297
    goto :goto_0

    .line 282
    :cond_3
    :sswitch_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 283
    :sswitch_data_0
    .sparse-switch
        0x31 -> :sswitch_0
        0x32 -> :sswitch_0
        0x33 -> :sswitch_0
        0x41 -> :sswitch_0
        0x4e -> :sswitch_0
    .end sparse-switch
.end method

.method private b(Lcom/google/t/a/b;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 307
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ab;->e(Lcom/google/android/gms/wallet/common/ui/ab;)[C

    move-result-object v0

    if-nez v0, :cond_0

    move v0, v1

    .line 322
    :goto_0
    return v0

    .line 310
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ab;->e(Lcom/google/android/gms/wallet/common/ui/ab;)[C

    move-result-object v5

    array-length v6, v5

    move v4, v2

    :goto_1
    if-ge v4, v6, :cond_5

    aget-char v0, v5, v4

    .line 312
    const/16 v3, 0x41

    if-ne v0, v3, :cond_1

    .line 313
    const/16 v0, 0x31

    .line 315
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    iget-object v7, v3, Lcom/google/android/gms/wallet/common/ui/ab;->a:[C

    if-eqz v7, :cond_3

    array-length v8, v7

    move v3, v2

    :goto_2
    if-ge v3, v8, :cond_3

    aget-char v9, v7, v3

    if-ne v9, v0, :cond_2

    move v3, v1

    :goto_3
    if-eqz v3, :cond_4

    .line 316
    invoke-static {p1, v0}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;C)Ljava/lang/String;

    move-result-object v0

    .line 317
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v2

    .line 318
    goto :goto_0

    .line 315
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_3
    move v3, v2

    goto :goto_3

    .line 310
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_5
    move v0, v1

    .line 322
    goto :goto_0
.end method


# virtual methods
.method public final convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 360
    instance-of v0, p1, Lcom/google/android/gms/wallet/common/a/c;

    if-eqz v0, :cond_0

    .line 361
    check-cast p1, Lcom/google/android/gms/wallet/common/a/c;

    invoke-virtual {p1}, Lcom/google/android/gms/wallet/common/a/c;->a()Ljava/lang/String;

    move-result-object v0

    .line 363
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Landroid/widget/Filter;->convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method protected final performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 2

    .prologue
    .line 330
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/ac;->a(Ljava/lang/CharSequence;)Ljava/util/ArrayList;

    move-result-object v0

    .line 332
    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    .line 333
    iput-object v0, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    .line 334
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    .line 335
    return-object v1
.end method

.method protected final publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    .prologue
    .line 341
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    iput-object p1, v1, Lcom/google/android/gms/wallet/common/ui/ab;->c:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    iput-object v0, v1, Lcom/google/android/gms/wallet/common/ui/ab;->b:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/ab;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/ab;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ab;->notifyDataSetChanged()V

    .line 342
    :goto_0
    return-void

    .line 341
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/ac;->a:Lcom/google/android/gms/wallet/common/ui/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ab;->notifyDataSetInvalidated()V

    goto :goto_0
.end method
