.class public final Lcom/google/android/gms/drive/api/a/a;
.super Lcom/google/android/gms/drive/api/a/b;
.source "SourceFile"


# instance fields
.field private final e:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

.field private final f:Lcom/google/android/gms/drive/internal/cd;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/AddEventListenerRequest;Lcom/google/android/gms/drive/internal/cd;Lcom/google/android/gms/drive/internal/ca;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    invoke-direct {p0, p1, p4, v1}, Lcom/google/android/gms/drive/api/a/b;-><init>(Lcom/google/android/gms/drive/api/d;Lcom/google/android/gms/drive/internal/ca;I)V

    .line 33
    iput-object p2, p0, Lcom/google/android/gms/drive/api/a/a;->e:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    .line 34
    iput-object p3, p0, Lcom/google/android/gms/drive/api/a/a;->f:Lcom/google/android/gms/drive/internal/cd;

    .line 35
    return-void

    .line 32
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V
    .locals 4

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/a;->e:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    const-string v1, "Invalid add event listener request: no request"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(Ljava/lang/Object;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/a;->e:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->b()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/a;->e:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/events/q;->a(ILcom/google/android/gms/drive/DriveId;)Z

    move-result v0

    const-string v1, "Invalid add event listener request: invalid drive id"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/service/i;->a(ZLjava/lang/String;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/a;->e:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/a;->c:Lcom/google/android/gms/drive/c/a;

    new-instance v1, Lcom/google/android/gms/drive/internal/l;

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/a;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v3, p0, Lcom/google/android/gms/drive/api/a/a;->e:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-interface {v2, v3}, Lcom/google/android/gms/drive/api/d;->a(Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/drive/internal/l;-><init>(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/aj;)Lcom/google/android/gms/drive/c/a;

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/a;->f:Lcom/google/android/gms/drive/internal/cd;

    if-eqz v0, :cond_1

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/a;->c:Lcom/google/android/gms/drive/c/a;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/c/a;->b(I)Lcom/google/android/gms/drive/c/a;

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/a;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/a;->e:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/a;->e:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->b()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/drive/api/a/a;->f:Lcom/google/android/gms/drive/internal/cd;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/drive/api/d;->a(Lcom/google/android/gms/drive/DriveId;ILcom/google/android/gms/drive/internal/cd;)V

    .line 75
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/a;->b:Lcom/google/android/gms/drive/internal/ca;

    invoke-interface {v0}, Lcom/google/android/gms/drive/internal/ca;->a()V

    .line 76
    return-void

    .line 71
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/a;->c:Lcom/google/android/gms/drive/c/a;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/c/a;->b(I)Lcom/google/android/gms/drive/c/a;

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/drive/api/a/a;->a:Lcom/google/android/gms/drive/api/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/api/a/a;->e:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/api/a/a;->e:Lcom/google/android/gms/drive/internal/AddEventListenerRequest;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/AddEventListenerRequest;->b()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/api/d;->a(Lcom/google/android/gms/drive/DriveId;I)V

    goto :goto_0
.end method
