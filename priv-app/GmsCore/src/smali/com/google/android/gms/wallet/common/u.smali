.class final Lcom/google/android/gms/wallet/common/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Z

.field final b:Z

.field final c:I

.field final d:Ljava/util/Locale;

.field private final e:Ljava/lang/String;


# direct methods
.method constructor <init>(ZZILjava/util/Locale;)V
    .locals 6

    .prologue
    .line 189
    const/4 v5, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/wallet/common/u;-><init>(ZZILjava/util/Locale;Ljava/lang/String;)V

    .line 190
    return-void
.end method

.method constructor <init>(ZZILjava/util/Locale;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 193
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/u;->a:Z

    .line 195
    iput-boolean p2, p0, Lcom/google/android/gms/wallet/common/u;->b:Z

    .line 196
    iput p3, p0, Lcom/google/android/gms/wallet/common/u;->c:I

    .line 197
    iput-object p4, p0, Lcom/google/android/gms/wallet/common/u;->d:Ljava/util/Locale;

    .line 198
    iput-object p5, p0, Lcom/google/android/gms/wallet/common/u;->e:Ljava/lang/String;

    .line 199
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 220
    if-ne p0, p1, :cond_1

    .line 230
    :cond_0
    :goto_0
    return v0

    .line 223
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 224
    goto :goto_0

    .line 226
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 227
    goto :goto_0

    .line 229
    :cond_3
    check-cast p1, Lcom/google/android/gms/wallet/common/u;

    .line 230
    iget-boolean v2, p0, Lcom/google/android/gms/wallet/common/u;->a:Z

    iget-boolean v3, p1, Lcom/google/android/gms/wallet/common/u;->a:Z

    if-ne v2, v3, :cond_4

    iget-boolean v2, p0, Lcom/google/android/gms/wallet/common/u;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/wallet/common/u;->b:Z

    if-ne v2, v3, :cond_4

    iget v2, p0, Lcom/google/android/gms/wallet/common/u;->c:I

    iget v3, p1, Lcom/google/android/gms/wallet/common/u;->c:I

    if-ne v2, v3, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/u;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/wallet/common/u;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/u;->d:Ljava/util/Locale;

    iget-object v3, p1, Lcom/google/android/gms/wallet/common/u;->d:Ljava/util/Locale;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/u;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 211
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/u;->a:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v4

    .line 212
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/u;->d:Ljava/util/Locale;

    if-nez v4, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 213
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/u;->b:Z

    if-eqz v1, :cond_3

    :goto_3
    add-int/2addr v0, v2

    .line 214
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/wallet/common/u;->c:I

    add-int/2addr v0, v1

    .line 215
    return v0

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/u;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v3

    .line 211
    goto :goto_1

    .line 212
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/u;->d:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->hashCode()I

    move-result v1

    goto :goto_2

    :cond_3
    move v2, v3

    .line 213
    goto :goto_3
.end method
