.class public final Lcom/google/android/gms/icing/c/a/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile q:[Lcom/google/android/gms/icing/c/a/b;


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;

.field public m:Ljava/lang/String;

.field public n:Lcom/google/android/gms/icing/c/a/d;

.field public o:[Lcom/google/android/gms/icing/c/a/u;

.field public p:I


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 532
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 533
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/c/a/b;->a:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->d:Ljava/lang/String;

    iput v3, p0, Lcom/google/android/gms/icing/c/a/b;->e:I

    iput v3, p0, Lcom/google/android/gms/icing/c/a/b;->f:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->g:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->h:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->i:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->j:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->k:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->l:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->m:Ljava/lang/String;

    iput-object v4, p0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    invoke-static {}, Lcom/google/android/gms/icing/c/a/u;->a()[Lcom/google/android/gms/icing/c/a/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    iput v2, p0, Lcom/google/android/gms/icing/c/a/b;->p:I

    iput-object v4, p0, Lcom/google/android/gms/icing/c/a/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    iput v2, p0, Lcom/google/android/gms/icing/c/a/b;->cachedSize:I

    .line 534
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/c/a/b;
    .locals 2

    .prologue
    .line 473
    sget-object v0, Lcom/google/android/gms/icing/c/a/b;->q:[Lcom/google/android/gms/icing/c/a/b;

    if-nez v0, :cond_1

    .line 474
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 476
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/c/a/b;->q:[Lcom/google/android/gms/icing/c/a/b;

    if-nez v0, :cond_0

    .line 477
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/c/a/b;

    sput-object v0, Lcom/google/android/gms/icing/c/a/b;->q:[Lcom/google/android/gms/icing/c/a/b;

    .line 479
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 481
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/c/a/b;->q:[Lcom/google/android/gms/icing/c/a/b;

    return-object v0

    .line 479
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 7

    .prologue
    .line 762
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 763
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/b;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 764
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/b;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 767
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 768
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 771
    :cond_1
    iget v1, p0, Lcom/google/android/gms/icing/c/a/b;->e:I

    if-eqz v1, :cond_2

    .line 772
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/icing/c/a/b;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 775
    :cond_2
    iget v1, p0, Lcom/google/android/gms/icing/c/a/b;->f:I

    if-eqz v1, :cond_3

    .line 776
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/icing/c/a/b;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 779
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 780
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 783
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->j:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 784
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 787
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->k:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 788
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 791
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->m:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 792
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 795
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    if-eqz v1, :cond_8

    .line 796
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 799
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 800
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 803
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 804
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 807
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    array-length v1, v1

    if-lez v1, :cond_d

    .line 808
    const/4 v1, 0x0

    move v6, v1

    move v1, v0

    move v0, v6

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    array-length v2, v2

    if-ge v0, v2, :cond_c

    .line 809
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    aget-object v2, v2, v0

    .line 810
    if-eqz v2, :cond_b

    .line 811
    const/16 v3, 0xc

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 808
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_c
    move v0, v1

    .line 816
    :cond_d
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    .line 817
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 820
    :cond_e
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 821
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 824
    :cond_f
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->l:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    .line 825
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 828
    :cond_10
    iget v1, p0, Lcom/google/android/gms/icing/c/a/b;->p:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_11

    .line 829
    const/16 v1, 0x10

    iget v2, p0, Lcom/google/android/gms/icing/c/a/b;->p:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 832
    :cond_11
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 560
    if-ne p1, p0, :cond_1

    .line 561
    const/4 v0, 0x1

    .line 662
    :cond_0
    :goto_0
    return v0

    .line 563
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/b;

    if-eqz v1, :cond_0

    .line 566
    check-cast p1, Lcom/google/android/gms/icing/c/a/b;

    .line 567
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/b;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/c/a/b;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 570
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->b:Ljava/lang/String;

    if-nez v1, :cond_d

    .line 571
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/b;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 577
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->c:Ljava/lang/String;

    if-nez v1, :cond_e

    .line 578
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/b;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 584
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->d:Ljava/lang/String;

    if-nez v1, :cond_f

    .line 585
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/b;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 591
    :cond_4
    iget v1, p0, Lcom/google/android/gms/icing/c/a/b;->e:I

    iget v2, p1, Lcom/google/android/gms/icing/c/a/b;->e:I

    if-ne v1, v2, :cond_0

    .line 594
    iget v1, p0, Lcom/google/android/gms/icing/c/a/b;->f:I

    iget v2, p1, Lcom/google/android/gms/icing/c/a/b;->f:I

    if-ne v1, v2, :cond_0

    .line 597
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->g:Ljava/lang/String;

    if-nez v1, :cond_10

    .line 598
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/b;->g:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 604
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->h:Ljava/lang/String;

    if-nez v1, :cond_11

    .line 605
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/b;->h:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 611
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->i:Ljava/lang/String;

    if-nez v1, :cond_12

    .line 612
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/b;->i:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 618
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->j:Ljava/lang/String;

    if-nez v1, :cond_13

    .line 619
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/b;->j:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 625
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->k:Ljava/lang/String;

    if-nez v1, :cond_14

    .line 626
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/b;->k:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 632
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->l:Ljava/lang/String;

    if-nez v1, :cond_15

    .line 633
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/b;->l:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 639
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->m:Ljava/lang/String;

    if-nez v1, :cond_16

    .line 640
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/b;->m:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 646
    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    if-nez v1, :cond_17

    .line 647
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    if-nez v1, :cond_0

    .line 655
    :cond_c
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 659
    iget v1, p0, Lcom/google/android/gms/icing/c/a/b;->p:I

    iget v2, p1, Lcom/google/android/gms/icing/c/a/b;->p:I

    if-ne v1, v2, :cond_0

    .line 662
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/b;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto/16 :goto_0

    .line 574
    :cond_d
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/b;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0

    .line 581
    :cond_e
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/b;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 588
    :cond_f
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/b;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 601
    :cond_10
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->g:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/b;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 608
    :cond_11
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->h:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/b;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 615
    :cond_12
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->i:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/b;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 622
    :cond_13
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->j:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/b;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 629
    :cond_14
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->k:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/b;->k:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 636
    :cond_15
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->l:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/b;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 643
    :cond_16
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->m:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/b;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    .line 651
    :cond_17
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/c/a/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 667
    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/b;->a:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/c/a/b;->a:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit16 v0, v0, 0x20f

    .line 670
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 672
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 674
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 676
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/c/a/b;->e:I

    add-int/2addr v0, v2

    .line 677
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/c/a/b;->f:I

    add-int/2addr v0, v2

    .line 678
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->g:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 680
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->h:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 682
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->i:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 684
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->j:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 686
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->k:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 688
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->l:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 690
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->m:Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 692
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    if-nez v2, :cond_a

    :goto_a
    add-int/2addr v0, v1

    .line 694
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 696
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/c/a/b;->p:I

    add-int/2addr v0, v1

    .line 697
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/b;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 698
    return v0

    .line 670
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 672
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 674
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 678
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 680
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 682
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 684
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 686
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_7

    .line 688
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_8

    .line 690
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_9

    .line 692
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/c/a/d;->hashCode()I

    move-result v1

    goto :goto_a
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 458
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/c/a/b;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/c/a/b;->e:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/icing/c/a/b;->f:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->k:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->m:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/c/a/d;

    invoke-direct {v0}, Lcom/google/android/gms/icing/c/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_c
    const/16 v0, 0x62

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/c/a/u;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/android/gms/icing/c/a/u;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/u;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/android/gms/icing/c/a/u;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/u;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->c:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/c/a/b;->p:I

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
        0x6a -> :sswitch_d
        0x72 -> :sswitch_e
        0x7a -> :sswitch_f
        0x80 -> :sswitch_10
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 704
    iget-wide v0, p0, Lcom/google/android/gms/icing/c/a/b;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 705
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/c/a/b;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 707
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 708
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 710
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/c/a/b;->e:I

    if-eqz v0, :cond_2

    .line 711
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/icing/c/a/b;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 713
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/c/a/b;->f:I

    if-eqz v0, :cond_3

    .line 714
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/c/a/b;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 716
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 717
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 719
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->j:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 720
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 722
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->k:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 723
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 725
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->m:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 726
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 728
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    if-eqz v0, :cond_8

    .line 729
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 731
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 732
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 734
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 735
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 737
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    array-length v0, v0

    if-lez v0, :cond_c

    .line 738
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    array-length v1, v1

    if-ge v0, v1, :cond_c

    .line 739
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    aget-object v1, v1, v0

    .line 740
    if-eqz v1, :cond_b

    .line 741
    const/16 v2, 0xc

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 738
    :cond_b
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 745
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 746
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 748
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    .line 749
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 751
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/b;->l:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 752
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/b;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 754
    :cond_f
    iget v0, p0, Lcom/google/android/gms/icing/c/a/b;->p:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_10

    .line 755
    const/16 v0, 0x10

    iget v1, p0, Lcom/google/android/gms/icing/c/a/b;->p:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 757
    :cond_10
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 758
    return-void
.end method
