.class public final Lcom/google/android/gms/common/ui/widget/k;
.super Landroid/widget/ArrayAdapter;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:I

.field private c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;III[Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p4, p5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;II[Ljava/lang/Object;)V

    .line 37
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/ui/widget/k;->a:Landroid/view/LayoutInflater;

    .line 38
    iput p3, p0, Lcom/google/android/gms/common/ui/widget/k;->b:I

    .line 39
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 45
    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/widget/k;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/ui/widget/k;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/common/ui/widget/k;->notifyDataSetChanged()V

    .line 46
    return-void
.end method

.method public final getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 67
    if-nez p2, :cond_0

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/k;->a:Landroid/view/LayoutInflater;

    const v1, 0x109000a

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    :goto_0
    move-object v0, v1

    .line 71
    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/ui/widget/k;->getItem(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 72
    return-object v1

    :cond_0
    move-object v1, p2

    goto :goto_0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 58
    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/k;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 60
    iget v0, p0, Lcom/google/android/gms/common/ui/widget/k;->b:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/common/ui/widget/k;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 62
    :cond_0
    return-object v1
.end method
