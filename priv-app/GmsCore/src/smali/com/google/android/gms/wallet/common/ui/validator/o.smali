.class public final Lcom/google/android/gms/wallet/common/ui/validator/o;
.super Lcom/google/android/gms/wallet/common/ui/validator/r;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/regex/Pattern;

.field private static final c:Ljava/util/regex/Pattern;

.field private static final d:Ljava/util/regex/Pattern;


# instance fields
.field private e:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-string v0, "^\\(?[+\uff0b]+0*\\d+\\)?.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/validator/o;->a:Ljava/util/regex/Pattern;

    .line 36
    const-string v0, "^(?:\\(?\\+?0*1\\)?(?:[.-])?)?\\(?[2-9][0-8][0-9]\\)?(?:[.-])?[2-9]((?:[.-])?[0-9]){6}(?:(?:#|x\\.?|ext\\.?|extension)(\\d+))?$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/validator/o;->c:Ljava/util/regex/Pattern;

    .line 37
    const-string v0, "^(?:\\(?[+\uff0b]+0*\\d+\\)?)?([-.()]?\\d[-.()]?){4}.*"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/validator/o;->d:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/validator/r;-><init>(Ljava/lang/CharSequence;)V

    .line 44
    const/16 v0, 0x2b3

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/validator/o;->e:I

    .line 45
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 48
    iput p1, p0, Lcom/google/android/gms/wallet/common/ui/validator/o;->e:I

    .line 49
    return-void
.end method

.method public final a(Landroid/widget/TextView;)Z
    .locals 3

    .prologue
    .line 53
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "\\s"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 54
    sget-object v1, Lcom/google/android/gms/wallet/common/ui/validator/o;->a:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 55
    sget-object v1, Lcom/google/android/gms/wallet/common/ui/validator/o;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    .line 59
    :goto_0
    return v0

    .line 56
    :cond_0
    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/validator/o;->e:I

    const/16 v2, 0x2b3

    if-ne v1, v2, :cond_1

    .line 57
    sget-object v1, Lcom/google/android/gms/wallet/common/ui/validator/o;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    goto :goto_0

    .line 59
    :cond_1
    sget-object v1, Lcom/google/android/gms/wallet/common/ui/validator/o;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    goto :goto_0
.end method
