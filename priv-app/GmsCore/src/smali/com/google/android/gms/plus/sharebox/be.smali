.class final Lcom/google/android/gms/plus/sharebox/be;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/es;
.implements Lcom/google/android/gms/common/et;


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/sharebox/au;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/sharebox/au;)V
    .locals 0

    .prologue
    .line 288
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/sharebox/au;B)V
    .locals 0

    .prologue
    .line 288
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/sharebox/be;-><init>(Lcom/google/android/gms/plus/sharebox/au;)V

    return-void
.end method


# virtual methods
.method public final T_()V
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 306
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->b(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/plus/internal/ab;->e()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;Ljava/lang/String;)Ljava/lang/String;

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->a()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->d(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->d(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    .line 315
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    const/4 v4, -0x1

    invoke-static {v2, v4}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;I)I

    .line 316
    array-length v4, v0

    move v2, v3

    .line 317
    :goto_1
    if-ge v2, v4, :cond_0

    .line 318
    aget-object v5, v0, v2

    iget-object v6, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v6}, Lcom/google/android/gms/plus/sharebox/au;->d(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    .line 319
    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v4, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;I)I

    .line 323
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->f(Lcom/google/android/gms/plus/sharebox/au;)I

    move-result v2

    if-gez v2, :cond_1

    .line 324
    const-string v2, "ShareBox"

    const-string v4, "Resolved account name not found among share eligable accounts"

    invoke-static {v2, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2, v3}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;I)I

    .line 327
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 328
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v3}, Lcom/google/android/gms/plus/sharebox/au;->f(Lcom/google/android/gms/plus/sharebox/au;)I

    move-result v3

    invoke-interface {v2, v0, v3}, Lcom/google/android/gms/plus/sharebox/bf;->a([Ljava/lang/String;I)V

    .line 332
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->g(Lcom/google/android/gms/plus/sharebox/au;)V

    .line 335
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->h(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->h(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-nez v0, :cond_3

    .line 336
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->h(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 339
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->i(Lcom/google/android/gms/plus/sharebox/au;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->b(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->j(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/model/posts/Post;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/at;Lcom/google/android/gms/plus/model/posts/Post;)V

    .line 366
    :goto_2
    return-void

    .line 310
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->e(Lcom/google/android/gms/plus/sharebox/au;)[Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 317
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_1

    .line 342
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bg;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 344
    :goto_3
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/bg;->g()Z

    move-result v2

    if-eqz v2, :cond_7

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/plus/sharebox/bg;->p:Lcom/google/android/gms/plus/sharebox/a/a;

    iget-object v1, v1, Lcom/google/android/gms/plus/sharebox/a/a;->a:Landroid/os/Bundle;

    .line 349
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v3}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/plus/sharebox/bg;->q:Lcom/google/android/gms/common/people/data/Audience;

    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v4}, Lcom/google/android/gms/plus/sharebox/au;->c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gms/plus/sharebox/bg;->f:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/plus/model/posts/Post;->a(Landroid/net/Uri;Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;Ljava/lang/String;)Lcom/google/android/gms/plus/model/posts/Post;

    move-result-object v0

    .line 354
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->k(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v1

    if-nez v1, :cond_9

    .line 355
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->b(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/as;Lcom/google/android/gms/plus/model/posts/Post;)V

    .line 360
    :goto_4
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->l(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/data/a/a;

    move-result-object v1

    if-nez v1, :cond_a

    invoke-virtual {v0}, Lcom/google/android/gms/plus/model/posts/Post;->c()Z

    move-result v1

    if-eqz v1, :cond_a

    .line 361
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/au;->b(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/plus/internal/ab;->a(Lcom/google/android/gms/plus/internal/ar;Lcom/google/android/gms/plus/model/posts/Post;)V

    goto/16 :goto_2

    :cond_8
    move-object v0, v1

    .line 342
    goto :goto_3

    .line 357
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v2, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v3}, Lcom/google/android/gms/plus/sharebox/au;->k(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/posts/Settings;)V

    goto :goto_4

    .line 363
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v1, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v2}, Lcom/google/android/gms/plus/sharebox/au;->l(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/data/a/a;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/data/a/a;)V

    goto/16 :goto_2
.end method

.method public final W_()V
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->k(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/model/posts/Settings;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->i(Lcom/google/android/gms/plus/sharebox/au;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->b(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 373
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 4

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 297
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 298
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    sget-object v0, Lcom/google/android/gms/plus/c/a;->ag:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;J)J

    .line 300
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/be;->a:Lcom/google/android/gms/plus/sharebox/au;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/gms/plus/sharebox/bf;->a(Lcom/google/android/gms/common/c;)V

    .line 302
    :cond_1
    return-void
.end method
