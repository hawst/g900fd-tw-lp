.class public final Lcom/google/android/gms/photos/autobackup/a/a/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/photos/autobackup/k;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;)V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-object p1, p0, Lcom/google/android/gms/photos/autobackup/a/a/e;->a:Lcom/google/android/gms/common/api/Status;

    .line 16
    iput-object p2, p0, Lcom/google/android/gms/photos/autobackup/a/a/e;->b:Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;

    .line 17
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/a/a/e;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/gms/photos/autobackup/a/a/e;->b:Lcom/google/android/gms/photos/autobackup/model/MigrationStatus;

    return-object v0
.end method
