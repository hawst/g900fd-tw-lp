.class public interface abstract Lcom/google/android/gms/appstate/c/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 37
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "app_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "client_context_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "key"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "local_version"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "local_data"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "conflict_version"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "conflict_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "data_dirty"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "upsync_required"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/appstate/c/e;->a:[Ljava/lang/String;

    return-void
.end method
