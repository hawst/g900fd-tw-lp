.class final Lcom/google/android/gms/backup/ap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field final synthetic a:Lcom/google/android/gms/backup/SetBackupAccountActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/backup/SetBackupAccountActivity;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/gms/backup/ap;->a:Lcom/google/android/gms/backup/SetBackupAccountActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run(Landroid/accounts/AccountManagerFuture;)V
    .locals 5

    .prologue
    .line 54
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 55
    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    .line 57
    if-eqz v1, :cond_0

    .line 59
    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 60
    const-string v2, "accountType"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 61
    iget-object v3, p0, Lcom/google/android/gms/backup/ap;->a:Lcom/google/android/gms/backup/SetBackupAccountActivity;

    new-instance v4, Landroid/accounts/Account;

    invoke-direct {v4, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3, v4}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->a(Lcom/google/android/gms/backup/SetBackupAccountActivity;Landroid/accounts/Account;)V

    .line 63
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/backup/ap;->a:Lcom/google/android/gms/backup/SetBackupAccountActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->setResult(I)V

    .line 64
    invoke-static {}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->a()Lcom/google/android/gms/auth/d/a;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->a()Lcom/google/android/gms/auth/d/a;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "account added: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/backup/ap;->a:Lcom/google/android/gms/backup/SetBackupAccountActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->finish()V

    .line 73
    :goto_0
    return-void

    .line 66
    :catch_0
    move-exception v0

    :try_start_1
    invoke-static {}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->a()Lcom/google/android/gms/auth/d/a;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->a()Lcom/google/android/gms/auth/d/a;

    move-result-object v0

    const-string v1, "addAccount was canceled"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 72
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/backup/ap;->a:Lcom/google/android/gms/backup/SetBackupAccountActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->finish()V

    goto :goto_0

    .line 67
    :catch_1
    move-exception v0

    .line 68
    :try_start_2
    invoke-static {}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->a()Lcom/google/android/gms/auth/d/a;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->a()Lcom/google/android/gms/auth/d/a;

    move-result-object v1

    const-string v2, "addAccount failed: "

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 72
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/backup/ap;->a:Lcom/google/android/gms/backup/SetBackupAccountActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->finish()V

    goto :goto_0

    .line 69
    :catch_2
    move-exception v0

    .line 70
    :try_start_3
    invoke-static {}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->a()Lcom/google/android/gms/auth/d/a;

    move-result-object v1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->a()Lcom/google/android/gms/auth/d/a;

    move-result-object v1

    const-string v2, "addAccount failed: "

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/d/a;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 72
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/backup/ap;->a:Lcom/google/android/gms/backup/SetBackupAccountActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->finish()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/backup/ap;->a:Lcom/google/android/gms/backup/SetBackupAccountActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/backup/SetBackupAccountActivity;->finish()V

    throw v0
.end method
