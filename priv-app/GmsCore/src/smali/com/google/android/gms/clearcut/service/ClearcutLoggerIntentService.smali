.class public final Lcom/google/android/gms/clearcut/service/ClearcutLoggerIntentService;
.super Lcom/google/android/gms/common/service/c;
.source "SourceFile"


# static fields
.field private static final b:Lcom/google/android/gms/common/service/d;


# instance fields
.field private c:Lcom/google/android/gms/playlog/store/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/gms/common/service/d;

    invoke-direct {v0}, Lcom/google/android/gms/common/service/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/clearcut/service/ClearcutLoggerIntentService;->b:Lcom/google/android/gms/common/service/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 63
    const-string v0, "ClearcutLoggerIntentService"

    sget-object v1, Lcom/google/android/gms/clearcut/service/ClearcutLoggerIntentService;->b:Lcom/google/android/gms/common/service/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/service/c;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/service/d;)V

    .line 64
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/clearcut/a/i;Lcom/google/android/gms/clearcut/LogEventParcelable;)V
    .locals 2

    .prologue
    .line 109
    new-instance v0, Lcom/google/android/gms/clearcut/service/a;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/clearcut/service/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/clearcut/a/i;Lcom/google/android/gms/clearcut/LogEventParcelable;)V

    sget-object v1, Lcom/google/android/gms/clearcut/service/ClearcutLoggerIntentService;->b:Lcom/google/android/gms/common/service/d;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/service/d;->add(Ljava/lang/Object;)Z

    const-string v0, "com.google.android.gms.clearcut.service.INTENT"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 110
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/playlog/store/f;
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/clearcut/service/ClearcutLoggerIntentService;->c:Lcom/google/android/gms/playlog/store/f;

    return-object v0
.end method

.method public final onCreate()V
    .locals 1

    .prologue
    .line 68
    invoke-super {p0}, Lcom/google/android/gms/common/service/c;->onCreate()V

    .line 69
    invoke-static {}, Lcom/google/android/gms/playlog/store/f;->a()Lcom/google/android/gms/playlog/store/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/service/ClearcutLoggerIntentService;->c:Lcom/google/android/gms/playlog/store/f;

    .line 70
    return-void
.end method

.method public final onDestroy()V
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/clearcut/service/ClearcutLoggerIntentService;->c:Lcom/google/android/gms/playlog/store/f;

    invoke-virtual {v0}, Lcom/google/android/gms/playlog/store/f;->close()V

    .line 75
    invoke-super {p0}, Lcom/google/android/gms/common/service/c;->onDestroy()V

    .line 76
    return-void
.end method
