.class public Lcom/google/android/gms/wallet/dynamite/fragment/b;
.super Lcom/google/android/gms/wallet/fragment/a/b;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/wallet/dynamite/service/a/a;


# static fields
.field private static final e:Ljava/lang/String;

.field private static final f:Ljava/lang/String;

.field private static final g:Ljava/lang/String;

.field private static final h:Ljava/lang/String;

.field private static final i:Ljava/lang/String;

.field private static final j:Ljava/lang/String;

.field private static final k:Ljava/lang/String;

.field private static final l:Ljava/lang/String;


# instance fields
.field a:Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

.field b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

.field c:Lcom/google/android/gms/wallet/MaskedWalletRequest;

.field d:Lcom/google/android/gms/wallet/MaskedWallet;

.field private final m:Lcom/google/android/gms/b/i;

.field private final n:Landroid/content/Context;

.field private final o:Landroid/content/Context;

.field private final p:Lcom/google/android/gms/wallet/fragment/a/c;

.field private q:Ljava/lang/String;

.field private r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

.field private s:Landroid/view/View;

.field private t:I

.field private u:Z

.field private v:Z

.field private w:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    const-class v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->e:Ljava/lang/String;

    .line 51
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/wallet/dynamite/fragment/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".FragmentOptions"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->f:Ljava/lang/String;

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/wallet/dynamite/fragment/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".WalletFragmentInitParams"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->g:Ljava/lang/String;

    .line 53
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/wallet/dynamite/fragment/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".MaskedWallet"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->h:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/wallet/dynamite/fragment/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".MaskedWalletRequest"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->i:Ljava/lang/String;

    .line 55
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/wallet/dynamite/fragment/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".WalletFragmentState"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->j:Ljava/lang/String;

    .line 56
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/wallet/dynamite/fragment/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".IntegratorEnabled"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->k:Ljava/lang/String;

    .line 57
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/google/android/gms/wallet/dynamite/fragment/b;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".FragmentKey"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->l:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/gms/b/i;Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;Lcom/google/android/gms/wallet/fragment/a/c;)V
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 87
    invoke-direct {p0}, Lcom/google/android/gms/wallet/fragment/a/b;-><init>()V

    .line 79
    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->t:I

    .line 80
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->u:Z

    .line 81
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->v:Z

    .line 88
    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->n:Landroid/content/Context;

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->n:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/ew;->f(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->o:Landroid/content/Context;

    .line 90
    iput-object p2, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->m:Lcom/google/android/gms/b/i;

    .line 91
    iput-object p3, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a:Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    .line 92
    iput-object p4, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->p:Lcom/google/android/gms/wallet/fragment/a/c;

    .line 93
    return-void
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 4

    .prologue
    .line 498
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 499
    if-nez v0, :cond_0

    .line 500
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    .line 502
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1, v0}, Landroid/os/Bundle;-><init>(Landroid/os/Bundle;)V

    .line 503
    const-class v0, Lcom/google/android/gms/wallet/MaskedWallet;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 504
    const/4 v0, -0x1

    if-ne p1, v0, :cond_1

    .line 507
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->o()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 508
    const-string v0, "com.google.android.gms.wallet.EXTRA_MASKED_WALLET"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/MaskedWallet;

    .line 510
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a(Lcom/google/android/gms/wallet/MaskedWallet;)V

    .line 513
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;->c()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->m()Landroid/app/Activity;

    move-result-object v2

    if-eqz v2, :cond_2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-virtual {v2, v0, p2, v3}, Landroid/app/Activity;->createPendingResult(ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    if-nez v0, :cond_3

    const-string v0, "WalletFragmentDelegateImpl"

    const-string v2, "Null pending result returned for onMaskedWalletLoaded"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 514
    :cond_2
    :goto_0
    const/4 v0, 0x1

    if-ne p1, v0, :cond_4

    .line 515
    const-string v0, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    const/16 v2, 0x19d

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 517
    const/16 v1, 0x192

    if-ne v0, v1, :cond_4

    .line 518
    const/4 v0, 0x4

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a(ILandroid/os/Bundle;)V

    .line 523
    :goto_1
    return-void

    .line 513
    :cond_3
    :try_start_0
    invoke-virtual {v0, p1}, Landroid/app/PendingIntent;->send(I)V
    :try_end_0
    .catch Landroid/app/PendingIntent$CanceledException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v2, "WalletFragmentDelegateImpl"

    const-string v3, "Exception setting pending result"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 522
    :cond_4
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b(I)V

    goto :goto_1
.end method

.method private a(ILandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 447
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->t:I

    if-ne v0, p1, :cond_1

    .line 460
    :cond_0
    :goto_0
    return-void

    .line 450
    :cond_1
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->t:I

    .line 451
    iput p1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->t:I

    .line 452
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->n()V

    .line 453
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->m()Landroid/app/Activity;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 455
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->p:Lcom/google/android/gms/wallet/fragment/a/c;

    invoke-interface {v1, v0, p1, p2}, Lcom/google/android/gms/wallet/fragment/a/c;->a(IILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 456
    :catch_0
    move-exception v0

    .line 457
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b(I)V
    .locals 1

    .prologue
    .line 443
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a(ILandroid/os/Bundle;)V

    .line 444
    return-void
.end method

.method private c(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 386
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/dynamite/service/a/c;->c(Landroid/os/Bundle;)V

    .line 387
    return-void
.end method

.method private g()Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a:Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;->a()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->n:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a:Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;->b()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/c/l;->a(ILjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;

    move-result-object v0

    .line 358
    const-string v1, "com.google.android.gms.wallet.CLIENT"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 359
    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 363
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->o()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 374
    :goto_0
    return-void

    .line 366
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    if-nez v0, :cond_1

    .line 369
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->v:Z

    goto :goto_0

    .line 371
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->g()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/dynamite/service/a/c;->b(Landroid/os/Bundle;)V

    .line 372
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->v:Z

    goto :goto_0
.end method

.method private i()V
    .locals 3

    .prologue
    .line 390
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->q()V

    .line 391
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->l()Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->g()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/dynamite/service/a/c;->a(Lcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V

    .line 392
    const/4 v0, 0x3

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a(ILandroid/os/Bundle;)V

    .line 393
    return-void
.end method

.method private j()V
    .locals 4

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    if-nez v0, :cond_1

    .line 406
    :cond_0
    :goto_0
    return-void

    .line 399
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->k()Lcom/google/android/gms/wallet/MaskedWalletRequest;

    move-result-object v0

    .line 400
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->g()Landroid/os/Bundle;

    move-result-object v1

    .line 401
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->l()Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 402
    const-string v2, "com.google.android.gms.wallet.EXTRA_GOOGLE_TRANSACTION_ID"

    iget-object v3, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;->d()Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/MaskedWallet;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 405
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/wallet/dynamite/service/a/c;->b(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method private k()Lcom/google/android/gms/wallet/MaskedWalletRequest;
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->c:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    if-eqz v0, :cond_0

    .line 410
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->c:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    .line 415
    :goto_0
    return-object v0

    .line 412
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    if-eqz v0, :cond_1

    .line 413
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;->b()Lcom/google/android/gms/wallet/MaskedWalletRequest;

    move-result-object v0

    goto :goto_0

    .line 415
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()Lcom/google/android/gms/wallet/MaskedWallet;
    .locals 1

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->d:Lcom/google/android/gms/wallet/MaskedWallet;

    if-eqz v0, :cond_0

    .line 426
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->d:Lcom/google/android/gms/wallet/MaskedWallet;

    .line 431
    :goto_0
    return-object v0

    .line 428
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    if-eqz v0, :cond_1

    .line 429
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;->d()Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v0

    goto :goto_0

    .line 431
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m()Landroid/app/Activity;
    .locals 2

    .prologue
    .line 436
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->m:Lcom/google/android/gms/b/i;

    invoke-interface {v0}, Lcom/google/android/gms/b/i;->a()Lcom/google/android/gms/b/l;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Lcom/google/android/gms/b/l;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 437
    :catch_0
    move-exception v0

    .line 438
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private n()V
    .locals 2

    .prologue
    .line 463
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->s:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 464
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->t:I

    packed-switch v0, :pswitch_data_0

    .line 469
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->s:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 473
    :cond_0
    :goto_0
    return-void

    .line 466
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->s:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->u:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0

    .line 464
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private o()Z
    .locals 2

    .prologue
    .line 526
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->p()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private p()I
    .locals 1

    .prologue
    .line 530
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a:Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    if-eqz v0, :cond_0

    .line 531
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a:Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;->d()I

    move-result v0

    .line 533
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()V
    .locals 2

    .prologue
    .line 537
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->p()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->l()Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 538
    const-string v0, "WalletFragmentDelegateImpl"

    const-string v1, "WalletFragment inconsistency: BUY_BUTTON mode should not have masked wallet set."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    :cond_0
    :goto_0
    return-void

    .line 540
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->p()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->k()Lcom/google/android/gms/wallet/MaskedWalletRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 542
    const-string v0, "WalletFragmentDelegateImpl"

    const-string v1, "WalletFragment inconsistency: SELECTION_DETAILS mode should not have masked wallet request set."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->t:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x2

    const/4 v1, 0x1

    .line 337
    if-ne p1, v2, :cond_1

    .line 338
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 339
    const-string v1, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    const/16 v2, 0x192

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 341
    invoke-direct {p0, v3, v0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a(ILandroid/os/Bundle;)V

    .line 352
    :cond_0
    :goto_0
    return-void

    .line 342
    :cond_1
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->t:I

    if-ne v0, v3, :cond_0

    if-ne p1, v1, :cond_0

    .line 344
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    if-eqz v0, :cond_2

    .line 345
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b(I)V

    goto :goto_0

    .line 349
    :cond_2
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b(I)V

    goto :goto_0
.end method

.method public final a(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 292
    if-nez p3, :cond_0

    .line 293
    new-instance p3, Landroid/content/Intent;

    invoke-direct {p3}, Landroid/content/Intent;-><init>()V

    .line 295
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 300
    const-string v0, "WalletFragmentDelegateImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized request code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 303
    :goto_0
    return-void

    .line 297
    :pswitch_0
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 295
    nop

    :pswitch_data_0
    .packed-switch 0x1f4
        :pswitch_0
    .end packed-switch
.end method

.method public final a(ILcom/google/android/gms/wallet/MaskedWallet;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 307
    const/4 v0, 0x0

    .line 308
    if-eqz p3, :cond_0

    .line 309
    const-string v0, "com.google.android.gms.wallet.INTENT"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 311
    :cond_0
    const/4 v1, 0x6

    if-ne p1, v1, :cond_1

    if-eqz v0, :cond_1

    .line 313
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->m:Lcom/google/android/gms/b/i;

    const/16 v2, 0x1f4

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/b/i;->a(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 333
    :goto_0
    return-void

    .line 314
    :catch_0
    move-exception v0

    .line 315
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 319
    :cond_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 320
    if-nez p1, :cond_2

    .line 321
    const/4 v0, -0x1

    .line 322
    const-string v2, "com.google.android.gms.wallet.EXTRA_MASKED_WALLET"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 331
    :goto_1
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a(ILandroid/content/Intent;)V

    goto :goto_0

    .line 324
    :cond_2
    const/16 v0, 0x198

    if-ne p1, v0, :cond_3

    .line 325
    const/4 v0, 0x0

    .line 329
    :goto_2
    const-string v2, "com.google.android.gms.wallet.EXTRA_ERROR_CODE"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    .line 327
    :cond_3
    const/4 v0, 0x1

    goto :goto_2
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 156
    if-eqz p1, :cond_6

    .line 157
    const-class v0, Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 158
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->j:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->t:I

    .line 159
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->k:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->u:Z

    .line 161
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->l:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->q:Ljava/lang/String;

    .line 167
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 168
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a:Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    .line 171
    :cond_0
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    .line 172
    if-eqz v0, :cond_2

    .line 173
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    if-eqz v1, :cond_1

    .line 174
    const-string v1, "WalletFragment"

    const-string v2, "initialize(WalletFragmentInitParams) was called more than once. Ignoring."

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_1
    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    .line 180
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->c:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    if-nez v0, :cond_3

    .line 181
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/MaskedWalletRequest;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->c:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    .line 183
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->c:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    if-nez v0, :cond_4

    .line 184
    const-string v0, "WalletFragment"

    const-string v1, "updateMaskedWalletRequest() was called before initialize()"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->d:Lcom/google/android/gms/wallet/MaskedWallet;

    if-nez v0, :cond_5

    .line 187
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/MaskedWallet;

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->d:Lcom/google/android/gms/wallet/MaskedWallet;

    .line 189
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->d:Lcom/google/android/gms/wallet/MaskedWallet;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    if-nez v0, :cond_6

    .line 190
    const-string v0, "WalletFragment"

    const-string v1, "updateMaskedWallet() was called before initialize()"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 193
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 194
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->q:Ljava/lang/String;

    .line 196
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->q:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->n:Landroid/content/Context;

    sget-object v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->a:Lcom/google/android/gms/common/internal/a/b;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/android/gms/common/internal/a/b;

    const/16 v3, 0x14

    invoke-direct {v0, v3}, Lcom/google/android/gms/common/internal/a/b;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->a:Lcom/google/android/gms/common/internal/a/b;

    :cond_8
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->a:Lcom/google/android/gms/common/internal/a/b;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/a/b;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/dynamite/service/a/c;-><init>(Ljava/lang/String;Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/wallet/dynamite/service/a/c;->a:Lcom/google/android/gms/common/internal/a/b;

    invoke-virtual {v2, v1, v0}, Lcom/google/android/gms/common/internal/a/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_9
    iput-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    .line 197
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->j()V

    .line 198
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->q()V

    .line 199
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->v:Z

    if-eqz v0, :cond_a

    .line 200
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->h()V

    .line 202
    :cond_a
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/MaskedWallet;)V
    .locals 2

    .prologue
    .line 125
    iput-object p1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->d:Lcom/google/android/gms/wallet/MaskedWallet;

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->s:Landroid/view/View;

    instance-of v0, v0, Lcom/google/android/gms/wallet/dynamite/ui/g;

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->s:Landroid/view/View;

    check-cast v0, Lcom/google/android/gms/wallet/dynamite/ui/g;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->l()Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/dynamite/ui/g;->a(Lcom/google/android/gms/wallet/MaskedWallet;)V

    .line 129
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/MaskedWalletRequest;)V
    .locals 0

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->c:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    .line 121
    return-void
.end method

.method public final a(Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;)V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    if-nez v0, :cond_3

    .line 98
    iput-object p1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->c:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "WalletFragment"

    const-string v1, "updateMaskedWalletRequest() was called before initialize()"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->d:Lcom/google/android/gms/wallet/MaskedWallet;

    if-eqz v0, :cond_1

    .line 103
    const-string v0, "WalletFragment"

    const-string v1, "updateMaskedWallet() was called before initialize()"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;->d()Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a(Lcom/google/android/gms/wallet/MaskedWallet;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->s:Landroid/view/View;

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->t:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    .line 107
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b(I)V

    .line 109
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->j()V

    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->q()V

    .line 111
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->h()V

    .line 116
    :goto_0
    return-void

    .line 113
    :cond_3
    const-string v0, "WalletFragment"

    const-string v1, "initialize(WalletFragmentInitParams) was called more than once. Ignoring."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;)V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a:Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    if-nez v0, :cond_0

    .line 150
    iput-object p1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a:Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    .line 152
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 133
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->u:Z

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->s:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 135
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->n()V

    .line 137
    :cond_0
    return-void
.end method

.method public final b()Lcom/google/android/gms/b/l;
    .locals 4

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a:Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->o:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;->c()Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;

    move-result-object v2

    if-nez v2, :cond_0

    new-instance v0, Lcom/google/android/gms/wallet/dynamite/fragment/a;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/dynamite/fragment/a;-><init>()V

    .line 209
    :goto_0
    iget v1, v0, Lcom/google/android/gms/wallet/dynamite/fragment/a;->c:I

    iput v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->w:I

    .line 210
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->o()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 211
    new-instance v1, Lcom/google/android/gms/wallet/dynamite/ui/g;

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->o:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->n:Landroid/content/Context;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/wallet/dynamite/ui/g;-><init>(Landroid/content/Context;Landroid/content/Context;Lcom/google/android/gms/wallet/dynamite/fragment/a;)V

    .line 213
    invoke-virtual {v1, p0}, Lcom/google/android/gms/wallet/dynamite/ui/g;->a(Landroid/view/View$OnClickListener;)V

    .line 214
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->l()Lcom/google/android/gms/wallet/MaskedWallet;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/dynamite/ui/g;->a(Lcom/google/android/gms/wallet/MaskedWallet;)V

    .line 215
    iput-object v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->s:Landroid/view/View;

    .line 221
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->s:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0

    .line 207
    :cond_0
    new-instance v0, Lcom/google/android/gms/wallet/dynamite/fragment/a;

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/wallet/dynamite/fragment/a;-><init>(Lcom/google/android/gms/wallet/fragment/WalletFragmentStyle;Landroid/util/DisplayMetrics;)V

    goto :goto_0

    .line 217
    :cond_1
    new-instance v1, Lcom/google/android/gms/wallet/dynamite/ui/j;

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->o:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/wallet/dynamite/ui/j;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/dynamite/fragment/a;)V

    .line 218
    invoke-virtual {v1, p0}, Lcom/google/android/gms/wallet/dynamite/ui/j;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 219
    iput-object v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->s:Landroid/view/View;

    goto :goto_1
.end method

.method public final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->f:Ljava/lang/ref/WeakReference;

    .line 267
    const-class v0, Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a:Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    if-eqz v0, :cond_0

    .line 269
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->f:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a:Lcom/google/android/gms/wallet/fragment/WalletFragmentOptions;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    if-eqz v0, :cond_1

    .line 272
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->g:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 274
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->c:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    if-eqz v0, :cond_2

    .line 275
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->i:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->c:Lcom/google/android/gms/wallet/MaskedWalletRequest;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 277
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->d:Lcom/google/android/gms/wallet/MaskedWallet;

    if-eqz v0, :cond_3

    .line 278
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->h:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->d:Lcom/google/android/gms/wallet/MaskedWallet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 280
    :cond_3
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->k:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 281
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->j:Ljava/lang/String;

    iget v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->t:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 282
    sget-object v0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->l:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    iget-object v1, v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->g:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->e:Lcom/google/android/gms/wallet/dynamite/service/a/b;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->a()V

    .line 247
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 251
    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->t:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b:Lcom/google/android/gms/wallet/fragment/WalletFragmentInitParams;

    if-eqz v0, :cond_0

    .line 252
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->b(I)V

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->f:Ljava/lang/ref/WeakReference;

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/dynamite/service/a/c;->a()V

    .line 256
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->n()V

    .line 257
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->f:Ljava/lang/ref/WeakReference;

    .line 262
    return-void
.end method

.method public final f()V
    .locals 4

    .prologue
    .line 287
    iget-object v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    iget-object v1, v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->e:Lcom/google/android/gms/wallet/dynamite/service/a/b;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/dynamite/service/a/b;->b()V

    iget-object v1, v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->g:Landroid/os/Handler;

    iget-object v0, v0, Lcom/google/android/gms/wallet/dynamite/service/a/c;->g:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-static {v0, v2}, Landroid/os/Message;->obtain(Landroid/os/Handler;I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 288
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x3

    .line 226
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->g()Landroid/os/Bundle;

    move-result-object v2

    .line 227
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    .line 228
    sget v3, Lcom/google/android/gms/j;->cf:I

    if-ne v0, v3, :cond_0

    .line 229
    const-string v0, "com.google.android.gms.wallet.fragment.BUTTON"

    const/4 v1, 0x1

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 231
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->c(Landroid/os/Bundle;)V

    .line 232
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->i()V

    .line 242
    :goto_0
    return-void

    .line 233
    :cond_0
    sget v3, Lcom/google/android/gms/j;->cg:I

    if-ne v0, v3, :cond_1

    .line 234
    const-string v0, "com.google.android.gms.wallet.fragment.BUTTON"

    const/4 v1, 0x2

    invoke-virtual {v2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 235
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->c(Landroid/os/Bundle;)V

    .line 236
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->i()V

    goto :goto_0

    .line 238
    :cond_1
    const-string v3, "com.google.android.gms.wallet.fragment.BUTTON"

    iget v0, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->w:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_1
    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 239
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->c(Landroid/os/Bundle;)V

    .line 240
    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->q()V

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->k()Lcom/google/android/gms/wallet/MaskedWalletRequest;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/dynamite/fragment/b;->r:Lcom/google/android/gms/wallet/dynamite/service/a/c;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->g()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gms/wallet/dynamite/service/a/c;->a(Lcom/google/android/gms/wallet/MaskedWalletRequest;Landroid/os/Bundle;)V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "com.google.android.gms.wallet.EXTRA_MASKED_WALLET_REQUEST"

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/wallet/dynamite/fragment/b;->a(ILandroid/os/Bundle;)V

    goto :goto_0

    :pswitch_0
    move v0, v1

    .line 238
    goto :goto_1

    :pswitch_1
    const/4 v0, 0x4

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x5

    goto :goto_1

    :pswitch_3
    const/4 v0, 0x6

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
