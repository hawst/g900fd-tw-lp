.class final Lcom/google/android/gms/drive/c/a/o;
.super Lcom/google/android/gms/drive/c/a/m;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/c/d;


# instance fields
.field private c:Ljava/lang/String;

.field private d:Lcom/google/android/gms/drive/auth/CallingAppInfo;

.field private e:Z


# direct methods
.method constructor <init>(Landroid/os/Bundle;Lcom/google/android/gms/drive/c/a/n;Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 52
    const-string v0, "account-name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v0, "impression-session"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    new-instance v2, Lcom/google/android/libraries/rocket/impressions/i;

    invoke-direct {v2}, Lcom/google/android/libraries/rocket/impressions/i;-><init>()V

    invoke-interface {p2, v1}, Lcom/google/android/gms/drive/c/a/n;->a(Ljava/lang/String;)Lcom/google/android/libraries/rocket/impressions/o;

    move-result-object v1

    check-cast v0, Lcom/google/android/libraries/rocket/impressions/Session;

    new-instance v3, Lcom/google/android/libraries/rocket/impressions/h;

    iget-object v2, v2, Lcom/google/android/libraries/rocket/impressions/i;->a:Lcom/google/android/libraries/rocket/impressions/f;

    invoke-direct {v3, v2, v1, v0}, Lcom/google/android/libraries/rocket/impressions/h;-><init>(Lcom/google/android/libraries/rocket/impressions/f;Lcom/google/android/libraries/rocket/impressions/o;Lcom/google/android/libraries/rocket/impressions/Session;)V

    invoke-virtual {v3}, Lcom/google/android/libraries/rocket/impressions/h;->b()V

    invoke-direct {p0, v3, p3}, Lcom/google/android/gms/drive/c/a/m;-><init>(Lcom/google/android/libraries/rocket/impressions/h;Landroid/content/Context;)V

    .line 53
    const-string v0, "app-info"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/CallingAppInfo;

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/o;->d:Lcom/google/android/gms/drive/auth/CallingAppInfo;

    .line 54
    const-string v0, "account-name"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/o;->c:Ljava/lang/String;

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/c/a/o;->e:Z

    .line 56
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/drive/auth/CallingAppInfo;Ljava/lang/String;Lcom/google/android/gms/drive/c/a/n;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/libraries/rocket/impressions/i;

    invoke-direct {v0}, Lcom/google/android/libraries/rocket/impressions/i;-><init>()V

    invoke-interface {p3, p2}, Lcom/google/android/gms/drive/c/a/n;->a(Ljava/lang/String;)Lcom/google/android/libraries/rocket/impressions/o;

    move-result-object v1

    const/16 v2, 0xf

    invoke-static {v2}, Lcom/google/android/gms/drive/c/a/h;->a(I)Lcom/google/android/libraries/rocket/impressions/m;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/rocket/impressions/i;->a(Lcom/google/android/libraries/rocket/impressions/o;Lcom/google/android/libraries/rocket/impressions/m;)Lcom/google/android/libraries/rocket/impressions/h;

    move-result-object v0

    invoke-direct {p0, v0, p4}, Lcom/google/android/gms/drive/c/a/m;-><init>(Lcom/google/android/libraries/rocket/impressions/h;Landroid/content/Context;)V

    .line 35
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/auth/CallingAppInfo;

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/o;->d:Lcom/google/android/gms/drive/auth/CallingAppInfo;

    .line 36
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/drive/c/a/o;->c:Ljava/lang/String;

    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/c/a/o;->e:Z

    .line 38
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/c/a/d;)V
    .locals 2

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/android/gms/drive/c/a/o;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t send log event when paused"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 97
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/c/a/m;->a(Lcom/google/android/gms/drive/c/a/d;)V

    .line 98
    return-void

    .line 96
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final synthetic c()Lcom/google/android/gms/drive/c/a;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0}, Lcom/google/android/gms/drive/c/a/o;->d()Lcom/google/android/gms/drive/c/a/d;

    move-result-object v0

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/drive/c/a/d;
    .locals 2

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/android/gms/drive/c/a/o;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Can\'t create log event when paused"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 72
    invoke-super {p0}, Lcom/google/android/gms/drive/c/a/m;->d()Lcom/google/android/gms/drive/c/a/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/o;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/c/a/d;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/c/a/d;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/c/a/o;->d:Lcom/google/android/gms/drive/auth/CallingAppInfo;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/c/a/d;->b(Lcom/google/android/gms/drive/auth/CallingAppInfo;)Lcom/google/android/gms/drive/c/a/d;

    move-result-object v0

    return-object v0

    .line 70
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/google/android/gms/drive/c/a/o;->e:Z

    return v0
.end method

.method public final m_()Landroid/os/Bundle;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 79
    iget-boolean v0, p0, Lcom/google/android/gms/drive/c/a/o;->e:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Can\'t pause an already paused session"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 80
    iput-boolean v1, p0, Lcom/google/android/gms/drive/c/a/o;->e:Z

    .line 82
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 83
    const-string v1, "impression-session"

    iget-object v2, p0, Lcom/google/android/gms/drive/c/a/o;->a:Lcom/google/android/libraries/rocket/impressions/h;

    invoke-virtual {v2}, Lcom/google/android/libraries/rocket/impressions/h;->c()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 84
    const-string v1, "account-name"

    iget-object v2, p0, Lcom/google/android/gms/drive/c/a/o;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const-string v1, "app-info"

    iget-object v2, p0, Lcom/google/android/gms/drive/c/a/o;->d:Lcom/google/android/gms/drive/auth/CallingAppInfo;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 86
    return-object v0

    .line 79
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
