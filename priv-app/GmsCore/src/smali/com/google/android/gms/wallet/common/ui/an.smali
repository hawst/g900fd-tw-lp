.class public final Lcom/google/android/gms/wallet/common/ui/an;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lcom/google/android/gms/wallet/common/ui/ah;
.implements Lcom/google/android/gms/wallet/common/ui/cb;
.implements Lcom/google/android/gms/wallet/common/ui/dp;


# static fields
.field static final a:Ljava/lang/String;


# instance fields
.field private A:Landroid/content/Intent;

.field private B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

.field private C:Ljava/lang/String;

.field private D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

.field private E:I

.field private F:Lcom/google/android/gms/wallet/common/ui/ah;

.field private G:Landroid/view/View;

.field private final H:Landroid/text/TextWatcher;

.field private final I:Lcom/google/android/gms/wallet/service/l;

.field b:I

.field c:Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;

.field d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

.field e:Landroid/widget/TextView;

.field f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field g:Landroid/widget/TextView;

.field h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

.field j:Lcom/google/android/gms/wallet/common/ui/ax;

.field k:Landroid/view/View;

.field l:Landroid/view/View;

.field m:Lcom/google/android/gms/wallet/common/ui/bh;

.field n:Lcom/google/android/gms/wallet/common/ui/bi;

.field o:Lcom/google/android/gms/wallet/common/ui/aw;

.field p:Lcom/google/android/gms/wallet/common/ui/ai;

.field private q:Z

.field private r:Lcom/google/android/gms/wallet/common/ui/dh;

.field private s:Ljava/util/HashSet;

.field private t:Ljava/lang/String;

.field private u:Ljava/util/ArrayList;

.field private v:J

.field private w:I

.field private x:[I

.field private y:[I

.field private z:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const-string v0, "ccEntry"

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/an;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 64
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->q:Z

    .line 117
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->b:I

    .line 120
    new-instance v0, Ljava/util/HashSet;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->s:Ljava/util/HashSet;

    .line 121
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->t:Ljava/lang/String;

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->u:Ljava/util/ArrayList;

    .line 123
    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->v:J

    .line 124
    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->w:I

    .line 132
    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    .line 811
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/aq;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/aq;-><init>(Lcom/google/android/gms/wallet/common/ui/an;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->H:Landroid/text/TextWatcher;

    .line 877
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ar;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/common/ui/ar;-><init>(Lcom/google/android/gms/wallet/common/ui/an;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->I:Lcom/google/android/gms/wallet/service/l;

    .line 955
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/an;)I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    return v0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;I[I[ILjava/lang/String;)Lcom/google/android/gms/wallet/common/ui/an;
    .locals 3

    .prologue
    .line 186
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/an;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/an;-><init>()V

    .line 188
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 189
    const-string v2, "disallowedCreditCardTypes"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 190
    const-string v2, "disallowedCardCategories"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 191
    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 192
    const-string v2, "account"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 193
    const-string v2, "cardEntryContext"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 194
    const-string v2, "analyticsSessionId"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/an;->setArguments(Landroid/os/Bundle;)V

    .line 197
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/an;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/an;->b(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/an;)I
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    return v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 801
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->s:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 809
    :cond_0
    :goto_0
    return-void

    .line 804
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->t:Ljava/lang/String;

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->l()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 805
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/an;->t:Ljava/lang/String;

    .line 806
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->l()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/dh;->a:Lcom/google/android/gms/wallet/service/k;

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->w:I

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/wallet/service/k;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private b(Z)Z
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 221
    new-array v4, v7, [Lcom/google/android/gms/wallet/common/ui/dn;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    aput-object v0, v4, v2

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->m:Lcom/google/android/gms/wallet/common/ui/bh;

    aput-object v0, v4, v1

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/an;->n:Lcom/google/android/gms/wallet/common/ui/bi;

    aput-object v3, v4, v0

    const/4 v0, 0x3

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/an;->o:Lcom/google/android/gms/wallet/common/ui/aw;

    aput-object v3, v4, v0

    .line 228
    array-length v5, v4

    move v3, v2

    move v0, v1

    :goto_0
    if-ge v3, v5, :cond_3

    aget-object v6, v4, v3

    .line 229
    if-eqz p1, :cond_2

    .line 230
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->f()Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v0, :cond_1

    move v0, v1

    .line 228
    :cond_0
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 230
    goto :goto_1

    .line 231
    :cond_2
    invoke-interface {v6}, Lcom/google/android/gms/wallet/common/ui/dn;->e()Z

    move-result v6

    if-nez v6, :cond_0

    .line 238
    :goto_2
    return v2

    .line 235
    :cond_3
    if-eqz p1, :cond_4

    if-nez v0, :cond_4

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    if-ne v1, v7, :cond_4

    .line 236
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/common/ui/an;->d(Z)V

    :cond_4
    move v2, v0

    .line 238
    goto :goto_2
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/common/ui/an;)Lcom/google/android/gms/wallet/common/ui/ah;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->F:Lcom/google/android/gms/wallet/common/ui/ah;

    return-object v0
.end method

.method private c(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 605
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 606
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setVisibility(I)V

    .line 607
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 608
    if-eqz p1, :cond_0

    .line 609
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getHeight()I

    move-result v0

    .line 610
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    neg-int v2, v0

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/ui/dt;->b(Landroid/view/View;I)V

    .line 611
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->g:Landroid/widget/TextView;

    neg-int v2, v0

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/ui/dt;->b(Landroid/view/View;I)V

    .line 612
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    neg-int v2, v0

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/ui/dt;->b(Landroid/view/View;I)V

    .line 613
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    neg-int v2, v0

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/ui/dt;->b(Landroid/view/View;I)V

    .line 614
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->j:Lcom/google/android/gms/wallet/common/ui/ax;

    neg-int v0, v0

    iget-object v1, v1, Lcom/google/android/gms/wallet/common/ui/ax;->a:Landroid/widget/ImageView;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/dt;->b(Landroid/view/View;I)V

    .line 622
    :goto_0
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    .line 623
    return-void

    .line 616
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 617
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 618
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 619
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 620
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->j:Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ax;->b(I)V

    goto :goto_0
.end method

.method private d()V
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    if-eqz v0, :cond_0

    .line 328
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->q:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setEnabled(Z)V

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->q:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 330
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->q:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 331
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->q:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setEnabled(Z)V

    .line 332
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->k:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->q:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 333
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->l:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->q:Z

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 335
    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/wallet/common/ui/an;)V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/an;->c(Z)V

    return-void
.end method

.method private d(Z)V
    .locals 3

    .prologue
    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 626
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 627
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->l:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 628
    if-eqz p1, :cond_0

    .line 629
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;I)V

    .line 630
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getHeight()I

    move-result v0

    .line 631
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;I)V

    .line 632
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->g:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;I)V

    .line 633
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;I)V

    .line 634
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v1, v0}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;I)V

    .line 635
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->j:Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/ax;->c(I)V

    .line 644
    :goto_0
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    .line 645
    return-void

    .line 637
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setVisibility(I)V

    .line 638
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 639
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 640
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 641
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 642
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->j:Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ax;->b(I)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/gms/wallet/common/ui/an;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->j()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/wallet/common/ui/an;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->t:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/wallet/common/ui/an;)Ljava/util/HashSet;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->s:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/wallet/common/ui/an;)J
    .locals 2

    .prologue
    .line 64
    const-wide/16 v0, 0xbb8

    iput-wide v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->v:J

    return-wide v0
.end method

.method private h()Lcom/google/checkout/a/a/a/b;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 354
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 355
    const/4 v0, 0x0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v6

    add-int/lit8 v6, v6, -0x4

    invoke-static {v0, v6}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-virtual {v4, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v6

    .line 356
    invoke-static {v4}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;)I

    move-result v7

    .line 359
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 365
    :goto_0
    :try_start_1
    iget-object v8, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v8}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    add-int/lit16 v8, v8, 0x7d0

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    .line 370
    :goto_1
    new-instance v8, Lcom/google/checkout/a/a/a/b;

    invoke-direct {v8}, Lcom/google/checkout/a/a/a/b;-><init>()V

    .line 371
    if-eqz v0, :cond_0

    .line 372
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v8, Lcom/google/checkout/a/a/a/b;->b:I

    .line 374
    :cond_0
    if-eqz v1, :cond_1

    .line 375
    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v8, Lcom/google/checkout/a/a/a/b;->c:I

    .line 377
    :cond_1
    if-eqz v7, :cond_2

    .line 378
    iput v7, v8, Lcom/google/checkout/a/a/a/b;->f:I

    .line 380
    :cond_2
    iput-object v6, v8, Lcom/google/checkout/a/a/a/b;->e:Ljava/lang/String;

    .line 381
    new-instance v0, Lcom/google/checkout/a/a/a/c;

    invoke-direct {v0}, Lcom/google/checkout/a/a/a/c;-><init>()V

    iput-object v0, v8, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    .line 382
    iget-object v0, v8, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    iput-object v5, v0, Lcom/google/checkout/a/a/a/c;->b:Ljava/lang/String;

    .line 383
    iget-object v0, v8, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    iput-object v4, v0, Lcom/google/checkout/a/a/a/c;->a:Ljava/lang/String;

    .line 388
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    if-eqz v0, :cond_5

    .line 389
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    invoke-virtual {v0}, Lcom/google/android/gms/ocr/CreditCardOcrResult;->e()Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, v8, Lcom/google/checkout/a/a/a/b;->b:I

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    invoke-virtual {v1}, Lcom/google/android/gms/ocr/CreditCardOcrResult;->c()I

    move-result v1

    if-ne v0, v1, :cond_4

    iget v0, v8, Lcom/google/checkout/a/a/a/b;->c:I

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    invoke-virtual {v1}, Lcom/google/android/gms/ocr/CreditCardOcrResult;->d()I

    move-result v1

    if-ne v0, v1, :cond_4

    move v0, v2

    .line 394
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    invoke-virtual {v1}, Lcom/google/android/gms/ocr/CreditCardOcrResult;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    invoke-virtual {v1}, Lcom/google/android/gms/ocr/CreditCardOcrResult;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v4}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v3, v2

    .line 400
    :cond_3
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->C:Ljava/lang/String;

    invoke-static {v1, v3, v0, v2}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;->a(Landroid/content/Context;IILjava/lang/String;)Ljava/lang/String;

    .line 403
    return-object v8

    :catch_0
    move-exception v0

    move-object v0, v1

    goto/16 :goto_0

    :catch_1
    move-exception v8

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_2

    :cond_5
    move v0, v3

    goto :goto_3
.end method

.method private i()V
    .locals 2

    .prologue
    .line 770
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->b:I

    if-gez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->l()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 771
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->l()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/dh;->a:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->I:Lcom/google/android/gms/wallet/service/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/service/k;->c(Lcom/google/android/gms/wallet/service/l;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->b:I

    .line 776
    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/google/android/gms/wallet/common/ui/an;)[I
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->y:[I

    return-object v0
.end method

.method private j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 784
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    if-nez v0, :cond_0

    .line 785
    const/4 v0, 0x0

    .line 787
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic j(Lcom/google/android/gms/wallet/common/ui/an;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->u:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/wallet/common/ui/an;)J
    .locals 2

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->v:J

    return-wide v0
.end method

.method private k()V
    .locals 1

    .prologue
    .line 794
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->j()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/an;->b(Ljava/lang/String;)V

    .line 795
    return-void
.end method

.method private l()Lcom/google/android/gms/wallet/common/ui/dh;
    .locals 2

    .prologue
    .line 869
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->r:Lcom/google/android/gms/wallet/common/ui/dh;

    if-nez v0, :cond_0

    .line 870
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/wallet/common/ui/an;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/dh;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->r:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 874
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->r:Lcom/google/android/gms/wallet/common/ui/dh;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/gms/wallet/common/ui/an;)Lcom/google/android/gms/wallet/service/l;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->I:Lcom/google/android/gms/wallet/service/l;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/gms/wallet/common/ui/an;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->t:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/gms/wallet/common/ui/an;)V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->k()V

    return-void
.end method

.method static synthetic o(Lcom/google/android/gms/wallet/common/ui/an;)J
    .locals 4

    .prologue
    .line 64
    iget-wide v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->v:J

    const-wide/16 v2, 0x2

    mul-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->v:J

    return-wide v0
.end method


# virtual methods
.method public final a()Lcom/google/checkout/a/a/a/d;
    .locals 2

    .prologue
    .line 346
    new-instance v0, Lcom/google/checkout/a/a/a/d;

    invoke-direct {v0}, Lcom/google/checkout/a/a/a/d;-><init>()V

    .line 347
    const/4 v1, 0x1

    iput v1, v0, Lcom/google/checkout/a/a/a/d;->a:I

    .line 348
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->h()Lcom/google/checkout/a/a/a/b;

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/a/a/a/d;->b:Lcom/google/checkout/a/a/a/b;

    .line 349
    return-object v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 939
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getId()I

    move-result v0

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    if-lt v0, v2, :cond_1

    .line 948
    :cond_0
    :goto_0
    return-void

    .line 944
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->j:Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/ax;->c(I)V

    .line 945
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/dt;->a(Landroid/view/View;I)V

    .line 946
    iput v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    .line 947
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/wallet/common/ui/ah;)V
    .locals 0

    .prologue
    .line 756
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/an;->F:Lcom/google/android/gms/wallet/common/ui/ah;

    .line 757
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 268
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->f()Z

    .line 269
    return-void
.end method

.method public final a(Z)V
    .locals 0

    .prologue
    .line 295
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/common/ui/an;->q:Z

    .line 296
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->d()V

    .line 297
    return-void
.end method

.method public final a([I)V
    .locals 2

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->x:[I

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 301
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/an;->x:[I

    .line 302
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->x:[I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a([I)V

    .line 304
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 262
    sget v0, Lcom/google/android/gms/p;->Bp:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/an;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/an;->a(Ljava/lang/String;)V

    .line 263
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;

    sget v2, Lcom/google/android/gms/p;->Bq:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/wallet/common/ui/an;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/wallet/common/ui/validator/BlacklistValidator;-><init>(Ljava/lang/CharSequence;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/validator/r;)V

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->f()Z

    .line 276
    return-void
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/an;->b(Z)Z

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 202
    invoke-direct {p0, v1}, Lcom/google/android/gms/wallet/common/ui/an;->b(Z)Z

    move-result v3

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 205
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    move v2, v1

    .line 208
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->C:Ljava/lang/String;

    invoke-static {v1, v0, v2, v4}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntryValidationEvent;->a(Landroid/content/Context;ZZLjava/lang/String;)Ljava/lang/String;

    .line 211
    return v3

    :cond_2
    move v0, v2

    .line 204
    goto :goto_0
.end method

.method public final g()Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 244
    const/4 v2, 0x4

    new-array v3, v2, [Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    aput-object v2, v3, v1

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v2, v3, v0

    const/4 v2, 0x2

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v4, v3, v2

    const/4 v2, 0x3

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    aput-object v4, v3, v2

    .line 251
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 252
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->getError()Ljava/lang/CharSequence;

    move-result-object v6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 253
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->clearFocus()V

    .line 254
    invoke-virtual {v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    .line 258
    :goto_1
    return v0

    .line 251
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 258
    goto :goto_1
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 8

    .prologue
    const/16 v2, 0x8

    const/4 v6, 0x3

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v5, 0x0

    .line 675
    const/16 v0, 0x1f4

    if-ne p1, v0, :cond_9

    .line 678
    packed-switch p2, :pswitch_data_0

    :cond_0
    move v4, v5

    move v3, v5

    .line 712
    :cond_1
    :goto_0
    if-eqz p3, :cond_8

    const-string v0, "com.google.android.gms.ocr.EXP_DATE_RECOGNITION_ENABLED"

    invoke-virtual {p3, v0, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_8

    move v2, v1

    .line 714
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sparse-switch p2, :sswitch_data_0

    move v1, v5

    :goto_2
    :sswitch_0
    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/an;->C:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/wallet/analytics/events/OcrResultReceivedEvent;->a(Landroid/content/Context;IZZZLjava/lang/String;)Ljava/lang/String;

    .line 719
    :goto_3
    return-void

    .line 680
    :pswitch_0
    invoke-static {p3}, Lcom/google/android/gms/ocr/CreditCardOcrResult;->a(Landroid/content/Intent;)Lcom/google/android/gms/ocr/CreditCardOcrResult;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    .line 681
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    if-eqz v0, :cond_0

    .line 682
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->G:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 683
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 684
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->z:Ljava/lang/CharSequence;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 685
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 686
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 687
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 688
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 689
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 690
    const/16 v0, 0xe

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 691
    iput v7, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    .line 697
    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    invoke-virtual {v0}, Lcom/google/android/gms/ocr/CreditCardOcrResult;->b()Ljava/lang/String;

    move-result-object v0

    .line 698
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_7

    move v3, v1

    .line 699
    :goto_5
    if-eqz v3, :cond_3

    .line 700
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setText(Ljava/lang/CharSequence;)V

    .line 701
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    .line 703
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    invoke-virtual {v0}, Lcom/google/android/gms/ocr/CreditCardOcrResult;->e()Z

    move-result v4

    .line 704
    if-eqz v4, :cond_1

    .line 705
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    invoke-virtual {v0}, Lcom/google/android/gms/ocr/CreditCardOcrResult;->c()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v2, :cond_4

    if-lez v0, :cond_4

    const/16 v2, 0xd

    if-ge v0, v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setText(Ljava/lang/CharSequence;)V

    .line 706
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    invoke-virtual {v0}, Lcom/google/android/gms/ocr/CreditCardOcrResult;->d()I

    move-result v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    if-eqz v2, :cond_5

    if-ltz v0, :cond_5

    rem-int/lit8 v0, v0, 0x64

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setText(Ljava/lang/CharSequence;)V

    .line 707
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->requestFocus()Z

    goto/16 :goto_0

    .line 693
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->j:Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/wallet/common/ui/ax;->b(I)V

    .line 694
    iput v6, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    goto :goto_4

    :cond_7
    move v3, v5

    .line 698
    goto :goto_5

    :cond_8
    move v2, v5

    .line 712
    goto/16 :goto_1

    :sswitch_1
    move v1, v6

    .line 714
    goto/16 :goto_2

    :sswitch_2
    move v1, v7

    goto/16 :goto_2

    :sswitch_3
    const/4 v1, 0x4

    goto/16 :goto_2

    .line 717
    :cond_9
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    goto/16 :goto_3

    .line 678
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
    .end packed-switch

    .line 714
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_1
        0x2711 -> :sswitch_2
        0x2713 -> :sswitch_3
        0x2714 -> :sswitch_2
        0x2715 -> :sswitch_3
        0x2716 -> :sswitch_3
        0x2717 -> :sswitch_1
    .end sparse-switch
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 408
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 409
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 410
    const-string v0, "buyFlowConfig"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 411
    const-string v0, "analyticsSessionId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->C:Ljava/lang/String;

    .line 412
    const-string v0, "cardEntryContext"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->w:I

    .line 414
    const-string v0, "disallowedCreditCardTypes"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->x:[I

    .line 415
    const-string v0, "disallowedCardCategories"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->y:[I

    .line 416
    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    .line 418
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->l()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v0, "onlinewallet"

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 420
    const-string v0, "account"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 421
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->B:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/wallet/common/ui/dh;->a(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;)Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->r:Lcom/google/android/gms/wallet/common/ui/dh;

    .line 426
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->r:Lcom/google/android/gms/wallet/common/ui/dh;

    sget-object v2, Lcom/google/android/gms/wallet/common/ui/an;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 430
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 724
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->k:Landroid/view/View;

    if-ne p1, v0, :cond_1

    .line 726
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->A:Landroid/content/Intent;

    const/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/q;->startActivityForResult(Landroid/content/Intent;I)V

    .line 727
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->C:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/analytics/events/OcrActivityLaunchedEvent;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    .line 731
    :cond_0
    :goto_0
    return-void

    .line 728
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->l:Landroid/view/View;

    if-ne p1, v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 729
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/an;->d(Z)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 434
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 436
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/util/e;->d(Landroid/app/Activity;)V

    .line 438
    new-instance v1, Lcom/google/android/gms/ocr/a;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/ocr/a;-><init>(Landroid/content/Context;)V

    iget-object v2, v1, Lcom/google/android/gms/ocr/a;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/gms/ocr/a;->b:Landroid/content/Intent;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/ay;->a(Landroid/content/pm/PackageManager;Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/google/android/gms/ocr/a;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/common/ew;->a(Landroid/content/Context;)I

    move-result v2

    if-eqz v2, :cond_1

    const-string v1, "CreditCardOcrIntentBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Google Play services is unavailable. Result="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->A:Landroid/content/Intent;

    .line 440
    if-eqz p1, :cond_0

    .line 441
    const-string v0, "enabled"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->q:Z

    .line 442
    const-string v0, "ocrResult"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ocr/CreditCardOcrResult;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    .line 443
    const-string v0, "serviceConnectionSavePoint"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->b:I

    .line 446
    const-string v0, "viewState"

    const/4 v1, 0x3

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    .line 448
    :cond_0
    return-void

    .line 438
    :cond_1
    iget-object v0, v1, Lcom/google/android/gms/ocr/a;->b:Landroid/content/Intent;

    goto :goto_0

    :cond_2
    const-string v1, "CreditCardOcrIntentBuilder"

    const-string v2, "Google Play services OCR activity is disabled or not available"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v7, 0x8

    .line 453
    sget v0, Lcom/google/android/gms/l;->gy:I

    const/4 v3, 0x0

    invoke-virtual {p1, v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 455
    sget v0, Lcom/google/android/gms/j;->dR:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->c:Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;

    .line 457
    sget v0, Lcom/google/android/gms/j;->cC:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->e:Landroid/widget/TextView;

    .line 458
    sget v0, Lcom/google/android/gms/j;->cB:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    .line 459
    sget v0, Lcom/google/android/gms/j;->fM:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 460
    sget v0, Lcom/google/android/gms/j;->fL:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->g:Landroid/widget/TextView;

    .line 461
    sget v0, Lcom/google/android/gms/j;->fN:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 462
    sget v0, Lcom/google/android/gms/j;->dT:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    .line 463
    sget v0, Lcom/google/android/gms/j;->gk:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->G:Landroid/view/View;

    .line 465
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v4, Lcom/google/android/gms/j;->dZ:I

    invoke-virtual {v0, v4}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ax;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->j:Lcom/google/android/gms/wallet/common/ui/ax;

    .line 467
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->j:Lcom/google/android/gms/wallet/common/ui/ax;

    if-nez v0, :cond_0

    .line 468
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/ax;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->j:Lcom/google/android/gms/wallet/common/ui/ax;

    .line 469
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    sget v4, Lcom/google/android/gms/j;->dZ:I

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/an;->j:Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-virtual {v0, v4, v5}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 474
    :cond_0
    sget v0, Lcom/google/android/gms/j;->mJ:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->k:Landroid/view/View;

    .line 475
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 476
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->k:Landroid/view/View;

    sget v4, Lcom/google/android/gms/j;->mK:I

    invoke-virtual {v0, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 477
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 480
    :cond_1
    sget v0, Lcom/google/android/gms/j;->fO:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->l:Landroid/view/View;

    .line 481
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->l:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 483
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/aw;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v6, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-direct {v0, v4, v5, v6}, Lcom/google/android/gms/wallet/common/ui/aw;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->o:Lcom/google/android/gms/wallet/common/ui/aw;

    .line 484
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/bg;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v6, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v0, v4, v5, v6}, Lcom/google/android/gms/wallet/common/ui/bg;-><init>(Landroid/content/Context;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;)V

    .line 486
    new-instance v4, Lcom/google/android/gms/wallet/common/ui/bh;

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v6, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v4, v5, v6, v0}, Lcom/google/android/gms/wallet/common/ui/bh;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/dn;)V

    iput-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->m:Lcom/google/android/gms/wallet/common/ui/bh;

    .line 487
    new-instance v4, Lcom/google/android/gms/wallet/common/ui/bi;

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-direct {v4, v5, v0}, Lcom/google/android/gms/wallet/common/ui/bi;-><init>(Lcom/google/android/gms/wallet/common/ui/FormEditText;Lcom/google/android/gms/wallet/common/ui/dn;)V

    iput-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->n:Lcom/google/android/gms/wallet/common/ui/bi;

    .line 488
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/ai;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-direct {v0, v4}, Lcom/google/android/gms/wallet/common/ui/ai;-><init>(Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->p:Lcom/google/android/gms/wallet/common/ui/ai;

    .line 490
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->o:Lcom/google/android/gms/wallet/common/ui/aw;

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/an;->o:Lcom/google/android/gms/wallet/common/ui/aw;

    invoke-virtual {v0, v4, v5, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V

    .line 491
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->m:Lcom/google/android/gms/wallet/common/ui/bh;

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/an;->m:Lcom/google/android/gms/wallet/common/ui/bh;

    invoke-virtual {v0, v4, v5, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V

    .line 492
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->n:Lcom/google/android/gms/wallet/common/ui/bi;

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/an;->n:Lcom/google/android/gms/wallet/common/ui/bi;

    invoke-virtual {v0, v4, v5, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V

    .line 493
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->p:Lcom/google/android/gms/wallet/common/ui/ai;

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0, v4, v5, v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lcom/google/android/gms/wallet/common/ui/am;Lcom/google/android/gms/wallet/common/ui/dn;Z)V

    .line 495
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->o:Lcom/google/android/gms/wallet/common/ui/aw;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/dn;)V

    .line 496
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->m:Lcom/google/android/gms/wallet/common/ui/bh;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/dn;)V

    .line 497
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->n:Lcom/google/android/gms/wallet/common/ui/bi;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/dn;)V

    .line 499
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/an;->m:Lcom/google/android/gms/wallet/common/ui/bh;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 501
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->getHint()Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->z:Ljava/lang/CharSequence;

    .line 503
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    if-nez v0, :cond_3

    .line 504
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->A:Landroid/content/Intent;

    if-eqz v0, :cond_2

    .line 505
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->k:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 514
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->k:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->C:Ljava/lang/String;

    invoke-static {v4, v0, v1}, Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;->a(Landroid/content/Context;ZLjava/lang/String;)Ljava/lang/String;

    .line 517
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    packed-switch v0, :pswitch_data_0

    .line 564
    :goto_2
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/ap;

    invoke-direct {v1, p0}, Lcom/google/android/gms/wallet/common/ui/ap;-><init>(Lcom/google/android/gms/wallet/common/ui/an;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/ah;)V

    .line 584
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    sget v1, Lcom/google/android/gms/j;->fM:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setNextFocusDownId(I)V

    .line 585
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v1, Lcom/google/android/gms/j;->fN:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusDownId(I)V

    .line 586
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v1, Lcom/google/android/gms/j;->dT:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusDownId(I)V

    .line 589
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v1, Lcom/google/android/gms/j;->cB:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusUpId(I)V

    .line 590
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v1, Lcom/google/android/gms/j;->fM:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusUpId(I)V

    .line 591
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    sget v1, Lcom/google/android/gms/j;->fN:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setNextFocusUpId(I)V

    .line 593
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->c:Lcom/google/android/gms/wallet/common/ui/CreditCardImagesView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lcom/google/android/gms/wallet/common/ui/ak;)V

    .line 594
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->H:Landroid/text/TextWatcher;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 595
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->b()V

    .line 596
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->x:[I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a([I)V

    .line 598
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->a(Lcom/google/android/gms/wallet/common/ui/ah;)V

    .line 599
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->d()V

    .line 601
    return-object v3

    .line 507
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->k:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    .line 508
    iput v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    goto :goto_0

    .line 511
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->k:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v0, v2

    .line 514
    goto :goto_1

    .line 522
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->G:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 523
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->G:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 525
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->Al:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 527
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 531
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->f:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 532
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 533
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->h:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 534
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    .line 535
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->j:Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/wallet/common/ui/ax;->b(I)V

    .line 536
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    new-instance v1, Lcom/google/android/gms/wallet/common/ui/ao;

    invoke-direct {v1, p0}, Lcom/google/android/gms/wallet/common/ui/ao;-><init>(Lcom/google/android/gms/wallet/common/ui/an;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->a(Lcom/google/android/gms/wallet/common/ui/ah;)V

    goto/16 :goto_2

    .line 551
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 552
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->j:Lcom/google/android/gms/wallet/common/ui/ax;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/wallet/common/ui/ax;->b(I)V

    .line 553
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->i:Lcom/google/android/gms/wallet/common/ui/FormEditText;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/wallet/common/ui/FormEditText;->setVisibility(I)V

    goto/16 :goto_2

    .line 556
    :pswitch_4
    invoke-direct {p0, v2}, Lcom/google/android/gms/wallet/common/ui/an;->c(Z)V

    goto/16 :goto_2

    .line 559
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setVisibility(I)V

    .line 560
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .line 517
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final onFocusChange(Landroid/view/View;Z)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 736
    if-nez p2, :cond_1

    .line 749
    :cond_0
    :goto_0
    return-void

    .line 740
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->G:Landroid/view/View;

    if-ne p1, v0, :cond_2

    .line 741
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/an;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->G:Landroid/view/View;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/ae;->b(Landroid/content/Context;Landroid/view/View;)Z

    goto :goto_0

    .line 742
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    if-ne p1, v0, :cond_0

    .line 743
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->G:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 744
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->z:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setHint(Ljava/lang/CharSequence;)V

    .line 745
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->k:Landroid/view/View;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/ui/dt;->b(Landroid/view/View;I)V

    .line 746
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    .line 747
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->d:Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/CardNumberEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto :goto_0
.end method

.method public final onPause()V
    .locals 0

    .prologue
    .line 656
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 657
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->i()V

    .line 658
    return-void
.end method

.method public final onResume()V
    .locals 3

    .prologue
    .line 649
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 650
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->l()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->l()Lcom/google/android/gms/wallet/common/ui/dh;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/dh;->a:Lcom/google/android/gms/wallet/service/k;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->I:Lcom/google/android/gms/wallet/service/l;

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/an;->b:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/service/k;->b(Lcom/google/android/gms/wallet/service/l;I)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->b:I

    .line 651
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->k()V

    .line 652
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 662
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 664
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/ui/an;->i()V

    .line 665
    const-string v0, "serviceConnectionSavePoint"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 666
    const-string v0, "enabled"

    iget-boolean v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->q:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 667
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    if-eqz v0, :cond_0

    .line 668
    const-string v0, "ocrResult"

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->D:Lcom/google/android/gms/ocr/CreditCardOcrResult;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 670
    :cond_0
    const-string v0, "viewState"

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/an;->E:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 671
    return-void
.end method
