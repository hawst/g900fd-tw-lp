.class public final Lcom/google/android/gms/wallet/service/e;
.super Lcom/google/android/gms/wallet/service/b;
.source "SourceFile"


# instance fields
.field j:Lcom/google/android/gms/wallet/service/ia/l;

.field k:Lcom/google/android/gms/wallet/service/ow/v;

.field final l:I

.field m:Landroid/os/Handler;

.field final n:Ljava/util/concurrent/CountDownLatch;

.field private final o:Landroid/content/ServiceConnection;

.field private final p:Landroid/content/ServiceConnection;

.field private q:Z

.field private final r:Ljava/lang/Thread;


# direct methods
.method public constructor <init>(ILcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 128
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/gms/wallet/service/b;-><init>(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/accounts/Account;Landroid/content/Context;)V

    .line 129
    iput p1, p0, Lcom/google/android/gms/wallet/service/e;->l:I

    .line 131
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/e;->n:Ljava/util/concurrent/CountDownLatch;

    .line 134
    new-instance v0, Lcom/google/android/gms/wallet/service/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/service/f;-><init>(Lcom/google/android/gms/wallet/service/e;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/e;->o:Landroid/content/ServiceConnection;

    .line 159
    new-instance v0, Lcom/google/android/gms/wallet/service/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/service/g;-><init>(Lcom/google/android/gms/wallet/service/e;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/e;->p:Landroid/content/ServiceConnection;

    .line 184
    new-instance v0, Lcom/google/android/gms/wallet/service/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/wallet/service/h;-><init>(Lcom/google/android/gms/wallet/service/e;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/e;->r:Ljava/lang/Thread;

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/e;->r:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 248
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/e;->n:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 249
    const-string v0, "NetworkPaymentServiceConnection"

    const-string v1, "Service thread initialization complete"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :goto_0
    return-void

    .line 251
    :catch_0
    move-exception v0

    const-string v0, "NetworkPaymentServiceConnection"

    const-string v1, "Unable to initialize PaymentService background thread."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private c()Z
    .locals 1

    .prologue
    .line 655
    iget v0, p0, Lcom/google/android/gms/wallet/service/e;->l:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 659
    iget v0, p0, Lcom/google/android/gms/wallet/service/e;->l:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 356
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/e;->q:Z

    if-nez v0, :cond_2

    .line 357
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wallet.service.ia.IIaService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 360
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->f:Landroid/content/Context;

    const-string v2, "NetworkPaymentServiceConnection"

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->o:Landroid/content/ServiceConnection;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 363
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 364
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.wallet.service.ow.IOwInternalService"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    .line 367
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->f:Landroid/content/Context;

    const-string v2, "NetworkPaymentServiceConnection"

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->p:Landroid/content/ServiceConnection;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 370
    :cond_1
    iput-boolean v5, p0, Lcom/google/android/gms/wallet/service/e;->q:Z

    .line 372
    :cond_2
    return-void
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/g;)V
    .locals 3

    .prologue
    .line 632
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->d()Z

    move-result v0

    const-string v1, "Must specify connection to OwIntService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 634
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 635
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;-><init>(Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/g;)V

    .line 637
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/16 v2, 0x12

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 639
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 641
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/i;Z)V
    .locals 3

    .prologue
    .line 523
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->d()Z

    move-result v0

    const-string v1, "Must specify connection to OwIntService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 525
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 526
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->c()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;-><init>(Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/i;Ljava/lang/String;)V

    .line 529
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/16 v2, 0xc

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 532
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 534
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/k;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 476
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->d()Z

    move-result v0

    const-string v1, "Must specify connection to OwIntService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 478
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 479
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;-><init>(Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/k;Lcom/google/android/gms/wallet/Cart;Ljava/lang/String;)V

    .line 482
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/16 v2, 0x8

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 485
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 487
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/aa/b/a/a/a/a/o;Z)V
    .locals 3

    .prologue
    .line 461
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->d()Z

    move-result v0

    const-string v1, "Must specify connection to OwIntService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 463
    invoke-static {p2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 464
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;-><init>(Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/o;Z)V

    .line 466
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/4 v2, 0x7

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 468
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 470
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/aa;Z)V
    .locals 3

    .prologue
    .line 408
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 409
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 410
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;-><init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/aa;)V

    .line 412
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 414
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 416
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/ab;)V
    .locals 3

    .prologue
    .line 561
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 562
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 563
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;-><init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/ab;)V

    .line 564
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 566
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 568
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/ac;)V
    .locals 3

    .prologue
    .line 583
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 584
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 585
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;-><init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/ac;)V

    .line 586
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/16 v2, 0xe

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 588
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 590
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/af;)V
    .locals 3

    .prologue
    .line 572
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 573
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 574
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;-><init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/af;)V

    .line 575
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/16 v2, 0xd

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 577
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 579
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/ah;)V
    .locals 3

    .prologue
    .line 645
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 646
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 647
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;-><init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/ah;)V

    .line 648
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/16 v2, 0x13

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 650
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 652
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/ak;)V
    .locals 3

    .prologue
    .line 538
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 539
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 540
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;-><init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/ak;)V

    .line 542
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 544
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 546
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/am;)V
    .locals 3

    .prologue
    .line 550
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 551
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 552
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;-><init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/am;)V

    .line 553
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 555
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 557
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/ao;Z)V
    .locals 3

    .prologue
    .line 449
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 450
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 451
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;-><init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/ao;)V

    .line 452
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/4 v2, 0x6

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 454
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 456
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/ap;Z)V
    .locals 3

    .prologue
    .line 422
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 423
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 424
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;-><init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/ap;)V

    .line 426
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/4 v2, 0x4

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 428
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 430
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/s;)V
    .locals 3

    .prologue
    .line 595
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 596
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 597
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;-><init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/s;)V

    .line 599
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/16 v2, 0xf

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 601
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 603
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/u;)V
    .locals 3

    .prologue
    .line 620
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 621
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 622
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;-><init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/u;)V

    .line 624
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/16 v2, 0x11

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 626
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 628
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/w;)V
    .locals 3

    .prologue
    .line 608
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 609
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 610
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;-><init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/w;)V

    .line 612
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/16 v2, 0x10

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 614
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 616
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/z;Z)V
    .locals 3

    .prologue
    .line 436
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    const-string v1, "Must specify connection to IaService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 437
    invoke-virtual {p0, p1}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 438
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;-><init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/z;)V

    .line 439
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/4 v2, 0x5

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 441
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 443
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 5

    .prologue
    .line 505
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->d()Z

    move-result v0

    const-string v1, "Must specify connection to OwIntService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 507
    new-instance v0, Lcom/google/aa/b/a/a/a/a/e;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/e;-><init>()V

    .line 508
    iput p2, v0, Lcom/google/aa/b/a/a/a/a/e;->b:I

    .line 509
    new-instance v1, Lcom/google/aa/b/a/a/a/a/q;

    invoke-direct {v1}, Lcom/google/aa/b/a/a/a/a/q;-><init>()V

    iput-object v1, v0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    .line 510
    iget-object v1, v0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    iput-object p1, v1, Lcom/google/aa/b/a/a/a/a/q;->a:Ljava/lang/String;

    .line 511
    iget-object v1, v0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    invoke-static {}, Lcom/google/android/gms/wallet/service/ow/ao;->a()Lcom/google/aa/b/a/a/a/a/t;

    move-result-object v2

    iput-object v2, v1, Lcom/google/aa/b/a/a/a/a/q;->b:Lcom/google/aa/b/a/a/a/a/t;

    .line 512
    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 513
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/16 v2, 0xb

    new-instance v3, Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;-><init>(Landroid/accounts/Account;Lcom/google/aa/b/a/a/a/a/e;)V

    invoke-static {v1, v2, v3}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 516
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 518
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/aa/b/a/a/a/a/c;)V
    .locals 3

    .prologue
    .line 492
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->d()Z

    move-result v0

    const-string v1, "Must specify connection to OwIntService!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 494
    invoke-virtual {p0, p2}, Lcom/google/android/gms/wallet/service/e;->a(Lcom/google/protobuf/nano/j;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 495
    new-instance v0, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;

    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->b:Landroid/accounts/Account;

    invoke-direct {v0, v1, p1, p2}, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;-><init>(Landroid/accounts/Account;Ljava/lang/String;Lcom/google/aa/b/a/a/a/a/c;)V

    .line 497
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const/16 v2, 0xa

    invoke-static {v1, v2, v0}, Landroid/os/Message;->obtain(Landroid/os/Handler;ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 499
    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 501
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 396
    const-string v0, "NetworkPaymentServiceConnection"

    const-string v2, "destroy"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 397
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/service/e;->q:Z

    if-eqz v0, :cond_1

    const-string v0, "NetworkPaymentServiceConnection"

    const-string v2, "disconnect"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/e;->f:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/wallet/service/e;->o:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/e;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/wallet/service/e;->f:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->p:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    or-int/lit8 v0, v0, 0x2

    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    const v3, 0x8000

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessageAtFrontOfQueue(Landroid/os/Message;)Z

    iput-boolean v1, p0, Lcom/google/android/gms/wallet/service/e;->q:Z

    .line 400
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/e;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 401
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/e;->m:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 402
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected final c(ILjava/lang/Object;)Landroid/util/Pair;
    .locals 6

    .prologue
    .line 258
    const-wide/16 v2, 0x0

    .line 259
    const/4 v0, 0x0

    .line 263
    packed-switch p1, :pswitch_data_0

    .line 345
    :pswitch_0
    :try_start_0
    const-string v1, "NetworkPaymentServiceConnection"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown message type "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " passed to handler."

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 351
    :cond_0
    :goto_0
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 265
    :pswitch_1
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ia/l;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    .line 269
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ia/l;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateInstrumentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    .line 273
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ia/l;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    .line 277
    :pswitch_4
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ia/l;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/UpdateAddressRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    .line 281
    :pswitch_5
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->k:Lcom/google/android/gms/wallet/service/ow/v;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ow/v;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetMaskedWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    .line 285
    :pswitch_6
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->k:Lcom/google/android/gms/wallet/service/ow/v;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ow/v;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetWalletItemsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    .line 289
    :pswitch_7
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->k:Lcom/google/android/gms/wallet/service/ow/v;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ow/v;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetFullWalletForBuyerSelectionServiceRequest;)Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;

    move-result-object v1

    .line 292
    if-eqz v1, :cond_0

    .line 293
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->a()J

    move-result-wide v2

    .line 294
    invoke-virtual {v1}, Lcom/google/android/gms/wallet/service/ow/FullWalletResponse;->b()Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    .line 299
    :pswitch_8
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->k:Lcom/google/android/gms/wallet/service/ow/v;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ow/v;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/AuthenticateInstrumentServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    .line 303
    :pswitch_9
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->k:Lcom/google/android/gms/wallet/service/ow/v;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ow/v;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/GetBinDerivedDataServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    .line 307
    :pswitch_a
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ia/l;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto :goto_0

    .line 311
    :pswitch_b
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ia/l;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/PurchaseRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    .line 314
    :pswitch_c
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ia/l;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/CreateProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    .line 318
    :pswitch_d
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ia/l;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetLegalDocumentsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    .line 322
    :pswitch_e
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ia/l;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    .line 326
    :pswitch_f
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ia/l;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingGetPaymentOptionsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    .line 330
    :pswitch_10
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ia/l;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingUpdatePaymentSettingsRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    .line 334
    :pswitch_11
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ia/l;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/BillingMakePaymentRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    .line 338
    :pswitch_12
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->k:Lcom/google/android/gms/wallet/service/ow/v;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ow/v;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ow/CreateWalletObjectsServiceRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;

    move-result-object v0

    goto/16 :goto_0

    .line 342
    :pswitch_13
    iget-object v1, p0, Lcom/google/android/gms/wallet/service/e;->j:Lcom/google/android/gms/wallet/service/ia/l;

    iget-object v4, p0, Lcom/google/android/gms/wallet/service/e;->a:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    check-cast p2, Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;

    invoke-interface {v1, v4, p2}, Lcom/google/android/gms/wallet/service/ia/l;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Lcom/google/android/gms/wallet/service/ia/GetProfileRequest;)Lcom/google/android/gms/wallet/shared/service/ServerResponse;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto/16 :goto_0

    .line 348
    :catch_0
    move-exception v1

    .line 349
    const-string v4, "NetworkPaymentServiceConnection"

    const-string v5, "Failed to contact PaymentService:"

    invoke-static {v4, v5, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 263
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
    .end packed-switch
.end method
