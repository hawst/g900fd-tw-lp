.class public final Lcom/google/android/gms/ads/internal/h/a/a;
.super Lcom/google/android/gms/ads/internal/h/f;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:Lcom/google/android/gms/ads/internal/h/a/a;


# instance fields
.field private final c:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 17
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/ads/internal/h/a/a;->a:Ljava/lang/Object;

    return-void
.end method

.method private constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/h/f;-><init>()V

    .line 32
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/h/a/a;->c:Landroid/os/Bundle;

    .line 33
    return-void
.end method

.method public static a(Landroid/os/Bundle;)Lcom/google/android/gms/ads/internal/h/a/a;
    .locals 2

    .prologue
    .line 23
    sget-object v1, Lcom/google/android/gms/ads/internal/h/a/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 24
    :try_start_0
    sget-object v0, Lcom/google/android/gms/ads/internal/h/a/a;->b:Lcom/google/android/gms/ads/internal/h/a/a;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/google/android/gms/ads/internal/h/a/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/h/a/a;-><init>(Landroid/os/Bundle;)V

    sput-object v0, Lcom/google/android/gms/ads/internal/h/a/a;->b:Lcom/google/android/gms/ads/internal/h/a/a;

    .line 27
    :cond_0
    sget-object v0, Lcom/google/android/gms/ads/internal/h/a/a;->b:Lcom/google/android/gms/ads/internal/h/a/a;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 28
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/h/a/a;->c:Landroid/os/Bundle;

    return-object v0
.end method
