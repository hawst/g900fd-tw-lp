.class public abstract Lcom/google/android/gms/drive/ui/picker/a/a/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/SectionIndexer;


# instance fields
.field protected final a:Lcom/google/android/gms/drive/ui/picker/a/l;

.field private final b:Z

.field private final c:I

.field private final d:Landroid/util/SparseIntArray;

.field private final e:[Lcom/google/android/gms/drive/ui/picker/a/a/p;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/ui/picker/a/l;[Lcom/google/android/gms/drive/ui/picker/a/a/p;Z)V
    .locals 2

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-boolean p3, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->b:Z

    .line 81
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    .line 82
    array-length v0, p2

    iput v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->c:I

    .line 83
    iput-object p2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->e:[Lcom/google/android/gms/drive/ui/picker/a/a/p;

    .line 84
    new-instance v0, Landroid/util/SparseIntArray;

    iget v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->c:I

    invoke-direct {v0, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->d:Landroid/util/SparseIntArray;

    .line 85
    return-void
.end method

.method private a(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 2

    .prologue
    .line 111
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/a/a/q;->a()Ljava/util/Comparator;

    move-result-object v0

    .line 112
    invoke-interface {v0, p1, p2}, Ljava/util/Comparator;->compare(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v0

    .line 113
    iget-boolean v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->b:Z

    if-eqz v1, :cond_0

    neg-int v0, v0

    :cond_0
    return v0
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/gms/drive/ui/picker/a/l;)Ljava/lang/Object;
.end method

.method protected abstract a()Ljava/util/Comparator;
.end method

.method public getPositionForSection(I)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/high16 v8, -0x80000000

    .line 128
    iget-object v4, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->d:Landroid/util/SparseIntArray;

    .line 129
    iget-object v5, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    .line 131
    if-eqz v5, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->e:[Lcom/google/android/gms/drive/ui/picker/a/a/p;

    if-nez v1, :cond_1

    .line 216
    :cond_0
    :goto_0
    return v0

    .line 136
    :cond_1
    if-lez p1, :cond_0

    .line 139
    iget v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->c:I

    if-lt p1, v1, :cond_2

    .line 140
    iget v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->c:I

    add-int/lit8 p1, v1, -0x1

    .line 143
    :cond_2
    invoke-interface {v5}, Lcom/google/android/gms/drive/ui/picker/a/l;->j()I

    move-result v6

    .line 145
    invoke-interface {v5}, Lcom/google/android/gms/drive/ui/picker/a/l;->h()I

    move-result v3

    .line 150
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->e:[Lcom/google/android/gms/drive/ui/picker/a/a/p;

    aget-object v1, v1, p1

    invoke-interface {v1}, Lcom/google/android/gms/drive/ui/picker/a/a/p;->a()Ljava/lang/Object;

    move-result-object v7

    .line 153
    invoke-virtual {v4, p1, v8}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    if-eq v8, v1, :cond_a

    .line 157
    if-gez v1, :cond_4

    .line 158
    neg-int v2, v1

    .line 167
    :goto_1
    if-lez p1, :cond_3

    .line 168
    add-int/lit8 v1, p1, -0x1

    .line 169
    invoke-virtual {v4, v1, v8}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    .line 170
    if-eq v1, v8, :cond_3

    .line 171
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v0

    .line 177
    :cond_3
    add-int v1, v2, v0

    div-int/lit8 v1, v1, 0x2

    move v9, v2

    move v2, v0

    move v0, v9

    .line 179
    :goto_2
    if-ge v1, v0, :cond_6

    .line 181
    invoke-interface {v5, v1}, Lcom/google/android/gms/drive/ui/picker/a/l;->a(I)Z

    .line 182
    invoke-virtual {p0, v5}, Lcom/google/android/gms/drive/ui/picker/a/a/q;->a(Lcom/google/android/gms/drive/ui/picker/a/l;)Ljava/lang/Object;

    move-result-object v8

    .line 183
    if-nez v8, :cond_5

    .line 184
    if-eqz v1, :cond_6

    .line 185
    add-int/lit8 v1, v1, -0x1

    .line 188
    goto :goto_2

    :cond_4
    move v0, v1

    .line 162
    goto :goto_0

    .line 191
    :cond_5
    invoke-direct {p0, v8, v7}, Lcom/google/android/gms/drive/ui/picker/a/a/q;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v8

    .line 192
    if-eqz v8, :cond_9

    .line 193
    if-gez v8, :cond_7

    .line 194
    add-int/lit8 v1, v1, 0x1

    .line 195
    if-lt v1, v3, :cond_8

    move v1, v3

    .line 214
    :cond_6
    invoke-virtual {v4, p1, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 215
    invoke-interface {v5, v6}, Lcom/google/android/gms/drive/ui/picker/a/l;->a(I)Z

    move v0, v1

    .line 216
    goto :goto_0

    :cond_7
    move v0, v1

    move v1, v2

    .line 212
    :cond_8
    :goto_3
    add-int v2, v1, v0

    div-int/lit8 v2, v2, 0x2

    move v9, v2

    move v2, v1

    move v1, v9

    .line 213
    goto :goto_2

    .line 204
    :cond_9
    if-eq v2, v1, :cond_6

    move v0, v1

    move v1, v2

    .line 206
    goto :goto_3

    :cond_a
    move v2, v3

    goto :goto_1
.end method

.method public getSectionForPosition(I)I
    .locals 3

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/l;->j()I

    move-result v0

    .line 226
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v1, p1}, Lcom/google/android/gms/drive/ui/picker/a/l;->a(I)Z

    .line 227
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/drive/ui/picker/a/a/q;->a(Lcom/google/android/gms/drive/ui/picker/a/l;)Ljava/lang/Object;

    move-result-object v1

    .line 228
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->a:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v2, v0}, Lcom/google/android/gms/drive/ui/picker/a/l;->a(I)Z

    .line 231
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->c:I

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_1

    .line 232
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->e:[Lcom/google/android/gms/drive/ui/picker/a/a/p;

    aget-object v2, v2, v0

    invoke-interface {v2}, Lcom/google/android/gms/drive/ui/picker/a/a/p;->a()Ljava/lang/Object;

    move-result-object v2

    .line 233
    invoke-direct {p0, v2, v1}, Lcom/google/android/gms/drive/ui/picker/a/a/q;->a(Ljava/lang/Object;Ljava/lang/Object;)I

    move-result v2

    if-gtz v2, :cond_0

    .line 237
    :goto_1
    return v0

    .line 231
    :cond_0
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 237
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a/q;->e:[Lcom/google/android/gms/drive/ui/picker/a/a/p;

    return-object v0
.end method
