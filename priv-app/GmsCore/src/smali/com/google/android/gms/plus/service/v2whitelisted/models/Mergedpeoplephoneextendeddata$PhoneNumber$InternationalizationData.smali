.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/k;

.field private static final i:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:I

.field d:Ljava/lang/String;

.field e:Z

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 278
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/k;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/k;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/k;

    .line 295
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 298
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->i:Ljava/util/HashMap;

    const-string v1, "countryCode"

    const-string v2, "countryCode"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 299
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->i:Ljava/util/HashMap;

    const-string v1, "internationalNumber"

    const-string v2, "internationalNumber"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 300
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->i:Ljava/util/HashMap;

    const-string v1, "isValid"

    const-string v2, "isValid"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 301
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->i:Ljava/util/HashMap;

    const-string v1, "nationalNumber"

    const-string v2, "nationalNumber"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 302
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->i:Ljava/util/HashMap;

    const-string v1, "regionCode"

    const-string v2, "regionCode"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 303
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->i:Ljava/util/HashMap;

    const-string v1, "validationResult"

    const-string v2, "validationResult"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 304
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 341
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 342
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->b:I

    .line 343
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->a:Ljava/util/Set;

    .line 344
    return-void
.end method

.method constructor <init>(Ljava/util/Set;IILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 356
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 357
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->a:Ljava/util/Set;

    .line 358
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->b:I

    .line 359
    iput p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->c:I

    .line 360
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->d:Ljava/lang/String;

    .line 361
    iput-boolean p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->e:Z

    .line 362
    iput-object p6, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->f:Ljava/lang/String;

    .line 363
    iput-object p7, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->g:Ljava/lang/String;

    .line 364
    iput-object p8, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->h:Ljava/lang/String;

    .line 365
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 308
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->i:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 513
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 514
    packed-switch v0, :pswitch_data_0

    .line 519
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be an int."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 516
    :pswitch_0
    iput p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->c:I

    .line 522
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 523
    return-void

    .line 514
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 543
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 544
    packed-switch v0, :pswitch_data_0

    .line 558
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 546
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->d:Ljava/lang/String;

    .line 561
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 562
    return-void

    .line 549
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->f:Ljava/lang/String;

    goto :goto_0

    .line 552
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->g:Ljava/lang/String;

    goto :goto_0

    .line 555
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->h:Ljava/lang/String;

    goto :goto_0

    .line 544
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 528
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 529
    packed-switch v0, :pswitch_data_0

    .line 534
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a boolean."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 531
    :pswitch_0
    iput-boolean p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->e:Z

    .line 537
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 538
    return-void

    .line 529
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 479
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 493
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 481
    :pswitch_0
    iget v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 491
    :goto_0
    return-object v0

    .line 483
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->d:Ljava/lang/String;

    goto :goto_0

    .line 485
    :pswitch_2
    iget-boolean v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 487
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->f:Ljava/lang/String;

    goto :goto_0

    .line 489
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->g:Ljava/lang/String;

    goto :goto_0

    .line 491
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->h:Ljava/lang/String;

    goto :goto_0

    .line 479
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 464
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/k;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 579
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;

    if-nez v0, :cond_0

    move v0, v1

    .line 610
    :goto_0
    return v0

    .line 584
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 585
    goto :goto_0

    .line 588
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;

    .line 589
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 590
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 591
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 593
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 595
    goto :goto_0

    :cond_3
    move v0, v1

    .line 600
    goto :goto_0

    .line 603
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 605
    goto :goto_0

    :cond_5
    move v0, v2

    .line 610
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 566
    const/4 v0, 0x0

    .line 567
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->i:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 568
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 569
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 570
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 572
    goto :goto_0

    .line 573
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 469
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/k;

    invoke-static {p0, p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/k;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplephoneextendeddata$PhoneNumber$InternationalizationData;Landroid/os/Parcel;)V

    .line 470
    return-void
.end method
