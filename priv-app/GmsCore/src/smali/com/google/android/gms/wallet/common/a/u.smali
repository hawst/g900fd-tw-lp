.class public final Lcom/google/android/gms/wallet/common/a/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/wallet/common/a/v;


# instance fields
.field private final a:Ljava/util/Collection;

.field private b:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Ljava/util/Collection;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 35
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/a/u;->a:Ljava/util/Collection;

    .line 36
    return-void
.end method

.method private declared-synchronized c()V
    .locals 4

    .prologue
    .line 48
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/u;->b:Ljava/util/HashMap;

    if-nez v0, :cond_2

    .line 49
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/a/u;->b:Ljava/util/HashMap;

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/u;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 51
    iget-object v1, v0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 52
    iget-object v1, v0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    .line 55
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 56
    const/4 v3, 0x0

    invoke-static {v1, v3}, Lcom/google/android/gms/wallet/shared/common/a/a;->a(Ljava/lang/CharSequence;Z)Ljava/lang/String;

    move-result-object v3

    .line 59
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/a/u;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 61
    iget-object v1, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v1, v1, Lcom/google/t/a/b;->s:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 66
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v0, v0, Lcom/google/t/a/b;->s:Ljava/lang/String;

    .line 71
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/a/u;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 69
    :cond_1
    :try_start_1
    const-string v0, ""
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 74
    :cond_2
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    const-string v0, "LocalPhoneNumberSource"

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)Ljava/util/List;
    .locals 8

    .prologue
    .line 78
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const/4 v0, 0x0

    .line 121
    :goto_0
    return-object v0

    .line 82
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/a/u;->c()V

    .line 84
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 85
    invoke-static {p1}, Lcom/google/android/gms/wallet/shared/common/a/a;->a(Ljava/lang/CharSequence;)Ljava/util/regex/Pattern;

    move-result-object v5

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/u;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 88
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 89
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/util/au;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 90
    const/4 v0, 0x0

    .line 91
    if-eqz v5, :cond_2

    if-eqz v2, :cond_2

    .line 92
    invoke-virtual {v5, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    .line 93
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_2

    .line 94
    const/4 v0, 0x2

    .line 97
    :cond_2
    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4, v1}, Lcom/google/android/gms/wallet/shared/common/a/a;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 98
    or-int/lit8 v0, v0, 0x1

    move v4, v0

    .line 100
    :goto_2
    if-eqz v4, :cond_1

    .line 101
    invoke-static {v1}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 103
    and-int/lit8 v1, v4, 0x1

    if-eqz v1, :cond_3

    .line 104
    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/shared/common/a/a;->b(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/text/SpannableString;

    move-result-object v0

    move-object v1, v0

    .line 110
    :goto_3
    and-int/lit8 v0, v4, 0x2

    if-eqz v0, :cond_4

    .line 111
    invoke-static {v2, v5}, Lcom/google/android/gms/wallet/shared/common/a/a;->a(Ljava/lang/String;Ljava/util/regex/Pattern;)Landroid/text/SpannableString;

    move-result-object v0

    .line 116
    :goto_4
    new-instance v2, Lcom/google/android/gms/wallet/common/a/w;

    const-string v7, "LocalPhoneNumberSource"

    invoke-direct {v2, v1, v0, v4, v7}, Lcom/google/android/gms/wallet/common/a/w;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v1, v0

    .line 107
    goto :goto_3

    :cond_4
    move-object v0, v2

    .line 114
    goto :goto_4

    .line 120
    :cond_5
    sget-object v0, Lcom/google/android/gms/wallet/common/a/w;->a:Ljava/util/Comparator;

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    move-object v0, v3

    .line 121
    goto :goto_0

    :cond_6
    move v4, v0

    goto :goto_2
.end method

.method public final b()Lcom/google/android/gms/wallet/common/a/w;
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 126
    invoke-direct {p0}, Lcom/google/android/gms/wallet/common/a/u;->c()V

    .line 128
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/u;->a:Ljava/util/Collection;

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 130
    iget-object v5, v0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 131
    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    .line 134
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 135
    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/common/a/a;->a(Ljava/lang/CharSequence;Z)Ljava/lang/String;

    move-result-object v5

    .line 138
    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 139
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 140
    invoke-virtual {v2, v5, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    move v0, v1

    .line 139
    goto :goto_1

    .line 145
    :cond_2
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    move-object v3, v4

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 146
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 147
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 148
    if-nez v3, :cond_3

    .line 149
    if-le v0, v2, :cond_8

    move v2, v0

    move-object v3, v1

    .line 151
    goto :goto_2

    .line 153
    :cond_3
    if-le v0, v2, :cond_4

    move v2, v0

    move-object v3, v1

    .line 155
    goto :goto_2

    .line 156
    :cond_4
    if-ne v0, v2, :cond_8

    move-object v1, v4

    :goto_3
    move-object v3, v1

    .line 160
    goto :goto_2

    .line 161
    :cond_5
    if-eqz v3, :cond_6

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/u;->b:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 163
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 166
    :goto_4
    new-instance v0, Lcom/google/android/gms/wallet/common/a/w;

    invoke-static {v3}, Landroid/telephony/PhoneNumberUtils;->formatNumber(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x4

    const-string v3, "LocalPhoneNumberSource"

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/google/android/gms/wallet/common/a/w;-><init>(Ljava/lang/CharSequence;Ljava/lang/CharSequence;ILjava/lang/String;)V

    move-object v4, v0

    .line 169
    :cond_6
    return-object v4

    :cond_7
    move-object v4, v0

    goto :goto_4

    :cond_8
    move-object v1, v3

    goto :goto_3
.end method
