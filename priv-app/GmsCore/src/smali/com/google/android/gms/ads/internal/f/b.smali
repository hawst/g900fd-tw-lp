.class public final Lcom/google/android/gms/ads/internal/f/b;
.super Lcom/google/android/gms/ads/internal/f/a/e;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/f/d;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Landroid/graphics/drawable/Drawable;

.field private final c:Ljava/lang/String;

.field private final d:Landroid/graphics/drawable/Drawable;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/Object;

.field private h:Lcom/google/android/gms/ads/internal/f/c;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Landroid/graphics/drawable/Drawable;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/f/a/e;-><init>()V

    .line 64
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/f/b;->g:Ljava/lang/Object;

    .line 75
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/f/b;->a:Ljava/lang/String;

    .line 76
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/f/b;->b:Landroid/graphics/drawable/Drawable;

    .line 77
    iput-object p3, p0, Lcom/google/android/gms/ads/internal/f/b;->c:Ljava/lang/String;

    .line 78
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/f/b;->d:Landroid/graphics/drawable/Drawable;

    .line 79
    iput-object p5, p0, Lcom/google/android/gms/ads/internal/f/b;->e:Ljava/lang/String;

    .line 80
    iput-object p6, p0, Lcom/google/android/gms/ads/internal/f/b;->f:Ljava/lang/String;

    .line 81
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/b;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 115
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/f/b;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 116
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/b;->h:Lcom/google/android/gms/ads/internal/f/c;

    if-nez v0, :cond_0

    .line 117
    const-string v0, "Attempt to perform click before content ad initialized."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;)V

    .line 118
    monitor-exit v1

    .line 121
    :goto_0
    return-void

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/b;->h:Lcom/google/android/gms/ads/internal/f/c;

    const-string v2, "1"

    invoke-virtual {v0, v2, p1}, Lcom/google/android/gms/ads/internal/f/c;->a(Ljava/lang/String;I)V

    .line 121
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/f/c;)V
    .locals 2

    .prologue
    .line 137
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/f/b;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 138
    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/f/b;->h:Lcom/google/android/gms/ads/internal/f/c;

    .line 139
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Lcom/google/android/gms/b/l;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/b;->b:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/b;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/b/l;
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/b;->d:Landroid/graphics/drawable/Drawable;

    invoke-static {v0}, Lcom/google/android/gms/b/p;->a(Ljava/lang/Object;)Lcom/google/android/gms/b/l;

    move-result-object v0

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/b;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/b;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 126
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/f/b;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 127
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/b;->h:Lcom/google/android/gms/ads/internal/f/c;

    if-nez v0, :cond_0

    .line 128
    const-string v0, "Attempt to record impression before content ad initialized."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;)V

    .line 129
    monitor-exit v1

    .line 132
    :goto_0
    return-void

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/f/b;->h:Lcom/google/android/gms/ads/internal/f/c;

    iget-object v0, v0, Lcom/google/android/gms/ads/internal/f/c;->a:Lcom/google/android/gms/ads/internal/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/b;->n()V

    .line 132
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
