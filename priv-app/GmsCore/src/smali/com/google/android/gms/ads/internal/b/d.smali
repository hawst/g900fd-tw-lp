.class public final Lcom/google/android/gms/ads/internal/b/d;
.super Ljava/lang/Thread;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private final d:Ljava/lang/Object;

.field private final e:Lcom/google/android/gms/ads/internal/b/c;

.field private final f:Lcom/google/android/gms/ads/internal/b/b;

.field private final g:Lcom/google/android/gms/ads/internal/m/a;

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/b/c;Lcom/google/android/gms/ads/internal/b/b;Landroid/os/Bundle;Lcom/google/android/gms/ads/internal/m/a;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 48
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/b/d;->a:Z

    .line 49
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/b/d;->b:Z

    .line 50
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/b/d;->c:Z

    .line 65
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/b/d;->e:Lcom/google/android/gms/ads/internal/b/c;

    .line 66
    iput-object p2, p0, Lcom/google/android/gms/ads/internal/b/d;->f:Lcom/google/android/gms/ads/internal/b/b;

    .line 67
    iput-object p4, p0, Lcom/google/android/gms/ads/internal/b/d;->g:Lcom/google/android/gms/ads/internal/m/a;

    .line 68
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/b/d;->d:Ljava/lang/Object;

    .line 69
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->l:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/ads/internal/b/d;->i:I

    .line 70
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/ads/internal/b/d;->j:I

    .line 71
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->n:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/ads/internal/b/d;->k:I

    .line 72
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/ads/internal/b/d;->l:I

    .line 73
    sget-object v0, Lcom/google/android/gms/ads/internal/d/b;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/ads/internal/b/d;->h:I

    .line 74
    const-string v0, "ContentFetchTask"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/b/d;->setName(Ljava/lang/String;)V

    .line 75
    return-void
.end method

.method private a(Landroid/view/View;Lcom/google/android/gms/ads/internal/b/a;)Lcom/google/android/gms/ads/internal/b/h;
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 243
    if-nez p1, :cond_0

    .line 244
    new-instance v1, Lcom/google/android/gms/ads/internal/b/h;

    invoke-direct {v1, p0, v0, v0}, Lcom/google/android/gms/ads/internal/b/h;-><init>(Lcom/google/android/gms/ads/internal/b/d;II)V

    move-object v0, v1

    .line 274
    :goto_0
    return-object v0

    .line 247
    :cond_0
    instance-of v1, p1, Landroid/widget/TextView;

    if-eqz v1, :cond_1

    instance-of v1, p1, Landroid/widget/EditText;

    if-nez v1, :cond_1

    .line 248
    check-cast p1, Landroid/widget/TextView;

    .line 249
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    .line 250
    invoke-virtual {p2, v1}, Lcom/google/android/gms/ads/internal/b/a;->b(Ljava/lang/String;)V

    .line 251
    new-instance v1, Lcom/google/android/gms/ads/internal/b/h;

    invoke-direct {v1, p0, v2, v0}, Lcom/google/android/gms/ads/internal/b/h;-><init>(Lcom/google/android/gms/ads/internal/b/d;II)V

    move-object v0, v1

    goto :goto_0

    .line 253
    :cond_1
    instance-of v1, p1, Landroid/webkit/WebView;

    if-eqz v1, :cond_4

    instance-of v1, p1, Lcom/google/android/gms/ads/internal/p/a;

    if-nez v1, :cond_4

    .line 254
    invoke-virtual {p2}, Lcom/google/android/gms/ads/internal/b/a;->b()V

    .line 255
    check-cast p1, Landroid/webkit/WebView;

    const/16 v1, 0x13

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-nez v1, :cond_2

    move v1, v0

    :goto_1
    if-eqz v1, :cond_3

    .line 256
    new-instance v1, Lcom/google/android/gms/ads/internal/b/h;

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/gms/ads/internal/b/h;-><init>(Lcom/google/android/gms/ads/internal/b/d;II)V

    move-object v0, v1

    goto :goto_0

    .line 255
    :cond_2
    invoke-virtual {p2}, Lcom/google/android/gms/ads/internal/b/a;->b()V

    new-instance v1, Lcom/google/android/gms/ads/internal/b/f;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/ads/internal/b/f;-><init>(Lcom/google/android/gms/ads/internal/b/d;Lcom/google/android/gms/ads/internal/b/a;Landroid/webkit/WebView;)V

    invoke-virtual {p1, v1}, Landroid/webkit/WebView;->post(Ljava/lang/Runnable;)Z

    move v1, v2

    goto :goto_1

    .line 259
    :cond_3
    new-instance v1, Lcom/google/android/gms/ads/internal/b/h;

    invoke-direct {v1, p0, v0, v0}, Lcom/google/android/gms/ads/internal/b/h;-><init>(Lcom/google/android/gms/ads/internal/b/d;II)V

    move-object v0, v1

    goto :goto_0

    .line 262
    :cond_4
    instance-of v1, p1, Landroid/view/ViewGroup;

    if-eqz v1, :cond_6

    .line 263
    check-cast p1, Landroid/view/ViewGroup;

    move v1, v0

    move v2, v0

    .line 266
    :goto_2
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    if-ge v0, v3, :cond_5

    .line 267
    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-direct {p0, v3, p2}, Lcom/google/android/gms/ads/internal/b/d;->a(Landroid/view/View;Lcom/google/android/gms/ads/internal/b/a;)Lcom/google/android/gms/ads/internal/b/h;

    move-result-object v3

    .line 268
    iget v4, v3, Lcom/google/android/gms/ads/internal/b/h;->a:I

    add-int/2addr v2, v4

    .line 269
    iget v3, v3, Lcom/google/android/gms/ads/internal/b/h;->b:I

    add-int/2addr v1, v3

    .line 266
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 271
    :cond_5
    new-instance v0, Lcom/google/android/gms/ads/internal/b/h;

    invoke-direct {v0, p0, v2, v1}, Lcom/google/android/gms/ads/internal/b/h;-><init>(Lcom/google/android/gms/ads/internal/b/d;II)V

    goto :goto_0

    .line 274
    :cond_6
    new-instance v1, Lcom/google/android/gms/ads/internal/b/h;

    invoke-direct {v1, p0, v0, v0}, Lcom/google/android/gms/ads/internal/b/h;-><init>(Lcom/google/android/gms/ads/internal/b/d;II)V

    move-object v0, v1

    goto :goto_0
.end method

.method private e()Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 183
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b/d;->e:Lcom/google/android/gms/ads/internal/b/c;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/b/c;->b()Landroid/content/Context;

    move-result-object v2

    .line 188
    if-nez v2, :cond_0

    move v0, v3

    .line 223
    :goto_0
    return v0

    .line 192
    :cond_0
    const-string v0, "activity"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 194
    const-string v1, "keyguard"

    invoke-virtual {v2, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/KeyguardManager;

    .line 196
    const-string v4, "power"

    invoke-virtual {v2, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/PowerManager;

    .line 200
    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    if-nez v2, :cond_2

    :cond_1
    move v0, v3

    .line 201
    goto :goto_0

    .line 203
    :cond_2
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getRunningAppProcesses()Ljava/util/List;

    move-result-object v0

    .line 205
    if-nez v0, :cond_3

    move v0, v3

    .line 206
    goto :goto_0

    .line 208
    :cond_3
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningAppProcessInfo;

    .line 209
    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v5

    iget v6, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->pid:I

    if-ne v5, v6, :cond_4

    .line 210
    iget v0, v0, Landroid/app/ActivityManager$RunningAppProcessInfo;->importance:I

    const/16 v4, 0x64

    if-ne v0, v4, :cond_5

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-nez v0, :cond_5

    invoke-virtual {v2}, Landroid/os/PowerManager;->isScreenOn()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    if-eqz v0, :cond_5

    .line 216
    const/4 v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v3

    .line 221
    goto :goto_0

    .line 223
    :catch_0
    move-exception v0

    move v0, v3

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 89
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b/d;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 90
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/b/d;->a:Z

    if-eqz v0, :cond_0

    .line 91
    const-string v0, "Content hash thread already started, quiting..."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 92
    monitor-exit v1

    .line 97
    :goto_0
    return-void

    .line 94
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/b/d;->a:Z

    .line 95
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 96
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/b/d;->start()V

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(Landroid/view/View;)V
    .locals 5

    .prologue
    .line 149
    :try_start_0
    new-instance v0, Lcom/google/android/gms/ads/internal/b/a;

    iget v1, p0, Lcom/google/android/gms/ads/internal/b/d;->i:I

    iget v2, p0, Lcom/google/android/gms/ads/internal/b/d;->j:I

    iget v3, p0, Lcom/google/android/gms/ads/internal/b/d;->k:I

    iget v4, p0, Lcom/google/android/gms/ads/internal/b/d;->l:I

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/ads/internal/b/a;-><init>(IIII)V

    .line 153
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/ads/internal/b/d;->a(Landroid/view/View;Lcom/google/android/gms/ads/internal/b/a;)Lcom/google/android/gms/ads/internal/b/h;

    move-result-object v1

    .line 154
    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/b/a;->c()V

    .line 155
    iget v2, v1, Lcom/google/android/gms/ads/internal/b/h;->a:I

    if-nez v2, :cond_1

    iget v2, v1, Lcom/google/android/gms/ads/internal/b/h;->b:I

    if-nez v2, :cond_1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 160
    :cond_1
    iget v2, v1, Lcom/google/android/gms/ads/internal/b/h;->b:I

    if-nez v2, :cond_2

    iget v2, v0, Lcom/google/android/gms/ads/internal/b/a;->b:I

    if-eqz v2, :cond_0

    .line 164
    :cond_2
    iget v1, v1, Lcom/google/android/gms/ads/internal/b/h;->b:I

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b/d;->f:Lcom/google/android/gms/ads/internal/b/b;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/internal/b/b;->a(Lcom/google/android/gms/ads/internal/b/a;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 169
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b/d;->f:Lcom/google/android/gms/ads/internal/b/b;

    iget-object v2, v1, Lcom/google/android/gms/ads/internal/b/b;->a:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v3, v1, Lcom/google/android/gms/ads/internal/b/b;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    const/16 v4, 0xa

    if-lt v3, v4, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Queue is full, current size = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v1, Lcom/google/android/gms/ads/internal/b/b;->c:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    iget-object v3, v1, Lcom/google/android/gms/ads/internal/b/b;->c:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_4
    iget v3, v1, Lcom/google/android/gms/ads/internal/b/b;->b:I

    add-int/lit8 v4, v3, 0x1

    iput v4, v1, Lcom/google/android/gms/ads/internal/b/b;->b:I

    iput v3, v0, Lcom/google/android/gms/ads/internal/b/a;->c:I

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/b/b;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v2

    throw v0
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    .line 173
    :catch_0
    move-exception v0

    .line 171
    const-string v1, "Exception in fetchContentOnUIThread"

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 172
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b/d;->g:Lcom/google/android/gms/ads/internal/m/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/internal/m/a;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/ads/internal/b/a;Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 316
    iget-object v1, p1, Lcom/google/android/gms/ads/internal/b/a;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p1, Lcom/google/android/gms/ads/internal/b/a;->d:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p1, Lcom/google/android/gms/ads/internal/b/a;->d:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 318
    :try_start_1
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 320
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 321
    const-string v1, "text"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 322
    invoke-virtual {p2}, Landroid/webkit/WebView;->getTitle()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 323
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p2}, Landroid/webkit/WebView;->getTitle()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/ads/internal/b/a;->a(Ljava/lang/String;)V

    .line 328
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/ads/internal/b/a;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 330
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b/d;->f:Lcom/google/android/gms/ads/internal/b/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/ads/internal/b/b;->b(Lcom/google/android/gms/ads/internal/b/a;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    .line 339
    :cond_1
    :goto_1
    return-void

    .line 316
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 325
    :cond_2
    :try_start_2
    invoke-virtual {p1, v0}, Lcom/google/android/gms/ads/internal/b/a;->a(Ljava/lang/String;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 333
    :catch_0
    move-exception v0

    const-string v0, "Json string may be malformed."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    goto :goto_1

    .line 335
    :catch_1
    move-exception v0

    .line 336
    const-string v1, "Failed to get webview content."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 337
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b/d;->g:Lcom/google/android/gms/ads/internal/m/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/internal/m/a;->a(Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method public final b()Lcom/google/android/gms/ads/internal/b/a;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b/d;->f:Lcom/google/android/gms/ads/internal/b/b;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/b/b;->a()Lcom/google/android/gms/ads/internal/b/a;

    move-result-object v0

    return-object v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 346
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b/d;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 347
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/b/d;->b:Z

    .line 348
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b/d;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    .line 349
    const-string v0, "ContentFetchThread: wakeup"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 350
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 361
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/b/d;->b:Z

    return v0
.end method

.method public final run()V
    .locals 3

    .prologue
    .line 101
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/b/d;->c:Z

    if-nez v0, :cond_5

    .line 103
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/b/d;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b/d;->e:Lcom/google/android/gms/ads/internal/b/c;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/b/c;->a()Landroid/app/Activity;

    move-result-object v1

    .line 105
    if-nez v1, :cond_0

    .line 106
    const-string v0, "ContentFetchThread: no activity"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    const-string v1, "Error in ContentFetchTask"

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 117
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b/d;->g:Lcom/google/android/gms/ads/internal/m/a;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/ads/internal/m/a;->a(Ljava/lang/Throwable;)V

    .line 119
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b/d;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 120
    :goto_2
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/b/d;->b:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_4

    .line 122
    :try_start_2
    const-string v0, "ContentFetchTask: waiting"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/b/d;->d:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->wait()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    .line 126
    :catch_1
    move-exception v0

    goto :goto_2

    .line 109
    :cond_0
    if-eqz v1, :cond_2

    const/4 v0, 0x0

    :try_start_3
    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    :cond_1
    if-eqz v0, :cond_2

    if-eqz v0, :cond_2

    new-instance v1, Lcom/google/android/gms/ads/internal/b/e;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/ads/internal/b/e;-><init>(Lcom/google/android/gms/ads/internal/b/d;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 114
    :cond_2
    :goto_3
    iget v0, p0, Lcom/google/android/gms/ads/internal/b/d;->h:I

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v0, v0

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V

    goto :goto_1

    .line 111
    :cond_3
    const-string v0, "ContentFetchTask: sleeping"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 112
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/b/d;->d:Ljava/lang/Object;

    monitor-enter v1
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0

    const/4 v0, 0x1

    :try_start_4
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/b/d;->b:Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "ContentFetchThread: paused, mPause = "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/gms/ads/internal/b/d;->b:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_5
    monitor-exit v1

    throw v0
    :try_end_5
    .catch Ljava/lang/Throwable; {:try_start_5 .. :try_end_5} :catch_0

    .line 128
    :cond_4
    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto/16 :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 130
    :cond_5
    return-void
.end method
