.class public final Lcom/google/android/gms/icing/c/a/o;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1789
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1790
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/c/a/o;->a:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/o;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/o;->c:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/o;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/o;->cachedSize:I

    .line 1791
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1860
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1861
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/o;->a:Z

    if-eqz v1, :cond_0

    .line 1862
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/android/gms/icing/c/a/o;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1865
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/o;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1866
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/o;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1869
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/o;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1870
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/o;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1873
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1804
    if-ne p1, p0, :cond_1

    .line 1805
    const/4 v0, 0x1

    .line 1828
    :cond_0
    :goto_0
    return v0

    .line 1807
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/o;

    if-eqz v1, :cond_0

    .line 1810
    check-cast p1, Lcom/google/android/gms/icing/c/a/o;

    .line 1811
    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/o;->a:Z

    iget-boolean v2, p1, Lcom/google/android/gms/icing/c/a/o;->a:Z

    if-ne v1, v2, :cond_0

    .line 1814
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/o;->b:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 1815
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/o;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1821
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/o;->c:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 1822
    iget-object v1, p1, Lcom/google/android/gms/icing/c/a/o;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1828
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/o;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1818
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/o;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/o;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1825
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/o;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/o;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1833
    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/o;->a:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1835
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/o;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1837
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/o;->c:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 1839
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/o;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1840
    return v0

    .line 1833
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 1835
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/o;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1837
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/o;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1763
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/o;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/c/a/o;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/o;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/o;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1846
    iget-boolean v0, p0, Lcom/google/android/gms/icing/c/a/o;->a:Z

    if-eqz v0, :cond_0

    .line 1847
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/gms/icing/c/a/o;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1849
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/o;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1850
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/o;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1852
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/o;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1853
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/o;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1855
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1856
    return-void
.end method
