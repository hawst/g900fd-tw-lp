.class public final Lcom/google/android/gms/drive/realtime/cache/q;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/ExecutorService;

.field private static final b:Ljava/util/concurrent/ScheduledExecutorService;

.field private static final c:Lcom/google/c/a/a/b/e/n;


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/drive/realtime/cache/i;

.field private final f:Lcom/google/android/gms/drive/database/r;

.field private final g:Lcom/google/android/gms/drive/realtime/cache/o;

.field private final h:Lcom/google/c/a/a/b/a/a/a;

.field private final i:I

.field private final j:Lcom/google/android/gms/drive/realtime/cache/w;

.field private final k:Lcom/google/android/gms/drive/g/n;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 53
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/q;->a:Ljava/util/concurrent/ExecutorService;

    .line 54
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/q;->b:Ljava/util/concurrent/ScheduledExecutorService;

    .line 56
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/r;

    invoke-direct {v0}, Lcom/google/android/gms/drive/realtime/cache/r;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/realtime/cache/q;->c:Lcom/google/c/a/a/b/e/n;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/android/gms/drive/realtime/cache/o;Lcom/google/android/gms/drive/database/r;Lcom/google/c/a/a/b/a/a/a;Lcom/google/android/gms/drive/realtime/cache/w;Lcom/google/android/gms/drive/g/n;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/cache/q;->d:Landroid/content/Context;

    .line 78
    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/cache/q;->e:Lcom/google/android/gms/drive/realtime/cache/i;

    .line 79
    iput-object p3, p0, Lcom/google/android/gms/drive/realtime/cache/q;->g:Lcom/google/android/gms/drive/realtime/cache/o;

    .line 80
    iput-object p4, p0, Lcom/google/android/gms/drive/realtime/cache/q;->f:Lcom/google/android/gms/drive/database/r;

    .line 81
    iput-object p5, p0, Lcom/google/android/gms/drive/realtime/cache/q;->h:Lcom/google/c/a/a/b/a/a/a;

    .line 82
    const v0, 0x5b8d80

    iput v0, p0, Lcom/google/android/gms/drive/realtime/cache/q;->i:I

    .line 83
    iput-object p6, p0, Lcom/google/android/gms/drive/realtime/cache/q;->j:Lcom/google/android/gms/drive/realtime/cache/w;

    .line 84
    iput-object p7, p0, Lcom/google/android/gms/drive/realtime/cache/q;->k:Lcom/google/android/gms/drive/g/n;

    .line 85
    return-void
.end method

.method static synthetic b()Ljava/util/concurrent/ScheduledExecutorService;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/google/android/gms/drive/realtime/cache/q;->b:Ljava/util/concurrent/ScheduledExecutorService;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 18

    .prologue
    .line 92
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/drive/realtime/cache/q;->g:Lcom/google/android/gms/drive/realtime/cache/o;

    sget-object v2, Lcom/google/android/gms/drive/ai;->M:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v3, Lcom/google/android/gms/drive/database/x;->b:Lcom/google/android/gms/drive/database/x;

    sget-object v4, Lcom/google/android/gms/drive/database/model/bu;->i:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v4

    const-wide/16 v6, 0x1

    invoke-virtual {v4, v6, v7}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/gms/drive/database/model/bu;->j:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v7

    const-wide/16 v10, 0x1

    invoke-virtual {v7, v10, v11}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    sget-object v7, Lcom/google/android/gms/drive/database/model/bu;->k:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v7

    int-to-long v10, v2

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ab;->d()V

    sget-object v2, Lcom/google/android/gms/drive/database/model/aw;->a:Lcom/google/android/gms/drive/database/model/aw;

    invoke-virtual {v7, v2}, Lcom/google/android/gms/drive/database/model/ab;->a(Lcom/google/android/gms/drive/database/model/aw;)V

    new-instance v2, Lcom/google/android/gms/drive/database/SqlWhereClause;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7}, Lcom/google/android/gms/drive/database/model/ab;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v9, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v9, ">=?"

    invoke-virtual {v7, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v10, v11}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v2, v7, v9}, Lcom/google/android/gms/drive/database/SqlWhereClause;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v6

    iget-object v2, v8, Lcom/google/android/gms/drive/realtime/cache/o;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->a()Lcom/google/android/gms/drive/database/model/bt;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/bt;->e()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    new-instance v12, Lcom/google/android/gms/drive/database/b/b;

    iget-object v3, v8, Lcom/google/android/gms/drive/realtime/cache/o;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bs;->a()Lcom/google/android/gms/drive/database/model/bs;

    move-result-object v4

    invoke-direct {v12, v3, v2, v4}, Lcom/google/android/gms/drive/database/b/b;-><init>(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/c;)V

    .line 96
    :try_start_0
    const-string v2, "RealtimeDocumentSyncer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Syncing/compacting "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v12}, Lcom/google/android/gms/drive/database/b/d;->size()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " Realtime documents..."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    invoke-interface {v12}, Lcom/google/android/gms/drive/database/b/d;->iterator()Ljava/util/Iterator;

    move-result-object v13

    :cond_0
    :goto_0
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/drive/database/model/br;

    .line 100
    iget-object v14, v2, Lcom/google/android/gms/drive/database/model/br;->a:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 102
    :try_start_1
    const-string v3, "RealtimeDocumentSyncer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Syncing/compacting realtime document: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/realtime/cache/q;->f:Lcom/google/android/gms/drive/database/r;

    iget-wide v4, v2, Lcom/google/android/gms/drive/database/model/br;->b:J

    invoke-interface {v3, v4, v5}, Lcom/google/android/gms/drive/database/r;->f(J)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/br;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/drive/realtime/cache/q;->f:Lcom/google/android/gms/drive/database/r;

    iget-wide v6, v3, Lcom/google/android/gms/drive/database/model/f;->a:J

    iget-object v3, v3, Lcom/google/android/gms/drive/database/model/f;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-interface {v4, v6, v7, v3}, Lcom/google/android/gms/drive/database/r;->a(JLcom/google/android/gms/drive/auth/AppIdentity;)Lcom/google/android/gms/drive/auth/g;

    move-result-object v3

    new-instance v15, Lcom/google/android/gms/drive/realtime/e;

    invoke-direct {v15, v2, v3}, Lcom/google/android/gms/drive/realtime/e;-><init>(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/auth/g;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/realtime/cache/q;->g:Lcom/google/android/gms/drive/realtime/cache/o;

    const-string v3, "Syncer"

    const/4 v4, 0x0

    invoke-virtual {v2, v15, v3, v4}, Lcom/google/android/gms/drive/realtime/cache/o;->a(Lcom/google/android/gms/drive/realtime/e;Ljava/lang/String;Z)Lcom/google/android/gms/drive/realtime/cache/n;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v16

    if-nez v16, :cond_1

    :try_start_2
    const-string v2, "RealtimeDocumentSyncer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Realtime document is in use; skipping: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-eqz v16, :cond_0

    :try_start_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/realtime/cache/q;->d:Landroid/content/Context;

    new-instance v3, Lcom/google/android/gms/drive/realtime/cache/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/drive/realtime/cache/q;->j:Lcom/google/android/gms/drive/realtime/cache/w;

    const/4 v5, 0x0

    move-object/from16 v0, v16

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/gms/drive/realtime/cache/f;-><init>(Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/w;Z)V

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/realtime/cache/CachingOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_0

    .line 104
    :catch_0
    move-exception v2

    .line 105
    :try_start_4
    const-string v3, "RealtimeDocumentSyncer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unexpected error during sync/compaction for Realtime document: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_0

    .line 111
    :catch_1
    move-exception v2

    .line 112
    :try_start_5
    const-string v3, "RealtimeDocumentSyncer"

    const-string v4, "Syncing/compacting failed."

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 114
    invoke-interface {v12}, Lcom/google/android/gms/drive/database/b/d;->close()V

    .line 115
    :goto_1
    return-void

    .line 103
    :cond_1
    :try_start_6
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/drive/realtime/cache/n;->j()Lcom/google/android/gms/drive/database/model/br;

    move-result-object v2

    iget-boolean v3, v2, Lcom/google/android/gms/drive/database/model/br;->g:Z

    if-nez v3, :cond_2

    iget-boolean v2, v2, Lcom/google/android/gms/drive/database/model/br;->d:Z

    if-eqz v2, :cond_8

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/realtime/cache/q;->k:Lcom/google/android/gms/drive/g/n;

    invoke-interface {v2}, Lcom/google/android/gms/drive/g/n;->a()Z

    move-result v2

    if-eqz v2, :cond_9

    const-string v2, "RealtimeDocumentSyncer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Syncing Realtime document: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, v15, Lcom/google/android/gms/drive/realtime/e;->b:Lcom/google/android/gms/drive/auth/g;

    new-instance v5, Lcom/google/android/gms/drive/realtime/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/realtime/cache/q;->d:Landroid/content/Context;

    invoke-direct {v5, v3, v2}, Lcom/google/android/gms/drive/realtime/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;)V

    new-instance v11, Lcom/google/android/gms/drive/realtime/l;

    invoke-direct {v11}, Lcom/google/android/gms/drive/realtime/l;-><init>()V

    new-instance v10, Lcom/google/android/gms/drive/realtime/cache/t;

    const/4 v2, 0x0

    invoke-direct {v10, v2}, Lcom/google/android/gms/drive/realtime/cache/t;-><init>(B)V

    new-instance v2, Lcom/google/c/a/a/a/c/a;

    new-instance v3, Lcom/google/android/gms/drive/realtime/c;

    invoke-direct {v3}, Lcom/google/android/gms/drive/realtime/c;-><init>()V

    new-instance v4, Lcom/google/android/gms/drive/realtime/cache/u;

    const/4 v6, 0x0

    invoke-direct {v4, v6}, Lcom/google/android/gms/drive/realtime/cache/u;-><init>(B)V

    sget-object v6, Lcom/google/android/gms/drive/ai;->Q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const-string v7, "android"

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/gms/drive/realtime/cache/q;->i:I

    invoke-static {v8}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/google/c/a/a/a/c/a/d/x;

    sget-object v17, Lcom/google/android/gms/drive/realtime/cache/q;->a:Ljava/util/concurrent/ExecutorService;

    move-object/from16 v0, v17

    invoke-direct {v9, v0}, Lcom/google/c/a/a/a/c/a/d/x;-><init>(Ljava/util/concurrent/ExecutorService;)V

    invoke-direct/range {v2 .. v11}, Lcom/google/c/a/a/a/c/a;-><init>(Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/a/c/s;Lcom/google/c/a/a/a/c/u;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/c/a/a/a/c/a/d/k;Lcom/google/c/a/a/a/c/a/e/b;Lcom/google/c/a/a/a/c/a/e/f;)V

    new-instance v9, Lcom/google/c/a/a/b/a/a/f;

    sget-object v3, Lcom/google/android/gms/drive/realtime/cache/q;->c:Lcom/google/c/a/a/b/e/n;

    invoke-direct {v9, v2, v3, v10}, Lcom/google/c/a/a/b/a/a/f;-><init>(Lcom/google/c/a/a/a/c/i;Lcom/google/c/a/a/b/e/n;Lcom/google/c/a/a/a/c/a/e/b;)V

    new-instance v2, Lcom/google/android/gms/drive/realtime/cache/d;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/realtime/cache/q;->d:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/drive/realtime/cache/q;->e:Lcom/google/android/gms/drive/realtime/cache/i;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/drive/realtime/cache/q;->g:Lcom/google/android/gms/drive/realtime/cache/o;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/drive/realtime/cache/q;->j:Lcom/google/android/gms/drive/realtime/cache/w;

    const/4 v8, 0x0

    move-object/from16 v4, v16

    invoke-direct/range {v2 .. v8}, Lcom/google/android/gms/drive/realtime/cache/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/android/gms/drive/realtime/cache/o;Lcom/google/android/gms/drive/realtime/cache/w;Z)V

    new-instance v3, Lcom/google/android/gms/drive/realtime/cache/v;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/gms/drive/realtime/cache/v;-><init>(B)V

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/drive/realtime/cache/n;->a()Z

    move-result v4

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/drive/realtime/cache/q;->h:Lcom/google/c/a/a/b/a/a/a;

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/drive/realtime/cache/n;->c()Lcom/google/c/a/a/b/a/f;

    move-result-object v5

    invoke-virtual {v4, v5, v2, v9, v3}, Lcom/google/c/a/a/b/a/a/a;->a(Lcom/google/c/a/a/b/a/f;Lcom/google/c/a/a/b/a/e;Lcom/google/c/a/a/b/a/a/f;Lcom/google/c/a/a/b/a/a/g;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :goto_2
    :try_start_7
    iget-object v2, v3, Lcom/google/android/gms/drive/realtime/cache/v;->a:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v6}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v2

    if-nez v2, :cond_6

    new-instance v2, Ljava/util/concurrent/TimeoutException;

    const-string v3, "Sync timed out."

    invoke-direct {v2, v3}, Ljava/util/concurrent/TimeoutException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_7
    .catch Lcom/google/c/a/a/b/e/o; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :catch_2
    move-exception v2

    :try_start_8
    invoke-virtual {v2}, Lcom/google/c/a/a/b/e/o;->a()Z

    move-result v3

    if-eqz v3, :cond_3

    const-string v3, "RealtimeDocumentSyncer"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Fatal sync error for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v15, Lcom/google/android/gms/drive/realtime/e;->a:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/realtime/cache/q;->g:Lcom/google/android/gms/drive/realtime/cache/o;

    iget-object v4, v15, Lcom/google/android/gms/drive/realtime/e;->b:Lcom/google/android/gms/drive/auth/g;

    iget-object v5, v15, Lcom/google/android/gms/drive/realtime/e;->a:Lcom/google/android/gms/drive/DriveId;

    invoke-static {v5}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/realtime/cache/o;->a(Lcom/google/android/gms/drive/auth/g;Ljava/util/List;)V

    :cond_3
    throw v2
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :catchall_0
    move-exception v2

    if-eqz v16, :cond_4

    :try_start_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/realtime/cache/q;->d:Landroid/content/Context;

    new-instance v4, Lcom/google/android/gms/drive/realtime/cache/f;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/drive/realtime/cache/q;->j:Lcom/google/android/gms/drive/realtime/cache/w;

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-direct {v4, v0, v5, v6}, Lcom/google/android/gms/drive/realtime/cache/f;-><init>(Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/w;Z)V

    invoke-static {v3, v4}, Lcom/google/android/gms/drive/realtime/cache/CachingOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    :cond_4
    throw v2
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 114
    :catchall_1
    move-exception v2

    invoke-interface {v12}, Lcom/google/android/gms/drive/database/b/d;->close()V

    throw v2

    .line 103
    :cond_5
    :try_start_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/drive/realtime/cache/q;->h:Lcom/google/c/a/a/b/a/a/a;

    move-object/from16 v0, v16

    iget-object v5, v0, Lcom/google/android/gms/drive/realtime/cache/n;->a:Lcom/google/android/gms/drive/realtime/e;

    iget-object v5, v5, Lcom/google/android/gms/drive/realtime/e;->a:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v5}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v2, v9, v3}, Lcom/google/c/a/a/b/a/a/a;->a(Ljava/lang/String;Lcom/google/c/a/a/b/a/e;Lcom/google/c/a/a/b/a/a/f;Lcom/google/c/a/a/b/a/a/g;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto :goto_2

    :cond_6
    :try_start_b
    iget-boolean v2, v3, Lcom/google/android/gms/drive/realtime/cache/v;->b:Z

    if-nez v2, :cond_7

    iget-object v2, v3, Lcom/google/android/gms/drive/realtime/cache/v;->c:Lcom/google/c/a/a/b/e/o;

    throw v2
    :try_end_b
    .catch Lcom/google/c/a/a/b/e/o; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    :cond_7
    :try_start_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/realtime/cache/q;->d:Landroid/content/Context;

    new-instance v3, Lcom/google/android/gms/drive/realtime/cache/e;

    move-object/from16 v0, v16

    invoke-direct {v3, v0}, Lcom/google/android/gms/drive/realtime/cache/e;-><init>(Lcom/google/android/gms/drive/realtime/cache/n;)V

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/realtime/cache/CachingOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    :cond_8
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/realtime/cache/q;->d:Landroid/content/Context;

    new-instance v3, Lcom/google/android/gms/drive/realtime/cache/g;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/drive/realtime/cache/q;->e:Lcom/google/android/gms/drive/realtime/cache/i;

    move-object/from16 v0, v16

    invoke-direct {v3, v0, v4}, Lcom/google/android/gms/drive/realtime/cache/g;-><init>(Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/i;)V

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/realtime/cache/CachingOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    if-eqz v16, :cond_0

    :try_start_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/realtime/cache/q;->d:Landroid/content/Context;

    new-instance v3, Lcom/google/android/gms/drive/realtime/cache/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/drive/realtime/cache/q;->j:Lcom/google/android/gms/drive/realtime/cache/w;

    const/4 v5, 0x0

    move-object/from16 v0, v16

    invoke-direct {v3, v0, v4, v5}, Lcom/google/android/gms/drive/realtime/cache/f;-><init>(Lcom/google/android/gms/drive/realtime/cache/n;Lcom/google/android/gms/drive/realtime/cache/w;Z)V

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/realtime/cache/CachingOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V
    :try_end_d
    .catch Ljava/lang/Throwable; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto/16 :goto_0

    :cond_9
    :try_start_e
    const-string v2, "RealtimeDocumentSyncer"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Skipping sync for Realtime document (device is offline): "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_3

    .line 110
    :cond_a
    :try_start_f
    const-string v2, "RealtimeDocumentSyncer"

    const-string v3, "Syncing/compacting complete."

    invoke-static {v2, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_f
    .catch Ljava/lang/Throwable; {:try_start_f .. :try_end_f} :catch_1
    .catchall {:try_start_f .. :try_end_f} :catchall_1

    .line 114
    invoke-interface {v12}, Lcom/google/android/gms/drive/database/b/d;->close()V

    goto/16 :goto_1
.end method
