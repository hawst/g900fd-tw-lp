.class final Lcom/google/android/gms/plus/activity/i;
.super Lcom/google/android/gms/plus/activity/k;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V
    .locals 1

    .prologue
    .line 620
    iput-object p1, p0, Lcom/google/android/gms/plus/activity/i;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/plus/activity/k;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;B)V
    .locals 0

    .prologue
    .line 620
    invoke-direct {p0, p1}, Lcom/google/android/gms/plus/activity/i;-><init>(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)V

    return-void
.end method


# virtual methods
.method public final c()Z
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/i;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v0}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->i(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Landroid/app/PendingIntent;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()V
    .locals 7

    .prologue
    .line 629
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/i;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/i;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->i(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v1

    const/4 v2, 0x3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->startIntentSenderForResult(Landroid/content/IntentSender;ILandroid/content/Intent;III)V

    .line 634
    iget-object v0, p0, Lcom/google/android/gms/plus/activity/i;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    iget-object v1, p0, Lcom/google/android/gms/plus/activity/i;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v1}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->b(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/common/analytics/y;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    sget-object v3, Lcom/google/android/gms/common/analytics/y;->f:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v4, p0, Lcom/google/android/gms/plus/activity/i;->a:Lcom/google/android/gms/plus/activity/AccountSignUpActivity;

    invoke-static {v4}, Lcom/google/android/gms/plus/activity/AccountSignUpActivity;->c(Lcom/google/android/gms/plus/activity/AccountSignUpActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 641
    :goto_0
    return-void

    .line 638
    :catch_0
    move-exception v0

    .line 639
    const-string v1, "AccountSignUpActivity"

    const-string v2, "Exception showing GMS Auth Activity."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
