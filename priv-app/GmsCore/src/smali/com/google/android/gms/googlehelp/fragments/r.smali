.class public final Lcom/google/android/gms/googlehelp/fragments/r;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/webkit/WebView;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 105
    if-eqz p4, :cond_4

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-static {v2}, Ljava/lang/Character;->getDirectionality(C)B

    move-result v2

    if-eq v2, v0, :cond_0

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    :cond_0
    :goto_0
    if-eqz v0, :cond_3

    const-string v0, "<style>.ghelp-body {  margin: 0;}.ghelp-title {  background-color:#F2F2F2;  font-size:25px;  font-family: Sans-Serif-Light;  color:#666666;  padding:16px;  border-color:#A8A8A8;  border-width:1px;  border-bottom-style:solid;  direction:rtl;}.ghelp-content {  padding:0 16px 0 16px;}.ghelp-content ol, .ghelp-content ul {  -webkit-padding-start: 0;}</style>"

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "<html><head>"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</head><body class=\"ghelp-body\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    if-eqz p4, :cond_1

    const-string v1, "<h1 class=\"ghelp-title\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "</h1>"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string v1, "<div class=\"ghelp-content\">"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "</div></body></html>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 106
    const-string v3, "text/html"

    const-string v4, "UTF-8"

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Landroid/webkit/WebView;->loadDataWithBaseURL(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    return-void

    :cond_2
    move v0, v1

    .line 105
    goto :goto_0

    :cond_3
    const-string v0, "<style>.ghelp-body {  margin: 0;}.ghelp-title {  background-color:#F2F2F2;  font-size:25px;  font-family: Sans-Serif-Light;  color:#666666;  padding:16px;  border-color:#A8A8A8;  border-width:1px;  border-bottom-style:solid;}.ghelp-content {  padding:0 16px 0 16px;}.ghelp-content ol, .ghelp-content ul {  -webkit-padding-start: 0;}</style>"

    goto :goto_1

    :cond_4
    const-string v0, "<style>.ghelp-search-snippet {  font-size:small;  margin-top:4px;}</style>"

    goto :goto_1
.end method
