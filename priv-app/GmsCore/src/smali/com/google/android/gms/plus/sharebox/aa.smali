.class final Lcom/google/android/gms/plus/sharebox/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/MultiAutoCompleteTextView$Tokenizer;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/CharSequence;II)I
    .locals 7

    .prologue
    const/16 v5, 0xa

    .line 94
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v3

    .line 95
    const/4 v2, 0x0

    move v0, p1

    .line 96
    :goto_0
    if-ge v0, p2, :cond_0

    .line 97
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 100
    if-ne v1, v5, :cond_1

    move p2, v0

    .line 138
    :cond_0
    :goto_1
    return p2

    .line 105
    :cond_1
    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 106
    add-int/lit8 v2, v2, 0x1

    .line 107
    const/4 v1, 0x4

    if-lt v2, v1, :cond_2

    move p2, v0

    .line 108
    goto :goto_1

    .line 112
    :cond_2
    add-int/lit8 v1, v0, 0x1

    .line 113
    :goto_2
    if-ge v1, v3, :cond_4

    .line 114
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    .line 115
    if-ne v4, v5, :cond_3

    move p2, v0

    .line 116
    goto :goto_1

    .line 119
    :cond_3
    invoke-static {v4}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 120
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 124
    :cond_4
    if-ne v1, v3, :cond_5

    move p2, v0

    .line 125
    goto :goto_1

    .line 129
    :cond_5
    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    move v6, v0

    move v0, v1

    move v1, v6

    .line 132
    :cond_6
    if-le v0, p2, :cond_8

    invoke-static {v1}, Lcom/google/android/gms/plus/sharebox/aa;->a(C)Z

    move-result v1

    if-eqz v1, :cond_8

    if-eqz v0, :cond_7

    add-int/lit8 v1, v0, -0x1

    invoke-interface {p0, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    invoke-static {v1}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_7
    move p2, v0

    .line 135
    goto :goto_1

    .line 96
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static a(C)Z
    .locals 1

    .prologue
    .line 22
    const/16 v0, 0x2b

    if-eq p0, v0, :cond_0

    const/16 v0, 0x40

    if-ne p0, v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final findTokenEnd(Ljava/lang/CharSequence;I)I
    .locals 1

    .prologue
    .line 89
    invoke-static {p1, p2, p2}, Lcom/google/android/gms/plus/sharebox/aa;->a(Ljava/lang/CharSequence;II)I

    move-result v0

    return v0
.end method

.method public final findTokenStart(Ljava/lang/CharSequence;I)I
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 27
    add-int/lit8 v1, p2, -0x1

    :goto_0
    if-ltz v1, :cond_0

    .line 30
    invoke-interface {p1, v1}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 31
    const/16 v4, 0xa

    if-ne v0, v4, :cond_1

    .line 62
    :cond_0
    :goto_1
    return p2

    .line 35
    :cond_1
    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/aa;->a(C)Z

    move-result v0

    if-eqz v0, :cond_7

    if-eqz v1, :cond_2

    add-int/lit8 v0, v1, -0x1

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 39
    :cond_2
    instance-of v0, p1, Landroid/text/Spannable;

    if-eqz v0, :cond_5

    move-object v0, p1

    check-cast v0, Landroid/text/Spannable;

    const-class v4, Lcom/google/android/gms/plus/sharebox/MentionSpan;

    invoke-interface {v0, v1, v1, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/plus/sharebox/MentionSpan;

    if-eqz v0, :cond_3

    array-length v0, v0

    if-eqz v0, :cond_3

    move v0, v2

    :goto_2
    if-nez v0, :cond_0

    .line 44
    invoke-static {p1, v1, p2}, Lcom/google/android/gms/plus/sharebox/aa;->a(Ljava/lang/CharSequence;II)I

    move-result v0

    .line 47
    :goto_3
    if-ge v0, p2, :cond_6

    .line 48
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v4

    invoke-static {v4}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 49
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    move-object v0, p1

    .line 39
    check-cast v0, Landroid/text/Spannable;

    const-class v4, Landroid/text/style/URLSpan;

    invoke-interface {v0, v1, v1, v4}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    if-eqz v0, :cond_5

    array-length v4, v0

    if-eqz v4, :cond_5

    array-length v5, v0

    move v4, v3

    :goto_4
    if-ge v4, v5, :cond_5

    aget-object v6, v0, v4

    invoke-static {v6}, Lcom/google/android/gms/plus/sharebox/MentionSpan;->a(Landroid/text/style/URLSpan;)Z

    move-result v6

    if-eqz v6, :cond_4

    move v0, v2

    goto :goto_2

    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    :cond_5
    move v0, v3

    goto :goto_2

    .line 57
    :cond_6
    if-ne v0, p2, :cond_7

    move p2, v1

    .line 58
    goto :goto_1

    .line 27
    :cond_7
    add-int/lit8 v1, v1, -0x1

    goto :goto_0
.end method

.method public final terminateToken(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 143
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    .line 144
    if-eqz v0, :cond_0

    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v4, p1

    .line 152
    :goto_0
    return-object v4

    .line 147
    :cond_1
    instance-of v0, p1, Landroid/text/Spanned;

    if-eqz v0, :cond_2

    .line 148
    new-instance v4, Landroid/text/SpannableString;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    move-object v0, p1

    .line 149
    check-cast v0, Landroid/text/Spanned;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    const-class v3, Ljava/lang/Object;

    move v5, v1

    invoke-static/range {v0 .. v5}, Landroid/text/TextUtils;->copySpansFrom(Landroid/text/Spanned;IILjava/lang/Class;Landroid/text/Spannable;I)V

    goto :goto_0

    .line 152
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method
