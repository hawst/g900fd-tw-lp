.class public final Lcom/google/android/gms/wallet/common/ui/validator/m;
.super Lcom/google/android/gms/wallet/common/ui/validator/b;
.source "SourceFile"


# instance fields
.field protected final a:Ljava/util/HashMap;

.field private c:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/wallet/common/ui/validator/m;-><init>(Ljava/lang/CharSequence;Ljava/util/HashMap;)V

    .line 27
    return-void
.end method

.method private constructor <init>(Ljava/lang/CharSequence;Ljava/util/HashMap;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/validator/b;-><init>(Ljava/lang/CharSequence;)V

    .line 33
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/ui/validator/m;->a:Ljava/util/HashMap;

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Landroid/widget/TextView;)Z
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 52
    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/validator/m;->a:Ljava/util/HashMap;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/validator/m;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    if-eqz p1, :cond_4

    instance-of v0, p1, Lcom/google/android/gms/wallet/common/ui/ad;

    if-eqz v0, :cond_4

    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/ad;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/ad;->a()Lcom/google/android/gms/wallet/common/ui/validator/b;

    move-result-object v0

    instance-of v4, v0, Lcom/google/android/gms/wallet/common/ui/validator/m;

    if-eqz v4, :cond_4

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/validator/m;

    iget-object v4, v0, Lcom/google/android/gms/wallet/common/ui/validator/m;->c:Ljava/lang/ref/WeakReference;

    if-eqz v4, :cond_3

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/ui/validator/m;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/common/ui/validator/n;

    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/validator/n;->a()Landroid/widget/TextView;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {p1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/validator/n;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_5

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/validator/n;->cancel(Z)Z

    :cond_1
    move v0, v1

    :goto_2
    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/gms/wallet/common/ui/validator/n;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/validator/m;->a:Ljava/util/HashMap;

    invoke-direct {v0, p1, v3}, Lcom/google/android/gms/wallet/common/ui/validator/n;-><init>(Landroid/widget/TextView;Ljava/util/HashMap;)V

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v3, p0, Lcom/google/android/gms/wallet/common/ui/validator/m;->c:Ljava/lang/ref/WeakReference;

    new-array v3, v1, [Landroid/widget/TextView;

    aput-object p1, v3, v2

    invoke-virtual {v0, v3}, Lcom/google/android/gms/wallet/common/ui/validator/n;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    move-object v0, v3

    goto :goto_1

    :cond_4
    move-object v0, v3

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_2
.end method
