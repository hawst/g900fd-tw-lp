.class final Lcom/google/android/gms/googlehelp/helpactivities/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/helpactivities/q;->a:Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/q;->a:Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->c(Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/q;->a:Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->d(Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 219
    return-void

    .line 217
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/q;->a:Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;

    invoke-static {v0}, Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;->c(Lcom/google/android/gms/googlehelp/helpactivities/RealtimeSupportClassifierActivity;)Landroid/widget/TextView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 224
    return-void
.end method
