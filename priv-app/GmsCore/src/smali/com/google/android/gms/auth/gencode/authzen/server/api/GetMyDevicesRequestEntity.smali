.class public final Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/gencode/authzen/server/api/ag;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/ai;

.field private static final g:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Z

.field d:Z

.field e:I

.field f:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ai;

    invoke-direct {v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ai;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/ai;

    .line 51
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 54
    sput-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->g:Ljava/util/HashMap;

    const-string v1, "allowStaleRead"

    const-string v2, "allowStaleRead"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->g:Ljava/util/HashMap;

    const-string v1, "approvedForUnlockRequired"

    const-string v2, "approvedForUnlockRequired"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->g:Ljava/util/HashMap;

    const-string v1, "invocationReason"

    const-string v2, "invocationReason"

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->g:Ljava/util/HashMap;

    const-string v1, "retryCount"

    const-string v2, "retryCount"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 102
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->b:I

    .line 103
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->a:Ljava/util/Set;

    .line 104
    return-void
.end method

.method constructor <init>(Ljava/util/Set;IZZII)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 115
    iput-object p1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->a:Ljava/util/Set;

    .line 116
    iput p2, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->b:I

    .line 117
    iput-boolean p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->c:Z

    .line 118
    iput-boolean p4, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->d:Z

    .line 119
    iput p5, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->e:I

    .line 120
    iput p6, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->f:I

    .line 121
    return-void
.end method

.method public constructor <init>(Ljava/util/Set;ZZII)V
    .locals 1

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 131
    iput-object p1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->a:Ljava/util/Set;

    .line 132
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->b:I

    .line 133
    iput-boolean p2, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->c:Z

    .line 134
    iput-boolean p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->d:Z

    .line 135
    iput p4, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->e:I

    .line 136
    iput p5, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->f:I

    .line 137
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->g:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;I)V
    .locals 4

    .prologue
    .line 257
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 258
    packed-switch v0, :pswitch_data_0

    .line 266
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be an int."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 260
    :pswitch_0
    iput p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->e:I

    .line 269
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 270
    return-void

    .line 263
    :pswitch_1
    iput p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->f:I

    goto :goto_0

    .line 258
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 275
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 276
    packed-switch v0, :pswitch_data_0

    .line 284
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a boolean."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 278
    :pswitch_0
    iput-boolean p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->c:Z

    .line 287
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 288
    return-void

    .line 281
    :pswitch_1
    iput-boolean p3, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->d:Z

    goto :goto_0

    .line 276
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 227
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 237
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 229
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 235
    :goto_0
    return-object v0

    .line 231
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->d:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 233
    :pswitch_2
    iget v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 235
    :pswitch_3
    iget v0, p0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->f:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    .line 227
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 22
    return-object p0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 212
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/ai;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 316
    instance-of v0, p1, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;

    if-nez v0, :cond_0

    move v0, v1

    .line 347
    :goto_0
    return v0

    .line 321
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 322
    goto :goto_0

    .line 325
    :cond_1
    check-cast p1, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;

    .line 326
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 327
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 328
    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 330
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 332
    goto :goto_0

    :cond_3
    move v0, v1

    .line 337
    goto :goto_0

    .line 340
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 342
    goto :goto_0

    :cond_5
    move v0, v2

    .line 347
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 303
    const/4 v0, 0x0

    .line 304
    sget-object v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 305
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 306
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 307
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 309
    goto :goto_0

    .line 310
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 217
    sget-object v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;->CREATOR:Lcom/google/android/gms/auth/gencode/authzen/server/api/ai;

    invoke-static {p0, p1}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ai;->a(Lcom/google/android/gms/auth/gencode/authzen/server/api/GetMyDevicesRequestEntity;Landroid/os/Parcel;)V

    .line 218
    return-void
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 298
    const/4 v0, 0x1

    return v0
.end method
