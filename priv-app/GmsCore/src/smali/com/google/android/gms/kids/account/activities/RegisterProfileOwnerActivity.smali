.class public Lcom/google/android/gms/kids/account/activities/RegisterProfileOwnerActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 66
    const-string v0, "RegisterProfileOwnerActivity"

    const-string v3, "onActivityResult. resultCode: %d"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    if-nez p2, :cond_0

    move v0, v1

    .line 68
    :goto_0
    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/google/android/gms/kids/account/receiver/ProfileOwnerSetupCompleteReceiver;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 69
    const-string v4, "is_remove_account_key"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 70
    const-string v0, "account_to_operate_on_key"

    iget-object v4, p0, Lcom/google/android/gms/kids/account/activities/RegisterProfileOwnerActivity;->a:Ljava/lang/String;

    invoke-virtual {v3, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 72
    const-string v0, "RegisterProfileOwnerActivity"

    const-string v4, "sending intent: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v3, v1, v2

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 73
    invoke-virtual {p0, v3}, Lcom/google/android/gms/kids/account/activities/RegisterProfileOwnerActivity;->sendBroadcast(Landroid/content/Intent;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/gms/kids/account/activities/RegisterProfileOwnerActivity;->finish()V

    .line 75
    return-void

    :cond_0
    move v0, v2

    .line 67
    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 27
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 28
    sget v0, Lcom/google/android/gms/l;->cu:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/kids/account/activities/RegisterProfileOwnerActivity;->setContentView(I)V

    .line 29
    const-string v0, "RegisterProfileOwnerActivity"

    const-string v1, "onCreate"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 30
    invoke-virtual {p0}, Lcom/google/android/gms/kids/account/activities/RegisterProfileOwnerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account_to_operate_on"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/kids/account/activities/RegisterProfileOwnerActivity;->a:Ljava/lang/String;

    .line 31
    invoke-static {p0}, Lcom/google/android/gms/kids/common/b;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v0

    .line 32
    invoke-virtual {p0}, Lcom/google/android/gms/kids/account/activities/RegisterProfileOwnerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 33
    invoke-virtual {v1, v0, v3, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    .line 37
    const-string v1, "RegisterProfileOwnerActivity"

    const-string v2, "Profile owner component enabled in package manager"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.app.action.SET_PROFILE_OWNER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 40
    const-string v2, "android.app.extra.DEVICE_ADMIN"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 41
    const-string v0, "android.app.extra.ADD_EXPLANATION"

    invoke-virtual {p0}, Lcom/google/android/gms/kids/account/activities/RegisterProfileOwnerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->pG:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 44
    const-string v0, "android.app.extra.PROFILE_OWNER_NAME"

    invoke-virtual {p0}, Lcom/google/android/gms/kids/account/activities/RegisterProfileOwnerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->pE:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 47
    const-string v0, "RegisterProfileOwnerActivity"

    const-string v2, "Firing intent to set profile owner"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 48
    const-string v0, "RegisterProfileOwnerActivity"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "account to operate on :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/kids/account/activities/RegisterProfileOwnerActivity;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/kids/common/f;->e(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 51
    const/4 v0, 0x1

    :try_start_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/kids/account/activities/RegisterProfileOwnerActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 56
    :goto_0
    return-void

    .line 52
    :catch_0
    move-exception v0

    .line 53
    const-string v1, "RegisterProfileOwnerActivity"

    const-string v2, "Error starting activity for ACTION_SET_PROFILE_OWNER"

    new-array v3, v4, [Ljava/lang/Object;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/kids/common/f;->c(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    invoke-virtual {p0}, Lcom/google/android/gms/kids/account/activities/RegisterProfileOwnerActivity;->finish()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 60
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 61
    const-string v0, "RegisterProfileOwnerActivity"

    const-string v1, "onDestroy"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    return-void
.end method
