.class public Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private b:Lcom/google/android/gms/common/api/v;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 66
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "sourceid"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "data5"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "account_name"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 269
    const-string v2, "PeopleProfileActionGA"

    const-string v3, "onPackageChanged"

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    invoke-static {}, Lcom/google/android/gms/people/sync/a/a;->a()Z

    move-result v2

    if-nez v2, :cond_0

    .line 292
    :goto_0
    return-void

    .line 275
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 276
    new-instance v4, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v4, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 277
    const-string v2, "vnd.android.cursor.item/vnd.googleplus.profile"

    invoke-virtual {v4, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    .line 278
    const-string v2, "com.google.android.apps.plus"

    invoke-virtual {v4, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 279
    const/4 v2, 0x0

    .line 281
    const/4 v5, 0x0

    :try_start_0
    invoke-virtual {v3, v4, v5}, Landroid/content/pm/PackageManager;->resolveActivity(Landroid/content/Intent;I)Landroid/content/pm/ResolveInfo;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 290
    :goto_1
    if-eqz v2, :cond_3

    move v2, v1

    .line 291
    :goto_2
    if-nez v2, :cond_1

    move v0, v1

    :cond_1
    const/4 v2, 0x3

    invoke-static {v2}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "PeopleProfileActionGA"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setActivityEnabled, enabled="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    new-instance v3, Landroid/content/ComponentName;

    const-class v4, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    if-eqz v0, :cond_4

    invoke-virtual {v2, v3, v1, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto :goto_0

    .line 282
    :catch_0
    move-exception v3

    .line 287
    const-string v4, "PeopleProfileActionGA"

    const-string v5, "Package manager threw"

    invoke-static {v4, v5, v3}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    :cond_3
    move v2, v0

    .line 290
    goto :goto_2

    .line 291
    :cond_4
    const/4 v0, 0x2

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/i;)V
    .locals 3

    .prologue
    .line 58
    :try_start_0
    invoke-interface {p3}, Lcom/google/android/gms/people/i;->c()Lcom/google/android/gms/people/model/k;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {p3}, Lcom/google/android/gms/people/i;->c()Lcom/google/android/gms/people/model/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/model/k;->c()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    :goto_0
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/gms/people/person/PeopleCirclePickerSpringBoardActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v2, "account"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "qualified_id"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "circle_ids"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->startActivity(Landroid/content/Intent;)V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p3}, Lcom/google/android/gms/people/i;->w_()V

    return-void

    :cond_0
    :try_start_1
    invoke-interface {p3}, Lcom/google/android/gms/people/i;->c()Lcom/google/android/gms/people/model/k;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/people/model/k;->b(I)Lcom/google/android/gms/people/model/j;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/people/model/j;->d()[Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string v0, "PeopleProfileActionGA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PeopleClient unsuccessfully loaded PersonBuffer. Result: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {p3}, Lcom/google/android/gms/people/i;->w_()V

    throw v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 253
    invoke-static {p1}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 254
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 255
    const-string v0, "PeopleProfileActionGA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unrecognized qualified ID format: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    sget v0, Lcom/google/android/gms/p;->rX:I

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 262
    :goto_0
    return-void

    .line 259
    :cond_0
    invoke-static {p0, p2, v0, p3}, Lcom/google/android/gms/common/util/l;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 261
    invoke-static {p0, v0}, Lcom/google/android/gms/common/util/l;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    goto :goto_0
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 103
    const-string v0, "data_uri"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    .line 104
    new-instance v0, Landroid/support/v4/a/d;

    sget-object v3, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->a:[Ljava/lang/String;

    move-object v1, p0

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/a/d;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 110
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 58
    check-cast p2, Landroid/database/Cursor;

    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const-string v0, "PeopleProfileActionGA"

    const-string v1, "Contact data uri couldn\'t be loaded."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->e(Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/google/android/gms/p;->rX:I

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->finish()V

    :goto_0
    return-void

    :cond_1
    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->k(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "PeopleProfileActionGA"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized qualified ID format: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/people/internal/as;->e(Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/google/android/gms/p;->rX:I

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->finish()V

    goto :goto_0

    :cond_2
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v2, "view"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v1, "PeopleProfileActionGA"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized qualified ID format: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/people/internal/as;->e(Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/google/android/gms/p;->rX:I

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->finish()V

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "http://plus.google.com/"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_4
    const/4 v2, 0x2

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    const-string v0, "PeopleProfileActionGA"

    const-string v1, "No account for profile."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->e(Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/google/android/gms/p;->rX:I

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->finish()V

    goto/16 :goto_0

    :cond_5
    const-string v3, "addtocircle"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v1, p0, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->b:Lcom/google/android/gms/common/api/v;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->d()V

    :cond_6
    new-instance v1, Lcom/google/android/gms/common/api/w;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v3, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    new-instance v4, Lcom/google/android/gms/people/ad;

    invoke-direct {v4}, Lcom/google/android/gms/people/ad;-><init>()V

    const/16 v5, 0x50

    iput v5, v4, Lcom/google/android/gms/people/ad;->a:I

    invoke-virtual {v4}, Lcom/google/android/gms/people/ad;->a()Lcom/google/android/gms/people/ac;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->b:Lcom/google/android/gms/common/api/v;

    iget-object v1, p0, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->b()V

    new-instance v1, Lcom/google/android/gms/people/h;

    invoke-direct {v1}, Lcom/google/android/gms/people/h;-><init>()V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1, v3}, Lcom/google/android/gms/people/h;->a(Ljava/util/Collection;)Lcom/google/android/gms/people/h;

    sget-object v3, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    iget-object v4, p0, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->b:Lcom/google/android/gms/common/api/v;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v2, v5, v1}, Lcom/google/android/gms/people/c;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/h;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    new-instance v3, Lcom/google/android/gms/people/pub/a;

    invoke-direct {v3, p0, v0, v2}, Lcom/google/android/gms/people/pub/a;-><init>(Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v1, v3}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto/16 :goto_0

    :cond_7
    const-string v3, "conversation"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-direct {p0, v0, v2, v4}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->finish()V

    goto/16 :goto_0

    :cond_8
    const-string v3, "hangout"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    invoke-direct {p0, v0, v2, v5}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->a(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->finish()V

    goto/16 :goto_0

    :cond_9
    const-string v0, "PeopleProfileActionGA"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown profile command="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->e(Ljava/lang/String;Ljava/lang/String;)V

    sget v0, Lcom/google/android/gms/p;->rW:I

    invoke-static {p0, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->finish()V

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 80
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 82
    invoke-virtual {p0}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 83
    invoke-virtual {v0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    .line 84
    if-eqz v0, :cond_0

    sget-object v1, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\\d*"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 85
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->finish()V

    .line 93
    :goto_0
    return-void

    .line 89
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 90
    const-string v2, "data_uri"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 91
    invoke-virtual {p0}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->getSupportLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->b:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 246
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_1

    .line 247
    invoke-virtual {p0}, Lcom/google/android/gms/people/pub/PeopleProfileActionGatewayActivity;->finish()V

    .line 249
    :cond_1
    invoke-super {p0}, Landroid/support/v4/app/q;->onPause()V

    .line 250
    return-void
.end method
