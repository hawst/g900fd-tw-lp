.class public Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;
.super Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/c;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(IILjava/lang/String;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>()V

    .line 25
    iput-object p3, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;->j:Ljava/lang/String;

    .line 26
    iput p1, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;->a:I

    .line 27
    iput p2, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;->b:I

    .line 28
    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>(Landroid/os/Parcel;)V

    .line 32
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;->a:I

    .line 33
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;->b:I

    .line 34
    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public static a(Landroid/content/Context;IILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;-><init>(IILjava/lang/String;)V

    .line 40
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 41
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 46
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 51
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 52
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 53
    iget v0, p0, Lcom/google/android/gms/wallet/analytics/events/CreditCardEntrySubmittedEvent;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 54
    return-void
.end method
