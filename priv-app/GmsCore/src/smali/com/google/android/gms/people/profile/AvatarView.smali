.class public Lcom/google/android/gms/people/profile/AvatarView;
.super Landroid/view/View;
.source "SourceFile"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# static fields
.field private static b:Z

.field private static c:I

.field private static d:Z

.field private static e:Landroid/graphics/Paint;

.field private static f:Landroid/graphics/Paint;

.field private static g:I


# instance fields
.field private A:Lcom/google/android/gms/people/profile/j;

.field private B:F

.field private C:F

.field private D:Z

.field private E:F

.field private F:J

.field private G:Lcom/google/android/gms/people/profile/l;

.field private H:Lcom/google/android/gms/people/profile/k;

.field private I:Lcom/google/android/gms/people/profile/i;

.field private J:F

.field private K:Landroid/graphics/RectF;

.field private L:Landroid/graphics/RectF;

.field private M:Landroid/graphics/RectF;

.field private N:[F

.field a:F

.field private h:Landroid/graphics/drawable/Drawable;

.field private i:Landroid/graphics/Matrix;

.field private j:Landroid/graphics/Matrix;

.field private k:Landroid/graphics/Matrix;

.field private l:I

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z

.field private q:Landroid/graphics/Rect;

.field private r:I

.field private s:I

.field private t:Landroid/view/GestureDetector;

.field private u:Landroid/view/ScaleGestureDetector;

.field private v:Landroid/view/View$OnClickListener;

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 167
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 84
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    .line 86
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->k:Landroid/graphics/Matrix;

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->l:I

    .line 96
    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->n:Z

    .line 101
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    .line 122
    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->x:Z

    .line 158
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->K:Landroid/graphics/RectF;

    .line 160
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->L:Landroid/graphics/RectF;

    .line 162
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    .line 164
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->N:[F

    .line 168
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->f()V

    .line 169
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 172
    invoke-direct {p0, p1, p2}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 84
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    .line 86
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->k:Landroid/graphics/Matrix;

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->l:I

    .line 96
    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->n:Z

    .line 101
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    .line 122
    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->x:Z

    .line 158
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->K:Landroid/graphics/RectF;

    .line 160
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->L:Landroid/graphics/RectF;

    .line 162
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    .line 164
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->N:[F

    .line 173
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->f()V

    .line 174
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 177
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 84
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    .line 86
    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->k:Landroid/graphics/Matrix;

    .line 89
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->l:I

    .line 96
    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->n:Z

    .line 101
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    .line 122
    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->x:Z

    .line 158
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->K:Landroid/graphics/RectF;

    .line 160
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->L:Landroid/graphics/RectF;

    .line 162
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    .line 164
    const/16 v0, 0x9

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->N:[F

    .line 178
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->f()V

    .line 179
    return-void
.end method

.method private a(F)V
    .locals 2

    .prologue
    .line 965
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 967
    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    sub-float/2addr v0, p1

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3356bf95    # 5.0E-8f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    .line 970
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->rK:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 976
    :goto_0
    return-void

    .line 973
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->rJ:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a(FFF)V
    .locals 5

    .prologue
    .line 765
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->J:F

    neg-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getWidth()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getHeight()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 768
    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->C:F

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    .line 770
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->d()F

    move-result v1

    .line 771
    div-float v1, v0, v1

    .line 774
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1, v1, p2, p3}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    .line 777
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->e()V

    .line 780
    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->J:F

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    int-to-float v3, v3

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    int-to-float v4, v4

    invoke-virtual {v1, v2, v3, v4}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    .line 782
    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarView;->a(F)V

    .line 783
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    .line 784
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/people/profile/AvatarView;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->e()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/people/profile/AvatarView;F)V
    .locals 3

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->J:F

    add-float/2addr v0, p1

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->J:F

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getWidth()I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    int-to-float v1, v1

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getHeight()I

    move-result v2

    div-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {v0, p1, v1, v2}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/people/profile/AvatarView;FFF)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/people/profile/AvatarView;->a(FFF)V

    return-void
.end method

.method private a(Z)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/high16 v8, 0x41000000    # 8.0f

    const/4 v7, 0x0

    .line 672
    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarView;->a(F)V

    .line 673
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->m:Z

    if-nez v0, :cond_1

    .line 689
    :cond_0
    :goto_0
    return-void

    .line 676
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    .line 677
    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    .line 680
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, v3, v3, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 683
    if-nez p1, :cond_2

    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    cmpl-float v0, v0, v7

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->m:Z

    if-eqz v0, :cond_3

    .line 684
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v3

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    sub-int/2addr v3, v4

    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->K:Landroid/graphics/RectF;

    int-to-float v5, v0

    int-to-float v6, v1

    invoke-virtual {v4, v7, v7, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    int-to-float v1, v1

    int-to-float v0, v0

    div-float v0, v1, v0

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->a:F

    int-to-float v0, v3

    int-to-float v1, v2

    div-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->a:F

    cmpl-float v0, v1, v0

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v1, v2

    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->a:F

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->L:Landroid/graphics/RectF;

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->left:I

    int-to-float v3, v3

    sub-int v4, v0, v1

    int-to-float v4, v4

    iget-object v5, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->right:I

    int-to-float v5, v5

    add-int/2addr v0, v1

    int-to-float v0, v0

    invoke-virtual {v2, v3, v4, v5, v0}, Landroid/graphics/RectF;->set(FFFF)V

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->K:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->L:Landroid/graphics/RectF;

    sget-object v3, Landroid/graphics/Matrix$ScaleToFit;->CENTER:Landroid/graphics/Matrix$ScaleToFit;

    invoke-virtual {v0, v1, v2, v3}, Landroid/graphics/Matrix;->setRectToRect(Landroid/graphics/RectF;Landroid/graphics/RectF;Landroid/graphics/Matrix$ScaleToFit;)Z

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->k:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 685
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->d()F

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    mul-float/2addr v1, v8

    invoke-static {v1, v8}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->C:F

    .line 688
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    goto/16 :goto_0

    .line 684
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    int-to-float v1, v3

    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->a:F

    div-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->L:Landroid/graphics/RectF;

    sub-int v3, v0, v1

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v4, v4, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    add-int/2addr v0, v1

    int-to-float v0, v0

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    invoke-virtual {v2, v3, v4, v0, v1}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_1
.end method

.method private a(FF)Z
    .locals 8

    .prologue
    const/high16 v7, 0x40000000    # 2.0f

    .line 797
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->K:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 798
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 800
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    .line 801
    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    .line 802
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    .line 803
    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->right:F

    .line 806
    iget-boolean v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->n:Z

    if-eqz v4, :cond_0

    .line 808
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->right:F

    sub-float/2addr v0, v2

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v2

    invoke-static {v1, p1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 819
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    .line 820
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    int-to-float v2, v2

    .line 821
    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    .line 822
    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    .line 826
    iget-boolean v5, p0, Lcom/google/android/gms/people/profile/AvatarView;->n:Z

    if-eqz v5, :cond_2

    .line 828
    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->bottom:F

    sub-float/2addr v1, v3

    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v3

    invoke-static {v2, p2}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 840
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 841
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    .line 843
    cmpl-float v0, v0, p1

    if-nez v0, :cond_4

    cmpl-float v0, v1, p2

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_2
    return v0

    .line 812
    :cond_0
    sub-float v4, v3, v2

    sub-float v5, v1, v0

    cmpg-float v4, v4, v5

    if-gez v4, :cond_1

    .line 813
    sub-float/2addr v1, v0

    add-float/2addr v2, v3

    sub-float/2addr v1, v2

    div-float/2addr v1, v7

    add-float/2addr v0, v1

    goto :goto_0

    .line 815
    :cond_1
    sub-float/2addr v1, v3

    sub-float/2addr v0, v2

    invoke-static {v0, p1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {v1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_0

    .line 832
    :cond_2
    sub-float v5, v4, v3

    sub-float v6, v2, v1

    cmpg-float v5, v5, v6

    if-gez v5, :cond_3

    .line 833
    sub-float/2addr v2, v1

    add-float/2addr v3, v4

    sub-float/2addr v2, v3

    div-float/2addr v2, v7

    add-float/2addr v1, v2

    goto :goto_1

    .line 835
    :cond_3
    sub-float/2addr v2, v4

    sub-float/2addr v1, v3

    invoke-static {v1, p2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    goto :goto_1

    .line 843
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/gms/people/profile/AvatarView;FF)Z
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/people/profile/AvatarView;->a(FF)Z

    move-result v0

    return v0
.end method

.method private b()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 363
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    .line 366
    :cond_0
    iput-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    .line 367
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 470
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->k:Landroid/graphics/Matrix;

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->set(Landroid/graphics/Matrix;)V

    .line 476
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    .line 477
    return-void
.end method

.method private d()F
    .locals 2

    .prologue
    .line 737
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->N:[F

    invoke-virtual {v0, v1}, Landroid/graphics/Matrix;->getValues([F)V

    .line 738
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->N:[F

    const/4 v1, 0x0

    aget v0, v0, v1

    return v0
.end method

.method private e()V
    .locals 10

    .prologue
    const/high16 v9, 0x41a00000    # 20.0f

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v1, 0x0

    .line 850
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->K:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    .line 851
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 854
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    .line 855
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    int-to-float v2, v2

    .line 856
    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    .line 857
    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    .line 860
    sub-float v5, v4, v3

    sub-float v6, v2, v0

    cmpg-float v5, v5, v6

    if-gez v5, :cond_2

    .line 862
    sub-float/2addr v2, v0

    add-float/2addr v3, v4

    sub-float/2addr v2, v3

    div-float/2addr v2, v8

    add-float/2addr v0, v2

    .line 874
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    int-to-float v2, v2

    .line 875
    iget-object v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    int-to-float v3, v3

    .line 876
    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    .line 877
    iget-object v5, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    .line 880
    sub-float v6, v5, v4

    sub-float v7, v3, v2

    cmpg-float v6, v6, v7

    if-gez v6, :cond_5

    .line 882
    sub-float v1, v3, v2

    add-float v3, v5, v4

    sub-float/2addr v1, v3

    div-float/2addr v1, v8

    add-float/2addr v1, v2

    .line 893
    :cond_0
    :goto_1
    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v9

    if-gtz v2, :cond_1

    invoke-static {v1}, Ljava/lang/Math;->abs(F)F

    move-result v2

    cmpl-float v2, v2, v9

    if-lez v2, :cond_7

    .line 894
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->H:Lcom/google/android/gms/people/profile/k;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/people/profile/k;->a(FF)Z

    .line 899
    :goto_2
    return-void

    .line 863
    :cond_2
    cmpl-float v5, v3, v0

    if-lez v5, :cond_3

    .line 865
    sub-float/2addr v0, v3

    goto :goto_0

    .line 866
    :cond_3
    cmpg-float v0, v4, v2

    if-gez v0, :cond_4

    .line 868
    sub-float v0, v2, v4

    goto :goto_0

    :cond_4
    move v0, v1

    .line 870
    goto :goto_0

    .line 883
    :cond_5
    cmpl-float v6, v4, v2

    if-lez v6, :cond_6

    .line 885
    sub-float v1, v2, v4

    goto :goto_1

    .line 886
    :cond_6
    cmpg-float v2, v5, v3

    if-gez v2, :cond_0

    .line 888
    sub-float v1, v3, v5

    goto :goto_1

    .line 896
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->j:Landroid/graphics/Matrix;

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 897
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    goto :goto_2
.end method

.method private f()V
    .locals 5

    .prologue
    const/4 v0, 0x1

    .line 921
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 923
    sget-boolean v2, Lcom/google/android/gms/people/profile/AvatarView;->b:Z

    if-nez v2, :cond_0

    .line 924
    sput-boolean v0, Lcom/google/android/gms/people/profile/AvatarView;->b:Z

    .line 926
    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 928
    sget v3, Lcom/google/android/gms/f;->Q:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    sput v3, Lcom/google/android/gms/people/profile/AvatarView;->g:I

    .line 930
    sget v3, Lcom/google/android/gms/g;->bf:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    sput v3, Lcom/google/android/gms/people/profile/AvatarView;->c:I

    .line 933
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 934
    sput-object v3, Lcom/google/android/gms/people/profile/AvatarView;->e:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 935
    sget-object v3, Lcom/google/android/gms/people/profile/AvatarView;->e:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/gms/f;->R:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 936
    sget-object v3, Lcom/google/android/gms/people/profile/AvatarView;->e:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 938
    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3}, Landroid/graphics/Paint;-><init>()V

    .line 939
    sput-object v3, Lcom/google/android/gms/people/profile/AvatarView;->f:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 940
    sget-object v3, Lcom/google/android/gms/people/profile/AvatarView;->f:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/gms/f;->S:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 941
    sget-object v3, Lcom/google/android/gms/people/profile/AvatarView;->f:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 942
    sget-object v3, Lcom/google/android/gms/people/profile/AvatarView;->f:Landroid/graphics/Paint;

    sget v4, Lcom/google/android/gms/g;->bg:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {v3, v2}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 945
    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "android.hardware.touchscreen.multitouch.distinct"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    sput-boolean v2, Lcom/google/android/gms/people/profile/AvatarView;->d:Z

    .line 949
    :cond_0
    new-instance v2, Landroid/view/GestureDetector;

    const/4 v3, 0x0

    sget-boolean v4, Lcom/google/android/gms/people/profile/AvatarView;->d:Z

    if-nez v4, :cond_1

    :goto_0
    invoke-direct {v2, v1, p0, v3, v0}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;Landroid/os/Handler;Z)V

    iput-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->t:Landroid/view/GestureDetector;

    .line 950
    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-direct {v0, v1, p0}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->u:Landroid/view/ScaleGestureDetector;

    .line 951
    new-instance v0, Lcom/google/android/gms/people/profile/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/profile/j;-><init>(Lcom/google/android/gms/people/profile/AvatarView;)V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->A:Lcom/google/android/gms/people/profile/j;

    .line 952
    new-instance v0, Lcom/google/android/gms/people/profile/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/profile/l;-><init>(Lcom/google/android/gms/people/profile/AvatarView;)V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->G:Lcom/google/android/gms/people/profile/l;

    .line 953
    new-instance v0, Lcom/google/android/gms/people/profile/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/profile/k;-><init>(Lcom/google/android/gms/people/profile/AvatarView;)V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->H:Lcom/google/android/gms/people/profile/k;

    .line 954
    new-instance v0, Lcom/google/android/gms/people/profile/i;

    invoke-direct {v0, p0}, Lcom/google/android/gms/people/profile/i;-><init>(Lcom/google/android/gms/people/profile/AvatarView;)V

    iput-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->I:Lcom/google/android/gms/people/profile/i;

    .line 956
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->rJ:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/profile/AvatarView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 958
    return-void

    .line 949
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 662
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    .line 663
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-nez v0, :cond_0

    .line 664
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->c()V

    .line 666
    :cond_0
    return-void
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 7

    .prologue
    const/16 v4, 0x100

    .line 427
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->right:I

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v0, v1

    .line 430
    const/high16 v1, 0x43800000    # 256.0f

    int-to-float v0, v0

    div-float v0, v1, v0

    .line 431
    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    neg-int v1, v1

    .line 433
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->top:I

    neg-int v2, v2

    .line 436
    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v4, v4, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 438
    new-instance v4, Landroid/graphics/Canvas;

    invoke-direct {v4, v3}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    .line 439
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    sget v6, Lcom/google/android/gms/f;->Q:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 442
    new-instance v5, Landroid/graphics/Matrix;

    iget-object v6, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    invoke-direct {v5, v6}, Landroid/graphics/Matrix;-><init>(Landroid/graphics/Matrix;)V

    .line 443
    add-int v6, v1, v2

    if-eqz v6, :cond_0

    .line 444
    int-to-float v1, v1

    int-to-float v2, v2

    invoke-virtual {v5, v1, v2}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    .line 446
    :cond_0
    add-float v1, v0, v0

    const/4 v2, 0x0

    cmpl-float v1, v1, v2

    if-eqz v1, :cond_1

    .line 447
    invoke-virtual {v5, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    .line 449
    :cond_1
    invoke-virtual {v4, v5}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 452
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 455
    const/4 v2, 0x0

    .line 457
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 458
    :try_start_1
    sget-object v0, Lcom/google/android/gms/people/a/a;->R:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 459
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->JPEG:Landroid/graphics/Bitmap$CompressFormat;

    invoke-virtual {v3, v2, v0, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 461
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 462
    return-void

    .line 461
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_0
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 630
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->b()V

    .line 631
    sget-object v0, Lcom/google/android/gms/people/a/a;->Q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 632
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, p1, v0, v0}, Lcom/google/android/gms/common/util/ac;->a(Landroid/content/Context;Landroid/net/Uri;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 634
    if-nez v2, :cond_0

    .line 635
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No bitmap loaded from "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 637
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    instance-of v0, v0, Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eq v2, v0, :cond_4

    :cond_1
    if-eqz v2, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    if-eq v0, v3, :cond_5

    :cond_2
    const/4 v0, 0x1

    :goto_0
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->b()V

    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-nez v1, :cond_3

    if-eqz v2, :cond_3

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v1, v3, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    iget-object v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, p0}, Landroid/graphics/drawable/Drawable;->setCallback(Landroid/graphics/drawable/Drawable$Callback;)V

    :cond_3
    invoke-direct {p0, v0}, Lcom/google/android/gms/people/profile/AvatarView;->a(Z)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    .line 638
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->requestLayout()V

    .line 639
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    .line 640
    return-void

    :cond_5
    move v0, v1

    .line 637
    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_1
.end method

.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    .prologue
    .line 533
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    .line 541
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->invalidate()V

    .line 545
    :goto_0
    return-void

    .line 543
    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public jumpDrawablesToCurrentState()V
    .locals 1
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    .line 525
    invoke-super {p0}, Landroid/view/View;->jumpDrawablesToCurrentState()V

    .line 526
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 527
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->jumpToCurrentState()V

    .line 529
    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 495
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 496
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 497
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 499
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 497
    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 503
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 504
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 505
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 507
    :cond_0
    return-void
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 5

    .prologue
    .line 218
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->x:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-eqz v0, :cond_1

    .line 219
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->y:Z

    if-nez v0, :cond_0

    .line 220
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->d()F

    move-result v0

    .line 221
    const/high16 v1, 0x3fc00000    # 1.5f

    mul-float/2addr v1, v0

    .line 224
    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->B:F

    invoke-static {v2, v1}, Ljava/lang/Math;->max(FF)F

    move-result v1

    .line 225
    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->C:F

    invoke-static {v2, v1}, Ljava/lang/Math;->min(FF)F

    move-result v1

    .line 227
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->A:Lcom/google/android/gms/people/profile/j;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v4

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/google/android/gms/people/profile/j;->a(FFFF)Z

    .line 229
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->y:Z

    .line 231
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 236
    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 277
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-eqz v0, :cond_0

    .line 278
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->G:Lcom/google/android/gms/people/profile/l;

    invoke-virtual {v0}, Lcom/google/android/gms/people/profile/l;->a()V

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->H:Lcom/google/android/gms/people/profile/k;

    invoke-virtual {v0}, Lcom/google/android/gms/people/profile/k;->a()V

    .line 281
    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 549
    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    .line 551
    sget v0, Lcom/google/android/gms/people/profile/AvatarView;->g:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 554
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_0

    .line 584
    :goto_0
    return-void

    .line 557
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v0

    .line 558
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 560
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    if-eqz v2, :cond_1

    .line 561
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 563
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 564
    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 567
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    .line 568
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    if-eqz v0, :cond_2

    .line 569
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->M:Landroid/graphics/RectF;

    invoke-virtual {v0, v2}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    .line 572
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Canvas;->getSaveCount()I

    move-result v6

    .line 573
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getHeight()I

    move-result v0

    int-to-float v4, v0

    sget-object v5, Lcom/google/android/gms/people/profile/AvatarView;->e:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    .line 574
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    .line 575
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/Rect;)Z

    .line 577
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    if-eqz v0, :cond_3

    .line 578
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->i:Landroid/graphics/Matrix;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->concat(Landroid/graphics/Matrix;)V

    .line 581
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 582
    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 583
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/gms/people/profile/AvatarView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    .prologue
    .line 286
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-eqz v0, :cond_2

    .line 287
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->o:Z

    if-eqz v0, :cond_0

    .line 288
    const/4 p3, 0x0

    .line 290
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->D:Z

    if-nez v0, :cond_1

    .line 291
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->G:Lcom/google/android/gms/people/profile/l;

    invoke-virtual {v0, p3, p4}, Lcom/google/android/gms/people/profile/l;->a(FF)Z

    .line 293
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->D:Z

    .line 295
    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 5

    .prologue
    .line 588
    invoke-super/range {p0 .. p5}, Landroid/view/View;->onLayout(ZIIII)V

    .line 589
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->m:Z

    .line 590
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getWidth()I

    move-result v0

    .line 591
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getHeight()I

    move-result v1

    .line 594
    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 596
    sget v3, Lcom/google/android/gms/people/profile/AvatarView;->c:I

    .line 598
    invoke-static {v3, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->r:I

    .line 601
    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->r:I

    int-to-float v2, v2

    const/high16 v3, 0x3f800000    # 1.0f

    div-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->s:I

    .line 603
    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->r:I

    sub-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x2

    .line 604
    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->s:I

    sub-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    .line 605
    iget v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->r:I

    add-int/2addr v2, v0

    .line 606
    iget v3, p0, Lcom/google/android/gms/people/profile/AvatarView;->s:I

    add-int/2addr v3, v1

    .line 610
    iget-object v4, p0, Lcom/google/android/gms/people/profile/AvatarView;->q:Landroid/graphics/Rect;

    invoke-virtual {v4, v0, v1, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 612
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/profile/AvatarView;->a(Z)V

    .line 613
    return-void
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 255
    return-void
.end method

.method protected onMeasure(II)V
    .locals 2

    .prologue
    .line 617
    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->l:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 618
    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->l:I

    const/high16 v1, -0x80000000

    invoke-static {v0, v1}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, p1, v0}, Landroid/view/View;->onMeasure(II)V

    .line 620
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/AvatarView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->l:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarView;->setMeasuredDimension(II)V

    .line 624
    :goto_0
    return-void

    .line 622
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    goto :goto_0
.end method

.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 300
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->p:Z

    if-eqz v0, :cond_1

    .line 315
    :cond_0
    :goto_0
    return v3

    .line 303
    :cond_1
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v0

    const/high16 v1, 0x3f800000    # 1.0f

    sub-float/2addr v0, v1

    .line 305
    cmpg-float v1, v0, v2

    if-gez v1, :cond_2

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->E:F

    cmpl-float v1, v1, v2

    if-gtz v1, :cond_3

    :cond_2
    cmpl-float v1, v0, v2

    if-lez v1, :cond_4

    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->E:F

    cmpg-float v1, v1, v2

    if-gez v1, :cond_4

    .line 306
    :cond_3
    iput v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->E:F

    .line 308
    :cond_4
    iget v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->E:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->E:F

    .line 309
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->E:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    const v1, 0x3d23d70a    # 0.04f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    .line 310
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->z:Z

    .line 311
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->d()F

    move-result v0

    .line 312
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v1

    mul-float/2addr v0, v1

    .line 313
    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/people/profile/AvatarView;->a(FFF)V

    goto :goto_0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 320
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-eqz v0, :cond_0

    .line 321
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->A:Lcom/google/android/gms/people/profile/j;

    invoke-virtual {v0}, Lcom/google/android/gms/people/profile/j;->a()V

    .line 322
    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->z:Z

    .line 323
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->E:F

    .line 325
    :cond_0
    return v1
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 330
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->z:Z

    if-eqz v0, :cond_0

    .line 331
    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->y:Z

    .line 332
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->c()V

    .line 334
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/AvatarView;->D:Z

    .line 335
    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    .prologue
    .line 263
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->F:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->abs(J)J

    move-result-wide v0

    .line 265
    iget-boolean v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->o:Z

    if-eqz v2, :cond_0

    .line 266
    const/4 p3, 0x0

    .line 268
    :cond_0
    const-wide/16 v2, 0x190

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->w:Z

    if-eqz v0, :cond_1

    .line 270
    neg-float v0, p3

    neg-float v1, p4

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/people/profile/AvatarView;->a(FF)Z

    .line 272
    :cond_1
    const/4 v0, 0x1

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    .prologue
    .line 259
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->v:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->z:Z

    if-nez v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->v:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p0}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    .line 244
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->z:Z

    .line 245
    const/4 v0, 0x1

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 250
    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->u:Landroid/view/ScaleGestureDetector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->t:Landroid/view/GestureDetector;

    if-nez v0, :cond_1

    .line 213
    :cond_0
    :goto_0
    return v2

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->u:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->t:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    .line 191
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    .line 193
    packed-switch v0, :pswitch_data_0

    .line 202
    :cond_2
    :goto_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    .line 204
    packed-switch v0, :pswitch_data_1

    :pswitch_0
    goto :goto_0

    .line 207
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->G:Lcom/google/android/gms/people/profile/l;

    invoke-static {v0}, Lcom/google/android/gms/people/profile/l;->a(Lcom/google/android/gms/people/profile/l;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 208
    invoke-direct {p0}, Lcom/google/android/gms/people/profile/AvatarView;->e()V

    goto :goto_0

    .line 195
    :pswitch_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_3

    .line 196
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->F:J

    goto :goto_1

    .line 197
    :cond_3
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v0

    if-ne v0, v2, :cond_2

    .line 198
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->F:J

    goto :goto_1

    .line 193
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_2
    .end packed-switch

    .line 204
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    .prologue
    .line 339
    iput-object p1, p0, Lcom/google/android/gms/people/profile/AvatarView;->v:Landroid/view/View$OnClickListener;

    .line 340
    return-void
.end method

.method public setVisibility(I)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 511
    invoke-super {p0, p1}, Landroid/view/View;->setVisibility(I)V

    .line 512
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    .line 513
    iget-object v2, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/graphics/drawable/Drawable;->setVisible(ZZ)Z

    .line 515
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 513
    goto :goto_0
.end method

.method protected verifyDrawable(Landroid/graphics/drawable/Drawable;)Z
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/android/gms/people/profile/AvatarView;->h:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    invoke-super {p0, p1}, Landroid/view/View;->verifyDrawable(Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
