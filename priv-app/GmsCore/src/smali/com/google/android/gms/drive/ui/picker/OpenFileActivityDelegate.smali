.class public Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;
.super Lcom/google/android/gms/drive/ui/a;
.source "SourceFile"


# instance fields
.field private b:Lcom/google/android/gms/drive/ui/picker/j;

.field private c:Lcom/google/android/gms/drive/ui/picker/m;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/a;-><init>()V

    .line 135
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)Lcom/google/android/gms/drive/ui/picker/i;
    .locals 8

    .prologue
    .line 126
    new-instance v1, Lcom/google/android/gms/drive/ui/picker/i;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/drive/ui/picker/i;-><init>(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;)V

    return-object v1
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;)Lcom/google/android/gms/drive/ui/picker/m;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->c:Lcom/google/android/gms/drive/ui/picker/m;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->c:Lcom/google/android/gms/drive/ui/picker/m;

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->c:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/m;->c()V

    .line 353
    :cond_0
    return-void
.end method

.method protected final d()V
    .locals 5

    .prologue
    .line 325
    const-string v0, "OpenFileActivityDelegate"

    const-string v1, "onClientConnected hasPendingOperations=%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->b:Lcom/google/android/gms/drive/ui/picker/j;

    invoke-virtual {v4}, Lcom/google/android/gms/drive/ui/picker/j;->a()Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->c:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/m;->a()V

    .line 328
    return-void
.end method

.method public final e()Lcom/google/android/gms/drive/ui/picker/j;
    .locals 1

    .prologue
    .line 359
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->b:Lcom/google/android/gms/drive/ui/picker/j;

    return-object v0
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->c:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/m;->b()V

    .line 320
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/a;->onBackPressed()V

    .line 321
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 313
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/a;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 314
    invoke-static {p0}, Lcom/google/android/gms/drive/ui/p;->a(Landroid/app/Activity;)V

    .line 315
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 257
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/a;->onCreate(Landroid/os/Bundle;)V

    .line 258
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 260
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/j;

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, v2, v4}, Lcom/google/android/gms/drive/ui/picker/j;-><init>(Landroid/os/Handler;B)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->b:Lcom/google/android/gms/drive/ui/picker/j;

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->b:Lcom/google/android/gms/drive/ui/picker/j;

    new-instance v2, Lcom/google/android/gms/drive/ui/picker/h;

    invoke-direct {v2, p0}, Lcom/google/android/gms/drive/ui/picker/h;-><init>(Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;)V

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/ui/picker/j;->a(Lcom/google/android/gms/drive/ui/picker/j;Ljava/lang/Runnable;)V

    .line 268
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    sget v2, Lcom/google/android/gms/l;->an:I

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setCustomView(I)V

    invoke-virtual {v0, v5}, Landroid/app/ActionBar;->setDisplayShowCustomEnabled(Z)V

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    invoke-virtual {v0, v4}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    .line 270
    :cond_0
    const-string v0, "dialogTitle"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 271
    if-nez v0, :cond_1

    .line 272
    sget v0, Lcom/google/android/gms/p;->gI:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 273
    const-string v2, "dialogTitle"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 275
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->setTitle(Ljava/lang/CharSequence;)V

    .line 277
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    .line 278
    const-string v0, "OpenFileActivityDelegate"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/m;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->c:Lcom/google/android/gms/drive/ui/picker/m;

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->c:Lcom/google/android/gms/drive/ui/picker/m;

    if-nez v0, :cond_3

    .line 281
    new-instance v2, Lcom/google/android/gms/drive/ui/picker/m;

    invoke-direct {v2}, Lcom/google/android/gms/drive/ui/picker/m;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_2
    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/ui/picker/m;->setArguments(Landroid/os/Bundle;)V

    iput-object v2, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->c:Lcom/google/android/gms/drive/ui/picker/m;

    .line 283
    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    const v1, 0x1020002

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->c:Lcom/google/android/gms/drive/ui/picker/m;

    const-string v3, "OpenFileActivityDelegate"

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 289
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->c:Lcom/google/android/gms/drive/ui/picker/m;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/drive/ui/picker/m;->setHasOptionsMenu(Z)V

    .line 291
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->c:Lcom/google/android/gms/drive/ui/picker/m;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->a:Lcom/google/android/gms/common/api/v;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/common/api/v;)V

    .line 293
    if-nez p1, :cond_4

    .line 295
    invoke-virtual {p0, v4}, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->setResult(I)V

    .line 297
    :cond_4
    invoke-static {p0}, Lcom/google/android/gms/drive/ui/p;->a(Landroid/app/Activity;)V

    .line 298
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 332
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/a;->onPause()V

    .line 335
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->b:Lcom/google/android/gms/drive/ui/picker/j;

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/j;->b(Lcom/google/android/gms/drive/ui/picker/j;)V

    .line 336
    return-void
.end method
