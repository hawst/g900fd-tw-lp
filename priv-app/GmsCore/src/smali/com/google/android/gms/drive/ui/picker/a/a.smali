.class public abstract Lcom/google/android/gms/drive/ui/picker/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/ui/picker/a/n;


# instance fields
.field protected final a:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

.field protected final b:Landroid/widget/ListView;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/drive/ui/picker/view/DocListView;Landroid/widget/ListView;)V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a;->a:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    .line 23
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a;->b:Landroid/widget/ListView;

    .line 24
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/a;->b:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setSelection(I)V

    .line 44
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/ui/picker/a/l;)V
    .locals 1

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/a/a;->e()Lcom/google/android/gms/drive/ui/picker/a/r;

    move-result-object v0

    .line 49
    if-eqz v0, :cond_0

    .line 50
    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/ui/picker/a/r;->a(Lcom/google/android/gms/drive/ui/picker/a/l;)V

    .line 52
    :cond_0
    return-void
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/a/a;->e()Lcom/google/android/gms/drive/ui/picker/a/r;

    move-result-object v0

    .line 57
    if-eqz v0, :cond_0

    .line 58
    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/drive/ui/picker/a/r;->a(ZLjava/lang/String;)V

    .line 60
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/a/a;->e()Lcom/google/android/gms/drive/ui/picker/a/r;

    move-result-object v0

    .line 65
    if-eqz v0, :cond_0

    .line 66
    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/r;->a()V

    .line 68
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/a/a;->d()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/a/a;->e()Lcom/google/android/gms/drive/ui/picker/a/r;

    move-result-object v0

    .line 78
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/r;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method protected abstract e()Lcom/google/android/gms/drive/ui/picker/a/r;
.end method
