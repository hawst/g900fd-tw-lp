.class public final Lcom/google/android/gms/drive/metadata/sync/a/b;
.super Lcom/google/android/gms/drive/metadata/sync/a/c;
.source "SourceFile"


# instance fields
.field public final a:J

.field public final b:Ljava/util/Set;


# direct methods
.method public constructor <init>(JLjava/util/Set;)V
    .locals 3

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/gms/drive/metadata/sync/a/e;->a:Lcom/google/android/gms/drive/metadata/sync/a/e;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/metadata/sync/a/c;-><init>(Lcom/google/android/gms/drive/metadata/sync/a/e;)V

    .line 29
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 30
    iput-wide p1, p0, Lcom/google/android/gms/drive/metadata/sync/a/b;->a:J

    .line 31
    iput-object p3, p0, Lcom/google/android/gms/drive/metadata/sync/a/b;->b:Ljava/util/Set;

    .line 32
    return-void

    .line 29
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/a/b;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 35
    const-string v0, ":"

    invoke-static {p0, v0}, Landroid/text/TextUtils;->split(Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 38
    array-length v2, v0

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    .line 40
    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 41
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 56
    :cond_0
    new-instance v1, Lcom/google/android/gms/drive/metadata/sync/a/b;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/drive/metadata/sync/a/b;-><init>(JLjava/util/Set;)V

    return-object v1

    .line 44
    :cond_1
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p0}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 45
    const-string v2, "changeStamp"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 46
    const-string v4, "sortedAppIds"

    invoke-virtual {v0, v4}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 47
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 48
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v5

    if-ge v1, v5, :cond_0

    .line 49
    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 48
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 51
    :catch_0
    move-exception v0

    .line 53
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "JSON encoding failed"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    .line 89
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 91
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/a/b;->b:Ljava/util/Set;

    invoke-direct {v1, v2}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    .line 92
    const-string v2, "sortedAppIds"

    invoke-virtual {v0, v2, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 93
    const-string v1, "changeStamp"

    iget-wide v2, p0, Lcom/google/android/gms/drive/metadata/sync/a/b;->a:J

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 94
    :catch_0
    move-exception v0

    .line 96
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "JSON encoding failed"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 69
    if-ne p0, p1, :cond_1

    .line 70
    const/4 v0, 0x1

    .line 79
    :cond_0
    :goto_0
    return v0

    .line 72
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/drive/metadata/sync/a/b;

    if-eqz v1, :cond_0

    .line 75
    check-cast p1, Lcom/google/android/gms/drive/metadata/sync/a/b;

    .line 76
    iget-wide v2, p0, Lcom/google/android/gms/drive/metadata/sync/a/b;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/metadata/sync/a/b;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/b;->b:Ljava/util/Set;

    iget-object v1, p1, Lcom/google/android/gms/drive/metadata/sync/a/b;->b:Ljava/util/Set;

    invoke-interface {v0, v1}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 84
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/gms/drive/metadata/sync/a/b;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/a/b;->b:Ljava/util/Set;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
