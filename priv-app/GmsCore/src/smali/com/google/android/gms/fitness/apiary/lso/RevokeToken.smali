.class public final Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/fitness/apiary/lso/g;

.field private static final d:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/fitness/apiary/lso/g;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/apiary/lso/g;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->CREATOR:Lcom/google/android/gms/fitness/apiary/lso/g;

    .line 33
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 36
    sput-object v0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->d:Ljava/util/HashMap;

    const-string v1, "version_info"

    const-string v2, "version_info"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 37
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 63
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->b:I

    .line 64
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->a:Ljava/util/Set;

    .line 65
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 73
    iput-object p1, p0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->a:Ljava/util/Set;

    .line 74
    iput p2, p0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->b:I

    .line 75
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->c:Ljava/lang/String;

    .line 76
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->d:Ljava/util/HashMap;

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 147
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 148
    packed-switch v0, :pswitch_data_0

    .line 153
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 150
    :pswitch_0
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->c:Ljava/lang/String;

    .line 156
    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 157
    return-void

    .line 148
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 123
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 127
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->c:Ljava/lang/String;

    return-object v0

    .line 123
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->CREATOR:Lcom/google/android/gms/fitness/apiary/lso/g;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 174
    instance-of v0, p1, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;

    if-nez v0, :cond_0

    move v0, v1

    .line 205
    :goto_0
    return v0

    .line 179
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 180
    goto :goto_0

    .line 183
    :cond_1
    check-cast p1, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;

    .line 184
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 185
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 186
    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 188
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 190
    goto :goto_0

    :cond_3
    move v0, v1

    .line 195
    goto :goto_0

    .line 198
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 200
    goto :goto_0

    :cond_5
    move v0, v2

    .line 205
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 161
    const/4 v0, 0x0

    .line 162
    sget-object v1, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->d:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 163
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 164
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 165
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 167
    goto :goto_0

    .line 168
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 113
    sget-object v0, Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;->CREATOR:Lcom/google/android/gms/fitness/apiary/lso/g;

    invoke-static {p0, p1}, Lcom/google/android/gms/fitness/apiary/lso/g;->a(Lcom/google/android/gms/fitness/apiary/lso/RevokeToken;Landroid/os/Parcel;)V

    .line 114
    return-void
.end method
