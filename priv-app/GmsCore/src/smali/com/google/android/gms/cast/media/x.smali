.class final Lcom/google/android/gms/cast/media/x;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/content/Intent;

.field public b:Landroid/support/v7/c/w;

.field public c:J

.field final synthetic d:Lcom/google/android/gms/cast/media/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/media/a;Landroid/content/Intent;Landroid/support/v7/c/w;)V
    .locals 2

    .prologue
    .line 2531
    iput-object p1, p0, Lcom/google/android/gms/cast/media/x;->d:Lcom/google/android/gms/cast/media/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2532
    iput-object p2, p0, Lcom/google/android/gms/cast/media/x;->a:Landroid/content/Intent;

    .line 2533
    iput-object p3, p0, Lcom/google/android/gms/cast/media/x;->b:Landroid/support/v7/c/w;

    .line 2534
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/cast/media/x;->c:J

    .line 2535
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 4

    .prologue
    .line 2543
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 2544
    const-string v0, "com.google.android.gms.cast.EXTRA_ERROR_CODE"

    invoke-virtual {v1, v0, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 2545
    iget-object v2, p0, Lcom/google/android/gms/cast/media/x;->b:Landroid/support/v7/c/w;

    iget-object v0, p0, Lcom/google/android/gms/cast/media/x;->d:Lcom/google/android/gms/cast/media/a;

    invoke-static {v0}, Lcom/google/android/gms/cast/media/a;->j(Lcom/google/android/gms/cast/media/a;)Ljava/util/Map;

    move-result-object v0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0, v1}, Landroid/support/v7/c/w;->a(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 2546
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 2538
    invoke-static {}, Lcom/google/android/gms/cast/media/a;->b()Lcom/google/android/gms/cast/e/h;

    move-result-object v0

    const-string v1, "result bundle: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 2539
    iget-object v0, p0, Lcom/google/android/gms/cast/media/x;->b:Landroid/support/v7/c/w;

    invoke-virtual {v0, p1}, Landroid/support/v7/c/w;->a(Landroid/os/Bundle;)V

    .line 2540
    return-void
.end method
