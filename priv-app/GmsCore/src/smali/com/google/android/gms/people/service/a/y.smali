.class public final Lcom/google/android/gms/people/service/a/y;
.super Lcom/google/android/gms/people/service/a/b;
.source "SourceFile"


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/lang/String;

.field private final k:I

.field private final l:Ljava/lang/String;

.field private final m:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 7

    .prologue
    .line 66
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/people/service/a/b;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;)V

    .line 68
    iput-object p3, p0, Lcom/google/android/gms/people/service/a/y;->g:Ljava/lang/String;

    .line 69
    iput-object p6, p0, Lcom/google/android/gms/people/service/a/y;->h:Ljava/lang/String;

    .line 70
    iput-object p7, p0, Lcom/google/android/gms/people/service/a/y;->i:Ljava/lang/String;

    .line 71
    iput-object p8, p0, Lcom/google/android/gms/people/service/a/y;->j:Ljava/lang/String;

    .line 72
    move/from16 v0, p9

    iput v0, p0, Lcom/google/android/gms/people/service/a/y;->k:I

    .line 73
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/y;->l:Ljava/lang/String;

    .line 74
    move/from16 v0, p11

    iput-boolean v0, p0, Lcom/google/android/gms/people/service/a/y;->m:Z

    .line 75
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 12

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 165
    invoke-static {p0}, Lcom/google/android/gms/people/service/e;->a(Landroid/content/Context;)Lcom/google/android/gms/people/service/e;

    move-result-object v5

    .line 166
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/gms/people/service/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/people/e/a;

    move-result-object v6

    .line 169
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    move-object v0, v2

    .line 172
    :cond_0
    :try_start_0
    invoke-virtual {v5, p0, v6, v0}, Lcom/google/android/gms/people/service/e;->a(Landroid/content/Context;Lcom/google/android/gms/people/e/a;Ljava/lang/String;)Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;

    move-result-object v7

    .line 174
    invoke-virtual {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;->c()Ljava/util/List;

    move-result-object v8

    .line 175
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    move v4, v3

    .line 176
    :goto_0
    if-ge v4, v9, :cond_5

    .line 177
    invoke-interface {v8, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;

    .line 178
    const-string v10, "circle"

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->b()Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    move-result-object v11

    invoke-interface {v11}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->f()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_1

    .line 179
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->c()Ljava/lang/String;

    move-result-object v10

    .line 180
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/Audience;->b()Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/v1whitelisted/models/fa;->e()Ljava/lang/String;

    move-result-object v11

    .line 181
    const-string v0, "limited"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    :goto_1
    invoke-virtual {v1, v11, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 176
    :cond_1
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 181
    :cond_2
    const-string v0, "public"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    const-string v0, "private"

    invoke-virtual {v0, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    goto :goto_1

    :cond_4
    move v0, v3

    goto :goto_1

    .line 184
    :cond_5
    invoke-virtual {v7}, Lcom/google/android/gms/plus/service/v1whitelisted/models/AudiencesFeed;->d()Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 192
    if-nez v0, :cond_0

    move-object v0, v1

    .line 193
    :goto_2
    return-object v0

    .line 185
    :catch_0
    move-exception v0

    .line 186
    const-string v1, "PeopleService"

    const-string v3, "Authentication error"

    invoke-static {v1, v3, v0}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v0, v2

    .line 187
    goto :goto_2

    .line 188
    :catch_1
    move-exception v0

    .line 189
    const-string v1, "PeopleService"

    const-string v3, "Network error"

    invoke-static {v1, v3, v0}, Lcom/google/android/gms/people/f/n;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Exception;)V

    move-object v0, v2

    .line 190
    goto :goto_2
.end method


# virtual methods
.method public final b(Landroid/content/Context;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 14

    .prologue
    const/4 v13, 0x0

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/y;->g:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/y;->h:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/y;->i:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/y;->j:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/people/service/a/y;->k:I

    iget-object v5, p0, Lcom/google/android/gms/people/service/a/y;->l:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/gms/people/service/a/y;->m:Z

    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    const-string v8, "account"

    invoke-virtual {v7, v8, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "pagegaiaid"

    invoke-virtual {v7, v8, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v8, "localized_group_names"

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    new-instance v10, Landroid/os/Bundle;

    invoke-direct {v10}, Landroid/os/Bundle;-><init>()V

    const-string v11, "1"

    sget v12, Lcom/google/android/gms/p;->em:I

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "3"

    sget v12, Lcom/google/android/gms/p;->en:I

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v11, "4"

    sget v12, Lcom/google/android/gms/p;->ek:I

    invoke-virtual {v9, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v11, v9}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8, v10}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {p1, v1, v2, v7}, Lcom/google/android/gms/people/service/a/af;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)V

    if-eqz v6, :cond_0

    invoke-static {p1, v1, v2, v0}, Lcom/google/android/gms/people/service/a/y;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v6, "circlevisibility"

    invoke-virtual {v7, v6, v0}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    move-result-object v6

    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/c/b;->a(Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/String;

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v13

    const/4 v0, 0x1

    invoke-static {v3}, Lcom/google/android/gms/people/internal/at;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v5}, Lcom/google/android/gms/people/internal/at;->l(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string v0, ""

    :goto_0
    aput-object v0, v2, v1

    const-string v0, "SELECT _id,circle_id,name,sort_key,people_count,type,client_policies,last_modified,sync_to_contacts,for_sharing FROM circles WHERE (owner_id = ?1)AND ((?2 = \'\') OR (circle_id = ?2))AND ((?3=\'-999\') OR (?3=type) OR (?3=\'-998\' AND type != -1))AND ((?4 = \'\') OR (name like ?4 escape \'\\\')) ORDER BY sort_key"

    invoke-virtual {v6, v0, v2}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Landroid/database/AbstractWindowedCursor;

    new-instance v1, Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v1, v0, v13, v7}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    return-object v1

    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v3, 0x25

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
