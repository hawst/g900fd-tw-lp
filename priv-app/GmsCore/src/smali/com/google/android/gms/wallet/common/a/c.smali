.class public final Lcom/google/android/gms/wallet/common/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:[C

.field public static final b:Ljava/lang/String;

.field public static final c:Ljava/util/Comparator;


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/t/a/b;

.field private final f:Ljava/lang/CharSequence;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 18
    const/4 v0, 0x1

    new-array v0, v0, [C

    const/4 v1, 0x0

    const/16 v2, 0x52

    aput-char v2, v0, v1

    sput-object v0, Lcom/google/android/gms/wallet/common/a/c;->a:[C

    .line 20
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/wallet/common/a/c;->b:Ljava/lang/String;

    .line 28
    new-instance v0, Lcom/google/android/gms/wallet/common/a/d;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/a/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/a/c;->c:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/t/a/b;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    invoke-static {p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 78
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/a/c;->d:Ljava/lang/String;

    .line 79
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/a/c;->e:Lcom/google/t/a/b;

    .line 80
    iput-object p3, p0, Lcom/google/android/gms/wallet/common/a/c;->f:Ljava/lang/CharSequence;

    .line 81
    iput-object p4, p0, Lcom/google/android/gms/wallet/common/a/c;->g:Ljava/lang/String;

    .line 82
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/a/c;->h:Ljava/lang/String;

    .line 83
    return-void

    .line 77
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 96
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/a/c;->d:Ljava/lang/String;

    .line 97
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/a/c;->e:Lcom/google/t/a/b;

    .line 98
    iput-object p2, p0, Lcom/google/android/gms/wallet/common/a/c;->f:Ljava/lang/CharSequence;

    .line 99
    iput-object p3, p0, Lcom/google/android/gms/wallet/common/a/c;->g:Ljava/lang/String;

    .line 100
    iput-object p4, p0, Lcom/google/android/gms/wallet/common/a/c;->h:Ljava/lang/String;

    .line 101
    return-void

    .line 95
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    invoke-direct {p0, p1, p1, p2, v0}, Lcom/google/android/gms/wallet/common/a/c;-><init>(Ljava/lang/String;Ljava/lang/CharSequence;Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/c;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Lcom/google/t/a/b;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/c;->e:Lcom/google/t/a/b;

    return-object v0
.end method

.method public final c()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/c;->f:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/c;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/a/c;->h:Ljava/lang/String;

    return-object v0
.end method
