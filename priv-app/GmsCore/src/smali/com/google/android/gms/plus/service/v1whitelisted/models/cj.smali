.class public final Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/util/List;

.field public final b:Ljava/util/Set;

.field private c:Ljava/util/List;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;->b:Ljava/util/Set;

    .line 45
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/service/v1whitelisted/models/ci;
    .locals 4

    .prologue
    .line 71
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedRhsComponentItemEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;->b:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;->a:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;->c:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientLoggedRhsComponentItemEntity;-><init>(Ljava/util/Set;Ljava/util/List;Ljava/util/List;)V

    return-object v0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;
    .locals 2

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;->c:Ljava/util/List;

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/cj;->b:Ljava/util/Set;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 64
    return-object p0
.end method
