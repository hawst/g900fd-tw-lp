.class public Lcom/google/android/gms/car/CarPhoneStatus;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field public b:[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;

.field public c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Lcom/google/android/gms/car/fz;

    invoke-direct {v0}, Lcom/google/android/gms/car/fz;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/CarPhoneStatus;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/CarPhoneStatus;->a:I

    .line 126
    return-void
.end method

.method public constructor <init>(I[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;I)V
    .locals 0

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput p1, p0, Lcom/google/android/gms/car/CarPhoneStatus;->a:I

    .line 120
    iput-object p2, p0, Lcom/google/android/gms/car/CarPhoneStatus;->b:[Lcom/google/android/gms/car/CarPhoneStatus$CarCall;

    .line 121
    iput p3, p0, Lcom/google/android/gms/car/CarPhoneStatus;->c:I

    .line 122
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 142
    iget v0, p0, Lcom/google/android/gms/car/CarPhoneStatus;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 130
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 135
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/car/fz;->a(Lcom/google/android/gms/car/CarPhoneStatus;Landroid/os/Parcel;I)V

    .line 136
    return-void
.end method
