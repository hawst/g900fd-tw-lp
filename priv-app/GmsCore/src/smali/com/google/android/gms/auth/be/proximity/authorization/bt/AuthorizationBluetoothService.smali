.class public Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;
.super Landroid/app/IntentService;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/f;
.implements Lcom/google/android/gms/auth/trustagent/aw;


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Ljava/util/concurrent/Executor;

.field private c:Lcom/google/android/gms/auth/trustagent/at;

.field private d:Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

.field private e:Lcom/google/android/gms/auth/be/proximity/authorization/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 37
    const-class v0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 48
    invoke-static {}, Ljava/util/concurrent/Executors;->newCachedThreadPool()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->b:Ljava/util/concurrent/Executor;

    .line 49
    return-void
.end method

.method constructor <init>(Ljava/util/concurrent/Executor;Lcom/google/android/gms/auth/trustagent/at;Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;)V
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 58
    iput-object p1, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->b:Ljava/util/concurrent/Executor;

    .line 59
    iput-object p2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->c:Lcom/google/android/gms/auth/trustagent/at;

    .line 60
    iput-object p3, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->d:Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    .line 61
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;)Lcom/google/android/gms/auth/trustagent/at;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->c:Lcom/google/android/gms/auth/trustagent/at;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;)Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->d:Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;)Lcom/google/android/gms/auth/be/proximity/authorization/a;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->e:Lcom/google/android/gms/auth/be/proximity/authorization/a;

    return-object v0
.end method

.method static synthetic e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    return-object v0
.end method

.method private f()V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->b:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/gms/auth/be/proximity/authorization/bt/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/be/proximity/authorization/bt/a;-><init>(Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 163
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 167
    invoke-direct {p0}, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->f()V

    .line 168
    return-void
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0}, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->f()V

    .line 173
    return-void
.end method

.method public final c()V
    .locals 0

    .prologue
    .line 177
    invoke-direct {p0}, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->f()V

    .line 178
    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 182
    invoke-direct {p0}, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->f()V

    .line 183
    return-void
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 65
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 67
    invoke-static {}, Lcom/google/android/gms/auth/be/proximity/authorization/a;->a()Lcom/google/android/gms/auth/be/proximity/authorization/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->e:Lcom/google/android/gms/auth/be/proximity/authorization/a;

    .line 68
    invoke-static {p0}, Lcom/google/android/gms/auth/trustagent/at;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/trustagent/at;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->c:Lcom/google/android/gms/auth/trustagent/at;

    .line 69
    invoke-static {p0}, Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;->a(Landroid/content/Context;)Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->d:Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    .line 70
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 74
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    const-string v3, "Received onHandleIntent() call: %s"

    new-array v4, v1, [Ljava/lang/Object;

    aput-object p1, v4, v2

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->d:Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;->a(Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/f;)V

    .line 79
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->c:Lcom/google/android/gms/auth/trustagent/at;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/auth/trustagent/at;->a(Lcom/google/android/gms/auth/trustagent/aw;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/trustagent/az; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    :goto_0
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/auth/testability/android/bluetooth/b;->a()Lcom/google/android/gms/auth/testability/android/bluetooth/a;

    move-result-object v3

    if-nez v3, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    const-string v1, "Bluetooth is not supported on this device."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 88
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->c:Lcom/google/android/gms/auth/trustagent/at;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/auth/trustagent/at;->b(Lcom/google/android/gms/auth/trustagent/aw;)V
    :try_end_2
    .catch Lcom/google/android/gms/auth/trustagent/az; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 94
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->d:Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;->b(Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/f;)V

    .line 95
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 81
    :try_start_3
    sget-object v3, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    const-string v4, "Cannot start tracking for trust state."

    invoke-static {v3, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 94
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->d:Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/d;->b(Lcom/google/android/gms/auth/be/proximity/authorization/userpresence/f;)V

    throw v0

    .line 85
    :cond_0
    :try_start_4
    iget-object v0, v3, Lcom/google/android/gms/auth/testability/android/bluetooth/a;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    const-string v1, "Bluetooth is not enabled on this device."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1

    .line 87
    :catchall_1
    move-exception v0

    .line 88
    :try_start_5
    iget-object v1, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->c:Lcom/google/android/gms/auth/trustagent/at;

    invoke-virtual {v1, p0}, Lcom/google/android/gms/auth/trustagent/at;->b(Lcom/google/android/gms/auth/trustagent/aw;)V
    :try_end_5
    .catch Lcom/google/android/gms/auth/trustagent/az; {:try_start_5 .. :try_end_5} :catch_6
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 91
    :goto_3
    :try_start_6
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 85
    :cond_1
    :try_start_7
    const-string v4, "Easy Unlock"

    sget-object v0, Lcom/google/android/gms/auth/b/a;->G:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v5

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0xa

    if-lt v0, v6, :cond_2

    move v0, v1

    :goto_4
    invoke-static {v0}, Lcom/google/k/a/ah;->b(Z)V

    iget-object v0, v3, Lcom/google/android/gms/auth/testability/android/bluetooth/a;->a:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0, v4, v5}, Landroid/bluetooth/BluetoothAdapter;->listenUsingInsecureRfcommWithServiceRecord(Ljava/lang/String;Ljava/util/UUID;)Landroid/bluetooth/BluetoothServerSocket;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/testability/android/bluetooth/f;->a(Landroid/bluetooth/BluetoothServerSocket;)Lcom/google/android/gms/auth/testability/android/bluetooth/e;
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    move-result-object v1

    :goto_5
    :try_start_8
    sget-object v0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    const-string v2, "Starts to wait for incoming bluetooth connections."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Lcom/google/android/gms/auth/testability/android/bluetooth/e;->a()Lcom/google/android/gms/auth/testability/android/bluetooth/g;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    const-string v3, "Received bluetooth connection from %s."

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/testability/android/bluetooth/g;->d()Lcom/google/android/gms/auth/testability/android/bluetooth/BluetoothDevice;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/auth/testability/android/bluetooth/BluetoothDevice;->a()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->b:Ljava/util/concurrent/Executor;

    new-instance v3, Lcom/google/android/gms/auth/be/proximity/authorization/bt/c;

    iget-object v4, p0, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->e:Lcom/google/android/gms/auth/be/proximity/authorization/a;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/testability/android/bluetooth/g;->d()Lcom/google/android/gms/auth/testability/android/bluetooth/BluetoothDevice;

    move-result-object v5

    invoke-direct {v3, p0, v4, v5, v0}, Lcom/google/android/gms/auth/be/proximity/authorization/bt/c;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/be/proximity/authorization/a;Lcom/google/android/gms/auth/testability/android/bluetooth/BluetoothDevice;Lcom/google/android/gms/auth/testability/android/bluetooth/g;)V

    invoke-interface {v2, v3}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_1
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    goto :goto_5

    :catch_1
    move-exception v0

    :try_start_9
    sget-object v2, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    const-string v3, "Failed to accept the connection."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_2

    :try_start_a
    invoke-virtual {v1}, Lcom/google/android/gms/auth/testability/android/bluetooth/e;->close()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    goto/16 :goto_1

    :catch_2
    move-exception v0

    :try_start_b
    sget-object v1, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    const-string v2, "Failed to close the bluetooth server socket."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_1

    :cond_2
    move v0, v2

    goto :goto_4

    :catch_3
    move-exception v0

    sget-object v1, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    const-string v2, "Failed to create insecure RFCOMM server socket."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    goto/16 :goto_1

    :catchall_2
    move-exception v0

    :try_start_c
    invoke-virtual {v1}, Lcom/google/android/gms/auth/testability/android/bluetooth/e;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_4
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    :goto_6
    :try_start_d
    throw v0

    :catch_4
    move-exception v1

    sget-object v2, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    const-string v3, "Failed to close the bluetooth server socket."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    goto :goto_6

    .line 89
    :catch_5
    move-exception v0

    .line 90
    :try_start_e
    sget-object v1, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    const-string v2, "Cannot stop tracking for trust state."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_2

    .line 89
    :catch_6
    move-exception v1

    .line 90
    sget-object v2, Lcom/google/android/gms/auth/be/proximity/authorization/bt/AuthorizationBluetoothService;->a:Ljava/lang/String;

    const-string v3, "Cannot stop tracking for trust state."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto/16 :goto_3
.end method
