.class public final Lcom/google/android/gms/games/ui/b/a/e;
.super Lcom/google/android/gms/games/ui/b/a/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b/a/a;-><init>()V

    .line 21
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Landroid/content/Intent;)Lcom/google/android/gms/games/ui/b/a/e;
    .locals 3

    .prologue
    .line 38
    new-instance v0, Lcom/google/android/gms/games/ui/b/a/e;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/b/a/e;-><init>()V

    .line 40
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 41
    const-string v2, "signedInAccountName"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 42
    const-string v2, "newAccountName"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const-string v2, "intent"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 44
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/b/a/e;->setArguments(Landroid/os/Bundle;)V

    .line 46
    return-object v0
.end method


# virtual methods
.method protected final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 51
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b/a/e;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->hJ:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/q;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final c()V
    .locals 3

    .prologue
    .line 56
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b/a/e;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/b/a/e;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 58
    const-string v2, "intent"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 61
    instance-of v2, v1, Lcom/google/android/gms/games/ui/b/a/g;

    if-eqz v2, :cond_0

    .line 62
    check-cast v1, Lcom/google/android/gms/games/ui/b/a/g;

    .line 63
    invoke-interface {v1}, Lcom/google/android/gms/games/ui/b/a/g;->C()Lcom/google/android/gms/games/ui/b/a/f;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/ui/b/a/f;

    .line 69
    invoke-interface {v1, v0}, Lcom/google/android/gms/games/ui/b/a/f;->c(Landroid/content/Intent;)V

    .line 70
    return-void

    .line 65
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "IntentChangeAccountDialogFragment must be used with a parent Activity which implements IntentAccountSwitcherProvider."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
