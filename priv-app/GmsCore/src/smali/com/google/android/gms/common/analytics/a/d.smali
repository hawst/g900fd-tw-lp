.class public final Lcom/google/android/gms/common/analytics/a/d;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile j:[Lcom/google/android/gms/common/analytics/a/d;


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Long;

.field public i:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 695
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 696
    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->a:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->h:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->i:Ljava/lang/Integer;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/analytics/a/d;->cachedSize:I

    .line 697
    return-void
.end method

.method public static a()[Lcom/google/android/gms/common/analytics/a/d;
    .locals 2

    .prologue
    .line 657
    sget-object v0, Lcom/google/android/gms/common/analytics/a/d;->j:[Lcom/google/android/gms/common/analytics/a/d;

    if-nez v0, :cond_1

    .line 658
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 660
    :try_start_0
    sget-object v0, Lcom/google/android/gms/common/analytics/a/d;->j:[Lcom/google/android/gms/common/analytics/a/d;

    if-nez v0, :cond_0

    .line 661
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/common/analytics/a/d;

    sput-object v0, Lcom/google/android/gms/common/analytics/a/d;->j:[Lcom/google/android/gms/common/analytics/a/d;

    .line 663
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 665
    :cond_1
    sget-object v0, Lcom/google/android/gms/common/analytics/a/d;->j:[Lcom/google/android/gms/common/analytics/a/d;

    return-object v0

    .line 663
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 748
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 749
    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 750
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/d;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 753
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 754
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/d;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 757
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 758
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/d;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 761
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 762
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/d;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 765
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 766
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/d;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 769
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 770
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/d;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 773
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 774
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/d;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 777
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->h:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 778
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/d;->h:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 781
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 782
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/common/analytics/a/d;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 785
    :cond_8
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 643
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->a:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->h:Ljava/lang/Long;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->i:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 716
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 717
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 719
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 720
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 722
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 723
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 725
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 726
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 728
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 729
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 731
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 732
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 734
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 735
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 737
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->h:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 738
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 740
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/common/analytics/a/d;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 741
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/gms/common/analytics/a/d;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 743
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 744
    return-void
.end method
