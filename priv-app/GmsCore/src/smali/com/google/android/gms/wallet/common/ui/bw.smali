.class final Lcom/google/android/gms/wallet/common/ui/bw;
.super Landroid/webkit/WebViewClient;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;)V
    .locals 0

    .prologue
    .line 180
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/bw;->a:Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPageFinished(Landroid/webkit/WebView;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bw;->a:Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->b(Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;)Z

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bw;->a:Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->c(Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;)V

    .line 192
    return-void
.end method

.method public final onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bw;->a:Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->a(Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;)V

    .line 186
    return-void
.end method

.method public final shouldOverrideUrlLoading(Landroid/webkit/WebView;Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/bw;->a:Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "legalDocs"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;

    .line 198
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/LegalDocsForCountry;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 199
    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 201
    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/net/Uri;->getEncodedSchemeSpecificPart()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 203
    const/4 v0, 0x0

    .line 208
    :goto_0
    return v0

    .line 206
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 207
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/bw;->a:Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/ExplicitTosActivity;->startActivity(Landroid/content/Intent;)V

    .line 208
    const/4 v0, 0x1

    goto :goto_0
.end method
