.class public final Lcom/google/android/gms/wallet/common/z;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)I
    .locals 1

    .prologue
    .line 200
    packed-switch p0, :pswitch_data_0

    .line 209
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 202
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 204
    :pswitch_1
    const/4 v0, 0x2

    goto :goto_0

    .line 206
    :pswitch_2
    const/4 v0, 0x3

    goto :goto_0

    .line 200
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(Lcom/google/aa/b/a/a/a/a/w;)Lcom/google/checkout/inapp/proto/j;
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 25
    if-nez p0, :cond_0

    .line 26
    const/4 v0, 0x0

    .line 196
    :goto_0
    return-object v0

    .line 29
    :cond_0
    new-instance v0, Lcom/google/checkout/inapp/proto/j;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/j;-><init>()V

    .line 32
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_1

    .line 33
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    .line 40
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 41
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    .line 44
    :cond_2
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->e:I

    if-eqz v1, :cond_3

    .line 45
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->e:I

    iput v1, v0, Lcom/google/checkout/inapp/proto/j;->j:I

    .line 48
    :cond_3
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->f:I

    if-eqz v1, :cond_4

    .line 49
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->f:I

    iput v1, v0, Lcom/google/checkout/inapp/proto/j;->k:I

    .line 52
    :cond_4
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 53
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    .line 56
    :cond_5
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 57
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    .line 60
    :cond_6
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 61
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    .line 64
    :cond_7
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->i:I

    packed-switch v1, :pswitch_data_0

    .line 143
    :pswitch_0
    iput v5, v0, Lcom/google/checkout/inapp/proto/j;->h:I

    .line 145
    iget-object v1, v0, Lcom/google/checkout/inapp/proto/j;->g:[I

    invoke-static {v1, v8}, Lcom/google/android/gms/common/util/h;->b([II)[I

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/j;->g:[I

    .line 151
    :goto_1
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->b:I

    packed-switch v1, :pswitch_data_1

    .line 189
    :goto_2
    iput v4, v0, Lcom/google/checkout/inapp/proto/j;->c:I

    .line 193
    :goto_3
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->l:I

    invoke-static {v1}, Lcom/google/android/gms/wallet/common/z;->a(I)I

    move-result v1

    iput v1, v0, Lcom/google/checkout/inapp/proto/j;->l:I

    goto :goto_0

    .line 66
    :pswitch_1
    iput v6, v0, Lcom/google/checkout/inapp/proto/j;->h:I

    goto :goto_1

    .line 69
    :pswitch_2
    iput v6, v0, Lcom/google/checkout/inapp/proto/j;->h:I

    goto :goto_1

    .line 72
    :pswitch_3
    iput v7, v0, Lcom/google/checkout/inapp/proto/j;->h:I

    goto :goto_1

    .line 78
    :pswitch_4
    iput v6, v0, Lcom/google/checkout/inapp/proto/j;->h:I

    goto :goto_1

    .line 81
    :pswitch_5
    iput v5, v0, Lcom/google/checkout/inapp/proto/j;->h:I

    .line 83
    iget-object v1, v0, Lcom/google/checkout/inapp/proto/j;->g:[I

    invoke-static {v1, v7}, Lcom/google/android/gms/common/util/h;->b([II)[I

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/j;->g:[I

    goto :goto_1

    .line 88
    :pswitch_6
    iput v5, v0, Lcom/google/checkout/inapp/proto/j;->h:I

    .line 90
    iget-object v1, v0, Lcom/google/checkout/inapp/proto/j;->g:[I

    const/4 v2, 0x6

    invoke-static {v1, v2}, Lcom/google/android/gms/common/util/h;->b([II)[I

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/j;->g:[I

    goto :goto_1

    .line 101
    :pswitch_7
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->f:I

    if-eqz v1, :cond_8

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->e:I

    if-eqz v1, :cond_8

    .line 103
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->e:I

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->f:I

    const/4 v3, -0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/wallet/common/w;->a(III)I

    move-result v1

    packed-switch v1, :pswitch_data_2

    .line 121
    iput v5, v0, Lcom/google/checkout/inapp/proto/j;->h:I

    .line 123
    iget-object v1, v0, Lcom/google/checkout/inapp/proto/j;->g:[I

    invoke-static {v1, v8}, Lcom/google/android/gms/common/util/h;->b([II)[I

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/j;->g:[I

    goto :goto_1

    .line 106
    :pswitch_8
    iput v6, v0, Lcom/google/checkout/inapp/proto/j;->h:I

    goto :goto_1

    .line 110
    :pswitch_9
    iput v5, v0, Lcom/google/checkout/inapp/proto/j;->h:I

    .line 112
    iget-object v1, v0, Lcom/google/checkout/inapp/proto/j;->g:[I

    invoke-static {v1, v7}, Lcom/google/android/gms/common/util/h;->b([II)[I

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/j;->g:[I

    goto :goto_1

    .line 130
    :cond_8
    iput v5, v0, Lcom/google/checkout/inapp/proto/j;->h:I

    .line 132
    iget-object v1, v0, Lcom/google/checkout/inapp/proto/j;->g:[I

    invoke-static {v1, v8}, Lcom/google/android/gms/common/util/h;->b([II)[I

    move-result-object v1

    iput-object v1, v0, Lcom/google/checkout/inapp/proto/j;->g:[I

    goto :goto_1

    .line 153
    :pswitch_a
    iput v4, v0, Lcom/google/checkout/inapp/proto/j;->d:I

    .line 154
    iput v5, v0, Lcom/google/checkout/inapp/proto/j;->c:I

    goto :goto_3

    .line 157
    :pswitch_b
    iput v4, v0, Lcom/google/checkout/inapp/proto/j;->d:I

    .line 158
    const/4 v1, 0x4

    iput v1, v0, Lcom/google/checkout/inapp/proto/j;->c:I

    goto :goto_3

    .line 161
    :pswitch_c
    iput v4, v0, Lcom/google/checkout/inapp/proto/j;->d:I

    .line 162
    const/4 v1, 0x6

    iput v1, v0, Lcom/google/checkout/inapp/proto/j;->c:I

    goto :goto_3

    .line 165
    :pswitch_d
    iput v4, v0, Lcom/google/checkout/inapp/proto/j;->d:I

    .line 166
    iput v7, v0, Lcom/google/checkout/inapp/proto/j;->c:I

    goto :goto_3

    .line 169
    :pswitch_e
    iput v4, v0, Lcom/google/checkout/inapp/proto/j;->d:I

    .line 170
    iput v8, v0, Lcom/google/checkout/inapp/proto/j;->c:I

    goto :goto_3

    .line 173
    :pswitch_f
    iput v4, v0, Lcom/google/checkout/inapp/proto/j;->d:I

    .line 174
    const/4 v1, 0x7

    iput v1, v0, Lcom/google/checkout/inapp/proto/j;->c:I

    goto/16 :goto_3

    .line 177
    :pswitch_10
    iput v4, v0, Lcom/google/checkout/inapp/proto/j;->d:I

    goto/16 :goto_2

    .line 181
    :pswitch_11
    iput v4, v0, Lcom/google/checkout/inapp/proto/j;->d:I

    .line 182
    iput v6, v0, Lcom/google/checkout/inapp/proto/j;->c:I

    goto/16 :goto_3

    .line 185
    :pswitch_12
    iput v4, v0, Lcom/google/checkout/inapp/proto/j;->d:I

    .line 186
    const/16 v1, 0x1b

    iput v1, v0, Lcom/google/checkout/inapp/proto/j;->c:I

    goto/16 :goto_3

    .line 64
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_3
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch

    .line 151
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_10
        :pswitch_11
        :pswitch_d
        :pswitch_a
        :pswitch_b
        :pswitch_e
        :pswitch_c
        :pswitch_f
        :pswitch_12
    .end packed-switch

    .line 103
    :pswitch_data_2
    .packed-switch -0x1
        :pswitch_9
        :pswitch_8
    .end packed-switch
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 365
    packed-switch p0, :pswitch_data_0

    .line 378
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 367
    :pswitch_0
    const/4 v0, 0x3

    goto :goto_0

    .line 369
    :pswitch_1
    const/4 v0, 0x4

    goto :goto_0

    .line 371
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 373
    :pswitch_3
    const/4 v0, 0x1

    goto :goto_0

    .line 375
    :pswitch_4
    const/16 v0, 0x1b

    goto :goto_0

    .line 365
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method
