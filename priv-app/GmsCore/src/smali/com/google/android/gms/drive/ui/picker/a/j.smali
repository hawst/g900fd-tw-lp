.class public final Lcom/google/android/gms/drive/ui/picker/a/j;
.super Landroid/support/v4/a/a;
.source "SourceFile"


# instance fields
.field private final f:Lcom/google/android/gms/drive/ui/picker/a/k;

.field private final g:Lcom/google/android/gms/common/api/v;

.field private final h:Lcom/google/android/gms/drive/ui/picker/j;

.field private final i:Ljava/lang/String;

.field private j:Lcom/google/android/gms/drive/ui/picker/a/l;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;Lcom/google/android/gms/drive/ui/picker/a/k;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 36
    invoke-direct {p0, p1}, Landroid/support/v4/a/a;-><init>(Landroid/content/Context;)V

    .line 37
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/k;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->f:Lcom/google/android/gms/drive/ui/picker/a/k;

    .line 38
    iput-object p3, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->i:Ljava/lang/String;

    .line 39
    invoke-virtual {p1}, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->g:Lcom/google/android/gms/common/api/v;

    .line 40
    invoke-virtual {p1}, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->e()Lcom/google/android/gms/drive/ui/picker/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->h:Lcom/google/android/gms/drive/ui/picker/j;

    .line 41
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/ui/picker/a/l;)V
    .locals 2

    .prologue
    .line 61
    iget-boolean v0, p0, Landroid/support/v4/a/j;->r:Z

    if-eqz v0, :cond_1

    .line 63
    if-eqz p1, :cond_0

    .line 64
    invoke-interface {p1}, Lcom/google/android/gms/drive/ui/picker/a/l;->close()V

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    .line 70
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    .line 73
    :try_start_0
    iget-boolean v0, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v0, :cond_2

    .line 74
    invoke-super {p0, p1}, Landroid/support/v4/a/a;->b(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    :cond_2
    if-eqz v1, :cond_0

    if-eq v1, p1, :cond_0

    .line 78
    invoke-interface {v1}, Lcom/google/android/gms/drive/ui/picker/a/l;->close()V

    goto :goto_0

    .line 77
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_3

    if-eq v1, p1, :cond_3

    .line 78
    invoke-interface {v1}, Lcom/google/android/gms/drive/ui/picker/a/l;->close()V

    :cond_3
    throw v0
.end method


# virtual methods
.method protected final a()V
    .locals 0

    .prologue
    .line 108
    invoke-super {p0}, Landroid/support/v4/a/a;->a()V

    .line 109
    return-void
.end method

.method public final synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/gms/drive/ui/picker/a/l;

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/drive/ui/picker/a/l;->close()V

    :cond_0
    return-void
.end method

.method public final synthetic b(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 17
    check-cast p1, Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/ui/picker/a/j;->a(Lcom/google/android/gms/drive/ui/picker/a/l;)V

    return-void
.end method

.method public final synthetic d()Ljava/lang/Object;
    .locals 4

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->g:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->h:Lcom/google/android/gms/drive/ui/picker/j;

    sget-object v1, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->g:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->f:Lcom/google/android/gms/drive/ui/picker/a/k;

    iget-object v3, v3, Lcom/google/android/gms/drive/ui/picker/a/k;->a:Lcom/google/android/gms/drive/query/Query;

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/query/Query;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/j;->a(Lcom/google/android/gms/common/api/am;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/k;

    const-string v1, "DocListDataLoader"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Query result status: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/drive/k;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {v0}, Lcom/google/android/gms/drive/k;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->f:Lcom/google/android/gms/drive/ui/picker/a/k;

    invoke-interface {v0}, Lcom/google/android/gms/drive/k;->c()Z

    move-result v2

    iput-boolean v2, v1, Lcom/google/android/gms/drive/ui/picker/a/k;->b:Z

    new-instance v1, Lcom/google/android/gms/drive/ui/picker/a/x;

    invoke-interface {v0}, Lcom/google/android/gms/drive/k;->b()Lcom/google/android/gms/drive/ak;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/ui/picker/a/x;-><init>(Lcom/google/android/gms/drive/ak;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "DocListDataLoader"

    const-string v1, "Client is not connected"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final e()V
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/a/j;->a(Lcom/google/android/gms/drive/ui/picker/a/l;)V

    .line 89
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/a/j;->i()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    if-nez v0, :cond_2

    .line 90
    :cond_1
    invoke-virtual {p0}, Landroid/support/v4/a/j;->a()V

    .line 92
    :cond_2
    return-void
.end method

.method protected final f()V
    .locals 0

    .prologue
    .line 96
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/a/j;->b()Z

    .line 97
    return-void
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 113
    invoke-super {p0}, Landroid/support/v4/a/a;->g()V

    .line 115
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/a/j;->b()Z

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    if-eqz v0, :cond_0

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/l;->close()V

    .line 121
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    .line 122
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 126
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "DocListDataLoader [tagName="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", docListQuery="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/j;->f:Lcom/google/android/gms/drive/ui/picker/a/k;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
