.class public final Lcom/google/android/gms/drive/database/model/af;
.super Lcom/google/android/gms/drive/database/model/ae;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/drive/database/model/af;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/drive/database/model/af;

    invoke-direct {v0}, Lcom/google/android/gms/drive/database/model/af;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/database/model/af;->a:Lcom/google/android/gms/drive/database/model/af;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 30
    const/16 v0, 0x2b

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/model/ae;-><init>(I)V

    .line 32
    return-void
.end method

.method public static a()Lcom/google/android/gms/drive/database/model/af;
    .locals 1

    .prologue
    .line 26
    sget-object v0, Lcom/google/android/gms/drive/database/model/af;->a:Lcom/google/android/gms/drive/database/model/af;

    return-object v0
.end method

.method static synthetic d()Lcom/google/android/gms/drive/database/model/af;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/android/gms/drive/database/model/af;->a:Lcom/google/android/gms/drive/database/model/af;

    return-object v0
.end method

.method static synthetic h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Lcom/google/android/gms/drive/database/model/af;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static i()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/drive/auth/AppIdentity;->a()Lcom/google/android/gms/drive/auth/AppIdentity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/auth/AppIdentity;->d()Lorg/json/JSONObject;

    move-result-object v0

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 37
    :catch_0
    move-exception v0

    .line 38
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 49
    const-string v0, "DocumentContent"

    return-object v0
.end method

.method public final synthetic c()[Lcom/google/android/gms/drive/g/ak;
    .locals 1

    .prologue
    .line 21
    invoke-static {}, Lcom/google/android/gms/drive/database/model/ag;->values()[Lcom/google/android/gms/drive/database/model/ag;

    move-result-object v0

    return-object v0
.end method
