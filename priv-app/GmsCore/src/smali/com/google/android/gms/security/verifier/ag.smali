.class public final Lcom/google/android/gms/security/verifier/ag;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/gms/security/verifier/ag;->a:Ljava/util/Random;

    return-void
.end method

.method static a(J)D
    .locals 4

    .prologue
    .line 100
    long-to-double v0, p0

    const-wide v2, 0x4194997000000000L    # 8.64E7

    div-double/2addr v0, v2

    return-wide v0
.end method

.method static a(I)I
    .locals 1

    .prologue
    .line 84
    mul-int/lit16 v0, p0, 0x3e8

    return v0
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 25
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    .line 26
    return-void

    .line 28
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This method cannot be called from the UI thread."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static a([BLjava/io/InputStream;)V
    .locals 2

    .prologue
    .line 72
    const/4 v0, 0x0

    .line 73
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 74
    array-length v1, p0

    sub-int/2addr v1, v0

    .line 75
    invoke-virtual {p1, p0, v0, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 76
    if-gez v1, :cond_0

    .line 77
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Attempted to read past end of stream"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 79
    :cond_0
    add-int/2addr v0, v1

    .line 80
    goto :goto_0

    .line 81
    :cond_1
    return-void
.end method

.method static a(Ljava/io/File;)[B
    .locals 2

    .prologue
    .line 35
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 37
    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/security/verifier/ag;->a(Ljava/io/InputStream;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 39
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-object v1

    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v1
.end method

.method private static a(Ljava/io/InputStream;)[B
    .locals 4

    .prologue
    .line 49
    :try_start_0
    const-string v0, "SHA256"

    invoke-static {v0}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 55
    const/16 v1, 0x4000

    new-array v1, v1, [B

    .line 56
    :goto_0
    invoke-virtual {p0, v1}, Ljava/io/InputStream;->read([B)I

    move-result v2

    if-ltz v2, :cond_0

    .line 57
    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Ljava/security/MessageDigest;->update([BII)V

    goto :goto_0

    .line 50
    :catch_0
    move-exception v0

    .line 51
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 59
    :cond_0
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    return-object v0
.end method

.method static b()I
    .locals 1

    .prologue
    .line 92
    const v0, 0x6ddd00

    return v0
.end method

.method static b(I)I
    .locals 1

    .prologue
    .line 88
    mul-int/lit8 v0, p0, 0x3c

    mul-int/lit16 v0, v0, 0x3e8

    return v0
.end method

.method static c()I
    .locals 1

    .prologue
    .line 96
    const v0, 0x5265c00

    return v0
.end method

.method static d()[B
    .locals 2

    .prologue
    .line 104
    const/16 v0, 0x100

    new-array v0, v0, [B

    .line 105
    sget-object v1, Lcom/google/android/gms/security/verifier/ag;->a:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextBytes([B)V

    .line 106
    return-object v0
.end method
