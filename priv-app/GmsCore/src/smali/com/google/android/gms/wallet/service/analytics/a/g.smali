.class public Lcom/google/android/gms/wallet/service/analytics/a/g;
.super Lcom/google/android/gms/wallet/service/analytics/a/h;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/k/f/a/a/s;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/analytics/a/h;-><init>()V

    .line 34
    new-instance v0, Lcom/google/k/f/a/a/s;

    invoke-direct {v0}, Lcom/google/k/f/a/a/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->a:Lcom/google/k/f/a/a/s;

    .line 35
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/wallet/common/ag;)V
    .locals 2

    .prologue
    .line 38
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/h;-><init>(Lcom/google/android/gms/wallet/common/ag;)V

    .line 39
    const-class v0, Lcom/google/k/f/a/a/s;

    new-instance v1, Lcom/google/k/f/a/a/s;

    invoke-direct {v1}, Lcom/google/k/f/a/a/s;-><init>()V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/wallet/common/ag;->a(Ljava/lang/Class;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/k/f/a/a/s;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->a:Lcom/google/k/f/a/a/s;

    .line 41
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 61
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;

    if-eqz v0, :cond_0

    .line 62
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;

    .line 63
    iget-object v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;->c:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/g;->a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;Landroid/content/Context;)V

    .line 64
    invoke-virtual {p2}, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletRequestedEvent;->a()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->d:Ljava/lang/String;

    .line 97
    :goto_0
    return-void

    .line 65
    :cond_0
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;

    if-eqz v0, :cond_1

    .line 66
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->a:Lcom/google/k/f/a/a/s;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;->c:I

    iput v1, v0, Lcom/google/k/f/a/a/s;->d:I

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->a:Lcom/google/k/f/a/a/s;

    iget v1, p2, Lcom/google/android/gms/wallet/analytics/events/OwMwUnsuccessfulEvent;->d:I

    iput v1, v0, Lcom/google/k/f/a/a/s;->c:I

    goto :goto_0

    .line 69
    :cond_1
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwUserFoundToHavePreAuthEvent;

    if-eqz v0, :cond_2

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->a:Lcom/google/k/f/a/a/s;

    iput-boolean v1, v0, Lcom/google/k/f/a/a/s;->a:Z

    goto :goto_0

    .line 71
    :cond_2
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwMwChooserShownEvent;

    if-eqz v0, :cond_3

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->a:Lcom/google/k/f/a/a/s;

    iput-boolean v1, v0, Lcom/google/k/f/a/a/s;->b:Z

    goto :goto_0

    .line 73
    :cond_3
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;

    if-eqz v0, :cond_4

    .line 74
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;

    .line 75
    iget v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwMwDefaultsChangedEvent;->a:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 80
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->a:Lcom/google/k/f/a/a/s;

    iput-boolean v1, v0, Lcom/google/k/f/a/a/s;->e:Z

    goto :goto_0

    .line 77
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->a:Lcom/google/k/f/a/a/s;

    iput-boolean v1, v0, Lcom/google/k/f/a/a/s;->f:Z

    goto :goto_0

    .line 81
    :cond_4
    instance-of v0, p2, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;

    if-eqz v0, :cond_5

    .line 90
    check-cast p2, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->a:Lcom/google/k/f/a/a/s;

    iput v1, v0, Lcom/google/k/f/a/a/s;->c:I

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->a:Lcom/google/k/f/a/a/s;

    iget-boolean v1, p2, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;->c:Z

    iput-boolean v1, v0, Lcom/google/k/f/a/a/s;->g:Z

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->a:Lcom/google/k/f/a/a/s;

    iget-object v1, p2, Lcom/google/android/gms/wallet/analytics/events/OwMaskedWalletReceivedEvent;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/k/f/a/a/s;->h:Ljava/lang/String;

    goto :goto_0

    .line 95
    :cond_5
    const-string v0, "OwMaskedWalletSessionState"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to fill data for event "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 75
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/wallet/common/ah;)V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/service/analytics/a/h;->a(Lcom/google/android/gms/wallet/common/ah;)V

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->a:Lcom/google/k/f/a/a/s;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/wallet/common/ah;->a(Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/wallet/common/ah;

    .line 47
    return-void
.end method

.method protected final a(Lcom/google/k/f/a/a/j;)V
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->a:Lcom/google/k/f/a/a/s;

    iput-object v0, p1, Lcom/google/k/f/a/a/j;->l:Lcom/google/k/f/a/a/s;

    .line 52
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/analytics/a/g;->a:Lcom/google/k/f/a/a/s;

    iget v0, v0, Lcom/google/k/f/a/a/s;->c:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
