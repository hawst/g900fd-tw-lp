.class public Lcom/google/android/gms/drive/g/aw;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/drive/g/aw;

.field private static b:Lcom/google/android/gms/drive/g/aw;

.field private static c:Lcom/google/android/gms/drive/g/i;

.field private static d:Lcom/google/android/gms/drive/g/i;

.field private static e:Lcom/google/android/gms/drive/g/i;


# instance fields
.field private final A:Lcom/google/android/gms/drive/b/a/e;

.field private final B:Lcom/google/android/gms/drive/b/b/d;

.field private final C:Lcom/google/android/gms/drive/b/b/p;

.field private final D:Lcom/google/android/gms/drive/events/k;

.field private final E:Lcom/google/android/gms/drive/events/u;

.field private final F:Lcom/google/android/gms/drive/realtime/cache/w;

.field private final G:Lcom/google/android/gms/drive/c/c;

.field private final H:Lcom/google/android/gms/drive/c/b;

.field private final f:Lcom/google/android/gms/drive/g/n;

.field private final g:Landroid/content/Context;

.field private final h:Lcom/google/android/gms/drive/database/i;

.field private final i:Lcom/google/android/gms/drive/database/r;

.field private final j:Lcom/google/android/gms/drive/api/k;

.field private final k:Lcom/google/android/gms/drive/a/a/a;

.field private final l:Lcom/google/android/gms/drive/g/ae;

.field private final m:Lcom/google/android/gms/drive/g/w;

.field private final n:Lcom/google/android/gms/drive/d/f;

.field private final o:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

.field private final p:Lcom/google/android/gms/drive/b/f;

.field private final q:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;

.field private final r:Lcom/google/android/gms/drive/events/w;

.field private final s:Lcom/google/android/gms/drive/api/b;

.field private final t:Lcom/google/android/gms/drive/events/t;

.field private final u:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

.field private final v:Lcom/google/android/gms/drive/e/b;

.field private final w:Lcom/google/android/gms/drive/b/d;

.field private final x:Lcom/google/android/gms/drive/b/b/e;

.field private final y:Lcom/google/android/gms/drive/auth/f;

.field private final z:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/google/android/gms/drive/g/j;->a:Lcom/google/android/gms/drive/g/j;

    sput-object v0, Lcom/google/android/gms/drive/g/aw;->c:Lcom/google/android/gms/drive/g/i;

    .line 57
    sget-object v0, Lcom/google/android/gms/drive/g/j;->b:Lcom/google/android/gms/drive/g/j;

    sput-object v0, Lcom/google/android/gms/drive/g/aw;->d:Lcom/google/android/gms/drive/g/i;

    .line 58
    sget-object v0, Lcom/google/android/gms/drive/g/j;->c:Lcom/google/android/gms/drive/g/j;

    sput-object v0, Lcom/google/android/gms/drive/g/aw;->e:Lcom/google/android/gms/drive/g/i;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 11

    .prologue
    .line 130
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->g:Landroid/content/Context;

    .line 132
    new-instance v0, Lcom/google/android/gms/drive/g/p;

    invoke-direct {v0, p1}, Lcom/google/android/gms/drive/g/p;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->f:Lcom/google/android/gms/drive/g/n;

    .line 133
    new-instance v0, Lcom/google/android/gms/drive/g/aj;

    invoke-direct {v0, p1}, Lcom/google/android/gms/drive/g/aj;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->l:Lcom/google/android/gms/drive/g/ae;

    .line 134
    new-instance v0, Lcom/google/android/gms/drive/g/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/drive/g/w;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->m:Lcom/google/android/gms/drive/g/w;

    .line 135
    new-instance v0, Lcom/google/android/gms/drive/api/b;

    invoke-direct {v0}, Lcom/google/android/gms/drive/api/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->s:Lcom/google/android/gms/drive/api/b;

    .line 136
    new-instance v0, Lcom/google/android/gms/drive/database/i;

    iget-object v1, p0, Lcom/google/android/gms/drive/g/aw;->l:Lcom/google/android/gms/drive/g/ae;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/drive/database/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/g/ae;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->h:Lcom/google/android/gms/drive/database/i;

    .line 137
    new-instance v0, Lcom/google/android/gms/drive/d/a/c;

    invoke-direct {v0, p1}, Lcom/google/android/gms/drive/d/a/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->n:Lcom/google/android/gms/drive/d/f;

    .line 138
    new-instance v0, Lcom/google/android/gms/drive/c/a/h;

    invoke-static {p1}, Lcom/google/android/gms/drive/g/aw;->b(Landroid/content/Context;)Lcom/google/android/gms/drive/c/a/n;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/drive/c/a/h;-><init>(Lcom/google/android/gms/drive/c/a/n;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->G:Lcom/google/android/gms/drive/c/c;

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->G:Lcom/google/android/gms/drive/c/c;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/c;->a()Lcom/google/android/gms/drive/c/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->H:Lcom/google/android/gms/drive/c/b;

    .line 141
    new-instance v0, Lcom/google/android/gms/drive/database/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/g/aw;->h:Lcom/google/android/gms/drive/database/i;

    sget-object v2, Lcom/google/android/gms/drive/g/aw;->c:Lcom/google/android/gms/drive/g/i;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/drive/database/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/g/i;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->i:Lcom/google/android/gms/drive/database/r;

    .line 142
    new-instance v0, Lcom/google/android/gms/drive/events/u;

    invoke-direct {v0, p1}, Lcom/google/android/gms/drive/events/u;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->E:Lcom/google/android/gms/drive/events/u;

    .line 143
    new-instance v0, Lcom/google/android/gms/drive/events/w;

    iget-object v2, p0, Lcom/google/android/gms/drive/g/aw;->h:Lcom/google/android/gms/drive/database/i;

    iget-object v3, p0, Lcom/google/android/gms/drive/g/aw;->i:Lcom/google/android/gms/drive/database/r;

    iget-object v4, p0, Lcom/google/android/gms/drive/g/aw;->E:Lcom/google/android/gms/drive/events/u;

    sget-object v5, Lcom/google/android/gms/drive/g/aw;->c:Lcom/google/android/gms/drive/g/i;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/events/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/events/u;Lcom/google/android/gms/drive/g/i;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->r:Lcom/google/android/gms/drive/events/w;

    .line 145
    new-instance v0, Lcom/google/android/gms/drive/events/t;

    invoke-direct {v0}, Lcom/google/android/gms/drive/events/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->t:Lcom/google/android/gms/drive/events/t;

    .line 146
    new-instance v0, Lcom/google/android/gms/drive/b/f;

    iget-object v1, p0, Lcom/google/android/gms/drive/g/aw;->h:Lcom/google/android/gms/drive/database/i;

    iget-object v2, p0, Lcom/google/android/gms/drive/g/aw;->i:Lcom/google/android/gms/drive/database/r;

    sget-object v3, Lcom/google/android/gms/drive/g/aw;->c:Lcom/google/android/gms/drive/g/i;

    iget-object v4, p0, Lcom/google/android/gms/drive/g/aw;->m:Lcom/google/android/gms/drive/g/w;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/drive/b/f;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/g/w;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->p:Lcom/google/android/gms/drive/b/f;

    .line 147
    new-instance v0, Lcom/google/android/gms/drive/events/l;

    iget-object v2, p0, Lcom/google/android/gms/drive/g/aw;->r:Lcom/google/android/gms/drive/events/w;

    iget-object v3, p0, Lcom/google/android/gms/drive/g/aw;->s:Lcom/google/android/gms/drive/api/b;

    iget-object v4, p0, Lcom/google/android/gms/drive/g/aw;->t:Lcom/google/android/gms/drive/events/t;

    iget-object v5, p0, Lcom/google/android/gms/drive/g/aw;->E:Lcom/google/android/gms/drive/events/u;

    iget-object v6, p0, Lcom/google/android/gms/drive/g/aw;->p:Lcom/google/android/gms/drive/b/f;

    sget-object v7, Lcom/google/android/gms/drive/g/aw;->c:Lcom/google/android/gms/drive/g/i;

    iget-object v8, p0, Lcom/google/android/gms/drive/g/aw;->f:Lcom/google/android/gms/drive/g/n;

    iget-object v9, p0, Lcom/google/android/gms/drive/g/aw;->H:Lcom/google/android/gms/drive/c/b;

    iget-object v10, p0, Lcom/google/android/gms/drive/g/aw;->i:Lcom/google/android/gms/drive/database/r;

    move-object v1, p1

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/drive/events/l;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/events/w;Lcom/google/android/gms/drive/api/b;Lcom/google/android/gms/drive/events/t;Lcom/google/android/gms/drive/events/u;Lcom/google/android/gms/drive/b/f;Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/c/b;Lcom/google/android/gms/drive/database/r;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->D:Lcom/google/android/gms/drive/events/k;

    .line 150
    new-instance v0, Lcom/google/android/gms/drive/b/d;

    iget-object v2, p0, Lcom/google/android/gms/drive/g/aw;->i:Lcom/google/android/gms/drive/database/r;

    iget-object v3, p0, Lcom/google/android/gms/drive/g/aw;->m:Lcom/google/android/gms/drive/g/w;

    iget-object v4, p0, Lcom/google/android/gms/drive/g/aw;->p:Lcom/google/android/gms/drive/b/f;

    iget-object v5, p0, Lcom/google/android/gms/drive/g/aw;->H:Lcom/google/android/gms/drive/c/b;

    sget-object v6, Lcom/google/android/gms/drive/g/aw;->c:Lcom/google/android/gms/drive/g/i;

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/b/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/w;Lcom/google/android/gms/drive/b/f;Lcom/google/android/gms/drive/c/b;Lcom/google/android/gms/drive/g/i;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->w:Lcom/google/android/gms/drive/b/d;

    .line 152
    new-instance v0, Lcom/google/android/gms/drive/a/a/a;

    iget-object v3, p0, Lcom/google/android/gms/drive/g/aw;->i:Lcom/google/android/gms/drive/database/r;

    iget-object v4, p0, Lcom/google/android/gms/drive/g/aw;->h:Lcom/google/android/gms/drive/database/i;

    iget-object v5, p0, Lcom/google/android/gms/drive/g/aw;->f:Lcom/google/android/gms/drive/g/n;

    iget-object v6, p0, Lcom/google/android/gms/drive/g/aw;->D:Lcom/google/android/gms/drive/events/k;

    iget-object v7, p0, Lcom/google/android/gms/drive/g/aw;->p:Lcom/google/android/gms/drive/b/f;

    iget-object v8, p0, Lcom/google/android/gms/drive/g/aw;->w:Lcom/google/android/gms/drive/b/d;

    move-object v1, p1

    move-object v2, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/drive/a/a/a;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/g/aw;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/events/k;Lcom/google/android/gms/drive/b/f;Lcom/google/android/gms/drive/b/d;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->k:Lcom/google/android/gms/drive/a/a/a;

    .line 154
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;

    iget-object v1, p0, Lcom/google/android/gms/drive/g/aw;->p:Lcom/google/android/gms/drive/b/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;-><init>(Lcom/google/android/gms/drive/b/f;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->q:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;

    .line 156
    new-instance v0, Lcom/google/android/gms/drive/api/i;

    iget-object v1, p0, Lcom/google/android/gms/drive/g/aw;->k:Lcom/google/android/gms/drive/a/a/a;

    iget-object v2, p0, Lcom/google/android/gms/drive/g/aw;->p:Lcom/google/android/gms/drive/b/f;

    sget-object v3, Lcom/google/android/gms/drive/g/aw;->c:Lcom/google/android/gms/drive/g/i;

    iget-object v4, p0, Lcom/google/android/gms/drive/g/aw;->w:Lcom/google/android/gms/drive/b/d;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/drive/api/i;-><init>(Lcom/google/android/gms/drive/a/a/a;Lcom/google/android/gms/drive/b/f;Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/b/d;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->j:Lcom/google/android/gms/drive/api/k;

    .line 158
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

    iget-object v2, p0, Lcom/google/android/gms/drive/g/aw;->q:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;

    iget-object v3, p0, Lcom/google/android/gms/drive/g/aw;->p:Lcom/google/android/gms/drive/b/f;

    iget-object v4, p0, Lcom/google/android/gms/drive/g/aw;->i:Lcom/google/android/gms/drive/database/r;

    iget-object v5, p0, Lcom/google/android/gms/drive/g/aw;->w:Lcom/google/android/gms/drive/b/d;

    new-instance v6, Lcom/google/android/gms/drive/b/b;

    invoke-direct {v6}, Lcom/google/android/gms/drive/b/b;-><init>()V

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/h;Lcom/google/android/gms/drive/b/f;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/b/d;Lcom/google/android/gms/drive/b/b;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->u:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

    .line 160
    new-instance v0, Lcom/google/android/gms/drive/e/b;

    iget-object v1, p0, Lcom/google/android/gms/drive/g/aw;->f:Lcom/google/android/gms/drive/g/n;

    iget-object v2, p0, Lcom/google/android/gms/drive/g/aw;->i:Lcom/google/android/gms/drive/database/r;

    iget-object v3, p0, Lcom/google/android/gms/drive/g/aw;->l:Lcom/google/android/gms/drive/g/ae;

    iget-object v4, p0, Lcom/google/android/gms/drive/g/aw;->u:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

    new-instance v5, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;

    iget-object v6, p0, Lcom/google/android/gms/drive/g/aw;->n:Lcom/google/android/gms/drive/d/f;

    iget-object v7, p0, Lcom/google/android/gms/drive/g/aw;->i:Lcom/google/android/gms/drive/database/r;

    sget-object v8, Lcom/google/android/gms/drive/g/aw;->c:Lcom/google/android/gms/drive/g/i;

    invoke-direct {v5, v6, v7, v8}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;-><init>(Lcom/google/android/gms/drive/d/f;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/i;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/e/b;-><init>(Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/ae;Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;Lcom/google/android/gms/drive/metadata/sync/syncadapter/n;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->v:Lcom/google/android/gms/drive/e/b;

    .line 163
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    iget-object v2, p0, Lcom/google/android/gms/drive/g/aw;->f:Lcom/google/android/gms/drive/g/n;

    sget-object v3, Lcom/google/android/gms/drive/g/aw;->c:Lcom/google/android/gms/drive/g/i;

    iget-object v4, p0, Lcom/google/android/gms/drive/g/aw;->l:Lcom/google/android/gms/drive/g/ae;

    iget-object v5, p0, Lcom/google/android/gms/drive/g/aw;->v:Lcom/google/android/gms/drive/e/b;

    move-object v1, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/g/n;Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/g/ae;Lcom/google/android/gms/drive/e/b;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->o:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    .line 165
    new-instance v0, Lcom/google/android/gms/drive/b/b/e;

    invoke-direct {v0}, Lcom/google/android/gms/drive/b/b/e;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->x:Lcom/google/android/gms/drive/b/b/e;

    .line 166
    new-instance v0, Lcom/google/android/gms/drive/auth/f;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/g/aw;->i:Lcom/google/android/gms/drive/database/r;

    sget-object v3, Lcom/google/android/gms/drive/g/aw;->c:Lcom/google/android/gms/drive/g/i;

    iget-object v4, p0, Lcom/google/android/gms/drive/g/aw;->n:Lcom/google/android/gms/drive/d/f;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/drive/auth/f;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/g/i;Lcom/google/android/gms/drive/d/f;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->y:Lcom/google/android/gms/drive/auth/f;

    .line 168
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->z:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 169
    new-instance v1, Lcom/google/android/gms/drive/b/a/e;

    sget-object v0, Lcom/google/android/gms/drive/ai;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sget-object v0, Lcom/google/android/gms/drive/ai;->r:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget-object v0, Lcom/google/android/gms/drive/ai;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    sget-object v0, Lcom/google/android/gms/drive/ai;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/drive/b/a/e;-><init>(IIDI)V

    iput-object v1, p0, Lcom/google/android/gms/drive/g/aw;->A:Lcom/google/android/gms/drive/b/a/e;

    .line 174
    new-instance v0, Lcom/google/android/gms/drive/b/b/d;

    invoke-direct {v0}, Lcom/google/android/gms/drive/b/b/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->B:Lcom/google/android/gms/drive/b/b/d;

    .line 175
    new-instance v0, Lcom/google/android/gms/drive/b/b/p;

    invoke-direct {v0}, Lcom/google/android/gms/drive/b/b/p;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->C:Lcom/google/android/gms/drive/b/b/p;

    .line 176
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/realtime/cache/w;-><init>(Lcom/google/android/gms/drive/g/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/g/aw;->F:Lcom/google/android/gms/drive/realtime/cache/w;

    .line 177
    return-void
.end method

.method public static E()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 329
    sget-object v0, Lcom/google/android/gms/drive/g/aw;->a:Lcom/google/android/gms/drive/g/aw;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/drive/g/aw;->a:Lcom/google/android/gms/drive/g/aw;

    .line 330
    :goto_0
    if-nez v0, :cond_1

    .line 342
    :goto_1
    return-void

    .line 329
    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/g/aw;->b:Lcom/google/android/gms/drive/g/aw;

    goto :goto_0

    .line 333
    :cond_1
    iget-object v1, v0, Lcom/google/android/gms/drive/g/aw;->i:Lcom/google/android/gms/drive/database/r;

    invoke-interface {v1}, Lcom/google/android/gms/drive/database/r;->f()V

    .line 334
    iget-object v1, v0, Lcom/google/android/gms/drive/g/aw;->o:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;->a()Ljava/util/List;

    .line 335
    iget-object v1, v0, Lcom/google/android/gms/drive/g/aw;->k:Lcom/google/android/gms/drive/a/a/a;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/a/a/a;->b()V

    .line 336
    iget-object v1, v0, Lcom/google/android/gms/drive/g/aw;->v:Lcom/google/android/gms/drive/e/b;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/e/b;->c()V

    .line 337
    iget-object v1, v0, Lcom/google/android/gms/drive/g/aw;->w:Lcom/google/android/gms/drive/b/d;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/b/d;->a()Lcom/google/android/gms/drive/g/al;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/drive/g/al;->b()V

    iget-object v1, v1, Lcom/google/android/gms/drive/b/d;->e:Ljava/util/concurrent/ExecutorService;

    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdownNow()Ljava/util/List;

    .line 338
    iget-object v0, v0, Lcom/google/android/gms/drive/g/aw;->E:Lcom/google/android/gms/drive/events/u;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/events/u;->a()V

    .line 340
    sput-object v3, Lcom/google/android/gms/drive/g/aw;->a:Lcom/google/android/gms/drive/g/aw;

    .line 341
    sput-object v3, Lcom/google/android/gms/drive/g/aw;->b:Lcom/google/android/gms/drive/g/aw;

    goto :goto_1
.end method

.method public static a()Lcom/google/android/gms/drive/g/aw;
    .locals 2

    .prologue
    .line 114
    sget-object v0, Lcom/google/android/gms/drive/g/aw;->a:Lcom/google/android/gms/drive/g/aw;

    if-eqz v0, :cond_0

    .line 115
    sget-object v0, Lcom/google/android/gms/drive/g/aw;->a:Lcom/google/android/gms/drive/g/aw;

    .line 118
    :goto_0
    return-object v0

    .line 117
    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/g/aw;->b:Lcom/google/android/gms/drive/g/aw;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    const-string v1, "Singletons not initialized"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 118
    sget-object v0, Lcom/google/android/gms/drive/g/aw;->b:Lcom/google/android/gms/drive/g/aw;

    goto :goto_0

    .line 117
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 97
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 98
    const-class v1, Lcom/google/android/gms/drive/g/aw;

    monitor-enter v1

    .line 99
    :try_start_0
    sget-object v2, Lcom/google/android/gms/drive/g/aw;->b:Lcom/google/android/gms/drive/g/aw;

    if-eqz v2, :cond_0

    .line 100
    sget-object v2, Lcom/google/android/gms/drive/g/aw;->b:Lcom/google/android/gms/drive/g/aw;

    iget-object v2, v2, Lcom/google/android/gms/drive/g/aw;->g:Landroid/content/Context;

    if-eq v2, v0, :cond_1

    .line 101
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "attempted to initialize Singletons multiple times with different application context instances."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 105
    :cond_0
    :try_start_1
    new-instance v2, Lcom/google/android/gms/drive/g/aw;

    invoke-direct {v2, v0}, Lcom/google/android/gms/drive/g/aw;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/google/android/gms/drive/g/aw;->b:Lcom/google/android/gms/drive/g/aw;

    .line 107
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public static b(Landroid/content/Context;)Lcom/google/android/gms/drive/c/a/n;
    .locals 1

    .prologue
    .line 349
    sget-object v0, Lcom/google/android/gms/drive/ai;->J:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 356
    new-instance v0, Lcom/google/android/gms/drive/c/a/k;

    invoke-direct {v0}, Lcom/google/android/gms/drive/c/a/k;-><init>()V

    :goto_0
    return-object v0

    .line 351
    :pswitch_0
    new-instance v0, Lcom/google/android/gms/drive/c/a/i;

    invoke-direct {v0}, Lcom/google/android/gms/drive/c/a/i;-><init>()V

    goto :goto_0

    .line 353
    :pswitch_1
    new-instance v0, Lcom/google/android/gms/drive/c/a/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/c/a/b;-><init>(Landroid/content/Context;)V

    goto :goto_0

    .line 349
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b()Lcom/google/android/gms/drive/g/i;
    .locals 1

    .prologue
    .line 180
    sget-object v0, Lcom/google/android/gms/drive/g/aw;->c:Lcom/google/android/gms/drive/g/i;

    return-object v0
.end method

.method public static c()Lcom/google/android/gms/drive/g/i;
    .locals 1

    .prologue
    .line 184
    sget-object v0, Lcom/google/android/gms/drive/g/aw;->d:Lcom/google/android/gms/drive/g/i;

    return-object v0
.end method

.method public static d()Lcom/google/android/gms/drive/g/i;
    .locals 1

    .prologue
    .line 188
    sget-object v0, Lcom/google/android/gms/drive/g/aw;->e:Lcom/google/android/gms/drive/g/i;

    return-object v0
.end method


# virtual methods
.method public final A()Lcom/google/android/gms/drive/b/b/d;
    .locals 1

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->B:Lcom/google/android/gms/drive/b/b/d;

    return-object v0
.end method

.method public final B()Lcom/google/android/gms/drive/b/b/p;
    .locals 1

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->C:Lcom/google/android/gms/drive/b/b/p;

    return-object v0
.end method

.method public final C()Lcom/google/android/gms/drive/events/k;
    .locals 1

    .prologue
    .line 314
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->D:Lcom/google/android/gms/drive/events/k;

    return-object v0
.end method

.method public final D()Lcom/google/android/gms/drive/realtime/cache/w;
    .locals 1

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->F:Lcom/google/android/gms/drive/realtime/cache/w;

    return-object v0
.end method

.method public final e()Lcom/google/android/gms/drive/g/n;
    .locals 1

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->f:Lcom/google/android/gms/drive/g/n;

    return-object v0
.end method

.method public final f()Landroid/content/Context;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->g:Landroid/content/Context;

    return-object v0
.end method

.method public final g()Lcom/google/android/gms/drive/database/i;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->h:Lcom/google/android/gms/drive/database/i;

    return-object v0
.end method

.method public final h()Lcom/google/android/gms/drive/database/r;
    .locals 1

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->i:Lcom/google/android/gms/drive/database/r;

    return-object v0
.end method

.method public final i()Lcom/google/android/gms/drive/c/c;
    .locals 1

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->G:Lcom/google/android/gms/drive/c/c;

    return-object v0
.end method

.method public final j()Lcom/google/android/gms/drive/c/b;
    .locals 1

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->H:Lcom/google/android/gms/drive/c/b;

    return-object v0
.end method

.method public final k()Lcom/google/android/gms/drive/api/k;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->j:Lcom/google/android/gms/drive/api/k;

    return-object v0
.end method

.method public final l()Lcom/google/android/gms/drive/a/a/a;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->k:Lcom/google/android/gms/drive/a/a/a;

    return-object v0
.end method

.method public final m()Lcom/google/android/gms/drive/g/ae;
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->l:Lcom/google/android/gms/drive/g/ae;

    return-object v0
.end method

.method public final n()Lcom/google/android/gms/drive/g/w;
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->m:Lcom/google/android/gms/drive/g/w;

    return-object v0
.end method

.method public final o()Lcom/google/android/gms/drive/d/f;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->n:Lcom/google/android/gms/drive/d/f;

    return-object v0
.end method

.method public final p()Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->o:Lcom/google/android/gms/drive/metadata/sync/syncadapter/q;

    return-object v0
.end method

.method public final q()Lcom/google/android/gms/drive/b/f;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->p:Lcom/google/android/gms/drive/b/f;

    return-object v0
.end method

.method public final r()Lcom/google/android/gms/drive/events/w;
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->r:Lcom/google/android/gms/drive/events/w;

    return-object v0
.end method

.method public final s()Lcom/google/android/gms/drive/api/b;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->s:Lcom/google/android/gms/drive/api/b;

    return-object v0
.end method

.method public final t()Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->u:Lcom/google/android/gms/drive/metadata/sync/syncadapter/a/f;

    return-object v0
.end method

.method public final u()Lcom/google/android/gms/drive/e/b;
    .locals 1

    .prologue
    .line 282
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->v:Lcom/google/android/gms/drive/e/b;

    return-object v0
.end method

.method public final v()Lcom/google/android/gms/drive/b/d;
    .locals 1

    .prologue
    .line 286
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->w:Lcom/google/android/gms/drive/b/d;

    return-object v0
.end method

.method public final w()Lcom/google/android/gms/drive/b/b/e;
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->x:Lcom/google/android/gms/drive/b/b/e;

    return-object v0
.end method

.method public final x()Lcom/google/android/gms/drive/auth/f;
    .locals 1

    .prologue
    .line 294
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->y:Lcom/google/android/gms/drive/auth/f;

    return-object v0
.end method

.method public final y()Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->z:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method public final z()Lcom/google/android/gms/drive/b/a/e;
    .locals 1

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/android/gms/drive/g/aw;->A:Lcom/google/android/gms/drive/b/a/e;

    return-object v0
.end method
