.class public final Lcom/google/android/gms/people/profile/g;
.super Landroid/support/v4/app/m;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/DialogInterface$OnKeyListener;


# static fields
.field private static j:[Ljava/lang/String;

.field private static k:Ljava/lang/String;

.field private static l:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    .line 33
    return-void
.end method

.method private static a(Landroid/content/Context;)[Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 94
    sget-object v0, Lcom/google/android/gms/people/profile/g;->j:[Ljava/lang/String;

    if-nez v0, :cond_1

    .line 96
    sget v0, Lcom/google/android/gms/p;->rF:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/profile/g;->k:Ljava/lang/String;

    .line 97
    sget v0, Lcom/google/android/gms/p;->rE:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/profile/g;->l:Ljava/lang/String;

    .line 100
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 101
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/high16 v3, 0x10000

    invoke-virtual {v2, v0, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    .line 103
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    const/4 v0, 0x1

    .line 106
    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 107
    if-eqz v0, :cond_0

    .line 108
    sget-object v0, Lcom/google/android/gms/people/profile/g;->k:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 110
    :cond_0
    sget-object v0, Lcom/google/android/gms/people/profile/g;->l:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 111
    new-array v0, v1, [Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/people/profile/g;->j:[Ljava/lang/String;

    .line 113
    :cond_1
    sget-object v0, Lcom/google/android/gms/people/profile/g;->j:[Ljava/lang/String;

    return-object v0

    :cond_2
    move v0, v1

    .line 103
    goto :goto_0
.end method

.method public static b()Lcom/google/android/gms/people/profile/g;
    .locals 1

    .prologue
    .line 44
    new-instance v0, Lcom/google/android/gms/people/profile/g;

    invoke-direct {v0}, Lcom/google/android/gms/people/profile/g;-><init>()V

    return-object v0
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 49
    new-instance v0, Landroid/app/AlertDialog$Builder;

    new-instance v1, Landroid/view/ContextThemeWrapper;

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    sget v3, Lcom/google/android/gms/q;->j:I

    invoke-direct {v1, v2, v3}, Landroid/view/ContextThemeWrapper;-><init>(Landroid/content/Context;I)V

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 51
    sget v1, Lcom/google/android/gms/p;->rG:I

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/people/profile/g;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p0}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 55
    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 56
    invoke-virtual {v0, v4}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 57
    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 58
    invoke-virtual {v0, p0}, Landroid/app/Dialog;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)V

    .line 59
    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 79
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/profile/h;

    invoke-interface {v0}, Lcom/google/android/gms/people/profile/h;->c()V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/g;->dismiss()V

    .line 81
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    .prologue
    .line 64
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/profile/g;->a(Landroid/content/Context;)[Ljava/lang/String;

    move-result-object v0

    aget-object v0, v0, p2

    .line 65
    sget-object v1, Lcom/google/android/gms/people/profile/g;->k:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/profile/h;

    invoke-interface {v0}, Lcom/google/android/gms/people/profile/h;->a()V

    .line 67
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/g;->dismiss()V

    .line 70
    :goto_0
    return-void

    .line 68
    :cond_0
    sget-object v1, Lcom/google/android/gms/people/profile/g;->l:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/profile/h;

    invoke-interface {v0}, Lcom/google/android/gms/people/profile/h;->b()V

    .line 70
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/g;->dismiss()V

    goto :goto_0

    .line 72
    :cond_1
    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized avatar source option: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final onKey(Landroid/content/DialogInterface;ILandroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 85
    const/4 v0, 0x4

    if-ne p2, v0, :cond_0

    .line 86
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/g;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/profile/h;

    invoke-interface {v0}, Lcom/google/android/gms/people/profile/h;->c()V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/g;->dismiss()V

    .line 88
    const/4 v0, 0x1

    .line 90
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
