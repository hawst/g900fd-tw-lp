.class final Lcom/google/android/gms/fitness/sensors/a/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/k/k/a/af;

.field final synthetic b:Lcom/google/android/gms/fitness/sensors/a/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/sensors/a/e;Lcom/google/k/k/a/af;)V
    .locals 0

    .prologue
    .line 511
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/a/h;->b:Lcom/google/android/gms/fitness/sensors/a/e;

    iput-object p2, p0, Lcom/google/android/gms/fitness/sensors/a/h;->a:Lcom/google/k/k/a/af;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 514
    const-string v0, "Checking if connection is still in progress."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 515
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/a/h;->b:Lcom/google/android/gms/fitness/sensors/a/e;

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/a/h;->a:Lcom/google/k/k/a/af;

    iget-object v2, v0, Lcom/google/android/gms/fitness/sensors/a/e;->j:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-object v3, v0, Lcom/google/android/gms/fitness/sensors/a/e;->k:Lcom/google/k/k/a/aq;

    if-ne v3, v1, :cond_0

    const-string v1, "Connection is still in progress. Resetting connection."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/sensors/a/e;->a()Lcom/google/k/k/a/af;

    move-result-object v1

    new-instance v3, Lcom/google/android/gms/fitness/sensors/a/i;

    invoke-direct {v3, v0}, Lcom/google/android/gms/fitness/sensors/a/i;-><init>(Lcom/google/android/gms/fitness/sensors/a/e;)V

    invoke-static {v1, v3}, Lcom/google/k/k/a/n;->a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/m;)V

    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
