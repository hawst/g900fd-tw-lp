.class public final Lcom/google/android/gms/wallet/common/l;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/String;Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/common/DisplayHints;
    .locals 7

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x0

    .line 36
    :try_start_0
    const-string v0, "\\."

    invoke-virtual {p0, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 37
    array-length v2, v0

    if-eq v2, v3, :cond_0

    .line 38
    const-string v2, "JwtParser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Wrong number of components in jwt, expected 3, got "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v0, v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 72
    :goto_0
    return-object v0

    .line 41
    :cond_0
    const/4 v2, 0x1

    aget-object v0, v0, v2
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_2

    .line 44
    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/common/util/m;->b(Ljava/lang/String;)[B
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    .line 49
    :goto_1
    :try_start_2
    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/String;-><init>([B)V

    .line 50
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 51
    const-string v0, "typ"

    invoke-virtual {v3, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 52
    const-string v2, "/"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    array-length v4, v2

    const/4 v5, 0x5

    if-eq v4, v5, :cond_1

    const-string v2, "JwtTypeParser"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Expected 5 components in \"typ\" field: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 53
    :goto_2
    if-nez v0, :cond_2

    move-object v0, v1

    .line 54
    goto :goto_0

    .line 46
    :catch_0
    move-exception v2

    const-string v2, "JwtParser"

    const-string v3, "JWT decoding failed using base64_urlsafe, trying base64_default"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 47
    invoke-static {v0}, Lcom/google/android/gms/common/util/m;->a(Ljava/lang/String;)[B

    move-result-object v0

    goto :goto_1

    .line 52
    :cond_1
    new-instance v0, Lcom/google/android/gms/wallet/common/n;

    const/4 v4, 0x2

    aget-object v4, v2, v4

    invoke-static {v4}, Lcom/google/android/gms/wallet/common/r;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/o;

    move-result-object v4

    const/4 v5, 0x3

    aget-object v5, v2, v5

    invoke-static {v5}, Lcom/google/android/gms/wallet/common/r;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/p;

    move-result-object v5

    const/4 v6, 0x4

    aget-object v2, v2, v6

    invoke-static {v2}, Lcom/google/android/gms/wallet/common/r;->c(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/q;

    move-result-object v2

    invoke-direct {v0, v4, v5, v2}, Lcom/google/android/gms/wallet/common/n;-><init>(Lcom/google/android/gms/wallet/common/o;Lcom/google/android/gms/wallet/common/p;Lcom/google/android/gms/wallet/common/q;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_2

    .line 68
    :catch_1
    move-exception v0

    const-string v0, "JwtParser"

    const-string v2, "Invalid JSON in Jwt"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 69
    goto :goto_0

    .line 56
    :cond_2
    :try_start_3
    sget-object v2, Lcom/google/android/gms/wallet/common/m;->a:[I

    iget-object v4, v0, Lcom/google/android/gms/wallet/common/n;->a:Lcom/google/android/gms/wallet/common/p;

    invoke-virtual {v4}, Lcom/google/android/gms/wallet/common/p;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    .line 65
    if-nez p1, :cond_3

    move-object v0, v1

    goto :goto_0

    .line 60
    :pswitch_0
    const-string v2, "reviewPurchaseTitle"

    const-string v4, ""

    invoke-virtual {v3, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "request"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/gms/wallet/common/l;->b(Lorg/json/JSONObject;Lcom/google/android/gms/wallet/common/n;)Lcom/google/checkout/inapp/proto/f;

    move-result-object v0

    new-instance v3, Lcom/google/checkout/inapp/proto/d;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/d;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [Lcom/google/checkout/inapp/proto/f;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    iput-object v4, v3, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    invoke-static {v3}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/d;)V

    new-instance v0, Lcom/google/android/gms/wallet/common/DisplayHints;

    const-string v4, ""

    invoke-direct {v0, v4, v2, v3}, Lcom/google/android/gms/wallet/common/DisplayHints;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/checkout/inapp/proto/d;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 70
    :catch_2
    move-exception v0

    .line 71
    const-string v2, "JwtParser"

    const-string v3, "Invalid numeric field in JWT item"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 72
    goto/16 :goto_0

    .line 62
    :pswitch_1
    :try_start_4
    invoke-static {v3, v0}, Lcom/google/android/gms/wallet/common/l;->a(Lorg/json/JSONObject;Lcom/google/android/gms/wallet/common/n;)Lcom/google/android/gms/wallet/common/DisplayHints;

    move-result-object v0

    goto/16 :goto_0

    .line 65
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->f()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "com.google.android.libraries.inapp.EXTRA_DISPLAY_HINTS"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Landroid/os/Bundle;

    if-eqz v2, :cond_4

    check-cast v0, Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/g;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/common/DisplayHints;

    move-result-object v0

    :goto_3
    const-string v2, "reviewPurchaseTitle"

    const-string v4, ""

    invoke-virtual {v3, v2, v4}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/android/gms/wallet/common/DisplayHints;

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-direct {v0, v3, v2, v4}, Lcom/google/android/gms/wallet/common/DisplayHints;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/checkout/inapp/proto/d;)V

    goto/16 :goto_0

    :cond_4
    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_5

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/wallet/common/g;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/common/DisplayHints;

    move-result-object v0

    goto :goto_3

    :cond_5
    if-eqz v0, :cond_6

    const-string v0, "JwtParser"

    const-string v2, "Display hints object is not a Bundle or Json String"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move-object v0, v1

    goto :goto_3

    :cond_7
    invoke-virtual {v0, v2}, Lcom/google/android/gms/wallet/common/DisplayHints;->a(Ljava/lang/String;)V
    :try_end_4
    .catch Lorg/json/JSONException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_2

    goto/16 :goto_0

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lorg/json/JSONObject;Lcom/google/android/gms/wallet/common/n;)Lcom/google/android/gms/wallet/common/DisplayHints;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 94
    const-string v0, "reviewPurchaseTitle"

    const-string v2, ""

    invoke-virtual {p0, v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 95
    const-string v0, "request"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 96
    const-string v2, "items"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v4

    .line 97
    new-instance v5, Lcom/google/checkout/inapp/proto/d;

    invoke-direct {v5}, Lcom/google/checkout/inapp/proto/d;-><init>()V

    .line 99
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 100
    const/4 v0, 0x0

    .line 117
    :goto_0
    return-object v0

    .line 102
    :cond_0
    invoke-virtual {v4, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "currencyCode"

    invoke-virtual {v0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 103
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v7

    .line 104
    new-array v0, v7, [Lcom/google/checkout/inapp/proto/f;

    iput-object v0, v5, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    move v2, v1

    .line 105
    :goto_1
    if-ge v2, v7, :cond_4

    .line 106
    invoke-virtual {v4, v2}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    .line 107
    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/common/l;->b(Lorg/json/JSONObject;Lcom/google/android/gms/wallet/common/n;)Lcom/google/checkout/inapp/proto/f;

    move-result-object v8

    .line 108
    iget-object v0, v8, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_1

    iget-object v0, v8, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;

    if-nez v0, :cond_2

    :cond_1
    move v0, v1

    :goto_2
    if-nez v0, :cond_3

    .line 109
    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Jwt items don\'t have consistent currency code!"

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 108
    :cond_2
    iget-object v0, v8, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_2

    .line 111
    :cond_3
    iget-object v0, v5, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    aput-object v8, v0, v2

    .line 105
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 115
    :cond_4
    invoke-static {v5}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/d;)V

    .line 117
    new-instance v0, Lcom/google/android/gms/wallet/common/DisplayHints;

    const-string v1, ""

    invoke-direct {v0, v1, v3, v5}, Lcom/google/android/gms/wallet/common/DisplayHints;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/checkout/inapp/proto/d;)V

    goto :goto_0
.end method

.method private static b(Lorg/json/JSONObject;Lcom/google/android/gms/wallet/common/n;)Lcom/google/checkout/inapp/proto/f;
    .locals 8

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 148
    sget-object v0, Lcom/google/android/gms/wallet/common/m;->a:[I

    iget-object v2, p1, Lcom/google/android/gms/wallet/common/n;->a:Lcom/google/android/gms/wallet/common/p;

    invoke-virtual {v2}, Lcom/google/android/gms/wallet/common/p;->ordinal()I

    move-result v2

    aget v0, v0, v2

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    .line 149
    :goto_0
    sget-object v2, Lcom/google/android/gms/wallet/common/m;->a:[I

    iget-object v3, p1, Lcom/google/android/gms/wallet/common/n;->a:Lcom/google/android/gms/wallet/common/p;

    invoke-virtual {v3}, Lcom/google/android/gms/wallet/common/p;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_1

    :pswitch_1
    move-object v2, v1

    .line 150
    :goto_1
    const-string v3, "quantity"

    invoke-virtual {p0, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 151
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_2

    move v3, v4

    .line 155
    :goto_2
    new-instance v5, Lcom/google/checkout/inapp/proto/f;

    invoke-direct {v5}, Lcom/google/checkout/inapp/proto/f;-><init>()V

    .line 156
    iput-object v2, v5, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    .line 157
    iput-object v0, v5, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    .line 158
    iput v3, v5, Lcom/google/checkout/inapp/proto/f;->e:I

    .line 160
    const-string v0, "currencyCode"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 161
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 162
    const-string v2, "price"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 164
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 165
    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/d;

    move-result-object v1

    .line 166
    if-eqz v1, :cond_0

    .line 167
    iput-object v1, v5, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    .line 171
    :cond_0
    if-le v3, v4, :cond_1

    .line 172
    const-string v2, "net_cost"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 173
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 174
    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/w;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/checkout/inapp/proto/a/d;

    move-result-object v0

    .line 175
    if-eqz v0, :cond_1

    .line 176
    iput-object v0, v5, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    .line 185
    :cond_1
    :goto_3
    return-object v5

    .line 148
    :pswitch_2
    const-string v0, "name"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    const-string v0, "unsignedName"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 149
    :pswitch_4
    const-string v2, "description"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :pswitch_5
    const-string v2, "unsignedDescription"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    .line 151
    :cond_2
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    goto :goto_2

    .line 178
    :cond_3
    if-eqz v1, :cond_1

    .line 179
    new-instance v2, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v2}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    iput-object v2, v5, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    .line 180
    iget-object v2, v5, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    iput-object v0, v2, Lcom/google/checkout/inapp/proto/a/d;->b:Ljava/lang/String;

    .line 181
    iget-object v0, v5, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    int-to-long v2, v3

    iget-wide v6, v1, Lcom/google/checkout/inapp/proto/a/d;->a:J

    mul-long/2addr v2, v6

    iput-wide v2, v0, Lcom/google/checkout/inapp/proto/a/d;->a:J

    goto :goto_3

    .line 148
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch

    .line 149
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method
