.class public final Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;
.super Lcom/google/android/gms/games/ui/s;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/common/matches/e;
.implements Lcom/google/android/gms/games/ui/common/players/e;


# instance fields
.field private l:Lcom/google/android/gms/games/ui/bn;

.field private m:Lcom/google/android/gms/games/ui/common/players/d;

.field private n:Lcom/google/android/gms/games/ui/bn;

.field private o:Lcom/google/android/gms/games/ui/common/matches/d;

.field private p:Lcom/google/android/gms/games/ui/common/matches/e;

.field private q:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

.field private r:Ljava/lang/String;

.field private s:Lcom/google/android/gms/games/Player;

.field private t:Ljava/util/ArrayList;

.field private u:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/s;-><init>()V

    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->t:Ljava/util/ArrayList;

    .line 72
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->u:Z

    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->o:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/matches/d;->e()I

    move-result v0

    .line 196
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->n:Lcom/google/android/gms/games/ui/bn;

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/bn;->c(Z)V

    .line 197
    return-void

    .line 196
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x1

    .line 200
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->o:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/common/matches/d;->e()I

    move-result v1

    if-nez v1, :cond_0

    .line 203
    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->t:Ljava/util/ArrayList;

    .line 204
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->u:Z

    .line 216
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->t:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->t:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_4

    .line 217
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->t:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 218
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 219
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_2

    .line 220
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->t:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 219
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 208
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->t:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    .line 209
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->t:Ljava/util/ArrayList;

    .line 211
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->t:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 223
    :cond_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 224
    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->q:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 225
    const-string v1, "com.google.android.gms.games.REMOVED_ID_LIST"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 232
    :cond_3
    :goto_2
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->d:Lcom/google/android/gms/games/ui/q;

    const/16 v2, 0x384

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/games/ui/q;->setResult(ILandroid/content/Intent;)V

    .line 233
    return-void

    .line 226
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->u:Z

    if-eqz v1, :cond_3

    .line 227
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 228
    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->q:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 229
    const-string v1, "com.google.android.gms.games.REMOVE_CLUSTER"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_2
.end method


# virtual methods
.method public final B()V
    .locals 0

    .prologue
    .line 275
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 4

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->m:Lcom/google/android/gms/games/ui/common/players/d;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->q:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/Player;)V

    .line 140
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->u:Z

    if-eqz v0, :cond_1

    .line 142
    new-instance v0, Lcom/google/android/gms/common/data/w;

    invoke-direct {v0}, Lcom/google/android/gms/common/data/w;-><init>()V

    .line 153
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->o:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/common/matches/d;->a(Lcom/google/android/gms/common/data/d;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->h:Lcom/google/android/gms/games/ui/d/p;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/p;->b(I)V

    .line 155
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->a()V

    .line 156
    return-void

    .line 144
    :cond_1
    new-instance v0, Lcom/google/android/gms/common/data/w;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->q:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->d()Ljava/util/ArrayList;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/data/w;-><init>(Ljava/util/ArrayList;)V

    .line 145
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->t:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    .line 147
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->t:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v1, v2, :cond_0

    .line 148
    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->t:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/data/w;->b(Ljava/lang/Object;)V

    .line 147
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 237
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->p:Lcom/google/android/gms/games/ui/common/matches/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/e;->a(Lcom/google/android/gms/games/Game;)V

    .line 238
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/Player;)V
    .locals 4

    .prologue
    .line 265
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->s:Lcom/google/android/gms/games/Player;

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->d:Lcom/google/android/gms/games/ui/q;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->r:Ljava/lang/String;

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    .line 268
    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/support/v4/app/Fragment;Landroid/content/Intent;I)V

    .line 270
    return-void
.end method

.method public final varargs a(Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bh;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/ay;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 256
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->startActivity(Landroid/content/Intent;)V

    .line 261
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/Player;[Landroid/util/Pair;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->p:Lcom/google/android/gms/games/ui/common/matches/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/e;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 172
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->p:Lcom/google/android/gms/games/ui/common/matches/e;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/games/ui/common/matches/e;->a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->p:Lcom/google/android/gms/games/ui/common/matches/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/e;->b(Lcom/google/android/gms/games/Game;)V

    .line 243
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->p:Lcom/google/android/gms/games/ui/common/matches/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/e;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 177
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->a()V

    .line 180
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->b(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 181
    return-void
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->p:Lcom/google/android/gms/games/ui/common/matches/e;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/ui/common/matches/e;->d(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 186
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->a()V

    .line 189
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->b(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 190
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x1

    return v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 76
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/s;->onActivityCreated(Landroid/os/Bundle;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->d:Lcom/google/android/gms/games/ui/q;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/common/matches/ab;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->d:Lcom/google/android/gms/games/ui/q;

    check-cast v0, Lcom/google/android/gms/games/ui/common/matches/ab;

    .line 81
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/matches/ab;->U()Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->q:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    .line 82
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/matches/ab;->V()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->r:Ljava/lang/String;

    .line 83
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->q:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v2

    .line 84
    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v1

    const-string v3, "Must have a valid player to show cluster!"

    invoke-static {v1, v3}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 86
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->d:Lcom/google/android/gms/games/ui/q;

    instance-of v1, v1, Lcom/google/android/gms/games/ui/common/matches/v;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 87
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->d:Lcom/google/android/gms/games/ui/q;

    check-cast v1, Lcom/google/android/gms/games/ui/common/matches/v;

    invoke-interface {v1}, Lcom/google/android/gms/games/ui/common/matches/v;->T()Lcom/google/android/gms/games/ui/common/matches/u;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->p:Lcom/google/android/gms/games/ui/common/matches/e;

    .line 88
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->p:Lcom/google/android/gms/games/ui/common/matches/e;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 90
    new-instance v1, Lcom/google/android/gms/games/ui/bn;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v1, v3}, Lcom/google/android/gms/games/ui/bn;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->l:Lcom/google/android/gms/games/ui/bn;

    .line 91
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->l:Lcom/google/android/gms/games/ui/bn;

    sget v3, Lcom/google/android/gms/p;->iQ:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-interface {v2}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v5

    invoke-virtual {p0, v3, v4}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/bn;->a(Ljava/lang/String;)V

    .line 93
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->l:Lcom/google/android/gms/games/ui/bn;

    sget v2, Lcom/google/android/gms/p;->iP:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/bn;->h(I)V

    .line 95
    new-instance v1, Lcom/google/android/gms/games/ui/common/players/d;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v1, v2, p0}, Lcom/google/android/gms/games/ui/common/players/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/common/players/e;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->m:Lcom/google/android/gms/games/ui/common/players/d;

    .line 98
    new-instance v1, Lcom/google/android/gms/games/ui/bn;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/ui/bn;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->n:Lcom/google/android/gms/games/ui/bn;

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->n:Lcom/google/android/gms/games/ui/bn;

    sget v2, Lcom/google/android/gms/p;->iN:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/bn;->f(I)V

    .line 101
    new-instance v1, Lcom/google/android/gms/games/ui/common/matches/d;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->d:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v1, v2, p0}, Lcom/google/android/gms/games/ui/common/matches/d;-><init>(Lcom/google/android/gms/games/ui/q;Lcom/google/android/gms/games/ui/common/matches/e;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->o:Lcom/google/android/gms/games/ui/common/matches/d;

    .line 103
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->o:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/common/matches/ab;->W()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->r:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/games/ui/common/matches/d;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 106
    new-instance v0, Lcom/google/android/gms/games/ui/by;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/by;-><init>()V

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->l:Lcom/google/android/gms/games/ui/bn;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 108
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->m:Lcom/google/android/gms/games/ui/common/players/d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 109
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->n:Lcom/google/android/gms/games/ui/bn;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 110
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->o:Lcom/google/android/gms/games/ui/common/matches/d;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/by;->a(Lcom/google/android/gms/games/ui/ac;)Lcom/google/android/gms/games/ui/by;

    .line 111
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/by;->a()Lcom/google/android/gms/games/ui/bw;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->a(Landroid/support/v7/widget/bv;)V

    .line 113
    if-eqz p1, :cond_0

    .line 114
    const-string v0, "savedStateRemovedIdList"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->t:Ljava/util/ArrayList;

    .line 116
    const-string v0, "savedStateRemoveCluster"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->u:Z

    .line 118
    :cond_0
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 160
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/s;->onActivityResult(IILandroid/content/Intent;)V

    .line 162
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->s:Lcom/google/android/gms/games/Player;

    invoke-static {v0, p3}, Lcom/google/android/gms/games/ui/common/players/d;->a(Lcom/google/android/gms/games/Player;Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->m:Lcom/google/android/gms/games/ui/common/players/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/common/players/d;->d()V

    .line 167
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 127
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/s;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 129
    const-string v0, "savedStateRemovedIdList"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->t:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 130
    const-string v0, "savedStateRemoveCluster"

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/common/matches/PublicInvitationFragment;->u:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 131
    return-void
.end method
