.class public Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Landroid/accounts/Account;

.field private b:Lcom/google/checkout/inapp/proto/ac;

.field private c:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/gms/wallet/service/ia/h;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/service/ia/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/accounts/Account;Lcom/google/checkout/inapp/proto/ac;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->a:Landroid/accounts/Account;

    .line 24
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->b:Lcom/google/checkout/inapp/proto/ac;

    .line 25
    return-void
.end method

.method private constructor <init>(Landroid/accounts/Account;[B)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->a:Landroid/accounts/Account;

    .line 29
    iput-object p2, p0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->c:[B

    .line 30
    return-void
.end method

.method synthetic constructor <init>(Landroid/accounts/Account;[BB)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;-><init>(Landroid/accounts/Account;[B)V

    return-void
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->a:Landroid/accounts/Account;

    return-object v0
.end method

.method public final b()Lcom/google/checkout/inapp/proto/ac;
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->b:Lcom/google/checkout/inapp/proto/ac;

    if-nez v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->c:[B

    const-class v1, Lcom/google/checkout/inapp/proto/ac;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/ac;

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->b:Lcom/google/checkout/inapp/proto/ac;

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->b:Lcom/google/checkout/inapp/proto/ac;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->a:Landroid/accounts/Account;

    invoke-virtual {v0, p1, p2}, Landroid/accounts/Account;->writeToParcel(Landroid/os/Parcel;I)V

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->c:[B

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->b:Lcom/google/checkout/inapp/proto/ac;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->c:[B

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/service/ia/EnrollWithBrokerRequest;->c:[B

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByteArray([B)V

    .line 75
    return-void
.end method
