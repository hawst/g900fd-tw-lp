.class public final Lcom/google/android/gms/fitness/apiary/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/sync/d;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/fitness/apiary/l;

.field private final c:Lcom/google/android/gms/fitness/apiary/j;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/android/gms/common/server/ClientContext;

.field private final f:Lcom/google/android/gms/fitness/l/z;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/fitness/l/z;Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    iput-object p1, p0, Lcom/google/android/gms/fitness/apiary/m;->f:Lcom/google/android/gms/fitness/l/z;

    .line 54
    iput-object p2, p0, Lcom/google/android/gms/fitness/apiary/m;->a:Landroid/content/Context;

    .line 55
    new-instance v1, Lcom/google/android/gms/fitness/apiary/l;

    new-instance v2, Lcom/google/android/gms/fitness/apiary/k;

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, p2, v0}, Lcom/google/android/gms/fitness/apiary/k;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/google/android/gms/fitness/apiary/l;-><init>(Lcom/google/android/gms/common/server/q;)V

    iput-object v1, p0, Lcom/google/android/gms/fitness/apiary/m;->b:Lcom/google/android/gms/fitness/apiary/l;

    .line 56
    new-instance v1, Lcom/google/android/gms/fitness/apiary/j;

    new-instance v2, Lcom/google/android/gms/fitness/apiary/k;

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v2, p2, v0}, Lcom/google/android/gms/fitness/apiary/k;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-direct {v1, v2}, Lcom/google/android/gms/fitness/apiary/j;-><init>(Lcom/google/android/gms/common/server/q;)V

    iput-object v1, p0, Lcom/google/android/gms/fitness/apiary/m;->c:Lcom/google/android/gms/fitness/apiary/j;

    .line 58
    iput-object p3, p0, Lcom/google/android/gms/fitness/apiary/m;->d:Ljava/lang/String;

    .line 59
    new-instance v1, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v0, p3, p3, v2}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/fitness/apiary/m;->e:Lcom/google/android/gms/common/server/ClientContext;

    .line 60
    return-void
.end method

.method private static a(Lcom/android/volley/ac;)Lcom/google/android/gms/fitness/sync/g;
    .locals 2

    .prologue
    .line 368
    iget-object v0, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-nez v0, :cond_0

    .line 369
    new-instance v0, Lcom/google/android/gms/fitness/sync/g;

    const-string v1, "no network"

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/fitness/sync/g;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 377
    :goto_0
    return-object v0

    .line 372
    :cond_0
    iget-object v0, p0, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    iget v0, v0, Lcom/android/volley/m;->a:I

    .line 373
    rem-int/lit8 v1, v0, 0x64

    sub-int/2addr v0, v1

    .line 376
    const/16 v1, 0x190

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    .line 377
    :goto_1
    new-instance v1, Lcom/google/android/gms/fitness/sync/g;

    invoke-direct {v1, v0, p0}, Lcom/google/android/gms/fitness/sync/g;-><init>(ZLjava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0

    .line 376
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private static a(Lcom/google/android/gms/auth/q;)Lcom/google/android/gms/fitness/sync/g;
    .locals 1

    .prologue
    .line 360
    new-instance v0, Lcom/google/android/gms/fitness/sync/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/fitness/sync/g;-><init>(Lcom/google/android/gms/auth/q;)V

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/fitness/l/ae;)Lcom/google/android/gms/fitness/sync/g;
    .locals 2

    .prologue
    .line 364
    new-instance v0, Lcom/google/android/gms/fitness/sync/g;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/fitness/sync/g;-><init>(ZLjava/lang/Throwable;)V

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/data/DataSource;Ljava/lang/String;)Lcom/google/android/gms/fitness/apiary/a;
    .locals 7

    .prologue
    .line 92
    new-instance v3, Lcom/google/af/a/b/a/a/v;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/v;-><init>()V

    .line 93
    const-string v0, "me"

    iput-object v0, v3, Lcom/google/af/a/b/a/a/v;->a:Ljava/lang/String;

    .line 94
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v3, Lcom/google/af/a/b/a/a/v;->b:Ljava/lang/String;

    .line 95
    if-eqz p2, :cond_0

    .line 96
    iput-object p2, v3, Lcom/google/af/a/b/a/a/v;->c:Ljava/lang/String;

    .line 103
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/m;->c:Lcom/google/android/gms/fitness/apiary/j;

    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/m;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/fitness/apiary/j;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/users/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lcom/google/af/a/b/a/a/v;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/dataSources/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v3, Lcom/google/af/a/b/a/a/v;->b:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/dataPointChanges?alt=proto"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v3, Lcom/google/af/a/b/a/a/v;->c:Ljava/lang/String;

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "&pageToken="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lcom/google/af/a/b/a/a/v;->c:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v5, v3, Lcom/google/af/a/b/a/a/v;->d:Ljava/lang/Long;

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "&currentTimeMillis="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lcom/google/af/a/b/a/a/v;->d:Ljava/lang/Long;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v5, v3, Lcom/google/af/a/b/a/a/v;->e:Ljava/lang/Boolean;

    if-eqz v5, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "&deduplicateChanges="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v3, Lcom/google/af/a/b/a/a/v;->e:Ljava/lang/Boolean;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Lcom/google/af/a/b/a/a/w;

    invoke-direct {v5}, Lcom/google/af/a/b/a/a/w;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/b/a/a/w;

    .line 104
    iget-object v1, v0, Lcom/google/af/a/b/a/a/w;->a:[Lcom/google/af/a/b/a/a/c;

    iget-object v2, p0, Lcom/google/android/gms/fitness/apiary/m;->f:Lcom/google/android/gms/fitness/l/z;

    invoke-static {v1, p1, v2}, Lcom/google/android/gms/fitness/apiary/c;->a([Lcom/google/af/a/b/a/a/c;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/l/z;)Ljava/util/List;

    move-result-object v2

    .line 106
    iget-object v1, v0, Lcom/google/af/a/b/a/a/w;->b:[Lcom/google/af/a/b/a/a/c;

    iget-object v3, p0, Lcom/google/android/gms/fitness/apiary/m;->f:Lcom/google/android/gms/fitness/l/z;

    invoke-static {v1, p1, v3}, Lcom/google/android/gms/fitness/apiary/c;->a([Lcom/google/af/a/b/a/a/c;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/l/z;)Ljava/util/List;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v3

    .line 116
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 117
    iget-object v4, v0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    if-eqz v4, :cond_4

    .line 118
    iget-object v1, v0, Lcom/google/af/a/b/a/a/w;->d:[Lcom/google/af/a/b/a/a/i;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 120
    :cond_4
    new-instance v4, Lcom/google/android/gms/fitness/apiary/a;

    iget-object v0, v0, Lcom/google/af/a/b/a/a/w;->c:Ljava/lang/String;

    invoke-direct {v4, v2, v3, v1, v0}, Lcom/google/android/gms/fitness/apiary/a;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    return-object v4

    .line 108
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/google/android/gms/auth/q;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0

    .line 110
    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/android/volley/ac;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0

    .line 112
    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/google/android/gms/fitness/l/ae;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/fitness/apiary/a;
    .locals 7

    .prologue
    .line 186
    new-instance v3, Lcom/google/af/a/b/a/a/af;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/af;-><init>()V

    .line 187
    const-string v0, "me"

    iput-object v0, v3, Lcom/google/af/a/b/a/a/af;->a:Ljava/lang/String;

    .line 188
    iput-object p1, v3, Lcom/google/af/a/b/a/a/af;->e:Ljava/lang/String;

    .line 189
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, v3, Lcom/google/af/a/b/a/a/af;->d:Ljava/lang/Boolean;

    .line 193
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/m;->b:Lcom/google/android/gms/fitness/apiary/l;

    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/m;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/fitness/apiary/l;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/users/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lcom/google/af/a/b/a/a/af;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/sessions?alt=proto"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v3, Lcom/google/af/a/b/a/a/af;->b:Ljava/lang/String;

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "&startTime="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lcom/google/af/a/b/a/a/af;->b:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v5, v3, Lcom/google/af/a/b/a/a/af;->c:Ljava/lang/String;

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "&endTime="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lcom/google/af/a/b/a/a/af;->c:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v5, v3, Lcom/google/af/a/b/a/a/af;->d:Ljava/lang/Boolean;

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "&includeDeleted="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lcom/google/af/a/b/a/a/af;->d:Ljava/lang/Boolean;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    iget-object v5, v3, Lcom/google/af/a/b/a/a/af;->e:Ljava/lang/String;

    if-eqz v5, :cond_3

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "&pageToken="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v3, Lcom/google/af/a/b/a/a/af;->e:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_3
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Lcom/google/af/a/b/a/a/ag;

    invoke-direct {v5}, Lcom/google/af/a/b/a/a/ag;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/b/a/a/ag;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 199
    new-instance v1, Lcom/google/android/gms/fitness/apiary/a;

    iget-object v2, v0, Lcom/google/af/a/b/a/a/ag;->a:[Lcom/google/af/a/b/a/a/ah;

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    iget-object v3, v0, Lcom/google/af/a/b/a/a/ag;->b:[Lcom/google/af/a/b/a/a/ah;

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    iget-object v0, v0, Lcom/google/af/a/b/a/a/ag;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/fitness/apiary/a;-><init>(Ljava/util/List;Ljava/util/List;Ljava/lang/String;)V

    return-object v1

    .line 194
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/google/android/gms/auth/q;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0

    .line 196
    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/android/volley/ac;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0
.end method

.method public final a()Ljava/util/Collection;
    .locals 12

    .prologue
    const/4 v6, 0x0

    .line 65
    new-instance v4, Lcom/google/af/a/b/a/a/ad;

    invoke-direct {v4}, Lcom/google/af/a/b/a/a/ad;-><init>()V

    .line 67
    const-string v0, "me"

    iput-object v0, v4, Lcom/google/af/a/b/a/a/ad;->a:Ljava/lang/String;

    .line 71
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/m;->b:Lcom/google/android/gms/fitness/apiary/l;

    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/m;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/fitness/apiary/l;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x0

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "/users/"

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v4, Lcom/google/af/a/b/a/a/ad;->a:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v7, "/dataSources?alt=proto"

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v4, Lcom/google/af/a/b/a/a/ad;->b:[Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v7, v4, Lcom/google/af/a/b/a/a/ad;->b:[Ljava/lang/String;

    array-length v8, v7

    move v3, v6

    :goto_0
    if-ge v3, v8, :cond_0

    aget-object v9, v7, v3

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "&dataTypeName="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v9}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_0
    iget-object v3, v4, Lcom/google/af/a/b/a/a/ad;->c:Ljava/lang/String;

    if-eqz v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "&onlyModifiedAfter="

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v4, Lcom/google/af/a/b/a/a/ad;->c:Ljava/lang/String;

    invoke-static {v7}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v3, v4, Lcom/google/af/a/b/a/a/ad;->d:Ljava/lang/Boolean;

    if-eqz v3, :cond_2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "&includeDeleted="

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v4, Lcom/google/af/a/b/a/a/ad;->d:Ljava/lang/Boolean;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Lcom/google/af/a/b/a/a/ae;

    invoke-direct {v5}, Lcom/google/af/a/b/a/a/ae;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/b/a/a/ae;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 78
    new-instance v2, Ljava/util/ArrayList;

    iget-object v1, v0, Lcom/google/af/a/b/a/a/ae;->a:[Lcom/google/af/a/b/a/a/d;

    array-length v1, v1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 79
    iget-object v3, v0, Lcom/google/af/a/b/a/a/ae;->a:[Lcom/google/af/a/b/a/a/d;

    array-length v4, v3

    move v1, v6

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v3, v1

    .line 81
    :try_start_1
    sget-object v0, Lcom/google/android/gms/fitness/apiary/c;->f:Lcom/google/android/gms/fitness/apiary/b;

    invoke-interface {v0, v5}, Lcom/google/android/gms/fitness/apiary/b;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    .line 79
    :goto_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 72
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/google/android/gms/auth/q;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0

    .line 74
    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/android/volley/ac;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0

    .line 82
    :catch_2
    move-exception v0

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "unable to convert: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    new-array v7, v6, [Ljava/lang/Object;

    invoke-static {v0, v5, v7}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_2

    .line 86
    :cond_3
    return-object v2
.end method

.method public final a(Ljava/util/List;)Ljava/util/Collection;
    .locals 7

    .prologue
    const/4 v1, 0x1

    .line 209
    const-string v0, "sync.applyChanges count: %s"

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 210
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 232
    :goto_0
    return-object v0

    .line 214
    :cond_0
    new-instance v1, Lcom/google/af/a/b/a/a/p;

    invoke-direct {v1}, Lcom/google/af/a/b/a/a/p;-><init>()V

    .line 215
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/af/a/b/a/a/o;

    invoke-interface {p1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/af/a/b/a/a/o;

    iput-object v0, v1, Lcom/google/af/a/b/a/a/p;->a:[Lcom/google/af/a/b/a/a/o;

    .line 217
    new-instance v4, Lcom/google/af/a/b/a/a/m;

    invoke-direct {v4}, Lcom/google/af/a/b/a/a/m;-><init>()V

    .line 219
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/af/a/b/a/a/m;->b:Ljava/lang/Long;

    .line 220
    const-string v0, "me"

    iput-object v0, v4, Lcom/google/af/a/b/a/a/m;->a:Ljava/lang/String;

    .line 221
    iput-object v1, v4, Lcom/google/af/a/b/a/a/m;->c:Lcom/google/af/a/b/a/a/p;

    .line 225
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/m;->c:Lcom/google/android/gms/fitness/apiary/j;

    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/m;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/fitness/apiary/j;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/users/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v4, Lcom/google/af/a/b/a/a/m;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/applyDataPointChanges?alt=proto"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v4, Lcom/google/af/a/b/a/a/m;->b:Ljava/lang/Long;

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "&currentTimeMillis="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v4, Lcom/google/af/a/b/a/a/m;->b:Ljava/lang/Long;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v4, Lcom/google/af/a/b/a/a/m;->c:Lcom/google/af/a/b/a/a/p;

    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/af/a/b/a/a/n;

    invoke-direct {v5}, Lcom/google/af/a/b/a/a/n;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/b/a/a/n;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 232
    iget-object v0, v0, Lcom/google/af/a/b/a/a/n;->a:[Lcom/google/af/a/b/a/a/o;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    goto/16 :goto_0

    .line 226
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/google/android/gms/auth/q;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0

    .line 228
    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/android/volley/ac;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataSource;JJ)Ljava/util/List;
    .locals 8

    .prologue
    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4, p5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 130
    new-instance v3, Lcom/google/af/a/b/a/a/ac;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/ac;-><init>()V

    .line 131
    const-string v1, "me"

    iput-object v1, v3, Lcom/google/af/a/b/a/a/ac;->a:Ljava/lang/String;

    .line 132
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->h()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/google/af/a/b/a/a/ac;->b:Ljava/lang/String;

    .line 133
    iput-object v0, v3, Lcom/google/af/a/b/a/a/ac;->c:Ljava/lang/String;

    .line 137
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/m;->b:Lcom/google/android/gms/fitness/apiary/l;

    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/m;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/fitness/apiary/l;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/users/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lcom/google/af/a/b/a/a/ac;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/dataSources/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v3, Lcom/google/af/a/b/a/a/ac;->b:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/datasets/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v3, v3, Lcom/google/af/a/b/a/a/ac;->c:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "?alt=proto"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Lcom/google/af/a/b/a/a/aa;

    invoke-direct {v5}, Lcom/google/af/a/b/a/a/aa;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/b/a/a/aa;

    .line 138
    iget-object v0, v0, Lcom/google/af/a/b/a/a/aa;->d:[Lcom/google/af/a/b/a/a/c;

    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/m;->f:Lcom/google/android/gms/fitness/l/z;

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/fitness/apiary/c;->a([Lcom/google/af/a/b/a/a/c;Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/l/z;)Ljava/util/List;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/fitness/l/ae; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    return-object v0

    .line 139
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/google/android/gms/auth/q;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0

    .line 141
    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/android/volley/ac;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0

    .line 143
    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/google/android/gms/fitness/l/ae;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0
.end method

.method public final a(Lcom/google/af/a/b/a/a/ah;)V
    .locals 7

    .prologue
    .line 151
    new-instance v3, Lcom/google/af/a/b/a/a/ab;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/ab;-><init>()V

    .line 152
    const-string v0, "me"

    iput-object v0, v3, Lcom/google/af/a/b/a/a/ab;->a:Ljava/lang/String;

    .line 153
    iget-object v0, p1, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    iput-object v0, v3, Lcom/google/af/a/b/a/a/ab;->b:Ljava/lang/String;

    .line 154
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v3, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    .line 156
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/m;->b:Lcom/google/android/gms/fitness/apiary/l;

    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/m;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/fitness/apiary/l;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/users/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v3, Lcom/google/af/a/b/a/a/ab;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/sessions/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v3, Lcom/google/af/a/b/a/a/ab;->b:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "?alt=proto"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v3, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "&currentTimeMillis="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v3, Lcom/google/af/a/b/a/a/ab;->c:Ljava/lang/Long;

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Lcom/google/protobuf/b;

    invoke-direct {v5}, Lcom/google/protobuf/b;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 161
    return-void

    .line 157
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/google/android/gms/auth/q;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0

    .line 159
    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/android/volley/ac;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0
.end method

.method public final b(Ljava/util/List;)Ljava/util/List;
    .locals 10

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 267
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 269
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 271
    :try_start_0
    sget-object v1, Lcom/google/android/gms/fitness/apiary/c;->f:Lcom/google/android/gms/fitness/apiary/b;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/apiary/b;->a(Ljava/lang/Object;)Lcom/google/protobuf/nano/j;

    move-result-object v1

    check-cast v1, Lcom/google/af/a/b/a/a/d;

    .line 272
    new-instance v4, Lcom/google/af/a/b/a/a/q;

    invoke-direct {v4}, Lcom/google/af/a/b/a/a/q;-><init>()V

    .line 273
    iput-object v1, v4, Lcom/google/af/a/b/a/a/q;->b:Lcom/google/af/a/b/a/a/d;

    .line 274
    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 275
    :catch_0
    move-exception v1

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "unable to convert: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v1, v0, v4}, Lcom/google/android/gms/fitness/m/a;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 280
    :cond_0
    new-instance v1, Lcom/google/af/a/b/a/a/r;

    invoke-direct {v1}, Lcom/google/af/a/b/a/a/r;-><init>()V

    .line 282
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/af/a/b/a/a/q;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/af/a/b/a/a/q;

    iput-object v0, v1, Lcom/google/af/a/b/a/a/r;->a:[Lcom/google/af/a/b/a/a/q;

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/m;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/fitness/apiary/m;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/fitness/sync/c;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v2, "sync_time"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 286
    new-instance v4, Lcom/google/af/a/b/a/a/x;

    invoke-direct {v4}, Lcom/google/af/a/b/a/a/x;-><init>()V

    .line 288
    const-string v0, "me"

    iput-object v0, v4, Lcom/google/af/a/b/a/a/x;->a:Ljava/lang/String;

    .line 289
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/af/a/b/a/a/x;->b:Ljava/lang/Long;

    .line 290
    iput-object v1, v4, Lcom/google/af/a/b/a/a/x;->c:Lcom/google/af/a/b/a/a/r;

    .line 291
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/af/a/b/a/a/x;->d:Ljava/lang/Long;

    .line 295
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/m;->c:Lcom/google/android/gms/fitness/apiary/j;

    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/m;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/fitness/apiary/j;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "/users/"

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v4, Lcom/google/af/a/b/a/a/x;->a:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, "/syncDataSources?alt=proto"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v4, Lcom/google/af/a/b/a/a/x;->b:Ljava/lang/Long;

    if-eqz v5, :cond_1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "&currentTimeMillis="

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v4, Lcom/google/af/a/b/a/a/x;->b:Ljava/lang/Long;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    iget-object v5, v4, Lcom/google/af/a/b/a/a/x;->d:Ljava/lang/Long;

    if-eqz v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "&lastSyncTimeMillis="

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v4, Lcom/google/af/a/b/a/a/x;->d:Ljava/lang/Long;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v4, Lcom/google/af/a/b/a/a/x;->c:Lcom/google/af/a/b/a/a/r;

    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/af/a/b/a/a/y;

    invoke-direct {v5}, Lcom/google/af/a/b/a/a/y;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/b/a/a/y;
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_2

    .line 302
    iget-object v1, v0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    if-eqz v1, :cond_3

    iget-object v1, v0, Lcom/google/af/a/b/a/a/y;->c:[Lcom/google/af/a/b/a/a/s;

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v7

    .line 304
    :goto_1
    if-eqz v1, :cond_4

    .line 305
    new-instance v0, Lcom/google/android/gms/fitness/sync/h;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/sync/h;-><init>()V

    throw v0

    .line 296
    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/google/android/gms/auth/q;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0

    .line 298
    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/android/volley/ac;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0

    :cond_3
    move v1, v6

    .line 302
    goto :goto_1

    .line 310
    :cond_4
    new-instance v1, Ljava/util/ArrayList;

    iget-object v2, v0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    array-length v2, v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 311
    iget-object v2, v0, Lcom/google/af/a/b/a/a/y;->a:[Lcom/google/af/a/b/a/a/q;

    array-length v3, v2

    move v0, v6

    :goto_2
    if-ge v0, v3, :cond_5

    aget-object v4, v2, v0

    .line 312
    sget-object v5, Lcom/google/android/gms/fitness/apiary/c;->f:Lcom/google/android/gms/fitness/apiary/b;

    iget-object v4, v4, Lcom/google/af/a/b/a/a/q;->b:Lcom/google/af/a/b/a/a/d;

    invoke-interface {v5, v4}, Lcom/google/android/gms/fitness/apiary/b;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 311
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 314
    :cond_5
    return-object v1
.end method

.method public final b()V
    .locals 7

    .prologue
    .line 319
    new-instance v4, Lcom/google/af/a/b/a/a/t;

    invoke-direct {v4}, Lcom/google/af/a/b/a/a/t;-><init>()V

    .line 321
    const-string v0, "me"

    iput-object v0, v4, Lcom/google/af/a/b/a/a/t;->a:Ljava/lang/String;

    .line 324
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/m;->c:Lcom/google/android/gms/fitness/apiary/j;

    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/m;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/fitness/apiary/j;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/users/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v4, Lcom/google/af/a/b/a/a/t;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/deleteHistory?alt=proto"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/protobuf/b;

    invoke-direct {v5}, Lcom/google/protobuf/b;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 329
    return-void

    .line 325
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/google/android/gms/auth/q;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0

    .line 327
    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/android/volley/ac;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0
.end method

.method public final b(Lcom/google/af/a/b/a/a/ah;)V
    .locals 7

    .prologue
    .line 167
    new-instance v4, Lcom/google/af/a/b/a/a/ai;

    invoke-direct {v4}, Lcom/google/af/a/b/a/a/ai;-><init>()V

    .line 168
    const-string v0, "me"

    iput-object v0, v4, Lcom/google/af/a/b/a/a/ai;->a:Ljava/lang/String;

    .line 169
    iget-object v0, p1, Lcom/google/af/a/b/a/a/ah;->a:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/af/a/b/a/a/ai;->b:Ljava/lang/String;

    .line 170
    iput-object p1, v4, Lcom/google/af/a/b/a/a/ai;->c:Lcom/google/af/a/b/a/a/ah;

    .line 171
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/af/a/b/a/a/ai;->d:Ljava/lang/Long;

    .line 174
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/m;->b:Lcom/google/android/gms/fitness/apiary/l;

    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/m;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/fitness/apiary/l;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/users/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v4, Lcom/google/af/a/b/a/a/ai;->a:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/sessions/"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, v4, Lcom/google/af/a/b/a/a/ai;->b:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "?alt=proto"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v5, v4, Lcom/google/af/a/b/a/a/ai;->d:Ljava/lang/Long;

    if-eqz v5, :cond_0

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "&currentTimeMillis="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v4, Lcom/google/af/a/b/a/a/ai;->d:Ljava/lang/Long;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, v4, Lcom/google/af/a/b/a/a/ai;->c:Lcom/google/af/a/b/a/a/ah;

    invoke-static {v4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    new-instance v5, Lcom/google/af/a/b/a/a/ah;

    invoke-direct {v5}, Lcom/google/af/a/b/a/a/ah;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 180
    return-void

    .line 176
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/google/android/gms/auth/q;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0

    .line 178
    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/android/volley/ac;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0
.end method

.method public final c()Z
    .locals 7

    .prologue
    .line 334
    new-instance v3, Lcom/google/af/a/b/a/a/u;

    invoke-direct {v3}, Lcom/google/af/a/b/a/a/u;-><init>()V

    .line 336
    const-string v0, "me"

    iput-object v0, v3, Lcom/google/af/a/b/a/a/u;->a:Ljava/lang/String;

    .line 339
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/apiary/m;->c:Lcom/google/android/gms/fitness/apiary/j;

    iget-object v1, p0, Lcom/google/android/gms/fitness/apiary/m;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/fitness/apiary/j;->a:Lcom/google/android/gms/common/server/q;

    const/4 v2, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "/users/"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v3, Lcom/google/af/a/b/a/a/u;->a:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "/accountStatus?alt=proto"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    new-instance v5, Lcom/google/af/a/b/a/a/l;

    invoke-direct {v5}, Lcom/google/af/a/b/a/a/l;-><init>()V

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/b/a/a/l;

    .line 341
    iget-object v0, v0, Lcom/google/af/a/b/a/a/l;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    return v0

    .line 342
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/google/android/gms/auth/q;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0

    .line 344
    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/fitness/apiary/m;->a(Lcom/android/volley/ac;)Lcom/google/android/gms/fitness/sync/g;

    move-result-object v0

    throw v0
.end method
