.class public final Lcom/google/android/gms/games/g/u;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/games/g/ar;

.field public b:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2207
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2208
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/g/u;->a:Lcom/google/android/gms/games/g/ar;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/u;->b:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/u;->cachedSize:I

    .line 2209
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 2266
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2267
    iget-object v1, p0, Lcom/google/android/gms/games/g/u;->a:Lcom/google/android/gms/games/g/ar;

    if-eqz v1, :cond_0

    .line 2268
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/g/u;->a:Lcom/google/android/gms/games/g/ar;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2271
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/games/g/u;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 2272
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/games/g/u;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2275
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2220
    if-ne p1, p0, :cond_1

    .line 2239
    :cond_0
    :goto_0
    return v0

    .line 2223
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/u;

    if-nez v2, :cond_2

    move v0, v1

    .line 2224
    goto :goto_0

    .line 2226
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/u;

    .line 2227
    iget-object v2, p0, Lcom/google/android/gms/games/g/u;->a:Lcom/google/android/gms/games/g/ar;

    if-nez v2, :cond_3

    .line 2228
    iget-object v2, p1, Lcom/google/android/gms/games/g/u;->a:Lcom/google/android/gms/games/g/ar;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2229
    goto :goto_0

    .line 2232
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/u;->a:Lcom/google/android/gms/games/g/ar;

    iget-object v3, p1, Lcom/google/android/gms/games/g/u;->a:Lcom/google/android/gms/games/g/ar;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/ar;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2233
    goto :goto_0

    .line 2236
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/games/g/u;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/games/g/u;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 2237
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 2244
    iget-object v0, p0, Lcom/google/android/gms/games/g/u;->a:Lcom/google/android/gms/games/g/ar;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2247
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/games/g/u;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/games/g/u;->b:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 2249
    return v0

    .line 2244
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/u;->a:Lcom/google/android/gms/games/g/ar;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/ar;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 2184
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/u;->a:Lcom/google/android/gms/games/g/ar;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/g/ar;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/ar;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/u;->a:Lcom/google/android/gms/games/g/ar;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/u;->a:Lcom/google/android/gms/games/g/ar;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/g/u;->b:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 2255
    iget-object v0, p0, Lcom/google/android/gms/games/g/u;->a:Lcom/google/android/gms/games/g/ar;

    if-eqz v0, :cond_0

    .line 2256
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/g/u;->a:Lcom/google/android/gms/games/g/ar;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2258
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/games/g/u;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 2259
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/games/g/u;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2261
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2262
    return-void
.end method
