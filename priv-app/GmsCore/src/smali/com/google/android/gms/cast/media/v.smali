.class final Lcom/google/android/gms/cast/media/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:J

.field final synthetic b:I

.field final synthetic c:Lorg/json/JSONObject;

.field final synthetic d:Lcom/google/android/gms/cast/media/o;


# direct methods
.method constructor <init>(Lcom/google/android/gms/cast/media/o;JILorg/json/JSONObject;)V
    .locals 0

    .prologue
    .line 2228
    iput-object p1, p0, Lcom/google/android/gms/cast/media/v;->d:Lcom/google/android/gms/cast/media/o;

    iput-wide p2, p0, Lcom/google/android/gms/cast/media/v;->a:J

    iput p4, p0, Lcom/google/android/gms/cast/media/v;->b:I

    iput-object p5, p0, Lcom/google/android/gms/cast/media/v;->c:Lorg/json/JSONObject;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    .line 2231
    iget-object v1, p0, Lcom/google/android/gms/cast/media/v;->d:Lcom/google/android/gms/cast/media/o;

    iget-wide v2, p0, Lcom/google/android/gms/cast/media/v;->a:J

    iget v4, p0, Lcom/google/android/gms/cast/media/v;->b:I

    iget-object v5, p0, Lcom/google/android/gms/cast/media/v;->c:Lorg/json/JSONObject;

    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/w;

    iget-wide v8, v0, Lcom/google/android/gms/cast/media/w;->b:J

    cmp-long v7, v8, v2

    if-nez v7, :cond_0

    :goto_0
    iget-object v6, v1, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v7, "onRequestCompleted(); requestId=%d, status=%d, trackedItem=%s"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    const/4 v9, 0x2

    aput-object v0, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    if-eqz v0, :cond_3

    sparse-switch v4, :sswitch_data_0

    iget-object v2, v1, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v3, "unknown status %d; sending error state"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v6, v7

    invoke-virtual {v2, v3, v6}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x7

    invoke-static {v5}, Lcom/google/android/gms/cast/media/o;->a(Lorg/json/JSONObject;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/w;ILandroid/os/Bundle;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/w;)V

    .line 2232
    :cond_1
    :goto_1
    return-void

    .line 2231
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :sswitch_0
    :try_start_0
    iget-object v2, v1, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/internal/l;->g()J

    move-result-wide v2

    iget-object v4, v1, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v5, "Load completed; mediaSessionId=%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-wide/16 v4, -0x1

    iput-wide v4, v0, Lcom/google/android/gms/cast/media/w;->b:J

    iput-wide v2, v0, Lcom/google/android/gms/cast/media/w;->c:J

    iput-object v0, v1, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    invoke-virtual {v1}, Lcom/google/android/gms/cast/media/o;->j()V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v2

    iget-object v2, v1, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v3, "request completed, but no media session ID is available!"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x7

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/w;ILandroid/os/Bundle;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/w;)V

    goto :goto_1

    :sswitch_1
    iget-object v2, v1, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v3, "STATUS_CANCELED; sending error state"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x5

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/w;ILandroid/os/Bundle;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/w;)V

    goto :goto_1

    :sswitch_2
    iget-object v2, v1, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v3, "STATUS_TIMED_OUT; sending error state"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const/4 v2, 0x7

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/w;ILandroid/os/Bundle;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/w;)V

    goto :goto_1

    :cond_3
    iget-wide v6, v1, Lcom/google/android/gms/cast/media/o;->j:J

    cmp-long v0, v2, v6

    if-nez v0, :cond_d

    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "initial status request has completed"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-wide/16 v2, -0x1

    iput-wide v2, v1, Lcom/google/android/gms/cast/media/o;->j:J

    :try_start_1
    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/l;->g()J

    move-result-wide v2

    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_4
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/w;

    iget-wide v6, v0, Lcom/google/android/gms/cast/media/w;->c:J

    cmp-long v6, v6, v2

    if-nez v6, :cond_4

    :goto_2
    iget-object v5, v1, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    if-eqz v5, :cond_5

    iget-object v5, v1, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    if-eq v5, v0, :cond_5

    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    const/4 v5, 0x4

    const/4 v6, 0x0

    invoke-virtual {v1, v0, v5, v6}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/w;ILandroid/os/Bundle;)V

    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/w;)V

    :cond_5
    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->c:Lcom/google/android/gms/cast/media/x;

    if-eqz v0, :cond_6

    new-instance v5, Lcom/google/android/gms/cast/media/w;

    invoke-direct {v5, v1}, Lcom/google/android/gms/cast/media/w;-><init>(Lcom/google/android/gms/cast/media/o;)V

    iput-wide v2, v5, Lcom/google/android/gms/cast/media/w;->c:J

    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->c:Lcom/google/android/gms/cast/media/x;

    iget-object v0, v0, Lcom/google/android/gms/cast/media/x;->a:Landroid/content/Intent;

    const-string v2, "android.media.intent.extra.ITEM_STATUS_UPDATE_RECEIVER"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    iput-object v0, v5, Lcom/google/android/gms/cast/media/w;->d:Landroid/app/PendingIntent;

    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->p:Ljava/util/List;

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iput-object v5, v1, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    :cond_6
    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_7
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/media/w;

    iget-wide v6, v0, Lcom/google/android/gms/cast/media/w;->c:J

    const-wide/16 v8, -0x1

    cmp-long v3, v6, v8

    if-eqz v3, :cond_7

    iget-object v3, v1, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    if-eqz v3, :cond_8

    iget-wide v6, v0, Lcom/google/android/gms/cast/media/w;->c:J

    iget-object v3, v1, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    iget-wide v8, v3, Lcom/google/android/gms/cast/media/w;->c:J

    cmp-long v3, v6, v8

    if-gez v3, :cond_7

    :cond_8
    const/4 v3, 0x4

    const/4 v5, 0x0

    invoke-virtual {v1, v0, v3, v5}, Lcom/google/android/gms/cast/media/o;->a(Lcom/google/android/gms/cast/media/w;ILandroid/os/Bundle;)V

    iget-object v3, v1, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v5, "untrackAllItemsOlderThanCurrent() for item ID %s, load request %d, media session ID %d"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v0, Lcom/google/android/gms/cast/media/w;->a:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-wide v8, v0, Lcom/google/android/gms/cast/media/w;->b:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x2

    iget-wide v8, v0, Lcom/google/android/gms/cast/media/w;->c:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-virtual {v3, v5, v6}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Lcom/google/android/gms/cast/media/o;->c(I)V

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    :cond_9
    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "mSyncStatusRequest = %s, status=%d"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v1, Lcom/google/android/gms/cast/media/o;->c:Lcom/google/android/gms/cast/media/x;

    aput-object v6, v3, v5

    const/4 v5, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->c:Lcom/google/android/gms/cast/media/x;

    if-eqz v0, :cond_1

    if-nez v4, :cond_c

    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v2, "requestStatus completed; sending response"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v2, v1, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    if-eqz v2, :cond_a

    iget-object v2, v1, Lcom/google/android/gms/cast/media/o;->i:Lcom/google/android/gms/cast/internal/l;

    invoke-virtual {v2}, Lcom/google/android/gms/cast/internal/l;->e()Lcom/google/android/gms/cast/k;

    move-result-object v2

    const-string v3, "android.media.intent.extra.ITEM_ID"

    iget-object v4, v1, Lcom/google/android/gms/cast/media/o;->q:Lcom/google/android/gms/cast/media/w;

    iget-object v4, v4, Lcom/google/android/gms/cast/media/w;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {v1}, Lcom/google/android/gms/cast/media/o;->g()Landroid/os/Bundle;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v2, v2, Lcom/google/android/gms/cast/k;->b:Lcom/google/android/gms/cast/g;

    if-eqz v2, :cond_a

    invoke-static {v2}, Lcom/google/android/gms/cast/media/ak;->a(Lcom/google/android/gms/cast/g;)Landroid/os/Bundle;

    move-result-object v2

    iget-object v3, v1, Lcom/google/android/gms/cast/media/o;->o:Lcom/google/android/gms/cast/e/h;

    const-string v4, "adding metadata bundle: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v3, "android.media.intent.extra.ITEM_METADATA"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_a
    iget-object v2, v1, Lcom/google/android/gms/cast/media/o;->c:Lcom/google/android/gms/cast/media/x;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/media/x;->a(Landroid/os/Bundle;)V

    :goto_4
    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/android/gms/cast/media/o;->c:Lcom/google/android/gms/cast/media/x;

    goto/16 :goto_1

    :cond_b
    const/4 v0, 0x0

    goto/16 :goto_2

    :cond_c
    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->c:Lcom/google/android/gms/cast/media/x;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/cast/media/x;->a(I)V

    goto :goto_4

    :cond_d
    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->d:Lcom/google/android/gms/cast/media/x;

    if-eqz v0, :cond_e

    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->d:Lcom/google/android/gms/cast/media/x;

    iget-wide v4, v0, Lcom/google/android/gms/cast/media/x;->c:J

    cmp-long v0, v4, v2

    if-nez v0, :cond_e

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "android.media.intent.extra.SESSION_STATUS"

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/gms/cast/media/o;->d(I)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {v1}, Lcom/google/android/gms/cast/media/o;->g()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v2, v1, Lcom/google/android/gms/cast/media/o;->d:Lcom/google/android/gms/cast/media/x;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/media/x;->a(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/android/gms/cast/media/o;->d:Lcom/google/android/gms/cast/media/x;

    goto/16 :goto_1

    :cond_e
    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->e:Lcom/google/android/gms/cast/media/x;

    if-eqz v0, :cond_f

    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->e:Lcom/google/android/gms/cast/media/x;

    iget-wide v4, v0, Lcom/google/android/gms/cast/media/x;->c:J

    cmp-long v0, v4, v2

    if-nez v0, :cond_f

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "android.media.intent.extra.SESSION_STATUS"

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/gms/cast/media/o;->d(I)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {v1}, Lcom/google/android/gms/cast/media/o;->g()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v2, v1, Lcom/google/android/gms/cast/media/o;->e:Lcom/google/android/gms/cast/media/x;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/media/x;->a(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/android/gms/cast/media/o;->e:Lcom/google/android/gms/cast/media/x;

    goto/16 :goto_1

    :cond_f
    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->f:Lcom/google/android/gms/cast/media/x;

    if-eqz v0, :cond_10

    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->f:Lcom/google/android/gms/cast/media/x;

    iget-wide v4, v0, Lcom/google/android/gms/cast/media/x;->c:J

    cmp-long v0, v4, v2

    if-nez v0, :cond_10

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "android.media.intent.extra.SESSION_STATUS"

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/gms/cast/media/o;->d(I)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v2, "android.media.intent.extra.ITEM_STATUS"

    invoke-virtual {v1}, Lcom/google/android/gms/cast/media/o;->g()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v2, v1, Lcom/google/android/gms/cast/media/o;->f:Lcom/google/android/gms/cast/media/x;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/media/x;->a(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/android/gms/cast/media/o;->f:Lcom/google/android/gms/cast/media/x;

    goto/16 :goto_1

    :cond_10
    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->g:Lcom/google/android/gms/cast/media/x;

    if-eqz v0, :cond_1

    iget-object v0, v1, Lcom/google/android/gms/cast/media/o;->g:Lcom/google/android/gms/cast/media/x;

    iget-wide v4, v0, Lcom/google/android/gms/cast/media/x;->c:J

    cmp-long v0, v4, v2

    if-nez v0, :cond_1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "android.media.intent.extra.SESSION_STATUS"

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Lcom/google/android/gms/cast/media/o;->d(I)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v2, v1, Lcom/google/android/gms/cast/media/o;->g:Lcom/google/android/gms/cast/media/x;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/cast/media/x;->a(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/google/android/gms/cast/media/o;->g:Lcom/google/android/gms/cast/media/x;

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x835 -> :sswitch_1
        0x836 -> :sswitch_2
    .end sparse-switch
.end method
