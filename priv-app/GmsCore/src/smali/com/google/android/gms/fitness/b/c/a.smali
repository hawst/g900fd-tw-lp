.class abstract Lcom/google/android/gms/fitness/b/c/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/fitness/b/ad;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Z

.field private final c:Lcom/google/android/gms/fitness/b/b;

.field private final d:J


# direct methods
.method constructor <init>(Ljava/lang/String;ZLcom/google/android/gms/fitness/b/b;J)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lcom/google/android/gms/fitness/b/c/a;->a:Ljava/lang/String;

    .line 38
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/fitness/b/c/a;->b:Z

    .line 39
    iput-object p3, p0, Lcom/google/android/gms/fitness/b/c/a;->c:Lcom/google/android/gms/fitness/b/b;

    .line 40
    iput-wide p4, p0, Lcom/google/android/gms/fitness/b/c/a;->d:J

    .line 41
    return-void
.end method

.method private a(Lcom/google/android/gms/fitness/b/f;Ljava/lang/String;Lcom/google/android/gms/fitness/b/i;)Lcom/google/android/gms/fitness/b/f;
    .locals 3

    .prologue
    .line 95
    invoke-virtual {p0}, Lcom/google/android/gms/fitness/b/c/a;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/gms/fitness/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/fitness/b/h;->a:Lcom/google/android/gms/fitness/b/h;

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/f;->a(Lcom/google/android/gms/fitness/b/h;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/b/c/a;->a:Ljava/lang/String;

    iget-boolean v2, p0, Lcom/google/android/gms/fitness/b/c/a;->b:Z

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/b/c/v;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, p2}, Lcom/google/android/gms/fitness/b/c/v;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/f;->b(Ljava/lang/String;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/fitness/b/c/a;->c:Lcom/google/android/gms/fitness/b/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/fitness/b/f;->a(Lcom/google/android/gms/fitness/b/b;)Lcom/google/android/gms/fitness/b/f;

    .line 101
    if-eqz p3, :cond_0

    .line 102
    invoke-interface {p1, p3}, Lcom/google/android/gms/fitness/b/f;->a(Lcom/google/android/gms/fitness/b/i;)Lcom/google/android/gms/fitness/b/f;

    .line 104
    :cond_0
    return-object p1
.end method

.method private a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/r;JJ)Lcom/google/android/gms/fitness/b/r;
    .locals 19

    .prologue
    .line 130
    invoke-interface/range {p2 .. p2}, Lcom/google/android/gms/fitness/b/r;->a()Ljava/util/List;

    move-result-object v4

    .line 131
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 132
    invoke-interface/range {p1 .. p1}, Lcom/google/android/gms/fitness/b/s;->d()Lcom/google/android/gms/fitness/b/r;

    move-result-object v2

    .line 216
    :goto_0
    return-object v2

    .line 134
    :cond_0
    invoke-interface {v4}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v5

    const/4 v2, 0x0

    move-object v3, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/b/c;

    if-eqz v3, :cond_1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v6}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    cmp-long v3, v6, v8

    if-nez v3, :cond_1

    invoke-interface {v5}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    invoke-interface {v5}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    invoke-interface {v5}, Ljava/util/ListIterator;->remove()V

    invoke-interface {v5}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    :cond_1
    move-object v3, v2

    goto :goto_1

    .line 136
    :cond_2
    invoke-interface/range {p2 .. p2}, Lcom/google/android/gms/fitness/b/r;->c()Lcom/google/android/gms/fitness/b/e;

    move-result-object v2

    move-object/from16 v0, p0

    iget-wide v6, v0, Lcom/google/android/gms/fitness/b/c/a;->d:J

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2, v6, v7}, Lcom/google/android/gms/fitness/b/c/a;->a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/e;J)Lcom/google/android/gms/fitness/b/c/r;

    move-result-object v16

    .line 141
    invoke-interface {v4}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v17

    .line 142
    invoke-interface/range {v17 .. v17}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/b/c;

    .line 143
    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v8

    .line 144
    invoke-interface {v2}, Lcom/google/android/gms/fitness/b/c;->d()Ljava/util/List;

    move-result-object v3

    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/fitness/b/j;

    invoke-interface {v3}, Lcom/google/android/gms/fitness/b/j;->a()I

    move-result v3

    int-to-long v4, v3

    .line 149
    invoke-interface/range {p2 .. p2}, Lcom/google/android/gms/fitness/b/r;->c()Lcom/google/android/gms/fitness/b/e;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/fitness/b/c/w;->b(Lcom/google/android/gms/fitness/b/e;)Z

    move-result v12

    .line 150
    if-eqz v12, :cond_e

    .line 151
    sget-object v3, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2, v3}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    .line 152
    cmp-long v3, v6, p3

    if-ltz v3, :cond_e

    .line 154
    const-wide/16 v4, 0x0

    .line 155
    invoke-interface/range {v17 .. v17}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    :goto_2
    move-wide v8, v4

    move-wide v10, v6

    move-object v3, v2

    .line 160
    invoke-static {}, Lcom/google/android/gms/fitness/b/c/ae;->a()V

    .line 162
    :goto_3
    invoke-interface/range {v17 .. v17}, Ljava/util/ListIterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 163
    invoke-interface/range {v17 .. v17}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v13, v2

    check-cast v13, Lcom/google/android/gms/fitness/b/c;

    .line 164
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v13, v2}, Lcom/google/android/gms/fitness/b/c;->b(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v4

    .line 165
    sget-object v2, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v13, v2}, Lcom/google/android/gms/fitness/b/c;->a(Ljava/util/concurrent/TimeUnit;)J

    move-result-wide v6

    .line 166
    invoke-interface {v13}, Lcom/google/android/gms/fitness/b/c;->d()Ljava/util/List;

    move-result-object v2

    const/4 v14, 0x0

    invoke-interface {v2, v14}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/b/j;

    invoke-interface {v2}, Lcom/google/android/gms/fitness/b/j;->a()I

    move-result v14

    .line 169
    cmp-long v2, v4, v10

    if-gtz v2, :cond_5

    .line 171
    int-to-long v4, v14

    sub-long/2addr v4, v8

    long-to-int v2, v4

    move-wide v4, v10

    move/from16 v18, v2

    move-object v2, v3

    move/from16 v3, v18

    .line 179
    :goto_4
    if-gez v3, :cond_3

    if-eqz v14, :cond_d

    .line 184
    :cond_3
    int-to-long v14, v14

    .line 186
    if-nez v2, :cond_6

    const-wide/16 v8, 0x0

    .line 188
    :goto_5
    invoke-interface {v13}, Lcom/google/android/gms/fitness/b/c;->c()J

    move-result-wide v10

    .line 189
    if-ltz v3, :cond_4

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v12}, Lcom/google/android/gms/fitness/b/c/a;->a(IJJJJZ)Z

    move-result v2

    if-nez v2, :cond_8

    .line 194
    :cond_4
    if-nez v3, :cond_7

    sget-object v2, Ljava/util/logging/Level;->FINE:Ljava/util/logging/Level;

    .line 195
    :goto_6
    const-string v8, "One or more invalid step counts, delta=%s, start=%tT, end=%tT"

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v9, v10

    const/4 v3, 0x1

    sget-object v10, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v10, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v9, v3

    const/4 v3, 0x2

    sget-object v4, Ljava/util/concurrent/TimeUnit;->NANOSECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v4, v6, v7}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v9, v3

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v13}, Lcom/google/android/gms/fitness/b/c/ae;->a(Ljava/util/logging/Level;Ljava/lang/String;Lcom/google/android/gms/fitness/b/c;)V

    move-wide v8, v14

    move-wide v10, v6

    move-object v3, v13

    .line 202
    goto :goto_3

    .line 174
    :cond_5
    const/4 v2, 0x0

    move v3, v14

    goto :goto_4

    .line 186
    :cond_6
    invoke-interface {v2}, Lcom/google/android/gms/fitness/b/c;->c()J

    move-result-wide v8

    goto :goto_5

    .line 194
    :cond_7
    sget-object v2, Ljava/util/logging/Level;->CONFIG:Ljava/util/logging/Level;

    goto :goto_6

    .line 205
    :cond_8
    cmp-long v2, v6, p5

    if-gtz v2, :cond_c

    .line 207
    cmp-long v2, v6, p3

    if-lez v2, :cond_a

    .line 212
    move-object/from16 v0, v16

    iget-wide v8, v0, Lcom/google/android/gms/fitness/b/c/r;->e:J

    cmp-long v2, v8, v4

    if-eqz v2, :cond_b

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/fitness/b/c/r;->a()V

    move-object/from16 v0, v16

    iput-wide v4, v0, Lcom/google/android/gms/fitness/b/c/r;->d:J

    :cond_9
    :goto_7
    move-object/from16 v0, v16

    iget v2, v0, Lcom/google/android/gms/fitness/b/c/r;->c:I

    add-int/2addr v2, v3

    move-object/from16 v0, v16

    iput v2, v0, Lcom/google/android/gms/fitness/b/c/r;->c:I

    move-object/from16 v0, v16

    iput-wide v6, v0, Lcom/google/android/gms/fitness/b/c/r;->e:J

    :cond_a
    move-wide v8, v14

    move-wide v10, v6

    move-object v3, v13

    .line 214
    goto/16 :goto_3

    .line 212
    :cond_b
    move-object/from16 v0, v16

    iget-wide v8, v0, Lcom/google/android/gms/fitness/b/c/r;->d:J

    sub-long v8, v6, v8

    move-object/from16 v0, v16

    iget-wide v10, v0, Lcom/google/android/gms/fitness/b/c/r;->b:J

    cmp-long v2, v8, v10

    if-lez v2, :cond_9

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/fitness/b/c/r;->a()V

    move-object/from16 v0, v16

    iput-wide v4, v0, Lcom/google/android/gms/fitness/b/c/r;->d:J

    goto :goto_7

    .line 216
    :cond_c
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/fitness/b/c/r;->b()Lcom/google/android/gms/fitness/b/r;

    move-result-object v2

    goto/16 :goto_0

    :cond_d
    move-object v3, v2

    goto/16 :goto_3

    :cond_e
    move-wide v6, v8

    goto/16 :goto_2
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/e;J)Lcom/google/android/gms/fitness/b/c/r;
.end method

.method public a(Lcom/google/android/gms/fitness/b/g;Ljava/util/List;)Ljava/util/List;
    .locals 5

    .prologue
    .line 68
    if-eqz p2, :cond_0

    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 69
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/g;->a()Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    const-string v1, "{source_stream_id}"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/fitness/b/c/a;->a(Lcom/google/android/gms/fitness/b/f;Ljava/lang/String;Lcom/google/android/gms/fitness/b/i;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/f;->a()Lcom/google/android/gms/fitness/b/e;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 85
    :goto_0
    return-object v0

    .line 75
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 76
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/b/e;

    .line 77
    const-string v3, "com.google.step_count.cumulative"

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/e;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 79
    invoke-interface {p1}, Lcom/google/android/gms/fitness/b/g;->a()Lcom/google/android/gms/fitness/b/f;

    move-result-object v3

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/e;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/e;->f()Lcom/google/android/gms/fitness/b/i;

    move-result-object v0

    invoke-direct {p0, v3, v4, v0}, Lcom/google/android/gms/fitness/b/c/a;->a(Lcom/google/android/gms/fitness/b/f;Ljava/lang/String;Lcom/google/android/gms/fitness/b/i;)Lcom/google/android/gms/fitness/b/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/fitness/b/f;->a()Lcom/google/android/gms/fitness/b/e;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    move-object v0, v1

    .line 85
    goto :goto_0
.end method

.method public a(Ljava/util/List;JJLcom/google/android/gms/fitness/b/t;)Ljava/util/List;
    .locals 10

    .prologue
    .line 47
    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 48
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/fitness/b/r;

    .line 49
    invoke-interface/range {p6 .. p6}, Lcom/google/android/gms/fitness/b/t;->a()Lcom/google/android/gms/fitness/b/s;

    move-result-object v2

    .line 50
    invoke-interface {v2}, Lcom/google/android/gms/fitness/b/s;->a()Lcom/google/android/gms/fitness/b/f;

    move-result-object v1

    .line 51
    invoke-interface {v3}, Lcom/google/android/gms/fitness/b/r;->c()Lcom/google/android/gms/fitness/b/e;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/fitness/b/e;->c()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3}, Lcom/google/android/gms/fitness/b/r;->c()Lcom/google/android/gms/fitness/b/e;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/fitness/b/e;->f()Lcom/google/android/gms/fitness/b/i;

    move-result-object v5

    invoke-direct {p0, v1, v4, v5}, Lcom/google/android/gms/fitness/b/c/a;->a(Lcom/google/android/gms/fitness/b/f;Ljava/lang/String;Lcom/google/android/gms/fitness/b/i;)Lcom/google/android/gms/fitness/b/f;

    move-object v1, p0

    move-wide v4, p2

    move-wide v6, p4

    .line 56
    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/fitness/b/c/a;->a(Lcom/google/android/gms/fitness/b/s;Lcom/google/android/gms/fitness/b/r;JJ)Lcom/google/android/gms/fitness/b/r;

    move-result-object v1

    .line 58
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 61
    :cond_0
    return-object v0
.end method

.method protected a(IJJJJZ)Z
    .locals 1

    .prologue
    .line 249
    const/4 v0, 0x1

    return v0
.end method

.method public b()Ljava/util/List;
    .locals 3

    .prologue
    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 113
    new-instance v1, Lcom/google/android/gms/fitness/b/v;

    invoke-direct {v1}, Lcom/google/android/gms/fitness/b/v;-><init>()V

    const-string v2, "com.google.step_count.cumulative"

    iput-object v2, v1, Lcom/google/android/gms/fitness/b/v;->a:Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/fitness/b/p;->a:Lcom/google/android/gms/fitness/b/o;

    iput-object v2, v1, Lcom/google/android/gms/fitness/b/v;->b:Lcom/google/android/gms/fitness/b/o;

    iget-boolean v2, p0, Lcom/google/android/gms/fitness/b/c/a;->b:Z

    iput-boolean v2, v1, Lcom/google/android/gms/fitness/b/v;->e:Z

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/gms/fitness/b/v;->f:Z

    const/16 v2, 0xe10

    iput v2, v1, Lcom/google/android/gms/fitness/b/v;->i:I

    invoke-virtual {v1}, Lcom/google/android/gms/fitness/b/v;->a()Lcom/google/android/gms/fitness/b/u;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 122
    return-object v0
.end method
