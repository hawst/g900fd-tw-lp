.class public abstract Lcom/google/android/gms/common/ui/widget/d;
.super Lcom/google/android/gms/common/ui/widget/c;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/ui/widget/a;

.field private final c:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/ui/widget/a;Ljava/util/Collection;)V
    .locals 1

    .prologue
    .line 416
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/common/ui/widget/d;-><init>(Lcom/google/android/gms/common/ui/widget/a;Ljava/util/Collection;B)V

    .line 417
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/common/ui/widget/a;Ljava/util/Collection;B)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 420
    iput-object p1, p0, Lcom/google/android/gms/common/ui/widget/d;->a:Lcom/google/android/gms/common/ui/widget/a;

    invoke-direct {p0, p1}, Lcom/google/android/gms/common/ui/widget/c;-><init>(Lcom/google/android/gms/common/ui/widget/a;)V

    .line 421
    if-nez p2, :cond_1

    .line 422
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/ui/widget/d;->c:Ljava/util/List;

    .line 446
    :cond_0
    :goto_0
    return-void

    .line 426
    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    .line 430
    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 434
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ltz v0, :cond_3

    move v0, v1

    :goto_1
    const-string v5, "rangeStart"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 435
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p2}, Ljava/util/Collection;->size()I

    move-result v5

    if-gt v0, v5, :cond_4

    :goto_2
    const-string v0, "rangeEnd"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 437
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int v1, v0, v1

    .line 439
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/common/ui/widget/d;->c:Ljava/util/List;

    .line 440
    invoke-interface {p2}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v0, v3

    :cond_2
    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    .line 441
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-lez v4, :cond_5

    .line 442
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_3

    :cond_3
    move v0, v2

    .line 434
    goto :goto_1

    :cond_4
    move v1, v2

    .line 435
    goto :goto_2

    .line 444
    :cond_5
    iget-object v4, p0, Lcom/google/android/gms/common/ui/widget/d;->c:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 445
    iget-object v3, p0, Lcom/google/android/gms/common/ui/widget/d;->c:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-lt v3, v1, :cond_2

    goto :goto_0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method

.method public final a(ILandroid/view/View;Landroid/view/ViewGroup;Z)Landroid/view/View;
    .locals 1

    .prologue
    .line 465
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/ui/widget/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2, p4}, Lcom/google/android/gms/common/ui/widget/d;->a(Ljava/lang/Object;Landroid/view/View;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Ljava/lang/Object;Landroid/view/View;Z)Landroid/view/View;
.end method

.method public final a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/gms/common/ui/widget/d;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
