.class public final Lcom/google/android/gms/auth/setup/d2d/b/s;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Z

.field public c:I

.field public d:J

.field public e:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 39
    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->b:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->c:I

    iput-wide v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->d:J

    iput-wide v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->e:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->cachedSize:I

    .line 40
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    .line 75
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 76
    iget-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->a:Z

    if-eqz v1, :cond_0

    .line 77
    iget-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->a:Z

    invoke-static {v3}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 80
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->b:Z

    if-eqz v1, :cond_1

    .line 81
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 84
    :cond_1
    iget v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->c:I

    if-eq v1, v3, :cond_2

    .line 85
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 89
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 92
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->e:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_4

    .line 93
    const/4 v1, 0x5

    iget-wide v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 96
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->d:J

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->e:J

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x1

    .line 55
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->a:Z

    if-eqz v0, :cond_0

    .line 56
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->a:Z

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 58
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->b:Z

    if-eqz v0, :cond_1

    .line 59
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 61
    :cond_1
    iget v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->c:I

    if-eq v0, v2, :cond_2

    .line 62
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 64
    :cond_2
    iget-wide v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 65
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 67
    :cond_3
    iget-wide v0, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->e:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_4

    .line 68
    const/4 v0, 0x5

    iget-wide v2, p0, Lcom/google/android/gms/auth/setup/d2d/b/s;->e:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 70
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 71
    return-void
.end method
