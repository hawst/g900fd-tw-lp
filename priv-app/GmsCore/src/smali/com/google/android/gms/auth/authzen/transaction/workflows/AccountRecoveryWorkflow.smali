.class public Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;
.super Lcom/google/android/gms/auth/authzen/transaction/a;
.source "SourceFile"


# instance fields
.field private m:Lcom/google/android/gms/auth/e/c;

.field private n:J

.field private o:J


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/ab/b/a/a/r;)Z
    .locals 5

    .prologue
    const/4 v3, 0x3

    const/4 v0, 0x0

    .line 48
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/r;->c:Z

    if-nez v1, :cond_0

    .line 77
    :goto_0
    return v0

    .line 51
    :cond_0
    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    .line 57
    iget-boolean v2, p0, Lcom/google/ab/b/a/a/r;->i:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/ab/b/a/a/r;->j:Lcom/google/ab/b/a/a/c;

    iget-boolean v2, v2, Lcom/google/ab/b/a/a/c;->k:Z

    if-eqz v2, :cond_2

    .line 60
    iget-boolean v2, v1, Lcom/google/ab/b/a/a/q;->j:Z

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/google/ab/b/a/a/q;->k:Lcom/google/ab/b/a/a/b;

    iget-boolean v2, v2, Lcom/google/ab/b/a/a/b;->a:Z

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/google/ab/b/a/a/q;->k:Lcom/google/ab/b/a/a/b;

    iget-object v2, v2, Lcom/google/ab/b/a/a/b;->b:Lcom/google/ab/b/a/a/l;

    invoke-virtual {v2}, Lcom/google/ab/b/a/a/l;->c()I

    move-result v2

    if-eq v2, v3, :cond_2

    .line 64
    :cond_1
    const-string v1, "AuthZen"

    const-string v2, "AccountRecoveryStrings contains pin text but pin data is malformed in AccountRecoveryDescriptor"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 69
    :cond_2
    iget-boolean v2, v1, Lcom/google/ab/b/a/a/q;->j:Z

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/google/ab/b/a/a/q;->k:Lcom/google/ab/b/a/a/b;

    iget-boolean v2, v2, Lcom/google/ab/b/a/a/b;->a:Z

    if-eqz v2, :cond_3

    iget-object v2, v1, Lcom/google/ab/b/a/a/q;->k:Lcom/google/ab/b/a/a/b;

    iget-object v2, v2, Lcom/google/ab/b/a/a/b;->b:Lcom/google/ab/b/a/a/l;

    invoke-virtual {v2}, Lcom/google/ab/b/a/a/l;->c()I

    move-result v2

    if-eq v2, v3, :cond_3

    .line 73
    const-string v2, "AuthZen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected number of pin options found: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/google/ab/b/a/a/q;->k:Lcom/google/ab/b/a/a/b;

    iget-object v1, v1, Lcom/google/ab/b/a/a/b;->b:Lcom/google/ab/b/a/a/l;

    invoke-virtual {v1}, Lcom/google/ab/b/a/a/l;->c()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 77
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Lcom/google/ab/b/a/a/r;Ljava/lang/String;[B)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 42
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(Lcom/google/ab/b/a/a/r;Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    .line 43
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 44
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/auth/authzen/transaction/a/f;I)Z
    .locals 9

    .prologue
    const/16 v8, 0xc8

    const/16 v7, 0x64

    const/4 v3, 0x2

    const/4 v1, 0x0

    const/4 v6, 0x1

    .line 130
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/auth/authzen/transaction/a;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 213
    :goto_0
    return v6

    .line 133
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/auth/authzen/transaction/a/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 134
    sget-object v2, Lcom/google/android/gms/auth/authzen/transaction/a/d;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 135
    if-nez p2, :cond_3

    .line 137
    invoke-virtual {p0, v1, v1}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(IZ)V

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->d:Lcom/google/ab/b/a/a/r;

    iget-object v0, v0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-boolean v0, v0, Lcom/google/ab/b/a/a/q;->j:Z

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_2

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->d:Lcom/google/ab/b/a/a/r;

    iget-object v0, v0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-object v0, v0, Lcom/google/ab/b/a/a/q;->k:Lcom/google/ab/b/a/a/b;

    iget-object v0, v0, Lcom/google/ab/b/a/a/b;->b:Lcom/google/ab/b/a/a/l;

    iget-object v2, v0, Lcom/google/ab/b/a/a/l;->c:Ljava/util/List;

    .line 142
    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a:Landroid/os/Bundle;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/auth/authzen/transaction/a/b;->a(Landroid/os/Bundle;ILjava/util/List;)Lcom/google/android/gms/auth/authzen/transaction/a/b;

    move-result-object v0

    .line 148
    :goto_2
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;Lcom/google/android/gms/auth/authzen/transaction/a/f;)V

    goto :goto_0

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->d:Lcom/google/ab/b/a/a/r;

    iget-object v0, v0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-object v0, v0, Lcom/google/ab/b/a/a/q;->k:Lcom/google/ab/b/a/a/b;

    iget-boolean v0, v0, Lcom/google/ab/b/a/a/b;->a:Z

    goto :goto_1

    .line 146
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a:Landroid/os/Bundle;

    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/a/c;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/authzen/transaction/a/c;

    move-result-object v0

    goto :goto_2

    .line 150
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    const/4 v1, 0x3

    iput v1, v0, Lcom/google/android/gms/auth/e/c;->d:I

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->n:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/gms/auth/e/c;->f:J

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/e/c;->a()V

    .line 154
    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;)V

    goto :goto_0

    .line 157
    :cond_4
    sget-object v2, Lcom/google/android/gms/auth/authzen/transaction/a/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 159
    if-nez p2, :cond_5

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    iput v3, v0, Lcom/google/android/gms/auth/e/c;->d:I

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->n:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/gms/auth/e/c;->f:J

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/e/c;->a()V

    .line 164
    invoke-virtual {p0, v1, v1}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(II)V

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a:Landroid/os/Bundle;

    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/b/h;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 174
    :goto_3
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 167
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    iput v6, v0, Lcom/google/android/gms/auth/e/c;->d:I

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->n:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/gms/auth/e/c;->f:J

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/e/c;->a()V

    .line 171
    invoke-virtual {p0, v7, v6}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(II)V

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a:Landroid/os/Bundle;

    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/b/h;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 176
    :cond_6
    sget-object v2, Lcom/google/android/gms/auth/authzen/transaction/a/b;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 177
    if-nez p2, :cond_7

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    iput v3, v0, Lcom/google/android/gms/auth/e/c;->d:I

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->n:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/gms/auth/e/c;->f:J

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/e/c;->a()V

    .line 183
    invoke-virtual {p0, v1, v1}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(II)V

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a:Landroid/os/Bundle;

    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/b/h;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 185
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 186
    :cond_7
    if-ne p2, v6, :cond_8

    .line 188
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    iput v8, v0, Lcom/google/android/gms/auth/e/c;->d:I

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->n:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/gms/auth/e/c;->f:J

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/e/c;->a()V

    .line 191
    invoke-virtual {p0, v8, v8}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(II)V

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 193
    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/a;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/b/h;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/a;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/b/h;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/a;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/b/h;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/a/a;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/authzen/transaction/a/a;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;Lcom/google/android/gms/auth/authzen/transaction/a/f;)V

    goto/16 :goto_0

    .line 202
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    iput v7, v0, Lcom/google/android/gms/auth/e/c;->d:I

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->n:J

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lcom/google/android/gms/auth/e/c;->f:J

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/e/c;->a()V

    .line 205
    invoke-virtual {p0, v7, v6}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(II)V

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a:Landroid/os/Bundle;

    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/b/h;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 207
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 210
    :cond_9
    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 211
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->setResult(I)V

    .line 212
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->finish()V

    goto/16 :goto_0

    .line 215
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    iput v3, v1, Lcom/google/android/gms/auth/e/c;->e:I

    .line 216
    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/e/c;->a()V

    .line 217
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment not supported in account recovery workflow: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method protected final b(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 82
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->n:J

    .line 83
    new-instance v0, Lcom/google/android/gms/auth/e/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/e/c;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    .line 84
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/authzen/transaction/a;->b(Landroid/os/Bundle;)V

    .line 85
    return-void
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 95
    invoke-super {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;->onDestroy()V

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    if-eqz v0, :cond_0

    .line 100
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->n:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x57a58

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->o:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x64

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    iput-boolean v4, v0, Lcom/google/android/gms/auth/e/c;->g:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/e/c;->a()V

    .line 102
    :cond_0
    :goto_0
    return-void

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    iput-boolean v4, v0, Lcom/google/android/gms/auth/e/c;->i:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/e/c;->a()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    iget v0, v0, Lcom/google/android/gms/auth/e/c;->d:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    iget v0, v0, Lcom/google/android/gms/auth/e/c;->e:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    iput-boolean v4, v0, Lcom/google/android/gms/auth/e/c;->h:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->m:Lcom/google/android/gms/auth/e/c;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/e/c;->a()V

    goto :goto_0
.end method

.method public onStop()V
    .locals 2

    .prologue
    .line 89
    invoke-super {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;->onStop()V

    .line 90
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/AccountRecoveryWorkflow;->o:J

    .line 91
    return-void
.end method
