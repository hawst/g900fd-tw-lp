.class public Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;
.super Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;
.source "SourceFile"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/h;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/analytics/events/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>(Landroid/os/Parcel;)V

    .line 30
    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;->a:Z

    .line 31
    return-void

    .line 30
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;B)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method private constructor <init>(ZLjava/lang/String;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;-><init>()V

    .line 24
    iput-object p2, p0, Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;->j:Ljava/lang/String;

    .line 25
    iput-boolean p1, p0, Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;->a:Z

    .line 26
    return-void
.end method

.method public static a(Landroid/content/Context;ZLjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;-><init>(ZLjava/lang/String;)V

    .line 36
    invoke-static {p0, v0}, Lcom/google/android/gms/wallet/service/analytics/a;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;)V

    .line 37
    invoke-virtual {v0}, Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 47
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/wallet/analytics/events/WalletAnalyticsEvent;->writeToParcel(Landroid/os/Parcel;I)V

    .line 48
    iget-boolean v0, p0, Lcom/google/android/gms/wallet/analytics/events/OcrEnabledStateEvent;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    int-to-byte v0, v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    .line 49
    return-void

    .line 48
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
