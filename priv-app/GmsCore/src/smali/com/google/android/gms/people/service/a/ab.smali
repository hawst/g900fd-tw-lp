.class public final Lcom/google/android/gms/people/service/a/ab;
.super Lcom/google/android/gms/people/service/a/c;
.source "SourceFile"


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:I

.field private final k:Z

.field private final l:I

.field private final m:I

.field private final n:Ljava/lang/String;

.field private final o:Z

.field private final p:I

.field private final q:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;ZII)V
    .locals 8

    .prologue
    .line 49
    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    move-object v7, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/people/service/a/c;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;ZLjava/lang/String;)V

    .line 51
    iput-object p5, p0, Lcom/google/android/gms/people/service/a/ab;->g:Ljava/lang/String;

    .line 52
    iput-object p6, p0, Lcom/google/android/gms/people/service/a/ab;->h:Ljava/lang/String;

    .line 53
    iput-object p7, p0, Lcom/google/android/gms/people/service/a/ab;->i:Ljava/lang/String;

    .line 54
    move/from16 v0, p8

    iput v0, p0, Lcom/google/android/gms/people/service/a/ab;->j:I

    .line 55
    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/gms/people/service/a/ab;->k:Z

    .line 56
    move/from16 v0, p10

    iput v0, p0, Lcom/google/android/gms/people/service/a/ab;->l:I

    .line 57
    move/from16 v0, p11

    iput v0, p0, Lcom/google/android/gms/people/service/a/ab;->m:I

    .line 58
    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/google/android/gms/people/service/a/ab;->n:Ljava/lang/String;

    .line 59
    move/from16 v0, p13

    iput-boolean v0, p0, Lcom/google/android/gms/people/service/a/ab;->o:Z

    .line 60
    move/from16 v0, p14

    iput v0, p0, Lcom/google/android/gms/people/service/a/ab;->p:I

    .line 61
    move/from16 v0, p15

    iput v0, p0, Lcom/google/android/gms/people/service/a/ab;->q:I

    .line 62
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;ZII)[Lcom/google/android/gms/common/data/DataHolder;
    .locals 19

    .prologue
    .line 78
    or-int/lit8 v4, p6, 0x2

    .line 79
    or-int/lit8 v4, v4, 0x4

    .line 80
    or-int/lit8 v10, v4, 0x1

    .line 83
    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 89
    if-nez p10, :cond_3

    .line 90
    invoke-static {v14}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x3

    :goto_0
    move/from16 v16, v4

    .line 96
    :goto_1
    invoke-static/range {p8 .. p8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 98
    const/4 v9, 0x0

    .line 106
    :goto_2
    sget-object v4, Lcom/google/android/gms/people/a/a;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :goto_3
    if-eqz p9, :cond_2

    sget-object v4, Lcom/google/android/gms/people/a/a;->y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v4, Lcom/google/android/gms/people/a/a;->z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v4}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static/range {p0 .. p0}, Lcom/google/android/gms/people/ah;->a(Landroid/content/Context;)Lcom/google/android/gms/people/ah;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/ah;->a()Lcom/google/android/gms/people/a/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/a/c;->a()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static/range {p0 .. p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v4, v0, v1}, Lcom/google/android/gms/people/c/f;->d(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/16 v18, 0x1

    .line 110
    :goto_4
    invoke-static/range {p0 .. p0}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/people/c/f;->c()Lcom/google/android/gms/people/c/e;

    .line 111
    const/4 v8, 0x0

    const-wide/16 v12, 0x0

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object/from16 v7, p2

    move/from16 v11, p5

    move/from16 v15, p4

    move/from16 v17, p7

    invoke-static/range {v5 .. v18}, Lcom/google/android/gms/people/service/a/af;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;IIIZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v4

    .line 117
    const/4 v5, 0x0

    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p8

    move/from16 v2, p11

    invoke-static {v0, v5, v1, v2}, Lcom/google/android/gms/people/service/a/af;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 123
    const/4 v6, 0x2

    new-array v6, v6, [Lcom/google/android/gms/common/data/DataHolder;

    const/4 v7, 0x0

    aput-object v4, v6, v7

    const/4 v4, 0x1

    aput-object v5, v6, v4

    return-object v6

    .line 90
    :cond_0
    const/4 v4, 0x2

    goto/16 :goto_0

    .line 101
    :cond_1
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-static/range {p8 .. p8}, Lcom/google/android/gms/people/internal/at;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v9

    goto/16 :goto_2

    .line 106
    :pswitch_0
    const/16 v18, 0x1

    goto :goto_4

    :pswitch_1
    const/16 p9, 0x1

    goto/16 :goto_3

    :cond_2
    const/16 v18, 0x0

    goto :goto_4

    .line 119
    :catch_0
    move-exception v5

    .line 120
    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 121
    throw v5

    :cond_3
    move/from16 v16, p10

    goto/16 :goto_1

    .line 106
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public final a(Landroid/content/Context;)[Lcom/google/android/gms/common/data/DataHolder;
    .locals 12

    .prologue
    .line 66
    iget-object v1, p0, Lcom/google/android/gms/people/service/a/ab;->g:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/ab;->h:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/ab;->i:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/people/service/a/ab;->j:I

    iget-boolean v5, p0, Lcom/google/android/gms/people/service/a/ab;->k:Z

    iget v6, p0, Lcom/google/android/gms/people/service/a/ab;->l:I

    iget v7, p0, Lcom/google/android/gms/people/service/a/ab;->m:I

    iget-object v8, p0, Lcom/google/android/gms/people/service/a/ab;->n:Ljava/lang/String;

    iget-boolean v9, p0, Lcom/google/android/gms/people/service/a/ab;->o:Z

    iget v10, p0, Lcom/google/android/gms/people/service/a/ab;->p:I

    iget v11, p0, Lcom/google/android/gms/people/service/a/ab;->q:I

    move-object v0, p1

    invoke-static/range {v0 .. v11}, Lcom/google/android/gms/people/service/a/ab;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZIILjava/lang/String;ZII)[Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
