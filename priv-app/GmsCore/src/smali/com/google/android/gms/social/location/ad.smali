.class public Lcom/google/android/gms/social/location/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/q;

.field private final b:Lcom/google/android/gms/common/server/ClientContext;


# direct methods
.method public constructor <init>(Ljava/lang/String;Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 26
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 27
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v1

    invoke-virtual {p2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, p1, p1, v2}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/ad;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 29
    iget-object v1, p0, Lcom/google/android/gms/social/location/ad;->b:Lcom/google/android/gms/common/server/ClientContext;

    sget-object v0, Lcom/google/android/gms/social/a/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    .line 30
    new-instance v0, Lcom/google/android/gms/common/server/q;

    sget-object v1, Lcom/google/android/gms/social/a/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/social/a/a;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/social/a/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/String;

    const/4 v7, 0x0

    move-object v1, p2

    move v5, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/q;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/ad;->a:Lcom/google/android/gms/common/server/q;

    .line 34
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/protobuf/nano/j;Lcom/google/protobuf/nano/j;Ljava/lang/String;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 40
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/social/location/ad;->a:Lcom/google/android/gms/common/server/q;

    iget-object v1, p0, Lcom/google/android/gms/social/location/ad;->b:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v2, 0x1

    invoke-virtual {p3}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v4

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :goto_0
    return-object v0

    .line 45
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v6

    .line 48
    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v0, v6

    goto :goto_0
.end method
