.class public final Lcom/google/android/gms/fitness/service/av;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/concurrent/ConcurrentMap;

.field final b:Ljava/util/concurrent/ConcurrentHashMap;

.field public final c:Lcom/google/android/gms/fitness/sensors/a;

.field final d:Lcom/google/android/gms/fitness/l/a/a;

.field private final e:Landroid/content/Context;

.field private final f:Lcom/google/android/gms/fitness/service/az;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/fitness/sensors/a;Lcom/google/android/gms/fitness/l/a/a;Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p2, p0, Lcom/google/android/gms/fitness/service/av;->d:Lcom/google/android/gms/fitness/l/a/a;

    .line 69
    iput-object p3, p0, Lcom/google/android/gms/fitness/service/av;->e:Landroid/content/Context;

    .line 70
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/av;->a:Ljava/util/concurrent/ConcurrentMap;

    .line 71
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/av;->b:Ljava/util/concurrent/ConcurrentHashMap;

    .line 72
    invoke-static {p3}, Lcom/google/android/gms/fitness/service/az;->b(Landroid/content/Context;)Lcom/google/android/gms/fitness/service/az;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/av;->f:Lcom/google/android/gms/fitness/service/az;

    .line 77
    new-instance v0, Lcom/google/android/gms/fitness/sensors/e/f;

    sget-object v1, Lcom/google/android/gms/fitness/sensors/e/d;->b:Lcom/google/android/gms/fitness/sensors/e/d;

    const-string v2, "Recording"

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/fitness/sensors/e/f;-><init>(Lcom/google/android/gms/fitness/sensors/a;Lcom/google/android/gms/fitness/sensors/e/d;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/service/av;->c:Lcom/google/android/gms/fitness/sensors/a;

    .line 78
    return-void
.end method

.method private b()Lcom/google/android/gms/fitness/internal/a;
    .locals 4

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/av;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 186
    iget-object v2, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string v3, "com.google.android.apps.fitness"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 187
    new-instance v1, Lcom/google/android/gms/fitness/internal/a;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    const-string v2, "com.google.android.apps.fitness"

    iget-object v3, p0, Lcom/google/android/gms/fitness/service/av;->e:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/fitness/e/a;->a(Landroid/content/Context;)Z

    move-result v3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/fitness/internal/a;-><init>(ILjava/lang/String;Z)V

    move-object v0, v1

    .line 190
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a(Lcom/google/android/gms/fitness/c/c;)Lcom/google/k/k/a/af;
    .locals 17

    .prologue
    .line 85
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/fitness/c/c;->a:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/fitness/service/av;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v3, v2}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/List;

    if-eqz v2, :cond_1

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/sensors/e/g;

    iget-object v2, v2, Lcom/google/android/gms/fitness/sensors/e/g;->d:Lcom/google/android/gms/fitness/data/Subscription;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/fitness/c/c;->b:Lcom/google/android/gms/fitness/data/Subscription;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/fitness/data/Subscription;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    if-eqz v2, :cond_2

    .line 86
    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v2

    .line 105
    :goto_1
    return-object v2

    .line 85
    :cond_1
    const/4 v2, 0x0

    goto :goto_0

    .line 89
    :cond_2
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/gms/fitness/c/c;->b:Lcom/google/android/gms/fitness/data/Subscription;

    .line 90
    new-instance v5, Lcom/google/android/gms/fitness/service/ax;

    invoke-virtual {v10}, Lcom/google/android/gms/fitness/data/Subscription;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/fitness/service/av;->d:Lcom/google/android/gms/fitness/l/a/a;

    invoke-direct {v5, v2, v3}, Lcom/google/android/gms/fitness/service/ax;-><init>(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/l/a/a;)V

    .line 92
    invoke-virtual {v10}, Lcom/google/android/gms/fitness/data/Subscription;->d()J

    move-result-wide v6

    const-wide/16 v2, 0x0

    cmp-long v2, v6, v2

    if-gez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/fitness/service/av;->f:Lcom/google/android/gms/fitness/service/az;

    invoke-virtual {v10}, Lcom/google/android/gms/fitness/data/Subscription;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v3

    invoke-virtual {v10}, Lcom/google/android/gms/fitness/data/Subscription;->f()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/fitness/service/az;->a(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/DataType;)J

    move-result-wide v6

    const-wide/16 v2, -0x1

    cmp-long v2, v6, v2

    if-nez v2, :cond_3

    const-string v2, "The requested data source %s is disabled. Requested subscription is ignored."

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v10}, Lcom/google/android/gms/fitness/data/Subscription;->e()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v2

    .line 93
    :goto_2
    new-instance v3, Lcom/google/android/gms/fitness/service/aw;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1, v5}, Lcom/google/android/gms/fitness/service/aw;-><init>(Lcom/google/android/gms/fitness/service/av;Lcom/google/android/gms/fitness/c/c;Lcom/google/android/gms/fitness/data/l;)V

    invoke-static {v2, v3}, Lcom/google/k/k/a/n;->a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/m;)V

    goto :goto_1

    .line 92
    :cond_3
    const-wide/16 v2, 0x2

    div-long v8, v6, v2

    invoke-virtual {v10}, Lcom/google/android/gms/fitness/data/Subscription;->f()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/fitness/data/DataType;->l:Lcom/google/android/gms/fitness/data/DataType;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/fitness/data/DataType;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    sget-object v2, Lcom/google/android/gms/fitness/g/c;->aI:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/fitness/service/av;->c:Lcom/google/android/gms/fitness/sensors/a;

    new-instance v3, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

    invoke-virtual {v10}, Lcom/google/android/gms/fitness/data/Subscription;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v4

    invoke-virtual {v10}, Lcom/google/android/gms/fitness/data/Subscription;->c()I

    move-result v12

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/fitness/service/av;->e:Landroid/content/Context;

    const-string v11, "com.google.android.apps.fitness"

    invoke-static {v10, v11}, Lcom/google/android/gms/common/util/e;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/fitness/service/av;->b()Lcom/google/android/gms/fitness/internal/a;

    move-result-object v10

    if-eqz v10, :cond_5

    invoke-static {v10}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v14

    :goto_3
    const-wide/16 v15, -0x1

    move-wide v10, v6

    invoke-direct/range {v3 .. v16}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;-><init>(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/l;JJJILjava/util/List;Ljava/util/List;J)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/fitness/sensors/a;->a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/k/k/a/af;

    move-result-object v2

    goto :goto_2

    :cond_5
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v14

    goto :goto_3
.end method

.method final a()Ljava/util/Collection;
    .locals 3

    .prologue
    .line 289
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 290
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/av;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 291
    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 293
    :cond_0
    return-object v1
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 280
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/av;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/av;->b:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/av;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 283
    :cond_0
    monitor-exit p0

    return-void

    .line 280
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(Ljava/lang/String;Landroid/app/PendingIntent;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 265
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/av;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 266
    if-nez v0, :cond_0

    .line 267
    const-string v0, "Couldn\'t find any pending intents for %s"

    new-array v1, v4, [Ljava/lang/Object;

    aput-object p1, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 277
    :goto_0
    return-void

    .line 271
    :cond_0
    invoke-interface {v0, p2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 272
    const-string v1, "Pending intent %s not found in the list of intents %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v3

    aput-object v0, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->e(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    .line 276
    :cond_1
    invoke-interface {v0, p2}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 317
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "registrations"

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/av;->a:Ljava/util/concurrent/ConcurrentMap;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "registered for sessions"

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/av;->b:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
