.class public final Lcom/google/android/gms/drive/a/ai;
.super Lcom/google/android/gms/drive/a/b;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/android/gms/drive/database/model/ap;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/database/model/ap;Lcom/google/android/gms/drive/a/ac;)V
    .locals 6

    .prologue
    .line 58
    sget-object v1, Lcom/google/android/gms/drive/a/e;->l:Lcom/google/android/gms/drive/a/e;

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/a/ac;)V

    .line 59
    iput-object p4, p0, Lcom/google/android/gms/drive/a/ai;->d:Lcom/google/android/gms/drive/database/model/ap;

    .line 60
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V
    .locals 2

    .prologue
    .line 42
    sget-object v0, Lcom/google/android/gms/drive/a/e;->l:Lcom/google/android/gms/drive/a/e;

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/a/b;-><init>(Lcom/google/android/gms/drive/a/e;Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    .line 43
    const-string v0, "trashedState"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/ap;->a(J)Lcom/google/android/gms/drive/database/model/ap;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/a/ai;->d:Lcom/google/android/gms/drive/database/model/ap;

    .line 44
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;B)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/a/ai;-><init>(Lcom/google/android/gms/drive/database/model/a;Lorg/json/JSONObject;)V

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/a/c;
    .locals 6

    .prologue
    .line 77
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    sget-object v4, Lcom/google/android/gms/drive/database/model/ap;->a:Lcom/google/android/gms/drive/database/model/ap;

    .line 85
    :goto_0
    iget-object v1, p3, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    .line 86
    iget-object v2, p3, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/drive/a/ai;->d:Lcom/google/android/gms/drive/database/model/ap;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/drive/database/model/ap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 91
    new-instance v0, Lcom/google/android/gms/drive/a/z;

    sget-object v3, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/a/z;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/a/ac;)V

    .line 104
    :goto_1
    return-object v0

    .line 79
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->u()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    sget-object v4, Lcom/google/android/gms/drive/database/model/ap;->b:Lcom/google/android/gms/drive/database/model/ap;

    goto :goto_0

    .line 82
    :cond_1
    sget-object v4, Lcom/google/android/gms/drive/database/model/ap;->c:Lcom/google/android/gms/drive/database/model/ap;

    goto :goto_0

    .line 94
    :cond_2
    sget-object v0, Lcom/google/android/gms/drive/database/model/ap;->a:Lcom/google/android/gms/drive/database/model/ap;

    iget-object v3, p0, Lcom/google/android/gms/drive/a/ai;->d:Lcom/google/android/gms/drive/database/model/ap;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/database/model/ap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 95
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->t()V

    .line 101
    :goto_2
    const/4 v0, 0x1

    invoke-virtual {p2, v0}, Lcom/google/android/gms/drive/database/model/ah;->a(Z)V

    .line 103
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->a()Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v3

    .line 104
    new-instance v0, Lcom/google/android/gms/drive/a/ai;

    sget-object v5, Lcom/google/android/gms/drive/a/ac;->b:Lcom/google/android/gms/drive/a/ac;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/a/ai;-><init>(Lcom/google/android/gms/drive/database/model/a;Lcom/google/android/gms/drive/auth/AppIdentity;Lcom/google/android/gms/drive/database/model/EntrySpec;Lcom/google/android/gms/drive/database/model/ap;Lcom/google/android/gms/drive/a/ac;)V

    goto :goto_1

    .line 96
    :cond_3
    sget-object v0, Lcom/google/android/gms/drive/database/model/ap;->b:Lcom/google/android/gms/drive/database/model/ap;

    iget-object v3, p0, Lcom/google/android/gms/drive/a/ai;->d:Lcom/google/android/gms/drive/database/model/ap;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/database/model/ap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 97
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->v()V

    goto :goto_2

    .line 99
    :cond_4
    invoke-virtual {p2}, Lcom/google/android/gms/drive/database/model/ah;->w()V

    goto :goto_2
.end method

.method protected final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/drive/g/aw;)V
    .locals 2

    .prologue
    .line 111
    sget-object v0, Lcom/google/android/gms/drive/database/model/ap;->b:Lcom/google/android/gms/drive/database/model/ap;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/ai;->d:Lcom/google/android/gms/drive/database/model/ap;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot apply an implicit trash action on the server"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/database/model/ap;->c:Lcom/google/android/gms/drive/database/model/ap;

    iget-object v1, p0, Lcom/google/android/gms/drive/a/ai;->d:Lcom/google/android/gms/drive/database/model/ap;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/database/model/ap;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 117
    invoke-virtual {p3}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/drive/d/f;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    .line 121
    :goto_0
    return-void

    .line 119
    :cond_1
    invoke-virtual {p3}, Lcom/google/android/gms/drive/g/aw;->o()Lcom/google/android/gms/drive/d/f;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/drive/d/f;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 140
    if-ne p0, p1, :cond_1

    .line 147
    :cond_0
    :goto_0
    return v0

    .line 143
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/a/ai;

    if-nez v2, :cond_2

    move v0, v1

    .line 144
    goto :goto_0

    .line 146
    :cond_2
    check-cast p1, Lcom/google/android/gms/drive/a/ai;

    .line 147
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/a/ai;->a(Lcom/google/android/gms/drive/a/a;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/drive/a/ai;->d:Lcom/google/android/gms/drive/database/model/ap;

    iget-object v3, p1, Lcom/google/android/gms/drive/a/ai;->d:Lcom/google/android/gms/drive/database/model/ap;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/database/model/ap;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final f()Lorg/json/JSONObject;
    .locals 4

    .prologue
    .line 125
    invoke-super {p0}, Lcom/google/android/gms/drive/a/b;->f()Lorg/json/JSONObject;

    move-result-object v0

    .line 126
    const-string v1, "trashedState"

    iget-object v2, p0, Lcom/google/android/gms/drive/a/ai;->d:Lcom/google/android/gms/drive/database/model/ap;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/database/model/ap;->a()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 127
    return-object v0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/gms/drive/a/b;->c:Lcom/google/android/gms/drive/database/model/EntrySpec;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/EntrySpec;->hashCode()I

    move-result v0

    .line 134
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/drive/a/ai;->d:Lcom/google/android/gms/drive/database/model/ap;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/ap;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 152
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "SetTrashedAction [%s, mTrashedState=%s]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/a/ai;->h()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/a/ai;->d:Lcom/google/android/gms/drive/database/model/ap;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
