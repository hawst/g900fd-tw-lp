.class public Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;
.super Landroid/widget/TextView;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 28
    return-void
.end method


# virtual methods
.method public final a(IZ)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 41
    if-eqz p1, :cond_1

    .line 42
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/a/e;->a(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 43
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/common/a/e;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 44
    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    .line 45
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/16 v2, 0x2b3

    invoke-static {v1, v2}, Lcom/google/android/gms/wallet/common/a/e;->a(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 46
    if-eqz v1, :cond_0

    .line 47
    invoke-static {v1}, Lcom/google/android/gms/wallet/common/ui/ba;->a(Landroid/graphics/drawable/Drawable;)Lcom/google/android/gms/wallet/common/ui/ba;

    move-result-object v0

    .line 50
    :cond_0
    invoke-virtual {p0, v0, v3, v3, v3}, Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    .line 55
    :goto_0
    return-void

    .line 52
    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;->setText(Ljava/lang/CharSequence;)V

    .line 53
    invoke-virtual {p0, v3, v3, v3, v3}, Lcom/google/android/gms/wallet/common/ui/RegionCodeTextView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
