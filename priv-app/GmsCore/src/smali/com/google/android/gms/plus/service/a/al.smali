.class public final Lcom/google/android/gms/plus/service/a/al;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/plus/internal/c;

.field private final b:Lcom/google/android/gms/common/server/ClientContext;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/plus/internal/c;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/al;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/al;->a:Lcom/google/android/gms/plus/internal/c;

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 34
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/al;->b:Lcom/google/android/gms/common/server/ClientContext;

    new-instance v1, Lcom/google/android/gms/common/server/a/a;

    const/4 v2, 0x1

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/common/server/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/server/a/a;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "/revoke?token="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p2, Lcom/google/android/gms/plus/b/b;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v4, v2, v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    invoke-static {p1, v1}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/al;->a:Lcom/google/android/gms/plus/internal/c;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    .line 48
    :goto_0
    return-void

    .line 36
    :catch_0
    move-exception v0

    .line 37
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 38
    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p1, v6, v0, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 39
    const-string v2, "pendingIntent"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/al;->a:Lcom/google/android/gms/plus/internal/c;

    invoke-interface {v0, v8, v1}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 42
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/al;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0}, Lcom/google/android/gms/plus/m;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Landroid/os/Bundle;

    move-result-object v0

    .line 43
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/al;->a:Lcom/google/android/gms/plus/internal/c;

    invoke-interface {v1, v8, v0}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;)V

    goto :goto_0

    .line 45
    :catch_2
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/al;->a:Lcom/google/android/gms/plus/internal/c;

    const/4 v1, 0x7

    invoke-interface {v0, v1, v7}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/al;->a:Lcom/google/android/gms/plus/internal/c;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/al;->a:Lcom/google/android/gms/plus/internal/c;

    const/16 v1, 0x8

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/plus/internal/c;->a(ILandroid/os/Bundle;)V

    .line 55
    :cond_0
    return-void
.end method
