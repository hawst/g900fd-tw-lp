.class public final Lcom/google/android/gms/plus/b/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I


# instance fields
.field private final b:Lcom/google/android/gms/plus/service/pos/o;

.field private final c:Lcom/google/android/gms/plus/service/c/d;

.field private final d:Lcom/google/android/gms/plus/service/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    sget v0, Lcom/google/android/gms/g;->bG:I

    sput v0, Lcom/google/android/gms/plus/b/p;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/plus/service/pos/o;Lcom/google/android/gms/plus/service/c/d;Lcom/google/android/gms/plus/service/f;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-object p1, p0, Lcom/google/android/gms/plus/b/p;->b:Lcom/google/android/gms/plus/service/pos/o;

    .line 56
    iput-object p2, p0, Lcom/google/android/gms/plus/b/p;->c:Lcom/google/android/gms/plus/service/c/d;

    .line 57
    iput-object p3, p0, Lcom/google/android/gms/plus/b/p;->d:Lcom/google/android/gms/plus/service/f;

    .line 58
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 225
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 226
    const-string p0, ""

    .line 230
    :cond_0
    :goto_0
    return-object p0

    .line 229
    :cond_1
    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 230
    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 238
    invoke-static {}, Lcom/google/android/gms/plus/b/f;->a()Lcom/google/android/gms/plus/b/f;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/google/android/gms/plus/b/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/b/h;

    move-result-object v0

    .line 240
    if-nez v0, :cond_1

    .line 242
    :goto_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/b/a;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/plus/b/p;->c:Lcom/google/android/gms/plus/service/c/d;

    invoke-static {p3}, Lcom/google/android/gms/plus/b/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v4, "native:android_app"

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/plus/service/c/d;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/service/c/c;

    move-result-object v0

    .line 254
    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/c/c;->b()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/c/b;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/c/b;->b()Lcom/google/android/gms/plus/service/c/a;

    move-result-object v0

    .line 256
    invoke-static {v0}, Lcom/google/android/gms/plus/b/g;->a(Lcom/google/android/gms/plus/service/c/a;)Z

    move-result v0

    .line 257
    invoke-static {}, Lcom/google/android/gms/plus/b/f;->a()Lcom/google/android/gms/plus/b/f;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p3, v0}, Lcom/google/android/gms/plus/b/f;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/plus/service/pos/j;

    move-result-object v4

    .line 259
    if-nez v4, :cond_0

    new-instance v1, Lcom/google/android/gms/plus/service/pos/k;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/pos/k;-><init>()V

    invoke-virtual {v1, p3}, Lcom/google/android/gms/plus/service/pos/k;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/pos/k;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/service/pos/k;->a(Z)Lcom/google/android/gms/plus/service/pos/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/plus/service/pos/k;->a()Lcom/google/android/gms/plus/service/pos/j;

    move-result-object v4

    .line 262
    :cond_0
    sget-object v8, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;

    .line 263
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/google/android/gms/p;->uF:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    move-object v0, p1

    move-object v3, p3

    move-object v7, p4

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/plus/b/g;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/plus/service/pos/j;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/data/a/b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/data/a/b;->a:Landroid/os/Bundle;

    .line 267
    invoke-static {v8, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 240
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/plus/b/h;->a()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 268
    :catch_0
    move-exception v0

    .line 270
    invoke-static {}, Lcom/google/android/gms/plus/b/f;->a()Lcom/google/android/gms/plus/b/f;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p3}, Lcom/google/android/gms/plus/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 271
    const-string v1, "PosAgent"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 272
    const-string v1, "PosAgent"

    const-string v2, "Network error deleting +1."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 274
    :cond_2
    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;I)Landroid/util/Pair;
    .locals 13

    .prologue
    .line 169
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/plus/b/a;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 171
    invoke-static {}, Lcom/google/android/gms/plus/b/f;->a()Lcom/google/android/gms/plus/b/f;

    move-result-object v10

    .line 172
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_3

    const/4 v2, 0x0

    move v9, v2

    .line 174
    :goto_0
    const/4 v2, 0x0

    .line 175
    const/4 v3, 0x1

    move/from16 v0, p5

    if-eq v0, v3, :cond_6

    .line 176
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v10, v2, v0}, Lcom/google/android/gms/plus/b/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/b/h;

    move-result-object v2

    move-object v3, v2

    .line 179
    :goto_1
    if-nez v3, :cond_4

    .line 180
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/plus/b/p;->b:Lcom/google/android/gms/plus/service/pos/o;

    invoke-static/range {p3 .. p3}, Lcom/google/android/gms/plus/b/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    const-string v6, "native:android_app"

    const-string v7, "plusones/%1$s"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v2}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v11, v12

    invoke-static {v7, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    if-eqz v8, :cond_0

    const-string v7, "container"

    invoke-static {v8}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v7, v8}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_0
    if-eqz v4, :cond_1

    const-string v7, "max_people"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v7, v4}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_1
    if-eqz v5, :cond_2

    const-string v4, "nolog"

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :cond_2
    const-string v4, "source"

    invoke-static {v6}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v4, v5}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, v3, Lcom/google/android/gms/plus/service/pos/o;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x0

    const/4 v6, 0x0

    const-class v7, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    move-object v3, p2

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    .line 188
    new-instance v3, Lcom/google/android/gms/plus/b/h;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v3, v2, v4, v5}, Lcom/google/android/gms/plus/b/h;-><init>(Lcom/google/android/gms/plus/service/pos/j;J)V

    move-object v2, v3

    .line 195
    :goto_2
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v10, v3, v0, v2}, Lcom/google/android/gms/plus/b/f;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/b/h;)V

    .line 200
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v2, Lcom/google/android/gms/plus/b/h;->a:Lcom/google/android/gms/plus/service/pos/j;

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v2, p1

    move v4, v9

    move-object/from16 v5, p3

    move-object/from16 v9, p4

    invoke-static/range {v2 .. v9}, Lcom/google/android/gms/plus/b/g;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/plus/service/pos/j;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/data/a/b;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/plus/data/a/b;->a:Landroid/os/Bundle;

    .line 205
    sget-object v3, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;

    invoke-static {v3, v2}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 172
    :cond_3
    new-instance v2, Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v3

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "https://www.googleapis.com/auth/pos"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/b/p;->d:Lcom/google/android/gms/plus/service/f;

    invoke-static {p1}, Lcom/google/android/gms/plus/b/b;->a(Landroid/content/Context;)Lcom/google/android/gms/plus/b/b;

    sget-object v4, Lcom/google/android/gms/common/internal/aj;->f:[Ljava/lang/String;

    invoke-interface {v3, p1, v2, p2, v4}, Lcom/google/android/gms/plus/service/f;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Lcom/google/android/gms/common/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/c;->b()Z

    move-result v2

    move v9, v2

    goto/16 :goto_0

    .line 191
    :cond_4
    :try_start_1
    new-instance v2, Lcom/google/android/gms/plus/b/h;

    iget-object v3, v3, Lcom/google/android/gms/plus/b/h;->a:Lcom/google/android/gms/plus/service/pos/j;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/gms/plus/b/h;-><init>(Lcom/google/android/gms/plus/service/pos/j;J)V
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 206
    :catch_0
    move-exception v2

    .line 208
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v10, v3, v0}, Lcom/google/android/gms/plus/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    const-string v3, "PosAgent"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 210
    const-string v3, "PosAgent"

    invoke-virtual {v2}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 212
    :cond_5
    throw v2

    :cond_6
    move-object v3, v2

    goto/16 :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/util/Pair;
    .locals 9

    .prologue
    .line 289
    invoke-static {}, Lcom/google/android/gms/plus/b/f;->a()Lcom/google/android/gms/plus/b/f;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/google/android/gms/plus/b/f;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/b/h;

    move-result-object v0

    .line 291
    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 292
    :goto_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/plus/b/a;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 296
    :try_start_0
    new-instance v1, Lcom/google/android/gms/plus/service/pos/k;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/pos/k;-><init>()V

    invoke-static {p3}, Lcom/google/android/gms/plus/b/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/plus/service/pos/k;->a(Ljava/lang/String;)Lcom/google/android/gms/plus/service/pos/k;

    move-result-object v1

    iput-object p4, v1, Lcom/google/android/gms/plus/service/pos/k;->a:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/android/gms/plus/service/pos/k;->b:Ljava/util/Set;

    const/4 v4, 0x2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x1

    invoke-virtual {v1, v3}, Lcom/google/android/gms/plus/service/pos/k;->a(Z)Lcom/google/android/gms/plus/service/pos/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/plus/service/pos/k;->a()Lcom/google/android/gms/plus/service/pos/j;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    .line 302
    iget-object v5, p0, Lcom/google/android/gms/plus/b/p;->b:Lcom/google/android/gms/plus/service/pos/o;

    invoke-static {p3}, Lcom/google/android/gms/plus/b/p;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "native:android_app"

    const-string v6, "plusones/%1$s"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v1}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v8

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    if-eqz p4, :cond_0

    const-string v6, "abtk"

    invoke-static {p4}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v6, v7}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_0
    if-eqz v0, :cond_5

    const-string v6, "cdx"

    invoke-static {v0}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v6, v0}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-eqz v2, :cond_1

    const-string v1, "container"

    invoke-static {v2}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    const-string v1, "source"

    invoke-static {v3}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v5, Lcom/google/android/gms/plus/service/pos/o;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/pos/PlusonesEntity;

    .line 320
    invoke-static {}, Lcom/google/android/gms/plus/b/f;->a()Lcom/google/android/gms/plus/b/f;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/gms/plus/b/g;->a(Lcom/google/android/gms/plus/service/pos/j;)Z

    move-result v3

    invoke-virtual {v1, v2, p3, v3}, Lcom/google/android/gms/plus/b/f;->a(Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/plus/service/pos/j;

    move-result-object v4

    .line 324
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    if-nez v4, :cond_2

    move-object v4, v0

    :cond_2
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/google/android/gms/p;->uE:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v3, Lcom/google/android/gms/p;->uD:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object v0, p1

    move-object v3, p3

    move-object v7, p5

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/plus/b/g;->a(Landroid/content/Context;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/plus/service/pos/j;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/plus/data/a/b;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/plus/data/a/b;->a:Landroid/os/Bundle;

    .line 330
    sget-object v1, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;

    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 291
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/plus/b/h;->a()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 331
    :catch_0
    move-exception v0

    .line 333
    invoke-static {}, Lcom/google/android/gms/plus/b/f;->a()Lcom/google/android/gms/plus/b/f;

    move-result-object v1

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, p3}, Lcom/google/android/gms/plus/b/f;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    const-string v1, "PosAgent"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 335
    const-string v1, "PosAgent"

    const-string v2, "Network error inserting +1."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 337
    :cond_4
    throw v0

    :cond_5
    move-object v0, v1

    goto/16 :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/plus/data/a/c;
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v7, 0x1

    const/4 v8, 0x0

    .line 70
    .line 73
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/plus/b/a;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 75
    iget-object v2, p0, Lcom/google/android/gms/plus/b/p;->b:Lcom/google/android/gms/plus/service/pos/o;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const-string v4, "native:android_app"

    const-string v0, "plusones/getsignupstate"

    if-eqz v1, :cond_0

    const-string v5, "container"

    invoke-static {v1}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v5, v1}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    if-eqz v3, :cond_1

    const-string v1, "nolog"

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    const-string v1, "source"

    invoke-static {v4}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/plus/service/pos/o;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v2, Lcom/google/android/gms/plus/service/pos/o;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/plus/service/pos/GetsignupstateEntity;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/service/pos/GetsignupstateEntity;

    .line 80
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 81
    const-string v2, "display_name"

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/pos/g;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    const-string v2, "signedUp"

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/pos/g;->f()Z

    move-result v3

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 84
    new-instance v2, Lcom/google/android/gms/common/internal/bs;

    invoke-interface {v0}, Lcom/google/android/gms/plus/service/pos/g;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/common/internal/bs;-><init>(Ljava/lang/String;)V

    sget v0, Lcom/google/android/gms/plus/b/p;->a:I

    invoke-virtual {v2, p1, v0}, Lcom/google/android/gms/common/internal/bs;->a(Landroid/content/Context;I)Lcom/google/android/gms/common/internal/br;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/br;->a()Ljava/lang/String;

    move-result-object v0

    .line 87
    const-string v2, "profile_image_url"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    const-string v0, "account_name"

    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 97
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    const-string v0, "com.google.android.gms.plus"

    invoke-virtual {v2, v0}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_2

    move-result-object v4

    :try_start_1
    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->getLocalContentProvider()Landroid/content/ContentProvider;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/provider/PlusProvider;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/plus/provider/PlusProvider;->a(Ljava/lang/String;Landroid/content/ContentValues;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    .line 102
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/plus/provider/d;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v1

    .line 105
    :try_start_3
    invoke-virtual {v1}, Landroid/database/AbstractWindowedCursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_3

    .line 109
    const-string v0, "PosAgent"

    const-string v2, "Hit data removed condition"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 124
    :catchall_0
    move-exception v0

    :goto_0
    if-eqz v1, :cond_2

    .line 125
    invoke-virtual {v1}, Landroid/database/AbstractWindowedCursor;->close()V

    :cond_2
    throw v0

    .line 97
    :catchall_1
    move-exception v0

    :try_start_4
    invoke-virtual {v4}, Landroid/content/ContentProviderClient;->release()Z

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 124
    :catchall_2
    move-exception v0

    move-object v1, v6

    goto :goto_0

    .line 115
    :cond_3
    :try_start_5
    const-string v0, "display_name"

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 117
    const-string v0, "profile_image_url"

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 119
    const-string v0, "signedUp"

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/database/AbstractWindowedCursor;->getInt(I)I

    move-result v0

    if-eqz v0, :cond_5

    move v0, v7

    .line 121
    :goto_1
    new-instance v4, Lcom/google/android/gms/plus/data/a/c;

    invoke-direct {v4, v2, v3, v0}, Lcom/google/android/gms/plus/data/a/c;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 124
    if-eqz v1, :cond_4

    .line 125
    invoke-virtual {v1}, Landroid/database/AbstractWindowedCursor;->close()V

    :cond_4
    return-object v4

    :cond_5
    move v0, v8

    .line 119
    goto :goto_1
.end method
