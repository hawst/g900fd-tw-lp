.class final Lcom/google/android/gms/plus/internal/bw;
.super Lcom/google/android/gms/common/internal/al;
.source "SourceFile"


# instance fields
.field final synthetic b:Lcom/google/android/gms/plus/internal/ay;

.field private final c:Lcom/google/android/gms/common/c;

.field private final d:Landroid/os/ParcelFileDescriptor;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/an;Lcom/google/android/gms/common/c;Landroid/os/ParcelFileDescriptor;)V
    .locals 0

    .prologue
    .line 940
    iput-object p1, p0, Lcom/google/android/gms/plus/internal/bw;->b:Lcom/google/android/gms/plus/internal/ay;

    .line 941
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/internal/al;-><init>(Lcom/google/android/gms/common/internal/aj;Ljava/lang/Object;)V

    .line 942
    iput-object p3, p0, Lcom/google/android/gms/plus/internal/bw;->c:Lcom/google/android/gms/common/c;

    .line 943
    iput-object p4, p0, Lcom/google/android/gms/plus/internal/bw;->d:Landroid/os/ParcelFileDescriptor;

    .line 944
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 959
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/bw;->d:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    .line 960
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/bw;->d:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    .line 962
    :cond_0
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 926
    check-cast p1, Lcom/google/android/gms/plus/internal/an;

    iget-object v0, p0, Lcom/google/android/gms/plus/internal/bw;->c:Lcom/google/android/gms/common/c;

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/bw;->d:Landroid/os/ParcelFileDescriptor;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/plus/internal/an;->a(Lcom/google/android/gms/common/c;Landroid/os/ParcelFileDescriptor;)V

    return-void
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 954
    invoke-super {p0}, Lcom/google/android/gms/common/internal/al;->d()V

    .line 955
    return-void
.end method
