.class public Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;
.super Lcom/google/android/gms/drive/ui/h;
.source "SourceFile"


# instance fields
.field private b:Lcom/google/android/gms/drive/ui/create/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/h;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/drive/auth/g;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;ILcom/google/android/gms/drive/DriveId;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 117
    const-string v0, "app"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 119
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 120
    const-class v0, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;

    invoke-virtual {v1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 121
    const-string v0, "accountName"

    iget-object v2, p1, Lcom/google/android/gms/drive/auth/g;->a:Lcom/google/android/gms/drive/database/model/a;

    iget-object v2, v2, Lcom/google/android/gms/drive/database/model/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 122
    const-string v0, "callerPackagingId"

    iget-wide v2, p1, Lcom/google/android/gms/drive/auth/g;->b:J

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 123
    const-string v0, "callerPackageName"

    iget-object v2, p1, Lcom/google/android/gms/drive/auth/g;->c:Lcom/google/android/gms/drive/auth/AppIdentity;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/auth/AppIdentity;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 124
    const-string v0, "metadata"

    invoke-virtual {v1, v0, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 125
    const-string v0, "requestId"

    invoke-virtual {v1, v0, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 126
    const-string v0, "selectedCollectionDriveId"

    invoke-virtual {v1, v0, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 127
    const-string v0, "dialogTitle"

    invoke-virtual {v1, v0, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 128
    const-string v0, "isShortcut"

    invoke-virtual {v1, v0, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 133
    iget-object v0, p1, Lcom/google/android/gms/drive/auth/g;->e:Ljava/util/Set;

    invoke-static {v0}, Lcom/google/android/gms/drive/aa;->a(Ljava/lang/Iterable;)Ljava/util/Set;

    move-result-object v0

    .line 134
    const-string v2, "clientScopes"

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 137
    return-object v1
.end method


# virtual methods
.method protected final d()V
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->b:Lcom/google/android/gms/drive/ui/create/a;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/create/a;->c()V

    .line 143
    return-void
.end method

.method public onBackPressed()V
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->b:Lcom/google/android/gms/drive/ui/create/a;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/create/a;->b()V

    .line 107
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/h;->onBackPressed()V

    .line 108
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 100
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/h;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 101
    invoke-static {p0}, Lcom/google/android/gms/drive/ui/p;->a(Landroid/app/Activity;)V

    .line 102
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 41
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/h;->onCreate(Landroid/os/Bundle;)V

    .line 42
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 44
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/app/ActionBar;->setDisplayShowHomeEnabled(Z)V

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setDisplayShowTitleEnabled(Z)V

    .line 46
    :cond_0
    const-string v0, "dialogTitle"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 47
    if-nez v0, :cond_1

    .line 48
    sget v0, Lcom/google/android/gms/p;->fM:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 49
    const-string v4, "dialogTitle"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 51
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->setTitle(Ljava/lang/CharSequence;)V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    const-string v4, "CreateDocumentActivity"

    invoke-virtual {v0, v4}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/create/a;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->b:Lcom/google/android/gms/drive/ui/create/a;

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->b:Lcom/google/android/gms/drive/ui/create/a;

    if-nez v0, :cond_6

    .line 56
    invoke-virtual {v3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v3

    .line 57
    if-eqz v3, :cond_2

    const-string v0, "accountName"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v2

    :goto_0
    if-nez v0, :cond_5

    .line 58
    invoke-virtual {p0, v2}, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->setResult(I)V

    .line 59
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->finish()V

    .line 76
    :goto_1
    return-void

    .line 57
    :cond_3
    const-string v0, "clientScopes"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0

    .line 63
    :cond_5
    new-instance v0, Lcom/google/android/gms/drive/ui/create/a;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ui/create/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->b:Lcom/google/android/gms/drive/ui/create/a;

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->b:Lcom/google/android/gms/drive/ui/create/a;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/drive/ui/create/a;->setArguments(Landroid/os/Bundle;)V

    .line 65
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    const v1, 0x1020002

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->b:Lcom/google/android/gms/drive/ui/create/a;

    const-string v4, "CreateDocumentActivity"

    invoke-virtual {v0, v1, v3, v4}, Landroid/support/v4/app/aj;->a(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 69
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->b:Lcom/google/android/gms/drive/ui/create/a;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->a:Lcom/google/android/gms/common/api/v;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/create/a;->a(Lcom/google/android/gms/common/api/v;)V

    .line 71
    if-nez p1, :cond_7

    .line 73
    invoke-virtual {p0, v2}, Lcom/google/android/gms/drive/ui/create/CreateFileActivityDelegate;->setResult(I)V

    .line 75
    :cond_7
    invoke-static {p0}, Lcom/google/android/gms/drive/ui/p;->a(Landroid/app/Activity;)V

    goto :goto_1
.end method
