.class public Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 100
    const-string v0, "TransactionReplyService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 101
    return-void
.end method

.method public static a(Ljava/lang/String;[BLcom/google/ab/b/a/e/t;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 66
    iget-object v0, p2, Lcom/google/ab/b/a/e/t;->a:Lcom/google/ab/b/a/e/u;

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->a(Ljava/lang/String;[BLcom/google/ab/b/a/e/u;)Landroid/content/Intent;

    move-result-object v0

    .line 67
    const-string v1, "message"

    iget-object v2, p2, Lcom/google/ab/b/a/e/t;->b:[B

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 68
    return-object v0
.end method

.method private static a(Ljava/lang/String;[BLcom/google/ab/b/a/e/u;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 91
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    .line 92
    new-instance v0, Landroid/content/Intent;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 93
    const-string v1, "account"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 94
    const-string v1, "keyHandle"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 95
    const-string v1, "type"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 96
    return-object v0
.end method

.method public static a(Ljava/lang/String;[BLcom/google/protobuf/a/a;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 81
    sget-object v0, Lcom/google/ab/b/a/e/u;->e:Lcom/google/ab/b/a/e/u;

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->a(Ljava/lang/String;[BLcom/google/ab/b/a/e/u;)Landroid/content/Intent;

    move-result-object v0

    .line 82
    if-eqz p2, :cond_0

    .line 84
    const-string v1, "txId"

    invoke-virtual {p2}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 86
    :cond_0
    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 254
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "oauth2:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/auth/authzen/a/b;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 257
    const/4 v1, 0x0

    :goto_0
    const/4 v3, 0x3

    if-ge v1, v3, :cond_1

    .line 258
    if-lez v1, :cond_0

    .line 259
    invoke-static {}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->a()V

    .line 262
    :cond_0
    :try_start_0
    invoke-static {p0, p1, v2}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 270
    :cond_1
    :goto_1
    return-object v0

    .line 263
    :catch_0
    move-exception v3

    .line 264
    const-string v4, "AuthZen"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error getting auth token, attempt "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 257
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 265
    :catch_1
    move-exception v1

    .line 266
    const-string v2, "AuthZen"

    const-string v3, "Error getting auth token"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method private a(Lcom/google/ab/b/a/e/t;[B)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 277
    new-instance v1, Lcom/google/android/gms/auth/authzen/keyservice/g;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/authzen/keyservice/g;-><init>(Landroid/content/Context;)V

    .line 278
    invoke-virtual {v1, p2}, Lcom/google/android/gms/auth/authzen/keyservice/g;->a([B)Lcom/google/android/gms/auth/authzen/keyservice/b;

    move-result-object v2

    .line 279
    if-nez v2, :cond_0

    .line 280
    const-string v1, "AuthZen"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No encryption key found for given handle: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 301
    :goto_0
    return-object v0

    .line 284
    :cond_0
    :try_start_0
    const-string v3, "device_key"

    invoke-virtual {v1, v3}, Lcom/google/android/gms/auth/authzen/keyservice/g;->a(Ljava/lang/String;)Ljava/security/KeyPair;

    move-result-object v1

    .line 285
    if-nez v1, :cond_1

    .line 286
    const-string v1, "AuthZen"

    const-string v2, "Unable to get signing key. Cannot send authzen reply."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/gms/auth/authzen/keyservice/h; {:try_start_0 .. :try_end_0} :catch_3

    goto :goto_0

    .line 292
    :catch_0
    move-exception v1

    .line 293
    const-string v2, "AuthZen"

    const-string v3, "Error"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 289
    :cond_1
    :try_start_1
    iget-object v2, v2, Lcom/google/android/gms/auth/authzen/keyservice/b;->a:Lcom/google/android/gms/auth/authzen/keyservice/c;

    iget-object v2, v2, Lcom/google/android/gms/auth/authzen/keyservice/c;->b:Ljavax/crypto/SecretKey;

    invoke-static {p1, v1, v2}, Lcom/google/ab/b/a/e/s;->a(Lcom/google/ab/b/a/e/t;Ljava/security/KeyPair;Ljavax/crypto/SecretKey;)[B

    move-result-object v1

    .line 291
    invoke-static {v1}, Lcom/google/android/gms/common/util/m;->c([B)Ljava/lang/String;
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Lcom/google/android/gms/auth/authzen/keyservice/h; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v0

    goto :goto_0

    .line 294
    :catch_1
    move-exception v1

    .line 295
    const-string v2, "AuthZen"

    const-string v3, "Error"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 296
    :catch_2
    move-exception v1

    .line 297
    const-string v2, "AuthZen"

    const-string v3, "Error"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 298
    :catch_3
    move-exception v1

    .line 299
    const-string v2, "AuthZen"

    const-string v3, "Error"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private static a()V
    .locals 2

    .prologue
    .line 231
    const-wide/16 v0, 0x1f4

    :try_start_0
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(ILjava/lang/Exception;)V
    .locals 2

    .prologue
    .line 222
    new-instance v0, Lcom/google/android/gms/auth/e/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/e/c;-><init>(Landroid/content/Context;)V

    .line 223
    const/4 v1, 0x3

    iput v1, v0, Lcom/google/android/gms/auth/e/c;->e:I

    .line 224
    iput p1, v0, Lcom/google/android/gms/auth/e/c;->j:I

    .line 225
    invoke-virtual {v0}, Lcom/google/android/gms/auth/e/c;->a()V

    .line 226
    const-string v0, "AuthZen"

    const-string v1, "Error"

    invoke-static {v0, v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 227
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ab/b/a/e/u;Ljava/lang/String;)V
    .locals 11

    .prologue
    .line 168
    if-eqz p2, :cond_2

    .line 169
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, p1, p1, v2}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/auth/authzen/a/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    const-string v1, "auth_token"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v9, v0

    .line 174
    :goto_0
    const-string v0, "com.google.android.gms.API_KEY"

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/common/util/e;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    new-instance v10, Lcom/google/android/gms/auth/gencode/authzen/server/api/a;

    new-instance v0, Lcom/google/android/gms/common/server/n;

    invoke-static {}, Lcom/google/android/gms/auth/authzen/a/b;->a()Ljava/lang/String;

    move-result-object v2

    const-string v3, "cryptauth/v1/"

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/common/server/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v10, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/a;-><init>(Lcom/google/android/gms/common/server/n;)V

    .line 176
    const/4 v0, 0x0

    move v6, v0

    :goto_1
    const/4 v0, 0x3

    if-ge v6, v0, :cond_3

    .line 177
    if-eqz v6, :cond_0

    .line 178
    const-string v0, "AuthZen"

    const-string v1, "attempting send operation again"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/gms/auth/authzen/transaction/o;->a:[I

    invoke-virtual {p3}, Lcom/google/ab/b/a/e/u;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    const-string v0, "AuthZen"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid message type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 196
    :cond_1
    :goto_2
    return-void

    .line 171
    :cond_2
    const-string v0, "AuthZen"

    const-string v1, "Creating client context without an auth token"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 172
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v1

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/auth/authzen/a/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    move-object v9, v0

    goto :goto_0

    .line 181
    :pswitch_0
    :try_start_1
    new-instance v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/aq;

    invoke-direct {v1}, Lcom/google/android/gms/auth/gencode/authzen/server/api/aq;-><init>()V

    iput-object p1, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/aq;->a:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/aq;->c:Ljava/util/Set;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-object p4, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/aq;->b:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/aq;->c:Ljava/util/Set;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ReplyTxRequestEntity;

    iget-object v2, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/aq;->c:Ljava/util/Set;

    iget-object v3, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/aq;->a:Ljava/lang/String;

    iget-object v1, v1, Lcom/google/android/gms/auth/gencode/authzen/server/api/aq;->b:Ljava/lang/String;

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/gms/auth/gencode/authzen/server/api/ReplyTxRequestEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/ReplyTxRequestEntity;

    const-string v1, "authzen/replytx"

    iget-object v2, v10, Lcom/google/android/gms/auth/gencode/authzen/server/api/a;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x1

    invoke-virtual {v2, v9, v3, v1, v0}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    :goto_3
    const-string v0, "AuthZen"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Sent HTTP message of type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_2

    .line 183
    :catch_0
    move-exception v0

    .line 184
    invoke-direct {p0, v6, v0}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->a(ILjava/lang/Exception;)V

    .line 190
    :goto_4
    invoke-static {}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->a()V

    .line 176
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_1

    .line 181
    :pswitch_1
    :try_start_2
    new-instance v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/bc;

    invoke-direct {v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/bc;-><init>()V

    iput-object p1, v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/bc;->a:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/bc;->c:Ljava/util/Set;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-object p4, v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/bc;->b:Ljava/lang/String;

    iget-object v1, v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/bc;->c:Ljava/util/Set;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/google/android/gms/auth/gencode/authzen/server/api/SyncTxRequestEntity;

    iget-object v1, v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/bc;->c:Ljava/util/Set;

    iget-object v2, v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/bc;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/auth/gencode/authzen/server/api/bc;->b:Ljava/lang/String;

    invoke-direct {v4, v1, v2, v0}, Lcom/google/android/gms/auth/gencode/authzen/server/api/SyncTxRequestEntity;-><init>(Ljava/util/Set;Ljava/lang/String;Ljava/lang/String;)V

    check-cast v4, Lcom/google/android/gms/auth/gencode/authzen/server/api/SyncTxRequestEntity;

    const-string v3, "authzen/synctx"

    iget-object v0, v10, Lcom/google/android/gms/auth/gencode/authzen/server/api/a;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/gms/auth/gencode/authzen/server/api/SyncTxResponseEntity;

    move-object v1, v9

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_2
    .catch Lcom/google/android/gms/auth/q; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 185
    :catch_1
    move-exception v0

    .line 186
    invoke-direct {p0, v6, v0}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->a(ILjava/lang/Exception;)V

    goto :goto_4

    .line 192
    :cond_3
    const/4 v0, 0x3

    if-ne v6, v0, :cond_1

    sget-object v0, Lcom/google/ab/b/a/e/u;->d:Lcom/google/ab/b/a/e/u;

    if-ne p3, v0, :cond_1

    .line 193
    sget v0, Lcom/google/android/gms/p;->aq:I

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    goto/16 :goto_2

    .line 181
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v2, 0x1

    .line 105
    const-string v0, "AuthZen"

    const-string v1, "Sending authzen message..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 107
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 108
    const-class v1, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v2

    .line 110
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 112
    const-wide/16 v0, 0xbb8

    :try_start_0
    invoke-virtual {v2, v0, v1}, Landroid/os/PowerManager$WakeLock;->acquire(J)V

    .line 113
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v0, "type"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/ab/b/a/e/u;

    invoke-static {p0, v3}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    const-string v1, "AuthZen"

    const-string v5, "Failed to get auth token"

    invoke-static {v1, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v1, "message"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    if-nez v1, :cond_3

    sget-object v1, Lcom/google/ab/b/a/e/u;->e:Lcom/google/ab/b/a/e/u;

    if-ne v0, v1, :cond_6

    new-instance v5, Lcom/google/android/gms/auth/authzen/b/c;

    invoke-direct {v5, p0}, Lcom/google/android/gms/auth/authzen/b/c;-><init>(Landroid/content/Context;)V

    iget-object v1, v5, Lcom/google/android/gms/auth/authzen/b/c;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v5, v1}, Lcom/google/android/gms/auth/authzen/b/c;->a(Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v6

    new-instance v7, Lcom/google/ab/b/a/a/d;

    invoke-direct {v7}, Lcom/google/ab/b/a/a/d;-><init>()V

    iget-object v1, v6, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v7, v1}, Lcom/google/ab/b/a/a/d;->b(Ljava/lang/String;)Lcom/google/ab/b/a/a/d;

    move-result-object v7

    iget-object v1, v6, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    int-to-long v8, v1

    invoke-virtual {v7, v8, v9}, Lcom/google/ab/b/a/a/d;->a(J)Lcom/google/ab/b/a/a/d;

    move-result-object v1

    iget-object v6, v5, Lcom/google/android/gms/auth/authzen/b/c;->a:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/gms/auth/authzen/a/a;->b(Landroid/content/Context;)Z

    move-result v6

    invoke-virtual {v1, v6}, Lcom/google/ab/b/a/a/d;->a(Z)Lcom/google/ab/b/a/a/d;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/auth/authzen/b/c;->a()Z

    move-result v6

    invoke-virtual {v1, v6}, Lcom/google/ab/b/a/a/d;->d(Z)Lcom/google/ab/b/a/a/d;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/auth/authzen/b/c;->b()Z

    move-result v6

    invoke-virtual {v1, v6}, Lcom/google/ab/b/a/a/d;->e(Z)Lcom/google/ab/b/a/a/d;

    move-result-object v1

    invoke-virtual {v5}, Lcom/google/android/gms/auth/authzen/b/c;->c()Z

    move-result v6

    invoke-virtual {v1, v6}, Lcom/google/ab/b/a/a/d;->b(Z)Lcom/google/ab/b/a/a/d;

    move-result-object v1

    invoke-virtual {v5}, Lcom/google/android/gms/auth/authzen/b/c;->d()Z

    move-result v5

    invoke-virtual {v1, v5}, Lcom/google/ab/b/a/a/d;->c(Z)Lcom/google/ab/b/a/a/d;

    move-result-object v1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/ab/b/a/a/d;->a(Ljava/lang/String;)Lcom/google/ab/b/a/a/d;

    move-result-object v1

    if-eqz v4, :cond_1

    invoke-virtual {v1, v4}, Lcom/google/ab/b/a/a/d;->c(Ljava/lang/String;)Lcom/google/ab/b/a/a/d;

    :cond_1
    new-instance v5, Lcom/google/ab/b/a/a/t;

    invoke-direct {v5}, Lcom/google/ab/b/a/a/t;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Lcom/google/ab/b/a/a/t;->a(J)Lcom/google/ab/b/a/a/t;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/google/ab/b/a/a/t;->a(Lcom/google/ab/b/a/a/d;)Lcom/google/ab/b/a/a/t;

    move-result-object v1

    const-string v5, "txId"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string v5, "txId"

    invoke-virtual {p1, v5}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v5

    invoke-static {v5}, Lcom/google/protobuf/a/a;->a([B)Lcom/google/protobuf/a/a;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/ab/b/a/a/t;->a(Lcom/google/protobuf/a/a;)Lcom/google/ab/b/a/a/t;

    :cond_2
    invoke-virtual {v1}, Lcom/google/ab/b/a/a/t;->g()[B

    move-result-object v1

    :cond_3
    sget-object v5, Lcom/google/android/gms/auth/authzen/transaction/n;->a:[I

    invoke-virtual {v0}, Lcom/google/ab/b/a/e/u;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    :goto_0
    if-eqz v1, :cond_5

    new-instance v5, Lcom/google/ab/b/a/e/t;

    invoke-direct {v5, v0, v1}, Lcom/google/ab/b/a/e/t;-><init>(Lcom/google/ab/b/a/e/u;[B)V

    const-string v1, "keyHandle"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    invoke-direct {p0, v5, v1}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->a(Lcom/google/ab/b/a/e/t;[B)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    const/4 v1, 0x2

    invoke-static {v1}, Lcom/google/android/gms/auth/authzen/a/b;->a(I)Z

    move-result v1

    if-eqz v1, :cond_4

    sget-object v1, Lcom/google/ab/b/a/e/u;->e:Lcom/google/ab/b/a/e/u;

    if-ne v0, v1, :cond_4

    const-string v1, "AuthZen"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Sending GCM upstream message of type: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v1, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/google/android/gms/auth/authzen/transaction/g;

    const-string v6, "TRS"

    invoke-direct {v1, p0, v6}, Lcom/google/android/gms/auth/authzen/transaction/g;-><init>(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-virtual {v0}, Lcom/google/ab/b/a/e/u;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6, v3, v4, v5}, Lcom/google/android/gms/auth/authzen/transaction/g;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_4
    :goto_1
    const/4 v1, 0x1

    :try_start_2
    invoke-static {v1}, Lcom/google/android/gms/auth/authzen/a/b;->a(I)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-direct {p0, v3, v4, v0, v5}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ab/b/a/e/u;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 115
    :cond_5
    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 116
    return-void

    .line 113
    :cond_6
    :try_start_3
    const-string v1, "AuthZen"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "cannot build message for payload type "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    goto :goto_0

    :pswitch_0
    invoke-static {p0, v1}, Lcom/google/android/gms/auth/authzen/transaction/m;->a(Landroid/content/Context;[B)[B

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v6, -0x1

    invoke-direct {p0, v6, v1}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->a(ILjava/lang/Exception;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 115
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Landroid/os/PowerManager$WakeLock;->release()V

    throw v0

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
