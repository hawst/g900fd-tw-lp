.class public final Lcom/google/android/gms/wallet/common/ui/cz;
.super Lcom/google/android/gms/common/ui/a;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# static fields
.field static final j:Ljava/lang/Object;

.field static final k:Ljava/lang/Object;


# instance fields
.field private l:I

.field private m:Lcom/google/checkout/inapp/proto/j;

.field private n:Lcom/google/checkout/inapp/proto/a/b;

.field private o:[Lcom/google/checkout/inapp/proto/j;

.field private p:Ljava/util/ArrayList;

.field private q:Z

.field private r:Z

.field private s:[I

.field private t:[I

.field private u:Ljava/util/ArrayList;

.field private v:Lcom/google/android/gms/wallet/common/ui/db;

.field private w:Lcom/google/android/gms/wallet/shared/common/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/cz;->j:Ljava/lang/Object;

    .line 98
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/common/ui/cz;->k:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/gms/common/ui/a;-><init>()V

    .line 1017
    return-void
.end method

.method private static a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 4

    .prologue
    .line 356
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/cz;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/common/ui/cz;-><init>()V

    .line 357
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 358
    const-string v2, "mode"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 359
    const-string v2, "instrument"

    invoke-static {v1, v2, p1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 360
    const-string v2, "address"

    invoke-static {v1, v2, p2}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 361
    const-string v2, "instruments"

    invoke-static {p3}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([Lcom/google/protobuf/nano/j;)Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 363
    const-string v2, "existingAddresses"

    invoke-static {v1, v2, p4}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/util/Collection;)V

    .line 365
    const-string v2, "requiresFullBillingAddress"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 366
    const-string v2, "phoneNumberRequired"

    invoke-virtual {v1, v2, p6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 367
    const-string v2, "disallowedPaymentInstrumentTypes"

    invoke-virtual {v1, v2, p7}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 369
    const-string v2, "disallowedCardCategories"

    invoke-virtual {v1, v2, p8}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 371
    const-string v2, "allowedShippingCountryCodes"

    invoke-virtual {v1, v2, p9}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 373
    const-string v2, "buyFlowConfig"

    invoke-virtual {v1, v2, p10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 374
    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cz;->setArguments(Landroid/os/Bundle;)V

    .line 375
    return-object v0
.end method

.method public static a(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 115
    const/16 v0, 0x9

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v6, v5

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/cz;->a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/checkout/inapp/proto/a/b;Ljava/util/List;Ljava/util/ArrayList;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 224
    const/4 v0, 0x2

    const/4 v5, 0x1

    move-object v2, p0

    move-object v3, v1

    move-object v4, p1

    move v6, p3

    move-object v7, v1

    move-object v8, v1

    move-object v9, p2

    move-object v10, p4

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/cz;->a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/checkout/inapp/proto/j;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 11

    .prologue
    const/4 v2, 0x0

    .line 252
    const/4 v0, 0x3

    const/4 v5, 0x1

    move-object v1, p0

    move-object v3, p1

    move-object v4, p2

    move v6, p3

    move-object v7, v2

    move-object v8, v2

    move-object v9, v2

    move-object v10, p4

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/cz;->a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/List;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 324
    const/16 v0, 0x66

    const/4 v5, 0x1

    move-object v2, v1

    move-object v3, v1

    move-object v4, p0

    move v6, p1

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p2

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/cz;->a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    return-object v0
.end method

.method public static a([Lcom/google/checkout/inapp/proto/j;ZZ[I[ILcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 11

    .prologue
    .line 277
    const/16 v0, 0x65

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v9, 0x0

    move-object v3, p0

    move v5, p1

    move v6, p2

    move-object v7, p3

    move-object v8, p4

    move-object/from16 v10, p5

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/cz;->a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    return-object v0
.end method

.method private a(I)Ljava/lang/Object;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 669
    iget-object v0, p0, Landroid/support/v4/app/m;->f:Landroid/app/Dialog;

    .line 670
    instance-of v2, v0, Landroid/app/AlertDialog;

    if-nez v2, :cond_0

    move-object v0, v1

    .line 680
    :goto_0
    return-object v0

    .line 673
    :cond_0
    check-cast v0, Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->getListView()Landroid/widget/ListView;

    move-result-object v0

    .line 674
    if-nez v0, :cond_1

    move-object v0, v1

    .line 675
    goto :goto_0

    .line 677
    :cond_1
    if-ltz p1, :cond_2

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v2

    if-ge p1, v2, :cond_2

    .line 678
    invoke-virtual {v0, p1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 680
    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/cz;Ljava/lang/Object;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 54
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cz;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v1, p1, Lcom/google/checkout/inapp/proto/j;

    if-eqz v1, :cond_1

    check-cast p1, Lcom/google/checkout/inapp/proto/j;

    iget-object v0, p1, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    instance-of v1, p1, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_4

    check-cast p1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v1

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    if-eqz v1, :cond_2

    sget v0, Lcom/google/android/gms/p;->AH:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    sget v0, Lcom/google/android/gms/p;->AF:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    if-eqz v1, :cond_3

    sget v0, Lcom/google/android/gms/p;->AG:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    sget v0, Lcom/google/android/gms/p;->AE:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :sswitch_2
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    const-string v1, ", "

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_4
    sget-object v1, Lcom/google/android/gms/wallet/common/ui/cz;->k:Ljava/lang/Object;

    if-ne p1, v1, :cond_5

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->Ba:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    sget-object v1, Lcom/google/android/gms/wallet/common/ui/cz;->j:Ljava/lang/Object;

    if-ne p1, v1, :cond_0

    iget v1, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    packed-switch v1, :pswitch_data_1

    :pswitch_1
    goto :goto_0

    :pswitch_2
    sget v0, Lcom/google/android/gms/p;->AD:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    sget v0, Lcom/google/android/gms/p;->AB:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    sget v0, Lcom/google/android/gms/p;->AC:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Ljava/util/ArrayList;Lcom/google/checkout/inapp/proto/a/b;Ljava/util/Collection;)Ljava/util/ArrayList;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 391
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 392
    if-eqz p0, :cond_4

    .line 394
    if-eqz p2, :cond_5

    .line 395
    new-instance v0, Ljava/util/TreeSet;

    sget-object v1, Ljava/lang/String;->CASE_INSENSITIVE_ORDER:Ljava/util/Comparator;

    invoke-direct {v0, v1}, Ljava/util/TreeSet;-><init>(Ljava/util/Comparator;)V

    .line 396
    invoke-virtual {v0, p2}, Ljava/util/TreeSet;->addAll(Ljava/util/Collection;)Z

    move-object v1, v0

    .line 398
    :goto_0
    new-instance v4, Ljava/util/LinkedHashMap;

    invoke-direct {v4}, Ljava/util/LinkedHashMap;-><init>()V

    .line 400
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 401
    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 402
    invoke-static {v0}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 405
    if-eqz p1, :cond_1

    iget-object v6, p1, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    iget-object v7, v0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 408
    :cond_1
    if-eqz v1, :cond_2

    iget-object v6, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v6, v6, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v6}, Ljava/util/TreeSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 413
    :cond_2
    iget-object v6, v0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-virtual {v4, v6, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 417
    :cond_3
    invoke-virtual {v4}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/gms/wallet/common/a/e;->a(Ljava/util/Collection;[C)Ljava/util/ArrayList;

    move-result-object v0

    .line 419
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/t/a/b;

    .line 420
    invoke-virtual {v4, v0}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 423
    :cond_4
    return-object v3

    :cond_5
    move-object v1, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/cz;Landroid/widget/ImageView;Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 54
    instance-of v0, p2, Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_0

    move-object v0, p2

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cz;->w:Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/wallet/common/w;->a(Landroid/widget/ImageView;Lcom/google/checkout/inapp/proto/j;Lcom/google/android/gms/wallet/shared/common/b/a;)Z

    move-result v0

    if-eqz v0, :cond_0

    check-cast p2, Lcom/google/checkout/inapp/proto/j;

    invoke-static {p2}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/j;)Lcom/google/aa/b/a/g;

    move-result-object v0

    iget-object v0, v0, Lcom/google/aa/b/a/g;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/cz;Lcom/google/checkout/inapp/proto/a/b;)Z
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/common/ui/cz;Lcom/google/checkout/inapp/proto/j;)Z
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v0

    return v0
.end method

.method private a(Lcom/google/checkout/inapp/proto/a/b;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 914
    if-nez p1, :cond_0

    move v0, v1

    .line 932
    :goto_0
    return v0

    .line 917
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->b(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 918
    goto :goto_0

    .line 920
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->u:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    .line 922
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 923
    iget-object v4, p1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v4, v4, Lcom/google/t/a/b;->a:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    .line 928
    :goto_1
    if-nez v0, :cond_3

    move v0, v1

    .line 929
    goto :goto_0

    :cond_3
    move v0, v2

    .line 932
    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private a(Lcom/google/checkout/inapp/proto/j;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 895
    if-nez p1, :cond_1

    .line 910
    :cond_0
    :goto_0
    return v0

    .line 898
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cz;->t:[I

    iget v3, p1, Lcom/google/checkout/inapp/proto/j;->l:I

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 901
    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cz;->s:[I

    iget v3, p1, Lcom/google/checkout/inapp/proto/j;->c:I

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 904
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v0, v1

    .line 905
    goto :goto_0

    .line 907
    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->d(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 908
    goto :goto_0
.end method

.method public static b(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 121
    const/16 v0, 0xa

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v6, v5

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/cz;->a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/util/List;ZLcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 345
    const/16 v0, 0x68

    const/4 v5, 0x1

    move-object v2, v1

    move-object v3, v1

    move-object v4, p0

    move v6, p1

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p2

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/cz;->a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    return-object v0
.end method

.method public static b([Lcom/google/checkout/inapp/proto/j;ZZ[I[ILcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 11

    .prologue
    .line 302
    const/16 v0, 0x67

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v9, 0x0

    move-object v3, p0

    move v5, p1

    move v6, p2

    move-object v7, p3

    move-object v8, p4

    move-object/from16 v10, p5

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/cz;->a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/common/ui/cz;Ljava/lang/Object;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 54
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cz;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v0, p1, Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_1

    move-object v1, p1

    check-cast v1, Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cz;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const/4 v2, 0x1

    iget-boolean v3, p0, Lcom/google/android/gms/wallet/common/ui/cz;->q:Z

    iget-boolean v4, p0, Lcom/google/android/gms/wallet/common/ui/cz;->r:Z

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/cz;->s:[I

    iget-object v6, p0, Lcom/google/android/gms/wallet/common/ui/cz;->t:[I

    invoke-static/range {v0 .. v7}, Lcom/google/android/gms/wallet/common/w;->a(Landroid/content/Context;Lcom/google/checkout/inapp/proto/j;ZZZ[I[I[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    :cond_0
    :goto_0
    return-object v7

    :cond_1
    instance-of v0, p1, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/google/checkout/inapp/proto/a/b;

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    iget-object v0, p1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    const-string v1, ", "

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/dynamite/common/b/a;->a(Lcom/google/t/a/b;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :sswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->c(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget v0, Lcom/google/android/gms/p;->BX:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    :cond_2
    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->r:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/wallet/common/w;->a(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-nez v0, :cond_0

    sget v0, Lcom/google/android/gms/p;->Cb:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->getString(I)Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x3 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public static c(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 127
    const/16 v0, 0xb

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v6, v5

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/cz;->a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    return-object v0
.end method

.method public static d(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 142
    const/4 v0, 0x4

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v6, v5

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/cz;->a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/4 v1, 0x0

    .line 156
    const/16 v0, 0xc

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v6, v5

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/cz;->a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    return-object v0
.end method

.method public static f(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 171
    const/4 v0, 0x5

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v6, v5

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/cz;->a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    return-object v0
.end method

.method public static g(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 11

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 182
    const/4 v0, 0x6

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v6, v5

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/cz;->a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    return-object v0
.end method

.method public static h(Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;
    .locals 11

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 196
    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v5, v0

    move v6, v0

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, p0

    invoke-static/range {v0 .. v10}, Lcom/google/android/gms/wallet/common/ui/cz;->a(ILcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;[Lcom/google/checkout/inapp/proto/j;Ljava/util/List;ZZ[I[ILjava/util/ArrayList;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/ui/cz;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/wallet/common/ui/db;)V
    .locals 0

    .prologue
    .line 432
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    .line 433
    return-void
.end method

.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 13

    .prologue
    const/16 v12, 0x8

    const/4 v11, 0x7

    const/4 v10, 0x1

    const/4 v1, 0x0

    const/4 v9, -0x1

    .line 467
    if-nez p1, :cond_0

    .line 469
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cz;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "buyFlowConfig"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    .line 470
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cz;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v3

    const-string v4, "required_action_dialog"

    new-array v5, v10, [Ljava/lang/Object;

    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    sparse-switch v0, :sswitch_data_0

    const-string v6, "RequiredActionDialogFra"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "No mode string found for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "mode_unknown"

    :goto_0
    aput-object v0, v5, v1

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 476
    :cond_0
    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cz;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p0}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 482
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    sparse-switch v0, :sswitch_data_1

    .line 599
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Illegal dialog mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 470
    :sswitch_0
    const-string v0, "mode_new_instrument"

    goto :goto_0

    :sswitch_1
    const-string v0, "mode_new_shipping_address"

    goto :goto_0

    :sswitch_2
    const-string v0, "mode_full_billing_address"

    goto :goto_0

    :sswitch_3
    const-string v0, "mode_selected_instrument_not_found"

    goto :goto_0

    :sswitch_4
    const-string v0, "mode_selected_address_not_found"

    goto :goto_0

    :sswitch_5
    const-string v0, "mode_selected_prepaid_disallowed"

    goto :goto_0

    :sswitch_6
    const-string v0, "mode_selected_debit_disallowed"

    goto :goto_0

    :sswitch_7
    const-string v0, "mode_selected_amex_disallowed"

    goto :goto_0

    :sswitch_8
    const-string v0, "mode_accept_tos"

    goto :goto_0

    :sswitch_9
    const-string v0, "mode_instrument_selector"

    goto :goto_0

    :sswitch_a
    const-string v0, "mode_shipping_address_selector"

    goto :goto_0

    :sswitch_b
    const-string v0, "mode_upgrade_instrument"

    goto :goto_0

    :sswitch_c
    const-string v0, "mode_update_shipping_address"

    goto :goto_0

    :sswitch_d
    const-string v0, "mode_choose_alternative_instrument"

    goto :goto_0

    :sswitch_e
    const-string v0, "mode_choose_alternative_shipping_address"

    goto :goto_0

    :sswitch_f
    const-string v0, "mode_wallet_balance_not_available"

    goto :goto_0

    .line 484
    :sswitch_10
    sget v0, Lcom/google/android/gms/p;->AU:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 486
    sget v0, Lcom/google/android/gms/p;->AM:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 488
    sget v0, Lcom/google/android/gms/p;->Ch:I

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 489
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 622
    :goto_1
    return-object v0

    .line 491
    :sswitch_11
    sget v0, Lcom/google/android/gms/p;->AW:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 493
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->p:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cz;->n:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/cz;->u:Ljava/util/ArrayList;

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Ljava/util/ArrayList;Lcom/google/checkout/inapp/proto/a/b;Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v0

    .line 495
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 496
    sget v0, Lcom/google/android/gms/p;->AN:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 499
    sget v0, Lcom/google/android/gms/p;->Ch:I

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 510
    :goto_2
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_1

    .line 501
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 502
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 503
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->n:Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 504
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->n:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 506
    :cond_2
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/cz;->j:Ljava/lang/Object;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 507
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/da;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cz;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-direct {v0, p0, v3, v1}, Lcom/google/android/gms/wallet/common/ui/da;-><init>(Lcom/google/android/gms/wallet/common/ui/cz;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v2, v0, v9, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_2

    .line 512
    :sswitch_12
    sget v0, Lcom/google/android/gms/p;->AT:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 514
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->p:Ljava/util/ArrayList;

    const/4 v3, 0x0

    new-array v4, v10, [Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/wallet/common/ui/cz;->m:Lcom/google/checkout/inapp/proto/j;

    iget-object v5, v5, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v5, v5, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v5, v5, Lcom/google/t/a/b;->a:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Ljava/util/ArrayList;Lcom/google/checkout/inapp/proto/a/b;Ljava/util/Collection;)Ljava/util/ArrayList;

    move-result-object v0

    .line 516
    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 517
    sget v0, Lcom/google/android/gms/p;->AL:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 519
    sget v0, Lcom/google/android/gms/p;->Ch:I

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 534
    :goto_3
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_1

    .line 521
    :cond_3
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 522
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 523
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->m:Lcom/google/checkout/inapp/proto/j;

    iget-object v0, v0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 527
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->o:[Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->o:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    const/4 v3, 0x2

    if-lt v0, v3, :cond_4

    .line 528
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/cz;->k:Ljava/lang/Object;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 530
    :cond_4
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/cz;->j:Ljava/lang/Object;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 531
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/da;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cz;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-direct {v0, p0, v3, v1}, Lcom/google/android/gms/wallet/common/ui/da;-><init>(Lcom/google/android/gms/wallet/common/ui/cz;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v2, v0, v9, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto :goto_3

    .line 536
    :sswitch_13
    sget v0, Lcom/google/android/gms/p;->AR:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 538
    sget v0, Lcom/google/android/gms/p;->AK:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 540
    sget v0, Lcom/google/android/gms/p;->Ch:I

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 541
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    .line 543
    :sswitch_14
    sget v0, Lcom/google/android/gms/p;->AQ:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 545
    sget v0, Lcom/google/android/gms/p;->AJ:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 547
    sget v0, Lcom/google/android/gms/p;->Ch:I

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 548
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    .line 550
    :sswitch_15
    sget v0, Lcom/google/android/gms/p;->AV:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 552
    sget v0, Lcom/google/android/gms/p;->Cp:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 553
    sget v0, Lcom/google/android/gms/p;->Ch:I

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 554
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    .line 556
    :sswitch_16
    sget v0, Lcom/google/android/gms/p;->AS:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 558
    sget v0, Lcom/google/android/gms/p;->Aw:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 559
    sget v0, Lcom/google/android/gms/p;->Ch:I

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 560
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    .line 562
    :sswitch_17
    sget v0, Lcom/google/android/gms/p;->Bl:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 563
    sget v0, Lcom/google/android/gms/p;->zY:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 564
    sget v0, Lcom/google/android/gms/p;->Ch:I

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 565
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    .line 567
    :sswitch_18
    sget v0, Lcom/google/android/gms/p;->AP:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 568
    sget v0, Lcom/google/android/gms/p;->AI:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 569
    sget v0, Lcom/google/android/gms/p;->Ch:I

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 570
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    .line 572
    :sswitch_19
    sget v0, Lcom/google/android/gms/p;->AX:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 574
    iput v11, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    .line 602
    :goto_4
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    packed-switch v0, :pswitch_data_0

    .line 624
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown dialog mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 577
    :sswitch_1a
    sget v0, Lcom/google/android/gms/p;->AR:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 579
    iput v11, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    goto :goto_4

    .line 582
    :sswitch_1b
    sget v0, Lcom/google/android/gms/p;->AY:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 584
    iput v12, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    goto :goto_4

    .line 587
    :sswitch_1c
    sget v0, Lcom/google/android/gms/p;->AQ:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 589
    iput v12, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    goto :goto_4

    .line 592
    :sswitch_1d
    sget v0, Lcom/google/android/gms/p;->AZ:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 594
    sget v0, Lcom/google/android/gms/p;->AO:I

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 596
    sget v0, Lcom/google/android/gms/p;->Ch:I

    invoke-virtual {v2, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 597
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    .line 604
    :pswitch_0
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 605
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->o:[Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_5

    .line 606
    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/cz;->o:[Lcom/google/checkout/inapp/proto/j;

    array-length v5, v4

    move v0, v1

    :goto_5
    if-ge v0, v5, :cond_5

    aget-object v1, v4, v0

    .line 607
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 606
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 610
    :cond_5
    sget-object v0, Lcom/google/android/gms/wallet/common/ui/cz;->j:Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 611
    new-instance v0, Lcom/google/android/gms/wallet/common/ui/da;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cz;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, p0, v1, v3}, Lcom/google/android/gms/wallet/common/ui/da;-><init>(Lcom/google/android/gms/wallet/common/ui/cz;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v2, v0, v9, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 613
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    .line 615
    :pswitch_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 616
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cz;->p:Ljava/util/ArrayList;

    if-eqz v1, :cond_6

    .line 617
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cz;->p:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 619
    :cond_6
    sget-object v1, Lcom/google/android/gms/wallet/common/ui/cz;->j:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 620
    new-instance v1, Lcom/google/android/gms/wallet/common/ui/da;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cz;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-direct {v1, p0, v3, v0}, Lcom/google/android/gms/wallet/common/ui/da;-><init>(Lcom/google/android/gms/wallet/common/ui/cz;Landroid/content/Context;Ljava/util/List;)V

    invoke-virtual {v2, v1, v9, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 622
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto/16 :goto_1

    .line 470
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_8
        0x7 -> :sswitch_9
        0x8 -> :sswitch_a
        0x9 -> :sswitch_5
        0xa -> :sswitch_6
        0xb -> :sswitch_7
        0xc -> :sswitch_f
        0x65 -> :sswitch_b
        0x66 -> :sswitch_c
        0x67 -> :sswitch_d
        0x68 -> :sswitch_e
    .end sparse-switch

    .line 482
    :sswitch_data_1
    .sparse-switch
        0x1 -> :sswitch_10
        0x2 -> :sswitch_11
        0x3 -> :sswitch_12
        0x4 -> :sswitch_13
        0x5 -> :sswitch_14
        0x6 -> :sswitch_18
        0x9 -> :sswitch_15
        0xa -> :sswitch_16
        0xb -> :sswitch_17
        0xc -> :sswitch_1d
        0x65 -> :sswitch_19
        0x66 -> :sswitch_1b
        0x67 -> :sswitch_1a
        0x68 -> :sswitch_1c
    .end sparse-switch

    .line 602
    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 437
    invoke-super {p0, p1}, Lcom/google/android/gms/common/ui/a;->onAttach(Landroid/app/Activity;)V

    .line 439
    new-instance v0, Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/wallet/shared/common/b/a;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->w:Lcom/google/android/gms/wallet/shared/common/b/a;

    .line 440
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->w:Lcom/google/android/gms/wallet/shared/common/b/a;

    invoke-static {p1}, Lcom/google/android/gms/wallet/dynamite/image/a;->a(Landroid/content/Context;)Lcom/google/android/gms/wallet/dynamite/image/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/common/b/a;->a(Lcom/google/android/gms/wallet/dynamite/image/a;)V

    .line 441
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 768
    invoke-super {p0, p1}, Lcom/google/android/gms/common/ui/a;->onCancel(Landroid/content/DialogInterface;)V

    .line 770
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    if-eqz v0, :cond_0

    .line 771
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/db;->b()V

    .line 773
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x0

    .line 686
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    if-nez v0, :cond_1

    .line 764
    :cond_0
    :goto_0
    return-void

    .line 690
    :cond_1
    iget v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 703
    :pswitch_0
    if-ne p2, v1, :cond_0

    .line 704
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/db;->a()V

    goto :goto_0

    .line 698
    :pswitch_1
    if-ne p2, v1, :cond_0

    .line 699
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/db;->b()V

    goto :goto_0

    .line 708
    :pswitch_2
    if-ne p2, v1, :cond_3

    .line 709
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->n:Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 710
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cz;->n:Lcom/google/checkout/inapp/proto/a/b;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/db;->b(Lcom/google/checkout/inapp/proto/a/b;)V

    goto :goto_0

    .line 712
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/db;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto :goto_0

    .line 715
    :cond_3
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/common/ui/cz;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 716
    instance-of v1, v0, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_4

    .line 717
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/db;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto :goto_0

    .line 719
    :cond_4
    sget-object v1, Lcom/google/android/gms/wallet/common/ui/cz;->j:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 720
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/db;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto :goto_0

    .line 725
    :pswitch_3
    if-ne p2, v1, :cond_5

    .line 726
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cz;->m:Lcom/google/checkout/inapp/proto/j;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/db;->a(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;)V

    goto :goto_0

    .line 728
    :cond_5
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/common/ui/cz;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 729
    sget-object v1, Lcom/google/android/gms/wallet/common/ui/cz;->k:Ljava/lang/Object;

    if-ne v0, v1, :cond_6

    .line 730
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/db;->b()V

    goto :goto_0

    .line 731
    :cond_6
    instance-of v1, v0, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_7

    .line 732
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    iget-object v2, p0, Lcom/google/android/gms/wallet/common/ui/cz;->m:Lcom/google/checkout/inapp/proto/j;

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/wallet/common/ui/db;->a(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;)V

    goto :goto_0

    .line 734
    :cond_7
    sget-object v1, Lcom/google/android/gms/wallet/common/ui/cz;->j:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 735
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cz;->m:Lcom/google/checkout/inapp/proto/j;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/wallet/common/ui/db;->a(Lcom/google/checkout/inapp/proto/j;Lcom/google/checkout/inapp/proto/a/b;)V

    goto :goto_0

    .line 741
    :pswitch_4
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/common/ui/cz;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 742
    instance-of v1, v0, Lcom/google/checkout/inapp/proto/j;

    if-eqz v1, :cond_8

    .line 743
    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    .line 744
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/checkout/inapp/proto/j;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 745
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/db;->a(Lcom/google/checkout/inapp/proto/j;)V

    goto/16 :goto_0

    .line 747
    :cond_8
    sget-object v1, Lcom/google/android/gms/wallet/common/ui/cz;->j:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 748
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    invoke-interface {v0}, Lcom/google/android/gms/wallet/common/ui/db;->a()V

    goto/16 :goto_0

    .line 753
    :pswitch_5
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/common/ui/cz;->a(I)Ljava/lang/Object;

    move-result-object v0

    .line 754
    instance-of v1, v0, Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_9

    .line 755
    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    .line 756
    invoke-direct {p0, v0}, Lcom/google/android/gms/wallet/common/ui/cz;->a(Lcom/google/checkout/inapp/proto/a/b;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 757
    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/common/ui/db;->b(Lcom/google/checkout/inapp/proto/a/b;)V

    goto/16 :goto_0

    .line 759
    :cond_9
    sget-object v1, Lcom/google/android/gms/wallet/common/ui/cz;->j:Ljava/lang/Object;

    if-ne v0, v1, :cond_0

    .line 760
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->v:Lcom/google/android/gms/wallet/common/ui/db;

    invoke-interface {v0, v2}, Lcom/google/android/gms/wallet/common/ui/db;->a(Lcom/google/checkout/inapp/proto/a/b;)V

    goto/16 :goto_0

    .line 690
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 445
    invoke-super {p0, p1}, Lcom/google/android/gms/common/ui/a;->onCreate(Landroid/os/Bundle;)V

    .line 447
    invoke-virtual {p0}, Lcom/google/android/gms/wallet/common/ui/cz;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    .line 448
    const-string v0, "mode"

    const/4 v1, -0x1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->l:I

    .line 449
    const-string v0, "instrument"

    const-class v1, Lcom/google/checkout/inapp/proto/j;

    invoke-static {v5, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/j;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->m:Lcom/google/checkout/inapp/proto/j;

    .line 451
    const-string v0, "address"

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v5, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->n:Lcom/google/checkout/inapp/proto/a/b;

    .line 452
    const-string v0, "instruments"

    const-class v6, Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    if-nez v7, :cond_1

    move-object v0, v2

    :cond_0
    check-cast v0, [Lcom/google/checkout/inapp/proto/j;

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->o:[Lcom/google/checkout/inapp/proto/j;

    .line 454
    const-string v0, "existingAddresses"

    const-class v1, Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v5, v0, v1}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->b(Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/Class;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->p:Ljava/util/ArrayList;

    .line 456
    const-string v0, "requiresFullBillingAddress"

    const/4 v1, 0x1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->q:Z

    .line 457
    const-string v0, "phoneNumberRequired"

    invoke-virtual {v5, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->r:Z

    .line 458
    const-string v0, "disallowedPaymentInstrumentTypes"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->s:[I

    .line 460
    const-string v0, "allowedShippingCountryCodes"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->u:Ljava/util/ArrayList;

    .line 462
    const-string v0, "disallowedCardCategories"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getIntArray(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/common/ui/cz;->t:[I

    .line 463
    return-void

    .line 452
    :cond_1
    invoke-virtual {v7}, Landroid/os/Bundle;->size()I

    move-result v8

    invoke-static {v6, v8}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/protobuf/nano/j;

    move v3, v4

    :goto_0
    if-ge v3, v8, :cond_0

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-static {v1, v6}, Lcom/google/android/gms/wallet/shared/ProtoUtils;->a([BLjava/lang/Class;)Lcom/google/protobuf/nano/j;

    move-result-object v1

    :goto_1
    aput-object v1, v0, v3

    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_2
    move-object v1, v2

    goto :goto_1
.end method
