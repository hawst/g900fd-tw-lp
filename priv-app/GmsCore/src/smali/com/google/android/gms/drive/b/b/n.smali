.class public final Lcom/google/android/gms/drive/b/b/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/drive/auth/g;

.field final b:Ljava/lang/String;

.field final c:Z

.field final d:Lcom/google/android/gms/drive/DriveId;

.field final e:J

.field final f:Ljava/lang/String;

.field final g:Ljava/lang/String;

.field final h:Ljava/lang/String;

.field final i:Ljava/lang/String;

.field j:Ljava/lang/String;

.field final k:Ljava/lang/Integer;

.field private final l:Lcom/google/android/gms/drive/database/r;

.field private final m:Lcom/google/android/gms/drive/b/f;

.field private final n:J

.field private final o:Lcom/google/android/gms/drive/database/model/ax;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;ZLcom/google/android/gms/drive/DriveId;JLjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/b/f;JLcom/google/android/gms/drive/database/model/ax;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 67
    const-string v1, "Null metadata json provided in upload request."

    invoke-static {p1, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/drive/b/b/n;->b:Ljava/lang/String;

    .line 69
    iput-object p2, p0, Lcom/google/android/gms/drive/b/b/n;->a:Lcom/google/android/gms/drive/auth/g;

    .line 70
    iput-boolean p3, p0, Lcom/google/android/gms/drive/b/b/n;->c:Z

    .line 71
    iput-object p4, p0, Lcom/google/android/gms/drive/b/b/n;->d:Lcom/google/android/gms/drive/DriveId;

    .line 72
    iput-wide p5, p0, Lcom/google/android/gms/drive/b/b/n;->e:J

    .line 73
    iput-object p7, p0, Lcom/google/android/gms/drive/b/b/n;->f:Ljava/lang/String;

    .line 74
    iput-object p8, p0, Lcom/google/android/gms/drive/b/b/n;->j:Ljava/lang/String;

    .line 75
    iput-object p9, p0, Lcom/google/android/gms/drive/b/b/n;->l:Lcom/google/android/gms/drive/database/r;

    .line 76
    iput-object p10, p0, Lcom/google/android/gms/drive/b/b/n;->m:Lcom/google/android/gms/drive/b/f;

    .line 77
    iput-wide p11, p0, Lcom/google/android/gms/drive/b/b/n;->n:J

    .line 78
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/n;->o:Lcom/google/android/gms/drive/database/model/ax;

    .line 79
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/n;->g:Ljava/lang/String;

    .line 80
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/n;->h:Ljava/lang/String;

    .line 81
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/n;->i:Ljava/lang/String;

    .line 82
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/drive/b/b/n;->k:Ljava/lang/Integer;

    .line 83
    return-void
.end method

.method public static a(Lcom/google/android/gms/drive/database/model/ah;Lcom/google/android/gms/drive/g/aw;JLjava/lang/String;Ljava/lang/String;ZLcom/google/android/gms/drive/auth/g;Ljava/lang/Integer;)Lcom/google/android/gms/drive/b/b/n;
    .locals 22

    .prologue
    .line 213
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/drive/database/model/ah;->j()Ljava/lang/String;

    move-result-object v2

    .line 214
    if-eqz v2, :cond_0

    invoke-static {v2}, Lcom/google/android/gms/drive/DriveId;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v7

    .line 216
    :goto_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/drive/g/aw;->h()Lcom/google/android/gms/drive/database/r;

    move-result-object v12

    .line 217
    move-wide/from16 v0, p2

    invoke-interface {v12, v0, v1}, Lcom/google/android/gms/drive/database/r;->b(J)Lcom/google/android/gms/drive/database/model/bi;

    move-result-object v2

    .line 219
    iget-object v3, v2, Lcom/google/android/gms/drive/database/model/bi;->a:Ljava/lang/String;

    .line 220
    invoke-interface {v12, v3}, Lcom/google/android/gms/drive/database/r;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/database/model/ax;

    move-result-object v16

    .line 221
    move-object/from16 v0, v16

    iget-wide v8, v0, Lcom/google/android/gms/drive/database/model/ax;->h:J

    .line 223
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/drive/database/model/ah;->K()Ljava/lang/String;

    move-result-object v10

    .line 225
    new-instance v3, Lcom/google/android/gms/drive/b/b/n;

    iget-object v11, v2, Lcom/google/android/gms/drive/database/model/bi;->d:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/drive/g/aw;->q()Lcom/google/android/gms/drive/b/f;

    move-result-object v13

    iget-object v0, v2, Lcom/google/android/gms/drive/database/model/bi;->b:Ljava/lang/String;

    move-object/from16 v17, v0

    iget-object v0, v2, Lcom/google/android/gms/drive/database/model/bi;->c:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v4, p5

    move-object/from16 v5, p7

    move/from16 v6, p6

    move-wide/from16 v14, p2

    move-object/from16 v19, p4

    move-object/from16 v20, p8

    invoke-direct/range {v3 .. v20}, Lcom/google/android/gms/drive/b/b/n;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/auth/g;ZLcom/google/android/gms/drive/DriveId;JLjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/b/f;JLcom/google/android/gms/drive/database/model/ax;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v3

    .line 214
    :cond_0
    const/4 v7, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/n;->m:Lcom/google/android/gms/drive/b/f;

    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/n;->o:Lcom/google/android/gms/drive/database/model/ax;

    iget-object v1, v1, Lcom/google/android/gms/drive/database/model/ax;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/b/f;->b(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 195
    iput-object p1, p0, Lcom/google/android/gms/drive/b/b/n;->j:Ljava/lang/String;

    .line 196
    iget-object v0, p0, Lcom/google/android/gms/drive/b/b/n;->l:Lcom/google/android/gms/drive/database/r;

    iget-wide v2, p0, Lcom/google/android/gms/drive/b/b/n;->n:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/drive/database/r;->b(J)Lcom/google/android/gms/drive/database/model/bi;

    move-result-object v0

    .line 197
    iget-object v1, p0, Lcom/google/android/gms/drive/b/b/n;->j:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/drive/database/model/bi;->d:Ljava/lang/String;

    .line 198
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/bi;->i()V

    .line 199
    return-void
.end method
