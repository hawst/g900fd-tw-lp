.class public abstract Lcom/google/android/gms/auth/authzen/transaction/e;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/authzen/transaction/r;


# instance fields
.field protected c:Ljava/lang/String;

.field protected d:Lcom/google/ab/b/a/a/r;

.field protected e:Lcom/google/ab/b/a/a/s;

.field protected f:Lcom/google/android/gms/auth/authzen/keyservice/g;

.field protected g:J

.field protected h:J

.field protected i:Ljava/lang/String;

.field protected j:Landroid/content/BroadcastReceiver;

.field protected k:Z

.field protected l:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 70
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->l:Z

    return-void
.end method

.method private static a(Landroid/content/Intent;)Lcom/google/ab/b/a/a/r;
    .locals 3

    .prologue
    .line 279
    :try_start_0
    const-string v0, "tx_request"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/ab/b/a/a/r;->a([B)Lcom/google/ab/b/a/a/r;
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 280
    :catch_0
    move-exception v0

    .line 281
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to parse TxRequest"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 331
    invoke-static {p1}, Lcom/google/android/gms/auth/authzen/transaction/e;->a(Landroid/content/Intent;)Lcom/google/ab/b/a/a/r;

    move-result-object v0

    .line 332
    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/p;->a(Lcom/google/ab/b/a/a/r;)Ljava/lang/String;

    move-result-object v0

    .line 333
    invoke-static {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/p;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 336
    const/16 v0, 0x12b

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/auth/authzen/transaction/e;->a(Landroid/content/Context;Landroid/content/Intent;I)V

    .line 337
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/content/Intent;I)V
    .locals 6

    .prologue
    .line 352
    const-string v0, "account"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 353
    const-string v1, "encryption_key_handle"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v1

    .line 357
    :try_start_0
    const-string v2, "tx_request"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/ab/b/a/a/r;->a([B)Lcom/google/ab/b/a/a/r;
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 362
    new-instance v3, Lcom/google/ab/b/a/a/s;

    invoke-direct {v3}, Lcom/google/ab/b/a/a/s;-><init>()V

    invoke-virtual {v3, p2}, Lcom/google/ab/b/a/a/s;->a(I)Lcom/google/ab/b/a/a/s;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Lcom/google/ab/b/a/a/s;->b(J)Lcom/google/ab/b/a/a/s;

    move-result-object v3

    .line 365
    new-instance v4, Lcom/google/ab/b/a/a/p;

    invoke-direct {v4}, Lcom/google/ab/b/a/a/p;-><init>()V

    invoke-virtual {v4, v2}, Lcom/google/ab/b/a/a/p;->a(Lcom/google/ab/b/a/a/r;)Lcom/google/ab/b/a/a/p;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/google/ab/b/a/a/p;->a(Lcom/google/ab/b/a/a/s;)Lcom/google/ab/b/a/a/p;

    move-result-object v2

    .line 368
    new-instance v3, Lcom/google/ab/b/a/e/t;

    sget-object v4, Lcom/google/ab/b/a/e/u;->d:Lcom/google/ab/b/a/e/u;

    invoke-virtual {v2}, Lcom/google/ab/b/a/a/p;->g()[B

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lcom/google/ab/b/a/e/t;-><init>(Lcom/google/ab/b/a/e/u;[B)V

    .line 370
    invoke-static {v0, v1, v3}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->a(Ljava/lang/String;[BLcom/google/ab/b/a/e/t;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 371
    :goto_0
    return-void

    .line 358
    :catch_0
    move-exception v0

    .line 359
    const-string v1, "AuthZen"

    const-string v2, "Unable to parse TxRequest"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/authzen/transaction/e;)V
    .locals 3

    .prologue
    .line 35
    const-string v0, "AuthZen"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Closing existing activity with notification tag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-boolean v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->k:Z

    if-nez v0, :cond_0

    const/16 v0, 0xc7

    const/16 v1, 0x18f

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/authzen/transaction/e;->a(II)V

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->b()V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->finish()V

    return-void
.end method

.method private b(I)V
    .locals 6

    .prologue
    .line 258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->k:Z

    .line 261
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "encryption_key_handle"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    .line 262
    if-eqz v0, :cond_0

    .line 263
    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->e:Lcom/google/ab/b/a/a/s;

    invoke-virtual {v2, p1}, Lcom/google/ab/b/a/a/s;->a(I)Lcom/google/ab/b/a/a/s;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/ab/b/a/a/s;->b(J)Lcom/google/ab/b/a/a/s;

    new-instance v2, Lcom/google/ab/b/a/a/p;

    invoke-direct {v2}, Lcom/google/ab/b/a/a/p;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->d:Lcom/google/ab/b/a/a/r;

    invoke-virtual {v2, v3}, Lcom/google/ab/b/a/a/p;->a(Lcom/google/ab/b/a/a/r;)Lcom/google/ab/b/a/a/p;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->e:Lcom/google/ab/b/a/a/s;

    invoke-virtual {v2, v3}, Lcom/google/ab/b/a/a/p;->a(Lcom/google/ab/b/a/a/s;)Lcom/google/ab/b/a/a/p;

    move-result-object v2

    new-instance v3, Lcom/google/ab/b/a/e/t;

    sget-object v4, Lcom/google/ab/b/a/e/u;->d:Lcom/google/ab/b/a/e/u;

    invoke-virtual {v2}, Lcom/google/ab/b/a/a/p;->g()[B

    move-result-object v2

    invoke-direct {v3, v4, v2}, Lcom/google/ab/b/a/e/t;-><init>(Lcom/google/ab/b/a/e/u;[B)V

    invoke-static {v1, v0, v3}, Lcom/google/android/gms/auth/authzen/transaction/TransactionReplyService;->a(Ljava/lang/String;[BLcom/google/ab/b/a/e/t;)Landroid/content/Intent;

    move-result-object v0

    .line 265
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/e;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 267
    :cond_0
    return-void
.end method

.method public static b(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 341
    invoke-static {p0, p1}, Lcom/google/android/gms/auth/authzen/transaction/p;->b(Landroid/content/Context;Landroid/content/Intent;)V

    .line 348
    const/16 v0, 0xc7

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/auth/authzen/transaction/e;->a(Landroid/content/Context;Landroid/content/Intent;I)V

    .line 349
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 208
    const-string v0, "AuthZen"

    const-string v1, "Prompt canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 211
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->k:Z

    .line 213
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/e;->setResult(I)V

    .line 214
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->finish()V

    .line 215
    return-void
.end method

.method private static e()Lcom/google/ab/b/a/a/s;
    .locals 4

    .prologue
    .line 307
    new-instance v0, Lcom/google/ab/b/a/a/s;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/s;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/ab/b/a/a/s;->a(J)Lcom/google/ab/b/a/a/s;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 132
    return-void
.end method

.method protected final a(II)V
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x1

    invoke-virtual {p0, p2, v0}, Lcom/google/android/gms/auth/authzen/transaction/e;->a(IZ)V

    .line 274
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/authzen/transaction/e;->b(I)V

    .line 275
    return-void
.end method

.method protected final a(IZ)V
    .locals 4

    .prologue
    .line 299
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->g:J

    sub-long/2addr v0, v2

    .line 300
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->e:Lcom/google/ab/b/a/a/s;

    new-instance v3, Lcom/google/ab/b/a/a/u;

    invoke-direct {v3}, Lcom/google/ab/b/a/a/u;-><init>()V

    invoke-virtual {v3, v0, v1}, Lcom/google/ab/b/a/a/u;->a(J)Lcom/google/ab/b/a/a/u;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/ab/b/a/a/u;->a(I)Lcom/google/ab/b/a/a/u;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/ab/b/a/a/u;->a(Z)Lcom/google/ab/b/a/a/u;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/ab/b/a/a/s;->a(Lcom/google/ab/b/a/a/u;)Lcom/google/ab/b/a/a/s;

    .line 304
    return-void
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 137
    return-void
.end method

.method protected abstract b(Landroid/os/Bundle;)V
.end method

.method protected final d()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 150
    iget-boolean v1, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->k:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "cancel_prompt"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v6, 0x0

    .line 74
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 76
    const-string v1, "cancel_prompt"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 77
    invoke-direct {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->c()V

    .line 92
    :goto_0
    return-void

    .line 80
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->f:Lcom/google/android/gms/auth/authzen/keyservice/g;

    if-nez v1, :cond_1

    .line 81
    new-instance v1, Lcom/google/android/gms/auth/authzen/keyservice/g;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/authzen/keyservice/g;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->f:Lcom/google/android/gms/auth/authzen/keyservice/g;

    .line 83
    :cond_1
    const-string v1, "account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->c:Ljava/lang/String;

    .line 84
    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/e;->a(Landroid/content/Intent;)Lcom/google/ab/b/a/a/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->d:Lcom/google/ab/b/a/a/r;

    .line 85
    if-nez p1, :cond_3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->h:J

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "creation_elapsed_time"

    iget-wide v4, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->h:J

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->g:J

    iput-boolean v6, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->k:Z

    invoke-static {}, Lcom/google/android/gms/auth/authzen/transaction/e;->e()Lcom/google/ab/b/a/a/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->e:Lcom/google/ab/b/a/a/s;

    .line 86
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->l:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->d:Lcom/google/ab/b/a/a/r;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->getIntent()Landroid/content/Intent;

    move-result-object v4

    const-string v1, "creation_elapsed_time"

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    invoke-virtual {v4, v1, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/p;->b(Lcom/google/ab/b/a/a/r;)J

    move-result-wide v0

    const-string v5, "AuthZen"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Prompt lifetime in millis: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    cmp-long v5, v0, v2

    if-gez v5, :cond_4

    :goto_2
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-string v1, "cancel_prompt"

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    invoke-static {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/p;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    const/4 v4, 0x2

    add-long/2addr v2, v6

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/p;->b(Landroid/content/Context;Landroid/content/Intent;)V

    .line 87
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->d:Lcom/google/ab/b/a/a/r;

    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/p;->a(Lcom/google/ab/b/a/a/r;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->i:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/auth/authzen/transaction/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/authzen/transaction/f;-><init>(Lcom/google/android/gms/auth/authzen/transaction/e;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->j:Landroid/content/BroadcastReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "AUTHZEN_CLOSE_ACTIVITY"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/authzen/transaction/e;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 91
    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/authzen/transaction/e;->b(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 85
    :cond_3
    const-string v0, "alarm_set"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->l:Z

    const-string v0, "activity_start_elapsed_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->h:J

    const-string v0, "creation_elapsed_time"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->g:J

    const-string v0, "reply_sent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->k:Z

    :try_start_0
    const-string v0, "tx_response"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    new-instance v1, Lcom/google/ab/b/a/a/s;

    invoke-direct {v1}, Lcom/google/ab/b/a/a/s;-><init>()V

    array-length v4, v0

    invoke-virtual {v1, v0, v4}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v0

    check-cast v0, Lcom/google/ab/b/a/a/s;

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->e:Lcom/google/ab/b/a/a/s;
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_1

    :catch_0
    move-exception v0

    const-string v1, "AuthZen"

    const-string v4, "Failed to parse TxResponse"

    invoke-static {v1, v4, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-static {}, Lcom/google/android/gms/auth/authzen/transaction/e;->e()Lcom/google/ab/b/a/a/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->e:Lcom/google/ab/b/a/a/s;

    goto/16 :goto_1

    :cond_4
    move-wide v2, v0

    goto/16 :goto_2
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 117
    invoke-super {p0}, Landroid/support/v4/app/q;->onDestroy()V

    .line 118
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 119
    const/16 v0, 0x12b

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/e;->b(I)V

    .line 120
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->a()V

    .line 123
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/p;->a(Landroid/content/Context;Landroid/content/Intent;)Landroid/app/PendingIntent;

    move-result-object v1

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->j:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/e;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 127
    :cond_1
    return-void
.end method

.method public onNewIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 102
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onNewIntent(Landroid/content/Intent;)V

    .line 103
    const-string v0, "cancel_prompt"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    invoke-direct {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->c()V

    .line 113
    :goto_0
    return-void

    .line 110
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/e;->finish()V

    .line 111
    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/authzen/transaction/e;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 155
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 156
    const-string v0, "activity_start_elapsed_time"

    iget-wide v2, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->h:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 157
    const-string v0, "creation_elapsed_time"

    iget-wide v2, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->g:J

    invoke-virtual {p1, v0, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 158
    const-string v0, "alarm_set"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->l:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 159
    const-string v0, "reply_sent"

    iget-boolean v1, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 160
    const-string v0, "tx_response"

    iget-object v1, p0, Lcom/google/android/gms/auth/authzen/transaction/e;->e:Lcom/google/ab/b/a/a/s;

    invoke-virtual {v1}, Lcom/google/ab/b/a/a/s;->g()[B

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 161
    return-void
.end method
