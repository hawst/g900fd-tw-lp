.class public final Lcom/google/android/gms/analytics/bd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public final b:J

.field public final c:J

.field public final d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>(JJLjava/lang/String;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    const-string v0, "https:"

    iput-object v0, p0, Lcom/google/android/gms/analytics/bd;->f:Ljava/lang/String;

    .line 44
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/analytics/bd;->a:Ljava/lang/String;

    .line 45
    iput-wide p1, p0, Lcom/google/android/gms/analytics/bd;->b:J

    .line 46
    iput-wide p3, p0, Lcom/google/android/gms/analytics/bd;->c:J

    .line 47
    iput-object p5, p0, Lcom/google/android/gms/analytics/bd;->d:Ljava/lang/String;

    .line 48
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 74
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    iput-object p1, p0, Lcom/google/android/gms/analytics/bd;->e:Ljava/lang/String;

    .line 78
    invoke-virtual {p1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    const-string v1, "http:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const-string v0, "http:"

    iput-object v0, p0, Lcom/google/android/gms/analytics/bd;->f:Ljava/lang/String;

    goto :goto_0
.end method
