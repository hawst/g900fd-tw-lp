.class public final Lcom/google/android/gms/cast/c/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/cast/e/h;

.field private static final b:[Ljava/lang/String;

.field private static c:I


# instance fields
.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/cast/c/a;

.field private final f:Ljava/lang/String;

.field private final g:Lcom/google/android/gms/cast/c/m;

.field private final h:Ljava/util/ArrayList;

.field private final i:Ljava/util/LinkedList;

.field private final j:Landroid/os/Handler;

.field private k:Ljava/util/Set;

.field private l:Ljava/lang/String;

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 41
    new-instance v0, Lcom/google/android/gms/cast/e/h;

    const-string v1, "DeviceFilter"

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast/e/h;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/cast/c/h;->a:Lcom/google/android/gms/cast/e/h;

    .line 51
    sget-object v0, Lcom/google/android/gms/cast/a/a;->j:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "\\s+"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/c/h;->b:[Ljava/lang/String;

    .line 62
    const/4 v0, 0x0

    sput v0, Lcom/google/android/gms/cast/c/h;->c:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/cast/c/m;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    const/4 v0, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/gms/cast/c/h;->j:Landroid/os/Handler;

    .line 75
    iput-object p1, p0, Lcom/google/android/gms/cast/c/h;->d:Landroid/content/Context;

    .line 76
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/cast/c/h;->k:Ljava/util/Set;

    .line 77
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/gms/cast/c/h;->l:Ljava/lang/String;

    .line 78
    iput-boolean v0, p0, Lcom/google/android/gms/cast/c/h;->m:Z

    .line 79
    iput-object p2, p0, Lcom/google/android/gms/cast/c/h;->f:Ljava/lang/String;

    .line 80
    iput-object p3, p0, Lcom/google/android/gms/cast/c/h;->g:Lcom/google/android/gms/cast/c/m;

    .line 82
    new-instance v1, Lcom/google/android/gms/cast/c/a;

    iget-object v2, p0, Lcom/google/android/gms/cast/c/h;->d:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/gms/cast/c/a;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/cast/c/h;->e:Lcom/google/android/gms/cast/c/a;

    .line 85
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/cast/c/h;->h:Ljava/util/ArrayList;

    .line 86
    :goto_0
    if-ge v0, v3, :cond_0

    .line 87
    iget-object v1, p0, Lcom/google/android/gms/cast/c/h;->h:Ljava/util/ArrayList;

    new-instance v2, Lcom/google/android/gms/cast/c/n;

    invoke-direct {v2, p0}, Lcom/google/android/gms/cast/c/n;-><init>(Lcom/google/android/gms/cast/c/h;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 86
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 90
    :cond_0
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/h;->i:Ljava/util/LinkedList;

    .line 91
    return-void
.end method

.method static synthetic a()I
    .locals 1

    .prologue
    .line 40
    sget v0, Lcom/google/android/gms/cast/c/h;->c:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/gms/cast/c/h;->c:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/c/h;)Lcom/google/android/gms/cast/c/a;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->e:Lcom/google/android/gms/cast/c/a;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/cast/c/h;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->j:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic b()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/gms/cast/c/h;->b:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/cast/c/h;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c()Lcom/google/android/gms/cast/e/h;
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/gms/cast/c/h;->a:Lcom/google/android/gms/cast/e/h;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/cast/c/h;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/cast/c/h;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/cast/c/h;)Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/gms/cast/c/h;->m:Z

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/cast/c/h;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->k:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/cast/c/h;)Lcom/google/android/gms/cast/c/m;
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->g:Lcom/google/android/gms/cast/c/m;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/cast/c/h;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/CastDevice;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast/c/h;->a(Lcom/google/android/gms/cast/CastDevice;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/cast/CastDevice;)V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/n;

    .line 131
    invoke-virtual {v0, p1}, Lcom/google/android/gms/cast/c/n;->a(Lcom/google/android/gms/cast/CastDevice;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    :cond_1
    :goto_0
    return-void

    .line 136
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->i:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->i:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Ljava/util/Set;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/16 v8, 0x22

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->i:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/n;

    .line 100
    invoke-virtual {v0, v4}, Lcom/google/android/gms/cast/c/n;->a(Z)Lcom/google/android/gms/cast/CastDevice;

    goto :goto_0

    .line 102
    :cond_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0, p1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/c/h;->k:Ljava/util/Set;

    .line 103
    iput-boolean v5, p0, Lcom/google/android/gms/cast/c/h;->m:Z

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/cast/c/h;->k:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move-object v1, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/c/aa;

    .line 107
    iget-boolean v7, p0, Lcom/google/android/gms/cast/c/h;->m:Z

    iget-object v3, v0, Lcom/google/android/gms/cast/c/aa;->c:Ljava/util/Set;

    invoke-interface {v3}, Ljava/util/Set;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    move v3, v4

    :goto_2
    or-int/2addr v3, v7

    iput-boolean v3, p0, Lcom/google/android/gms/cast/c/h;->m:Z

    .line 109
    iget-object v3, v0, Lcom/google/android/gms/cast/c/aa;->b:Ljava/lang/String;

    .line 110
    if-eqz v3, :cond_6

    .line 111
    if-nez v1, :cond_5

    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 114
    :goto_3
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 115
    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 117
    :cond_1
    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :goto_4
    move-object v1, v0

    .line 119
    goto :goto_1

    :cond_2
    move v3, v5

    .line 107
    goto :goto_2

    .line 120
    :cond_3
    if-nez v1, :cond_4

    :goto_5
    iput-object v2, p0, Lcom/google/android/gms/cast/c/h;->l:Ljava/lang/String;

    .line 121
    return-void

    .line 120
    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_5

    :cond_5
    move-object v0, v1

    goto :goto_3

    :cond_6
    move-object v0, v1

    goto :goto_4
.end method
