.class public final Lcom/google/android/gms/plus/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentHashMap;

.field private static g:Ljava/lang/String;

.field private static h:Ljava/lang/String;

.field private static i:Ljava/lang/String;

.field private static j:Ljava/lang/String;

.field private static k:Ljava/lang/String;


# instance fields
.field private final b:Lcom/google/android/gms/common/server/y;

.field private final c:Lcom/google/android/gms/common/util/p;

.field private final d:Landroid/content/res/Configuration;

.field private final e:Landroid/content/pm/PackageManager;

.field private final f:Lcom/google/android/gms/plus/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/a/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    .line 150
    const-string v0, "0"

    sput-object v0, Lcom/google/android/gms/plus/a/c;->g:Ljava/lang/String;

    .line 151
    const-string v0, "1"

    sput-object v0, Lcom/google/android/gms/plus/a/c;->h:Ljava/lang/String;

    .line 152
    const-string v0, "2"

    sput-object v0, Lcom/google/android/gms/plus/a/c;->i:Ljava/lang/String;

    .line 153
    const-string v0, "3"

    sput-object v0, Lcom/google/android/gms/plus/a/c;->j:Ljava/lang/String;

    .line 154
    const-string v0, "4"

    sput-object v0, Lcom/google/android/gms/plus/a/c;->k:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Lcom/google/android/gms/common/server/y;

    invoke-direct {v0, p2}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Intent;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/a/c;->b:Lcom/google/android/gms/common/server/y;

    .line 75
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/a/c;->c:Lcom/google/android/gms/common/util/p;

    .line 76
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/a/c;->d:Landroid/content/res/Configuration;

    .line 77
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/a/c;->e:Landroid/content/pm/PackageManager;

    .line 78
    new-instance v0, Lcom/google/android/gms/plus/a/e;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/plus/a/e;-><init>(B)V

    iput-object v0, p0, Lcom/google/android/gms/plus/a/c;->f:Lcom/google/android/gms/plus/a/d;

    .line 79
    return-void
.end method

.method private static a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/dk;
    .locals 4

    .prologue
    .line 142
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/dl;

    invoke-direct {v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/dl;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;->b:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/dl;->a:Ljava/lang/String;

    iget-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/dl;->c:Ljava/util/Set;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget v0, p0, Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;->c:I

    iput v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/dl;->b:I

    iget-object v0, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/dl;->c:Ljava/util/Set;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iget-object v2, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/dl;->c:Ljava/util/Set;

    iget-object v3, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/dl;->a:Ljava/lang/String;

    iget v1, v1, Lcom/google/android/gms/plus/service/v1whitelisted/models/dl;->b:I

    invoke-direct {v0, v2, v3, v1}, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;-><init>(Ljava/util/Set;Ljava/lang/String;I)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 22

    .prologue
    .line 38
    check-cast p1, Lcom/google/android/gms/plus/service/DefaultIntentService;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->b:Lcom/google/android/gms/common/server/y;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/y;->a()Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->b:Lcom/google/android/gms/common/server/y;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/y;->d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->b:Lcom/google/android/gms/common/server/y;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/y;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v17

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->b:Lcom/google/android/gms/common/server/y;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/y;->c()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->b:Lcom/google/android/gms/common/server/y;

    iget-object v2, v2, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v3, "duration"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->b:Lcom/google/android/gms/common/server/y;

    iget-object v2, v2, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v3, "clientActionData"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    move-object v12, v2

    check-cast v12, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->b:Lcom/google/android/gms/common/server/y;

    iget-object v2, v2, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v3, "callingPackage"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/plus/a/c;->f:Lcom/google/android/gms/plus/a/d;

    if-nez v20, :cond_7

    const/4 v2, 0x0

    move-object v14, v2

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->b:Lcom/google/android/gms/common/server/y;

    iget-object v2, v2, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v3, "actionTarget"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    move-object v13, v2

    check-cast v13, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->b:Lcom/google/android/gms/common/server/y;

    iget-object v2, v2, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v3, "plusPageId"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    new-instance v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;

    invoke-direct {v11}, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;-><init>()V

    sget-object v2, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    iput-object v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->d:Ljava/lang/String;

    iget-object v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->i:Ljava/util/Set;

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v2, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    iput-object v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->e:Ljava/lang/String;

    iget-object v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->i:Ljava/util/Set;

    const/4 v3, 0x6

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->d:Landroid/content/res/Configuration;

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    packed-switch v2, :pswitch_data_0

    sget-object v2, Lcom/google/android/gms/plus/a/c;->g:Ljava/lang/String;

    :goto_1
    iput-object v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->f:Ljava/lang/String;

    iget-object v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->i:Ljava/util/Set;

    const/4 v3, 0x7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xc

    if-le v2, v3, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->d:Landroid/content/res/Configuration;

    iget v2, v2, Landroid/content/res/Configuration;->screenHeightDp:I

    iput v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->g:I

    iget-object v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->i:Ljava/util/Set;

    const/16 v3, 0x8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->d:Landroid/content/res/Configuration;

    iget v2, v2, Landroid/content/res/Configuration;->screenWidthDp:I

    iput v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->h:I

    iget-object v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->i:Ljava/util/Set;

    const/16 v3, 0x9

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->e:Landroid/content/pm/PackageManager;

    const-string v3, "android.hardware.screen.landscape"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->b:Z

    iget-object v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->i:Ljava/util/Set;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->e:Landroid/content/pm/PackageManager;

    const-string v3, "android.hardware.screen.portrait"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v2

    iput-boolean v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->c:Z

    iget-object v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->i:Ljava/util/Set;

    const/4 v3, 0x4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_0
    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/plus/a/c;->d:Landroid/content/res/Configuration;

    iget v2, v2, Landroid/content/res/Configuration;->densityDpi:I

    iput v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->a:I

    iget-object v2, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->i:Ljava/util/Set;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_1
    new-instance v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/OzDeviceInfoEntity;

    iget-object v3, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->i:Ljava/util/Set;

    iget v4, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->a:I

    iget-boolean v5, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->b:Z

    iget-boolean v6, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->c:Z

    iget-object v7, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->d:Ljava/lang/String;

    iget-object v8, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->e:Ljava/lang/String;

    iget-object v9, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->f:Ljava/lang/String;

    iget v10, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->g:I

    iget v11, v11, Lcom/google/android/gms/plus/service/v1whitelisted/models/eg;->h:I

    invoke-direct/range {v2 .. v11}, Lcom/google/android/gms/plus/service/v1whitelisted/models/OzDeviceInfoEntity;-><init>(Ljava/util/Set;IZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/plus/a/c;->c:Lcom/google/android/gms/common/util/p;

    invoke-static/range {v16 .. v16}, Lcom/google/android/gms/plus/a/c;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/dk;

    move-result-object v3

    invoke-static/range {v17 .. v17}, Lcom/google/android/gms/plus/a/c;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/dk;

    move-result-object v4

    invoke-static/range {v18 .. v18}, Lcom/google/android/gms/plus/a/c;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/plus/service/v1whitelisted/models/dk;

    move-result-object v5

    new-instance v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;

    invoke-direct {v8}, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;-><init>()V

    move/from16 v0, v19

    iput v0, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;->d:I

    iget-object v6, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;->e:Ljava/util/Set;

    const/16 v7, 0x18

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_2

    check-cast v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iput-object v3, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iget-object v3, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;->e:Ljava/util/Set;

    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v3, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_2
    move-object v3, v4

    check-cast v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iput-object v3, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iget-object v3, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;->e:Ljava/util/Set;

    const/16 v4, 0x14

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    if-eqz v5, :cond_3

    move-object v3, v5

    check-cast v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iput-object v3, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iget-object v3, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;->e:Ljava/util/Set;

    const/4 v4, 0x4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_3
    new-instance v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;

    iget-object v4, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;->e:Ljava/util/Set;

    iget-object v5, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iget-object v6, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iget-object v7, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;

    iget v8, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/di;->d:I

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsNamespacedTypeEntity;I)V

    new-instance v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;

    invoke-direct {v9}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;-><init>()V

    check-cast v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;

    iput-object v3, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;

    iget-object v3, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->g:Ljava/util/Set;

    const/4 v4, 0x7

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const/4 v3, 0x1

    iput-boolean v3, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->d:Z

    iget-object v3, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->g:Ljava/util/Set;

    const/16 v4, 0xa

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, v20

    iput-object v0, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->e:Ljava/lang/String;

    iget-object v3, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->g:Ljava/util/Set;

    const/16 v4, 0xe

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iput-object v14, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->f:Ljava/lang/String;

    iget-object v3, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->g:Ljava/util/Set;

    const/16 v4, 0xf

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    check-cast v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/OzDeviceInfoEntity;

    iput-object v2, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/OzDeviceInfoEntity;

    iget-object v2, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->g:Ljava/util/Set;

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    if-eqz v13, :cond_4

    check-cast v13, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    iput-object v13, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    iget-object v2, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->g:Ljava/util/Set;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_4
    new-instance v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/OzEventEntity;

    iget-object v3, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->g:Ljava/util/Set;

    iget-object v4, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    iget-object v5, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->b:Lcom/google/android/gms/plus/service/v1whitelisted/models/OzDeviceInfoEntity;

    iget-object v6, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;

    iget-boolean v7, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->d:Z

    iget-object v8, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->e:Ljava/lang/String;

    iget-object v9, v9, Lcom/google/android/gms/plus/service/v1whitelisted/models/ej;->f:Ljava/lang/String;

    invoke-direct/range {v2 .. v9}, Lcom/google/android/gms/plus/service/v1whitelisted/models/OzEventEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/OzDeviceInfoEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/FavaDiagnosticsEntity;ZLjava/lang/String;Ljava/lang/String;)V

    new-instance v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/cm;

    invoke-direct {v8}, Lcom/google/android/gms/plus/service/v1whitelisted/models/cm;-><init>()V

    check-cast v2, Lcom/google/android/gms/plus/service/v1whitelisted/models/OzEventEntity;

    iput-object v2, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/cm;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/OzEventEntity;

    iget-object v2, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/cm;->d:Ljava/util/Set;

    const/4 v3, 0x5

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    invoke-interface {v10}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    iput-wide v2, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/cm;->b:J

    iget-object v2, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/cm;->d:Ljava/util/Set;

    const/4 v3, 0x3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    if-eqz v12, :cond_5

    check-cast v12, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    iput-object v12, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/cm;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    iget-object v2, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/cm;->d:Ljava/util/Set;

    const/4 v3, 0x2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_5
    new-instance v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzEventEntity;

    iget-object v4, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/cm;->d:Ljava/util/Set;

    iget-object v5, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/cm;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    iget-wide v6, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/cm;->b:J

    iget-object v8, v8, Lcom/google/android/gms/plus/service/v1whitelisted/models/cm;->c:Lcom/google/android/gms/plus/service/v1whitelisted/models/OzEventEntity;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzEventEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;JLcom/google/android/gms/plus/service/v1whitelisted/models/OzEventEntity;)V

    check-cast v3, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzEventEntity;

    const-string v2, "PlusAnalyticsOperation"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    const-string v2, "PlusAnalyticsOperation"

    const-string v4, "Processing task %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "accountName"

    invoke-virtual {v2, v4, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "type"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v4, "payload"

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzEventEntity;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "onBehalfOf"

    move-object/from16 v0, v21

    invoke-virtual {v2, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "timestamp"

    invoke-virtual {v3}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientOzEventEntity;->d()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v4, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/plus/service/DefaultIntentService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/plus/internal/v;->a:Landroid/net/Uri;

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    return-void

    :cond_7
    sget-object v2, Lcom/google/android/gms/plus/a/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, v20

    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-nez v2, :cond_8

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-interface {v3, v0, v1}, Lcom/google/android/gms/plus/a/d;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    sget-object v3, Lcom/google/android/gms/plus/a/c;->a:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, v20

    invoke-virtual {v3, v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_8
    move-object v14, v2

    goto/16 :goto_0

    :pswitch_0
    sget-object v2, Lcom/google/android/gms/plus/a/c;->h:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_1
    sget-object v2, Lcom/google/android/gms/plus/a/c;->i:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_2
    sget-object v2, Lcom/google/android/gms/plus/a/c;->j:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_3
    sget-object v2, Lcom/google/android/gms/plus/a/c;->k:Ljava/lang/String;

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
