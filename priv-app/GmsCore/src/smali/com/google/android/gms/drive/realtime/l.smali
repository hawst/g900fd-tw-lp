.class public final Lcom/google/android/gms/drive/realtime/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/a/c/a/e/f;


# instance fields
.field private final b:Ljava/util/concurrent/ExecutorService;

.field private volatile c:Landroid/os/Handler;

.field private final d:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 263
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 259
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/l;->b:Ljava/util/concurrent/ExecutorService;

    .line 261
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/l;->d:Ljava/util/concurrent/CountDownLatch;

    .line 264
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/l;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/android/gms/drive/realtime/m;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/realtime/m;-><init>(Lcom/google/android/gms/drive/realtime/l;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 272
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/realtime/l;Landroid/os/Handler;)Landroid/os/Handler;
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/l;->c:Landroid/os/Handler;

    return-object p1
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/l;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 277
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/l;->c:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->getThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    invoke-interface {p1}, Ljava/lang/Runnable;->run()V

    .line 297
    :goto_0
    return-void

    .line 280
    :cond_0
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 281
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/l;->c:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/drive/realtime/n;

    invoke-direct {v2, p0, p1, v0}, Lcom/google/android/gms/drive/realtime/n;-><init>(Lcom/google/android/gms/drive/realtime/l;Ljava/lang/Runnable;Ljava/util/concurrent/CountDownLatch;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 294
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 295
    :catch_0
    move-exception v0

    .line 296
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
