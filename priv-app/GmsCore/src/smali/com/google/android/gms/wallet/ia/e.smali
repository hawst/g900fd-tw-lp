.class final Lcom/google/android/gms/wallet/ia/e;
.super Lcom/google/android/gms/wallet/service/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V
    .locals 0

    .prologue
    .line 661
    iput-object p1, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-direct {p0}, Lcom/google/android/gms/wallet/service/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 775
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->s(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    .line 776
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/t;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 666
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iput-object p1, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/checkout/inapp/proto/t;

    .line 667
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {p1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Lcom/google/checkout/inapp/proto/t;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 668
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0, p1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Lcom/google/checkout/inapp/proto/t;)V

    .line 669
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Z)V

    .line 671
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h:Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;

    iget-object v1, p1, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v2, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v2}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/checkout/inapp/proto/a/d;

    move-result-object v2

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/wallet/ia/PaymentAmountInputView;->a(Lcom/google/checkout/inapp/proto/a/d;Lcom/google/checkout/inapp/proto/a/d;Lcom/google/checkout/inapp/proto/a/d;)V

    .line 676
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-boolean v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    iget-object v0, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_0

    .line 677
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0, v5}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Z)V

    .line 680
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->c(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 682
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->c(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v1

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, "create_to_ui_populated"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 684
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->b(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 686
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->d(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/i;

    .line 687
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->e(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/h;

    .line 689
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/checkout/inapp/proto/v;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 693
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0, v4}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;Z)V

    .line 694
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-boolean v4, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 696
    iget v0, p1, Lcom/google/checkout/inapp/proto/v;->a:I

    packed-switch v0, :pswitch_data_0

    .line 735
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "make_payment_failure"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 741
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    .line 744
    :goto_0
    return-void

    .line 698
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    iput-object v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->b:Lcom/google/checkout/inapp/proto/j;

    .line 699
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->e:Lcom/google/android/gms/wallet/common/ui/cc;

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/common/ui/cc;->a(Lcom/google/checkout/inapp/proto/j;)V

    .line 700
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iput-object v1, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/checkout/inapp/proto/t;

    .line 702
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->f(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    .line 705
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    sget v2, Lcom/google/android/gms/p;->Ay:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 711
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "make_payment_success"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 716
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->h(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/h;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "click_to_activity_result"

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 720
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v1}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->g(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/i;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 722
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->i(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/i;

    .line 723
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->j(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Lcom/google/android/apps/common/a/a/h;

    .line 726
    :cond_0
    iget v0, p1, Lcom/google/checkout/inapp/proto/v;->a:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 727
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    goto/16 :goto_0

    .line 729
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->l(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    goto/16 :goto_0

    .line 696
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 770
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->r(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    .line 771
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 759
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->m:Lcom/google/android/gms/wallet/common/PaymentModel;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/wallet/common/PaymentModel;->e:Z

    .line 760
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->o(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)Z

    .line 761
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v0, v0, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q:Lcom/google/checkout/inapp/proto/t;

    if-eqz v0, :cond_0

    .line 762
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->p(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    .line 766
    :goto_0
    return-void

    .line 764
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->q(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    goto :goto_0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 750
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-static {v0, v1}, Lcom/google/android/gms/wallet/common/b;->a(Landroid/content/Context;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Lcom/google/android/gms/wallet/common/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    iget-object v1, v1, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->k:Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->f()Ljava/lang/String;

    move-result-object v1

    const-string v2, "make_payment_failure"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/wallet/common/a;->a(Lcom/google/android/gms/wallet/common/b;Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/e;->a:Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;

    invoke-static {v0}, Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;->n(Lcom/google/android/gms/wallet/ia/BillingMakePaymentActivity;)V

    .line 755
    return-void
.end method
