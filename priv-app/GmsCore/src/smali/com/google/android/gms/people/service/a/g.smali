.class abstract Lcom/google/android/gms/people/service/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/service/a/q;


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/service/a/f;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/service/a/f;)V
    .locals 0

    .prologue
    .line 458
    iput-object p1, p0, Lcom/google/android/gms/people/service/a/g;->a:Lcom/google/android/gms/people/service/a/f;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/people/service/a/f;B)V
    .locals 0

    .prologue
    .line 458
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/service/a/g;-><init>(Lcom/google/android/gms/people/service/a/f;)V

    return-void
.end method


# virtual methods
.method abstract a(Landroid/os/Bundle;Lcom/google/android/gms/people/c/e;Ljava/lang/String;)V
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/people/e/a;Landroid/os/Bundle;)Z
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 469
    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    .line 470
    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->d()Lcom/google/android/gms/people/c/e;

    move-result-object v2

    .line 472
    invoke-static {p1}, Lcom/google/android/gms/people/c/f;->a(Landroid/content/Context;)Lcom/google/android/gms/people/c/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/c/f;->e()Lcom/google/android/gms/people/c/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/g;->a:Lcom/google/android/gms/people/service/a/f;

    invoke-virtual {v1}, Lcom/google/android/gms/people/service/a/f;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/people/service/a/g;->a:Lcom/google/android/gms/people/service/a/f;

    invoke-static {v3}, Lcom/google/android/gms/people/service/a/f;->a(Lcom/google/android/gms/people/service/a/f;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/people/c/b;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 475
    invoke-virtual {v2}, Lcom/google/android/gms/people/c/e;->a()V

    .line 479
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/g;->a:Lcom/google/android/gms/people/service/a/f;

    invoke-static {v0}, Lcom/google/android/gms/people/service/a/f;->b(Lcom/google/android/gms/people/service/a/f;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/g;->a:Lcom/google/android/gms/people/service/a/f;

    invoke-static {v0}, Lcom/google/android/gms/people/service/a/f;->b(Lcom/google/android/gms/people/service/a/f;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 481
    const-string v4, "INSERT INTO temp_gaia_ordinal(ordinal, gaia_id, qualified_id) VALUES (?, ?, ?)"

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6, v0}, Lcom/google/android/gms/people/internal/at;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Lcom/google/android/gms/people/c/e;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 479
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 492
    :cond_0
    invoke-virtual {p0, p3, v2, v3}, Lcom/google/android/gms/people/service/a/g;->a(Landroid/os/Bundle;Lcom/google/android/gms/people/c/e;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 494
    invoke-virtual {v2, v7}, Lcom/google/android/gms/people/c/e;->a(Z)V

    return v7

    :catchall_0
    move-exception v0

    invoke-virtual {v2, v7}, Lcom/google/android/gms/people/c/e;->a(Z)V

    throw v0
.end method
