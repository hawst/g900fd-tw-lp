.class final Lcom/google/android/gms/people/service/a/m;
.super Lcom/google/android/gms/people/service/a/n;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/service/a/f;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/people/service/a/f;)V
    .locals 1

    .prologue
    .line 681
    iput-object p1, p0, Lcom/google/android/gms/people/service/a/m;->a:Lcom/google/android/gms/people/service/a/f;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/people/service/a/n;-><init>(Lcom/google/android/gms/people/service/a/f;B)V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/people/service/a/f;B)V
    .locals 0

    .prologue
    .line 681
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/service/a/m;-><init>(Lcom/google/android/gms/people/service/a/f;)V

    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/gms/people/a/b;
    .locals 1

    .prologue
    .line 684
    iget-object v0, p0, Lcom/google/android/gms/people/service/a/m;->a:Lcom/google/android/gms/people/service/a/f;

    invoke-static {v0}, Lcom/google/android/gms/people/service/a/f;->c(Lcom/google/android/gms/people/service/a/f;)Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->e()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 685
    const/4 v0, 0x0

    .line 688
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "list_by_phone"

    invoke-static {v0}, Lcom/google/android/gms/people/a/b;->b(Ljava/lang/String;)Lcom/google/android/gms/people/a/b;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;Landroid/os/Bundle;)Z
    .locals 3

    .prologue
    .line 693
    invoke-static {p1}, Lcom/google/android/gms/people/identity/internal/b;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 694
    const/4 v0, 0x0

    .line 706
    :goto_0
    return v0

    .line 697
    :cond_0
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 698
    const-string v0, "PeopleService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Requesting \'"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 701
    :cond_1
    const-string v0, "on_behalf_of"

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/m;->a:Lcom/google/android/gms/people/service/a/f;

    invoke-static {v1}, Lcom/google/android/gms/people/service/a/f;->a(Lcom/google/android/gms/people/service/a/f;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 702
    const-string v0, "qualified_id"

    invoke-virtual {p2, v0, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 703
    const-string v0, "phone"

    invoke-static {p1}, Lcom/google/android/gms/people/identity/internal/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 706
    const/4 v0, 0x1

    goto :goto_0
.end method
