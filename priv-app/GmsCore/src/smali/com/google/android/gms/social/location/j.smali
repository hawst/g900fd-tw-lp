.class public final Lcom/google/android/gms/social/location/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;
.implements Lcom/google/android/gms/social/location/b;


# instance fields
.field final a:Landroid/content/BroadcastReceiver;

.field final b:Landroid/content/Context;

.field c:Lcom/google/android/gms/common/api/v;

.field d:Lcom/google/android/gms/location/reporting/h;

.field e:Landroid/content/Intent;

.field f:I

.field g:Z

.field private final h:Landroid/accounts/Account;

.field private final i:Landroid/support/v4/app/v;

.field private j:Lcom/google/android/gms/social/location/n;

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Landroid/support/v4/app/v;Lcom/google/android/gms/social/location/n;)V
    .locals 2

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    new-instance v0, Lcom/google/android/gms/social/location/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/social/location/k;-><init>(Lcom/google/android/gms/social/location/j;)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/j;->a:Landroid/content/BroadcastReceiver;

    .line 80
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/social/location/j;->f:I

    .line 97
    iput-object p1, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    .line 98
    new-instance v0, Landroid/accounts/Account;

    const-string v1, "com.google"

    invoke-direct {v0, p2, v1}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/social/location/j;->h:Landroid/accounts/Account;

    .line 99
    iput-object p3, p0, Lcom/google/android/gms/social/location/j;->i:Landroid/support/v4/app/v;

    .line 100
    iput-object p4, p0, Lcom/google/android/gms/social/location/j;->j:Lcom/google/android/gms/social/location/n;

    .line 101
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/location/reporting/j;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/social/location/j;->c:Lcom/google/android/gms/common/api/v;

    .line 106
    return-void
.end method

.method static a(Landroid/content/Context;)Z
    .locals 2

    .prologue
    .line 458
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 459
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 1

    .prologue
    .line 441
    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->j:Lcom/google/android/gms/social/location/n;

    if-eqz v0, :cond_0

    .line 442
    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->j:Lcom/google/android/gms/social/location/n;

    invoke-interface {v0}, Lcom/google/android/gms/social/location/n;->a()V

    .line 444
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 140
    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->pJ:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/social/location/j;->h:Landroid/accounts/Account;

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    sget v4, Lcom/google/android/gms/p;->hp:I

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v3, v5, v0

    aput-object v2, v5, v6

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/social/location/j;->k:Ljava/lang/String;

    sget v4, Lcom/google/android/gms/p;->hq:I

    new-array v5, v6, [Ljava/lang/Object;

    aput-object v2, v5, v0

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/gms/social/location/j;->l:Ljava/lang/String;

    sget v4, Lcom/google/android/gms/p;->hr:I

    new-array v5, v7, [Ljava/lang/Object;

    aput-object v3, v5, v0

    aput-object v2, v5, v6

    invoke-virtual {v1, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/social/location/j;->m:Ljava/lang/String;

    .line 141
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/j;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->d:Lcom/google/android/gms/location/reporting/h;

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/social/location/j;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->d:Lcom/google/android/gms/location/reporting/h;

    if-nez v1, :cond_2

    :cond_0
    move v1, v0

    :goto_0
    if-eqz v1, :cond_3

    .line 142
    :cond_1
    :goto_1
    return-void

    .line 141
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->d:Lcom/google/android/gms/location/reporting/h;

    invoke-interface {v1}, Lcom/google/android/gms/location/reporting/h;->d()Z

    move-result v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/j;->b()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->d:Lcom/google/android/gms/location/reporting/h;

    if-nez v1, :cond_5

    :cond_4
    :goto_2
    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->hd:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->k:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->yL:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->dD:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/social/location/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/social/location/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/social/location/a;->a(Lcom/google/android/gms/social/location/b;)V

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->i:Landroid/support/v4/app/v;

    const-string v2, "enable_location_reporting_auto"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/social/location/a;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->d:Lcom/google/android/gms/location/reporting/h;

    invoke-interface {v0}, Lcom/google/android/gms/location/reporting/h;->e()Z

    move-result v0

    goto :goto_2

    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/j;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/social/location/j;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->hd:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->m:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->ry:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->dD:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/social/location/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/social/location/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/social/location/a;->a(Lcom/google/android/gms/social/location/b;)V

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->i:Landroid/support/v4/app/v;

    const-string v2, "enable_location_reporting_manual_multi_account"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/social/location/a;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->hd:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->ry:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->dD:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/social/location/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/social/location/a;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/social/location/a;->a(Lcom/google/android/gms/social/location/b;)V

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->i:Landroid/support/v4/app/v;

    const-string v2, "enable_location_reporting_manual "

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/social/location/a;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 415
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/social/location/j;->f:I

    .line 416
    invoke-direct {p0}, Lcom/google/android/gms/social/location/j;->g()V

    .line 417
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 376
    const-string v0, "enable_location_reporting_auto"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 377
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/j;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    if-nez v2, :cond_0

    .line 378
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/j;->e()V

    .line 385
    :cond_0
    :goto_1
    return-void

    .line 377
    :cond_1
    sget-object v0, Lcom/google/android/gms/location/reporting/j;->b:Lcom/google/android/gms/location/reporting/g;

    iget-object v2, p0, Lcom/google/android/gms/social/location/j;->c:Lcom/google/android/gms/common/api/v;

    iget-object v3, p0, Lcom/google/android/gms/social/location/j;->h:Landroid/accounts/Account;

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/location/reporting/g;->b(Lcom/google/android/gms/common/api/v;Landroid/accounts/Account;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/social/location/l;

    invoke-direct {v2, p0}, Lcom/google/android/gms/social/location/l;-><init>(Lcom/google/android/gms/social/location/j;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    move v2, v1

    goto :goto_0

    .line 380
    :cond_2
    const-string v0, "enable_location_reporting_manual "

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "enable_location_reporting_manual_multi_account"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "enable_location_reporting_error"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 383
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    const-string v3, "location"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->getProviders(Z)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_4

    move v0, v1

    :goto_2
    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    const-string v1, "com.google.android.gms.location.settings.GOOGLE_LOCATION_SETTINGS"

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v1, 0x10000000

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "com.google.android.gms"

    invoke-virtual {v2, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    :try_start_0
    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    const-string v0, "GCoreLocationSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Problem while starting settings activity"

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/j;->d()V

    goto :goto_1
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 389
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/social/location/j;->f:I

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 397
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/j;->f()V

    .line 400
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 401
    const-string v1, "com.google.android.gms.location.reporting.SETTINGS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 402
    iget-boolean v1, p0, Lcom/google/android/gms/social/location/j;->g:Z

    if-nez v1, :cond_0

    .line 403
    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/social/location/j;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 404
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/social/location/j;->g:Z

    .line 406
    :cond_0
    return-void
.end method

.method final c(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->i:Landroid/support/v4/app/v;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/social/location/a;

    .line 369
    if-eqz v0, :cond_0

    .line 370
    invoke-virtual {v0, p0}, Lcom/google/android/gms/social/location/a;->a(Lcom/google/android/gms/social/location/b;)V

    .line 372
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 202
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/j;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->d:Lcom/google/android/gms/location/reporting/h;

    if-nez v1, :cond_1

    .line 206
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->d:Lcom/google/android/gms/location/reporting/h;

    invoke-interface {v1}, Lcom/google/android/gms/location/reporting/h;->b()Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->e:Landroid/content/Intent;

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/j;->c()Z

    move-result v0

    if-nez v0, :cond_1

    .line 250
    :cond_0
    :goto_0
    return-void

    .line 245
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->d:Lcom/google/android/gms/location/reporting/h;

    invoke-interface {v0}, Lcom/google/android/gms/location/reporting/h;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 246
    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->h:Landroid/accounts/Account;

    invoke-static {v0}, Lcom/google/android/gms/location/a/b;->a(Landroid/accounts/Account;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 247
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->e:Landroid/content/Intent;

    if-eqz v0, :cond_0

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->e:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method final e()V
    .locals 6

    .prologue
    .line 306
    iget-object v0, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->hc:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    sget v2, Lcom/google/android/gms/p;->hb:I

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/social/location/j;->h:Landroid/accounts/Account;

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    sget v3, Lcom/google/android/gms/p;->ry:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/social/location/j;->b:Landroid/content/Context;

    sget v4, Lcom/google/android/gms/p;->dD:I

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/social/location/a;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/social/location/a;

    move-result-object v0

    .line 311
    invoke-virtual {v0, p0}, Lcom/google/android/gms/social/location/a;->a(Lcom/google/android/gms/social/location/b;)V

    .line 312
    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->i:Landroid/support/v4/app/v;

    const-string v2, "enable_location_reporting_error"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/social/location/a;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 313
    return-void
.end method

.method final f()V
    .locals 3

    .prologue
    .line 420
    invoke-virtual {p0}, Lcom/google/android/gms/social/location/j;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 438
    :goto_0
    return-void

    .line 424
    :cond_0
    sget-object v0, Lcom/google/android/gms/location/reporting/j;->b:Lcom/google/android/gms/location/reporting/g;

    iget-object v1, p0, Lcom/google/android/gms/social/location/j;->c:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/social/location/j;->h:Landroid/accounts/Account;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/location/reporting/g;->a(Lcom/google/android/gms/common/api/v;Landroid/accounts/Account;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/social/location/m;

    invoke-direct {v1, p0}, Lcom/google/android/gms/social/location/m;-><init>(Lcom/google/android/gms/social/location/j;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 437
    invoke-direct {p0}, Lcom/google/android/gms/social/location/j;->g()V

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 0

    .prologue
    .line 410
    invoke-direct {p0}, Lcom/google/android/gms/social/location/j;->g()V

    .line 411
    return-void
.end method
