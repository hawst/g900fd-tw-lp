.class public final Lcom/google/android/gms/games/g/ab;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/games/g/u;

.field public b:Lcom/google/android/gms/games/g/h;

.field public c:Lcom/google/android/gms/games/g/g;

.field public d:Lcom/google/android/gms/games/g/al;

.field public e:Lcom/google/android/gms/games/g/i;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3247
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3248
    iput-object v0, p0, Lcom/google/android/gms/games/g/ab;->a:Lcom/google/android/gms/games/g/u;

    iput-object v0, p0, Lcom/google/android/gms/games/g/ab;->b:Lcom/google/android/gms/games/g/h;

    iput-object v0, p0, Lcom/google/android/gms/games/g/ab;->c:Lcom/google/android/gms/games/g/g;

    iput-object v0, p0, Lcom/google/android/gms/games/g/ab;->d:Lcom/google/android/gms/games/g/al;

    iput-object v0, p0, Lcom/google/android/gms/games/g/ab;->e:Lcom/google/android/gms/games/g/i;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/g/ab;->cachedSize:I

    .line 3249
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3357
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3358
    iget-object v1, p0, Lcom/google/android/gms/games/g/ab;->a:Lcom/google/android/gms/games/g/u;

    if-eqz v1, :cond_0

    .line 3359
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->a:Lcom/google/android/gms/games/g/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3362
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/g/ab;->b:Lcom/google/android/gms/games/g/h;

    if-eqz v1, :cond_1

    .line 3363
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->b:Lcom/google/android/gms/games/g/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3366
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/g/ab;->c:Lcom/google/android/gms/games/g/g;

    if-eqz v1, :cond_2

    .line 3367
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->c:Lcom/google/android/gms/games/g/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3370
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/g/ab;->d:Lcom/google/android/gms/games/g/al;

    if-eqz v1, :cond_3

    .line 3371
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->d:Lcom/google/android/gms/games/g/al;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3374
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/g/ab;->e:Lcom/google/android/gms/games/g/i;

    if-eqz v1, :cond_4

    .line 3375
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->e:Lcom/google/android/gms/games/g/i;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3378
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3263
    if-ne p1, p0, :cond_1

    .line 3315
    :cond_0
    :goto_0
    return v0

    .line 3266
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/games/g/ab;

    if-nez v2, :cond_2

    move v0, v1

    .line 3267
    goto :goto_0

    .line 3269
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/g/ab;

    .line 3270
    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->a:Lcom/google/android/gms/games/g/u;

    if-nez v2, :cond_3

    .line 3271
    iget-object v2, p1, Lcom/google/android/gms/games/g/ab;->a:Lcom/google/android/gms/games/g/u;

    if-eqz v2, :cond_4

    move v0, v1

    .line 3272
    goto :goto_0

    .line 3275
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->a:Lcom/google/android/gms/games/g/u;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ab;->a:Lcom/google/android/gms/games/g/u;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/u;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 3276
    goto :goto_0

    .line 3279
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->b:Lcom/google/android/gms/games/g/h;

    if-nez v2, :cond_5

    .line 3280
    iget-object v2, p1, Lcom/google/android/gms/games/g/ab;->b:Lcom/google/android/gms/games/g/h;

    if-eqz v2, :cond_6

    move v0, v1

    .line 3281
    goto :goto_0

    .line 3284
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->b:Lcom/google/android/gms/games/g/h;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ab;->b:Lcom/google/android/gms/games/g/h;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/h;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 3285
    goto :goto_0

    .line 3288
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->c:Lcom/google/android/gms/games/g/g;

    if-nez v2, :cond_7

    .line 3289
    iget-object v2, p1, Lcom/google/android/gms/games/g/ab;->c:Lcom/google/android/gms/games/g/g;

    if-eqz v2, :cond_8

    move v0, v1

    .line 3290
    goto :goto_0

    .line 3293
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->c:Lcom/google/android/gms/games/g/g;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ab;->c:Lcom/google/android/gms/games/g/g;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/g;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 3294
    goto :goto_0

    .line 3297
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->d:Lcom/google/android/gms/games/g/al;

    if-nez v2, :cond_9

    .line 3298
    iget-object v2, p1, Lcom/google/android/gms/games/g/ab;->d:Lcom/google/android/gms/games/g/al;

    if-eqz v2, :cond_a

    move v0, v1

    .line 3299
    goto :goto_0

    .line 3302
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->d:Lcom/google/android/gms/games/g/al;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ab;->d:Lcom/google/android/gms/games/g/al;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/al;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 3303
    goto :goto_0

    .line 3306
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->e:Lcom/google/android/gms/games/g/i;

    if-nez v2, :cond_b

    .line 3307
    iget-object v2, p1, Lcom/google/android/gms/games/g/ab;->e:Lcom/google/android/gms/games/g/i;

    if-eqz v2, :cond_0

    move v0, v1

    .line 3308
    goto :goto_0

    .line 3311
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->e:Lcom/google/android/gms/games/g/i;

    iget-object v3, p1, Lcom/google/android/gms/games/g/ab;->e:Lcom/google/android/gms/games/g/i;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/g/i;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 3312
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3320
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->a:Lcom/google/android/gms/games/g/u;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3323
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->b:Lcom/google/android/gms/games/g/h;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3325
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->c:Lcom/google/android/gms/games/g/g;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3327
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->d:Lcom/google/android/gms/games/g/al;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 3329
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/games/g/ab;->e:Lcom/google/android/gms/games/g/i;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 3331
    return v0

    .line 3320
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->a:Lcom/google/android/gms/games/g/u;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/u;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3323
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->b:Lcom/google/android/gms/games/g/h;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/h;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3325
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->c:Lcom/google/android/gms/games/g/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/g;->hashCode()I

    move-result v0

    goto :goto_2

    .line 3327
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->d:Lcom/google/android/gms/games/g/al;

    invoke-virtual {v0}, Lcom/google/android/gms/games/g/al;->hashCode()I

    move-result v0

    goto :goto_3

    .line 3329
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/g/ab;->e:Lcom/google/android/gms/games/g/i;

    invoke-virtual {v1}, Lcom/google/android/gms/games/g/i;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3215
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->a:Lcom/google/android/gms/games/g/u;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/g/u;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/u;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/ab;->a:Lcom/google/android/gms/games/g/u;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->a:Lcom/google/android/gms/games/g/u;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->b:Lcom/google/android/gms/games/g/h;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/games/g/h;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/h;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/ab;->b:Lcom/google/android/gms/games/g/h;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->b:Lcom/google/android/gms/games/g/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->c:Lcom/google/android/gms/games/g/g;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/games/g/g;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/ab;->c:Lcom/google/android/gms/games/g/g;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->c:Lcom/google/android/gms/games/g/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->d:Lcom/google/android/gms/games/g/al;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/android/gms/games/g/al;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/al;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/ab;->d:Lcom/google/android/gms/games/g/al;

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->d:Lcom/google/android/gms/games/g/al;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->e:Lcom/google/android/gms/games/g/i;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/android/gms/games/g/i;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/g/ab;->e:Lcom/google/android/gms/games/g/i;

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->e:Lcom/google/android/gms/games/g/i;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3337
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->a:Lcom/google/android/gms/games/g/u;

    if-eqz v0, :cond_0

    .line 3338
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/games/g/ab;->a:Lcom/google/android/gms/games/g/u;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3340
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->b:Lcom/google/android/gms/games/g/h;

    if-eqz v0, :cond_1

    .line 3341
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/games/g/ab;->b:Lcom/google/android/gms/games/g/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3343
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->c:Lcom/google/android/gms/games/g/g;

    if-eqz v0, :cond_2

    .line 3344
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/games/g/ab;->c:Lcom/google/android/gms/games/g/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3346
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->d:Lcom/google/android/gms/games/g/al;

    if-eqz v0, :cond_3

    .line 3347
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/games/g/ab;->d:Lcom/google/android/gms/games/g/al;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3349
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/g/ab;->e:Lcom/google/android/gms/games/g/i;

    if-eqz v0, :cond_4

    .line 3350
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/games/g/ab;->e:Lcom/google/android/gms/games/g/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3352
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3353
    return-void
.end method
