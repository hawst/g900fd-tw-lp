.class public final Lcom/google/android/gms/kids/restrictions/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;


# instance fields
.field private final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 19
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "restriction"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/kids/restrictions/c;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Z)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-boolean p1, p0, Lcom/google/android/gms/kids/restrictions/c;->b:Z

    .line 26
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 14

    .prologue
    const/4 v8, 0x0

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 29
    invoke-static {}, Lcom/google/android/gms/kids/common/d;->b()Landroid/content/Context;

    move-result-object v0

    const-string v1, "device_policy"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    invoke-static {}, Lcom/google/android/gms/kids/common/d;->b()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->isProfileOwnerApp(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 30
    const-string v0, "RestrictionsProcessor"

    const-string v1, "Profile owner not set"

    new-array v2, v7, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 54
    :goto_0
    return-void

    .line 36
    :cond_0
    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/kids/restrictions/a;->e()Lcom/google/android/gms/kids/restrictions/a;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/kids/restrictions/a;->d()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/kids/restrictions/c;->a:[Ljava/lang/String;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v2

    .line 39
    :try_start_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move v0, v7

    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->isAfterLast()Z

    move-result v1

    if-nez v1, :cond_12

    .line 40
    add-int/lit8 v4, v0, 0x1

    .line 41
    new-instance v3, Lcom/google/s/a/q;

    invoke-direct {v3}, Lcom/google/s/a/q;-><init>()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 43
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_2
    .catch Lcom/google/protobuf/nano/i; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 48
    :try_start_3
    iget-object v0, v3, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    iget-object v0, v0, Lcom/google/s/a/s;->a:Ljava/lang/Integer;

    if-nez v0, :cond_2

    const-string v0, "RestrictionsProcessor"

    const-string v1, "Invalid restriction.key.domain"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 39
    :cond_1
    :goto_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move v0, v4

    goto :goto_1

    .line 44
    :catch_0
    move-exception v0

    .line 45
    const-string v1, "RestrictionsProcessor"

    const-string v3, "Could not parse"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1, v0, v3, v5}, Lcom/google/android/gms/kids/common/f;->d(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2

    .line 51
    :catchall_0
    move-exception v0

    move-object v1, v2

    :goto_3
    invoke-static {v1}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0

    .line 48
    :cond_2
    :try_start_4
    iget-object v0, v3, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    iget-object v0, v0, Lcom/google/s/a/s;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_2

    :pswitch_0
    const-string v0, "device_policy"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    invoke-static {p1}, Lcom/google/android/gms/kids/common/b;->a(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v5

    iget-object v1, v3, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    iget-object v9, v1, Lcom/google/s/a/s;->b:Lcom/google/s/a/m;

    if-eqz v9, :cond_3

    iget-object v9, v1, Lcom/google/s/a/s;->b:Lcom/google/s/a/m;

    iget-object v9, v9, Lcom/google/s/a/m;->b:Ljava/lang/Integer;

    if-nez v9, :cond_4

    :cond_3
    const-string v0, "RestrictionsProcessor"

    const-string v1, "Invalid key.androidAppKey.type"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_2

    :cond_4
    iget-object v9, v1, Lcom/google/s/a/s;->b:Lcom/google/s/a/m;

    iget-object v9, v9, Lcom/google/s/a/m;->b:Ljava/lang/Integer;

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v9

    if-ne v9, v6, :cond_1

    iget-object v1, v1, Lcom/google/s/a/s;->b:Lcom/google/s/a/m;

    iget-object v9, v1, Lcom/google/s/a/m;->a:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/kids/restrictions/c;->b:Z

    if-nez v1, :cond_6

    iget-object v1, v3, Lcom/google/s/a/q;->d:Lcom/google/s/a/v;

    if-eqz v1, :cond_6

    iget-object v1, v3, Lcom/google/s/a/q;->d:Lcom/google/s/a/v;

    iget-object v1, v1, Lcom/google/s/a/v;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result v1

    if-nez v1, :cond_6

    move v1, v6

    :goto_4
    :try_start_5
    const-string v10, "RestrictionsProcessor"

    const-string v11, "%s is %s"

    const/4 v3, 0x2

    new-array v12, v3, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object v9, v12, v3

    const/4 v13, 0x1

    if-eqz v1, :cond_7

    const-string v3, "hidden"

    :goto_5
    aput-object v3, v12, v13

    invoke-static {v10, v11, v12}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {v0, v5, v9}, Landroid/app/admin/DevicePolicyManager;->isApplicationHidden(Landroid/content/ComponentName;Ljava/lang/String;)Z

    move-result v3

    if-eq v3, v1, :cond_1

    invoke-virtual {v0, v5, v9, v1}, Landroid/app/admin/DevicePolicyManager;->setApplicationHidden(Landroid/content/ComponentName;Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    if-nez v3, :cond_8

    :cond_5
    const-string v0, "RestrictionsProcessor"

    const-string v1, "Successfully updated settings: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v9, v3, v5

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_5
    .catch Ljava/lang/RuntimeException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_2

    :catch_1
    move-exception v0

    :try_start_6
    const-string v1, "RestrictionsProcessor"

    const-string v3, "Exception when updating settings: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v9, v5, v10

    invoke-static {v1, v0, v3, v5}, Lcom/google/android/gms/kids/common/f;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto/16 :goto_2

    :cond_6
    move v1, v7

    goto :goto_4

    :cond_7
    :try_start_7
    const-string v3, "not hidden"

    goto :goto_5

    :cond_8
    const-string v0, "RestrictionsProcessor"

    const-string v1, "Did not update settings: %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v9, v3, v5

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_7
    .catch Ljava/lang/RuntimeException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    :pswitch_1
    :try_start_8
    iget-object v0, v3, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    iget-object v0, v0, Lcom/google/s/a/s;->c:Lcom/google/s/a/p;

    if-eqz v0, :cond_9

    iget-object v0, v3, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    iget-object v0, v0, Lcom/google/s/a/s;->c:Lcom/google/s/a/p;

    iget-object v0, v0, Lcom/google/s/a/p;->a:Ljava/lang/Integer;

    if-nez v0, :cond_a

    :cond_9
    const-string v0, "RestrictionsProcessor"

    const-string v1, "Invalid restriction.key.lockdownKey"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_a
    iget-object v0, v3, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    iget-object v0, v0, Lcom/google/s/a/s;->c:Lcom/google/s/a/p;

    iget-object v0, v0, Lcom/google/s/a/p;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    const-string v1, "RestrictionsProcessor"

    const-string v3, "Unhandled type: %d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v9

    invoke-static {v1, v3, v5}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    :pswitch_2
    iget-boolean v1, p0, Lcom/google/android/gms/kids/restrictions/c;->b:Z

    iget-object v0, v3, Lcom/google/s/a/q;->b:Lcom/google/s/a/s;

    iget-object v0, v0, Lcom/google/s/a/s;->c:Lcom/google/s/a/p;

    iget-object v5, v0, Lcom/google/s/a/p;->b:Ljava/lang/String;
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :try_start_9
    const-class v0, Landroid/os/UserManager;

    invoke-virtual {v0, v5}, Ljava/lang/Class;->getDeclaredField(Ljava/lang/String;)Ljava/lang/reflect/Field;
    :try_end_9
    .catch Ljava/lang/NoSuchFieldException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v0

    :goto_6
    if-eqz v0, :cond_b

    :try_start_a
    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getModifiers()I

    move-result v9

    invoke-static {v9}, Ljava/lang/reflect/Modifier;->isStatic(I)Z

    move-result v9

    if-eqz v9, :cond_b

    invoke-virtual {v0}, Ljava/lang/reflect/Field;->getType()Ljava/lang/Class;

    move-result-object v9

    const-class v10, Ljava/lang/String;

    if-eq v9, v10, :cond_c

    :cond_b
    const-string v0, "UserManagerRestrictionProcessor"

    const-string v1, "Cannot find restriction type %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v5, v3, v9

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    goto/16 :goto_2

    :catch_2
    move-exception v0

    move-object v0, v8

    goto :goto_6

    :cond_c
    const/4 v9, 0x0

    :try_start_b
    invoke-virtual {v0, v9}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_b
    .catch Ljava/lang/IllegalAccessException; {:try_start_b .. :try_end_b} :catch_4
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    if-nez v1, :cond_d

    :try_start_c
    iget-object v1, v3, Lcom/google/s/a/q;->d:Lcom/google/s/a/v;

    if-eqz v1, :cond_d

    iget-object v1, v3, Lcom/google/s/a/q;->d:Lcom/google/s/a/v;

    iget-object v1, v1, Lcom/google/s/a/v;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_e

    :cond_d
    move v1, v6

    :goto_7
    if-eqz v1, :cond_f

    move v3, v7

    :goto_8
    const-string v9, "UserManagerRestrictionProcessor"

    const-string v10, "%s %s"

    const/4 v1, 0x2

    new-array v11, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object v5, v11, v1

    const/4 v5, 0x1

    if-eqz v3, :cond_10

    const-string v1, "set"

    :goto_9
    aput-object v1, v11, v5

    invoke-static {v9, v10, v11}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    const-string v1, "device_policy"

    invoke-virtual {p1, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/admin/DevicePolicyManager;

    invoke-static {p1}, Lcom/google/android/gms/kids/common/b;->a(Landroid/content/Context;)Landroid/content/ComponentName;
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    move-result-object v5

    if-eqz v3, :cond_11

    :try_start_d
    invoke-virtual {v1, v5, v0}, Landroid/app/admin/DevicePolicyManager;->addUserRestriction(Landroid/content/ComponentName;Ljava/lang/String;)V
    :try_end_d
    .catch Ljava/lang/RuntimeException; {:try_start_d .. :try_end_d} :catch_3
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    goto/16 :goto_2

    :catch_3
    move-exception v0

    :try_start_e
    const-string v1, "UserManagerRestrictionProcessor"

    const-string v3, "Cannot update value for %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v9, 0x0

    aput-object v0, v5, v9

    invoke-static {v1, v0, v3, v5}, Lcom/google/android/gms/kids/common/f;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    :catch_4
    move-exception v0

    const-string v1, "UserManagerRestrictionProcessor"

    const-string v3, "Cannot obtain value of restriction %s"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    aput-object v5, v9, v10

    invoke-static {v1, v0, v3, v9}, Lcom/google/android/gms/kids/common/f;->b(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_2

    :cond_e
    move v1, v7

    goto :goto_7

    :cond_f
    iget-object v1, v3, Lcom/google/s/a/q;->d:Lcom/google/s/a/v;

    iget-object v1, v1, Lcom/google/s/a/v;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    move v3, v1

    goto :goto_8

    :cond_10
    const-string v1, "unset"
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    goto :goto_9

    :cond_11
    :try_start_f
    invoke-virtual {v1, v5, v0}, Landroid/app/admin/DevicePolicyManager;->clearUserRestriction(Landroid/content/ComponentName;Ljava/lang/String;)V
    :try_end_f
    .catch Ljava/lang/RuntimeException; {:try_start_f .. :try_end_f} :catch_3
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    goto/16 :goto_2

    .line 51
    :cond_12
    invoke-static {v2}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 53
    const-string v1, "RestrictionsProcessor"

    const-string v2, "Processed %d entries"

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v7

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/kids/common/f;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 51
    :catchall_1
    move-exception v0

    move-object v1, v8

    goto/16 :goto_3

    .line 48
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_2
    .end packed-switch
.end method
