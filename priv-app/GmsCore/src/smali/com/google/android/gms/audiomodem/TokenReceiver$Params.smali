.class public Lcom/google/android/gms/audiomodem/TokenReceiver$Params;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:[Lcom/google/android/gms/audiomodem/Encoding;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/audiomodem/bn;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/bn;-><init>()V

    sput-object v0, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(I[Lcom/google/android/gms/audiomodem/Encoding;)V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput p1, p0, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->a:I

    .line 65
    iput-object p2, p0, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->b:[Lcom/google/android/gms/audiomodem/Encoding;

    .line 66
    return-void
.end method

.method public constructor <init>([Lcom/google/android/gms/audiomodem/Encoding;)V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;-><init>(I[Lcom/google/android/gms/audiomodem/Encoding;)V

    .line 70
    return-void
.end method

.method public static a()Lcom/google/android/gms/audiomodem/TokenReceiver$Params;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 33
    new-instance v0, Lcom/google/android/gms/audiomodem/am;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/am;-><init>()V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/audiomodem/am;->a(I)Lcom/google/android/gms/audiomodem/am;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/audiomodem/am;->b(I)Lcom/google/android/gms/audiomodem/am;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/am;->a()Lcom/google/android/gms/audiomodem/Encoding;

    move-result-object v0

    .line 38
    new-instance v1, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    new-array v2, v4, [Lcom/google/android/gms/audiomodem/Encoding;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-direct {v1, v4, v2}, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;-><init>(I[Lcom/google/android/gms/audiomodem/Encoding;)V

    return-object v1
.end method

.method public static a(I)Lcom/google/android/gms/audiomodem/TokenReceiver$Params;
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 42
    new-instance v0, Lcom/google/android/gms/audiomodem/an;

    invoke-direct {v0}, Lcom/google/android/gms/audiomodem/an;-><init>()V

    invoke-virtual {v0, p0}, Lcom/google/android/gms/audiomodem/an;->a(I)Lcom/google/android/gms/audiomodem/an;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/audiomodem/an;->b(I)Lcom/google/android/gms/audiomodem/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/audiomodem/an;->a()Lcom/google/android/gms/audiomodem/Encoding;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;

    new-array v2, v4, [Lcom/google/android/gms/audiomodem/Encoding;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-direct {v1, v4, v2}, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;-><init>(I[Lcom/google/android/gms/audiomodem/Encoding;)V

    return-object v1
.end method


# virtual methods
.method final b()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->a:I

    return v0
.end method

.method public final c()[Lcom/google/android/gms/audiomodem/Encoding;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/TokenReceiver$Params;->b:[Lcom/google/android/gms/audiomodem/Encoding;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 87
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/audiomodem/bn;->a(Lcom/google/android/gms/audiomodem/TokenReceiver$Params;Landroid/os/Parcel;I)V

    .line 88
    return-void
.end method
