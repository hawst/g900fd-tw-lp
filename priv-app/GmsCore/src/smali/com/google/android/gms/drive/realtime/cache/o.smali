.class public final Lcom/google/android/gms/drive/realtime/cache/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/drive/database/i;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/common/util/p;

.field private final d:Lcom/google/android/gms/drive/realtime/cache/k;

.field private final e:Lcom/google/android/gms/drive/database/r;

.field private final f:Lcom/google/android/gms/drive/realtime/cache/c;

.field private final g:Ljava/util/concurrent/ConcurrentMap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/common/util/p;Lcom/google/android/gms/drive/realtime/cache/i;Lcom/google/android/gms/drive/database/r;Lcom/google/android/gms/drive/realtime/cache/c;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/o;->g:Ljava/util/concurrent/ConcurrentMap;

    .line 59
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/cache/o;->b:Landroid/content/Context;

    .line 60
    iput-object p2, p0, Lcom/google/android/gms/drive/realtime/cache/o;->a:Lcom/google/android/gms/drive/database/i;

    .line 61
    iput-object p3, p0, Lcom/google/android/gms/drive/realtime/cache/o;->c:Lcom/google/android/gms/common/util/p;

    .line 62
    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/k;

    invoke-direct {v0, p4}, Lcom/google/android/gms/drive/realtime/cache/k;-><init>(Lcom/google/android/gms/drive/realtime/cache/i;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/o;->d:Lcom/google/android/gms/drive/realtime/cache/k;

    .line 63
    iput-object p5, p0, Lcom/google/android/gms/drive/realtime/cache/o;->e:Lcom/google/android/gms/drive/database/r;

    .line 64
    iput-object p6, p0, Lcom/google/android/gms/drive/realtime/cache/o;->f:Lcom/google/android/gms/drive/realtime/cache/c;

    .line 65
    return-void
.end method

.method private c(Lcom/google/android/gms/drive/realtime/e;)Lcom/google/android/gms/drive/database/model/br;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 144
    iget-object v0, p1, Lcom/google/android/gms/drive/realtime/e;->a:Lcom/google/android/gms/drive/DriveId;

    .line 146
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/o;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v3, p1, Lcom/google/android/gms/drive/realtime/e;->b:Lcom/google/android/gms/drive/auth/g;

    invoke-interface {v1, v3}, Lcom/google/android/gms/drive/database/r;->c(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v1

    .line 147
    if-nez v1, :cond_0

    .line 183
    :goto_0
    return-object v2

    .line 150
    :cond_0
    iget-wide v4, v1, Lcom/google/android/gms/drive/database/model/ad;->f:J

    .line 153
    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 154
    sget-object v1, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    sget-object v3, Lcom/google/android/gms/drive/database/model/bu;->d:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/drive/database/model/ab;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    new-array v3, v8, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v6, Lcom/google/android/gms/drive/database/model/bu;->e:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-virtual {v1, v0, v3}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    move-object v4, v0

    .line 168
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/o;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {}, Lcom/google/android/gms/drive/database/model/bt;->a()Lcom/google/android/gms/drive/database/model/bt;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/model/bt;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4}, Lcom/google/android/gms/drive/database/SqlWhereClause;->c()[Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/drive/database/i;->a(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 176
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eq v0, v8, :cond_2

    .line 177
    const-string v0, "RealtimeDocumentCacheProvider"

    const-string v3, "No cached realtime content."

    invoke-static {v0, v3}, Lcom/google/android/gms/drive/g/ab;->a(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 161
    :cond_1
    sget-object v1, Lcom/google/android/gms/drive/database/x;->a:Lcom/google/android/gms/drive/database/x;

    sget-object v3, Lcom/google/android/gms/drive/database/model/bu;->a:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v6

    invoke-virtual {v3, v6, v7}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    new-array v3, v8, [Lcom/google/android/gms/drive/database/SqlWhereClause;

    sget-object v6, Lcom/google/android/gms/drive/database/model/bu;->e:Lcom/google/android/gms/drive/database/model/bu;

    invoke-virtual {v6}, Lcom/google/android/gms/drive/database/model/bu;->b()Lcom/google/android/gms/drive/database/model/ab;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Lcom/google/android/gms/drive/database/model/ab;->a(J)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v4

    aput-object v4, v3, v9

    invoke-virtual {v1, v0, v3}, Lcom/google/android/gms/drive/database/x;->a(Lcom/google/android/gms/drive/database/SqlWhereClause;[Lcom/google/android/gms/drive/database/SqlWhereClause;)Lcom/google/android/gms/drive/database/SqlWhereClause;

    move-result-object v0

    move-object v4, v0

    goto :goto_1

    .line 180
    :cond_2
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/o;->a:Lcom/google/android/gms/drive/database/i;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/br;->a(Lcom/google/android/gms/drive/database/i;Landroid/database/Cursor;)Lcom/google/android/gms/drive/database/model/br;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v2

    .line 183
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/realtime/e;Ljava/lang/String;Z)Lcom/google/android/gms/drive/realtime/cache/n;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/o;->g:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/realtime/cache/n;

    .line 88
    if-eqz v0, :cond_1

    .line 89
    if-eqz p3, :cond_3

    .line 90
    const-string v1, "RealtimeDocumentCacheProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Closing cache (lock steal): "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-virtual {v0}, Lcom/google/android/gms/drive/realtime/cache/n;->h()V

    .line 97
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/realtime/cache/o;->b(Lcom/google/android/gms/drive/realtime/e;)Lcom/google/android/gms/drive/database/model/br;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/o;->f:Lcom/google/android/gms/drive/realtime/cache/c;

    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/o;->b:Landroid/content/Context;

    iget-object v1, v4, Lcom/google/android/gms/drive/database/model/br;->c:Ljava/lang/String;

    new-instance v2, Ljava/io/File;

    invoke-static {v0}, Lcom/google/android/gms/drive/realtime/cache/c;->a(Landroid/content/Context;)Ljava/io/File;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/drive/realtime/cache/a/g;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/o;->b:Landroid/content/Context;

    invoke-direct {v2, v1, v0}, Lcom/google/android/gms/drive/realtime/cache/a/g;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/drive/realtime/cache/n;

    iget-object v3, p0, Lcom/google/android/gms/drive/realtime/cache/o;->d:Lcom/google/android/gms/drive/realtime/cache/k;

    iget-object v6, p0, Lcom/google/android/gms/drive/realtime/cache/o;->g:Ljava/util/concurrent/ConcurrentMap;

    move-object v1, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/realtime/cache/n;-><init>(Lcom/google/android/gms/drive/realtime/e;Lcom/google/android/gms/drive/realtime/cache/a/g;Lcom/google/android/gms/drive/realtime/cache/k;Lcom/google/android/gms/drive/database/model/br;Ljava/lang/String;Ljava/util/Map;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/o;->g:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v1, p1, v0}, Ljava/util/concurrent/ConcurrentMap;->putIfAbsent(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_2

    move-object v0, v7

    .line 98
    :cond_2
    if-eqz v0, :cond_0

    .line 99
    :goto_0
    return-object v0

    .line 93
    :cond_3
    const-string v1, "RealtimeDocumentCacheProvider"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Cannot open; cache is already in use: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/g/ab;->b(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v7

    .line 94
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/drive/auth/g;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/o;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->c()V

    .line 218
    :try_start_0
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    .line 219
    new-instance v2, Lcom/google/android/gms/drive/realtime/e;

    invoke-direct {v2, v0, p1}, Lcom/google/android/gms/drive/realtime/e;-><init>(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/auth/g;)V

    invoke-direct {p0, v2}, Lcom/google/android/gms/drive/realtime/cache/o;->c(Lcom/google/android/gms/drive/realtime/e;)Lcom/google/android/gms/drive/database/model/br;

    move-result-object v0

    .line 221
    if-eqz v0, :cond_0

    .line 222
    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/br;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 227
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/o;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/database/i;->d()V

    .line 228
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/o;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/cache/o;->e:Lcom/google/android/gms/drive/database/r;

    invoke-static {v1, v2}, Lcom/google/android/gms/drive/realtime/cache/l;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/r;)V

    throw v0

    .line 225
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/o;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->e()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/o;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/i;->d()V

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/o;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/o;->e:Lcom/google/android/gms/drive/database/r;

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/realtime/cache/l;->a(Landroid/content/Context;Lcom/google/android/gms/drive/database/r;)V

    .line 229
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/drive/realtime/e;)Z
    .locals 1

    .prologue
    .line 72
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/realtime/cache/o;->c(Lcom/google/android/gms/drive/realtime/e;)Lcom/google/android/gms/drive/database/model/br;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lcom/google/android/gms/drive/realtime/e;)Lcom/google/android/gms/drive/database/model/br;
    .locals 9

    .prologue
    .line 118
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/realtime/cache/o;->c(Lcom/google/android/gms/drive/realtime/e;)Lcom/google/android/gms/drive/database/model/br;

    move-result-object v0

    .line 119
    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/o;->e:Lcom/google/android/gms/drive/database/r;

    iget-object v1, p1, Lcom/google/android/gms/drive/realtime/e;->b:Lcom/google/android/gms/drive/auth/g;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/database/r;->c(Lcom/google/android/gms/drive/auth/g;)Lcom/google/android/gms/drive/database/model/f;

    move-result-object v0

    iget-wide v4, v0, Lcom/google/android/gms/drive/database/model/ad;->f:J

    iget-object v3, p1, Lcom/google/android/gms/drive/realtime/e;->a:Lcom/google/android/gms/drive/DriveId;

    const/4 v2, 0x0

    invoke-virtual {v3}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v0

    const-wide/16 v6, 0x0

    cmp-long v0, v0, v6

    if-lez v0, :cond_1

    invoke-virtual {v3}, Lcom/google/android/gms/drive/DriveId;->b()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/database/model/EntrySpec;->a(J)Lcom/google/android/gms/drive/database/model/EntrySpec;

    move-result-object v2

    :cond_1
    new-instance v0, Lcom/google/android/gms/drive/database/model/br;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/cache/o;->a:Lcom/google/android/gms/drive/database/i;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v6, p0, Lcom/google/android/gms/drive/realtime/cache/o;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v6}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v6

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v8

    invoke-virtual {v8}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/drive/database/model/br;-><init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/model/EntrySpec;Ljava/lang/String;JJLjava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/database/model/br;->i()V

    goto :goto_0
.end method
