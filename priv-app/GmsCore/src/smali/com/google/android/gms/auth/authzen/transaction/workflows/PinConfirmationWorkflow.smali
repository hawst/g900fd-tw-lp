.class public Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;
.super Lcom/google/android/gms/auth/authzen/transaction/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/gms/auth/authzen/transaction/a;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/ab/b/a/a/r;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 38
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/r;->c:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-boolean v1, v1, Lcom/google/ab/b/a/a/q;->f:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-object v1, v1, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    iget-boolean v1, v1, Lcom/google/ab/b/a/a/e;->c:Z

    if-nez v1, :cond_1

    .line 41
    :cond_0
    const-string v1, "AuthZen"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Malformed request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 66
    :goto_0
    return v0

    .line 45
    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/auth/authzen/transaction/b/i;->a(Lcom/google/ab/b/a/a/r;)Lcom/google/android/gms/auth/authzen/transaction/b/h;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/auth/authzen/transaction/b/h;->b()Landroid/os/Bundle;
    :try_end_0
    .catch Lcom/google/android/gms/auth/authzen/transaction/b/j; {:try_start_0 .. :try_end_0} :catch_0

    .line 52
    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-object v1, v1, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    .line 53
    invoke-virtual {v1}, Lcom/google/ab/b/a/a/e;->c()I

    move-result v2

    if-gtz v2, :cond_2

    .line 54
    const-string v1, "AuthZen"

    const-string v2, "Request has 0 SelectorDescriptors"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 47
    :catch_0
    move-exception v1

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-object v1, v1, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    iget v1, v1, Lcom/google/ab/b/a/a/e;->d:I

    .line 48
    const-string v2, "AuthZen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Error while creating TextProvider for UseCase: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 60
    :cond_2
    invoke-virtual {v1, v0}, Lcom/google/ab/b/a/a/e;->a(I)Lcom/google/ab/b/a/a/l;

    move-result-object v1

    .line 61
    invoke-virtual {v1}, Lcom/google/ab/b/a/a/l;->c()I

    move-result v2

    const/4 v3, 0x3

    if-eq v2, v3, :cond_3

    .line 62
    const-string v2, "AuthZen"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unexpected number of pin options found: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/ab/b/a/a/l;->c()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 66
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static b(Lcom/google/ab/b/a/a/r;Ljava/lang/String;[B)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 32
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a(Lcom/google/ab/b/a/a/r;Ljava/lang/String;[B)Landroid/content/Intent;

    move-result-object v0

    .line 33
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 34
    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/auth/authzen/transaction/a/f;I)Z
    .locals 5

    .prologue
    const/16 v2, 0xc8

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 71
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/auth/authzen/transaction/a;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 118
    :goto_0
    return v4

    .line 74
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/auth/authzen/transaction/a/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 75
    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/d;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 76
    if-nez p2, :cond_1

    .line 78
    invoke-virtual {p0, v3, v3}, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a(IZ)V

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->d:Lcom/google/ab/b/a/a/r;

    iget-object v0, v0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    iget-object v0, v0, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    invoke-virtual {v0, v3}, Lcom/google/ab/b/a/a/e;->a(I)Lcom/google/ab/b/a/a/l;

    move-result-object v0

    iget-object v1, v0, Lcom/google/ab/b/a/a/l;->c:Ljava/util/List;

    .line 80
    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a:Landroid/os/Bundle;

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/auth/authzen/transaction/a/b;->a(Landroid/os/Bundle;ILjava/util/List;)Lcom/google/android/gms/auth/authzen/transaction/a/b;

    move-result-object v0

    .line 82
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;Lcom/google/android/gms/auth/authzen/transaction/a/f;)V

    goto :goto_0

    .line 85
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;)V

    goto :goto_0

    .line 88
    :cond_2
    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 89
    if-nez p2, :cond_3

    .line 92
    invoke-virtual {p0, v3, v3}, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a(II)V

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a:Landroid/os/Bundle;

    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/b/h;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 94
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 95
    :cond_3
    if-ne p2, v4, :cond_4

    .line 97
    invoke-virtual {p0, v2, v2}, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a(II)V

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 99
    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/a;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/b/h;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/a;->c:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/b/h;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/a;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/b/h;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/a;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a:Landroid/os/Bundle;

    sget-object v3, Lcom/google/android/gms/auth/authzen/transaction/b/h;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 107
    invoke-static {v0}, Lcom/google/android/gms/auth/authzen/transaction/a/a;->a(Landroid/os/Bundle;)Lcom/google/android/gms/auth/authzen/transaction/a/a;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a(Lcom/google/android/gms/auth/authzen/transaction/a/f;Lcom/google/android/gms/auth/authzen/transaction/a/f;)V

    goto/16 :goto_0

    .line 110
    :cond_4
    const/16 v0, 0x64

    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a(II)V

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a:Landroid/os/Bundle;

    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/b/h;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 112
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 115
    :cond_5
    sget-object v1, Lcom/google/android/gms/auth/authzen/transaction/a/a;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 116
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->setResult(I)V

    .line 117
    invoke-virtual {p0}, Lcom/google/android/gms/auth/authzen/transaction/workflows/PinConfirmationWorkflow;->finish()V

    goto/16 :goto_0

    .line 120
    :cond_6
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Fragment not supported in account recovery workflow: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
.end method
