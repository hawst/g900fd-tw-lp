.class final Lcom/google/android/gms/fitness/service/as;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/gms/fitness/a/o;

.field final synthetic c:Lcom/google/android/gms/fitness/service/aq;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/service/aq;Ljava/lang/String;Lcom/google/android/gms/fitness/a/o;)V
    .locals 0

    .prologue
    .line 72
    iput-object p1, p0, Lcom/google/android/gms/fitness/service/as;->c:Lcom/google/android/gms/fitness/service/aq;

    iput-object p2, p0, Lcom/google/android/gms/fitness/service/as;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/fitness/service/as;->b:Lcom/google/android/gms/fitness/a/o;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/as;->c:Lcom/google/android/gms/fitness/service/aq;

    iget-object v0, v0, Lcom/google/android/gms/fitness/service/aq;->d:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/gms/fitness/service/as;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/as;->b:Lcom/google/android/gms/fitness/a/o;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/as;->c:Lcom/google/android/gms/fitness/service/aq;

    iget-object v0, v0, Lcom/google/android/gms/fitness/service/aq;->f:Lcom/google/android/gms/fitness/service/at;

    if-nez v0, :cond_0

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/as;->c:Lcom/google/android/gms/fitness/service/aq;

    new-instance v1, Lcom/google/android/gms/fitness/service/at;

    iget-object v2, p0, Lcom/google/android/gms/fitness/service/as;->c:Lcom/google/android/gms/fitness/service/aq;

    invoke-direct {v1, v2}, Lcom/google/android/gms/fitness/service/at;-><init>(Lcom/google/android/gms/fitness/service/aq;)V

    iput-object v1, v0, Lcom/google/android/gms/fitness/service/aq;->f:Lcom/google/android/gms/fitness/service/at;

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/fitness/service/as;->c:Lcom/google/android/gms/fitness/service/aq;

    iget-object v1, v0, Lcom/google/android/gms/fitness/service/aq;->b:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/gms/fitness/service/as;->c:Lcom/google/android/gms/fitness/service/aq;

    iget-object v2, v0, Lcom/google/android/gms/fitness/service/aq;->f:Lcom/google/android/gms/fitness/service/at;

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    sget-object v0, Lcom/google/android/gms/fitness/g/c;->z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v3, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 83
    :cond_0
    return-void
.end method
