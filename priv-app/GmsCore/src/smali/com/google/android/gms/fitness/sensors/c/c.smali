.class public Lcom/google/android/gms/fitness/sensors/c/c;
.super Lcom/google/android/gms/fitness/sensors/b/a;
.source "SourceFile"


# static fields
.field public static a:Z

.field public static b:Z


# instance fields
.field public final c:Ljava/util/List;

.field private final d:Lcom/google/android/gms/fitness/sensors/a;

.field private final e:Z

.field private final f:Ljava/util/concurrent/ConcurrentHashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/fitness/sensors/c/c;->a:Z

    .line 37
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/fitness/sensors/c/c;->b:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/fitness/sensors/a;Ljava/util/List;Z)V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/gms/fitness/sensors/b/a;-><init>()V

    .line 47
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/c/c;->f:Ljava/util/concurrent/ConcurrentHashMap;

    .line 60
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/sensors/a;

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/c/c;->d:Lcom/google/android/gms/fitness/sensors/a;

    .line 61
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/fitness/sensors/c/c;->c:Ljava/util/List;

    .line 62
    iput-boolean p3, p0, Lcom/google/android/gms/fitness/sensors/c/c;->e:Z

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/fitness/sensors/c/c;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/c/c;->f:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/sensors/c/b;)Z
    .locals 2

    .prologue
    .line 98
    invoke-interface {p2}, Lcom/google/android/gms/fitness/sensors/c/b;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->h()Ljava/lang/String;

    move-result-object v0

    .line 99
    invoke-virtual {p1}, Lcom/google/android/gms/fitness/data/DataSource;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/gms/fitness/sensors/c/c;->a(Lcom/google/android/gms/fitness/sensors/c/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/sensors/c/b;)Z
    .locals 1

    .prologue
    .line 92
    invoke-interface {p2}, Lcom/google/android/gms/fitness/sensors/c/b;->c()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/fitness/data/DataType;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p2}, Lcom/google/android/gms/fitness/sensors/c/c;->a(Lcom/google/android/gms/fitness/sensors/c/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/fitness/sensors/c/b;)Z
    .locals 1

    .prologue
    .line 181
    invoke-interface {p1}, Lcom/google/android/gms/fitness/sensors/c/b;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/fitness/sensors/c/c;->c(Lcom/google/android/gms/fitness/data/DataType;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/google/android/gms/fitness/data/DataType;)Ljava/util/List;
    .locals 4

    .prologue
    .line 189
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/c/c;->d:Lcom/google/android/gms/fitness/sensors/a;

    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/sensors/a;->a(Lcom/google/android/gms/fitness/data/DataType;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/c/c;->d:Lcom/google/android/gms/fitness/sensors/a;

    invoke-interface {v0, p1}, Lcom/google/android/gms/fitness/sensors/a;->b(Lcom/google/android/gms/fitness/data/DataType;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/DataSource;

    .line 195
    iget-boolean v3, p0, Lcom/google/android/gms/fitness/sensors/c/c;->e:Z

    if-nez v3, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/DataSource;->b()I

    move-result v3

    if-nez v3, :cond_0

    .line 196
    :cond_1
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 200
    :cond_2
    return-object v1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/k/k/a/af;
    .locals 17

    .prologue
    .line 116
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/fitness/sensors/c/c;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/fitness/sensors/c/b;

    .line 117
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2}, Lcom/google/android/gms/fitness/sensors/c/c;->a(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/sensors/c/b;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 118
    invoke-interface {v2}, Lcom/google/android/gms/fitness/sensors/c/b;->b()Lcom/google/android/gms/fitness/data/DataType;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/google/android/gms/fitness/sensors/c/c;->c(Lcom/google/android/gms/fitness/data/DataType;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v2, "no input data sources found for %s"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v2

    .line 121
    :goto_0
    return-object v2

    .line 118
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v4

    const/4 v5, 0x1

    if-le v4, v5, :cond_2

    const-string v4, "more than one input data sources found for.  Using the first one: %s, %s, %s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object p1, v5, v6

    const/4 v6, 0x1

    aput-object v2, v5, v6

    const/4 v6, 0x2

    aput-object v3, v5, v6

    invoke-static {v4, v5}, Lcom/google/android/gms/fitness/m/a;->d(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_2
    const/4 v4, 0x0

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/fitness/data/DataSource;

    new-instance v5, Lcom/google/android/gms/fitness/sensors/c/e;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->c()Lcom/google/android/gms/fitness/data/l;

    move-result-object v3

    const/4 v6, 0x0

    invoke-direct {v5, v2, v3, v6}, Lcom/google/android/gms/fitness/sensors/c/e;-><init>(Lcom/google/android/gms/fitness/sensors/c/b;Lcom/google/android/gms/fitness/data/l;B)V

    new-instance v3, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->f()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->e()J

    move-result-wide v8

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->g()J

    move-result-wide v10

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->j()I

    move-result v12

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->h()Ljava/util/List;

    move-result-object v13

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->i()Ljava/util/List;

    move-result-object v14

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;->k()J

    move-result-wide v15

    invoke-direct/range {v3 .. v16}, Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;-><init>(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/data/l;JJJILjava/util/List;Ljava/util/List;J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/fitness/sensors/c/c;->d:Lcom/google/android/gms/fitness/sensors/a;

    invoke-interface {v2, v3}, Lcom/google/android/gms/fitness/sensors/a;->a(Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)Lcom/google/k/k/a/af;

    move-result-object v2

    new-instance v4, Lcom/google/android/gms/fitness/sensors/c/d;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v4, v0, v1, v5, v3}, Lcom/google/android/gms/fitness/sensors/c/d;-><init>(Lcom/google/android/gms/fitness/sensors/c/c;Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;Lcom/google/android/gms/fitness/sensors/c/e;Lcom/google/android/gms/fitness/request/SensorRegistrationRequest;)V

    invoke-static {v2, v4}, Lcom/google/k/k/a/n;->a(Lcom/google/k/k/a/af;Lcom/google/k/k/a/m;)V

    goto :goto_0

    .line 121
    :cond_3
    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-static {v2}, Lcom/google/k/k/a/n;->a(Ljava/lang/Object;)Lcom/google/k/k/a/af;

    move-result-object v2

    goto :goto_0
.end method

.method public a(Lcom/google/android/gms/fitness/data/DataSource;)Z
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/c/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/sensors/c/b;

    .line 82
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/fitness/sensors/c/c;->a(Lcom/google/android/gms/fitness/data/DataSource;Lcom/google/android/gms/fitness/sensors/c/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const/4 v0, 0x1

    .line 86
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/DataType;)Z
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/c/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/sensors/c/b;

    .line 72
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/fitness/sensors/c/c;->a(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/sensors/c/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    const/4 v0, 0x1

    .line 76
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/l;)Z
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/c/c;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/l;

    .line 206
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/c/c;->d:Lcom/google/android/gms/fitness/sensors/a;

    invoke-interface {v1, v0}, Lcom/google/android/gms/fitness/sensors/a;->a(Lcom/google/android/gms/fitness/data/l;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/fitness/data/DataType;)Ljava/util/List;
    .locals 4

    .prologue
    .line 105
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/c/c;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/sensors/c/b;

    .line 107
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/fitness/sensors/c/c;->a(Lcom/google/android/gms/fitness/data/DataType;Lcom/google/android/gms/fitness/sensors/c/b;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 108
    invoke-interface {v0}, Lcom/google/android/gms/fitness/sensors/c/b;->a()Lcom/google/android/gms/fitness/data/DataSource;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 111
    :cond_1
    return-object v1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/gms/fitness/sensors/c/c;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/fitness/data/l;

    .line 214
    invoke-virtual {p0, v0}, Lcom/google/android/gms/fitness/sensors/c/c;->a(Lcom/google/android/gms/fitness/data/l;)Z

    goto :goto_0

    .line 216
    :cond_0
    return-void
.end method
