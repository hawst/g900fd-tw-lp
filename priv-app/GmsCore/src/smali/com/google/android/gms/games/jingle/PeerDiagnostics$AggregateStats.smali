.class public Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final mCount:J

.field private final mMax:J

.field private final mMin:J

.field private final mSum:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    iput-wide v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->mMin:J

    .line 32
    iput-wide v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->mMax:J

    .line 33
    iput-wide v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->mSum:J

    .line 34
    iput-wide v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->mCount:J

    .line 35
    return-void
.end method


# virtual methods
.method public getCount()J
    .locals 2

    .prologue
    .line 47
    iget-wide v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->mCount:J

    return-wide v0
.end method

.method public getMax()J
    .locals 2

    .prologue
    .line 41
    iget-wide v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->mMax:J

    return-wide v0
.end method

.method public getMin()J
    .locals 2

    .prologue
    .line 38
    iget-wide v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->mMin:J

    return-wide v0
.end method

.method public getSum()J
    .locals 2

    .prologue
    .line 44
    iget-wide v0, p0, Lcom/google/android/gms/games/jingle/PeerDiagnostics$AggregateStats;->mSum:J

    return-wide v0
.end method
