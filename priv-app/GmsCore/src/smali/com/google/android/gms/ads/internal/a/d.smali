.class public final Lcom/google/android/gms/ads/internal/a/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;
.implements Landroid/view/ViewTreeObserver$OnScrollChangedListener;


# annotations
.annotation runtime Lcom/google/android/gms/ads/internal/m/a/a;
.end annotation


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Ljava/lang/ref/WeakReference;

.field private c:Ljava/lang/ref/WeakReference;

.field private final d:Ljava/lang/ref/WeakReference;

.field private final e:Lcom/google/android/gms/ads/internal/a/b;

.field private final f:Landroid/content/Context;

.field private final g:Lcom/google/android/gms/ads/internal/i/a;

.field private final h:Lcom/google/android/gms/ads/internal/util/a/a;

.field private i:Z

.field private final j:Landroid/view/WindowManager;

.field private final k:Landroid/os/PowerManager;

.field private final l:Landroid/app/KeyguardManager;

.field private m:Lcom/google/android/gms/ads/internal/a/o;

.field private n:Z

.field private o:Z

.field private p:J

.field private q:Z

.field private r:Z

.field private s:Landroid/content/BroadcastReceiver;

.field private final t:Ljava/util/HashSet;

.field private final u:Lcom/google/android/gms/ads/internal/g/m;

.field private final v:Lcom/google/android/gms/ads/internal/g/m;

.field private final w:Lcom/google/android/gms/ads/internal/g/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/ads/internal/client/AdSizeParcel;Lcom/google/android/gms/ads/internal/o/a;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Landroid/view/View;Lcom/google/android/gms/ads/internal/i/a;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->a:Ljava/lang/Object;

    .line 109
    iput-boolean v1, p0, Lcom/google/android/gms/ads/internal/a/d;->n:Z

    .line 110
    iput-boolean v1, p0, Lcom/google/android/gms/ads/internal/a/d;->o:Z

    .line 112
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/a/d;->p:J

    .line 119
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->t:Ljava/util/HashSet;

    .line 353
    new-instance v0, Lcom/google/android/gms/ads/internal/a/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/a/k;-><init>(Lcom/google/android/gms/ads/internal/a/d;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->u:Lcom/google/android/gms/ads/internal/g/m;

    .line 362
    new-instance v0, Lcom/google/android/gms/ads/internal/a/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/a/l;-><init>(Lcom/google/android/gms/ads/internal/a/d;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->v:Lcom/google/android/gms/ads/internal/g/m;

    .line 372
    new-instance v0, Lcom/google/android/gms/ads/internal/a/m;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/a/m;-><init>(Lcom/google/android/gms/ads/internal/a/d;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->w:Lcom/google/android/gms/ads/internal/g/m;

    .line 125
    iput-object p5, p0, Lcom/google/android/gms/ads/internal/a/d;->g:Lcom/google/android/gms/ads/internal/i/a;

    .line 126
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->b:Ljava/lang/ref/WeakReference;

    .line 127
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p4}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->d:Ljava/lang/ref/WeakReference;

    .line 128
    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->c:Ljava/lang/ref/WeakReference;

    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/a/d;->q:Z

    .line 131
    new-instance v0, Lcom/google/android/gms/ads/internal/a/b;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p1, Lcom/google/android/gms/ads/internal/client/AdSizeParcel;->b:Ljava/lang/String;

    iget-object v4, p2, Lcom/google/android/gms/ads/internal/o/a;->j:Lorg/json/JSONObject;

    invoke-virtual {p2}, Lcom/google/android/gms/ads/internal/o/a;->a()Z

    move-result v5

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/ads/internal/a/b;-><init>(Ljava/lang/String;Lcom/google/android/gms/ads/internal/util/client/VersionInfoParcel;Ljava/lang/String;Lorg/json/JSONObject;Z)V

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->e:Lcom/google/android/gms/ads/internal/a/b;

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->g:Lcom/google/android/gms/ads/internal/i/a;

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/i/a;->a()Lcom/google/android/gms/ads/internal/util/a/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->h:Lcom/google/android/gms/ads/internal/util/a/a;

    .line 138
    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->j:Landroid/view/WindowManager;

    .line 140
    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->k:Landroid/os/PowerManager;

    .line 142
    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "keyguard"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->l:Landroid/app/KeyguardManager;

    .line 144
    invoke-virtual {p4}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->f:Landroid/content/Context;

    .line 146
    :try_start_0
    invoke-direct {p0, p4}, Lcom/google/android/gms/ads/internal/a/d;->a(Landroid/view/View;)Lorg/json/JSONObject;

    move-result-object v0

    .line 147
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->h:Lcom/google/android/gms/ads/internal/util/a/a;

    new-instance v2, Lcom/google/android/gms/ads/internal/a/e;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/ads/internal/a/e;-><init>(Lcom/google/android/gms/ads/internal/a/d;Lorg/json/JSONObject;)V

    new-instance v0, Lcom/google/android/gms/ads/internal/a/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/ads/internal/a/g;-><init>(Lcom/google/android/gms/ads/internal/a/d;)V

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/ads/internal/util/a/a;->a(Lcom/google/android/gms/ads/internal/util/a/e;Lcom/google/android/gms/ads/internal/util/a/b;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->h:Lcom/google/android/gms/ads/internal/util/a/a;

    new-instance v1, Lcom/google/android/gms/ads/internal/a/h;

    invoke-direct {v1, p0}, Lcom/google/android/gms/ads/internal/a/h;-><init>(Lcom/google/android/gms/ads/internal/a/d;)V

    new-instance v2, Lcom/google/android/gms/ads/internal/a/i;

    invoke-direct {v2, p0}, Lcom/google/android/gms/ads/internal/a/i;-><init>(Lcom/google/android/gms/ads/internal/a/d;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/ads/internal/util/a/a;->a(Lcom/google/android/gms/ads/internal/util/a/e;Lcom/google/android/gms/ads/internal/util/a/b;)V

    .line 182
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Tracking ad unit: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->e:Lcom/google/android/gms/ads/internal/a/b;

    iget-object v1, v1, Lcom/google/android/gms/ads/internal/a/b;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 183
    return-void

    .line 161
    :catch_0
    move-exception v0

    .line 165
    const-string v1, "Failure while processing active view data."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 166
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method private static a(ILandroid/util/DisplayMetrics;)I
    .locals 2

    .prologue
    .line 318
    iget v0, p1, Landroid/util/DisplayMetrics;->density:F

    .line 319
    int-to-float v1, p0

    div-float v0, v1, v0

    float-to-int v0, v0

    return v0
.end method

.method private a(Landroid/view/View;)Lorg/json/JSONObject;
    .locals 14

    .prologue
    .line 446
    const/16 v0, 0x13

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 449
    invoke-virtual {p1}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v0

    .line 461
    :goto_0
    const/4 v1, 0x2

    new-array v1, v1, [I

    .line 462
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 463
    invoke-virtual {p1, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    .line 464
    invoke-virtual {p1, v2}, Landroid/view/View;->getLocationInWindow([I)V

    .line 466
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    .line 468
    new-instance v3, Landroid/graphics/Rect;

    invoke-direct {v3}, Landroid/graphics/Rect;-><init>()V

    .line 469
    const/4 v4, 0x0

    aget v4, v1, v4

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 470
    const/4 v4, 0x1

    aget v1, v1, v4

    iput v1, v3, Landroid/graphics/Rect;->top:I

    .line 471
    iget v1, v3, Landroid/graphics/Rect;->left:I

    invoke-virtual {p1}, Landroid/view/View;->getWidth()I

    move-result v4

    add-int/2addr v1, v4

    iput v1, v3, Landroid/graphics/Rect;->right:I

    .line 472
    iget v1, v3, Landroid/graphics/Rect;->top:I

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v1, v4

    iput v1, v3, Landroid/graphics/Rect;->bottom:I

    .line 474
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 475
    iget-object v4, p0, Lcom/google/android/gms/ads/internal/a/d;->j:Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getWidth()I

    move-result v4

    iput v4, v1, Landroid/graphics/Rect;->right:I

    .line 476
    iget-object v4, p0, Lcom/google/android/gms/ads/internal/a/d;->j:Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/Display;->getHeight()I

    move-result v4

    iput v4, v1, Landroid/graphics/Rect;->bottom:I

    .line 478
    new-instance v4, Landroid/graphics/Rect;

    invoke-direct {v4}, Landroid/graphics/Rect;-><init>()V

    .line 479
    const/4 v5, 0x0

    invoke-virtual {p1, v4, v5}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;Landroid/graphics/Point;)Z

    move-result v5

    .line 481
    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    .line 482
    invoke-virtual {p1, v6}, Landroid/view/View;->getLocalVisibleRect(Landroid/graphics/Rect;)Z

    move-result v7

    .line 484
    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    .line 485
    invoke-virtual {p1, v8}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 486
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/a/d;->j()Lorg/json/JSONObject;

    move-result-object v9

    .line 487
    const-string v10, "windowVisibility"

    invoke-virtual {p1}, Landroid/view/View;->getWindowVisibility()I

    move-result v11

    invoke-virtual {v9, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "isStopped"

    iget-boolean v12, p0, Lcom/google/android/gms/ads/internal/a/d;->o:Z

    invoke-virtual {v10, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "isPaused"

    iget-boolean v12, p0, Lcom/google/android/gms/ads/internal/a/d;->n:Z

    invoke-virtual {v10, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "isAttachedToWindow"

    invoke-virtual {v10, v11, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v10, "viewBox"

    new-instance v11, Lorg/json/JSONObject;

    invoke-direct {v11}, Lorg/json/JSONObject;-><init>()V

    const-string v12, "top"

    iget v13, v1, Landroid/graphics/Rect;->top:I

    invoke-static {v13, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v11

    const-string v12, "bottom"

    iget v13, v1, Landroid/graphics/Rect;->bottom:I

    invoke-static {v13, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v11

    const-string v12, "left"

    iget v13, v1, Landroid/graphics/Rect;->left:I

    invoke-static {v13, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v13

    invoke-virtual {v11, v12, v13}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v11

    const-string v12, "right"

    iget v1, v1, Landroid/graphics/Rect;->right:I

    invoke-static {v1, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v1

    invoke-virtual {v11, v12, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v1

    invoke-virtual {v0, v10, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "adBox"

    new-instance v10, Lorg/json/JSONObject;

    invoke-direct {v10}, Lorg/json/JSONObject;-><init>()V

    const-string v11, "top"

    iget v12, v3, Landroid/graphics/Rect;->top:I

    invoke-static {v12, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v12

    invoke-virtual {v10, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "bottom"

    iget v12, v3, Landroid/graphics/Rect;->bottom:I

    invoke-static {v12, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v12

    invoke-virtual {v10, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "left"

    iget v12, v3, Landroid/graphics/Rect;->left:I

    invoke-static {v12, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v12

    invoke-virtual {v10, v11, v12}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v10

    const-string v11, "right"

    iget v3, v3, Landroid/graphics/Rect;->right:I

    invoke-static {v3, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v3

    invoke-virtual {v10, v11, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "globalVisibleBox"

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v10, "top"

    iget v11, v4, Landroid/graphics/Rect;->top:I

    invoke-static {v11, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v11

    invoke-virtual {v3, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v10, "bottom"

    iget v11, v4, Landroid/graphics/Rect;->bottom:I

    invoke-static {v11, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v11

    invoke-virtual {v3, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v10, "left"

    iget v11, v4, Landroid/graphics/Rect;->left:I

    invoke-static {v11, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v11

    invoke-virtual {v3, v10, v11}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v10, "right"

    iget v4, v4, Landroid/graphics/Rect;->right:I

    invoke-static {v4, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v4

    invoke-virtual {v3, v10, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "globalVisibleBoxVisible"

    invoke-virtual {v0, v1, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "localVisibleBox"

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "top"

    iget v5, v6, Landroid/graphics/Rect;->top:I

    invoke-static {v5, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "bottom"

    iget v5, v6, Landroid/graphics/Rect;->bottom:I

    invoke-static {v5, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "left"

    iget v5, v6, Landroid/graphics/Rect;->left:I

    invoke-static {v5, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "right"

    iget v5, v6, Landroid/graphics/Rect;->right:I

    invoke-static {v5, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "localVisibleBoxVisible"

    invoke-virtual {v0, v1, v7}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "hitBox"

    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    const-string v4, "top"

    iget v5, v8, Landroid/graphics/Rect;->top:I

    invoke-static {v5, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "bottom"

    iget v5, v8, Landroid/graphics/Rect;->bottom:I

    invoke-static {v5, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "left"

    iget v5, v8, Landroid/graphics/Rect;->left:I

    invoke-static {v5, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "right"

    iget v5, v8, Landroid/graphics/Rect;->right:I

    invoke-static {v5, v2}, Lcom/google/android/gms/ads/internal/a/d;->a(ILandroid/util/DisplayMetrics;)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;I)Lorg/json/JSONObject;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v0

    const-string v1, "screenDensity"

    iget v2, v2, Landroid/util/DisplayMetrics;->density:F

    float-to-double v2, v2

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "isVisible"

    invoke-virtual {p1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p1}, Landroid/view/View;->isShown()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->k:Landroid/os/PowerManager;

    invoke-virtual {v0}, Landroid/os/PowerManager;->isScreenOn()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->l:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->inKeyguardRestrictedInputMode()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/ads/internal/util/g;->a()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_0
    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 525
    return-object v9

    .line 450
    :cond_1
    const/16 v0, 0x12

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 451
    invoke-virtual {p1}, Landroid/view/View;->getWindowId()Landroid/view/WindowId;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getWindowVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_3

    :cond_2
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 456
    :cond_4
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-nez v0, :cond_5

    invoke-virtual {p1}, Landroid/view/View;->getWindowVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_6

    :cond_5
    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 487
    :cond_7
    const/4 v0, 0x0

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/ads/internal/a/d;)Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/a/d;->i:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/ads/internal/a/d;)Lcom/google/android/gms/ads/internal/a/b;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->e:Lcom/google/android/gms/ads/internal/a/b;

    return-object v0
.end method

.method private i()V
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->m:Lcom/google/android/gms/ads/internal/a/o;

    if-eqz v0, :cond_0

    .line 300
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->m:Lcom/google/android/gms/ads/internal/a/o;

    invoke-interface {v0, p0}, Lcom/google/android/gms/ads/internal/a/o;->a(Lcom/google/android/gms/ads/internal/a/d;)V

    .line 302
    :cond_0
    return-void
.end method

.method private j()Lorg/json/JSONObject;
    .locals 6

    .prologue
    .line 406
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 407
    const-string v1, "afmaVersion"

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/a/d;->e:Lcom/google/android/gms/ads/internal/a/b;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/a/b;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "activeViewJSON"

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/a/d;->e:Lcom/google/android/gms/ads/internal/a/b;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/a/b;->b:Lorg/json/JSONObject;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "timestamp"

    invoke-static {}, Lcom/google/android/gms/ads/internal/o/e;->a()Lcom/google/android/gms/ads/internal/o/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/ads/internal/o/e;->b()Lcom/google/android/gms/common/util/p;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "adFormat"

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/a/d;->e:Lcom/google/android/gms/ads/internal/a/b;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/a/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "hashCode"

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/a/d;->e:Lcom/google/android/gms/ads/internal/a/b;

    iget-object v3, v3, Lcom/google/android/gms/ads/internal/a/b;->c:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "isMraid"

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/a/d;->e:Lcom/google/android/gms/ads/internal/a/b;

    iget-boolean v3, v3, Lcom/google/android/gms/ads/internal/a/b;->e:Z

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 413
    return-object v0
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .prologue
    .line 200
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 201
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->s:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 202
    monitor-exit v1

    .line 214
    :goto_0
    return-void

    .line 204
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 205
    const-string v2, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 206
    const-string v2, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 207
    new-instance v2, Lcom/google/android/gms/ads/internal/a/j;

    invoke-direct {v2, p0}, Lcom/google/android/gms/ads/internal/a/j;-><init>(Lcom/google/android/gms/ads/internal/a/d;)V

    iput-object v2, p0, Lcom/google/android/gms/ads/internal/a/d;->s:Landroid/content/BroadcastReceiver;

    .line 213
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/a/d;->f:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/a/d;->s:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 214
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/ads/internal/a/a;)V
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->t:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 187
    return-void
.end method

.method public final a(Lcom/google/android/gms/ads/internal/a/o;)V
    .locals 2

    .prologue
    .line 553
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 554
    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/ads/internal/a/d;->m:Lcom/google/android/gms/ads/internal/a/o;

    .line 555
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final a(Lcom/google/android/gms/ads/internal/a/p;)V
    .locals 2

    .prologue
    .line 346
    const-string v0, "/updateActiveView"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->u:Lcom/google/android/gms/ads/internal/g/m;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/ads/internal/a/p;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 348
    const-string v0, "/untrackActiveViewUnit"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->v:Lcom/google/android/gms/ads/internal/g/m;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/ads/internal/a/p;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 350
    const-string v0, "/visibilityChanged"

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->w:Lcom/google/android/gms/ads/internal/g/m;

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/ads/internal/a/p;->a(Ljava/lang/String;Lcom/google/android/gms/ads/internal/g/m;)V

    .line 352
    return-void
.end method

.method protected final a(Lorg/json/JSONObject;)V
    .locals 3

    .prologue
    .line 418
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0}, Lorg/json/JSONArray;-><init>()V

    .line 419
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 420
    invoke-virtual {v0, p1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 421
    const-string v2, "units"

    invoke-virtual {v1, v2, v0}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 422
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->h:Lcom/google/android/gms/ads/internal/util/a/a;

    new-instance v2, Lcom/google/android/gms/ads/internal/a/n;

    invoke-direct {v2, p0, v1}, Lcom/google/android/gms/ads/internal/a/n;-><init>(Lcom/google/android/gms/ads/internal/a/d;Lorg/json/JSONObject;)V

    new-instance v1, Lcom/google/android/gms/ads/internal/a/f;

    invoke-direct {v1, p0}, Lcom/google/android/gms/ads/internal/a/f;-><init>(Lcom/google/android/gms/ads/internal/a/d;)V

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/ads/internal/util/a/a;->a(Lcom/google/android/gms/ads/internal/util/a/e;Lcom/google/android/gms/ads/internal/util/a/b;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 435
    :goto_0
    return-void

    .line 432
    :catch_0
    move-exception v0

    .line 433
    const-string v1, "Skipping active view message."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected final a(Z)V
    .locals 2

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->t:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/a/a;

    .line 195
    invoke-interface {v0, p1}, Lcom/google/android/gms/ads/internal/a/a;->a(Z)V

    goto :goto_0

    .line 197
    :cond_0
    return-void
.end method

.method protected final a(Ljava/util/Map;)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 338
    if-nez p1, :cond_0

    move v0, v1

    .line 342
    :goto_0
    return v0

    .line 341
    :cond_0
    const-string v0, "hashCode"

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 342
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/a/d;->e:Lcom/google/android/gms/ads/internal/a/b;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/a/b;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method protected final b()V
    .locals 4

    .prologue
    .line 232
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 233
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewTreeObserver;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/ViewTreeObserver;->isAlive()Z

    move-result v2

    if-nez v2, :cond_2

    .line 234
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/a/d;->a:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->s:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->f:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/ads/internal/a/d;->s:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v3}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->s:Landroid/content/BroadcastReceiver;

    :cond_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 235
    const/4 v0, 0x0

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/a/d;->q:Z

    .line 236
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/a/d;->i()V

    .line 237
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->g:Lcom/google/android/gms/ads/internal/i/a;

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/a/d;->h:Lcom/google/android/gms/ads/internal/util/a/a;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/ads/internal/i/a;->a(Lcom/google/android/gms/ads/internal/util/a/a;)V

    .line 238
    monitor-exit v1

    return-void

    .line 233
    :cond_2
    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 234
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0
.end method

.method protected final b(Z)V
    .locals 8

    .prologue
    .line 263
    iget-object v2, p0, Lcom/google/android/gms/ads/internal/a/d;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 264
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/a/d;->i:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/a/d;->q:Z

    if-nez v0, :cond_1

    .line 265
    :cond_0
    monitor-exit v2

    .line 295
    :goto_0
    return-void

    .line 268
    :cond_1
    invoke-static {}, Lcom/google/android/gms/ads/internal/o/e;->a()Lcom/google/android/gms/ads/internal/o/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/ads/internal/o/e;->b()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v0

    .line 269
    if-eqz p1, :cond_2

    iget-wide v4, p0, Lcom/google/android/gms/ads/internal/a/d;->p:J

    const-wide/16 v6, 0xc8

    add-long/2addr v4, v6

    cmp-long v3, v4, v0

    if-lez v3, :cond_2

    .line 270
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 295
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 273
    :cond_2
    :try_start_1
    iput-wide v0, p0, Lcom/google/android/gms/ads/internal/a/d;->p:J

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/ads/internal/o/a;

    .line 276
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 277
    if-eqz v1, :cond_3

    if-nez v0, :cond_4

    :cond_3
    const/4 v0, 0x1

    .line 279
    :goto_1
    if-eqz v0, :cond_5

    .line 280
    invoke-virtual {p0}, Lcom/google/android/gms/ads/internal/a/d;->c()V

    .line 281
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 277
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 285
    :cond_5
    :try_start_2
    invoke-direct {p0, v1}, Lcom/google/android/gms/ads/internal/a/d;->a(Landroid/view/View;)Lorg/json/JSONObject;

    move-result-object v0

    .line 286
    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/a/d;->a(Lorg/json/JSONObject;)V
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 293
    :goto_2
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/ads/internal/a/d;->d:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    if-eqz v0, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/ViewTreeObserver;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    if-eq v0, v1, :cond_6

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    .line 294
    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/a/d;->i()V

    .line 295
    monitor-exit v2

    goto :goto_0

    .line 287
    :catch_0
    move-exception v0

    .line 288
    const-string v1, "Active view update failed."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 289
    :catch_1
    move-exception v0

    .line 290
    const-string v1, "Active view update failed."

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_2
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 242
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 243
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/a/d;->q:Z

    if-eqz v0, :cond_0

    .line 244
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/a/d;->r:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 246
    :try_start_1
    invoke-direct {p0}, Lcom/google/android/gms/ads/internal/a/d;->j()Lorg/json/JSONObject;

    move-result-object v0

    const-string v2, "doneReasonCode"

    const-string v3, "u"

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 247
    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/a/d;->a(Lorg/json/JSONObject;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 253
    :goto_0
    :try_start_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Untracking ad unit: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/ads/internal/a/d;->e:Lcom/google/android/gms/ads/internal/a/b;

    iget-object v2, v2, Lcom/google/android/gms/ads/internal/a/b;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 255
    :cond_0
    monitor-exit v1

    return-void

    .line 248
    :catch_0
    move-exception v0

    .line 249
    const-string v2, "JSON failure while processing active view data."

    invoke-static {v2, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 255
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 250
    :catch_1
    move-exception v0

    .line 251
    :try_start_3
    const-string v2, "Failure while processing active view data."

    invoke-static {v2, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method protected final d()V
    .locals 1

    .prologue
    .line 259
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/a/d;->b(Z)V

    .line 260
    return-void
.end method

.method public final e()Z
    .locals 2

    .prologue
    .line 305
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 306
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/internal/a/d;->q:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 307
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 562
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 563
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/a/d;->o:Z

    .line 564
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/a/d;->b(Z)V

    .line 565
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 572
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 573
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/a/d;->n:Z

    .line 574
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/a/d;->b(Z)V

    .line 575
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final h()V
    .locals 2

    .prologue
    .line 582
    iget-object v1, p0, Lcom/google/android/gms/ads/internal/a/d;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 583
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/ads/internal/a/d;->n:Z

    .line 584
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/a/d;->b(Z)V

    .line 586
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onGlobalLayout()V
    .locals 1

    .prologue
    .line 549
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/a/d;->b(Z)V

    .line 550
    return-void
.end method

.method public final onScrollChanged()V
    .locals 1

    .prologue
    .line 544
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/ads/internal/a/d;->b(Z)V

    .line 545
    return-void
.end method
