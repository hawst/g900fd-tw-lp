.class public final Lcom/google/android/gms/identity/accounts/api/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static final a:Lcom/google/android/gms/identity/accounts/api/e;

.field private static final b:Lcom/google/android/gms/identity/accounts/api/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 65
    new-instance v0, Lcom/google/android/gms/identity/accounts/api/c;

    invoke-direct {v0}, Lcom/google/android/gms/identity/accounts/api/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/identity/accounts/api/b;->a:Lcom/google/android/gms/identity/accounts/api/e;

    .line 85
    new-instance v0, Lcom/google/android/gms/identity/accounts/api/d;

    sget-object v1, Lcom/google/android/gms/identity/accounts/api/b;->a:Lcom/google/android/gms/identity/accounts/api/e;

    invoke-direct {v0, v1}, Lcom/google/android/gms/identity/accounts/api/d;-><init>(Lcom/google/android/gms/identity/accounts/api/e;)V

    sput-object v0, Lcom/google/android/gms/identity/accounts/api/b;->b:Lcom/google/android/gms/identity/accounts/api/d;

    return-void
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;Lcom/google/android/gms/identity/accounts/api/AccountData;)Z
    .locals 3

    .prologue
    .line 109
    sget-object v1, Lcom/google/android/gms/identity/accounts/api/b;->b:Lcom/google/android/gms/identity/accounts/api/d;

    const-string v0, "Context must not be null."

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Intent must not be null."

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "Account data must not be null."

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, v1, Lcom/google/android/gms/identity/accounts/api/d;->a:Lcom/google/android/gms/identity/accounts/api/e;

    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/identity/accounts/api/e;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "com.google.android.gms.accounts.ACCOUNT_DATA"

    invoke-static {p2, p1, v0}, Lcom/google/android/gms/common/internal/safeparcel/d;->a(Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;Landroid/content/Intent;Ljava/lang/String;)V

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
