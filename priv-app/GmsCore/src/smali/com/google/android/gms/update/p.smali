.class final Lcom/google/android/gms/update/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 42
    const-string v0, "SystemUpdateClient"

    const-string v1, "Connected to SystemUpdateService."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    monitor-enter p0

    .line 44
    :try_start_0
    invoke-static {p2}, Lcom/google/android/gms/update/f;->a(Landroid/os/IBinder;)Lcom/google/android/gms/update/e;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/update/o;->a(Lcom/google/android/gms/update/e;)Lcom/google/android/gms/update/e;

    .line 45
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 46
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    invoke-static {}, Lcom/google/android/gms/update/o;->g()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.update.STATUS_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 49
    return-void

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 53
    const-string v0, "SystemUpdateClient"

    const-string v1, "Unexpectedly disconnected from SystemUpdateService."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    monitor-enter p0

    .line 55
    const/4 v0, 0x0

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/update/o;->a(Lcom/google/android/gms/update/e;)Lcom/google/android/gms/update/e;

    .line 56
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
