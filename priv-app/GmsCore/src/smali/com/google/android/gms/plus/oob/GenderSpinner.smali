.class public Lcom/google/android/gms/plus/oob/GenderSpinner;
.super Landroid/widget/Spinner;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/plus/oob/t;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 32
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/plus/oob/t;)V
    .locals 0

    .prologue
    .line 50
    iput-object p1, p0, Lcom/google/android/gms/plus/oob/GenderSpinner;->a:Lcom/google/android/gms/plus/oob/t;

    .line 51
    return-void
.end method

.method public setSelection(I)V
    .locals 1

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/widget/Spinner;->setSelection(I)V

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/GenderSpinner;->a:Lcom/google/android/gms/plus/oob/t;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/plus/oob/GenderSpinner;->a:Lcom/google/android/gms/plus/oob/t;

    invoke-interface {v0, p1}, Lcom/google/android/gms/plus/oob/t;->b(I)V

    .line 47
    :cond_0
    return-void
.end method
