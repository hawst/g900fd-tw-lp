.class public final Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;
.super Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;
.source "SourceFile"


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/az;

.field private static final i:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

.field f:Ljava/lang/String;

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 4706
    new-instance v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/az;

    invoke-direct {v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/az;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/az;

    .line 4755
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 4758
    sput-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->i:Ljava/util/HashMap;

    const-string v1, "formattedProtocol"

    const-string v2, "formattedProtocol"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4759
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->i:Ljava/util/HashMap;

    const-string v1, "formattedType"

    const-string v2, "formattedType"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4760
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->i:Ljava/util/HashMap;

    const-string v1, "metadata"

    const-string v2, "metadata"

    const/4 v3, 0x4

    const-class v4, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4763
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->i:Ljava/util/HashMap;

    const-string v1, "protocol"

    const-string v2, "protocol"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4764
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->i:Ljava/util/HashMap;

    const-string v1, "type"

    const-string v2, "type"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4765
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->i:Ljava/util/HashMap;

    const-string v1, "value"

    const-string v2, "value"

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 4766
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4835
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 4836
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->b:I

    .line 4837
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->a:Ljava/util/Set;

    .line 4838
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 4850
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastSafeParcelableJsonResponse;-><init>()V

    .line 4851
    iput-object p1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->a:Ljava/util/Set;

    .line 4852
    iput p2, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->b:I

    .line 4853
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->c:Ljava/lang/String;

    .line 4854
    iput-object p4, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->d:Ljava/lang/String;

    .line 4855
    iput-object p5, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->e:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    .line 4856
    iput-object p6, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->f:Ljava/lang/String;

    .line 4857
    iput-object p7, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->g:Ljava/lang/String;

    .line 4858
    iput-object p8, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->h:Ljava/lang/String;

    .line 4859
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 4770
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->i:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 5066
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 5067
    packed-switch v0, :pswitch_data_0

    .line 5072
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 5069
    :pswitch_0
    check-cast p3, Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->e:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    .line 5076
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5077
    return-void

    .line 5067
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 5039
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 5040
    packed-switch v0, :pswitch_data_0

    .line 5057
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 5042
    :pswitch_1
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->c:Ljava/lang/String;

    .line 5060
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 5061
    return-void

    .line 5045
    :pswitch_2
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->d:Ljava/lang/String;

    goto :goto_0

    .line 5048
    :pswitch_3
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->f:Ljava/lang/String;

    goto :goto_0

    .line 5051
    :pswitch_4
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->g:Ljava/lang/String;

    goto :goto_0

    .line 5054
    :pswitch_5
    iput-object p3, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->h:Ljava/lang/String;

    goto :goto_0

    .line 5040
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 5000
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 5005
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 5019
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 5007
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->c:Ljava/lang/String;

    .line 5017
    :goto_0
    return-object v0

    .line 5009
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->d:Ljava/lang/String;

    goto :goto_0

    .line 5011
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->e:Lcom/google/android/gms/plus/service/v2whitelisted/models/Mergedpeoplemetadata;

    goto :goto_0

    .line 5013
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->f:Ljava/lang/String;

    goto :goto_0

    .line 5015
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->g:Ljava/lang/String;

    goto :goto_0

    .line 5017
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->h:Ljava/lang/String;

    goto :goto_0

    .line 5005
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 4990
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/az;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 5094
    instance-of v0, p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;

    if-nez v0, :cond_0

    move v0, v1

    .line 5125
    :goto_0
    return v0

    .line 5099
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 5100
    goto :goto_0

    .line 5103
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;

    .line 5104
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 5105
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 5106
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 5108
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 5110
    goto :goto_0

    :cond_3
    move v0, v1

    .line 5115
    goto :goto_0

    .line 5118
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 5120
    goto :goto_0

    :cond_5
    move v0, v2

    .line 5125
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 5081
    const/4 v0, 0x0

    .line 5082
    sget-object v1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->i:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 5083
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 5084
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 5085
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 5087
    goto :goto_0

    .line 5088
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 4995
    sget-object v0, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;->CREATOR:Lcom/google/android/gms/plus/service/v2whitelisted/models/az;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/plus/service/v2whitelisted/models/az;->a(Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$InstantMessaging;Landroid/os/Parcel;I)V

    .line 4996
    return-void
.end method
