.class final Lcom/google/android/gms/fitness/sensors/g/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/fitness/sensors/g/j;


# direct methods
.method constructor <init>(Lcom/google/android/gms/fitness/sensors/g/j;)V
    .locals 0

    .prologue
    .line 258
    iput-object p1, p0, Lcom/google/android/gms/fitness/sensors/g/k;->a:Lcom/google/android/gms/fitness/sensors/g/j;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 258
    check-cast p1, Lcom/google/android/gms/wearable/u;

    invoke-interface {p1}, Lcom/google/android/gms/wearable/u;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-le v1, v2, :cond_0

    const-string v1, "There must be only one connected node, but was %d"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Lcom/google/android/gms/fitness/m/a;->f(Ljava/lang/String;[Ljava/lang/Object;)I

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/sensors/g/k;->a:Lcom/google/android/gms/fitness/sensors/g/j;

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wearable/s;

    invoke-static {v1, v0}, Lcom/google/android/gms/fitness/sensors/g/j;->a(Lcom/google/android/gms/fitness/sensors/g/j;Lcom/google/android/gms/wearable/s;)Lcom/google/android/gms/wearable/s;

    :cond_1
    return-void
.end method
