.class final Lcom/google/android/gms/drive/realtime/cache/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/a/a/g;


# instance fields
.field final a:Ljava/util/concurrent/CountDownLatch;

.field b:Z

.field c:Lcom/google/c/a/a/b/e/o;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    .line 261
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 262
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/v;->a:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 261
    invoke-direct {p0}, Lcom/google/android/gms/drive/realtime/cache/v;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 268
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/v;->b:Z

    .line 269
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/v;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 270
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/e/o;)V
    .locals 1

    .prologue
    .line 274
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/cache/v;->c:Lcom/google/c/a/a/b/e/o;

    .line 275
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/realtime/cache/v;->b:Z

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/cache/v;->a:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 277
    return-void
.end method
