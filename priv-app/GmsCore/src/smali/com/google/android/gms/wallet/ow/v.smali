.class public final Lcom/google/android/gms/wallet/ow/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field a:Landroid/widget/AdapterView;

.field b:Lcom/google/android/gms/wallet/ow/u;

.field private c:Landroid/content/Context;

.field private d:I

.field private e:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/AdapterView;Landroid/util/AttributeSet;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput v0, p0, Lcom/google/android/gms/wallet/ow/v;->d:I

    .line 37
    iput v0, p0, Lcom/google/android/gms/wallet/ow/v;->e:I

    .line 50
    iput-object p1, p0, Lcom/google/android/gms/wallet/ow/v;->c:Landroid/content/Context;

    .line 51
    iput-object p2, p0, Lcom/google/android/gms/wallet/ow/v;->a:Landroid/widget/AdapterView;

    .line 53
    sget-object v0, Lcom/google/android/gms/r;->br:[I

    invoke-virtual {p1, p3, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    sget v1, Lcom/google/android/gms/r;->bt:I

    sget v2, Lcom/google/android/gms/l;->hf:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/ow/v;->d:I

    sget v1, Lcom/google/android/gms/r;->bs:I

    sget v2, Lcom/google/android/gms/l;->hg:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/wallet/ow/v;->e:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/wallet/ow/v;)I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/google/android/gms/wallet/ow/v;->d:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/wallet/ow/v;)I
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/google/android/gms/wallet/ow/v;->e:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/wallet/ow/v;)Landroid/widget/AdapterView;
    .locals 1

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/v;->a:Landroid/widget/AdapterView;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/aa/b/a/h;)V
    .locals 3

    .prologue
    .line 90
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/v;->a:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/wallet/ow/w;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/ow/w;->a(Lcom/google/aa/b/a/h;)I

    move-result v1

    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/v;->a:Landroid/widget/AdapterView;

    instance-of v0, v0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/v;->a:Landroid/widget/AdapterView;

    check-cast v0, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/wallet/ow/LoyaltyWobSelectorExpander;->a(IZ)V

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 90
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/v;->a:Landroid/widget/AdapterView;

    invoke-virtual {v0, v1}, Landroid/widget/AdapterView;->setSelection(I)V

    goto :goto_0
.end method

.method public final a([Lcom/google/aa/b/a/h;)V
    .locals 2

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/v;->a:Landroid/widget/AdapterView;

    invoke-virtual {v0, p0}, Landroid/widget/AdapterView;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 86
    new-instance v0, Lcom/google/android/gms/wallet/ow/w;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/v;->c:Landroid/content/Context;

    invoke-direct {v0, p0, v1, p1}, Lcom/google/android/gms/wallet/ow/w;-><init>(Lcom/google/android/gms/wallet/ow/v;Landroid/content/Context;[Lcom/google/aa/b/a/h;)V

    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/v;->a:Landroid/widget/AdapterView;

    invoke-virtual {v1, v0}, Landroid/widget/AdapterView;->setAdapter(Landroid/widget/Adapter;)V

    .line 87
    return-void
.end method

.method public final onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/v;->b:Lcom/google/android/gms/wallet/ow/u;

    if-eqz v0, :cond_0

    .line 204
    iget-object v1, p0, Lcom/google/android/gms/wallet/ow/v;->b:Lcom/google/android/gms/wallet/ow/u;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/v;->a:Landroid/widget/AdapterView;

    invoke-virtual {v0}, Landroid/widget/AdapterView;->getAdapter()Landroid/widget/Adapter;

    move-result-object v0

    invoke-interface {v0, p3}, Landroid/widget/Adapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/aa/b/a/h;

    invoke-interface {v1, v0}, Lcom/google/android/gms/wallet/ow/u;->a(Lcom/google/aa/b/a/h;)V

    .line 206
    :cond_0
    return-void
.end method

.method public final onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 2

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/v;->b:Lcom/google/android/gms/wallet/ow/u;

    if-eqz v0, :cond_0

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/wallet/ow/v;->b:Lcom/google/android/gms/wallet/ow/u;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/wallet/ow/u;->a(Lcom/google/aa/b/a/h;)V

    .line 213
    :cond_0
    return-void
.end method
