.class public final Lcom/google/android/gms/auth/f/c;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Z

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:J

.field private g:Z

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1626
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 1631
    iput-boolean v2, p0, Lcom/google/android/gms/auth/f/c;->b:Z

    .line 1648
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/auth/f/c;->d:Ljava/lang/String;

    .line 1665
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/auth/f/c;->f:J

    .line 1682
    iput v2, p0, Lcom/google/android/gms/auth/f/c;->h:I

    .line 1726
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/auth/f/c;->i:I

    .line 1626
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1729
    iget v0, p0, Lcom/google/android/gms/auth/f/c;->i:I

    if-gez v0, :cond_0

    .line 1731
    invoke-virtual {p0}, Lcom/google/android/gms/auth/f/c;->b()I

    .line 1733
    :cond_0
    iget v0, p0, Lcom/google/android/gms/auth/f/c;->i:I

    return v0
.end method

.method public final a(I)Lcom/google/android/gms/auth/f/c;
    .locals 1

    .prologue
    .line 1686
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/c;->g:Z

    .line 1687
    iput p1, p0, Lcom/google/android/gms/auth/f/c;->h:I

    .line 1688
    return-object p0
.end method

.method public final a(J)Lcom/google/android/gms/auth/f/c;
    .locals 1

    .prologue
    .line 1669
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/c;->e:Z

    .line 1670
    iput-wide p1, p0, Lcom/google/android/gms/auth/f/c;->f:J

    .line 1671
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/auth/f/c;
    .locals 1

    .prologue
    .line 1652
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/c;->c:Z

    .line 1653
    iput-object p1, p0, Lcom/google/android/gms/auth/f/c;->d:Ljava/lang/String;

    .line 1654
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/auth/f/c;
    .locals 1

    .prologue
    .line 1635
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/f/c;->a:Z

    .line 1636
    iput-boolean p1, p0, Lcom/google/android/gms/auth/f/c;->b:Z

    .line 1637
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 1623
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/c;->a(Z)Lcom/google/android/gms/auth/f/c;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/c;->a(Ljava/lang/String;)Lcom/google/android/gms/auth/f/c;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/f/c;->a(J)Lcom/google/android/gms/auth/f/c;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/f/c;->a(I)Lcom/google/android/gms/auth/f/c;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 1712
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/c;->a:Z

    if-eqz v0, :cond_0

    .line 1713
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/c;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 1715
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/c;->c:Z

    if-eqz v0, :cond_1

    .line 1716
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/auth/f/c;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 1718
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/c;->e:Z

    if-eqz v0, :cond_2

    .line 1719
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/auth/f/c;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 1721
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/auth/f/c;->g:Z

    if-eqz v0, :cond_3

    .line 1722
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/auth/f/c;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 1724
    :cond_3
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 1738
    const/4 v0, 0x0

    .line 1739
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/c;->a:Z

    if-eqz v1, :cond_0

    .line 1740
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/c;->b:Z

    invoke-static {v0}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 1743
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/c;->c:Z

    if-eqz v1, :cond_1

    .line 1744
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/auth/f/c;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1747
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/c;->e:Z

    if-eqz v1, :cond_2

    .line 1748
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/auth/f/c;->f:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 1751
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/auth/f/c;->g:Z

    if-eqz v1, :cond_3

    .line 1752
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/auth/f/c;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1755
    :cond_3
    iput v0, p0, Lcom/google/android/gms/auth/f/c;->i:I

    .line 1756
    return v0
.end method
