.class final Lcom/google/android/gms/wallet/common/ui/dd;
.super Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/wallet/common/ui/dc;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/wallet/common/ui/dc;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 411
    invoke-direct {p0, p2}, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;-><init>(Landroid/content/Context;)V

    .line 412
    iput-object p1, p0, Lcom/google/android/gms/wallet/common/ui/dd;->a:Lcom/google/android/gms/wallet/common/ui/dc;

    .line 413
    return-void
.end method

.method private varargs a([Ljava/lang/String;)Landroid/util/Pair;
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 424
    new-instance v2, Lcom/google/android/apps/common/a/a/i;

    const-string v0, "get_auth_tokens"

    invoke-direct {v2, v0}, Lcom/google/android/apps/common/a/a/i;-><init>(Ljava/lang/String;)V

    .line 425
    invoke-virtual {v2}, Lcom/google/android/apps/common/a/a/i;->a()Lcom/google/android/apps/common/a/a/h;

    move-result-object v3

    .line 430
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dd;->b:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/wallet/common/ui/dd;->a:Lcom/google/android/gms/wallet/common/ui/dc;

    iget-object v4, v4, Lcom/google/android/gms/wallet/common/ui/dc;->a:Landroid/accounts/Account;

    iget-object v4, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v5, 0x0

    aget-object v5, p1, v5

    invoke-static {v0, v4, v5}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 436
    :goto_0
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const-string v5, "get_auth_token"

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/android/apps/common/a/a/i;->a(Lcom/google/android/apps/common/a/a/h;[Ljava/lang/String;)Z

    .line 437
    iget-object v3, p0, Lcom/google/android/gms/wallet/common/ui/dd;->b:Landroid/content/Context;

    invoke-static {v3, v2}, Lcom/google/android/gms/wallet/common/e;->a(Landroid/content/Context;Lcom/google/android/apps/common/a/a/i;)V

    .line 439
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0

    .line 432
    :catch_0
    move-exception v0

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    goto :goto_0
.end method


# virtual methods
.method protected final bridge synthetic a([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 404
    check-cast p1, [Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/common/ui/dd;->a([Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 404
    check-cast p1, Landroid/util/Pair;

    invoke-super {p0, p1}, Lcom/google/android/gms/wallet/shared/common/ExceptionHandlingAsyncTask;->onPostExecute(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dd;->a:Lcom/google/android/gms/wallet/common/ui/dc;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/wallet/common/ui/dc;->b:Lcom/google/android/gms/wallet/common/ui/dd;

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dd;->a:Lcom/google/android/gms/wallet/common/ui/dc;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dc;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-eqz v0, :cond_3

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Exception;

    instance-of v1, v0, Ljava/io/IOException;

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dd;->a:Lcom/google/android/gms/wallet/common/ui/dc;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dc;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v1, v0, Lcom/google/android/gms/auth/ae;

    if-eqz v1, :cond_2

    check-cast v0, Lcom/google/android/gms/auth/ae;

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dd;->a:Lcom/google/android/gms/wallet/common/ui/dc;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/ae;->b()Landroid/content/Intent;

    move-result-object v0

    const/16 v2, 0x1f5

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/wallet/common/ui/dc;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    const-string v1, "RetrieveAuthTokensFragment"

    const-string v2, "Authentication error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dd;->a:Lcom/google/android/gms/wallet/common/ui/dc;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dc;->b()V

    goto :goto_0

    :cond_3
    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/wallet/common/ui/dd;->a:Lcom/google/android/gms/wallet/common/ui/dc;

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/wallet/common/ui/dc;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    const-string v0, "RetrieveAuthTokensFragment"

    const-string v1, "Unknown authentication error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/wallet/common/ui/dd;->a:Lcom/google/android/gms/wallet/common/ui/dc;

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/common/ui/dc;->b()V

    goto :goto_0
.end method
