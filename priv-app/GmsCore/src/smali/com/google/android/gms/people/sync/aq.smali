.class final Lcom/google/android/gms/people/sync/aq;
.super Lcom/google/android/gms/people/sync/bb;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/people/sync/ap;


# direct methods
.method constructor <init>(Lcom/google/android/gms/people/sync/ap;Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Map;Z)V
    .locals 0

    .prologue
    .line 2476
    iput-object p1, p0, Lcom/google/android/gms/people/sync/aq;->a:Lcom/google/android/gms/people/sync/ap;

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/gms/people/sync/bb;-><init>(Lcom/google/android/gms/people/sync/j;Lcom/google/android/gms/plus/service/v2whitelisted/models/Person;Ljava/util/Map;Z)V

    return-void
.end method


# virtual methods
.method public final synthetic a(Ljava/lang/Object;ILjava/lang/Object;)V
    .locals 2

    .prologue
    .line 2476
    check-cast p1, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;

    check-cast p3, Landroid/content/ContentValues;

    const-string v0, "display_name"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "formatted_name"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "given_name"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "family_name"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "middle_name"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "honorific_suffix"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "honorific_prefix"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "yomi_given_name"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->n()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "yomi_family_name"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->m()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "yomi_honorific_suffix"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->p()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "yomi_honorific_prefix"

    invoke-virtual {p1}, Lcom/google/android/gms/plus/service/v2whitelisted/models/Person$Names;->o()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
