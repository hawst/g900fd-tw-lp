.class final Lcom/google/android/gms/audiomodem/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/audiomodem/ad;


# direct methods
.method constructor <init>(Lcom/google/android/gms/audiomodem/ad;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/gms/audiomodem/ae;->a:Lcom/google/android/gms/audiomodem/ad;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 2

    .prologue
    .line 52
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ae;->a:Lcom/google/android/gms/audiomodem/ad;

    iget-object v1, v0, Lcom/google/android/gms/audiomodem/ad;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 54
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ae;->a:Lcom/google/android/gms/audiomodem/ad;

    check-cast p2, Landroid/bluetooth/BluetoothHeadset;

    iput-object p2, v0, Lcom/google/android/gms/audiomodem/ad;->b:Landroid/bluetooth/BluetoothHeadset;

    .line 55
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    :cond_0
    return-void

    .line 55
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onServiceDisconnected(I)V
    .locals 3

    .prologue
    .line 43
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ae;->a:Lcom/google/android/gms/audiomodem/ad;

    iget-object v1, v0, Lcom/google/android/gms/audiomodem/ad;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 45
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/audiomodem/ae;->a:Lcom/google/android/gms/audiomodem/ad;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/gms/audiomodem/ad;->b:Landroid/bluetooth/BluetoothHeadset;

    .line 46
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 48
    :cond_0
    return-void

    .line 46
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
