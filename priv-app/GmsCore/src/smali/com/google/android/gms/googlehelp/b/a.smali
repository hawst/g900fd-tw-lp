.class public final Lcom/google/android/gms/googlehelp/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static c:Lcom/google/android/gms/common/a/d;

.field public static d:Lcom/google/android/gms/common/a/d;

.field public static e:Lcom/google/android/gms/common/a/d;

.field public static f:Lcom/google/android/gms/common/a/d;

.field public static g:Lcom/google/android/gms/common/a/d;

.field public static h:Lcom/google/android/gms/common/a/d;

.field public static i:Lcom/google/android/gms/common/a/d;

.field public static j:Lcom/google/android/gms/common/a/d;

.field public static k:Lcom/google/android/gms/common/a/d;

.field public static l:Lcom/google/android/gms/common/a/d;

.field public static m:Lcom/google/android/gms/common/a/d;

.field public static n:Lcom/google/android/gms/common/a/d;

.field public static o:Lcom/google/android/gms/common/a/d;

.field public static p:Lcom/google/android/gms/common/a/d;

.field public static q:Lcom/google/android/gms/common/a/d;

.field public static r:Lcom/google/android/gms/common/a/d;

.field public static s:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 15
    const-string v0, "gms:googlehelp:http_scheme"

    const-string v1, "https"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->a:Lcom/google/android/gms/common/a/d;

    .line 18
    const-string v0, "gms:googlehelp:http_authority"

    const-string v1, "www.google.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->b:Lcom/google/android/gms/common/a/d;

    .line 21
    const-string v0, "gms:googlehelp:click_to_call_path"

    const-string v1, "/tools/feedback/mobile/__clicktocall"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->c:Lcom/google/android/gms/common/a/d;

    .line 25
    const-string v0, "gms:googlehelp:get_configurations_path"

    const-string v1, "/tools/feedback/mobile/get-configurations"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->d:Lcom/google/android/gms/common/a/d;

    .line 29
    const-string v0, "gms:googlehelp:escalation_options_request_path"

    const-string v1, "/tools/feedback/mobile/escalation-options-post"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->e:Lcom/google/android/gms/common/a/d;

    .line 33
    const-string v0, "gms:googlehelp:get_query_suggestions_path"

    const-string v1, "/complete/search"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->f:Lcom/google/android/gms/common/a/d;

    .line 37
    const-string v0, "gms:googlehelp:recommendations_request_path"

    const-string v1, "/tools/feedback/mobile/__contextual"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->g:Lcom/google/android/gms/common/a/d;

    .line 41
    const-string v0, "gms:googlehelp:report_metrics_request_path"

    const-string v1, "/tools/feedback/metric/report"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->h:Lcom/google/android/gms/common/a/d;

    .line 45
    const-string v0, "gms:googlehelp:submit_contact_form_request_path"

    const-string v1, "/tools/feedback/mobile/__contact-form"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->i:Lcom/google/android/gms/common/a/d;

    .line 49
    const-string v0, "gms:googlehelp:video_call_request_path"

    const-string v1, "/tools/feedback/mobile/__video-call"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->j:Lcom/google/android/gms/common/a/d;

    .line 53
    const-string v0, "gms:googlehelp:chat_request_path"

    const-string v1, "/tools/feedback/mobile/__chat"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->k:Lcom/google/android/gms/common/a/d;

    .line 58
    const-string v0, "gms:googlehelp:base_url"

    const-string v1, "https://www.google.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->l:Lcom/google/android/gms/common/a/d;

    .line 61
    const-string v0, "gms:googlehelp:hangouts_validate_device_and_app_support"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->m:Lcom/google/android/gms/common/a/d;

    .line 65
    const-string v0, "gms:googlehelp:hangouts_total_num_retries"

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->n:Lcom/google/android/gms/common/a/d;

    .line 68
    const-string v0, "gms:googlehelp:video_call_update_delay_ms"

    const-wide/16 v2, 0x2710

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->o:Lcom/google/android/gms/common/a/d;

    .line 72
    const-string v0, "gms:googlehelp:video_call_update_delay_after_ready_ms"

    const-wide/16 v2, 0x7530

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->p:Lcom/google/android/gms/common/a/d;

    .line 76
    const-string v0, "gms:googlehelp:network_timeout_seconds"

    const-wide/16 v2, 0x14

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->q:Lcom/google/android/gms/common/a/d;

    .line 79
    const-string v0, "gms:googlehelp:ongoing_session_last_stopped_minutes"

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->r:Lcom/google/android/gms/common/a/d;

    .line 82
    const-string v0, "gms:googlehelp:use_gcm_for_hangouts_updates"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/googlehelp/b/a;->s:Lcom/google/android/gms/common/a/d;

    return-void
.end method
