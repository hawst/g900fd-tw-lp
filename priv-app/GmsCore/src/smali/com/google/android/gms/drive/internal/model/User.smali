.class public final Lcom/google/android/gms/drive/internal/model/User;
.super Lcom/google/android/gms/common/server/response/FastJsonResponse;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/drive/internal/model/z;

.field private static final h:Ljava/util/HashMap;


# instance fields
.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Z

.field f:Ljava/lang/String;

.field g:Lcom/google/android/gms/drive/internal/model/User$Picture;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/drive/internal/model/z;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/model/z;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/model/User;->CREATOR:Lcom/google/android/gms/drive/internal/model/z;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 77
    sput-object v0, Lcom/google/android/gms/drive/internal/model/User;->h:Ljava/util/HashMap;

    const-string v1, "displayName"

    const-string v2, "displayName"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/google/android/gms/drive/internal/model/User;->h:Ljava/util/HashMap;

    const-string v1, "emailAddress"

    const-string v2, "emailAddress"

    const/4 v3, 0x5

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/google/android/gms/drive/internal/model/User;->h:Ljava/util/HashMap;

    const-string v1, "isAuthenticatedUser"

    const-string v2, "isAuthenticatedUser"

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->e(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/google/android/gms/drive/internal/model/User;->h:Ljava/util/HashMap;

    const-string v1, "permissionId"

    const-string v2, "permissionId"

    const/16 v3, 0x8

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;I)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/google/android/gms/drive/internal/model/User;->h:Ljava/util/HashMap;

    const-string v1, "picture"

    const-string v2, "picture"

    const/16 v3, 0x9

    const-class v4, Lcom/google/android/gms/drive/internal/model/User$Picture;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 134
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/drive/internal/model/User;->b:I

    .line 135
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/model/User;->a:Ljava/util/Set;

    .line 136
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/gms/drive/internal/model/User$Picture;)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/FastJsonResponse;-><init>()V

    .line 148
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/model/User;->a:Ljava/util/Set;

    .line 149
    iput p2, p0, Lcom/google/android/gms/drive/internal/model/User;->b:I

    .line 150
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/User;->c:Ljava/lang/String;

    .line 151
    iput-object p4, p0, Lcom/google/android/gms/drive/internal/model/User;->d:Ljava/lang/String;

    .line 152
    iput-boolean p5, p0, Lcom/google/android/gms/drive/internal/model/User;->e:Z

    .line 153
    iput-object p6, p0, Lcom/google/android/gms/drive/internal/model/User;->f:Ljava/lang/String;

    .line 154
    iput-object p7, p0, Lcom/google/android/gms/drive/internal/model/User;->g:Lcom/google/android/gms/drive/internal/model/User$Picture;

    .line 155
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 88
    sget-object v0, Lcom/google/android/gms/drive/internal/model/User;->h:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 4

    .prologue
    .line 628
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 629
    packed-switch v0, :pswitch_data_0

    .line 634
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not a known custom type.  Found "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 631
    :pswitch_0
    check-cast p3, Lcom/google/android/gms/drive/internal/model/User$Picture;

    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/User;->g:Lcom/google/android/gms/drive/internal/model/User$Picture;

    .line 638
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/User;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 639
    return-void

    .line 629
    :pswitch_data_0
    .packed-switch 0x9
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 607
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 608
    sparse-switch v0, :sswitch_data_0

    .line 619
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a String."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 610
    :sswitch_0
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/User;->c:Ljava/lang/String;

    .line 622
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/User;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 623
    return-void

    .line 613
    :sswitch_1
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/User;->d:Ljava/lang/String;

    goto :goto_0

    .line 616
    :sswitch_2
    iput-object p3, p0, Lcom/google/android/gms/drive/internal/model/User;->f:Ljava/lang/String;

    goto :goto_0

    .line 608
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x5 -> :sswitch_1
        0x8 -> :sswitch_2
    .end sparse-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 592
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    .line 593
    packed-switch v0, :pswitch_data_0

    .line 598
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Field with id="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " is not known to be a boolean."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 595
    :pswitch_0
    iput-boolean p3, p0, Lcom/google/android/gms/drive/internal/model/User;->e:Z

    .line 601
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/model/User;->a:Ljava/util/Set;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 602
    return-void

    .line 593
    :pswitch_data_0
    .packed-switch 0x6
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z
    .locals 2

    .prologue
    .line 544
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/User;->a:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 549
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 561
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 551
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/User;->c:Ljava/lang/String;

    .line 559
    :goto_0
    return-object v0

    .line 553
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/User;->d:Ljava/lang/String;

    goto :goto_0

    .line 555
    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/User;->e:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    .line 557
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/User;->f:Ljava/lang/String;

    goto :goto_0

    .line 559
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/User;->g:Lcom/google/android/gms/drive/internal/model/User$Picture;

    goto :goto_0

    .line 549
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method protected final b(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 534
    const/4 v0, 0x0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/User;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/User;->d:Ljava/lang/String;

    return-object v0
.end method

.method protected final c(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 539
    const/4 v0, 0x0

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 212
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/model/User;->e:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 523
    sget-object v0, Lcom/google/android/gms/drive/internal/model/User;->CREATOR:Lcom/google/android/gms/drive/internal/model/z;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/User;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 656
    instance-of v0, p1, Lcom/google/android/gms/drive/internal/model/User;

    if-nez v0, :cond_0

    move v0, v1

    .line 687
    :goto_0
    return v0

    .line 661
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    .line 662
    goto :goto_0

    .line 665
    :cond_1
    check-cast p1, Lcom/google/android/gms/drive/internal/model/User;

    .line 666
    sget-object v0, Lcom/google/android/gms/drive/internal/model/User;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 667
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/User;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 668
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/User;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 670
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/User;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/User;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 672
    goto :goto_0

    :cond_3
    move v0, v1

    .line 677
    goto :goto_0

    .line 680
    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/internal/model/User;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    .line 682
    goto :goto_0

    :cond_5
    move v0, v2

    .line 687
    goto :goto_0
.end method

.method public final f()Lcom/google/android/gms/drive/internal/model/User$Picture;
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/model/User;->g:Lcom/google/android/gms/drive/internal/model/User$Picture;

    return-object v0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 643
    const/4 v0, 0x0

    .line 644
    sget-object v1, Lcom/google/android/gms/drive/internal/model/User;->h:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    .line 645
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/User;->a(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 646
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h()I

    move-result v3

    add-int/2addr v1, v3

    .line 647
    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/internal/model/User;->b(Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    .line 649
    goto :goto_0

    .line 650
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 528
    sget-object v0, Lcom/google/android/gms/drive/internal/model/User;->CREATOR:Lcom/google/android/gms/drive/internal/model/z;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/drive/internal/model/z;->a(Lcom/google/android/gms/drive/internal/model/User;Landroid/os/Parcel;I)V

    .line 529
    return-void
.end method
