.class public final Lcom/google/android/gms/plus/service/v1whitelisted/models/af;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

.field public final b:Ljava/util/Set;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 980
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 981
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->b:Ljava/util/Set;

    .line 982
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/service/v1whitelisted/models/ae;
    .locals 11

    .prologue
    .line 1076
    new-instance v0, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;

    iget-object v1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->b:Ljava/util/Set;

    iget-object v2, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->a:Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;

    iget-object v3, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity;

    iget-object v6, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->f:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->g:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->h:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->i:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->j:Ljava/lang/String;

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity;-><init>(Ljava/util/Set;Lcom/google/android/gms/plus/service/v1whitelisted/models/AclEntity;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ag;)Lcom/google/android/gms/plus/service/v1whitelisted/models/af;
    .locals 2

    .prologue
    .line 1018
    check-cast p1, Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity;

    iput-object p1, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActivityEntity$ObjectEntity;

    .line 1019
    iget-object v0, p0, Lcom/google/android/gms/plus/service/v1whitelisted/models/af;->b:Ljava/util/Set;

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1020
    return-object p0
.end method
