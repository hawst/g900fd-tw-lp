.class final Lcom/google/android/gms/people/profile/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/google/android/gms/people/profile/AvatarView;

.field private b:F

.field private c:F

.field private d:J

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/profile/AvatarView;)V
    .locals 2

    .prologue
    .line 1170
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1171
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/people/profile/k;->d:J

    .line 1172
    iput-object p1, p0, Lcom/google/android/gms/people/profile/k;->a:Lcom/google/android/gms/people/profile/AvatarView;

    .line 1173
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1195
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/k;->e:Z

    .line 1196
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/k;->f:Z

    .line 1197
    return-void
.end method

.method public final a(FF)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1179
    iget-boolean v2, p0, Lcom/google/android/gms/people/profile/k;->e:Z

    if-eqz v2, :cond_0

    .line 1188
    :goto_0
    return v0

    .line 1182
    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/gms/people/profile/k;->d:J

    .line 1183
    iput p1, p0, Lcom/google/android/gms/people/profile/k;->b:F

    .line 1184
    iput p2, p0, Lcom/google/android/gms/people/profile/k;->c:F

    .line 1185
    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/k;->f:Z

    .line 1186
    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/k;->e:Z

    .line 1187
    iget-object v0, p0, Lcom/google/android/gms/people/profile/k;->a:Lcom/google/android/gms/people/profile/AvatarView;

    const-wide/16 v2, 0xfa

    invoke-virtual {v0, p0, v2, v3}, Lcom/google/android/gms/people/profile/AvatarView;->postDelayed(Ljava/lang/Runnable;J)Z

    move v0, v1

    .line 1188
    goto :goto_0
.end method

.method public final run()V
    .locals 13

    .prologue
    const-wide/16 v10, -0x1

    const/high16 v8, 0x7fc00000    # NaNf

    const/high16 v7, 0x41200000    # 10.0f

    const/high16 v6, 0x42c80000    # 100.0f

    const/4 v1, 0x0

    .line 1202
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/k;->f:Z

    if-eqz v0, :cond_1

    .line 1243
    :cond_0
    :goto_0
    return-void

    .line 1207
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1208
    iget-wide v4, p0, Lcom/google/android/gms/people/profile/k;->d:J

    cmp-long v0, v4, v10

    if-eqz v0, :cond_5

    iget-wide v4, p0, Lcom/google/android/gms/people/profile/k;->d:J

    sub-long v4, v2, v4

    long-to-float v0, v4

    .line 1210
    :goto_1
    iget-wide v4, p0, Lcom/google/android/gms/people/profile/k;->d:J

    cmp-long v4, v4, v10

    if-nez v4, :cond_2

    .line 1211
    iput-wide v2, p0, Lcom/google/android/gms/people/profile/k;->d:J

    .line 1216
    :cond_2
    cmpl-float v2, v0, v6

    if-ltz v2, :cond_6

    .line 1217
    iget v0, p0, Lcom/google/android/gms/people/profile/k;->b:F

    .line 1226
    :cond_3
    iget v2, p0, Lcom/google/android/gms/people/profile/k;->c:F

    move v12, v2

    move v2, v0

    move v0, v12

    .line 1230
    :goto_2
    iget-object v3, p0, Lcom/google/android/gms/people/profile/k;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-static {v3, v2, v0}, Lcom/google/android/gms/people/profile/AvatarView;->a(Lcom/google/android/gms/people/profile/AvatarView;FF)Z

    .line 1231
    iget v3, p0, Lcom/google/android/gms/people/profile/k;->b:F

    sub-float v2, v3, v2

    iput v2, p0, Lcom/google/android/gms/people/profile/k;->b:F

    .line 1232
    iget v2, p0, Lcom/google/android/gms/people/profile/k;->c:F

    sub-float v0, v2, v0

    iput v0, p0, Lcom/google/android/gms/people/profile/k;->c:F

    .line 1234
    iget v0, p0, Lcom/google/android/gms/people/profile/k;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    iget v0, p0, Lcom/google/android/gms/people/profile/k;->c:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    .line 1235
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/k;->a()V

    .line 1239
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/k;->f:Z

    if-nez v0, :cond_0

    .line 1242
    iget-object v0, p0, Lcom/google/android/gms/people/profile/k;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/profile/AvatarView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_5
    move v0, v1

    .line 1208
    goto :goto_1

    .line 1220
    :cond_6
    iget v2, p0, Lcom/google/android/gms/people/profile/k;->b:F

    sub-float v3, v6, v0

    div-float/2addr v2, v3

    mul-float/2addr v2, v7

    .line 1221
    iget v3, p0, Lcom/google/android/gms/people/profile/k;->c:F

    sub-float v0, v6, v0

    div-float v0, v3, v0

    mul-float v3, v0, v7

    .line 1222
    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v4, p0, Lcom/google/android/gms/people/profile/k;->b:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v0, v0, v4

    if-gtz v0, :cond_7

    cmpl-float v0, v2, v8

    if-nez v0, :cond_8

    .line 1223
    :cond_7
    iget v0, p0, Lcom/google/android/gms/people/profile/k;->b:F

    .line 1225
    :goto_3
    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v2

    iget v4, p0, Lcom/google/android/gms/people/profile/k;->c:F

    invoke-static {v4}, Ljava/lang/Math;->abs(F)F

    move-result v4

    cmpl-float v2, v2, v4

    if-gtz v2, :cond_3

    cmpl-float v2, v3, v8

    if-eqz v2, :cond_3

    move v2, v0

    move v0, v3

    goto :goto_2

    :cond_8
    move v0, v2

    goto :goto_3
.end method
