.class public final Lcom/google/android/gms/icing/c/a/f;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/android/gms/icing/c/a/g;

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4528
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 4529
    invoke-static {}, Lcom/google/android/gms/icing/c/a/g;->a()[Lcom/google/android/gms/icing/c/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    iput v1, p0, Lcom/google/android/gms/icing/c/a/f;->b:I

    iput v1, p0, Lcom/google/android/gms/icing/c/a/f;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/c/a/f;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/c/a/f;->cachedSize:I

    .line 4530
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 4596
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 4597
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 4598
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 4599
    iget-object v2, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    aget-object v2, v2, v0

    .line 4600
    if-eqz v2, :cond_0

    .line 4601
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 4598
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4606
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/c/a/f;->b:I

    if-eqz v0, :cond_2

    .line 4607
    const/4 v0, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/c/a/f;->b:I

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 4610
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/c/a/f;->c:I

    if-eqz v0, :cond_3

    .line 4611
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/android/gms/icing/c/a/f;->c:I

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 4614
    :cond_3
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4543
    if-ne p1, p0, :cond_1

    .line 4544
    const/4 v0, 0x1

    .line 4560
    :cond_0
    :goto_0
    return v0

    .line 4546
    :cond_1
    instance-of v1, p1, Lcom/google/android/gms/icing/c/a/f;

    if-eqz v1, :cond_0

    .line 4549
    check-cast p1, Lcom/google/android/gms/icing/c/a/f;

    .line 4550
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    iget-object v2, p1, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4554
    iget v1, p0, Lcom/google/android/gms/icing/c/a/f;->b:I

    iget v2, p1, Lcom/google/android/gms/icing/c/a/f;->b:I

    if-ne v1, v2, :cond_0

    .line 4557
    iget v1, p0, Lcom/google/android/gms/icing/c/a/f;->c:I

    iget v2, p1, Lcom/google/android/gms/icing/c/a/f;->c:I

    if-ne v1, v2, :cond_0

    .line 4560
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/c/a/f;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 4565
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 4568
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/c/a/f;->b:I

    add-int/2addr v0, v1

    .line 4569
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/c/a/f;->c:I

    add-int/2addr v0, v1

    .line 4570
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/android/gms/icing/c/a/f;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4571
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4300
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/c/a/f;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/c/a/g;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/c/a/g;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/g;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/c/a/g;

    invoke-direct {v3}, Lcom/google/android/gms/icing/c/a/g;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/c/a/f;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/c/a/f;->c:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 4577
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 4578
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 4579
    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    aget-object v1, v1, v0

    .line 4580
    if-eqz v1, :cond_0

    .line 4581
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4578
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4585
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/c/a/f;->b:I

    if-eqz v0, :cond_2

    .line 4586
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/c/a/f;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4588
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/c/a/f;->c:I

    if-eqz v0, :cond_3

    .line 4589
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/icing/c/a/f;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4591
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4592
    return-void
.end method
