.class public final Lcom/google/android/gms/plus/service/a/ak;
.super Lcom/google/android/gms/plus/service/a/a;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Lcom/google/android/gms/plus/internal/f;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/plus/internal/f;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/gms/plus/service/a/a;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/plus/service/a/ak;->a:Ljava/lang/String;

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/plus/service/a/ak;->b:Lcom/google/android/gms/plus/internal/f;

    .line 29
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/plus/b/b;)V
    .locals 4

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ak;->a:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 35
    if-nez v0, :cond_0

    .line 36
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/ak;->a:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 37
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 40
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 44
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/service/a/ak;->b:Lcom/google/android/gms/plus/internal/f;

    invoke-interface {v1, v0}, Lcom/google/android/gms/plus/internal/f;->a(Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/plus/service/a/ak;->b:Lcom/google/android/gms/plus/internal/f;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/internal/f;->a(Ljava/lang/String;)V

    .line 52
    return-void
.end method
