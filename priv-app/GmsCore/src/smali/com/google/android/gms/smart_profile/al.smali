.class public final Lcom/google/android/gms/smart_profile/al;
.super Landroid/support/v4/app/ar;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;
.implements Lcom/google/android/gms/smart_profile/ac;
.implements Lcom/google/android/gms/smart_profile/e;


# instance fields
.field private i:I

.field private j:I

.field private k:Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Lcom/google/android/gms/people/identity/models/Person;

.field private p:Lcom/google/android/gms/smart_profile/d;

.field private q:Lcom/google/android/gms/smart_profile/am;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/support/v4/app/ar;-><init>()V

    .line 71
    return-void
.end method

.method public static a(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/smart_profile/al;
    .locals 3

    .prologue
    .line 301
    new-instance v0, Lcom/google/android/gms/smart_profile/al;

    invoke-direct {v0}, Lcom/google/android/gms/smart_profile/al;-><init>()V

    .line 302
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 303
    const-string v2, "relationship"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 304
    const-string v2, "targetPersonId"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    const-string v2, "viewerAccountName"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 306
    const-string v2, "viewerPageId"

    invoke-virtual {v1, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 307
    const-string v2, "qualifiedId"

    invoke-virtual {v1, v2, p4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 308
    const-string v2, "applicationId"

    invoke-virtual {v1, v2, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 309
    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/al;->setArguments(Landroid/os/Bundle;)V

    .line 310
    return-object v0
.end method

.method private c()V
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/al;->q:Lcom/google/android/gms/smart_profile/am;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/am;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/al;->q:Lcom/google/android/gms/smart_profile/am;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/al;->a(Landroid/widget/ListAdapter;)V

    .line 426
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/al;->q:Lcom/google/android/gms/smart_profile/am;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/am;->notifyDataSetChanged()V

    .line 428
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 5

    .prologue
    .line 371
    packed-switch p1, :pswitch_data_0

    .line 379
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 373
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/al;->o:Lcom/google/android/gms/people/identity/models/Person;

    invoke-interface {v0}, Lcom/google/android/gms/people/identity/models/Person;->k()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/al;->m:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/smart_profile/al;->j:I

    new-instance v3, Lcom/google/c/f/c/a/a/b;

    invoke-direct {v3}, Lcom/google/c/f/c/a/a/b;-><init>()V

    iput-object v0, v3, Lcom/google/c/f/c/a/a/b;->a:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/smart_profile/a/a;->F:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iput-object v0, v3, Lcom/google/c/f/c/a/a/b;->b:Ljava/lang/Integer;

    invoke-static {v3}, Lcom/google/c/f/c/a/a/b;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    new-instance v3, Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;

    invoke-direct {v3, v0, v1, v2}, Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;-><init>([BLjava/lang/String;I)V

    .line 375
    new-instance v0, Lcom/google/android/gms/smart_profile/b/h;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/al;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/al;->p:Lcom/google/android/gms/smart_profile/d;

    iget-object v2, v2, Lcom/google/android/gms/smart_profile/d;->c:Lcom/google/android/gms/plus/internal/ab;

    iget v4, p0, Lcom/google/android/gms/smart_profile/al;->i:I

    invoke-direct {v0, v1, v2, v4, v3}, Lcom/google/android/gms/smart_profile/b/h;-><init>(Landroid/content/Context;Lcom/google/android/gms/plus/internal/ab;ILcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;)V

    goto :goto_0

    .line 371
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 383
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 46
    check-cast p2, [Lcom/google/c/f/c/a/i;

    if-eqz p2, :cond_0

    array-length v0, p2

    if-nez v0, :cond_1

    :cond_0
    iget v0, p0, Lcom/google/android/gms/smart_profile/al;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/al;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->rR:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/al;->a(Ljava/lang/CharSequence;)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/al;->q:Lcom/google/android/gms/smart_profile/am;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/smart_profile/am;->a([Lcom/google/c/f/c/a/i;)V

    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/al;->c()V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/al;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->rS:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/smart_profile/al;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public final ai_()V
    .locals 8

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/al;->p:Lcom/google/android/gms/smart_profile/d;

    iget-object v7, v0, Lcom/google/android/gms/smart_profile/d;->b:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    .line 351
    invoke-virtual {v7}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->g()Lcom/google/android/gms/smart_profile/SmartProfilePerson;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/al;->o:Lcom/google/android/gms/people/identity/models/Person;

    .line 352
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/al;->o:Lcom/google/android/gms/people/identity/models/Person;

    if-nez v0, :cond_1

    .line 367
    :cond_0
    :goto_0
    return-void

    .line 357
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/al;->q:Lcom/google/android/gms/smart_profile/am;

    if-nez v0, :cond_0

    .line 358
    new-instance v0, Lcom/google/android/gms/smart_profile/am;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/al;->p:Lcom/google/android/gms/smart_profile/d;

    iget-object v2, v1, Lcom/google/android/gms/smart_profile/d;->a:Lcom/google/android/gms/common/api/v;

    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/al;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getIntent()Landroid/content/Intent;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/al;->m:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/al;->l:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/al;->p:Lcom/google/android/gms/smart_profile/d;

    iget-object v1, v1, Lcom/google/android/gms/smart_profile/d;->b:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->j()I

    move-result v6

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/smart_profile/am;-><init>(Landroid/support/v4/app/Fragment;Lcom/google/android/gms/common/api/v;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/al;->q:Lcom/google/android/gms/smart_profile/am;

    .line 361
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/al;->q:Lcom/google/android/gms/smart_profile/am;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/al;->p:Lcom/google/android/gms/smart_profile/d;

    iget-object v1, v1, Lcom/google/android/gms/smart_profile/d;->b:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->d()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/am;->b(Ljava/util/Map;)V

    .line 363
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/al;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 364
    invoke-virtual {v7, p0}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->a(Lcom/google/android/gms/smart_profile/ac;)V

    .line 365
    invoke-virtual {v7}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->b()V

    goto :goto_0
.end method

.method public final aj_()V
    .locals 2

    .prologue
    .line 400
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/al;->q:Lcom/google/android/gms/smart_profile/am;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/al;->p:Lcom/google/android/gms/smart_profile/d;

    iget-object v1, v1, Lcom/google/android/gms/smart_profile/d;->b:Lcom/google/android/gms/smart_profile/IdentityPersonUtil;

    invoke-virtual {v1}, Lcom/google/android/gms/smart_profile/IdentityPersonUtil;->e()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/smart_profile/am;->a(Ljava/util/Map;)V

    .line 401
    invoke-direct {p0}, Lcom/google/android/gms/smart_profile/al;->c()V

    .line 402
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    .line 328
    invoke-super {p0, p1}, Landroid/support/v4/app/ar;->onActivityCreated(Landroid/os/Bundle;)V

    .line 330
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/al;->a()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    .line 332
    new-instance v0, Lcom/google/android/gms/smart_profile/d;

    iget-object v3, p0, Lcom/google/android/gms/smart_profile/al;->l:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/smart_profile/al;->m:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/smart_profile/al;->n:Ljava/lang/String;

    iget v6, p0, Lcom/google/android/gms/smart_profile/al;->j:I

    const/4 v7, 0x1

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/smart_profile/d;-><init>(Landroid/support/v4/app/Fragment;Lcom/google/android/gms/smart_profile/e;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/al;->p:Lcom/google/android/gms/smart_profile/d;

    .line 334
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 406
    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 407
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/ar;->onActivityResult(IILandroid/content/Intent;)V

    .line 408
    invoke-static {p3}, Lcom/google/android/gms/common/audience/a/i;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/audience/a/j;

    move-result-object v0

    .line 409
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/al;->q:Lcom/google/android/gms/smart_profile/am;

    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/j;->f()Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/j;->i()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/smart_profile/am;->a(Lcom/google/android/gms/common/people/data/AudienceMember;Ljava/util/List;)V

    .line 410
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/al;->q:Lcom/google/android/gms/smart_profile/am;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/am;->notifyDataSetChanged()V

    .line 412
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 315
    invoke-super {p0, p1}, Landroid/support/v4/app/ar;->onCreate(Landroid/os/Bundle;)V

    .line 317
    invoke-virtual {p0}, Lcom/google/android/gms/smart_profile/al;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 318
    const-string v1, "relationship"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/smart_profile/al;->i:I

    .line 319
    const-string v1, "applicationId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/smart_profile/al;->j:I

    .line 320
    const-string v1, "targetPersonId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/smart_profile/al;->k:Ljava/lang/String;

    .line 321
    const-string v1, "viewerAccountName"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/smart_profile/al;->l:Ljava/lang/String;

    .line 322
    const-string v1, "qualifiedId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/smart_profile/al;->n:Ljava/lang/String;

    .line 323
    const-string v1, "viewerPageId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/al;->m:Ljava/lang/String;

    .line 324
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 338
    invoke-super {p0}, Landroid/support/v4/app/ar;->onStart()V

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/al;->p:Lcom/google/android/gms/smart_profile/d;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/d;->b()V

    .line 340
    return-void
.end method

.method public final onStop()V
    .locals 1

    .prologue
    .line 344
    invoke-super {p0}, Landroid/support/v4/app/ar;->onStop()V

    .line 345
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/al;->p:Lcom/google/android/gms/smart_profile/d;

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/d;->c()V

    .line 346
    return-void
.end method
