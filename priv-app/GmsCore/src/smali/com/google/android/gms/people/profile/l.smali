.class final Lcom/google/android/gms/people/profile/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/google/android/gms/people/profile/AvatarView;

.field private b:F

.field private c:F

.field private d:J

.field private e:Z

.field private f:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/profile/AvatarView;)V
    .locals 2

    .prologue
    .line 1074
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1075
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/people/profile/l;->d:J

    .line 1076
    iput-object p1, p0, Lcom/google/android/gms/people/profile/l;->a:Lcom/google/android/gms/people/profile/AvatarView;

    .line 1077
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/people/profile/l;)Z
    .locals 1

    .prologue
    .line 1060
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/l;->e:Z

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1099
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/l;->e:Z

    .line 1100
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/l;->f:Z

    .line 1101
    return-void
.end method

.method public final a(FF)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1083
    iget-boolean v2, p0, Lcom/google/android/gms/people/profile/l;->e:Z

    if-eqz v2, :cond_0

    .line 1092
    :goto_0
    return v0

    .line 1086
    :cond_0
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/gms/people/profile/l;->d:J

    .line 1087
    iput p1, p0, Lcom/google/android/gms/people/profile/l;->b:F

    .line 1088
    iput p2, p0, Lcom/google/android/gms/people/profile/l;->c:F

    .line 1089
    iput-boolean v0, p0, Lcom/google/android/gms/people/profile/l;->f:Z

    .line 1090
    iput-boolean v1, p0, Lcom/google/android/gms/people/profile/l;->e:Z

    .line 1091
    iget-object v0, p0, Lcom/google/android/gms/people/profile/l;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/profile/AvatarView;->post(Ljava/lang/Runnable;)Z

    move v0, v1

    .line 1092
    goto :goto_0
.end method

.method public final run()V
    .locals 9

    .prologue
    const/high16 v8, 0x447a0000    # 1000.0f

    const/4 v1, 0x0

    .line 1106
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/l;->f:Z

    if-eqz v0, :cond_1

    .line 1151
    :cond_0
    :goto_0
    return-void

    .line 1111
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 1112
    iget-wide v4, p0, Lcom/google/android/gms/people/profile/l;->d:J

    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-eqz v0, :cond_7

    iget-wide v4, p0, Lcom/google/android/gms/people/profile/l;->d:J

    sub-long v4, v2, v4

    long-to-float v0, v4

    div-float/2addr v0, v8

    .line 1113
    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/people/profile/l;->a:Lcom/google/android/gms/people/profile/AvatarView;

    iget v5, p0, Lcom/google/android/gms/people/profile/l;->b:F

    mul-float/2addr v5, v0

    iget v6, p0, Lcom/google/android/gms/people/profile/l;->c:F

    mul-float/2addr v6, v0

    invoke-static {v4, v5, v6}, Lcom/google/android/gms/people/profile/AvatarView;->a(Lcom/google/android/gms/people/profile/AvatarView;FF)Z

    move-result v4

    .line 1114
    iput-wide v2, p0, Lcom/google/android/gms/people/profile/l;->d:J

    .line 1116
    mul-float/2addr v0, v8

    .line 1117
    iget v2, p0, Lcom/google/android/gms/people/profile/l;->b:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_8

    .line 1118
    iget v2, p0, Lcom/google/android/gms/people/profile/l;->b:F

    sub-float/2addr v2, v0

    iput v2, p0, Lcom/google/android/gms/people/profile/l;->b:F

    .line 1119
    iget v2, p0, Lcom/google/android/gms/people/profile/l;->b:F

    cmpg-float v2, v2, v1

    if-gez v2, :cond_2

    .line 1120
    iput v1, p0, Lcom/google/android/gms/people/profile/l;->b:F

    .line 1128
    :cond_2
    :goto_2
    iget v2, p0, Lcom/google/android/gms/people/profile/l;->c:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_9

    .line 1129
    iget v2, p0, Lcom/google/android/gms/people/profile/l;->c:F

    sub-float v0, v2, v0

    iput v0, p0, Lcom/google/android/gms/people/profile/l;->c:F

    .line 1130
    iget v0, p0, Lcom/google/android/gms/people/profile/l;->c:F

    cmpg-float v0, v0, v1

    if-gez v0, :cond_3

    .line 1131
    iput v1, p0, Lcom/google/android/gms/people/profile/l;->c:F

    .line 1141
    :cond_3
    :goto_3
    iget v0, p0, Lcom/google/android/gms/people/profile/l;->b:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_4

    iget v0, p0, Lcom/google/android/gms/people/profile/l;->c:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_5

    :cond_4
    if-nez v4, :cond_6

    .line 1142
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/people/profile/l;->a()V

    .line 1143
    iget-object v0, p0, Lcom/google/android/gms/people/profile/l;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-static {v0}, Lcom/google/android/gms/people/profile/AvatarView;->a(Lcom/google/android/gms/people/profile/AvatarView;)V

    .line 1147
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/gms/people/profile/l;->f:Z

    if-nez v0, :cond_0

    .line 1150
    iget-object v0, p0, Lcom/google/android/gms/people/profile/l;->a:Lcom/google/android/gms/people/profile/AvatarView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/people/profile/AvatarView;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_7
    move v0, v1

    .line 1112
    goto :goto_1

    .line 1123
    :cond_8
    iget v2, p0, Lcom/google/android/gms/people/profile/l;->b:F

    add-float/2addr v2, v0

    iput v2, p0, Lcom/google/android/gms/people/profile/l;->b:F

    .line 1124
    iget v2, p0, Lcom/google/android/gms/people/profile/l;->b:F

    cmpl-float v2, v2, v1

    if-lez v2, :cond_2

    .line 1125
    iput v1, p0, Lcom/google/android/gms/people/profile/l;->b:F

    goto :goto_2

    .line 1134
    :cond_9
    iget v2, p0, Lcom/google/android/gms/people/profile/l;->c:F

    add-float/2addr v0, v2

    iput v0, p0, Lcom/google/android/gms/people/profile/l;->c:F

    .line 1135
    iget v0, p0, Lcom/google/android/gms/people/profile/l;->c:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_3

    .line 1136
    iput v1, p0, Lcom/google/android/gms/people/profile/l;->c:F

    goto :goto_3
.end method
