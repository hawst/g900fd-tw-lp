.class public final Lcom/google/android/gms/drive/g/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/g/at;


# instance fields
.field private final a:J

.field private final b:D

.field private final c:J

.field private final d:Ljava/util/Random;

.field private final e:Lcom/google/android/gms/drive/g/i;

.field private f:I

.field private g:J


# direct methods
.method public constructor <init>(JD)V
    .locals 9

    .prologue
    .line 32
    const-wide v6, 0x7fffffffffffffffL

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/drive/g/v;-><init>(JDJ)V

    .line 33
    return-void
.end method

.method public constructor <init>(JDJ)V
    .locals 11

    .prologue
    .line 48
    new-instance v8, Ljava/util/Random;

    invoke-direct {v8}, Ljava/util/Random;-><init>()V

    invoke-static {}, Lcom/google/android/gms/drive/g/aw;->c()Lcom/google/android/gms/drive/g/i;

    move-result-object v9

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide/from16 v6, p5

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/drive/g/v;-><init>(JDJLjava/util/Random;Lcom/google/android/gms/drive/g/i;)V

    .line 50
    return-void
.end method

.method private constructor <init>(JDJLjava/util/Random;Lcom/google/android/gms/drive/g/i;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput v2, p0, Lcom/google/android/gms/drive/g/v;->f:I

    .line 55
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-lez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 56
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, p3, v4

    if-ltz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 57
    cmp-long v0, p1, p5

    if-gtz v0, :cond_2

    :goto_2
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 58
    iput-wide p1, p0, Lcom/google/android/gms/drive/g/v;->a:J

    .line 59
    iput-wide p3, p0, Lcom/google/android/gms/drive/g/v;->b:D

    .line 60
    iput-wide p5, p0, Lcom/google/android/gms/drive/g/v;->c:J

    .line 61
    invoke-static {p7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Random;

    iput-object v0, p0, Lcom/google/android/gms/drive/g/v;->d:Ljava/util/Random;

    .line 62
    invoke-static {p8}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/g/v;->e:Lcom/google/android/gms/drive/g/i;

    .line 63
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/v;->g()V

    .line 64
    return-void

    :cond_0
    move v0, v2

    .line 55
    goto :goto_0

    :cond_1
    move v0, v2

    .line 56
    goto :goto_1

    :cond_2
    move v1, v2

    .line 57
    goto :goto_2
.end method

.method private declared-synchronized a()V
    .locals 6

    .prologue
    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    .line 93
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gms/drive/g/v;->b:D

    sub-double/2addr v0, v4

    iget-object v2, p0, Lcom/google/android/gms/drive/g/v;->d:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextDouble()D

    move-result-wide v2

    mul-double/2addr v0, v2

    add-double/2addr v0, v4

    .line 94
    iget-wide v2, p0, Lcom/google/android/gms/drive/g/v;->b:D

    iget v4, p0, Lcom/google/android/gms/drive/g/v;->f:I

    int-to-double v4, v4

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->pow(DD)D

    move-result-wide v2

    mul-double/2addr v0, v2

    .line 95
    iget-wide v2, p0, Lcom/google/android/gms/drive/g/v;->a:J

    long-to-double v2, v2

    mul-double/2addr v0, v2

    .line 96
    iget-wide v2, p0, Lcom/google/android/gms/drive/g/v;->c:J

    long-to-double v2, v2

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->min(DD)D

    move-result-wide v0

    double-to-long v0, v0

    iget-object v2, p0, Lcom/google/android/gms/drive/g/v;->e:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v2}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/gms/drive/g/v;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 98
    monitor-exit p0

    return-void

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized b()J
    .locals 6

    .prologue
    .line 107
    monitor-enter p0

    const-wide/16 v0, 0x0

    :try_start_0
    iget-wide v2, p0, Lcom/google/android/gms/drive/g/v;->g:J

    iget-object v4, p0, Lcom/google/android/gms/drive/g/v;->e:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v4}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 108
    iget v2, p0, Lcom/google/android/gms/drive/g/v;->f:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/google/android/gms/drive/g/v;->f:I

    .line 109
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/v;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    monitor-exit p0

    return-wide v0

    .line 107
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized g()V
    .locals 1

    .prologue
    .line 131
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/google/android/gms/drive/g/v;->f:I

    .line 132
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/v;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    monitor-exit p0

    return-void

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized c()Z
    .locals 4

    .prologue
    .line 68
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/g/v;->e:Lcom/google/android/gms/drive/g/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/g/i;->a()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/drive/g/v;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()Z
    .locals 1

    .prologue
    .line 73
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/drive/g/v;->c()Z

    move-result v0

    .line 74
    if-eqz v0, :cond_0

    .line 75
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/v;->b()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 77
    :cond_0
    monitor-exit p0

    return v0

    .line 73
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()V
    .locals 4

    .prologue
    .line 118
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/g/v;->b()J

    move-result-wide v0

    .line 119
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    .line 120
    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    :cond_0
    monitor-exit p0

    return-void

    .line 118
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 145
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ExponentialBackoffRateLimiter[%d tokens, initialMs=%d, factor=%.3f]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/gms/drive/g/v;->f:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-wide v4, p0, Lcom/google/android/gms/drive/g/v;->a:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/google/android/gms/drive/g/v;->b:D

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
