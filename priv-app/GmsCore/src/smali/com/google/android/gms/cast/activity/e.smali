.class final Lcom/google/android/gms/cast/activity/e;
.super Lcom/google/android/gms/cast/e/d;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 249
    iput-object p1, p0, Lcom/google/android/gms/cast/activity/e;->a:Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;

    .line 250
    invoke-virtual {p1}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->c()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->d(Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;)Landroid/os/Handler;

    move-result-object v2

    invoke-direct {p0, v0, p2, v1, v2}, Lcom/google/android/gms/cast/e/d;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Handler;)V

    .line 251
    iput-object p2, p0, Lcom/google/android/gms/cast/activity/e;->d:Ljava/lang/String;

    .line 252
    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/protobuf/a/f;
    .locals 3

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/e;->a:Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/activity/CastNearbyPinActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast/e/i;->a(Ljava/util/List;)Lcom/google/i/a/a/a/a;

    move-result-object v1

    .line 258
    invoke-virtual {v1}, Lcom/google/i/a/a/a/a;->c()I

    move-result v0

    if-nez v0, :cond_0

    .line 259
    const/4 v0, 0x0

    .line 266
    :goto_0
    return-object v0

    .line 261
    :cond_0
    new-instance v0, Lcom/google/i/a/a/b/e;

    invoke-direct {v0}, Lcom/google/i/a/a/b/e;-><init>()V

    .line 262
    invoke-virtual {v0, v1}, Lcom/google/i/a/a/b/e;->a(Lcom/google/i/a/a/a/a;)Lcom/google/i/a/a/b/e;

    .line 263
    new-instance v1, Lcom/google/i/a/a/a/b;

    invoke-direct {v1}, Lcom/google/i/a/a/a/b;-><init>()V

    .line 264
    iget-object v2, p0, Lcom/google/android/gms/cast/activity/e;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/i/a/a/a/b;->a(Ljava/lang/String;)Lcom/google/i/a/a/a/b;

    .line 265
    invoke-virtual {v0, v1}, Lcom/google/i/a/a/b/e;->a(Lcom/google/i/a/a/a/b;)Lcom/google/i/a/a/b/e;

    goto :goto_0
.end method

.method protected final a(I)V
    .locals 5

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/gms/cast/activity/e;->c:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Failed to start audio for type %s, error=%d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/cast/activity/e;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 291
    return-void
.end method

.method protected final a([B)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 273
    :try_start_0
    new-instance v0, Lcom/google/i/a/a/b/f;

    invoke-direct {v0}, Lcom/google/i/a/a/b/f;-><init>()V

    array-length v1, p1

    invoke-virtual {v0, p1, v1}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v0

    check-cast v0, Lcom/google/i/a/a/b/f;

    .line 275
    iget-object v1, p0, Lcom/google/android/gms/cast/activity/e;->c:Lcom/google/android/gms/cast/e/h;

    const-string v2, "Successfully requested the %s for nearby devices"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/cast/activity/e;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 276
    iget-object v1, p0, Lcom/google/android/gms/cast/activity/e;->c:Lcom/google/android/gms/cast/e/h;

    const-string v2, "success count=%d, offline count=%d, error count=%d"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, v0, Lcom/google/i/a/a/b/f;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, v0, Lcom/google/i/a/a/b/f;->b:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget v0, v0, Lcom/google/i/a/a/b/f;->c:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/e/h;->b(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_0

    .line 285
    :goto_0
    return-void

    .line 283
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/cast/activity/e;->c:Lcom/google/android/gms/cast/e/h;

    const-string v1, "Unable to parse SendMessageResponse data"

    new-array v2, v6, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/e/h;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
