.class public Lcom/google/android/gms/googlehelp/helpactivities/OpenHelpActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 15
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 16
    const-string v0, "com.google.android.gms.googlehelp.HELP"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 18
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/OpenHelpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 19
    const-string v2, "EXTRA_HELP_CONFIG"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 20
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "OpenHelpActivity requires a HelpConfig extra."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 23
    :cond_0
    invoke-virtual {v0, v1}, Landroid/content/Intent;->putExtras(Landroid/content/Intent;)Landroid/content/Intent;

    .line 25
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 26
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 29
    :cond_1
    const/16 v1, 0x6f

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/OpenHelpActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 30
    return-void
.end method
