.class public final Lcom/google/android/gms/plus/plusone/u;
.super Landroid/widget/FrameLayout;
.source "SourceFile"


# static fields
.field private static final a:I

.field private static final b:I

.field private static final c:I


# instance fields
.field private d:Lcom/google/android/gms/plus/data/a/a;

.field private e:Lcom/google/android/gms/plus/internal/ab;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    sget v0, Lcom/google/android/gms/l;->dn:I

    sput v0, Lcom/google/android/gms/plus/plusone/u;->a:I

    .line 36
    sget v0, Lcom/google/android/gms/l;->eD:I

    sput v0, Lcom/google/android/gms/plus/plusone/u;->b:I

    .line 37
    sget v0, Lcom/google/android/gms/l;->eV:I

    sput v0, Lcom/google/android/gms/plus/plusone/u;->c:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 45
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 46
    return-void
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 217
    if-nez p0, :cond_0

    .line 220
    :goto_0
    return-object p0

    :cond_0
    invoke-static {p0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/plus/internal/ab;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/u;->e:Lcom/google/android/gms/plus/internal/ab;

    .line 58
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/data/a/a;)Z
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/16 v10, 0x8

    const/4 v5, 0x0

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/u;->e:Lcom/google/android/gms/plus/internal/ab;

    const-string v1, "Call initialize first"

    invoke-static {v0, v1}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    iput-object p1, p0, Lcom/google/android/gms/plus/plusone/u;->d:Lcom/google/android/gms/plus/data/a/a;

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/u;->removeAllViews()V

    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/u;->d:Lcom/google/android/gms/plus/data/a/a;

    if-nez v0, :cond_0

    move v0, v5

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/plusone/u;->d:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/data/a/a;->a()Ljava/lang/String;

    move-result-object v0

    const-string v1, "article"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/u;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/plus/plusone/u;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    sget v0, Lcom/google/android/gms/j;->aN:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v1, p0, Lcom/google/android/gms/plus/plusone/u;->e:Lcom/google/android/gms/plus/internal/ab;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Lcom/google/android/gms/plus/internal/ab;)V

    sget v1, Lcom/google/android/gms/j;->aP:I

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/j;->aM:I

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    sget v3, Lcom/google/android/gms/j;->aO:I

    invoke-virtual {v4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v6, p0, Lcom/google/android/gms/plus/plusone/u;->e:Lcom/google/android/gms/plus/internal/ab;

    invoke-virtual {v3, v6}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Lcom/google/android/gms/plus/internal/ab;)V

    iget-object v6, p0, Lcom/google/android/gms/plus/plusone/u;->d:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v6}, Lcom/google/android/gms/plus/data/a/a;->b()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/plus/plusone/u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/plus/plusone/u;->d:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v7}, Lcom/google/android/gms/plus/data/a/a;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Lcom/google/android/gms/plus/plusone/u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gms/plus/plusone/u;->d:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v8}, Lcom/google/android/gms/plus/data/a/a;->d()Ljava/lang/String;

    move-result-object v8

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_1

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-virtual {v0, v10}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v3, v5}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    invoke-static {v8}, Lcom/google/android/gms/plus/internal/x;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Landroid/net/Uri;)V

    :goto_3
    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v4

    :goto_4
    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/plusone/u;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/u;->invalidate()V

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/u;->requestLayout()V

    const/4 v0, 0x1

    goto/16 :goto_0

    :cond_1
    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {v2, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_2

    :cond_3
    invoke-virtual {v3, v10}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    goto :goto_3

    :cond_4
    const-string v1, "video"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/u;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/plus/plusone/u;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sget v0, Lcom/google/android/gms/j;->tP:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/j;->tO:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/u;->e:Lcom/google/android/gms/plus/internal/ab;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Lcom/google/android/gms/plus/internal/ab;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/u;->d:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/data/a/a;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/plusone/u;->d:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/data/a/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/plus/plusone/u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_5

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {v1, v5}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    invoke-static {v3}, Lcom/google/android/gms/plus/internal/x;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Landroid/net/Uri;)V

    :goto_6
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v2

    goto :goto_4

    :cond_5
    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_5

    :cond_6
    invoke-virtual {v1, v10}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    goto :goto_6

    :cond_7
    const-string v1, "photo"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-virtual {p0}, Lcom/google/android/gms/plus/plusone/u;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    sget v1, Lcom/google/android/gms/plus/plusone/u;->b:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sget v0, Lcom/google/android/gms/j;->nV:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/j;->nU:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/plus/data/internal/PlusImageView;

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/u;->e:Lcom/google/android/gms/plus/internal/ab;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Lcom/google/android/gms/plus/internal/ab;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/plusone/u;->d:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/data/a/a;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/plusone/u;->d:Lcom/google/android/gms/plus/data/a/a;

    invoke-virtual {v4}, Lcom/google/android/gms/plus/data/a/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/plus/plusone/u;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_8

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_7
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {v1, v5}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    invoke-static {v3}, Lcom/google/android/gms/plus/internal/x;->a(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->a(Landroid/net/Uri;)V

    :goto_8
    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v2

    goto/16 :goto_4

    :cond_8
    invoke-virtual {v0, v10}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_7

    :cond_9
    invoke-virtual {v1, v10}, Lcom/google/android/gms/plus/data/internal/PlusImageView;->setVisibility(I)V

    goto :goto_8

    :cond_a
    const-string v1, "PreviewView"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unsupported content type:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v5

    goto/16 :goto_0
.end method
