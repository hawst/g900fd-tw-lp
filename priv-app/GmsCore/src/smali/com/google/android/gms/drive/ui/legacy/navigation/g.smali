.class public final Lcom/google/android/gms/drive/ui/legacy/navigation/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/ui/legacy/navigation/f;


# instance fields
.field private a:Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

.field private final b:Ljava/lang/Thread;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/g;->b:Ljava/lang/Thread;

    .line 28
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/g;->b:Ljava/lang/Thread;

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 61
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/legacy/navigation/g;->c()V

    .line 34
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/g;->a:Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;->a()Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/legacy/navigation/g;->c()V

    .line 52
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/g;->a:Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    .line 53
    return-void
.end method

.method public final b()Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/legacy/navigation/g;->c()V

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/legacy/navigation/g;->a:Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    return-object v0
.end method
