.class public final Lcom/google/android/gms/feedback/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/http/GoogleHttpClient;

.field private b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/gms/feedback/aa;->b:Landroid/content/Context;

    .line 61
    new-instance v0, Lcom/google/android/gms/http/GoogleHttpClient;

    const-string v1, "AndroidGoogleFeedback/1.1"

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object v0, p0, Lcom/google/android/gms/feedback/aa;->a:Lcom/google/android/gms/http/GoogleHttpClient;

    .line 62
    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 146
    const/4 v1, 0x0

    .line 147
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 149
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/aa;->b:Landroid/content/Context;

    const-string v2, "oauth2:https://www.googleapis.com/auth/supportcontent"

    invoke-static {v0, p1, v2}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 154
    :goto_0
    return-object v0

    .line 150
    :catch_0
    move-exception v0

    .line 151
    :goto_1
    const-string v2, "GFEEDBACK_GoogleFeedbackHttpHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GoogleAuthException: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object v0, v1

    goto :goto_0

    .line 150
    :catch_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/feedback/ErrorReport;Ljava/lang/String;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 76
    invoke-direct {p0, p2}, Lcom/google/android/gms/feedback/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/feedback/aa;->b:Landroid/content/Context;

    invoke-static {p1, v0}, Lcom/google/android/gms/feedback/ac;->a(Lcom/google/android/gms/feedback/ErrorReport;Landroid/content/Context;)Li/i;

    move-result-object v0

    .line 79
    iget-object v2, p0, Lcom/google/android/gms/feedback/aa;->b:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/google/android/gms/feedback/ac;->a(Landroid/content/Context;Li/i;)Ljava/io/File;

    move-result-object v4

    .line 81
    const/4 v0, 0x0

    move v2, v1

    .line 83
    :goto_0
    const/4 v5, 0x3

    if-ge v2, v5, :cond_2

    .line 85
    :try_start_0
    invoke-virtual {p0, v4, v3}, Lcom/google/android/gms/feedback/aa;->a(Ljava/io/File;Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-eqz v5, :cond_1

    .line 86
    if-eqz v4, :cond_0

    .line 98
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    :cond_0
    const/4 v0, 0x1

    .line 101
    :goto_1
    return v0

    .line 88
    :catch_0
    move-exception v0

    .line 89
    :try_start_1
    const-string v5, "GFEEDBACK_GoogleFeedbackHttpHelper"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "network error "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 93
    :cond_2
    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 94
    :catch_1
    move-exception v0

    .line 95
    :try_start_2
    const-string v2, "GFEEDBACK_GoogleFeedbackHttpHelper"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 97
    if-eqz v4, :cond_3

    .line 98
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    :cond_3
    move v0, v1

    .line 101
    goto :goto_1

    .line 97
    :catchall_0
    move-exception v0

    if-eqz v4, :cond_4

    .line 98
    invoke-virtual {v4}, Ljava/io/File;->delete()Z

    :cond_4
    throw v0
.end method

.method public final a(Ljava/io/File;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 111
    new-instance v1, Lorg/apache/http/client/methods/HttpPost;

    const-string v2, "https://www.google.com/tools/feedback/android/__submit"

    invoke-direct {v1, v2}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 112
    const-string v2, "Content-encoding"

    const-string v3, "gzip"

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    new-instance v2, Lorg/apache/http/entity/FileEntity;

    const-string v3, "application/x-protobuf"

    invoke-direct {v2, p1, v3}, Lorg/apache/http/entity/FileEntity;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 114
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 115
    const-string v2, "Authorization"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "OAuth "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/feedback/aa;->a:Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/http/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 119
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v1

    .line 121
    if-nez v1, :cond_1

    .line 122
    const-string v1, "GFEEDBACK_GoogleFeedbackHttpHelper"

    const-string v2, "null status line"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    :goto_0
    :pswitch_0
    return v0

    .line 126
    :cond_1
    invoke-interface {v1}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v1

    .line 127
    const-string v2, "GFEEDBACK_GoogleFeedbackHttpHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sending report status: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    div-int/lit8 v1, v1, 0x64

    .line 130
    packed-switch v1, :pswitch_data_0

    .line 138
    const/4 v0, 0x1

    goto :goto_0

    .line 130
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
    .end packed-switch
.end method
