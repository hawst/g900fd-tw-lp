.class public Lcom/google/android/gms/appdatasearch/QuerySpecification;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ag;


# instance fields
.field final a:I

.field public final b:Z

.field public final c:Ljava/util/List;

.field public final d:Ljava/util/List;

.field public final e:Z

.field public final f:I

.field public final g:I

.field public final h:Z

.field public final i:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/appdatasearch/ag;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ag;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ag;

    return-void
.end method

.method public constructor <init>(IZLjava/util/List;Ljava/util/List;ZIIZI)V
    .locals 0

    .prologue
    .line 90
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    iput p1, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->a:I

    .line 92
    iput-boolean p2, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->b:Z

    .line 93
    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    .line 94
    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    .line 95
    iput-boolean p5, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->e:Z

    .line 96
    iput p6, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->f:I

    .line 97
    iput p7, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->g:I

    .line 98
    iput-boolean p8, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->h:Z

    .line 99
    iput p9, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->i:I

    .line 100
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ag;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 111
    sget-object v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ag;

    invoke-static {p0, p1}, Lcom/google/android/gms/appdatasearch/ag;->a(Lcom/google/android/gms/appdatasearch/QuerySpecification;Landroid/os/Parcel;)V

    .line 112
    return-void
.end method
