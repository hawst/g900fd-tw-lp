.class public final Lcom/google/android/gms/plus/internal/ay;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/internal/ab;


# instance fields
.field private final g:Lcom/google/android/gms/plus/internal/PlusSession;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)V
    .locals 1

    .prologue
    .line 1287
    invoke-virtual {p2}, Lcom/google/android/gms/plus/internal/PlusSession;->c()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, p3, p4, v0}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;[Ljava/lang/String;)V

    .line 1289
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/ay;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    .line 1290
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 52
    invoke-static {p1}, Lcom/google/android/gms/plus/internal/j;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/i;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 3

    .prologue
    .line 1745
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/ay;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusSession;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 1746
    const-string v1, "skip_oob"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1747
    iget-object v1, p0, Lcom/google/android/gms/plus/internal/ay;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/PlusSession;->e()[Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1748
    const-string v1, "required_features"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/ay;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/PlusSession;->e()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 1750
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/plus/internal/ay;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/internal/PlusSession;->h()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1751
    const-string v1, "application_name"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/ay;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/PlusSession;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1753
    :cond_1
    const-string v1, "auth_package"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/ay;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/PlusSession;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 1754
    new-instance v1, Lcom/google/android/gms/common/internal/GetServiceRequest;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/internal/GetServiceRequest;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/ay;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/PlusSession;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->a(Ljava/lang/String;)Lcom/google/android/gms/common/internal/GetServiceRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/ay;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/PlusSession;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/internal/d;->a(Ljava/lang/String;)Lcom/google/android/gms/common/internal/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->a(Lcom/google/android/gms/common/internal/ba;)Lcom/google/android/gms/common/internal/GetServiceRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->d:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/common/util/as;->a([Ljava/lang/String;)[Lcom/google/android/gms/common/api/Scope;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->a([Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/internal/GetServiceRequest;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/internal/GetServiceRequest;->a(Landroid/os/Bundle;)Lcom/google/android/gms/common/internal/GetServiceRequest;

    move-result-object v0

    .line 1759
    invoke-interface {p1, p2, v0}, Lcom/google/android/gms/common/internal/bj;->b(Lcom/google/android/gms/common/internal/bg;Lcom/google/android/gms/common/internal/GetServiceRequest;)V

    .line 1760
    return-void
.end method

.method public final a(Lcom/google/android/gms/plus/internal/ae;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 3

    .prologue
    .line 1613
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1614
    new-instance v1, Lcom/google/android/gms/plus/internal/bb;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bb;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ae;)V

    .line 1616
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1620
    :goto_0
    return-void

    .line 1618
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/plus/internal/bb;->a(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/af;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1600
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1601
    new-instance v1, Lcom/google/android/gms/plus/internal/bd;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bd;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/af;)V

    .line 1603
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1608
    :goto_0
    return-void

    .line 1605
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, p2, v2}, Lcom/google/android/gms/plus/internal/bd;->a(ILandroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/AppAclsEntity;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/ag;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/util/List;ZZZZ)V
    .locals 9

    .prologue
    .line 1626
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1627
    new-instance v1, Lcom/google/android/gms/plus/internal/bf;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bf;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ag;)V

    .line 1629
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    move/from16 v8, p8

    invoke-interface/range {v0 .. v8}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/plus/internal/model/apps/ApplicationEntity;Ljava/util/List;ZZZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1634
    :goto_0
    return-void

    .line 1632
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/plus/internal/bf;->a(ILandroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/ah;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 1533
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1534
    new-instance v1, Lcom/google/android/gms/plus/internal/bh;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bh;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ah;)V

    .line 1537
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1542
    :goto_0
    return-void

    .line 1539
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/plus/internal/bh;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/ai;Lcom/google/android/gms/plus/model/posts/Comment;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1336
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1339
    new-instance v1, Lcom/google/android/gms/plus/internal/az;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/az;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ai;)V

    .line 1341
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Comment;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1345
    :goto_0
    return-void

    .line 1343
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/az;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Comment;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/aj;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1487
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1489
    new-instance v1, Lcom/google/android/gms/plus/internal/bj;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bj;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/aj;)V

    .line 1492
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/plus/internal/i;->c(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1498
    :goto_0
    return-void

    .line 1496
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/bj;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/ak;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1439
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1441
    new-instance v1, Lcom/google/android/gms/plus/internal/bp;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bp;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ak;)V

    .line 1443
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/plus/internal/i;->f(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1449
    :goto_0
    return-void

    .line 1447
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/bp;->a(ILandroid/os/Bundle;Ljava/util/List;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/al;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 1454
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1456
    new-instance v1, Lcom/google/android/gms/plus/internal/br;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/br;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/al;)V

    .line 1458
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/plus/internal/i;->e(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1464
    :goto_0
    return-void

    .line 1462
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, p2}, Lcom/google/android/gms/plus/internal/br;->a(ILandroid/os/Bundle;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/am;Ljava/lang/String;ILjava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 1406
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1408
    new-instance v1, Lcom/google/android/gms/plus/internal/bt;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bt;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/am;)V

    .line 1410
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1417
    :goto_0
    return-void

    .line 1415
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v7, v7}, Lcom/google/android/gms/plus/internal/bt;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/an;Landroid/net/Uri;I)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1518
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1519
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1520
    const-string v0, "bounding_box"

    invoke-virtual {v1, v0, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1521
    new-instance v2, Lcom/google/android/gms/plus/internal/bv;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/plus/internal/bv;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/an;)V

    .line 1524
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v2, p2, v1}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;Landroid/net/Uri;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1528
    :goto_0
    return-void

    .line 1526
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v2, v0, v3, v3}, Lcom/google/android/gms/plus/internal/bv;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/ao;ILcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1715
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1717
    new-instance v1, Lcom/google/android/gms/plus/internal/bl;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bl;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ao;)V

    .line 1720
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;ILcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesRequest;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1725
    :goto_0
    return-void

    .line 1723
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/bl;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/smart_profile/PeopleForProfilesResponse;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/ap;IILjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1688
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1689
    new-instance v1, Lcom/google/android/gms/plus/internal/bx;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bx;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ap;)V

    .line 1692
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/google/android/gms/plus/internal/i;->b(Lcom/google/android/gms/plus/internal/f;IILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1696
    :goto_0
    return-void

    .line 1694
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/bx;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/PeopleFeed;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/aq;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1306
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1309
    new-instance v1, Lcom/google/android/gms/plus/internal/bz;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bz;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/aq;)V

    .line 1311
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1317
    :goto_0
    return-void

    .line 1315
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/bz;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/aq;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1321
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1324
    new-instance v1, Lcom/google/android/gms/plus/internal/bz;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bz;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/aq;)V

    .line 1326
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1332
    :goto_0
    return-void

    .line 1330
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/bz;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/ar;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1362
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1364
    new-instance v1, Lcom/google/android/gms/plus/internal/cb;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/cb;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ar;)V

    .line 1368
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/plus/internal/i;->c(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1372
    :goto_0
    return-void

    .line 1370
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/cb;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/as;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1376
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1378
    new-instance v1, Lcom/google/android/gms/plus/internal/cd;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/cd;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/as;)V

    .line 1381
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/plus/internal/i;->b(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1385
    :goto_0
    return-void

    .line 1383
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/cd;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Settings;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/at;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1349
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1352
    new-instance v1, Lcom/google/android/gms/plus/internal/cf;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/cf;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/at;)V

    .line 1354
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/model/posts/Post;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1358
    :goto_0
    return-void

    .line 1356
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/cf;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/model/posts/Post;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/au;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1502
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1504
    new-instance v1, Lcom/google/android/gms/plus/internal/ch;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/ch;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/au;)V

    .line 1507
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1513
    :goto_0
    return-void

    .line 1511
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/ch;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/av;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1701
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1703
    new-instance v1, Lcom/google/android/gms/plus/internal/bn;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bn;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/av;)V

    .line 1706
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1710
    :goto_0
    return-void

    .line 1708
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/bn;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/internal/model/smart_profile/CardsResponse;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/aw;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1655
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1656
    new-instance v1, Lcom/google/android/gms/plus/internal/cj;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/cj;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/aw;)V

    .line 1659
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1664
    :goto_0
    return-void

    .line 1662
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v6, v6}, Lcom/google/android/gms/plus/internal/cj;->b(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/internal/ax;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1675
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1677
    new-instance v1, Lcom/google/android/gms/plus/internal/cl;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/cl;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ax;)V

    .line 1679
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1683
    :goto_0
    return-void

    .line 1681
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/cl;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/UpgradeAccountEntity;)V

    goto :goto_0
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1739
    const-string v0, "com.google.android.gms.plus.service.internal.START"

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/plus/internal/ah;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 1548
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1549
    new-instance v1, Lcom/google/android/gms/plus/internal/bh;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bh;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/ah;)V

    .line 1551
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2, p2, p3}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;IILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1555
    :goto_0
    return-void

    .line 1553
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/plus/internal/bh;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/plus/internal/am;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1423
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1425
    new-instance v1, Lcom/google/android/gms/plus/internal/bt;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bt;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/am;)V

    .line 1428
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/google/android/gms/plus/internal/i;->a(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1434
    :goto_0
    return-void

    .line 1432
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/bt;->a(ILandroid/os/Bundle;Lcom/google/android/gms/plus/service/v1whitelisted/models/MomentsFeed;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/plus/internal/aq;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1389
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1392
    new-instance v1, Lcom/google/android/gms/plus/internal/bz;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/plus/internal/bz;-><init>(Lcom/google/android/gms/plus/internal/ay;Lcom/google/android/gms/plus/internal/aq;)V

    .line 1394
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/plus/internal/i;->b(Lcom/google/android/gms/plus/internal/f;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1400
    :goto_0
    return-void

    .line 1398
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/plus/internal/bz;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1734
    const-string v0, "com.google.android.gms.plus.internal.IPlusInternalService"

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1294
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->j()V

    .line 1297
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/ay;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/i;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/i;->a()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 1298
    :catch_0
    move-exception v0

    .line 1299
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
