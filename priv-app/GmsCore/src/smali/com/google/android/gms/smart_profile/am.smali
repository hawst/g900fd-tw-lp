.class final Lcom/google/android/gms/smart_profile/am;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/smart_profile/b/b;


# instance fields
.field private a:Lcom/google/android/gms/common/api/v;

.field private b:Landroid/support/v4/app/Fragment;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:[Lcom/google/c/f/c/a/i;

.field private g:Ljava/util/Map;

.field private h:Ljava/util/Map;

.field private i:Landroid/view/LayoutInflater;

.field private j:Landroid/content/Intent;

.field private k:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/google/android/gms/common/api/v;Landroid/content/Intent;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 95
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/am;->b:Landroid/support/v4/app/Fragment;

    .line 97
    iput-object p2, p0, Lcom/google/android/gms/smart_profile/am;->a:Lcom/google/android/gms/common/api/v;

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->b:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/am;->i:Landroid/view/LayoutInflater;

    .line 99
    iput-object p3, p0, Lcom/google/android/gms/smart_profile/am;->j:Landroid/content/Intent;

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->b:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/am;->k:Landroid/content/res/Resources;

    .line 101
    iput-object p4, p0, Lcom/google/android/gms/smart_profile/am;->d:Ljava/lang/String;

    .line 102
    iput-object p5, p0, Lcom/google/android/gms/smart_profile/am;->e:Ljava/lang/String;

    .line 103
    iput p6, p0, Lcom/google/android/gms/smart_profile/am;->c:I

    .line 104
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/am;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->j:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/smart_profile/am;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)V
    .locals 3

    .prologue
    .line 71
    invoke-static {p1, p2, p3, p4, p5}, Lcom/google/android/gms/smart_profile/aj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/am;->b:Landroid/support/v4/app/Fragment;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/Fragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/smart_profile/am;)Landroid/support/v4/app/Fragment;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->b:Landroid/support/v4/app/Fragment;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/smart_profile/am;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/smart_profile/am;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/smart_profile/am;)I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/android/gms/smart_profile/am;->c:I

    return v0
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 71
    check-cast p1, Landroid/widget/ImageView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final synthetic a(Landroid/view/View;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 71
    check-cast p1, Landroid/widget/ImageView;

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/am;->k:Landroid/content/res/Resources;

    invoke-direct {v0, v1, p2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/people/data/AudienceMember;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 119
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    new-array v2, v0, [Ljava/lang/String;

    .line 120
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 121
    invoke-interface {p2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->d()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v1

    .line 120
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->h:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    return-void
.end method

.method public final a(Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 111
    if-nez p1, :cond_0

    .line 112
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/am;->h:Ljava/util/Map;

    .line 116
    :goto_0
    return-void

    .line 114
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/am;->h:Ljava/util/Map;

    goto :goto_0
.end method

.method public final a([Lcom/google/c/f/c/a/i;)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/am;->f:[Lcom/google/c/f/c/a/i;

    .line 108
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->g:Ljava/util/Map;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->h:Ljava/util/Map;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/util/Map;)V
    .locals 2

    .prologue
    .line 127
    if-nez p1, :cond_0

    .line 128
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/smart_profile/am;->g:Ljava/util/Map;

    .line 132
    :goto_0
    return-void

    .line 130
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/smart_profile/am;->g:Ljava/util/Map;

    goto :goto_0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->f:[Lcom/google/c/f/c/a/i;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->f:[Lcom/google/c/f/c/a/i;

    array-length v0, v0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->f:[Lcom/google/c/f/c/a/i;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->f:[Lcom/google/c/f/c/a/i;

    aget-object v0, v0, p1

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 155
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 12
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "InflateParams"
        }
    .end annotation

    .prologue
    const/4 v8, 0x0

    const/4 v11, 0x4

    const/4 v5, 0x1

    const/4 v9, 0x0

    .line 161
    invoke-virtual {p0, p1}, Lcom/google/android/gms/smart_profile/am;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/c/f/c/a/i;

    .line 164
    if-nez p2, :cond_1

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->i:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->cP:I

    invoke-virtual {v0, v1, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    .line 166
    new-instance v1, Lcom/google/android/gms/smart_profile/aq;

    invoke-direct {v1, v9}, Lcom/google/android/gms/smart_profile/aq;-><init>(B)V

    .line 167
    sget v0, Lcom/google/android/gms/j;->by:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v1, Lcom/google/android/gms/smart_profile/aq;->a:Landroid/widget/ImageView;

    .line 168
    sget v0, Lcom/google/android/gms/j;->ex:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/smart_profile/aq;->b:Landroid/widget/TextView;

    .line 169
    sget v0, Lcom/google/android/gms/j;->me:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/smart_profile/aq;->c:Landroid/widget/TextView;

    .line 170
    sget v0, Lcom/google/android/gms/j;->T:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/smart_profile/aq;->d:Landroid/widget/TextView;

    .line 172
    sget v0, Lcom/google/android/gms/j;->kf:I

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v1, Lcom/google/android/gms/smart_profile/aq;->e:Landroid/widget/TextView;

    .line 174
    invoke-virtual {p2, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    move-object v7, v1

    .line 179
    :goto_0
    new-instance v0, Lcom/google/android/gms/smart_profile/an;

    invoke-direct {v0, p0, v6}, Lcom/google/android/gms/smart_profile/an;-><init>(Lcom/google/android/gms/smart_profile/am;Lcom/google/c/f/c/a/i;)V

    invoke-virtual {p2, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 189
    new-instance v0, Lcom/google/android/gms/smart_profile/b/a;

    iget-object v2, p0, Lcom/google/android/gms/smart_profile/am;->a:Lcom/google/android/gms/common/api/v;

    iget-object v3, v7, Lcom/google/android/gms/smart_profile/aq;->a:Landroid/widget/ImageView;

    iget-object v1, v6, Lcom/google/c/f/c/a/i;->c:Lcom/google/c/f/c/a/f;

    iget-object v1, v1, Lcom/google/c/f/c/a/f;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    const-string v4, "//"

    invoke-virtual {v1, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v10, "https:"

    invoke-direct {v4, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_9

    sget-object v1, Lcom/google/android/gms/smart_profile/a/a;->B:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v4, v1

    :goto_1
    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/smart_profile/b/a;-><init>(Lcom/google/android/gms/smart_profile/b/b;Lcom/google/android/gms/common/api/v;Landroid/view/View;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/gms/smart_profile/b/a;->a()V

    .line 194
    iget-object v0, v7, Lcom/google/android/gms/smart_profile/aq;->b:Landroid/widget/TextView;

    iget-object v1, v6, Lcom/google/c/f/c/a/i;->c:Lcom/google/c/f/c/a/f;

    iget-object v1, v1, Lcom/google/c/f/c/a/f;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    iget-object v0, v6, Lcom/google/c/f/c/a/i;->c:Lcom/google/c/f/c/a/f;

    iget-object v1, v0, Lcom/google/c/f/c/a/f;->t:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/am;->k:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/gms/p;->rT:I

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/c/f/c/a/f;->t:Ljava/lang/String;

    aput-object v0, v3, v9

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 197
    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 198
    iget-object v1, v7, Lcom/google/android/gms/smart_profile/aq;->c:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 199
    iget-object v0, v7, Lcom/google/android/gms/smart_profile/aq;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 204
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->h:Ljava/util/Map;

    iget-object v1, v6, Lcom/google/c/f/c/a/i;->a:Lcom/google/c/f/c/a/e;

    iget-object v1, v1, Lcom/google/c/f/c/a/e;->c:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/people/internal/at;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 210
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/am;->k:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/gms/f;->aw:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    .line 211
    iget-object v2, v7, Lcom/google/android/gms/smart_profile/aq;->d:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/google/android/gms/smart_profile/bf;->a(ILandroid/widget/TextView;)V

    .line 212
    iget-object v2, v7, Lcom/google/android/gms/smart_profile/aq;->e:Landroid/widget/TextView;

    invoke-static {v1, v2}, Lcom/google/android/gms/smart_profile/bf;->a(ILandroid/widget/TextView;)V

    .line 214
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 215
    if-eqz v0, :cond_8

    array-length v1, v0

    if-lez v1, :cond_8

    .line 216
    iget-object v1, v7, Lcom/google/android/gms/smart_profile/aq;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v11}, Landroid/widget/TextView;->setVisibility(I)V

    .line 219
    array-length v1, v0

    if-ne v1, v5, :cond_6

    .line 220
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/am;->g:Ljava/util/Map;

    aget-object v2, v0, v9

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v2, v1

    .line 226
    :goto_4
    array-length v5, v0

    move v3, v9

    :goto_5
    if-ge v3, v5, :cond_7

    aget-object v8, v0, v3

    .line 227
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/am;->g:Ljava/util/Map;

    invoke-interface {v1, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v8, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v1

    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_5

    .line 176
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/smart_profile/aq;

    move-object v7, v0

    goto/16 :goto_0

    .line 196
    :cond_2
    iget-object v1, v0, Lcom/google/c/f/c/a/f;->r:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/am;->k:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/gms/p;->rU:I

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/c/f/c/a/f;->r:Ljava/lang/String;

    aput-object v0, v3, v9

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_3
    iget-object v1, v0, Lcom/google/c/f/c/a/f;->s:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/smart_profile/am;->k:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/gms/p;->rV:I

    new-array v3, v5, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/c/f/c/a/f;->s:Ljava/lang/String;

    aput-object v0, v3, v9

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_4
    move-object v0, v8

    goto/16 :goto_2

    .line 201
    :cond_5
    iget-object v0, v7, Lcom/google/android/gms/smart_profile/aq;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_3

    .line 222
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/smart_profile/am;->k:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/gms/p;->wp:I

    new-array v3, v5, [Ljava/lang/Object;

    array-length v5, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v9

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    goto :goto_4

    .line 230
    :cond_7
    iget-object v0, v7, Lcom/google/android/gms/smart_profile/aq;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 231
    iget-object v0, v7, Lcom/google/android/gms/smart_profile/aq;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 237
    :goto_6
    iget-object v0, v6, Lcom/google/c/f/c/a/i;->a:Lcom/google/c/f/c/a/e;

    iget-object v0, v0, Lcom/google/c/f/c/a/e;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 240
    iget-object v1, v7, Lcom/google/android/gms/smart_profile/aq;->d:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/gms/smart_profile/ao;

    invoke-direct {v2, p0, v0, v4}, Lcom/google/android/gms/smart_profile/ao;-><init>(Lcom/google/android/gms/smart_profile/am;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 247
    iget-object v1, v7, Lcom/google/android/gms/smart_profile/aq;->e:Landroid/widget/TextView;

    new-instance v2, Lcom/google/android/gms/smart_profile/ap;

    invoke-direct {v2, p0, v0, v4}, Lcom/google/android/gms/smart_profile/ap;-><init>(Lcom/google/android/gms/smart_profile/am;Ljava/lang/String;Ljava/util/List;)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 254
    return-object p2

    .line 233
    :cond_8
    iget-object v0, v7, Lcom/google/android/gms/smart_profile/aq;->d:Landroid/widget/TextView;

    invoke-virtual {v0, v9}, Landroid/widget/TextView;->setVisibility(I)V

    .line 234
    iget-object v0, v7, Lcom/google/android/gms/smart_profile/aq;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_6

    :cond_9
    move-object v4, v1

    goto/16 :goto_1
.end method

.method public final isEmpty()Z
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->f:[Lcom/google/c/f/c/a/i;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/smart_profile/am;->f:[Lcom/google/c/f/c/a/i;

    array-length v0, v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
