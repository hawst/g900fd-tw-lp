.class final Lcom/google/android/gms/car/og;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ljava/util/concurrent/Callable;

.field private final b:Ljava/util/concurrent/CountDownLatch;

.field private c:Ljava/lang/Object;

.field private d:Ljava/lang/IllegalStateException;


# direct methods
.method constructor <init>(Ljava/util/concurrent/Callable;)V
    .locals 2

    .prologue
    .line 237
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 233
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/og;->b:Ljava/util/concurrent/CountDownLatch;

    .line 238
    iput-object p1, p0, Lcom/google/android/gms/car/og;->a:Ljava/util/concurrent/Callable;

    .line 239
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/gms/car/og;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/car/og;->d:Ljava/lang/IllegalStateException;

    if-eqz v0, :cond_0

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/car/og;->d:Ljava/lang/IllegalStateException;

    throw v0

    .line 264
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/og;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public final run()V
    .locals 2

    .prologue
    .line 244
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/og;->a:Ljava/util/concurrent/Callable;

    invoke-interface {v0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/og;->c:Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 250
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/og;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 251
    return-void

    .line 245
    :catch_0
    move-exception v0

    .line 246
    iput-object v0, p0, Lcom/google/android/gms/car/og;->d:Ljava/lang/IllegalStateException;

    goto :goto_0

    .line 247
    :catch_1
    move-exception v0

    .line 248
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
