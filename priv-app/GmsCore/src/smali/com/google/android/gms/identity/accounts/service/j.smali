.class public final Lcom/google/android/gms/identity/accounts/service/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/identity/accounts/service/d;


# instance fields
.field private final a:Lcom/google/android/gms/identity/accounts/service/l;

.field private final b:[B


# direct methods
.method public constructor <init>(Lcom/google/android/gms/identity/accounts/service/l;[B)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/gms/identity/accounts/service/j;->a:Lcom/google/android/gms/identity/accounts/service/l;

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/identity/accounts/service/j;->b:[B

    .line 27
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/identity/accounts/service/j;->a:Lcom/google/android/gms/identity/accounts/service/l;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/identity/accounts/service/l;->a(Lcom/google/android/gms/identity/accounts/api/AccountData;)V

    .line 37
    return-void
.end method

.method public final a(Lcom/google/android/gms/identity/accounts/security/a;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 31
    iget-object v2, p0, Lcom/google/android/gms/identity/accounts/service/j;->a:Lcom/google/android/gms/identity/accounts/service/l;

    iget-object v3, p0, Lcom/google/android/gms/identity/accounts/service/j;->b:[B

    const-string v0, "Encrypted bytes must not be null."

    invoke-static {v3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    array-length v0, v3

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v4, "Encrypted bytes must not be empty."

    invoke-static {v0, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    invoke-static {v3}, Lcom/google/android/gms/identity/accounts/security/a;->a([B)Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;

    move-result-object v0

    if-nez v0, :cond_1

    move-object v0, v1

    :goto_1
    invoke-interface {v2, v0}, Lcom/google/android/gms/identity/accounts/service/l;->a(Lcom/google/android/gms/identity/accounts/api/AccountData;)V

    .line 32
    return-void

    .line 31
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v0}, Lcom/google/android/gms/identity/accounts/security/a;->a(Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;)Z

    move-result v3

    if-nez v3, :cond_2

    move-object v0, v1

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v0}, Lcom/google/android/gms/identity/accounts/security/a;->b(Lcom/google/android/gms/identity/accounts/security/EncryptedAccountData;)Lcom/google/android/gms/identity/accounts/api/AccountData;

    move-result-object v0

    goto :goto_1
.end method
