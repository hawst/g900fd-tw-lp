.class public final Lcom/google/android/gms/plus/sharebox/au;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/plus/internal/ar;
.implements Lcom/google/android/gms/plus/internal/as;
.implements Lcom/google/android/gms/plus/internal/at;


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:Lcom/google/android/gms/common/api/Status;


# instance fields
.field private A:J

.field private final B:Lcom/google/android/gms/common/api/aq;

.field private final C:Lcom/google/android/gms/common/api/aq;

.field private final D:Lcom/google/android/gms/common/api/aq;

.field private final E:Lcom/google/android/gms/common/api/aq;

.field private final F:Lcom/google/android/gms/common/api/aq;

.field private final G:Lcom/google/android/gms/common/api/aq;

.field private c:Lcom/google/android/gms/plus/internal/ad;

.field private d:Lcom/google/android/gms/plus/internal/ab;

.field private e:Lcom/google/android/gms/plus/sharebox/be;

.field private f:Lcom/google/android/gms/common/api/v;

.field private g:Lcom/google/android/gms/plus/sharebox/bd;

.field private h:Lcom/google/android/gms/plus/sharebox/bf;

.field private i:Lcom/google/android/gms/plus/sharebox/bg;

.field private j:Ljava/lang/String;

.field private k:[Ljava/lang/String;

.field private l:Ljava/lang/String;

.field private m:I

.field private n:Z

.field private o:Lcom/google/android/gms/plus/model/posts/Post;

.field private p:Lcom/google/android/gms/plus/model/posts/Settings;

.field private q:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

.field private r:Lcom/google/android/gms/plus/data/a/a;

.field private s:Ljava/lang/String;

.field private t:Landroid/graphics/Bitmap;

.field private u:Z

.field private v:Ljava/lang/String;

.field private w:Z

.field private x:Ljava/lang/String;

.field private y:Lcom/google/android/gms/common/people/data/Audience;

.field private final z:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 78
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "https://www.googleapis.com/auth/plus.me"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "https://www.googleapis.com/auth/plus.stream.write"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "https://www.googleapis.com/auth/plus.settings"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "https://www.googleapis.com/auth/plus.pages.manage"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/plus/sharebox/au;->a:[Ljava/lang/String;

    .line 85
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/plus/sharebox/au;->b:Lcom/google/android/gms/common/api/Status;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 476
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 227
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->z:Ljava/util/ArrayList;

    .line 229
    sget-object v0, Lcom/google/android/gms/plus/c/a;->af:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/plus/sharebox/au;->A:J

    .line 814
    new-instance v0, Lcom/google/android/gms/plus/sharebox/av;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/sharebox/av;-><init>(Lcom/google/android/gms/plus/sharebox/au;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->B:Lcom/google/android/gms/common/api/aq;

    .line 841
    new-instance v0, Lcom/google/android/gms/plus/sharebox/aw;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/sharebox/aw;-><init>(Lcom/google/android/gms/plus/sharebox/au;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->C:Lcom/google/android/gms/common/api/aq;

    .line 856
    new-instance v0, Lcom/google/android/gms/plus/sharebox/ax;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/sharebox/ax;-><init>(Lcom/google/android/gms/plus/sharebox/au;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->D:Lcom/google/android/gms/common/api/aq;

    .line 875
    new-instance v0, Lcom/google/android/gms/plus/sharebox/ay;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/sharebox/ay;-><init>(Lcom/google/android/gms/plus/sharebox/au;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->E:Lcom/google/android/gms/common/api/aq;

    .line 912
    new-instance v0, Lcom/google/android/gms/plus/sharebox/az;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/sharebox/az;-><init>(Lcom/google/android/gms/plus/sharebox/au;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->F:Lcom/google/android/gms/common/api/aq;

    .line 929
    new-instance v0, Lcom/google/android/gms/plus/sharebox/ba;

    invoke-direct {v0, p0}, Lcom/google/android/gms/plus/sharebox/ba;-><init>(Lcom/google/android/gms/plus/sharebox/au;)V

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->G:Lcom/google/android/gms/common/api/aq;

    .line 477
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->c:Lcom/google/android/gms/plus/internal/ad;

    .line 478
    return-void
.end method

.method static synthetic A(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/aq;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->E:Lcom/google/android/gms/common/api/aq;

    return-object v0
.end method

.method static synthetic B(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->v:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/au;I)I
    .locals 0

    .prologue
    .line 71
    iput p1, p0, Lcom/google/android/gms/plus/sharebox/au;->m:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/au;J)J
    .locals 1

    .prologue
    .line 71
    iput-wide p1, p0, Lcom/google/android/gms/plus/sharebox/au;->A:J

    return-wide p1
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/au;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/au;->t:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/plus/sharebox/au;
    .locals 3

    .prologue
    .line 462
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "specified_account_name"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Lcom/google/android/gms/plus/sharebox/au;

    invoke-direct {v2}, Lcom/google/android/gms/plus/sharebox/au;-><init>()V

    iput-object v0, v2, Lcom/google/android/gms/plus/sharebox/au;->c:Lcom/google/android/gms/plus/internal/ad;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/plus/sharebox/au;->setArguments(Landroid/os/Bundle;)V

    return-object v2
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bf;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/au;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/au;->l:Ljava/lang/String;

    return-object p1
.end method

.method private a(Lcom/google/android/gms/common/api/Status;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 940
    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->x:Ljava/lang/String;

    .line 941
    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->y:Lcom/google/android/gms/common/people/data/Audience;

    .line 943
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    if-eqz v0, :cond_0

    .line 944
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/plus/sharebox/bf;->a(Lcom/google/android/gms/common/api/Status;Ljava/lang/String;[Ljava/lang/String;)V

    .line 947
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/plus/sharebox/au;Lcom/google/android/gms/common/api/Status;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0, p1, p2, p4}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/api/Status;Ljava/lang/String;[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/internal/ab;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/plus/sharebox/au;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/au;->s:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic c(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/sharebox/bg;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->i:Lcom/google/android/gms/plus/sharebox/bg;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->l:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/plus/sharebox/au;)[Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->k:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/plus/sharebox/au;)I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/android/gms/plus/sharebox/au;->m:I

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/plus/sharebox/au;)V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->z:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->z:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    move v3, v2

    :goto_1
    if-ge v3, v4, :cond_7

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/sharebox/bb;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bb;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bb;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bb;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bb;->b()Z

    move-result v1

    if-nez v1, :cond_3

    :cond_1
    const-string v0, "ShareBox"

    const-string v1, "Unhandled queued navigation log request"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_2
    move v1, v2

    goto :goto_2

    :cond_3
    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/bb;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bb;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    goto :goto_3

    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bb;->c()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v0, "ShareBox"

    const-string v1, "Unhandled queued log request"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_5
    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/bb;->c()Z

    move-result v1

    if-nez v1, :cond_6

    const-string v0, "ShareBox"

    const-string v1, "Unhandled queued action log request"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_3

    :cond_6
    iget-object v1, v0, Lcom/google/android/gms/plus/sharebox/bb;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v6, v0, Lcom/google/android/gms/plus/sharebox/bb;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iget-object v7, v0, Lcom/google/android/gms/plus/sharebox/bb;->d:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bb;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {p0, v1, v6, v7, v0}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V

    goto :goto_3

    :cond_7
    return-void
.end method

.method static synthetic h(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/plus/sharebox/au;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/au;->n:Z

    return v0
.end method

.method static synthetic j(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/model/posts/Post;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->o:Lcom/google/android/gms/plus/model/posts/Post;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/model/posts/Settings;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->p:Lcom/google/android/gms/plus/model/posts/Settings;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/data/a/a;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->r:Lcom/google/android/gms/plus/data/a/a;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/gms/plus/sharebox/au;)J
    .locals 2

    .prologue
    .line 71
    iget-wide v0, p0, Lcom/google/android/gms/plus/sharebox/au;->A:J

    return-wide v0
.end method

.method static synthetic n(Lcom/google/android/gms/plus/sharebox/au;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/au;->u:Z

    return v0
.end method

.method static synthetic o(Lcom/google/android/gms/plus/sharebox/au;)Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/au;->u:Z

    return v0
.end method

.method static synthetic p()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 71
    sget-object v0, Lcom/google/android/gms/plus/sharebox/au;->b:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method static synthetic p(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/aq;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->F:Lcom/google/android/gms/common/api/aq;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->v:Ljava/lang/String;

    return-object v0
.end method

.method private q()V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 786
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->y:Lcom/google/android/gms/common/people/data/Audience;

    invoke-static {v0}, Lcom/google/android/gms/plus/sharebox/j;->a(Lcom/google/android/gms/common/people/data/Audience;)Ljava/util/List;

    move-result-object v5

    .line 788
    if-eqz v5, :cond_0

    invoke-interface {v5}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 789
    :cond_0
    const-string v0, "ShareBox"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 790
    const-string v0, "ShareBox"

    const-string v1, "No people to add to circle"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 792
    :cond_1
    sget-object v0, Lcom/google/android/gms/plus/sharebox/au;->b:Lcom/google/android/gms/common/api/Status;

    invoke-direct {p0, v0, v2, v2}, Lcom/google/android/gms/plus/sharebox/au;->a(Lcom/google/android/gms/common/api/Status;Ljava/lang/String;[Ljava/lang/String;)V

    .line 804
    :cond_2
    :goto_0
    return-void

    .line 796
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 797
    sget-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/au;->i:Lcom/google/android/gms/plus/sharebox/bg;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/au;->x:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/j;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->G:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0

    .line 801
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-nez v0, :cond_2

    .line 802
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    goto :goto_0
.end method

.method static synthetic r(Lcom/google/android/gms/plus/sharebox/au;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/au;->w:Z

    return v0
.end method

.method static synthetic s(Lcom/google/android/gms/plus/sharebox/au;)Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/au;->w:Z

    return v0
.end method

.method static synthetic t(Lcom/google/android/gms/plus/sharebox/au;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/au;->q()V

    return-void
.end method

.method static synthetic u(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/plus/circles/AddToCircleConsentData;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->q:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    return-object v0
.end method

.method static synthetic v(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/aq;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->B:Lcom/google/android/gms/common/api/aq;

    return-object v0
.end method

.method static synthetic w(Lcom/google/android/gms/plus/sharebox/au;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->s:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic x(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/aq;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->C:Lcom/google/android/gms/common/api/aq;

    return-object v0
.end method

.method static synthetic y(Lcom/google/android/gms/plus/sharebox/au;)Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->t:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method static synthetic z(Lcom/google/android/gms/plus/sharebox/au;)Lcom/google/android/gms/common/api/aq;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->D:Lcom/google/android/gms/common/api/aq;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/plus/internal/ab;
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/plus/circles/AddToCircleConsentData;)V
    .locals 2

    .prologue
    .line 827
    iput-object p2, p0, Lcom/google/android/gms/plus/sharebox/au;->q:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    .line 828
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    if-eqz v0, :cond_0

    .line 829
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->q:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    invoke-interface {v0, p1}, Lcom/google/android/gms/plus/sharebox/bf;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 831
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/data/a/a;)V
    .locals 1

    .prologue
    .line 835
    iput-object p2, p0, Lcom/google/android/gms/plus/sharebox/au;->r:Lcom/google/android/gms/plus/data/a/a;

    .line 836
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    if-eqz v0, :cond_0

    .line 837
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/plus/sharebox/bf;->a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/data/a/a;)V

    .line 839
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 1

    .prologue
    .line 719
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/au;->n:Z

    if-eqz v0, :cond_0

    .line 720
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    if-eqz v0, :cond_0

    .line 721
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/plus/sharebox/bf;->a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/posts/Post;)V

    .line 724
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/au;->n:Z

    .line 725
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/posts/Settings;)V
    .locals 1

    .prologue
    .line 808
    iput-object p2, p0, Lcom/google/android/gms/plus/sharebox/au;->p:Lcom/google/android/gms/plus/model/posts/Settings;

    .line 809
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    if-eqz v0, :cond_0

    .line 810
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/plus/sharebox/bf;->a(Lcom/google/android/gms/common/c;Lcom/google/android/gms/plus/model/posts/Settings;)V

    .line 812
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 1

    .prologue
    .line 970
    sget-object v0, Lcom/google/android/gms/common/analytics/v;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/plus/sharebox/au;->b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 971
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 3

    .prologue
    .line 954
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/au;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 955
    if-nez v0, :cond_0

    .line 967
    :goto_0
    return-void

    .line 958
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/au;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 959
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->j:Ljava/lang/String;

    invoke-static {v0, v1, p1, p2, v2}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_0

    .line 962
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->z:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/gms/plus/sharebox/bc;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/plus/sharebox/bc;-><init>(B)V

    iput-object p1, v1, Lcom/google/android/gms/plus/sharebox/bc;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iput-object p2, v1, Lcom/google/android/gms/plus/sharebox/bc;->b:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/bc;->a()Lcom/google/android/gms/plus/sharebox/bb;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)V
    .locals 3

    .prologue
    .line 991
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/au;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 992
    if-nez v0, :cond_0

    .line 1006
    :goto_0
    return-void

    .line 995
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/au;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 996
    new-instance v1, Lcom/google/android/gms/common/server/y;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/server/y;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/server/y;->c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    if-nez p2, :cond_1

    sget-object p2, Lcom/google/android/gms/common/analytics/v;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    :cond_1
    invoke-virtual {v1, p2}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->j:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/server/y;->a(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;

    move-result-object v1

    if-eqz p3, :cond_2

    invoke-virtual {v1, p3}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lcom/google/android/gms/common/server/y;

    :cond_2
    if-eqz p4, :cond_3

    invoke-virtual {v1, p4}, Lcom/google/android/gms/common/server/y;->a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)Lcom/google/android/gms/common/server/y;

    :cond_3
    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/y;)V

    goto :goto_0

    .line 999
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->z:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/gms/plus/sharebox/bc;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/plus/sharebox/bc;-><init>(B)V

    iput-object p2, v1, Lcom/google/android/gms/plus/sharebox/bc;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iput-object p1, v1, Lcom/google/android/gms/plus/sharebox/bc;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iput-object p3, v1, Lcom/google/android/gms/plus/sharebox/bc;->d:Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;

    iput-object p4, v1, Lcom/google/android/gms/plus/sharebox/bc;->e:Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/bc;->a()Lcom/google/android/gms/plus/sharebox/bb;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/plus/model/posts/Post;)V
    .locals 2

    .prologue
    .line 698
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/au;->n:Z

    if-eqz v0, :cond_0

    .line 699
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "One post at a time please"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 701
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/au;->n:Z

    .line 702
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/au;->o:Lcom/google/android/gms/plus/model/posts/Post;

    .line 703
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 704
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->e:Lcom/google/android/gms/plus/sharebox/be;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/sharebox/be;->T_()V

    .line 708
    :cond_1
    :goto_0
    return-void

    .line 705
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-nez v0, :cond_1

    .line 706
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/people/data/Audience;)V
    .locals 3

    .prologue
    .line 765
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/au;->w:Z

    if-eqz v0, :cond_0

    .line 766
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Add people to circle request already initiated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 771
    :cond_0
    sget-object v0, Lcom/google/android/gms/people/x;->m:Lcom/google/android/gms/people/v;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->g:Lcom/google/android/gms/plus/sharebox/bd;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/people/v;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/w;)Lcom/google/android/gms/common/api/am;

    .line 774
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/au;->w:Z

    .line 775
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/au;->x:Ljava/lang/String;

    .line 776
    iput-object p2, p0, Lcom/google/android/gms/plus/sharebox/au;->y:Lcom/google/android/gms/common/people/data/Audience;

    .line 777
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 778
    invoke-direct {p0}, Lcom/google/android/gms/plus/sharebox/au;->q()V

    .line 782
    :cond_1
    :goto_0
    return-void

    .line 779
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-nez v0, :cond_1

    .line 780
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    goto :goto_0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->l:Ljava/lang/String;

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 6

    .prologue
    .line 974
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/au;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 975
    if-nez v0, :cond_0

    .line 987
    :goto_0
    return-void

    .line 978
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/au;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 979
    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->l:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v2}, Lcom/google/android/gms/plus/sharebox/bf;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v2

    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/au;->j:Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/common/server/x;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Ljava/lang/String;)V

    goto :goto_0

    .line 982
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->z:Ljava/util/ArrayList;

    new-instance v1, Lcom/google/android/gms/plus/sharebox/bc;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/plus/sharebox/bc;-><init>(B)V

    iput-object p1, v1, Lcom/google/android/gms/plus/sharebox/bc;->c:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    iput-object p2, v1, Lcom/google/android/gms/plus/sharebox/bc;->a:Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/sharebox/bc;->a()Lcom/google/android/gms/plus/sharebox/bb;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 739
    iget-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/au;->u:Z

    if-eqz v0, :cond_0

    .line 740
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Create circle request already initiated"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 745
    :cond_0
    sget-object v0, Lcom/google/android/gms/people/x;->m:Lcom/google/android/gms/people/v;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->g:Lcom/google/android/gms/plus/sharebox/bd;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/people/v;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/w;)Lcom/google/android/gms/common/api/am;

    .line 748
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/plus/sharebox/au;->u:Z

    .line 749
    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/au;->v:Ljava/lang/String;

    .line 750
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 751
    sget-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->l:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/au;->i:Lcom/google/android/gms/plus/sharebox/bg;

    invoke-virtual {v3}, Lcom/google/android/gms/plus/sharebox/bg;->b()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/au;->v:Ljava/lang/String;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/people/j;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->F:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 757
    :cond_1
    :goto_0
    return-void

    .line 754
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-nez v0, :cond_1

    .line 755
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->l:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 513
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->k:[Ljava/lang/String;

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 521
    iget v0, p0, Lcom/google/android/gms/plus/sharebox/au;->m:I

    return v0
.end method

.method public final f()Lcom/google/android/gms/plus/model/posts/Settings;
    .locals 1

    .prologue
    .line 528
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->p:Lcom/google/android/gms/plus/model/posts/Settings;

    return-object v0
.end method

.method public final g()Lcom/google/android/gms/plus/data/a/a;
    .locals 1

    .prologue
    .line 536
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->r:Lcom/google/android/gms/plus/data/a/a;

    return-object v0
.end method

.method public final h()Lcom/google/android/gms/plus/circles/AddToCircleConsentData;
    .locals 1

    .prologue
    .line 540
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->q:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    return-object v0
.end method

.method public final i()Z
    .locals 1

    .prologue
    .line 544
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->q:Lcom/google/android/gms/plus/circles/AddToCircleConsentData;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 551
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->p:Lcom/google/android/gms/plus/model/posts/Settings;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Z
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->r:Lcom/google/android/gms/plus/data/a/a;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final l()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->t:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 572
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->t:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final n()V
    .locals 2

    .prologue
    .line 687
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/au;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 688
    invoke-static {v0}, Lcom/google/android/gms/common/util/a;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->k:[Ljava/lang/String;

    .line 689
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 690
    return-void
.end method

.method public final o()Z
    .locals 1

    .prologue
    .line 731
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->p:Lcom/google/android/gms/plus/model/posts/Settings;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 600
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 601
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/au;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    .line 602
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 661
    :cond_0
    :goto_0
    return-void

    .line 606
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bf;->k()Lcom/google/android/gms/plus/sharebox/bg;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->i:Lcom/google/android/gms/plus/sharebox/bg;

    .line 607
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v0}, Lcom/google/android/gms/plus/sharebox/bf;->getCallingPackage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->j:Ljava/lang/String;

    .line 610
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->j:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 611
    invoke-static {v0}, Lcom/google/android/gms/common/util/a;->a(Ljava/util/List;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->k:[Ljava/lang/String;

    .line 612
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/au;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v2, "specified_account_name"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 613
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->j:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/au;->k:[Ljava/lang/String;

    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/plus/sharebox/at;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 617
    new-instance v2, Lcom/google/android/gms/plus/internal/cn;

    invoke-direct {v2, v1}, Lcom/google/android/gms/plus/internal/cn;-><init>(Landroid/content/Context;)V

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/au;->j:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/gms/plus/internal/cn;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    invoke-interface {v3}, Lcom/google/android/gms/plus/sharebox/bf;->j()Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/plus/internal/cn;->f:Lcom/google/android/gms/plus/internal/PlusCommonExtras;

    iput-object v0, v2, Lcom/google/android/gms/plus/internal/cn;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->i:Lcom/google/android/gms/plus/sharebox/bg;

    iget-object v0, v0, Lcom/google/android/gms/plus/sharebox/bg;->m:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/gms/plus/internal/cn;->e:Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/plus/sharebox/au;->a:[Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/plus/internal/cn;->a([Ljava/lang/String;)Lcom/google/android/gms/plus/internal/cn;

    move-result-object v0

    .line 623
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->i:Lcom/google/android/gms/plus/sharebox/bg;

    iget-object v2, v2, Lcom/google/android/gms/plus/sharebox/bg;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/plus/sharebox/at;->c(Landroid/app/Activity;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 625
    new-array v2, v6, [Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/gms/plus/internal/cn;->d:[Ljava/lang/String;

    .line 628
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    if-nez v2, :cond_3

    .line 629
    new-instance v2, Lcom/google/android/gms/plus/sharebox/be;

    invoke-direct {v2, p0, v6}, Lcom/google/android/gms/plus/sharebox/be;-><init>(Lcom/google/android/gms/plus/sharebox/au;B)V

    iput-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->e:Lcom/google/android/gms/plus/sharebox/be;

    .line 631
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->c:Lcom/google/android/gms/plus/internal/ad;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/cn;->b()Lcom/google/android/gms/plus/internal/PlusSession;

    move-result-object v0

    iget-object v4, p0, Lcom/google/android/gms/plus/sharebox/au;->e:Lcom/google/android/gms/plus/sharebox/be;

    iget-object v5, p0, Lcom/google/android/gms/plus/sharebox/au;->e:Lcom/google/android/gms/plus/sharebox/be;

    invoke-interface {v2, v3, v0, v4, v5}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;Lcom/google/android/gms/plus/internal/PlusSession;Lcom/google/android/gms/common/es;Lcom/google/android/gms/common/et;)Lcom/google/android/gms/plus/internal/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    .line 638
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->a()V

    .line 641
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    if-nez v0, :cond_0

    .line 645
    invoke-virtual {p0}, Lcom/google/android/gms/plus/sharebox/au;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->j:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0x50

    .line 648
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->i:Lcom/google/android/gms/plus/sharebox/bg;

    iget-object v2, v2, Lcom/google/android/gms/plus/sharebox/bg;->m:Ljava/lang/String;

    if-eqz v2, :cond_4

    .line 650
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->i:Lcom/google/android/gms/plus/sharebox/bg;

    iget-object v2, v2, Lcom/google/android/gms/plus/sharebox/bg;->m:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 655
    :cond_4
    :goto_2
    new-instance v2, Lcom/google/android/gms/plus/sharebox/bd;

    invoke-direct {v2, p0, v6}, Lcom/google/android/gms/plus/sharebox/bd;-><init>(Lcom/google/android/gms/plus/sharebox/au;B)V

    iput-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->g:Lcom/google/android/gms/plus/sharebox/bd;

    .line 656
    iget-object v2, p0, Lcom/google/android/gms/plus/sharebox/au;->c:Lcom/google/android/gms/plus/internal/ad;

    iget-object v3, p0, Lcom/google/android/gms/plus/sharebox/au;->j:Ljava/lang/String;

    invoke-interface {v2, v1, v0, v3}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;ILjava/lang/String;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    .line 658
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->g:Lcom/google/android/gms/plus/sharebox/bd;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    .line 659
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    iget-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->g:Lcom/google/android/gms/plus/sharebox/bd;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/y;)V

    goto/16 :goto_0

    .line 645
    :cond_5
    const/16 v0, 0x64

    goto :goto_1

    :catch_0
    move-exception v2

    goto :goto_2
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 3

    .prologue
    .line 577
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 578
    instance-of v0, p1, Lcom/google/android/gms/plus/sharebox/bf;

    if-nez v0, :cond_0

    .line 579
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Host must implement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/gms/plus/sharebox/bf;

    invoke-virtual {v2}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 582
    :cond_0
    check-cast p1, Lcom/google/android/gms/plus/sharebox/bf;

    iput-object p1, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    .line 583
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 593
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 594
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/plus/sharebox/au;->setRetainInstance(Z)V

    .line 595
    return-void
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 666
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 667
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->c_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->n_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 668
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    invoke-interface {v0}, Lcom/google/android/gms/plus/internal/ab;->b()V

    .line 670
    :cond_1
    iput-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->d:Lcom/google/android/gms/plus/internal/ab;

    .line 672
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 673
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 675
    :cond_3
    iput-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->f:Lcom/google/android/gms/common/api/v;

    .line 677
    iput-object v1, p0, Lcom/google/android/gms/plus/sharebox/au;->l:Ljava/lang/String;

    .line 678
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/plus/sharebox/au;->m:I

    .line 679
    return-void
.end method

.method public final onDetach()V
    .locals 1

    .prologue
    .line 587
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDetach()V

    .line 588
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/plus/sharebox/au;->h:Lcom/google/android/gms/plus/sharebox/bf;

    .line 589
    return-void
.end method
