.class public final Lcom/google/android/gms/drive/ui/picker/a/b;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/ui/picker/a/r;


# static fields
.field private static final a:I


# instance fields
.field private final b:Lcom/google/android/gms/drive/ui/picker/a/m;

.field private final c:Landroid/view/LayoutInflater;

.field private d:I

.field private final e:Lcom/google/android/gms/drive/ui/picker/view/i;

.field private final f:Lcom/google/android/gms/drive/g/av;

.field private final g:Lcom/google/android/gms/drive/ui/picker/a/t;

.field private final h:Lcom/google/android/gms/drive/ui/picker/a/s;

.field private i:Lcom/google/android/gms/drive/ui/picker/a/l;

.field private j:Lcom/google/android/gms/drive/ui/picker/a/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    sget v0, Lcom/google/android/gms/l;->ap:I

    sput v0, Lcom/google/android/gms/drive/ui/picker/a/b;->a:I

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/ui/picker/view/i;Lcom/google/android/gms/drive/g/av;Lcom/google/android/gms/drive/ui/picker/a/t;Lcom/google/android/gms/drive/ui/picker/a/l;Landroid/view/View$OnClickListener;Lcom/google/android/gms/drive/ui/picker/a/s;)V
    .locals 4

    .prologue
    .line 86
    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 87
    iput-object p5, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->i:Lcom/google/android/gms/drive/ui/picker/a/l;

    .line 88
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->c:Landroid/view/LayoutInflater;

    .line 89
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/view/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->e:Lcom/google/android/gms/drive/ui/picker/view/i;

    .line 90
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/g/av;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->f:Lcom/google/android/gms/drive/g/av;

    .line 91
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/t;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->g:Lcom/google/android/gms/drive/ui/picker/a/t;

    .line 92
    invoke-static {p7}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/s;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->h:Lcom/google/android/gms/drive/ui/picker/a/s;

    .line 94
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/ui/picker/a/c;-><init>(Lcom/google/android/gms/drive/ui/picker/a/b;)V

    .line 101
    new-instance v1, Lcom/google/android/gms/drive/ui/picker/a/m;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->c:Landroid/view/LayoutInflater;

    sget v3, Lcom/google/android/gms/l;->aq:I

    invoke-direct {v1, v2, p6, v3, v0}, Lcom/google/android/gms/drive/ui/picker/a/m;-><init>(Landroid/view/LayoutInflater;Landroid/view/View$OnClickListener;ILandroid/database/DataSetObserver;)V

    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->b:Lcom/google/android/gms/drive/ui/picker/a/m;

    .line 104
    return-void
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/gms/drive/ui/picker/view/i;Lcom/google/android/gms/drive/g/av;Lcom/google/android/gms/drive/ui/picker/a/t;Lcom/google/android/gms/drive/ui/picker/a/l;Landroid/view/View$OnClickListener;Lcom/google/android/gms/drive/ui/picker/a/s;B)V
    .locals 0

    .prologue
    .line 31
    invoke-direct/range {p0 .. p7}, Lcom/google/android/gms/drive/ui/picker/a/b;-><init>(Landroid/content/Context;Lcom/google/android/gms/drive/ui/picker/view/i;Lcom/google/android/gms/drive/g/av;Lcom/google/android/gms/drive/ui/picker/a/t;Lcom/google/android/gms/drive/ui/picker/a/l;Landroid/view/View$OnClickListener;Lcom/google/android/gms/drive/ui/picker/a/s;)V

    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->i:Lcom/google/android/gms/drive/ui/picker/a/l;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->d:I

    .line 146
    return-void

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->i:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/l;->h()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->b:Lcom/google/android/gms/drive/ui/picker/a/m;

    iget-boolean v1, v0, Lcom/google/android/gms/drive/ui/picker/a/m;->e:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/drive/ui/picker/a/m;->e:Z

    iget-object v0, v0, Lcom/google/android/gms/drive/ui/picker/a/m;->c:Landroid/database/DataSetObserver;

    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 197
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/ui/picker/a/a/g;Lcom/google/android/gms/drive/ui/picker/a/q;)V
    .locals 8

    .prologue
    .line 111
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/a/e;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->i:Lcom/google/android/gms/drive/ui/picker/a/l;

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->e:Lcom/google/android/gms/drive/ui/picker/view/i;

    iget-object v5, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->f:Lcom/google/android/gms/drive/g/av;

    iget-object v6, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->g:Lcom/google/android/gms/drive/ui/picker/a/t;

    iget-object v7, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->h:Lcom/google/android/gms/drive/ui/picker/a/s;

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/drive/ui/picker/a/e;-><init>(Lcom/google/android/gms/drive/ui/picker/a/l;Lcom/google/android/gms/drive/ui/picker/a/a/g;Lcom/google/android/gms/drive/ui/picker/a/q;Lcom/google/android/gms/drive/ui/picker/view/i;Lcom/google/android/gms/drive/g/av;Lcom/google/android/gms/drive/ui/picker/a/t;Lcom/google/android/gms/drive/ui/picker/a/s;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->j:Lcom/google/android/gms/drive/ui/picker/a/e;

    .line 119
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/ui/picker/a/l;)V
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->i:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/picker/a/l;->close()V

    .line 124
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->i:Lcom/google/android/gms/drive/ui/picker/a/l;

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->j:Lcom/google/android/gms/drive/ui/picker/a/e;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->i:Lcom/google/android/gms/drive/ui/picker/a/l;

    iget-object v2, v0, Lcom/google/android/gms/drive/ui/picker/a/e;->e:Lcom/google/android/gms/drive/ui/picker/a/a/g;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/drive/ui/picker/a/a/g;->a(Lcom/google/android/gms/drive/ui/picker/a/l;)V

    iput-object v1, v0, Lcom/google/android/gms/drive/ui/picker/a/e;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    iget-object v1, v0, Lcom/google/android/gms/drive/ui/picker/a/e;->e:Lcom/google/android/gms/drive/ui/picker/a/a/g;

    invoke-virtual {v1}, Lcom/google/android/gms/drive/ui/picker/a/a/g;->c()Landroid/widget/SectionIndexer;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/drive/ui/picker/a/e;->h:Landroid/widget/SectionIndexer;

    .line 126
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/a/b;->notifyDataSetChanged()V

    .line 127
    return-void
.end method

.method public final a(ZLjava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 201
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->b:Lcom/google/android/gms/drive/ui/picker/a/m;

    iget-boolean v0, v2, Lcom/google/android/gms/drive/ui/picker/a/m;->e:Z

    if-eqz v0, :cond_0

    iget-boolean v0, v2, Lcom/google/android/gms/drive/ui/picker/a/m;->f:Z

    if-ne v0, p1, :cond_0

    iget-object v0, v2, Lcom/google/android/gms/drive/ui/picker/a/m;->g:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_0
    move v0, v1

    :goto_0
    iput-boolean v1, v2, Lcom/google/android/gms/drive/ui/picker/a/m;->e:Z

    iput-boolean p1, v2, Lcom/google/android/gms/drive/ui/picker/a/m;->f:Z

    iput-object p2, v2, Lcom/google/android/gms/drive/ui/picker/a/m;->g:Ljava/lang/String;

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Lcom/google/android/gms/drive/ui/picker/a/m;->a()V

    iget-object v0, v2, Lcom/google/android/gms/drive/ui/picker/a/m;->c:Landroid/database/DataSetObserver;

    invoke-virtual {v0}, Landroid/database/DataSetObserver;->onChanged()V

    .line 202
    :cond_1
    return-void

    .line 201
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getCount()I
    .locals 2

    .prologue
    .line 150
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/b;->b()V

    .line 153
    iget v1, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->d:I

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->b:Lcom/google/android/gms/drive/ui/picker/a/m;

    iget-boolean v0, v0, Lcom/google/android/gms/drive/ui/picker/a/m;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 158
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->i:Lcom/google/android/gms/drive/ui/picker/a/l;

    if-eqz v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->i:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/ui/picker/a/l;->a(I)Z

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->i:Lcom/google/android/gms/drive/ui/picker/a/l;

    .line 162
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 168
    int-to-long v0, p1

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 186
    iget v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->d:I

    if-ge p1, v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final getPositionForSection(I)I
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->j:Lcom/google/android/gms/drive/ui/picker/a/e;

    iget-object v0, v0, Lcom/google/android/gms/drive/ui/picker/a/e;->h:Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getPositionForSection(I)I

    move-result v0

    return v0
.end method

.method public final getSectionForPosition(I)I
    .locals 1

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->j:Lcom/google/android/gms/drive/ui/picker/a/e;

    iget-object v0, v0, Lcom/google/android/gms/drive/ui/picker/a/e;->h:Landroid/widget/SectionIndexer;

    invoke-interface {v0, p1}, Landroid/widget/SectionIndexer;->getSectionForPosition(I)I

    move-result v0

    return v0
.end method

.method public final getSections()[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/a/b;->j:Lcom/google/android/gms/drive/ui/picker/a/e;

    iget-object v0, v0, Lcom/google/android/gms/drive/ui/picker/a/e;->h:Landroid/widget/SectionIndexer;

    invoke-interface {v0}, Landroid/widget/SectionIndexer;->getSections()[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 16

    .prologue
    .line 206
    invoke-virtual/range {p0 .. p1}, Lcom/google/android/gms/drive/ui/picker/a/b;->getItemViewType(I)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    .line 207
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/drive/ui/picker/a/b;->b:Lcom/google/android/gms/drive/ui/picker/a/m;

    if-eqz p2, :cond_0

    move-object/from16 v0, p2

    instance-of v2, v0, Landroid/widget/Button;

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, v3, Lcom/google/android/gms/drive/ui/picker/a/m;->a:Landroid/view/LayoutInflater;

    iget v4, v3, Lcom/google/android/gms/drive/ui/picker/a/m;->d:I

    const/4 v5, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v4, v0, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    sget v2, Lcom/google/android/gms/j;->ss:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, v3, Lcom/google/android/gms/drive/ui/picker/a/m;->h:Landroid/widget/Button;

    iget-object v2, v3, Lcom/google/android/gms/drive/ui/picker/a/m;->h:Landroid/widget/Button;

    iget-object v4, v3, Lcom/google/android/gms/drive/ui/picker/a/m;->b:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v4}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/gms/drive/ui/picker/a/m;->a()V

    .line 223
    :goto_0
    return-object p2

    .line 209
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/ui/picker/a/b;->i:Lcom/google/android/gms/drive/ui/picker/a/l;

    if-nez v2, :cond_3

    .line 210
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "this should only be called when cursor is valid"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 212
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/ui/picker/a/b;->i:Lcom/google/android/gms/drive/ui/picker/a/l;

    move/from16 v0, p1

    invoke-interface {v2, v0}, Lcom/google/android/gms/drive/ui/picker/a/l;->a(I)Z

    move-result v2

    if-nez v2, :cond_4

    .line 213
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "couldn\'t move cursor to position "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 216
    :cond_4
    if-nez p2, :cond_5

    .line 217
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/ui/picker/a/b;->c:Landroid/view/LayoutInflater;

    sget v3, Lcom/google/android/gms/drive/ui/picker/a/b;->a:I

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v2, v3, v0, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 218
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/drive/ui/picker/a/b;->j:Lcom/google/android/gms/drive/ui/picker/a/e;

    new-instance v2, Lcom/google/android/gms/drive/ui/picker/a/h;

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Lcom/google/android/gms/drive/ui/picker/a/h;-><init>(Landroid/view/View;)V

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 222
    :cond_5
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/drive/ui/picker/a/b;->j:Lcom/google/android/gms/drive/ui/picker/a/e;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/drive/ui/picker/a/b;->i:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/drive/ui/picker/a/h;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->n:Z

    invoke-interface {v9}, Lcom/google/android/gms/drive/ui/picker/a/l;->n()Lcom/google/android/gms/drive/DriveId;

    move-result-object v10

    iget-object v3, v8, Lcom/google/android/gms/drive/ui/picker/a/e;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-virtual {v3, v9}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    new-instance v2, Ljava/lang/AssertionError;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Trying to bind view with a wrong row accessor: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v2

    :cond_6
    iget-object v3, v8, Lcom/google/android/gms/drive/ui/picker/a/e;->e:Lcom/google/android/gms/drive/ui/picker/a/a/g;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/ui/picker/a/a/g;->b()Lcom/google/android/gms/drive/ui/picker/a/a/r;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->c:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, v8, Lcom/google/android/gms/drive/ui/picker/a/e;->d:Lcom/google/android/gms/drive/ui/picker/view/i;

    invoke-interface {v3}, Lcom/google/android/gms/drive/ui/picker/view/i;->g()Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-virtual {v10, v3}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v11

    iget-object v3, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->b:Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;

    iget-object v4, v8, Lcom/google/android/gms/drive/ui/picker/a/e;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v4}, Lcom/google/android/gms/drive/ui/picker/a/l;->a()Ljava/lang/String;

    move-result-object v4

    sget-object v5, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->a(Ljava/lang/String;Landroid/graphics/Typeface;)V

    iget-object v5, v8, Lcom/google/android/gms/drive/ui/picker/a/e;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v5}, Lcom/google/android/gms/drive/ui/picker/a/l;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/gms/drive/database/model/ah;->b(Ljava/lang/String;)I

    move-result v5

    iget-object v7, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->a:Landroid/content/Context;

    invoke-virtual {v7, v5}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v7, " "

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v8}, Lcom/google/android/gms/drive/ui/picker/a/e;->a()Z

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->setEnabled(Z)V

    invoke-interface {v9}, Lcom/google/android/gms/drive/ui/picker/a/l;->i()Z

    iget-object v3, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->c:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    invoke-interface {v3}, Lcom/google/android/gms/drive/ui/picker/a/a/r;->a()Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->c:Lcom/google/android/gms/drive/ui/picker/a/a/r;

    iget-object v4, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->a:Landroid/content/Context;

    invoke-interface {v3, v4}, Lcom/google/android/gms/drive/ui/picker/a/a/r;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->d:Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;

    iget-object v5, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->e:Landroid/view/View;

    const/4 v7, 0x0

    invoke-virtual {v5, v7}, Landroid/view/View;->setVisibility(I)V

    const/4 v5, 0x0

    invoke-virtual {v4, v3, v5}, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->a(Ljava/lang/String;Landroid/graphics/Typeface;)V

    :goto_1
    iget-object v3, v8, Lcom/google/android/gms/drive/ui/picker/a/e;->e:Lcom/google/android/gms/drive/ui/picker/a/a/g;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/ui/picker/a/a/g;->d()Lcom/google/android/gms/drive/ui/picker/a/a/a;

    move-result-object v3

    iget-object v4, v8, Lcom/google/android/gms/drive/ui/picker/a/e;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/ui/picker/a/a/a;->a(Lcom/google/android/gms/drive/ui/picker/a/l;)Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_7

    iget-boolean v4, v8, Lcom/google/android/gms/drive/ui/picker/a/e;->b:Z

    if-nez v4, :cond_7

    iget-object v4, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->a:Landroid/content/Context;

    iget v5, v8, Lcom/google/android/gms/drive/ui/picker/a/e;->g:I

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, v8, Lcom/google/android/gms/drive/ui/picker/a/e;->f:Lcom/google/android/gms/drive/g/av;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    invoke-virtual {v13, v14, v15}, Lcom/google/android/gms/drive/g/av;->a(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v12

    invoke-virtual {v4, v5, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v6, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_7
    invoke-interface {v9}, Lcom/google/android/gms/drive/ui/picker/a/l;->e()Z

    move-result v3

    iget-object v4, v8, Lcom/google/android/gms/drive/ui/picker/a/e;->j:Lcom/google/android/gms/drive/ui/picker/a/l;

    invoke-interface {v4}, Lcom/google/android/gms/drive/ui/picker/a/l;->b()Ljava/lang/String;

    move-result-object v4

    iget-object v5, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->h:Landroid/widget/ImageView;

    invoke-static {v4, v3}, Lcom/google/android/gms/drive/database/model/ah;->a(Ljava/lang/String;Z)I

    move-result v3

    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    const/16 v3, 0xb

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-virtual {v8}, Lcom/google/android/gms/drive/ui/picker/a/e;->a()Z

    move-result v3

    if-eqz v3, :cond_b

    const/high16 v3, 0x3f800000    # 1.0f

    :goto_2
    invoke-virtual {v5, v3}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_8
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v3, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->i:Landroid/widget/ImageView;

    sget v4, Lcom/google/android/gms/p;->gz:I

    iget-boolean v5, v8, Lcom/google/android/gms/drive/ui/picker/a/e;->a:Z

    if-nez v5, :cond_c

    invoke-interface {v9}, Lcom/google/android/gms/drive/ui/picker/a/l;->e()Z

    move-result v5

    if-eqz v5, :cond_c

    const/4 v5, 0x1

    :goto_3
    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/drive/ui/picker/a/e;->a(Lcom/google/android/gms/drive/ui/picker/a/h;Landroid/widget/ImageView;IZLjava/util/List;Ljava/util/List;)V

    iget-object v3, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->j:Landroid/widget/ImageView;

    sget v4, Lcom/google/android/gms/p;->gy:I

    iget-boolean v5, v8, Lcom/google/android/gms/drive/ui/picker/a/e;->b:Z

    if-nez v5, :cond_d

    invoke-interface {v9}, Lcom/google/android/gms/drive/ui/picker/a/l;->d()Z

    move-result v5

    if-eqz v5, :cond_d

    const/4 v5, 0x1

    :goto_4
    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/drive/ui/picker/a/e;->a(Lcom/google/android/gms/drive/ui/picker/a/h;Landroid/widget/ImageView;IZLjava/util/List;Ljava/util/List;)V

    iget-object v3, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->k:Landroid/widget/ImageView;

    sget v4, Lcom/google/android/gms/p;->gA:I

    iget-boolean v5, v8, Lcom/google/android/gms/drive/ui/picker/a/e;->c:Z

    if-nez v5, :cond_e

    invoke-interface {v9}, Lcom/google/android/gms/drive/ui/picker/a/l;->c()Z

    move-result v5

    if-eqz v5, :cond_e

    const/4 v5, 0x1

    :goto_5
    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/drive/ui/picker/a/e;->a(Lcom/google/android/gms/drive/ui/picker/a/h;Landroid/widget/ImageView;IZLjava/util/List;Ljava/util/List;)V

    const-string v3, "  "

    invoke-static {v3, v6}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "  "

    invoke-static {v4, v7}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->f:Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v3, v6}, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->a(Ljava/lang/String;Landroid/graphics/Typeface;)V

    iget-object v3, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->f:Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v8, v9, v2, v10}, Lcom/google/android/gms/drive/ui/picker/a/e;->a(Lcom/google/android/gms/drive/ui/picker/a/l;Lcom/google/android/gms/drive/ui/picker/a/h;Lcom/google/android/gms/drive/DriveId;)V

    iget-object v3, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->g:Landroid/view/View;

    if-eqz v3, :cond_9

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    if-eqz v11, :cond_f

    sget v5, Lcom/google/android/gms/f;->h:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_9
    :goto_6
    invoke-virtual {v8}, Lcom/google/android/gms/drive/ui/picker/a/e;->a()Z

    move-result v3

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/view/View;->setEnabled(Z)V

    iget-object v3, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->f:Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;

    iget-boolean v2, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->n:Z

    if-eqz v2, :cond_11

    const/4 v2, 0x0

    :goto_7
    invoke-virtual {v3, v2}, Lcom/google/android/gms/drive/ui/picker/view/FixedSizeTextView;->setVisibility(I)V

    goto/16 :goto_0

    :cond_a
    iget-object v3, v2, Lcom/google/android/gms/drive/ui/picker/a/h;->e:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_1

    :cond_b
    const v3, 0x3f19999a    # 0.6f

    goto/16 :goto_2

    :cond_c
    const/4 v5, 0x0

    goto :goto_3

    :cond_d
    const/4 v5, 0x0

    goto :goto_4

    :cond_e
    const/4 v5, 0x0

    goto :goto_5

    :cond_f
    sget v4, Lcom/google/android/gms/h;->S:I

    invoke-virtual {v3}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    const/16 v5, 0x10

    invoke-static {v5}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v5

    if-eqz v5, :cond_10

    invoke-virtual {v3, v4}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_6

    :cond_10
    invoke-virtual {v3, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_6

    :cond_11
    const/16 v2, 0x8

    goto :goto_7
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 191
    const/4 v0, 0x2

    return v0
.end method

.method public final hasStableIds()Z
    .locals 1

    .prologue
    .line 248
    const/4 v0, 0x0

    return v0
.end method

.method public final notifyDataSetChanged()V
    .locals 0

    .prologue
    .line 179
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/a/b;->b()V

    .line 180
    invoke-super {p0}, Landroid/widget/BaseAdapter;->notifyDataSetChanged()V

    .line 181
    return-void
.end method
