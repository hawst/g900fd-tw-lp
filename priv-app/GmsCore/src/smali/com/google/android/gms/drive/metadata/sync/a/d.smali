.class public final Lcom/google/android/gms/drive/metadata/sync/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/drive/metadata/sync/a/c;

.field public final b:J

.field private final c:Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/metadata/sync/a/c;Ljava/lang/String;J)V
    .locals 3

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const-wide/16 v0, 0x0

    cmp-long v0, p3, v0

    if-gtz v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 23
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/sync/a/c;

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    .line 24
    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->c:Ljava/lang/String;

    .line 25
    iput-wide p3, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->b:J

    .line 26
    return-void

    .line 22
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/drive/metadata/sync/a/c;Ljava/lang/String;J)Lcom/google/android/gms/drive/metadata/sync/a/d;
    .locals 2

    .prologue
    .line 108
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/a/d;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/drive/metadata/sync/a/d;-><init>(Lcom/google/android/gms/drive/metadata/sync/a/c;Ljava/lang/String;J)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/drive/metadata/sync/a/d;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/sync/a/d;
    .locals 6

    .prologue
    .line 120
    new-instance v0, Lcom/google/android/gms/drive/metadata/sync/a/d;

    iget-object v1, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    iget-wide v2, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->b:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/google/android/gms/drive/metadata/sync/a/d;-><init>(Lcom/google/android/gms/drive/metadata/sync/a/c;Ljava/lang/String;J)V

    return-object v0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 40
    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/sync/a/d;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/sync/a/d;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->c:Ljava/lang/String;

    if-nez v3, :cond_2

    move v3, v2

    :goto_1
    if-ne v0, v3, :cond_3

    :goto_2
    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->c:Ljava/lang/String;

    return-object v0

    :cond_1
    move v0, v1

    .line 40
    goto :goto_0

    :cond_2
    move v3, v1

    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_2
.end method

.method public final b()Z
    .locals 4

    .prologue
    .line 51
    iget-wide v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 59
    invoke-virtual {p0}, Lcom/google/android/gms/drive/metadata/sync/a/d;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64
    if-ne p0, p1, :cond_1

    .line 71
    :cond_0
    :goto_0
    return v0

    .line 67
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/drive/metadata/sync/a/d;

    if-nez v2, :cond_2

    move v0, v1

    .line 68
    goto :goto_0

    .line 70
    :cond_2
    check-cast p1, Lcom/google/android/gms/drive/metadata/sync/a/d;

    .line 71
    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    iget-object v3, p1, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/drive/metadata/sync/a/d;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-wide v2, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/drive/metadata/sync/a/d;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    .line 78
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 83
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "FeedState[feed=%s, nextPageToken=%s, numPagesRetrieved=%d]"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->a:Lcom/google/android/gms/drive/metadata/sync/a/c;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->c:Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-wide v4, p0, Lcom/google/android/gms/drive/metadata/sync/a/d;->b:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
