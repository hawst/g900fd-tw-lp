.class public final Lcom/google/android/gms/people/service/a/al;
.super Lcom/google/android/gms/people/service/a/a;
.source "SourceFile"


# instance fields
.field private final g:Ljava/lang/String;

.field private final h:Z

.field private final i:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;Z[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/people/service/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;ILcom/google/android/gms/people/internal/f;Ljava/lang/String;)V

    .line 27
    iput-object p5, p0, Lcom/google/android/gms/people/service/a/al;->g:Ljava/lang/String;

    .line 28
    iput-boolean p6, p0, Lcom/google/android/gms/people/service/a/al;->h:Z

    .line 29
    iput-object p7, p0, Lcom/google/android/gms/people/service/a/al;->i:[Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public final f()Landroid/util/Pair;
    .locals 3

    .prologue
    .line 34
    new-instance v0, Lcom/google/android/gms/people/sync/a/a;

    iget-object v1, p0, Lcom/google/android/gms/people/service/a/al;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/al;->g:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/people/sync/a/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/gms/people/service/a/al;->h:Z

    iget-object v2, p0, Lcom/google/android/gms/people/service/a/al;->i:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/people/sync/a/a;->a(Z[Ljava/lang/String;)V

    .line 36
    sget-object v0, Lcom/google/android/gms/people/service/b;->c:Lcom/google/android/gms/people/service/b;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method
