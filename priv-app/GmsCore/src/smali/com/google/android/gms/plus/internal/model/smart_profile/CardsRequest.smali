.class public Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/plus/internal/model/smart_profile/a;


# instance fields
.field private final a:I

.field private final b:[B

.field private final c:Ljava/lang/String;

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/plus/internal/model/smart_profile/a;

    invoke-direct {v0}, Lcom/google/android/gms/plus/internal/model/smart_profile/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->CREATOR:Lcom/google/android/gms/plus/internal/model/smart_profile/a;

    return-void
.end method

.method public constructor <init>(I[BLjava/lang/String;I)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput p1, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->a:I

    .line 43
    iput-object p2, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->b:[B

    .line 44
    iput-object p3, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->c:Ljava/lang/String;

    .line 45
    iput p4, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->d:I

    .line 46
    return-void
.end method

.method public constructor <init>([BLjava/lang/String;I)V
    .locals 1

    .prologue
    .line 49
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;-><init>(I[BLjava/lang/String;I)V

    .line 50
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 53
    iget v0, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->a:I

    return v0
.end method

.method public final b()[B
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->b:[B

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 71
    iget v0, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->d:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 98
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 86
    instance-of v1, p1, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;

    if-nez v1, :cond_1

    .line 90
    :cond_0
    :goto_0
    return v0

    .line 89
    :cond_1
    check-cast p1, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;

    .line 90
    iget v1, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->a:I

    iget v2, p1, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->a:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->b:[B

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->b:[B

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->d:I

    iget v2, p1, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->d:I

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 81
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->b:[B

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 76
    invoke-static {p0, p1}, Lcom/google/android/gms/plus/internal/model/smart_profile/a;->a(Lcom/google/android/gms/plus/internal/model/smart_profile/CardsRequest;Landroid/os/Parcel;)V

    .line 77
    return-void
.end method
