.class public final Lcom/google/android/gms/wearable/node/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:J

.field private final d:J

.field private e:J

.field private f:J


# direct methods
.method public constructor <init>(IIJJ)V
    .locals 3

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput p1, p0, Lcom/google/android/gms/wearable/node/a/c;->a:I

    .line 30
    iput p2, p0, Lcom/google/android/gms/wearable/node/a/c;->b:I

    .line 31
    iput-wide p3, p0, Lcom/google/android/gms/wearable/node/a/c;->c:J

    .line 32
    iput-wide p5, p0, Lcom/google/android/gms/wearable/node/a/c;->d:J

    .line 33
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/wearable/node/a/c;->f:J

    .line 34
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 37
    iput-wide v0, p0, Lcom/google/android/gms/wearable/node/a/c;->e:J

    .line 38
    iput-wide v0, p0, Lcom/google/android/gms/wearable/node/a/c;->f:J

    .line 39
    return-void
.end method

.method public final b()J
    .locals 6

    .prologue
    const-wide/16 v4, 0x1

    .line 42
    iget v0, p0, Lcom/google/android/gms/wearable/node/a/c;->b:I

    int-to-long v0, v0

    iget-wide v2, p0, Lcom/google/android/gms/wearable/node/a/c;->e:J

    add-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/wearable/node/a/c;->e:J

    .line 43
    iget v0, p0, Lcom/google/android/gms/wearable/node/a/c;->a:I

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/wearable/node/a/c;->e:J

    sub-long/2addr v2, v4

    long-to-int v2, v2

    shl-int/2addr v1, v2

    mul-int/2addr v0, v1

    .line 44
    iget-wide v2, p0, Lcom/google/android/gms/wearable/node/a/c;->f:J

    int-to-long v4, v0

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/gms/wearable/node/a/c;->f:J

    .line 45
    invoke-virtual {p0}, Lcom/google/android/gms/wearable/node/a/c;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 46
    iget-wide v0, p0, Lcom/google/android/gms/wearable/node/a/c;->d:J

    .line 48
    :goto_0
    return-wide v0

    :cond_0
    int-to-long v0, v0

    goto :goto_0
.end method

.method public final c()Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 53
    iget-wide v2, p0, Lcom/google/android/gms/wearable/node/a/c;->c:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/wearable/node/a/c;->f:J

    iget-wide v4, p0, Lcom/google/android/gms/wearable/node/a/c;->c:J

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 60
    iget v0, p0, Lcom/google/android/gms/wearable/node/a/c;->b:I

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/gms/wearable/node/a/c;->e:J

    .line 61
    iget-wide v0, p0, Lcom/google/android/gms/wearable/node/a/c;->f:J

    iget-wide v2, p0, Lcom/google/android/gms/wearable/node/a/c;->c:J

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/wearable/node/a/c;->f:J

    .line 62
    return-void
.end method
