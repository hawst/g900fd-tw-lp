.class final Lcom/google/android/gms/update/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/update/UpdateFromSdCardService;

.field private final b:Landroid/content/Context;


# direct methods
.method constructor <init>(Lcom/google/android/gms/update/UpdateFromSdCardService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 98
    iput-object p1, p0, Lcom/google/android/gms/update/ah;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 99
    iput-object p2, p0, Lcom/google/android/gms/update/ah;->b:Landroid/content/Context;

    .line 100
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x6

    const/4 v3, 0x4

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/update/ah;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-static {v0}, Lcom/google/android/gms/update/UpdateFromSdCardService;->a(Lcom/google/android/gms/update/UpdateFromSdCardService;)Lcom/google/android/gms/update/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/update/h;->c()I

    move-result v0

    .line 105
    iget-object v1, p0, Lcom/google/android/gms/update/ah;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-static {v1}, Lcom/google/android/gms/update/UpdateFromSdCardService;->b(Lcom/google/android/gms/update/UpdateFromSdCardService;)I

    move-result v1

    .line 106
    iget-object v2, p0, Lcom/google/android/gms/update/ah;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-static {v2}, Lcom/google/android/gms/update/UpdateFromSdCardService;->b(Lcom/google/android/gms/update/UpdateFromSdCardService;)I

    move-result v2

    if-eq v2, v4, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/update/ah;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-static {v2}, Lcom/google/android/gms/update/UpdateFromSdCardService;->b(Lcom/google/android/gms/update/UpdateFromSdCardService;)I

    move-result v2

    if-eq v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/update/ah;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-static {v2}, Lcom/google/android/gms/update/UpdateFromSdCardService;->b(Lcom/google/android/gms/update/UpdateFromSdCardService;)I

    move-result v2

    if-ne v2, v3, :cond_1

    .line 108
    :cond_0
    if-nez v0, :cond_3

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/update/ah;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-static {v0, v4}, Lcom/google/android/gms/update/UpdateFromSdCardService;->a(Lcom/google/android/gms/update/UpdateFromSdCardService;I)I

    .line 116
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/update/ah;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-static {v0}, Lcom/google/android/gms/update/UpdateFromSdCardService;->b(Lcom/google/android/gms/update/UpdateFromSdCardService;)I

    move-result v0

    if-eq v1, v0, :cond_2

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/update/ah;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-static {v0, v1}, Lcom/google/android/gms/update/UpdateFromSdCardService;->a(Lcom/google/android/gms/update/UpdateFromSdCardService;I)I

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/update/ah;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-static {v0}, Lcom/google/android/gms/update/UpdateFromSdCardService;->c(Lcom/google/android/gms/update/UpdateFromSdCardService;)V

    .line 120
    :cond_2
    return-void

    .line 110
    :cond_3
    const/4 v2, 0x2

    if-ne v0, v2, :cond_4

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/update/ah;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    invoke-static {v0, v3}, Lcom/google/android/gms/update/UpdateFromSdCardService;->a(Lcom/google/android/gms/update/UpdateFromSdCardService;I)I

    goto :goto_0

    .line 112
    :cond_4
    const/4 v2, 0x1

    if-ne v0, v2, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/android/gms/update/ah;->a:Lcom/google/android/gms/update/UpdateFromSdCardService;

    const/4 v2, 0x5

    invoke-static {v0, v2}, Lcom/google/android/gms/update/UpdateFromSdCardService;->a(Lcom/google/android/gms/update/UpdateFromSdCardService;I)I

    goto :goto_0
.end method
