.class final Lcom/google/android/gms/car/support/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;
.implements Ljava/util/Map$Entry;


# instance fields
.field a:I

.field b:I

.field c:Z

.field final synthetic d:Lcom/google/android/gms/car/support/u;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/support/u;)V
    .locals 1

    .prologue
    .line 77
    iput-object p1, p0, Lcom/google/android/gms/car/support/y;->d:Lcom/google/android/gms/car/support/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/y;->c:Z

    .line 78
    invoke-virtual {p1}, Lcom/google/android/gms/car/support/u;->a()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/support/y;->a:I

    .line 79
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/support/y;->b:I

    .line 80
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 136
    iget-boolean v2, p0, Lcom/google/android/gms/car/support/y;->c:Z

    if-nez v2, :cond_0

    .line 137
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This container does not support retaining Map.Entry objects"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_0
    instance-of v2, p1, Ljava/util/Map$Entry;

    if-nez v2, :cond_2

    .line 144
    :cond_1
    :goto_0
    return v0

    .line 143
    :cond_2
    check-cast p1, Ljava/util/Map$Entry;

    .line 144
    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/support/y;->d:Lcom/google/android/gms/car/support/u;

    iget v4, p0, Lcom/google/android/gms/car/support/y;->b:I

    invoke-virtual {v3, v4, v0}, Lcom/google/android/gms/car/support/u;->a(II)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/support/f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {p1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/support/y;->d:Lcom/google/android/gms/car/support/u;

    iget v4, p0, Lcom/google/android/gms/car/support/y;->b:I

    invoke-virtual {v3, v4, v1}, Lcom/google/android/gms/car/support/u;->a(II)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/support/f;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public final getKey()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 108
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/y;->c:Z

    if-nez v0, :cond_0

    .line 109
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This container does not support retaining Map.Entry objects"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/y;->d:Lcom/google/android/gms/car/support/u;

    iget v1, p0, Lcom/google/android/gms/car/support/y;->b:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/support/u;->a(II)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getValue()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 118
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/y;->c:Z

    if-nez v0, :cond_0

    .line 119
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This container does not support retaining Map.Entry objects"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/y;->d:Lcom/google/android/gms/car/support/u;

    iget v1, p0, Lcom/google/android/gms/car/support/y;->b:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/support/u;->a(II)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final hasNext()Z
    .locals 2

    .prologue
    .line 84
    iget v0, p0, Lcom/google/android/gms/car/support/y;->b:I

    iget v1, p0, Lcom/google/android/gms/car/support/y;->a:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 150
    iget-boolean v1, p0, Lcom/google/android/gms/car/support/y;->c:Z

    if-nez v1, :cond_0

    .line 151
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This container does not support retaining Map.Entry objects"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 154
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/support/y;->d:Lcom/google/android/gms/car/support/u;

    iget v2, p0, Lcom/google/android/gms/car/support/y;->b:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/car/support/u;->a(II)Ljava/lang/Object;

    move-result-object v1

    .line 155
    iget-object v2, p0, Lcom/google/android/gms/car/support/y;->d:Lcom/google/android/gms/car/support/u;

    iget v3, p0, Lcom/google/android/gms/car/support/y;->b:I

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/car/support/u;->a(II)Ljava/lang/Object;

    move-result-object v2

    .line 156
    if-nez v1, :cond_1

    move v1, v0

    :goto_0
    if-nez v2, :cond_2

    :goto_1
    xor-int/2addr v0, v1

    return v0

    :cond_1
    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1
.end method

.method public final bridge synthetic next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/gms/car/support/y;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/support/y;->b:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/y;->c:Z

    return-object p0
.end method

.method public final remove()V
    .locals 2

    .prologue
    .line 96
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/y;->c:Z

    if-nez v0, :cond_0

    .line 97
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/y;->d:Lcom/google/android/gms/car/support/u;

    iget v1, p0, Lcom/google/android/gms/car/support/y;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/support/u;->a(I)V

    .line 100
    iget v0, p0, Lcom/google/android/gms/car/support/y;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/support/y;->b:I

    .line 101
    iget v0, p0, Lcom/google/android/gms/car/support/y;->a:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/support/y;->a:I

    .line 102
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/support/y;->c:Z

    .line 103
    return-void
.end method

.method public final setValue(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 127
    iget-boolean v0, p0, Lcom/google/android/gms/car/support/y;->c:Z

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This container does not support retaining Map.Entry objects"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 131
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/support/y;->d:Lcom/google/android/gms/car/support/u;

    iget v1, p0, Lcom/google/android/gms/car/support/y;->b:I

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/car/support/u;->a(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 162
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/y;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/car/support/y;->getValue()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
