.class public final Lcom/google/android/gms/drive/ui/picker/m;
.super Lcom/google/android/gms/drive/ui/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/ui/picker/a/s;
.implements Lcom/google/android/gms/drive/ui/picker/a/t;


# instance fields
.field private A:Lcom/google/android/gms/common/api/v;

.field private B:Lcom/google/android/gms/drive/c/d;

.field private C:Lcom/google/android/gms/drive/c/a;

.field private D:Landroid/os/Bundle;

.field private a:Lcom/google/android/gms/drive/ui/legacy/navigation/f;

.field private b:Lcom/google/android/gms/drive/DriveId;

.field private c:Lcom/google/android/gms/drive/aj;

.field private d:Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

.field private e:Lcom/google/android/gms/drive/DriveId;

.field private f:Lcom/google/android/gms/drive/DriveId;

.field private g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

.field private h:Landroid/widget/ProgressBar;

.field private i:Landroid/widget/ImageView;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/TextView;

.field private l:Lcom/google/android/gms/drive/DriveId;

.field private m:J

.field private n:Ljava/lang/String;

.field private o:Lcom/google/android/gms/drive/ui/picker/a/a/t;

.field private p:Ljava/lang/Runnable;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:Z

.field private t:Ljava/util/Set;

.field private u:Landroid/view/MenuItem;

.field private v:Landroid/view/View;

.field private w:Landroid/widget/Button;

.field private x:Landroid/widget/Button;

.field private y:Lcom/google/android/gms/drive/ui/picker/ab;

.field private final z:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/j;-><init>()V

    .line 194
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;->a:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->o:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    .line 218
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/ab;->a:Lcom/google/android/gms/drive/ui/picker/ab;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    .line 223
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->z:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/m;Lcom/google/android/gms/drive/ak;)Lcom/google/android/gms/drive/aj;
    .locals 4

    .prologue
    .line 92
    invoke-virtual {p1}, Lcom/google/android/gms/drive/ak;->c()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/drive/ak;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/aj;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/drive/ak;->b(I)Lcom/google/android/gms/drive/aj;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/m;Lcom/google/android/gms/drive/ui/picker/ab;)Lcom/google/android/gms/drive/ui/picker/ab;
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    return-object p1
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 430
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, p1, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 431
    return-void
.end method

.method private a(Lcom/google/android/gms/drive/DriveId;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 744
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 745
    if-nez v0, :cond_0

    .line 746
    const-string v0, "PickEntryDialogFragment"

    const-string v1, "Activity is null in onAttemptEntrySelection()"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/g/ab;->c(Ljava/lang/String;Ljava/lang/String;)I

    .line 789
    :goto_0
    return-void

    .line 750
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    if-nez v0, :cond_1

    .line 752
    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    .line 753
    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    .line 754
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/m;->g()V

    .line 755
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/m;->h()V

    goto :goto_0

    .line 759
    :cond_1
    if-nez p1, :cond_2

    .line 760
    invoke-direct {p0, v1, v1}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/aj;Lcom/google/android/gms/drive/aj;)V

    goto :goto_0

    .line 763
    :cond_2
    sget-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/n;

    move-result-object v0

    .line 764
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/y;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/ui/picker/aa;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/ui/picker/aa;-><init>(Lcom/google/android/gms/drive/ui/picker/m;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/drive/aj;Lcom/google/android/gms/drive/aj;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 826
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 827
    const-string v2, "disablePreselectedEntry"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    .line 828
    const-string v3, "driveId"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    .line 831
    if-eqz p1, :cond_5

    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->g()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gms/drive/ui/picker/m;->b(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 835
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    .line 836
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/drive/DriveId;)V

    .line 837
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/m;->c:Lcom/google/android/gms/drive/aj;

    .line 844
    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 851
    :cond_1
    if-eqz p1, :cond_6

    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->o()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->e:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_6

    .line 854
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->e:Lcom/google/android/gms/drive/DriveId;

    .line 858
    :goto_1
    if-nez p2, :cond_8

    move-object v0, v1

    :goto_2
    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    .line 863
    :cond_2
    new-instance v0, Lcom/google/android/gms/drive/ui/legacy/navigation/b;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/b;-><init>()V

    const-string v1, "notInTrash"

    invoke-static {v1}, Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/legacy/navigation/SimpleCriterion;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/legacy/navigation/b;->a(Lcom/google/android/gms/drive/ui/legacy/navigation/Criterion;)Lcom/google/android/gms/drive/ui/legacy/navigation/b;

    if-eqz p2, :cond_9

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->l:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    new-instance v1, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->q:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/ui/legacy/navigation/ChildrenOfCollectionCriterion;-><init>(Ljava/lang/String;Lcom/google/android/gms/drive/DriveId;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/legacy/navigation/b;->a(Lcom/google/android/gms/drive/ui/legacy/navigation/Criterion;)Lcom/google/android/gms/drive/ui/legacy/navigation/b;

    :goto_3
    new-instance v1, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;

    iget-object v0, v0, Lcom/google/android/gms/drive/ui/legacy/navigation/b;->a:Ljava/util/List;

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSetImpl;-><init>(Ljava/util/Collection;)V

    .line 864
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->d:Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 865
    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->d:Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    .line 866
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/m;->b(Lcom/google/android/gms/drive/DriveId;)Z

    move-result v0

    if-eqz v0, :cond_a

    sget-object v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;->a:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    :goto_4
    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->o:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    .line 868
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->a:Lcom/google/android/gms/drive/ui/legacy/navigation/f;

    new-instance v1, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->d:Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    invoke-direct {v1, v2}, Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;-><init>(Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/ui/legacy/navigation/f;->a(Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a()Z

    move-result v0

    if-nez v0, :cond_b

    :cond_3
    :goto_5
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/m;->e()V

    .line 871
    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/m;->g()V

    .line 872
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/m;->h()V

    .line 873
    return-void

    .line 839
    :cond_5
    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    .line 840
    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->c:Lcom/google/android/gms/drive/aj;

    .line 841
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/drive/DriveId;)V

    goto/16 :goto_0

    .line 856
    :cond_6
    if-eqz p2, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->l:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p2}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-virtual {p2}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    invoke-interface {v2, v3, v0}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/n;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v2}, Lcom/google/android/gms/drive/y;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/drive/ui/picker/p;

    invoke-direct {v2, p0}, Lcom/google/android/gms/drive/ui/picker/p;-><init>(Lcom/google/android/gms/drive/ui/picker/m;)V

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto/16 :goto_1

    :cond_7
    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->e:Lcom/google/android/gms/drive/DriveId;

    goto/16 :goto_1

    .line 858
    :cond_8
    invoke-virtual {p2}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    goto/16 :goto_2

    .line 863
    :cond_9
    new-instance v1, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    iget-object v2, v2, Lcom/google/android/gms/drive/ui/picker/ab;->f:Lcom/google/android/gms/drive/ui/picker/a/q;

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/m;->q:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/ui/legacy/navigation/EntriesFilterCriterion;-><init>(Lcom/google/android/gms/drive/ui/picker/a/q;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/legacy/navigation/b;->a(Lcom/google/android/gms/drive/ui/legacy/navigation/Criterion;)Lcom/google/android/gms/drive/ui/legacy/navigation/b;

    goto/16 :goto_3

    .line 866
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    iget-object v0, v0, Lcom/google/android/gms/drive/ui/picker/ab;->f:Lcom/google/android/gms/drive/ui/picker/a/q;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/a/q;->a()Lcom/google/android/gms/drive/ui/picker/a/a/t;

    move-result-object v0

    goto :goto_4

    .line 868
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->o:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->o:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/drive/ui/picker/a/a/t;)V

    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->a:Lcom/google/android/gms/drive/ui/legacy/navigation/f;

    invoke-interface {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/f;->a()Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->a:Lcom/google/android/gms/drive/ui/legacy/navigation/f;

    invoke-interface {v1}, Lcom/google/android/gms/drive/ui/legacy/navigation/f;->b()Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/drive/ui/legacy/navigation/NavigationPathElement;)V

    goto :goto_5
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/m;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->e:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->e:Lcom/google/android/gms/drive/DriveId;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/DriveId;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    if-eqz v0, :cond_0

    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    iput-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->d:Lcom/google/android/gms/drive/ui/legacy/navigation/CriterionSet;

    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/m;->e()V

    invoke-direct {p0, v1}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/DriveId;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/m;I)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/ui/picker/m;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/m;Lcom/google/android/gms/drive/DriveId;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/DriveId;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/m;Lcom/google/android/gms/drive/aj;)V
    .locals 3

    .prologue
    .line 92
    sget-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/n;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/y;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/ui/picker/o;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/drive/ui/picker/o;-><init>(Lcom/google/android/gms/drive/ui/picker/m;Lcom/google/android/gms/drive/aj;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/drive/ui/picker/m;Lcom/google/android/gms/drive/aj;Lcom/google/android/gms/drive/aj;)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/aj;Lcom/google/android/gms/drive/aj;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/drive/ui/picker/m;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/DriveId;
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/m;->e:Lcom/google/android/gms/drive/DriveId;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/gms/drive/ui/picker/m;)Lcom/google/android/gms/drive/ui/picker/ab;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    return-object v0
.end method

.method private b(I)V
    .locals 3

    .prologue
    .line 688
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->B:Lcom/google/android/gms/drive/c/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/d;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    const/4 v1, 0x3

    const/16 v2, 0x1d

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/c/a;->a(II)Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/c/a;->c(I)Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->b()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    .line 693
    if-nez p1, :cond_0

    .line 696
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->c:Lcom/google/android/gms/drive/aj;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/c/a;->a(Lcom/google/android/gms/drive/aj;)Lcom/google/android/gms/drive/c/a;

    .line 699
    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->a()V

    .line 700
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->B:Lcom/google/android/gms/drive/c/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/d;->b()V

    .line 701
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/drive/ui/picker/m;I)V
    .locals 0

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/ui/picker/m;->a(I)V

    return-void
.end method

.method private b(Lcom/google/android/gms/drive/DriveId;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 921
    if-nez p1, :cond_1

    .line 934
    :cond_0
    :goto_0
    return v0

    .line 925
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->l:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/ab;->a:Lcom/google/android/gms/drive/ui/picker/ab;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/ui/picker/ab;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 934
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1117
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->t:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "application/vnd.google-apps.folder"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->t:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/google/android/gms/drive/ui/picker/m;)Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->l:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/drive/ui/picker/m;)V
    .locals 5

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/DriveId;)V

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/b;->i:Lcom/google/android/gms/drive/p;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    iget-wide v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->m:J

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/drive/p;->a(Lcom/google/android/gms/common/api/v;JLcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/ui/picker/q;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/ui/picker/q;-><init>(Lcom/google/android/gms/drive/ui/picker/m;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/gms/drive/ui/picker/m;)Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method private e()V
    .locals 8

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 477
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->j:Landroid/widget/TextView;

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->v:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 478
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    if-nez v0, :cond_7

    move v0, v3

    :goto_1
    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->setVisibility(I)V

    .line 479
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->eY:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    if-eqz v1, :cond_8

    :goto_2
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    .line 480
    return-void

    .line 477
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->j:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/g;->q:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v4, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->j:Landroid/widget/TextView;

    if-nez v1, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->v:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->v:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->np:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->j:Landroid/widget/TextView;

    if-nez v1, :cond_5

    move-object v2, v0

    :goto_4
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->v:Landroid/view/View;

    sget v5, Lcom/google/android/gms/j;->jV:I

    invoke-virtual {v1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    if-eqz v5, :cond_3

    iget-object v5, p0, Lcom/google/android/gms/drive/ui/picker/m;->l:Lcom/google/android/gms/drive/DriveId;

    iget-object v6, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {v5, v6}, Lcom/google/android/gms/drive/DriveId;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    :cond_3
    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    iget v1, v1, Lcom/google/android/gms/drive/ui/picker/ab;->h:I

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->k:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->j:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/g;->r:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {v1, v4, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->k:Landroid/widget/TextView;

    move-object v2, v1

    goto :goto_4

    :cond_6
    sget-object v5, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    iget-object v6, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    iget-object v7, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-interface {v5, v6, v7}, Lcom/google/android/gms/drive/h;->b(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/q;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    invoke-interface {v5, v6}, Lcom/google/android/gms/drive/q;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v5

    new-instance v6, Lcom/google/android/gms/drive/ui/picker/y;

    invoke-direct {v6, p0, v2, v0, v1}, Lcom/google/android/gms/drive/ui/picker/y;-><init>(Lcom/google/android/gms/drive/ui/picker/m;Landroid/widget/TextView;Landroid/widget/TextView;Landroid/widget/ImageView;)V

    invoke-interface {v5, v6}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto/16 :goto_0

    :cond_7
    move v0, v4

    .line 478
    goto/16 :goto_1

    :cond_8
    move v3, v4

    .line 479
    goto/16 :goto_2
.end method

.method static synthetic f(Lcom/google/android/gms/drive/ui/picker/m;)J
    .locals 2

    .prologue
    .line 92
    iget-wide v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->m:J

    return-wide v0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 654
    sget-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/h;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/drive/q;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/q;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->l:Lcom/google/android/gms/drive/DriveId;

    .line 656
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/drive/ui/picker/ab;->a:Lcom/google/android/gms/drive/ui/picker/ab;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/ab;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 657
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->l:Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    .line 661
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    if-nez v0, :cond_0

    const-string v0, "application/vnd.google-apps.folder"

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/m;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 662
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->l:Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    .line 666
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    if-nez v0, :cond_2

    .line 667
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/m;->e()V

    .line 668
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/m;->g()V

    .line 673
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/m;->h()V

    .line 675
    iget-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->s:Z

    if-nez v0, :cond_1

    .line 676
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->s:Z

    .line 677
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/m;->i()Lcom/google/android/gms/drive/ui/picker/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1, v2}, Lcom/google/android/gms/drive/h;->d(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/drive/ui/picker/z;

    invoke-direct {v2, p0}, Lcom/google/android/gms/drive/ui/picker/z;-><init>(Lcom/google/android/gms/drive/ui/picker/m;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/drive/ui/picker/j;->a(Lcom/google/android/gms/common/api/am;Lcom/google/android/gms/common/api/aq;)V

    .line 685
    :cond_1
    return-void

    .line 670
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/DriveId;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    goto :goto_1
.end method

.method static synthetic g(Lcom/google/android/gms/drive/ui/picker/m;)Lcom/google/android/gms/drive/ui/picker/view/DocListView;
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    return-object v0
.end method

.method private g()V
    .locals 2

    .prologue
    .line 938
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->w:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    invoke-direct {p0, v1}, Lcom/google/android/gms/drive/ui/picker/m;->b(Lcom/google/android/gms/drive/DriveId;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 939
    return-void
.end method

.method static synthetic h(Lcom/google/android/gms/drive/ui/picker/m;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 92
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->p:Ljava/lang/Runnable;

    return-object v0
.end method

.method private h()V
    .locals 2

    .prologue
    .line 942
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->u:Landroid/view/MenuItem;

    if-eqz v0, :cond_0

    .line 943
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/m;->b(Lcom/google/android/gms/drive/DriveId;)Z

    move-result v0

    .line 944
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->u:Landroid/view/MenuItem;

    invoke-interface {v1, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 945
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->u:Landroid/view/MenuItem;

    invoke-interface {v1}, Landroid/view/MenuItem;->getIcon()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v0, :cond_1

    const/16 v0, 0xff

    :goto_0
    invoke-virtual {v1, v0}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 948
    :cond_0
    return-void

    .line 945
    :cond_1
    const/16 v0, 0x99

    goto :goto_0
.end method

.method private i()Lcom/google/android/gms/drive/ui/picker/j;
    .locals 1

    .prologue
    .line 1059
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/OpenFileActivityDelegate;->e()Lcom/google/android/gms/drive/ui/picker/j;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method final a()V
    .locals 1

    .prologue
    .line 647
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/m;->f()V

    .line 648
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    if-eqz v0, :cond_0

    .line 649
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->c()V

    .line 651
    :cond_0
    return-void
.end method

.method public final a(Landroid/view/View;ILcom/google/android/gms/drive/DriveId;)V
    .locals 0

    .prologue
    .line 723
    invoke-direct {p0, p3}, Lcom/google/android/gms/drive/ui/picker/m;->a(Lcom/google/android/gms/drive/DriveId;)V

    .line 724
    return-void
.end method

.method final a(Lcom/google/android/gms/common/api/v;)V
    .locals 0

    .prologue
    .line 232
    iput-object p1, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    .line 233
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 1107
    const-string v0, "application/vnd.google-apps.folder"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/ui/picker/m;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()V
    .locals 1

    .prologue
    .line 704
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/m;->b(I)V

    .line 705
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->z:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/drive/ui/picker/r;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/ui/picker/r;-><init>(Lcom/google/android/gms/drive/ui/picker/m;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1056
    return-void
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 1068
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 1069
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1101
    :cond_0
    :goto_0
    return-void

    .line 1072
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/m;->i()Lcom/google/android/gms/drive/ui/picker/j;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/j;->a()Z

    move-result v1

    .line 1073
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    if-eqz v1, :cond_3

    sget-object v0, Lcom/google/android/gms/drive/ui/picker/view/h;->b:Lcom/google/android/gms/drive/ui/picker/view/h;

    :goto_1
    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/drive/ui/picker/view/h;)V

    .line 1074
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->h:Landroid/widget/ProgressBar;

    if-eqz v1, :cond_4

    const/4 v0, 0x0

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1077
    if-eqz v1, :cond_5

    .line 1078
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->C:Lcom/google/android/gms/drive/c/a;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->B:Lcom/google/android/gms/drive/c/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/d;->e()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1079
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->B:Lcom/google/android/gms/drive/c/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/d;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    const/4 v2, 0x3

    const/16 v3, 0x1e

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/drive/c/a;->a(II)Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->C:Lcom/google/android/gms/drive/c/a;

    .line 1091
    :cond_2
    :goto_3
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->p:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    if-eqz v0, :cond_0

    .line 1092
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/s;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/ui/picker/s;-><init>(Lcom/google/android/gms/drive/ui/picker/m;)V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->p:Ljava/lang/Runnable;

    .line 1099
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->p:Ljava/lang/Runnable;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 1073
    :cond_3
    sget-object v0, Lcom/google/android/gms/drive/ui/picker/view/h;->c:Lcom/google/android/gms/drive/ui/picker/view/h;

    goto :goto_1

    .line 1074
    :cond_4
    const/16 v0, 0x8

    goto :goto_2

    .line 1084
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->C:Lcom/google/android/gms/drive/c/a;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->B:Lcom/google/android/gms/drive/c/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/d;->e()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1085
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->C:Lcom/google/android/gms/drive/c/a;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->d()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->a()V

    .line 1087
    :cond_6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->C:Lcom/google/android/gms/drive/c/a;

    goto :goto_3
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 734
    if-nez p1, :cond_1

    .line 735
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 736
    const-string v0, "driveId"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    .line 741
    :cond_0
    :goto_0
    return-void

    .line 739
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/drive/ui/j;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 237
    new-instance v0, Lcom/google/android/gms/drive/ui/legacy/navigation/g;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ui/legacy/navigation/g;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->a:Lcom/google/android/gms/drive/ui/legacy/navigation/f;

    .line 239
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/j;->onCreate(Landroid/os/Bundle;)V

    .line 240
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 242
    const-string v0, "accountName"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->q:Ljava/lang/String;

    .line 243
    const-string v0, "callerPackagingId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->m:J

    .line 244
    const-string v0, "callerPackageName"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->n:Ljava/lang/String;

    .line 245
    const-string v0, "mimeTypes"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    invoke-static {v2}, Lcom/google/android/gms/drive/g/z;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->t:Ljava/util/Set;

    .line 246
    const-string v0, "driveId"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    .line 247
    const-string v0, "dialogTitle"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->r:Ljava/lang/String;

    .line 249
    if-eqz p1, :cond_0

    .line 250
    const-string v0, "driveId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    .line 251
    const-string v0, "parentDriveId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->e:Lcom/google/android/gms/drive/DriveId;

    .line 252
    const-string v0, "sortKind"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/a/a/t;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->o:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    .line 253
    const-string v0, "topCollection"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 254
    if-eqz v0, :cond_2

    invoke-static {v0}, Lcom/google/android/gms/drive/ui/picker/ab;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/ui/picker/ab;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    .line 256
    const-string v0, "currentFolderDriveId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    .line 257
    const-string v0, "hasRequestedInitialSync"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->s:Z

    .line 259
    const-string v0, "logSessionState"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->D:Landroid/os/Bundle;

    .line 261
    :cond_0
    return-void

    .line 245
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 254
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 3

    .prologue
    .line 418
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/drive/ui/j;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    .line 419
    sget v0, Lcom/google/android/gms/m;->c:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 420
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 421
    const-string v1, "showNewFolder"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    .line 422
    if-eqz v0, :cond_0

    .line 423
    sget v0, Lcom/google/android/gms/j;->eT:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->u:Landroid/view/MenuItem;

    .line 427
    :goto_0
    return-void

    .line 425
    :cond_0
    sget v0, Lcom/google/android/gms/j;->eT:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 11

    .prologue
    const/16 v10, 0x15

    const/4 v9, 0x0

    const/16 v8, 0x8

    .line 272
    sget v0, Lcom/google/android/gms/l;->ar:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 274
    sget v0, Lcom/google/android/gms/j;->eF:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    .line 277
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    sget-object v1, Lcom/google/android/gms/drive/ui/picker/view/h;->b:Lcom/google/android/gms/drive/ui/picker/view/h;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/drive/ui/picker/view/h;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Landroid/support/v4/app/Fragment;)V

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/drive/ui/picker/a/t;)V

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->q:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Ljava/lang/String;)V

    .line 281
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->a(Lcom/google/android/gms/drive/ui/picker/a/s;)V

    .line 282
    sget v0, Lcom/google/android/gms/j;->eX:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->h:Landroid/widget/ProgressBar;

    .line 288
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xf

    if-ne v0, v1, :cond_0

    .line 289
    new-instance v0, Landroid/widget/FrameLayout$LayoutParams;

    const/4 v1, -0x1

    const/4 v2, -0x2

    invoke-direct {v0, v1, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    .line 291
    const/4 v1, -0x4

    invoke-virtual {v0, v9, v1, v9, v9}, Landroid/widget/FrameLayout$LayoutParams;->setMargins(IIII)V

    .line 292
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->h:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 295
    :cond_0
    sget v0, Lcom/google/android/gms/j;->eV:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->v:Landroid/view/View;

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->v:Landroid/view/View;

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 298
    new-instance v1, Lcom/google/android/gms/drive/ui/picker/n;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/ui/picker/n;-><init>(Lcom/google/android/gms/drive/ui/picker/m;)V

    .line 305
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->v:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->eW:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 306
    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 308
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/ActionBar;->getCustomView()Landroid/view/View;

    move-result-object v2

    sget v0, Lcom/google/android/gms/j;->y:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->j:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gms/drive/ui/picker/m;->r:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->j:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v4, Lcom/google/android/gms/g;->q:I

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v0, v9, v3}, Landroid/widget/TextView;->setTextSize(IF)V

    sget v0, Lcom/google/android/gms/j;->v:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->k:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->k:Landroid/widget/TextView;

    sget-object v3, Lcom/google/android/gms/drive/ui/picker/ab;->a:Lcom/google/android/gms/drive/ui/picker/ab;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/ui/picker/ab;->b()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->k:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    sget v0, Lcom/google/android/gms/j;->u:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->i:Landroid/widget/ImageView;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, v8}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 310
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    .line 311
    sget v0, Lcom/google/android/gms/j;->eY:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/widget/ListView;

    .line 313
    invoke-virtual {v6, v8}, Landroid/widget/ListView;->setVisibility(I)V

    .line 314
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/t;

    sget v3, Lcom/google/android/gms/l;->as:I

    sget v4, Lcom/google/android/gms/j;->mq:I

    invoke-static {}, Lcom/google/android/gms/drive/ui/picker/ab;->c()[Lcom/google/android/gms/drive/ui/picker/ab;

    move-result-object v5

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/ui/picker/t;-><init>(Lcom/google/android/gms/drive/ui/picker/m;Landroid/content/Context;II[Lcom/google/android/gms/drive/ui/picker/ab;)V

    .line 330
    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 331
    new-instance v0, Lcom/google/android/gms/drive/ui/picker/u;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/ui/picker/u;-><init>(Lcom/google/android/gms/drive/ui/picker/m;)V

    invoke-virtual {v6, v0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    .line 340
    sget v0, Lcom/google/android/gms/j;->eJ:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->w:Landroid/widget/Button;

    .line 341
    sget v0, Lcom/google/android/gms/j;->eI:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->x:Landroid/widget/Button;

    .line 344
    new-instance v1, Lcom/google/android/gms/drive/ui/picker/v;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/ui/picker/v;-><init>(Lcom/google/android/gms/drive/ui/picker/m;)V

    .line 359
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->w:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 360
    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->w:Landroid/widget/Button;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "selectButtonText"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    if-lez v3, :cond_5

    invoke-virtual {p0, v3}, Lcom/google/android/gms/drive/ui/picker/m;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_2
    :goto_0
    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 361
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->w:Landroid/widget/Button;

    invoke-virtual {v0, v9}, Landroid/widget/Button;->setEnabled(Z)V

    .line 362
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->x:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 363
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->x:Landroid/widget/Button;

    const/high16 v1, 0x1040000

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    .line 366
    invoke-static {v10}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 367
    sget v0, Lcom/google/android/gms/j;->eS:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 368
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    .line 371
    :cond_3
    invoke-static {v10}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    sget v0, Lcom/google/android/gms/j;->eR:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    sget v0, Lcom/google/android/gms/j;->eQ:I

    invoke-virtual {v7, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/g;->s:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setElevation(F)V

    .line 373
    :cond_4
    return-object v7

    .line 360
    :cond_5
    const-string v3, "selectButtonText"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    sget v0, Lcom/google/android/gms/p;->fT:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/ui/picker/m;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final onDestroyView()V
    .locals 1

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ui/picker/view/DocListView;->h()V

    .line 412
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->g:Lcom/google/android/gms/drive/ui/picker/view/DocListView;

    .line 413
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/j;->onDestroyView()V

    .line 414
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 4

    .prologue
    .line 435
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    .line 436
    sget v1, Lcom/google/android/gms/j;->eT:I

    if-ne v0, v1, :cond_0

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->q:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/drive/ui/picker/CreateFolderActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/drive/DriveId;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/drive/ui/picker/m;->startActivityForResult(Landroid/content/Intent;I)V

    .line 470
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0

    .line 438
    :cond_0
    sget v1, Lcom/google/android/gms/j;->eU:I

    if-ne v0, v1, :cond_2

    .line 439
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 440
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/m;->i()Lcom/google/android/gms/drive/ui/picker/j;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1, v2}, Lcom/google/android/gms/drive/h;->d(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/drive/ui/picker/w;

    invoke-direct {v2, p0}, Lcom/google/android/gms/drive/ui/picker/w;-><init>(Lcom/google/android/gms/drive/ui/picker/m;)V

    new-instance v3, Lcom/google/android/gms/drive/ui/picker/x;

    invoke-direct {v3, p0}, Lcom/google/android/gms/drive/ui/picker/x;-><init>(Lcom/google/android/gms/drive/ui/picker/m;)V

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/drive/ui/picker/j;->a(Lcom/google/android/gms/common/api/am;Lcom/google/android/gms/common/api/aq;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 465
    :cond_1
    sget v0, Lcom/google/android/gms/p;->gF:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/ui/picker/m;->a(I)V

    goto :goto_0

    .line 468
    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/j;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_1
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 607
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/j;->onPause()V

    .line 608
    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 609
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->B:Lcom/google/android/gms/drive/c/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/d;->m_()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->D:Landroid/os/Bundle;

    .line 611
    :cond_0
    return-void
.end method

.method public final onResume()V
    .locals 6

    .prologue
    .line 615
    invoke-super {p0}, Lcom/google/android/gms/drive/ui/j;->onResume()V

    .line 616
    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->A:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 617
    invoke-direct {p0}, Lcom/google/android/gms/drive/ui/picker/m;->f()V

    .line 619
    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/c/a/h;

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/drive/g/aw;->b(Landroid/content/Context;)Lcom/google/android/gms/drive/c/a/n;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/ui/picker/m;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/c/a/h;-><init>(Lcom/google/android/gms/drive/c/a/n;Landroid/content/Context;)V

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->D:Landroid/os/Bundle;

    if-nez v1, :cond_1

    new-instance v1, Lcom/google/android/gms/drive/auth/CallingAppInfo;

    iget-wide v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->m:J

    iget-object v4, p0, Lcom/google/android/gms/drive/ui/picker/m;->n:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/drive/auth/CallingAppInfo;-><init>(JLjava/lang/String;I)V

    iget-object v2, p0, Lcom/google/android/gms/drive/ui/picker/m;->q:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/c/c;->b(Lcom/google/android/gms/drive/auth/CallingAppInfo;Ljava/lang/String;)Lcom/google/android/gms/drive/c/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->B:Lcom/google/android/gms/drive/c/d;

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->B:Lcom/google/android/gms/drive/c/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/d;->a()V

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->B:Lcom/google/android/gms/drive/c/d;

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/d;->c()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->b()Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    const/4 v1, 0x3

    const/16 v2, 0x1f

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/c/a;->a(II)Lcom/google/android/gms/drive/c/a;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/c/a;->a()V

    .line 620
    :goto_0
    return-void

    .line 619
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->D:Landroid/os/Bundle;

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/c/c;->a(Landroid/os/Bundle;)Lcom/google/android/gms/drive/c/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->B:Lcom/google/android/gms/drive/c/d;

    goto :goto_0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 582
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/ui/j;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 584
    const-string v0, "driveId"

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->b:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 585
    const-string v0, "parentDriveId"

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->e:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 586
    const-string v0, "sortKind"

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->o:Lcom/google/android/gms/drive/ui/picker/a/a/t;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 587
    const-string v1, "topCollection"

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/drive/ui/picker/m;->y:Lcom/google/android/gms/drive/ui/picker/ab;

    iget-object v0, v0, Lcom/google/android/gms/drive/ui/picker/ab;->e:Ljava/lang/String;

    :goto_0
    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 589
    const-string v0, "currentFolderDriveId"

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->f:Lcom/google/android/gms/drive/DriveId;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 590
    const-string v0, "hasRequestedInitialSync"

    iget-boolean v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->s:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 591
    const-string v0, "logSessionState"

    iget-object v1, p0, Lcom/google/android/gms/drive/ui/picker/m;->D:Landroid/os/Bundle;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 592
    return-void

    .line 587
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
