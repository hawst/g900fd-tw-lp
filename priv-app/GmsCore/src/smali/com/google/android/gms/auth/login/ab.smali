.class final Lcom/google/android/gms/auth/login/ab;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field a:Z

.field final synthetic b:Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;)V
    .locals 1

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/gms/auth/login/ab;->b:Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/login/ab;->a:Z

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 145
    iget-boolean v0, p0, Lcom/google/android/gms/auth/login/ab;->a:Z

    if-nez v0, :cond_0

    .line 146
    iput-boolean v1, p0, Lcom/google/android/gms/auth/login/ab;->a:Z

    .line 150
    iget v0, p1, Landroid/os/Message;->arg1:I

    if-ne v0, v1, :cond_1

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ab;->b:Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->setResult(I)V

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ab;->b:Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->finish()V

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    iget v0, p1, Landroid/os/Message;->arg1:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 157
    invoke-static {}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->l()Lcom/google/android/gms/auth/d/a;

    move-result-object v0

    const-string v1, "Checkin completed but failed."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ab;->b:Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;

    invoke-static {v0}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->a(Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;)V

    goto :goto_0

    .line 162
    :cond_2
    invoke-static {}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->l()Lcom/google/android/gms/auth/d/a;

    move-result-object v0

    const-string v1, "Checkin timed out."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/auth/login/ab;->b:Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;

    invoke-static {v0}, Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;->a(Lcom/google/android/gms/auth/login/CheckinInterstitialActivity;)V

    goto :goto_0
.end method
