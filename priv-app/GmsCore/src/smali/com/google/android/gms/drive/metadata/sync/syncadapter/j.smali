.class public final Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;
.super Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;
.source "SourceFile"


# instance fields
.field public b:Ljava/lang/String;

.field public c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;-><init>(Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;)V

    .line 18
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;->b:Ljava/lang/String;

    .line 19
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;->c:Z

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 27
    iget-boolean v0, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;->c:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v2, "Already finished. Did you delegate from more than one FeedProcessor to this one?"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 29
    invoke-super {p0, p1}, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;->a(Ljava/lang/String;)V

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;->b:Ljava/lang/String;

    .line 34
    iput-boolean v1, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/j;->c:Z

    .line 35
    return-void

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 50
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "NextFeedMonitorProcessor[delegate=%s]"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/metadata/sync/syncadapter/h;->a:Lcom/google/android/gms/drive/metadata/sync/syncadapter/e;

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
