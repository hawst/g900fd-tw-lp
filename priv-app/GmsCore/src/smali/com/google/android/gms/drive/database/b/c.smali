.class public final Lcom/google/android/gms/drive/database/b/c;
.super Lcom/google/android/gms/drive/database/b/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/drive/database/i;

.field private final b:Lcom/google/android/gms/drive/database/r;

.field private c:Landroid/database/Cursor;

.field private final d:Lcom/google/android/gms/drive/database/a/f;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Landroid/database/Cursor;Lcom/google/android/gms/drive/database/a/f;)V
    .locals 1

    .prologue
    .line 39
    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/database/b/a;-><init>(I)V

    .line 40
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/i;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/b/c;->a:Lcom/google/android/gms/drive/database/i;

    .line 41
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/b/c;->c:Landroid/database/Cursor;

    .line 42
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/r;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/b/c;->b:Lcom/google/android/gms/drive/database/r;

    .line 43
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/database/a/f;

    iput-object v0, p0, Lcom/google/android/gms/drive/database/b/c;->d:Lcom/google/android/gms/drive/database/a/f;

    .line 44
    return-void
.end method


# virtual methods
.method protected final a(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/drive/database/b/c;->c:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Failed to move delegate cursor."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/database/b/c;->d:Lcom/google/android/gms/drive/database/a/f;

    iget-object v1, p0, Lcom/google/android/gms/drive/database/b/c;->a:Lcom/google/android/gms/drive/database/i;

    iget-object v2, p0, Lcom/google/android/gms/drive/database/b/c;->b:Lcom/google/android/gms/drive/database/r;

    iget-object v3, p0, Lcom/google/android/gms/drive/database/b/c;->c:Landroid/database/Cursor;

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/drive/database/a/f;->a(Lcom/google/android/gms/drive/database/i;Lcom/google/android/gms/drive/database/r;Landroid/database/Cursor;)Ljava/lang/Object;

    move-result-object v0

    .line 59
    const-string v1, "Result decoder returned null."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final a()V
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/drive/database/b/c;->c:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 49
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/database/b/c;->c:Landroid/database/Cursor;

    .line 50
    return-void
.end method
