.class public abstract Lcom/google/android/gms/location/copresence/u;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/location/copresence/u;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static b:Lcom/google/android/gms/location/copresence/u;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field private static c:Lcom/google/android/gms/location/copresence/u;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 34
    new-instance v2, Lcom/google/android/gms/location/copresence/v;

    invoke-direct {v2}, Lcom/google/android/gms/location/copresence/v;-><init>()V

    iget-boolean v0, v2, Lcom/google/android/gms/location/copresence/v;->c:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Cannot call setNoOptInRequired() in conjunction with setWakeUpOthers()."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    iput-boolean v1, v2, Lcom/google/android/gms/location/copresence/v;->b:Z

    invoke-virtual {v2}, Lcom/google/android/gms/location/copresence/v;->a()Lcom/google/android/gms/location/copresence/u;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/copresence/u;->a:Lcom/google/android/gms/location/copresence/u;

    .line 35
    new-instance v0, Lcom/google/android/gms/location/copresence/v;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/v;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/v;->a()Lcom/google/android/gms/location/copresence/u;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/copresence/u;->b:Lcom/google/android/gms/location/copresence/u;

    .line 36
    new-instance v0, Lcom/google/android/gms/location/copresence/v;

    invoke-direct {v0}, Lcom/google/android/gms/location/copresence/v;-><init>()V

    iput-boolean v1, v0, Lcom/google/android/gms/location/copresence/v;->a:Z

    invoke-virtual {v0}, Lcom/google/android/gms/location/copresence/v;->a()Lcom/google/android/gms/location/copresence/u;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/location/copresence/u;->c:Lcom/google/android/gms/location/copresence/u;

    .line 37
    return-void

    .line 34
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    return-void
.end method
