.class public Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/googlehelp/common/b;


# instance fields
.field private a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

.field private b:Landroid/content/Intent;

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/googlehelp/common/a;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 102
    iget-object v1, p0, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->b:Landroid/content/Intent;

    const-string v3, "EXTRA_IS_VIDEO"

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->b:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-static {v1, p1}, Lcom/google/android/gms/googlehelp/common/e;->b(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V

    .line 104
    :goto_0
    if-eqz v0, :cond_0

    .line 105
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 106
    const/16 v1, 0x6f

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 107
    iput-boolean v5, p0, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->c:Z

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->b:Landroid/content/Intent;

    const-string v1, "EXTRA_IS_VIDEO"

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 111
    invoke-static {p0}, Lcom/google/android/gms/googlehelp/service/VideoCallStatusUpdateService;->a(Landroid/content/Context;)V

    .line 117
    :cond_0
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->c:Z

    if-eqz v0, :cond_1

    .line 119
    invoke-virtual {p1}, Lcom/google/android/gms/googlehelp/common/a;->b()Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    const-string v1, "hangout_was_opened"

    invoke-virtual {v0, v1, v5}, Lcom/google/android/gms/googlehelp/common/h;->a(Ljava/lang/String;Z)Lcom/google/android/gms/googlehelp/common/h;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/googlehelp/common/h;->a:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 124
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->finish()V

    .line 125
    return-void

    .line 102
    :cond_2
    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g()Landroid/accounts/Account;

    move-result-object v0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v1, "vnd.google.android.hangouts/vnd.google.android.hangout_privileged"

    invoke-static {p0, v1, v0, v5}, Lcom/google/android/gms/common/util/l;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string v1, "hangout_uri"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_3
    const-string v1, "auto_join_call_policy"

    const-string v2, "always"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->b:Landroid/content/Intent;

    const-string v3, "EXTRA_CHAT_CONVERSATION_ID"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {v1, p1}, Lcom/google/android/gms/googlehelp/common/e;->a(Lcom/google/android/gms/googlehelp/common/HelpConfig;Lcom/google/android/gms/googlehelp/common/a;)V

    goto :goto_0

    :cond_5
    invoke-virtual {v1}, Lcom/google/android/gms/googlehelp/common/HelpConfig;->g()Landroid/accounts/Account;

    move-result-object v0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v1, "vnd.google.android.hangouts/vnd.google.android.hangout_privileged"

    invoke-static {p0, v1, v0, v4}, Lcom/google/android/gms/common/util/l;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_6

    const-string v1, "conversation_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_6
    const-string v1, "auto_join_call_policy"

    const-string v2, "always"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 113
    :cond_7
    invoke-static {p0}, Lcom/google/android/gms/googlehelp/service/ChatStatusUpdateService;->a(Landroid/content/Context;)V

    goto :goto_1
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 39
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 41
    invoke-virtual {p0}, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->b:Landroid/content/Intent;

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->b:Landroid/content/Intent;

    const-string v1, "EXTRA_HELP_CONFIG"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/googlehelp/common/HelpConfig;

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot open a Hangout without a HelpConfig."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/helpactivities/OpenHangoutActivity;->a:Lcom/google/android/gms/googlehelp/common/HelpConfig;

    invoke-static {p0, p0, v0}, Lcom/google/android/gms/googlehelp/common/a;->a(Lcom/google/android/gms/googlehelp/common/b;Landroid/content/Context;Lcom/google/android/gms/googlehelp/common/HelpConfig;)V

    .line 50
    return-void
.end method
