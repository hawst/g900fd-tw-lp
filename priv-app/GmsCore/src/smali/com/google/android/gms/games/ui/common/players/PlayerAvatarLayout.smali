.class public final Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;
.super Landroid/widget/RelativeLayout;
.source "SourceFile"


# instance fields
.field private final a:Landroid/graphics/Rect;

.field private b:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 22
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 23
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 26
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 27
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 32
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;->a:Landroid/graphics/Rect;

    .line 33
    return-void
.end method


# virtual methods
.method protected final onFinishInflate()V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Landroid/widget/RelativeLayout;->onFinishInflate()V

    .line 39
    sget v0, Lcom/google/android/gms/j;->sj:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;->b:Landroid/view/View;

    .line 40
    return-void
.end method

.method public final onLayout(ZIIII)V
    .locals 3

    .prologue
    .line 44
    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;->b:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;->a:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 47
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/g;->at:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 49
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;->a:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->left:I

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 50
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;->a:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->right:I

    add-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 51
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;->a:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->top:I

    sub-int/2addr v2, v0

    iput v2, v1, Landroid/graphics/Rect;->top:I

    .line 52
    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;->a:Landroid/graphics/Rect;

    iget v2, v1, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    iput v0, v1, Landroid/graphics/Rect;->bottom:I

    .line 53
    new-instance v0, Lcom/google/android/play/utils/c;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;->a:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;->b:Landroid/view/View;

    invoke-direct {v0, v1, v2}, Lcom/google/android/play/utils/c;-><init>(Landroid/graphics/Rect;Landroid/view/View;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/common/players/PlayerAvatarLayout;->setTouchDelegate(Landroid/view/TouchDelegate;)V

    .line 54
    return-void
.end method
