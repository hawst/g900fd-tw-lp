.class public abstract Lcom/google/android/gms/cast/service/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Ljava/lang/String;


# instance fields
.field protected final b:Lcom/google/android/gms/cast/b/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 120
    const-string v0, "CastOperation"

    sput-object v0, Lcom/google/android/gms/cast/service/b;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/cast/b/e;)V
    .locals 2

    .prologue
    .line 123
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p1, p0, Lcom/google/android/gms/cast/service/b;->b:Lcom/google/android/gms/cast/b/e;

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/cast/service/b;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {p0}, Lcom/google/android/gms/cast/service/b;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/b/e;->c(Z)V

    .line 126
    return-void
.end method


# virtual methods
.method public abstract a()V
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/gms/cast/service/b;->b:Lcom/google/android/gms/cast/b/e;

    invoke-virtual {p0}, Lcom/google/android/gms/cast/service/b;->c()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/cast/b/e;->d(Z)V

    .line 132
    return-void
.end method

.method protected c()Z
    .locals 1

    .prologue
    .line 135
    const/4 v0, 0x0

    return v0
.end method
