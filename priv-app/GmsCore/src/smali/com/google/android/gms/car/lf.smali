.class public abstract Lcom/google/android/gms/car/lf;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/le;


# direct methods
.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/car/le;
    .locals 2

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v0, "com.google.android.gms.car.ICarCallListener"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/car/le;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/google/android/gms/car/le;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lcom/google/android/gms/car/lg;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/lg;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 37
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 228
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v2

    :goto_0
    return v2

    .line 45
    :sswitch_0
    const-string v0, "com.google.android.gms.car.ICarCallListener"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 50
    :sswitch_1
    const-string v0, "com.google.android.gms.car.ICarCallListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    sget-object v0, Landroid/view/KeyEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 58
    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/lf;->a(Landroid/view/KeyEvent;)V

    goto :goto_0

    :cond_0
    move-object v0, v1

    .line 56
    goto :goto_1

    .line 63
    :sswitch_2
    const-string v0, "com.google.android.gms.car.ICarCallListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 65
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v0, v2

    .line 67
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 70
    invoke-virtual {p0, v0, v1, v3}, Lcom/google/android/gms/car/lf;->a(ZII)V

    goto :goto_0

    .line 65
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 75
    :sswitch_3
    const-string v0, "com.google.android.gms.car.ICarCallListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 77
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_2

    .line 78
    sget-object v0, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall;

    .line 83
    :goto_3
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/lf;->a(Lcom/google/android/gms/car/CarCall;)V

    goto :goto_0

    :cond_2
    move-object v0, v1

    .line 81
    goto :goto_3

    .line 88
    :sswitch_4
    const-string v0, "com.google.android.gms.car.ICarCallListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 90
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_3

    .line 91
    sget-object v0, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall;

    .line 96
    :goto_4
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/lf;->b(Lcom/google/android/gms/car/CarCall;)V

    goto :goto_0

    :cond_3
    move-object v0, v1

    .line 94
    goto :goto_4

    .line 101
    :sswitch_5
    const-string v0, "com.google.android.gms.car.ICarCallListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 103
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 104
    sget-object v0, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall;

    .line 110
    :goto_5
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 111
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/lf;->a(Lcom/google/android/gms/car/CarCall;I)V

    goto/16 :goto_0

    :cond_4
    move-object v0, v1

    .line 107
    goto :goto_5

    .line 116
    :sswitch_6
    const-string v0, "com.google.android.gms.car.ICarCallListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 118
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_5

    .line 119
    sget-object v0, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall;

    move-object v3, v0

    .line 125
    :goto_6
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_6

    .line 126
    sget-object v0, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall;

    .line 131
    :goto_7
    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/car/lf;->a(Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall;)V

    goto/16 :goto_0

    :cond_5
    move-object v3, v1

    .line 122
    goto :goto_6

    :cond_6
    move-object v0, v1

    .line 129
    goto :goto_7

    .line 136
    :sswitch_7
    const-string v0, "com.google.android.gms.car.ICarCallListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 138
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_7

    .line 139
    sget-object v0, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall;

    .line 145
    :goto_8
    sget-object v1, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v1

    .line 146
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/lf;->a(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V

    goto/16 :goto_0

    :cond_7
    move-object v0, v1

    .line 142
    goto :goto_8

    .line 151
    :sswitch_8
    const-string v0, "com.google.android.gms.car.ICarCallListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 153
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_8

    .line 154
    sget-object v0, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall;

    move-object v3, v0

    .line 160
    :goto_9
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_9

    .line 161
    sget-object v0, Lcom/google/android/gms/car/CarCall$Details;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall$Details;

    .line 166
    :goto_a
    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/car/lf;->a(Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall$Details;)V

    goto/16 :goto_0

    :cond_8
    move-object v3, v1

    .line 157
    goto :goto_9

    :cond_9
    move-object v0, v1

    .line 164
    goto :goto_a

    .line 171
    :sswitch_9
    const-string v0, "com.google.android.gms.car.ICarCallListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 173
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_a

    .line 174
    sget-object v0, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall;

    .line 180
    :goto_b
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v1

    .line 181
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/lf;->b(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V

    goto/16 :goto_0

    :cond_a
    move-object v0, v1

    .line 177
    goto :goto_b

    .line 186
    :sswitch_a
    const-string v0, "com.google.android.gms.car.ICarCallListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 188
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_b

    .line 189
    sget-object v0, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall;

    .line 195
    :goto_c
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 196
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/lf;->a(Lcom/google/android/gms/car/CarCall;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_b
    move-object v0, v1

    .line 192
    goto :goto_c

    .line 201
    :sswitch_b
    const-string v0, "com.google.android.gms.car.ICarCallListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 203
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_c

    .line 204
    sget-object v0, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall;

    .line 209
    :goto_d
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/lf;->c(Lcom/google/android/gms/car/CarCall;)V

    goto/16 :goto_0

    :cond_c
    move-object v0, v1

    .line 207
    goto :goto_d

    .line 214
    :sswitch_c
    const-string v0, "com.google.android.gms.car.ICarCallListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 216
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_d

    .line 217
    sget-object v0, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall;

    .line 223
    :goto_e
    sget-object v1, Lcom/google/android/gms/car/CarCall;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v1

    .line 224
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/lf;->c(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V

    goto/16 :goto_0

    :cond_d
    move-object v0, v1

    .line 220
    goto :goto_e

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
