.class public final Lcom/google/android/c/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/LinkedList;

.field private static final b:Ljava/util/LinkedList;

.field private static final c:Ljava/util/LinkedList;

.field private static final d:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final e:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final f:Ljava/util/concurrent/locks/ReentrantLock;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 33
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/google/android/c/a/a;->a:Ljava/util/LinkedList;

    .line 34
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/google/android/c/a/a;->b:Ljava/util/LinkedList;

    .line 35
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    sput-object v0, Lcom/google/android/c/a/a;->c:Ljava/util/LinkedList;

    .line 37
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/c/a/a;->d:Ljava/util/concurrent/locks/ReentrantLock;

    .line 38
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/c/a/a;->e:Ljava/util/concurrent/locks/ReentrantLock;

    .line 39
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/c/a/a;->f:Ljava/util/concurrent/locks/ReentrantLock;

    return-void
.end method

.method public static a(I)Ljava/nio/ByteBuffer;
    .locals 4

    .prologue
    .line 53
    const/16 v0, 0x200

    if-gt p0, v0, :cond_1

    .line 56
    sget-object v1, Lcom/google/android/c/a/a;->c:Ljava/util/LinkedList;

    .line 57
    sget-object v0, Lcom/google/android/c/a/a;->d:Ljava/util/concurrent/locks/ReentrantLock;

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    .line 65
    :goto_0
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 66
    invoke-virtual {v0}, Ljava/util/LinkedList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 67
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 68
    invoke-static {p0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 75
    :cond_0
    :goto_1
    return-object v0

    .line 58
    :cond_1
    const/16 v0, 0x4000

    if-gt p0, v0, :cond_2

    .line 59
    sget-object v1, Lcom/google/android/c/a/a;->b:Ljava/util/LinkedList;

    .line 60
    sget-object v0, Lcom/google/android/c/a/a;->e:Ljava/util/concurrent/locks/ReentrantLock;

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    goto :goto_0

    .line 62
    :cond_2
    sget-object v1, Lcom/google/android/c/a/a;->a:Ljava/util/LinkedList;

    .line 63
    sget-object v0, Lcom/google/android/c/a/a;->f:Ljava/util/concurrent/locks/ReentrantLock;

    move-object v3, v0

    move-object v0, v1

    move-object v1, v3

    goto :goto_0

    .line 70
    :cond_3
    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 71
    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 72
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v1

    if-ge v1, p0, :cond_0

    .line 73
    invoke-static {p0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    goto :goto_1
.end method

.method public static a(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 79
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    .line 80
    invoke-virtual {p0, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 81
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 82
    const/16 v1, 0x200

    if-gt v0, v1, :cond_0

    .line 85
    sget-object v1, Lcom/google/android/c/a/a;->c:Ljava/util/LinkedList;

    .line 86
    sget-object v0, Lcom/google/android/c/a/a;->d:Ljava/util/concurrent/locks/ReentrantLock;

    .line 95
    :goto_0
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 96
    invoke-virtual {v1, p0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    .line 97
    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 98
    return-void

    .line 87
    :cond_0
    const/16 v1, 0x4000

    if-gt v0, v1, :cond_1

    .line 88
    sget-object v1, Lcom/google/android/c/a/a;->b:Ljava/util/LinkedList;

    .line 89
    sget-object v0, Lcom/google/android/c/a/a;->e:Ljava/util/concurrent/locks/ReentrantLock;

    goto :goto_0

    .line 91
    :cond_1
    sget-object v1, Lcom/google/android/c/a/a;->a:Ljava/util/LinkedList;

    .line 92
    sget-object v0, Lcom/google/android/c/a/a;->f:Ljava/util/concurrent/locks/ReentrantLock;

    goto :goto_0
.end method
