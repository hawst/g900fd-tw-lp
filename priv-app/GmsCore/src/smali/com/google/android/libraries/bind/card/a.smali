.class public abstract Lcom/google/android/libraries/bind/card/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/libraries/bind/c/b;

.field private static final c:Landroid/view/View$OnLongClickListener;


# instance fields
.field private final b:Lcom/google/android/libraries/bind/data/l;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    const-class v0, Lcom/google/android/libraries/bind/card/a;

    invoke-static {v0}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/Class;)Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    sput-object v0, Lcom/google/android/libraries/bind/card/a;->a:Lcom/google/android/libraries/bind/c/b;

    .line 95
    new-instance v0, Lcom/google/android/libraries/bind/card/b;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/card/b;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/card/a;->c:Landroid/view/View$OnLongClickListener;

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;I)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 617
    sget-object v0, Lcom/google/android/libraries/bind/card/a;->a:Lcom/google/android/libraries/bind/c/b;

    const-string v2, "startEditing position: %d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v1

    invoke-virtual {v0, v2, v3}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 619
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 620
    const/4 v2, 0x0

    .line 621
    :goto_0
    if-eqz v0, :cond_3

    .line 622
    instance-of v3, v0, Lcom/google/android/libraries/bind/experimental/card/a;

    if-eqz v3, :cond_0

    .line 623
    check-cast v0, Lcom/google/android/libraries/bind/experimental/card/a;

    .line 628
    :goto_1
    if-nez v0, :cond_1

    move v0, v1

    .line 636
    :goto_2
    return v0

    .line 626
    :cond_0
    invoke-interface {v0}, Landroid/view/ViewParent;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    goto :goto_0

    .line 632
    :cond_1
    iget-object v2, p0, Lcom/google/android/libraries/bind/card/a;->b:Lcom/google/android/libraries/bind/data/l;

    invoke-virtual {v2, p2}, Lcom/google/android/libraries/bind/data/l;->a(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v2

    .line 633
    sget v3, Lcom/google/android/libraries/bind/experimental/card/a;->a:I

    invoke-virtual {v2, v3}, Lcom/google/android/libraries/bind/data/Data;->e(I)Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    .line 634
    goto :goto_2

    .line 636
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/bind/card/a;->b:Lcom/google/android/libraries/bind/data/l;

    invoke-virtual {v1, p2}, Lcom/google/android/libraries/bind/data/l;->b(I)Ljava/lang/Object;

    invoke-interface {v0}, Lcom/google/android/libraries/bind/experimental/card/a;->a()Z

    move-result v0

    goto :goto_2

    :cond_3
    move-object v0, v2

    goto :goto_1
.end method
