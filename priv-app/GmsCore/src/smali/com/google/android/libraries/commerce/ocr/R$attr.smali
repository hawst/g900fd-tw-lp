.class public final Lcom/google/android/libraries/commerce/ocr/R$attr;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final adSize:I = 0x7f01003e

.field public static final adSizes:I = 0x7f01003f

.field public static final adUnitId:I = 0x7f010040

.field public static final allowShortcuts:I = 0x7f010099

.field public static final appTheme:I = 0x7f01017b

.field public static final buyButtonAppearance:I = 0x7f010182

.field public static final buyButtonHeight:I = 0x7f01017f

.field public static final buyButtonText:I = 0x7f010181

.field public static final buyButtonWidth:I = 0x7f010180

.field public static final cameraBearing:I = 0x7f0100aa

.field public static final cameraTargetLat:I = 0x7f0100ab

.field public static final cameraTargetLng:I = 0x7f0100ac

.field public static final cameraTilt:I = 0x7f0100ad

.field public static final cameraZoom:I = 0x7f0100ae

.field public static final circleCrop:I = 0x7f0100a8

.field public static final contentProviderUri:I = 0x7f01005f

.field public static final corpusId:I = 0x7f01005d

.field public static final corpusVersion:I = 0x7f01005e

.field public static final defaultIntentAction:I = 0x7f010096

.field public static final defaultIntentActivity:I = 0x7f010098

.field public static final defaultIntentData:I = 0x7f010097

.field public static final environment:I = 0x7f01017c

.field public static final featureType:I = 0x7f0100f1

.field public static final fragmentMode:I = 0x7f01017e

.field public static final fragmentStyle:I = 0x7f01017d

.field public static final imageAspectRatio:I = 0x7f0100a7

.field public static final imageAspectRatioAdjust:I = 0x7f0100a6

.field public static final indexPrefixes:I = 0x7f0100ee

.field public static final inputEnabled:I = 0x7f01009d

.field public static final liteMode:I = 0x7f0100af

.field public static final mapType:I = 0x7f0100a9

.field public static final maskedWalletDetailsBackground:I = 0x7f010185

.field public static final maskedWalletDetailsButtonBackground:I = 0x7f010187

.field public static final maskedWalletDetailsButtonTextAppearance:I = 0x7f010186

.field public static final maskedWalletDetailsHeaderTextAppearance:I = 0x7f010184

.field public static final maskedWalletDetailsLogoImageType:I = 0x7f010189

.field public static final maskedWalletDetailsLogoTextColor:I = 0x7f010188

.field public static final maskedWalletDetailsTextAppearance:I = 0x7f010183

.field public static final noIndex:I = 0x7f0100ec

.field public static final paramName:I = 0x7f010072

.field public static final paramValue:I = 0x7f010073

.field public static final schemaOrgProperty:I = 0x7f0100f0

.field public static final schemaOrgType:I = 0x7f010061

.field public static final searchEnabled:I = 0x7f010093

.field public static final searchLabel:I = 0x7f010094

.field public static final sectionContent:I = 0x7f01009b

.field public static final sectionFormat:I = 0x7f0100eb

.field public static final sectionId:I = 0x7f0100ea

.field public static final sectionType:I = 0x7f01009a

.field public static final sectionWeight:I = 0x7f0100ed

.field public static final settingsDescription:I = 0x7f010095

.field public static final sourceClass:I = 0x7f01009e

.field public static final subsectionSeparator:I = 0x7f0100ef

.field public static final toAddressesSection:I = 0x7f0100a2

.field public static final trimmable:I = 0x7f010060

.field public static final uiCompass:I = 0x7f0100b0

.field public static final uiMapToolbar:I = 0x7f0100b8

.field public static final uiRotateGestures:I = 0x7f0100b1

.field public static final uiScrollGestures:I = 0x7f0100b2

.field public static final uiTiltGestures:I = 0x7f0100b3

.field public static final uiZoomControls:I = 0x7f0100b4

.field public static final uiZoomGestures:I = 0x7f0100b5

.field public static final useViewLifecycle:I = 0x7f0100b6

.field public static final userInputSection:I = 0x7f0100a0

.field public static final userInputTag:I = 0x7f01009f

.field public static final userInputValue:I = 0x7f0100a1

.field public static final zOrderOnTop:I = 0x7f0100b7


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
