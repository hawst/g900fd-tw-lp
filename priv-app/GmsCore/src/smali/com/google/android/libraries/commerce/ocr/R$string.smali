.class public final Lcom/google/android/libraries/commerce/ocr/R$string;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final accept:I = 0x7f120010

.field public static final common_android_wear_notification_needs_update_text:I = 0x7f120266

.field public static final common_android_wear_update_text:I = 0x7f120267

.field public static final common_android_wear_update_title:I = 0x7f120268

.field public static final common_google_play_services_enable_button:I = 0x7f12027a

.field public static final common_google_play_services_enable_text:I = 0x7f12027b

.field public static final common_google_play_services_enable_title:I = 0x7f12027c

.field public static final common_google_play_services_error_notification_requested_by_msg:I = 0x7f12027e

.field public static final common_google_play_services_install_button:I = 0x7f12027f

.field public static final common_google_play_services_install_text_phone:I = 0x7f120280

.field public static final common_google_play_services_install_text_tablet:I = 0x7f120281

.field public static final common_google_play_services_install_title:I = 0x7f120282

.field public static final common_google_play_services_invalid_account_text:I = 0x7f120283

.field public static final common_google_play_services_invalid_account_title:I = 0x7f120284

.field public static final common_google_play_services_needs_enabling_title:I = 0x7f120285

.field public static final common_google_play_services_network_error_text:I = 0x7f120286

.field public static final common_google_play_services_network_error_title:I = 0x7f120287

.field public static final common_google_play_services_notification_needs_installation_title:I = 0x7f120289

.field public static final common_google_play_services_notification_needs_update_title:I = 0x7f12028a

.field public static final common_google_play_services_notification_ticker:I = 0x7f12028b

.field public static final common_google_play_services_unknown_issue:I = 0x7f12028c

.field public static final common_google_play_services_unsupported_text:I = 0x7f12028d

.field public static final common_google_play_services_unsupported_title:I = 0x7f12028e

.field public static final common_google_play_services_update_button:I = 0x7f12028f

.field public static final common_google_play_services_update_text:I = 0x7f120290

.field public static final common_google_play_services_update_title:I = 0x7f120291

.field public static final common_open_on_phone:I = 0x7f12029f

.field public static final common_signin_button_text:I = 0x7f1202a9

.field public static final common_signin_button_text_long:I = 0x7f1202aa

.field public static final create_calendar_message:I = 0x7f1202bd

.field public static final create_calendar_title:I = 0x7f1202be

.field public static final decline:I = 0x7f1202bf

.field public static final ocr_cc_enter_manually_details:I = 0x7f120666

.field public static final ocr_cc_invalid_cc_number:I = 0x7f120667

.field public static final ocr_cc_sample_number:I = 0x7f120668

.field public static final ocr_cc_scan_card_details:I = 0x7f120669

.field public static final ocr_confirm:I = 0x7f12066a

.field public static final ocr_error_title:I = 0x7f12066b

.field public static final ocr_help:I = 0x7f12066c

.field public static final ocr_skip_scan:I = 0x7f12066d

.field public static final ocr_take_picture:I = 0x7f12066e

.field public static final store_picture_message:I = 0x7f1208ae

.field public static final store_picture_title:I = 0x7f1208af

.field public static final wallet_buy_button_place_holder:I = 0x7f120943


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 229
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
