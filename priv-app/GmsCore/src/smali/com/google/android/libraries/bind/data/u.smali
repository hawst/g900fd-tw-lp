.class public Lcom/google/android/libraries/bind/data/u;
.super Lcom/google/android/libraries/bind/data/af;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/libraries/bind/data/ag;

.field protected final b:I

.field protected final c:Lcom/google/android/libraries/bind/data/k;

.field protected final d:Lcom/google/android/libraries/bind/data/r;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/data/l;Ljava/util/concurrent/Executor;Lcom/google/android/libraries/bind/data/r;Lcom/google/android/libraries/bind/data/l;)V
    .locals 1

    .prologue
    .line 128
    if-nez p3, :cond_0

    :goto_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/libraries/bind/data/af;-><init>(Lcom/google/android/libraries/bind/data/l;Ljava/util/concurrent/Executor;)V

    .line 129
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    iget-object v0, p4, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/u;->a:Lcom/google/android/libraries/bind/data/ag;

    .line 130
    iget v0, p4, Lcom/google/android/libraries/bind/data/l;->d:I

    iput v0, p0, Lcom/google/android/libraries/bind/data/u;->b:I

    .line 131
    iget-object v0, p4, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ag;->c()Lcom/google/android/libraries/bind/data/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/u;->c:Lcom/google/android/libraries/bind/data/k;

    .line 132
    iput-object p3, p0, Lcom/google/android/libraries/bind/data/u;->d:Lcom/google/android/libraries/bind/data/r;

    .line 133
    return-void

    .line 128
    :cond_0
    invoke-interface {p3}, Lcom/google/android/libraries/bind/data/r;->a()Ljava/util/concurrent/Executor;

    move-result-object p2

    goto :goto_0
.end method


# virtual methods
.method protected final a()V
    .locals 1

    .prologue
    .line 137
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/af;->a()V

    .line 138
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->d:Lcom/google/android/libraries/bind/data/r;

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->d:Lcom/google/android/libraries/bind/data/r;

    .line 141
    :cond_0
    return-void
.end method

.method protected final a(Lcom/google/android/libraries/bind/data/ag;Lcom/google/android/libraries/bind/data/j;)V
    .locals 2

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->h:Lcom/google/android/libraries/bind/data/l;

    iget v1, p0, Lcom/google/android/libraries/bind/data/u;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p0, p1, p2, v1}, Lcom/google/android/libraries/bind/data/l;->a(Lcom/google/android/libraries/bind/data/af;Lcom/google/android/libraries/bind/data/ag;Lcom/google/android/libraries/bind/data/j;Ljava/lang/Integer;)V

    .line 179
    return-void
.end method

.method protected b()Ljava/util/List;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->d:Lcom/google/android/libraries/bind/data/r;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->d:Lcom/google/android/libraries/bind/data/r;

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->a:Lcom/google/android/libraries/bind/data/ag;

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/ag;->a:Ljava/util/List;

    return-object v0
.end method

.method protected final c()Ljava/util/List;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 150
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->c:Lcom/google/android/libraries/bind/data/k;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->c:Lcom/google/android/libraries/bind/data/k;

    throw v0

    .line 153
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/u;->b()Ljava/util/List;

    move-result-object v0

    .line 154
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/af;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_2

    move-object v0, v2

    .line 173
    :cond_1
    :goto_0
    return-object v0

    .line 157
    :cond_2
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/u;->d:Lcom/google/android/libraries/bind/data/r;

    if-eqz v1, :cond_1

    .line 160
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v1, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 161
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/u;->d:Lcom/google/android/libraries/bind/data/r;

    if-eqz v3, :cond_5

    .line 162
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    .line 163
    iget-object v4, p0, Lcom/google/android/libraries/bind/data/u;->d:Lcom/google/android/libraries/bind/data/r;

    .line 164
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 168
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/af;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v2

    .line 169
    goto :goto_0

    .line 171
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/u;->d:Lcom/google/android/libraries/bind/data/r;

    invoke-interface {v0, v1}, Lcom/google/android/libraries/bind/data/r;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_0
.end method
