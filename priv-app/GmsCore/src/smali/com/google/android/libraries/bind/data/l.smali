.class public Lcom/google/android/libraries/bind/data/l;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected a:Lcom/google/android/libraries/bind/c/b;

.field protected final b:I

.field protected c:Lcom/google/android/libraries/bind/data/af;

.field d:I

.field e:Lcom/google/android/libraries/bind/data/ag;

.field protected f:Z

.field private final g:Lcom/google/android/libraries/bind/data/ag;

.field private final h:Lcom/google/android/libraries/bind/data/ab;

.field private i:Z

.field private j:Ljava/util/concurrent/CopyOnWriteArraySet;

.field private k:Z

.field private l:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/bind/data/l;-><init>(IB)V

    .line 62
    return-void
.end method

.method private constructor <init>(IB)V
    .locals 1

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    new-instance v0, Lcom/google/android/libraries/bind/data/ab;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/ab;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/l;->h:Lcom/google/android/libraries/bind/data/ab;

    .line 45
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/l;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    .line 65
    iput p1, p0, Lcom/google/android/libraries/bind/data/l;->b:I

    .line 66
    new-instance v0, Lcom/google/android/libraries/bind/data/ag;

    invoke-direct {v0, p1}, Lcom/google/android/libraries/bind/data/ag;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/l;->g:Lcom/google/android/libraries/bind/data/ag;

    .line 67
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->g:Lcom/google/android/libraries/bind/data/ag;

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    .line 73
    return-void
.end method

.method private l()Lcom/google/android/libraries/bind/c/b;
    .locals 1

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->a:Lcom/google/android/libraries/bind/c/b;

    if-nez v0, :cond_0

    .line 108
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/Class;)Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/l;->a:Lcom/google/android/libraries/bind/c/b;

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->a:Lcom/google/android/libraries/bind/c/b;

    return-object v0
.end method

.method private m()Z
    .locals 1

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->h:Lcom/google/android/libraries/bind/data/ab;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ab;->a()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 330
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->l()Lcom/google/android/libraries/bind/c/b;

    move-result-object v2

    const-string v3, "registerForInvalidation"

    new-array v4, v0, [Ljava/lang/Object;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 331
    iget-boolean v2, p0, Lcom/google/android/libraries/bind/data/l;->i:Z

    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/d/b;->a(Z)V

    .line 332
    iput-boolean v1, p0, Lcom/google/android/libraries/bind/data/l;->i:Z

    .line 333
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/l;->e()V

    .line 334
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/l;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/l;->k:Z

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/l;->h()Z

    move-result v0

    if-nez v0, :cond_2

    .line 335
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/l;->j()V

    .line 337
    :cond_2
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 340
    :cond_3
    return-void
.end method

.method private o()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 343
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->l()Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    const-string v1, "unregisterForInvalidation"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 344
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/l;->i:Z

    invoke-static {v0}, Lcom/google/android/libraries/bind/d/b;->a(Z)V

    .line 345
    iput-boolean v3, p0, Lcom/google/android/libraries/bind/data/l;->i:Z

    .line 346
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/l;->f()V

    .line 347
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->j:Ljava/util/concurrent/CopyOnWriteArraySet;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArraySet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_0

    .line 350
    :cond_0
    return-void
.end method

.method private p()V
    .locals 3

    .prologue
    .line 541
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->l()Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    const-string v1, "stopRefreshTask"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 542
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->c:Lcom/google/android/libraries/bind/data/af;

    if-eqz v0, :cond_0

    .line 543
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->c:Lcom/google/android/libraries/bind/data/af;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/af;->e()V

    .line 544
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/l;->c:Lcom/google/android/libraries/bind/data/af;

    .line 546
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 168
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    .line 169
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/ag;->b(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 134
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->l()Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    const-string v1, "invalidateData(clearList=%b)"

    new-array v2, v5, [Ljava/lang/Object;

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v5, p0, Lcom/google/android/libraries/bind/data/l;->l:Z

    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/l;->k:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/l;->j()V

    .line 135
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/libraries/bind/data/af;Lcom/google/android/libraries/bind/data/ag;Lcom/google/android/libraries/bind/data/j;Ljava/lang/Integer;)V
    .locals 6

    .prologue
    .line 409
    new-instance v0, Lcom/google/android/libraries/bind/data/m;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/libraries/bind/data/m;-><init>(Lcom/google/android/libraries/bind/data/l;Lcom/google/android/libraries/bind/data/af;Lcom/google/android/libraries/bind/data/ag;Lcom/google/android/libraries/bind/data/j;Ljava/lang/Integer;)V

    .line 418
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 419
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 423
    :goto_0
    return-void

    .line 421
    :cond_0
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->c()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/libraries/bind/data/ag;Lcom/google/android/libraries/bind/data/j;Ljava/lang/Integer;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 388
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    .line 389
    if-nez p1, :cond_0

    .line 390
    iget-object p1, p0, Lcom/google/android/libraries/bind/data/l;->g:Lcom/google/android/libraries/bind/data/ag;

    .line 392
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/libraries/bind/data/ag;->b()Z

    move-result v0

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/l;->l:Z

    .line 393
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->g:Lcom/google/android/libraries/bind/data/ag;

    if-ne p1, v0, :cond_4

    move v3, v1

    .line 394
    :goto_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ag;->b()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/libraries/bind/data/ag;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 395
    :cond_1
    :goto_2
    if-eqz v3, :cond_6

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    iget-object v4, p0, Lcom/google/android/libraries/bind/data/l;->g:Lcom/google/android/libraries/bind/data/ag;

    if-ne v0, v4, :cond_6

    if-nez v1, :cond_6

    .line 405
    :cond_2
    :goto_3
    return-void

    :cond_3
    move v0, v2

    .line 392
    goto :goto_0

    :cond_4
    move v3, v2

    .line 393
    goto :goto_1

    :cond_5
    move v1, v2

    .line 394
    goto :goto_2

    .line 399
    :cond_6
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    .line 400
    if-nez p3, :cond_7

    iget v0, p0, Lcom/google/android/libraries/bind/data/l;->d:I

    add-int/lit8 v0, v0, 0x1

    :goto_4
    iput v0, p0, Lcom/google/android/libraries/bind/data/l;->d:I

    .line 401
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->l()Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    const-string v1, "notifyDataChanged"

    new-array v4, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v4}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->h:Lcom/google/android/libraries/bind/data/ab;

    new-instance v1, Ljava/util/ArrayList;

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/ab;->a:Ljava/util/List;

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_5
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/ac;

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/ac;->a:Lcom/google/android/libraries/bind/data/n;

    invoke-virtual {v0, p2}, Lcom/google/android/libraries/bind/data/n;->a(Lcom/google/android/libraries/bind/data/j;)V

    goto :goto_5

    .line 400
    :cond_7
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_4

    .line 402
    :cond_8
    if-nez v3, :cond_2

    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/l;->f:Z

    if-eqz v0, :cond_2

    .line 403
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->l()Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    const-string v1, "stopAutoRefresh"

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v3}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    iput-boolean v2, p0, Lcom/google/android/libraries/bind/data/l;->k:Z

    goto :goto_3
.end method

.method public final a(Lcom/google/android/libraries/bind/data/j;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 383
    invoke-virtual {p0, v0, p1, v0}, Lcom/google/android/libraries/bind/data/l;->a(Lcom/google/android/libraries/bind/data/ag;Lcom/google/android/libraries/bind/data/j;Ljava/lang/Integer;)V

    .line 384
    return-void
.end method

.method public final a(Lcom/google/android/libraries/bind/data/n;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 274
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->h:Lcom/google/android/libraries/bind/data/ab;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Observer is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, v0, Lcom/google/android/libraries/bind/data/ab;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    new-instance v2, Lcom/google/android/libraries/bind/data/ac;

    invoke-direct {v2, p1, v4}, Lcom/google/android/libraries/bind/data/ac;-><init>(Lcom/google/android/libraries/bind/data/n;I)V

    iget-object v3, v0, Lcom/google/android/libraries/bind/data/ab;->a:Ljava/util/List;

    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/ab;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->n()V

    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->l()Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    const-string v1, "registerDataSetObserver - count: %d, registeredForInvalidation: %b"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/l;->h:Lcom/google/android/libraries/bind/data/ab;

    invoke-virtual {v3}, Lcom/google/android/libraries/bind/data/ab;->a()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget-boolean v4, p0, Lcom/google/android/libraries/bind/data/l;->i:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 275
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 160
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    .line 161
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ag;->a()I

    move-result v0

    return v0
.end method

.method public final b(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 176
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    .line 177
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/ag;->c(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/libraries/bind/data/n;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 294
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    .line 295
    iget-object v3, p0, Lcom/google/android/libraries/bind/data/l;->h:Lcom/google/android/libraries/bind/data/ab;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Observer is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, v3, Lcom/google/android/libraries/bind/data/ab;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v2

    :goto_0
    if-eqz v0, :cond_1

    .line 296
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->o()V

    .line 297
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->p()V

    .line 299
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->l()Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    const-string v1, "unregisterDataSetObserver - count: %d, registeredForInvalidation: %b"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/google/android/libraries/bind/data/l;->h:Lcom/google/android/libraries/bind/data/ab;

    invoke-virtual {v4}, Lcom/google/android/libraries/bind/data/ab;->a()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v2

    const/4 v2, 0x1

    iget-boolean v4, p0, Lcom/google/android/libraries/bind/data/l;->i:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {v0, v1, v3}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 301
    return-void

    :cond_2
    move v1, v2

    .line 295
    :goto_1
    iget-object v0, v3, Lcom/google/android/libraries/bind/data/ab;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    iget-object v0, v3, Lcom/google/android/libraries/bind/data/ab;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/ac;

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/ac;->a:Lcom/google/android/libraries/bind/data/n;

    if-ne v0, p1, :cond_4

    iget-object v0, v3, Lcom/google/android/libraries/bind/data/ab;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_3
    iget-object v0, v3, Lcom/google/android/libraries/bind/data/ab;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    goto :goto_0

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 184
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    .line 185
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ag;->d()Z

    move-result v0

    return v0
.end method

.method public d()Z
    .locals 1

    .prologue
    .line 322
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/l;->l:Z

    return v0
.end method

.method protected e()V
    .locals 0

    .prologue
    .line 358
    return-void
.end method

.method protected f()V
    .locals 0

    .prologue
    .line 366
    return-void
.end method

.method protected finalize()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 92
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/l;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->h:Lcom/google/android/libraries/bind/data/ab;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ab;->a()I

    move-result v0

    if-lez v0, :cond_1

    .line 93
    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->l()Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    const-string v1, "Leaked datalist"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 94
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->l()Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    const-string v1, "  Observables: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/l;->h:Lcom/google/android/libraries/bind/data/ab;

    aput-object v3, v2, v4

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->c(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    :cond_1
    invoke-super {p0}, Ljava/lang/Object;->finalize()V

    .line 97
    return-void
.end method

.method protected g()[I
    .locals 1

    .prologue
    .line 375
    const/4 v0, 0x0

    return-object v0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 438
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    .line 440
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/l;->g:Lcom/google/android/libraries/bind/data/ag;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()Lcom/google/android/libraries/bind/data/l;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 478
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->l()Lcom/google/android/libraries/bind/c/b;

    move-result-object v0

    const-string v1, "startAutoRefresh"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 479
    iput-boolean v3, p0, Lcom/google/android/libraries/bind/data/l;->f:Z

    .line 480
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/l;->k:Z

    if-nez v0, :cond_0

    .line 481
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/l;->k:Z

    .line 482
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/l;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 483
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/l;->j()V

    .line 486
    :cond_0
    return-object p0
.end method

.method public j()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 510
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    .line 511
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->l()Lcom/google/android/libraries/bind/c/b;

    move-result-object v1

    const-string v2, "refresh"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 512
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->l()Lcom/google/android/libraries/bind/c/b;

    move-result-object v1

    const-string v2, "startRefreshTask"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->p()V

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/l;->k()Lcom/google/android/libraries/bind/data/af;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/data/l;->c:Lcom/google/android/libraries/bind/data/af;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/l;->c:Lcom/google/android/libraries/bind/data/af;

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/l;->l()Lcom/google/android/libraries/bind/c/b;

    move-result-object v1

    const-string v2, "no refresh task"

    new-array v3, v0, [Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/l;->h()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/l;->l:Z

    .line 513
    :goto_0
    return-void

    .line 512
    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->c:Lcom/google/android/libraries/bind/data/af;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/af;->d()V

    goto :goto_0
.end method

.method protected k()Lcom/google/android/libraries/bind/data/af;
    .locals 1

    .prologue
    .line 554
    const/4 v0, 0x0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 81
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "%s - primaryKey: %s, size: %d, exception: %s"

    const/4 v0, 0x4

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v0, 0x0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x1

    iget v4, p0, Lcom/google/android/libraries/bind/data/l;->b:I

    invoke-static {v4}, Lcom/google/android/libraries/bind/data/Data;->f(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v0, 0x2

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/l;->b()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    const/4 v4, 0x3

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ag;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ag;->c()Lcom/google/android/libraries/bind/data/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/k;->getMessage()Ljava/lang/String;

    move-result-object v0

    :goto_0
    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string v0, "no"

    goto :goto_0
.end method
