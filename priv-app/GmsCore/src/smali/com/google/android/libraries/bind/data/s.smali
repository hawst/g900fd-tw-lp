.class public Lcom/google/android/libraries/bind/data/s;
.super Lcom/google/android/libraries/bind/data/l;
.source "SourceFile"


# instance fields
.field protected final g:Lcom/google/android/libraries/bind/data/r;

.field protected final h:Lcom/google/android/libraries/bind/data/l;

.field private final i:Lcom/google/android/libraries/bind/data/n;

.field private final j:[I

.field private k:I


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/data/l;Lcom/google/android/libraries/bind/data/r;[II)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 39
    invoke-direct {p0, p4}, Lcom/google/android/libraries/bind/data/l;-><init>(I)V

    .line 40
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/d/b;->a(Z)V

    .line 41
    iput-object p2, p0, Lcom/google/android/libraries/bind/data/s;->g:Lcom/google/android/libraries/bind/data/r;

    .line 42
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/s;->h:Lcom/google/android/libraries/bind/data/l;

    .line 43
    iput-object p3, p0, Lcom/google/android/libraries/bind/data/s;->j:[I

    .line 44
    new-instance v0, Lcom/google/android/libraries/bind/data/t;

    invoke-direct {v0, p0, v1}, Lcom/google/android/libraries/bind/data/t;-><init>(Lcom/google/android/libraries/bind/data/s;Z)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/s;->i:Lcom/google/android/libraries/bind/data/n;

    .line 53
    return-void

    :cond_0
    move v0, v1

    .line 40
    goto :goto_0
.end method


# virtual methods
.method public final d()Z
    .locals 2

    .prologue
    .line 87
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/l;->d()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/s;->h:Lcom/google/android/libraries/bind/data/l;

    iget v0, v0, Lcom/google/android/libraries/bind/data/l;->d:I

    iget v1, p0, Lcom/google/android/libraries/bind/data/s;->k:I

    if-eq v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final e()V
    .locals 2

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/s;->h:Lcom/google/android/libraries/bind/data/l;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/s;->i:Lcom/google/android/libraries/bind/data/n;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/l;->a(Lcom/google/android/libraries/bind/data/n;)V

    .line 93
    return-void
.end method

.method protected final f()V
    .locals 2

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/s;->h:Lcom/google/android/libraries/bind/data/l;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/s;->i:Lcom/google/android/libraries/bind/data/n;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/l;->b(Lcom/google/android/libraries/bind/data/n;)V

    .line 98
    return-void
.end method

.method protected final g()[I
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/s;->j:[I

    return-object v0
.end method

.method public final j()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/s;->h:Lcom/google/android/libraries/bind/data/l;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/l;->h()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/s;->h:Lcom/google/android/libraries/bind/data/l;

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ag;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 67
    :cond_0
    invoke-super {p0}, Lcom/google/android/libraries/bind/data/l;->j()V

    .line 72
    :goto_0
    return-void

    .line 70
    :cond_1
    sget-object v0, Lcom/google/android/libraries/bind/data/j;->a:Lcom/google/android/libraries/bind/data/j;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/data/s;->a(Lcom/google/android/libraries/bind/data/j;)V

    goto :goto_0
.end method

.method protected k()Lcom/google/android/libraries/bind/data/af;
    .locals 4

    .prologue
    .line 110
    new-instance v1, Lcom/google/android/libraries/bind/data/u;

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/s;->h:Lcom/google/android/libraries/bind/data/l;

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ag;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/libraries/bind/a/o;->c:Lcom/google/android/libraries/bind/a/j;

    :goto_0
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/s;->g:Lcom/google/android/libraries/bind/data/r;

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/s;->h:Lcom/google/android/libraries/bind/data/l;

    invoke-direct {v1, p0, v0, v2, v3}, Lcom/google/android/libraries/bind/data/u;-><init>(Lcom/google/android/libraries/bind/data/l;Ljava/util/concurrent/Executor;Lcom/google/android/libraries/bind/data/r;Lcom/google/android/libraries/bind/data/l;)V

    return-object v1

    :cond_0
    sget-object v0, Lcom/google/android/libraries/bind/a/o;->a:Lcom/google/android/libraries/bind/a/j;

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-super {p0}, Lcom/google/android/libraries/bind/data/l;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "\n\nParent:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/s;->h:Lcom/google/android/libraries/bind/data/l;

    invoke-virtual {v1}, Lcom/google/android/libraries/bind/data/l;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
