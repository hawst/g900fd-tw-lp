.class public final Lcom/google/android/libraries/bind/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field protected static a:Ljava/util/Map;

.field protected static b:Lcom/google/android/libraries/bind/c/a;

.field private static e:Z


# instance fields
.field protected c:Ljava/lang/String;

.field protected d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/c/b;->a:Ljava/util/Map;

    .line 15
    new-instance v0, Lcom/google/android/libraries/bind/c/c;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/c/c;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/c/b;->b:Lcom/google/android/libraries/bind/c/a;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/libraries/bind/c/b;->c:Ljava/lang/String;

    .line 43
    return-void
.end method

.method public static a(Ljava/lang/Class;)Lcom/google/android/libraries/bind/c/b;
    .locals 3

    .prologue
    .line 21
    invoke-virtual {p0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    sget-object v0, Lcom/google/android/libraries/bind/c/b;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/libraries/bind/c/b;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/c/b;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/libraries/bind/c/b;

    invoke-direct {v0, v1}, Lcom/google/android/libraries/bind/c/b;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/libraries/bind/c/b;->a:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method private static varargs a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    array-length v0, p2

    if-nez v0, :cond_2

    :cond_0
    move-object v0, p1

    .line 137
    :goto_0
    if-nez v0, :cond_1

    const-string v0, ""

    .line 139
    :cond_1
    return-object v0

    .line 135
    :cond_2
    invoke-static {p1, p2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final varargs a(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 65
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/c/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    sget-object v0, Lcom/google/android/libraries/bind/c/b;->b:Lcom/google/android/libraries/bind/c/a;

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/libraries/bind/c/b;->c:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v3, p1, p2}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/c/a;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 68
    :cond_0
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/c/b;->d:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/google/android/libraries/bind/c/b;->e:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final varargs b(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 77
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/c/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    sget-object v0, Lcom/google/android/libraries/bind/c/b;->b:Lcom/google/android/libraries/bind/c/a;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/libraries/bind/c/b;->c:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v3, p1, p2}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/c/a;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 80
    :cond_0
    return-void
.end method

.method public final varargs c(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 104
    sget-object v0, Lcom/google/android/libraries/bind/c/b;->b:Lcom/google/android/libraries/bind/c/a;

    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/libraries/bind/c/b;->c:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v3, p1, p2}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/c/a;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 105
    return-void
.end method

.method public final varargs d(Ljava/lang/String;[Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 121
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/c/b;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/libraries/bind/c/b;->b:Lcom/google/android/libraries/bind/c/a;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/libraries/bind/c/b;->c:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-static {v3, p1, p2}, Lcom/google/android/libraries/bind/c/b;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/libraries/bind/c/a;->a(ILjava/lang/String;Ljava/lang/String;)V

    .line 122
    :cond_0
    return-void
.end method
