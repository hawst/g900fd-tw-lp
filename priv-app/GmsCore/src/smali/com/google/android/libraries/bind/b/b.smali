.class public Lcom/google/android/libraries/bind/b/b;
.super Landroid/support/v4/view/cm;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Landroid/support/v4/view/cm;-><init>(Landroid/content/Context;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Landroid/support/v4/view/cm;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method


# virtual methods
.method public final i()Z
    .locals 3
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "NewApi"
        }
    .end annotation

    .prologue
    const/4 v0, 0x1

    .line 28
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/b/b;->getLayoutDirection()I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onRtlPropertiesChanged(I)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    .line 56
    invoke-super {p0, p1}, Landroid/support/v4/view/cm;->onRtlPropertiesChanged(I)V

    .line 57
    iget-object v0, p0, Landroid/support/v4/view/ViewPager;->a:Landroid/support/v4/view/at;

    iget v1, p0, Landroid/support/v4/view/ViewPager;->b:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/b/c;->a(Landroid/support/v4/view/at;I)I

    move-result v0

    .line 58
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->a:Landroid/support/v4/view/at;

    instance-of v1, v1, Lcom/google/android/libraries/bind/b/a;

    if-eqz v1, :cond_0

    .line 59
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->a:Landroid/support/v4/view/at;

    .line 63
    :cond_0
    iget-object v1, p0, Landroid/support/v4/view/ViewPager;->a:Landroid/support/v4/view/at;

    invoke-static {v1, v0}, Lcom/google/android/libraries/bind/b/c;->b(Landroid/support/v4/view/at;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/b/b;->a(I)V

    .line 64
    return-void
.end method
