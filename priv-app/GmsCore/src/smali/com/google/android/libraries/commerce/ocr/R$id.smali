.class public final Lcom/google/android/libraries/commerce/ocr/R$id;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final adjust_height:I = 0x7f0f008b

.field public static final adjust_width:I = 0x7f0f008c

.field public static final book_now:I = 0x7f0f00b3

.field public static final buyButton:I = 0x7f0f00b1

.field public static final buy_now:I = 0x7f0f00b4

.field public static final buy_with_google:I = 0x7f0f00b5

.field public static final classic:I = 0x7f0f00b7

.field public static final contact:I = 0x7f0f0086

.field public static final demote_common_words:I = 0x7f0f00a0

.field public static final demote_rfc822_hostnames:I = 0x7f0f00a1

.field public static final donate_with_google:I = 0x7f0f00b6

.field public static final email:I = 0x7f0f0087

.field public static final grayscale:I = 0x7f0f00b8

.field public static final holo_dark:I = 0x7f0f00ac

.field public static final holo_light:I = 0x7f0f00ad

.field public static final html:I = 0x7f0f009c

.field public static final hybrid:I = 0x7f0f008d

.field public static final icon_uri:I = 0x7f0f007d

.field public static final instant_message:I = 0x7f0f0088

.field public static final intent_action:I = 0x7f0f007e

.field public static final intent_activity:I = 0x7f0f007f

.field public static final intent_data:I = 0x7f0f0080

.field public static final intent_data_id:I = 0x7f0f0081

.field public static final intent_extra_data:I = 0x7f0f0082

.field public static final large_icon_uri:I = 0x7f0f0083

.field public static final match_global_nicknames:I = 0x7f0f00a2

.field public static final match_parent:I = 0x7f0f006b

.field public static final monochrome:I = 0x7f0f00b9

.field public static final none:I = 0x7f0f0066

.field public static final normal:I = 0x7f0f0062

.field public static final ocrBoundingBoxRoot:I = 0x7f0f03bb

.field public static final ocrButtonDivider:I = 0x7f0f03bf

.field public static final ocrCaptureFrameButton:I = 0x7f0f03c0

.field public static final ocrContainer:I = 0x7f0f03b7

.field public static final ocrHelpImageView:I = 0x7f0f03bc

.field public static final ocrMain:I = 0x7f0f03b6

.field public static final ocrMessage:I = 0x7f0f03bd

.field public static final ocrOverlay:I = 0x7f0f03b9

.field public static final ocrPreview:I = 0x7f0f03b8

.field public static final ocrPreviewButtonContainer:I = 0x7f0f03be

.field public static final ocrPreviewOverlayRoot:I = 0x7f0f03ba

.field public static final ocrProgressOverlay:I = 0x7f0f03c2

.field public static final ocrSkipScanButton:I = 0x7f0f03c1

.field public static final omnibox_title_section:I = 0x7f0f00a3

.field public static final omnibox_url_section:I = 0x7f0f00a4

.field public static final plain:I = 0x7f0f009d

.field public static final production:I = 0x7f0f00ae

.field public static final rfc822:I = 0x7f0f009e

.field public static final sandbox:I = 0x7f0f00af

.field public static final satellite:I = 0x7f0f008e

.field public static final selectionDetails:I = 0x7f0f00b2

.field public static final strict_sandbox:I = 0x7f0f00b0

.field public static final terrain:I = 0x7f0f008f

.field public static final text1:I = 0x7f0f0084

.field public static final text2:I = 0x7f0f0085

.field public static final url:I = 0x7f0f009f

.field public static final wrap_content:I = 0x7f0f006d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 160
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
