.class Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Ljava/lang/Object;

.field public final b:Landroid/support/v4/app/Fragment$SavedState;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 131
    new-instance v0, Lcom/google/android/libraries/bind/data/z;

    invoke-direct {v0}, Lcom/google/android/libraries/bind/data/z;-><init>()V

    sput-object v0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;Landroid/support/v4/app/Fragment$SavedState;)V
    .locals 0

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->a:Ljava/lang/Object;

    .line 97
    iput-object p2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->b:Landroid/support/v4/app/Fragment$SavedState;

    .line 98
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 102
    instance-of v1, p1, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    if-eqz v1, :cond_0

    .line 103
    check-cast p1, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;

    .line 104
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->a:Ljava/lang/Object;

    iget-object v2, p1, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->a:Ljava/lang/Object;

    invoke-static {v1, v2}, Lcom/google/android/libraries/bind/d/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->b:Landroid/support/v4/app/Fragment$SavedState;

    iget-object v2, p1, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->b:Landroid/support/v4/app/Fragment$SavedState;

    invoke-static {v1, v2}, Lcom/google/android/libraries/bind/d/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    .line 107
    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 112
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->a:Ljava/lang/Object;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->b:Landroid/support/v4/app/Fragment$SavedState;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/libraries/bind/d/b;->a([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 122
    const-string v0, "key: %s, fragmentState: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->a:Ljava/lang/Object;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->b:Landroid/support/v4/app/Fragment$SavedState;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->a:Ljava/lang/Object;

    invoke-static {v0, p1, p2}, Lcom/google/android/libraries/bind/d/a;->a(Ljava/lang/Object;Landroid/os/Parcel;I)V

    .line 128
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/FragmentStateDataPagerAdapter$FragmentAdapterState;->b:Landroid/support/v4/app/Fragment$SavedState;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 129
    return-void
.end method
