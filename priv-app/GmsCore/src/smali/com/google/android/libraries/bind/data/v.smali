.class public final Lcom/google/android/libraries/bind/data/v;
.super Lcom/google/android/libraries/bind/data/s;
.source "SourceFile"


# instance fields
.field i:Z

.field private final j:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/data/l;Ljava/lang/Object;Lcom/google/android/libraries/bind/data/r;[I)V
    .locals 1

    .prologue
    .line 22
    iget v0, p1, Lcom/google/android/libraries/bind/data/l;->b:I

    invoke-direct {p0, p1, p3, p4, v0}, Lcom/google/android/libraries/bind/data/s;-><init>(Lcom/google/android/libraries/bind/data/l;Lcom/google/android/libraries/bind/data/r;[II)V

    .line 27
    iput-object p2, p0, Lcom/google/android/libraries/bind/data/v;->j:Ljava/lang/Object;

    .line 28
    return-void
.end method


# virtual methods
.method protected final k()Lcom/google/android/libraries/bind/data/af;
    .locals 7

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/v;->h:Lcom/google/android/libraries/bind/data/l;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/v;->j:Ljava/lang/Object;

    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/ag;->a(Ljava/lang/Object;)I

    move-result v0

    .line 46
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/libraries/bind/data/v;->i:Z

    if-nez v1, :cond_0

    .line 48
    const/4 v0, 0x0

    .line 51
    :goto_0
    return-object v0

    .line 50
    :cond_0
    iget-object v1, p0, Lcom/google/android/libraries/bind/data/v;->h:Lcom/google/android/libraries/bind/data/l;

    invoke-virtual {v1, v0}, Lcom/google/android/libraries/bind/data/l;->a(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v6

    .line 51
    new-instance v0, Lcom/google/android/libraries/bind/data/w;

    sget-object v3, Lcom/google/android/libraries/bind/a/o;->c:Lcom/google/android/libraries/bind/a/j;

    iget-object v4, p0, Lcom/google/android/libraries/bind/data/v;->g:Lcom/google/android/libraries/bind/data/r;

    iget-object v5, p0, Lcom/google/android/libraries/bind/data/v;->h:Lcom/google/android/libraries/bind/data/l;

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/libraries/bind/data/w;-><init>(Lcom/google/android/libraries/bind/data/v;Lcom/google/android/libraries/bind/data/l;Ljava/util/concurrent/Executor;Lcom/google/android/libraries/bind/data/r;Lcom/google/android/libraries/bind/data/l;Lcom/google/android/libraries/bind/data/Data;)V

    goto :goto_0
.end method
