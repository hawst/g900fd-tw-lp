.class public Lcom/google/android/libraries/bind/widget/BoundImageView;
.super Landroid/widget/ImageView;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/libraries/bind/data/f;


# instance fields
.field private final a:Lcom/google/android/libraries/bind/data/g;

.field private b:Ljava/lang/Integer;

.field private c:Ljava/lang/Integer;

.field private d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/libraries/bind/widget/BoundImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 29
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/widget/BoundImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 37
    new-instance v0, Lcom/google/android/libraries/bind/data/g;

    invoke-direct {v0, p1, p2, p0}, Lcom/google/android/libraries/bind/data/g;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Landroid/view/View;)V

    iput-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->a:Lcom/google/android/libraries/bind/data/g;

    .line 38
    sget-object v0, Lcom/google/android/libraries/bind/e;->f:[I

    const/4 v1, 0x0

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 40
    sget v1, Lcom/google/android/libraries/bind/e;->h:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/g;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->b:Ljava/lang/Integer;

    .line 41
    sget v1, Lcom/google/android/libraries/bind/e;->g:I

    invoke-static {v0, v1}, Lcom/google/android/libraries/bind/data/g;->a(Landroid/content/res/TypedArray;I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->c:Ljava/lang/Integer;

    .line 43
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 44
    return-void
.end method


# virtual methods
.method public final a_(Lcom/google/android/libraries/bind/data/Data;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 48
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->a:Lcom/google/android/libraries/bind/data/g;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/g;->a(Lcom/google/android/libraries/bind/data/Data;)V

    .line 49
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 50
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    .line 51
    instance-of v2, v0, Landroid/net/Uri;

    if-eqz v2, :cond_2

    .line 52
    check-cast v0, Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/libraries/bind/widget/BoundImageView;->setImageURI(Landroid/net/Uri;)V

    .line 58
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 59
    if-nez p1, :cond_3

    move-object v0, v1

    .line 60
    :goto_1
    iget-object v2, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->d:Ljava/lang/Integer;

    invoke-static {v2, v0}, Lcom/google/android/libraries/bind/d/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 61
    iput-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->d:Ljava/lang/Integer;

    .line 62
    if-nez v0, :cond_4

    .line 64
    :goto_2
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/widget/BoundImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 67
    :cond_1
    return-void

    .line 54
    :cond_2
    invoke-virtual {p0, v1}, Lcom/google/android/libraries/bind/widget/BoundImageView;->setImageURI(Landroid/net/Uri;)V

    goto :goto_0

    .line 59
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/bind/widget/BoundImageView;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/libraries/bind/data/Data;->d(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    .line 62
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/widget/BoundImageView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    goto :goto_2
.end method
