.class public abstract Lcom/google/android/libraries/bind/data/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Ljava/util/concurrent/Executor;

.field private b:Z

.field protected final g:Lcom/google/android/libraries/bind/data/ag;

.field protected final h:Lcom/google/android/libraries/bind/data/l;

.field final i:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method public constructor <init>(Lcom/google/android/libraries/bind/data/l;Ljava/util/concurrent/Executor;)V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>()V

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/af;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 21
    iput-object p1, p0, Lcom/google/android/libraries/bind/data/af;->h:Lcom/google/android/libraries/bind/data/l;

    .line 22
    iput-object p2, p0, Lcom/google/android/libraries/bind/data/af;->a:Ljava/util/concurrent/Executor;

    .line 23
    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    iget-object v0, p1, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    iput-object v0, p0, Lcom/google/android/libraries/bind/data/af;->g:Lcom/google/android/libraries/bind/data/ag;

    .line 24
    return-void
.end method


# virtual methods
.method protected a()V
    .locals 0

    .prologue
    .line 38
    return-void
.end method

.method protected a(Lcom/google/android/libraries/bind/data/ag;Lcom/google/android/libraries/bind/data/j;)V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/af;->h:Lcom/google/android/libraries/bind/data/l;

    const/4 v1, 0x0

    invoke-virtual {v0, p0, p1, p2, v1}, Lcom/google/android/libraries/bind/data/l;->a(Lcom/google/android/libraries/bind/data/af;Lcom/google/android/libraries/bind/data/ag;Lcom/google/android/libraries/bind/data/j;Ljava/lang/Integer;)V

    .line 79
    return-void
.end method

.method protected abstract c()Ljava/util/List;
.end method

.method public final d()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 27
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/af;->b:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/libraries/bind/d/b;->a(Z)V

    .line 28
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/af;->a()V

    .line 29
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/af;->a:Ljava/util/concurrent/Executor;

    invoke-interface {v0, p0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 30
    iput-boolean v1, p0, Lcom/google/android/libraries/bind/data/af;->b:Z

    .line 31
    return-void

    .line 27
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/af;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 43
    return-void
.end method

.method public run()V
    .locals 13

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 51
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/af;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    :cond_0
    :goto_0
    return-void

    .line 54
    :cond_1
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/af;->c()Ljava/util/List;

    move-result-object v6

    .line 58
    if-eqz v6, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/af;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    const/4 v0, 0x1

    new-array v7, v0, [Lcom/google/android/libraries/bind/data/j;

    .line 62
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/af;->g:Lcom/google/android/libraries/bind/data/ag;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ag;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/libraries/bind/data/j;->b:Lcom/google/android/libraries/bind/data/j;

    aput-object v1, v7, v0

    move v0, v2

    :goto_1
    if-eqz v0, :cond_0

    .line 65
    new-instance v1, Lcom/google/android/libraries/bind/data/ag;

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/af;->h:Lcom/google/android/libraries/bind/data/l;

    iget v0, v0, Lcom/google/android/libraries/bind/data/l;->b:I

    invoke-direct {v1, v0, v6}, Lcom/google/android/libraries/bind/data/ag;-><init>(ILjava/util/List;)V

    .line 66
    const/4 v0, 0x0

    aget-object v0, v7, v0
    :try_end_0
    .catch Lcom/google/android/libraries/bind/data/k; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_2
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/af;->i:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_0

    .line 74
    invoke-virtual {p0, v1, v0}, Lcom/google/android/libraries/bind/data/af;->a(Lcom/google/android/libraries/bind/data/ag;Lcom/google/android/libraries/bind/data/j;)V

    goto :goto_0

    .line 62
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/af;->g:Lcom/google/android/libraries/bind/data/ag;

    iget-object v8, v0, Lcom/google/android/libraries/bind/data/ag;->a:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v1

    if-eq v0, v1, :cond_3

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/libraries/bind/data/j;->b:Lcom/google/android/libraries/bind/data/j;

    aput-object v1, v7, v0

    move v0, v2

    goto :goto_1

    :cond_3
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v9

    move v1, v3

    :goto_3
    if-ge v1, v9, :cond_5

    invoke-interface {v8, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    iget-object v4, p0, Lcom/google/android/libraries/bind/data/af;->g:Lcom/google/android/libraries/bind/data/ag;

    iget v4, v4, Lcom/google/android/libraries/bind/data/ag;->b:I

    invoke-virtual {v0, v4}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v6, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    iget-object v5, p0, Lcom/google/android/libraries/bind/data/af;->h:Lcom/google/android/libraries/bind/data/l;

    iget v5, v5, Lcom/google/android/libraries/bind/data/l;->b:I

    invoke-virtual {v0, v5}, Lcom/google/android/libraries/bind/data/Data;->b(I)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/libraries/bind/d/b;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/libraries/bind/data/j;->b:Lcom/google/android/libraries/bind/data/j;

    aput-object v1, v7, v0

    move v0, v2

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    :cond_5
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/libraries/bind/data/j;->c:Lcom/google/android/libraries/bind/data/j;

    aput-object v1, v7, v0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/af;->h:Lcom/google/android/libraries/bind/data/l;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/l;->g()[I

    move-result-object v10

    move v5, v3

    :goto_4
    if-ge v5, v9, :cond_a

    invoke-interface {v8, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/Data;

    invoke-interface {v6, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/libraries/bind/data/Data;

    if-nez v10, :cond_6

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/Data;->equals(Ljava/lang/Object;)Z

    move-result v0

    :goto_5
    if-nez v0, :cond_9

    move v0, v2

    goto/16 :goto_1

    :cond_6
    array-length v11, v10

    move v4, v3

    :goto_6
    if-ge v4, v11, :cond_8

    aget v12, v10, v4

    invoke-virtual {v0, v1, v12}, Lcom/google/android/libraries/bind/data/Data;->a(Lcom/google/android/libraries/bind/data/Data;I)Z
    :try_end_1
    .catch Lcom/google/android/libraries/bind/data/k; {:try_start_1 .. :try_end_1} :catch_0

    move-result v12

    if-nez v12, :cond_7

    move v0, v3

    goto :goto_5

    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    :cond_8
    move v0, v2

    goto :goto_5

    :cond_9
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_4

    :cond_a
    move v0, v3

    goto/16 :goto_1

    .line 67
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 68
    new-instance v1, Lcom/google/android/libraries/bind/data/ag;

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/af;->h:Lcom/google/android/libraries/bind/data/l;

    iget v0, v0, Lcom/google/android/libraries/bind/data/l;->b:I

    invoke-direct {v1, v0, v2}, Lcom/google/android/libraries/bind/data/ag;-><init>(ILcom/google/android/libraries/bind/data/k;)V

    .line 69
    new-instance v0, Lcom/google/android/libraries/bind/data/j;

    invoke-direct {v0, v2}, Lcom/google/android/libraries/bind/data/j;-><init>(Lcom/google/android/libraries/bind/data/k;)V

    goto/16 :goto_2
.end method
