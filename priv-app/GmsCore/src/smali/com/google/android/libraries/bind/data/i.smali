.class public abstract Lcom/google/android/libraries/bind/data/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/widget/ListAdapter;
.implements Landroid/widget/SpinnerAdapter;


# instance fields
.field protected final a:Lcom/google/android/libraries/bind/e/a;

.field protected b:Lcom/google/android/libraries/bind/data/l;

.field protected c:Lcom/google/android/libraries/bind/data/ah;

.field protected d:Lcom/google/android/libraries/bind/data/ah;

.field protected e:Lcom/google/android/libraries/bind/data/ah;

.field private final f:Lcom/google/android/libraries/bind/data/n;

.field private final g:Lcom/google/android/libraries/bind/data/ad;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z


# direct methods
.method private static a(Landroid/view/ViewGroup;)Landroid/view/ViewGroup$LayoutParams;
    .locals 4

    .prologue
    .line 317
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingLeft()I

    move-result v1

    sub-int/2addr v0, v1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingRight()I

    move-result v1

    sub-int/2addr v0, v1

    .line 318
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/libraries/bind/b;->a:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    mul-int/lit8 v2, v2, 0x2

    sub-int/2addr v1, v2

    .line 320
    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v2, v0, v1}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    return-object v2
.end method

.method private b()V
    .locals 2

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->g:Lcom/google/android/libraries/bind/data/ad;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ad;->a()I

    move-result v0

    if-nez v0, :cond_2

    .line 112
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/i;->h:Z

    if-eqz v0, :cond_1

    .line 113
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/i;->f:Lcom/google/android/libraries/bind/data/n;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/l;->b(Lcom/google/android/libraries/bind/data/n;)V

    .line 114
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/i;->h:Z

    .line 123
    :cond_1
    :goto_0
    return-void

    .line 118
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/i;->h:Z

    if-nez v0, :cond_1

    .line 119
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/i;->f:Lcom/google/android/libraries/bind/data/n;

    invoke-virtual {v0, v1}, Lcom/google/android/libraries/bind/data/l;->a(Lcom/google/android/libraries/bind/data/n;)V

    .line 120
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/libraries/bind/data/i;->h:Z

    goto :goto_0
.end method

.method private c()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 172
    iget-boolean v2, p0, Lcom/google/android/libraries/bind/data/i;->i:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/libraries/bind/data/i;->a()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->e()Z

    move-result v2

    if-nez v2, :cond_1

    move v2, v0

    :goto_0
    if-eqz v2, :cond_2

    :goto_1
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private d()Z
    .locals 1

    .prologue
    .line 176
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/i;->j:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/l;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->c()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->e()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    .prologue
    .line 181
    iget-boolean v0, p0, Lcom/google/android/libraries/bind/data/i;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/ag;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 1

    .prologue
    .line 189
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->c()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a(ILandroid/view/View;Lcom/google/android/libraries/bind/data/Data;)Landroid/view/View;
.end method

.method public final a(Landroid/database/DataSetObserver;I)V
    .locals 3

    .prologue
    .line 256
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->g:Lcom/google/android/libraries/bind/data/ad;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Observer is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, v0, Lcom/google/android/libraries/bind/data/ad;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    new-instance v1, Lcom/google/android/libraries/bind/data/ae;

    invoke-direct {v1, p1, p2}, Lcom/google/android/libraries/bind/data/ae;-><init>(Landroid/database/DataSetObserver;I)V

    iget-object v2, v0, Lcom/google/android/libraries/bind/data/ad;->a:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/ad;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 257
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->b()V

    .line 258
    return-void
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/l;->h()Z

    move-result v0

    goto :goto_0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 273
    const/4 v0, 0x1

    return v0
.end method

.method public final c(I)Lcom/google/android/libraries/bind/data/Data;
    .locals 1

    .prologue
    .line 209
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/l;->a(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    goto :goto_0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 204
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/l;->b()I

    move-result v0

    goto :goto_0
.end method

.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    .prologue
    .line 238
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/libraries/bind/data/i;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/google/android/libraries/bind/data/i;->c(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 9

    .prologue
    const-wide/16 v2, 0x0

    .line 214
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 215
    const-wide v0, 0x7fffffffffffffffL

    .line 223
    :cond_0
    :goto_0
    return-wide v0

    .line 217
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 218
    const-wide v0, 0x7ffffffffffffffeL

    goto :goto_0

    .line 220
    :cond_2
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 221
    const-wide v0, 0x7ffffffffffffffdL

    goto :goto_0

    .line 223
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/l;->b(I)Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_4

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    goto :goto_0

    :cond_4
    instance-of v1, v0, Ljava/lang/Integer;

    if-eqz v1, :cond_5

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0

    :cond_5
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    const/4 v0, 0x0

    move v8, v0

    move-wide v0, v2

    move v2, v8

    :goto_1
    if-ge v2, v7, :cond_0

    const-wide/16 v4, 0x3f

    mul-long/2addr v0, v4

    invoke-virtual {v6, v2}, Ljava/lang/String;->charAt(I)C

    move-result v3

    int-to-long v4, v3

    add-long/2addr v4, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move-wide v0, v4

    goto :goto_1

    :cond_6
    move-wide v0, v2

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    .prologue
    .line 228
    const/4 v0, 0x0

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    .prologue
    .line 283
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 284
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->c:Lcom/google/android/libraries/bind/data/ah;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/i;->a:Lcom/google/android/libraries/bind/e/a;

    invoke-interface {v0, p3}, Lcom/google/android/libraries/bind/data/ah;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-static {p3}, Lcom/google/android/libraries/bind/data/i;->a(Landroid/view/ViewGroup;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 297
    :cond_0
    :goto_0
    return-object v0

    .line 286
    :cond_1
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 287
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->d:Lcom/google/android/libraries/bind/data/ah;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/i;->a:Lcom/google/android/libraries/bind/e/a;

    invoke-interface {v0, p3}, Lcom/google/android/libraries/bind/data/ah;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-static {p3}, Lcom/google/android/libraries/bind/data/i;->a(Landroid/view/ViewGroup;)Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0

    .line 289
    :cond_2
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 290
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->e:Lcom/google/android/libraries/bind/data/ah;

    iget-object v1, p0, Lcom/google/android/libraries/bind/data/i;->a:Lcom/google/android/libraries/bind/e/a;

    invoke-interface {v0, p3}, Lcom/google/android/libraries/bind/data/ah;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    .line 292
    :cond_3
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    invoke-static {}, Lcom/google/android/libraries/bind/a/a;->a()V

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/l;->e:Lcom/google/android/libraries/bind/data/ag;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/ag;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 293
    const/4 v0, 0x0

    goto :goto_0

    .line 295
    :cond_4
    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    invoke-virtual {v0, p1}, Lcom/google/android/libraries/bind/data/l;->a(I)Lcom/google/android/libraries/bind/data/Data;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/libraries/bind/data/i;->a(ILandroid/view/View;Lcom/google/android/libraries/bind/data/Data;)Landroid/view/View;

    move-result-object v0

    .line 296
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    if-eqz v1, :cond_0

    instance-of v2, v1, Landroid/widget/AbsListView$LayoutParams;

    if-nez v2, :cond_0

    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    invoke-direct {v2, v1}, Landroid/widget/AbsListView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 233
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x1

    return v0
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 248
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->f()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/libraries/bind/data/i;->b:Lcom/google/android/libraries/bind/data/l;

    invoke-virtual {v0}, Lcom/google/android/libraries/bind/data/l;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    .prologue
    .line 278
    const/4 v0, 0x1

    return v0
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/libraries/bind/data/i;->a(Landroid/database/DataSetObserver;I)V

    .line 263
    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 3

    .prologue
    .line 267
    iget-object v2, p0, Lcom/google/android/libraries/bind/data/i;->g:Lcom/google/android/libraries/bind/data/ad;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Observer is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, v2, Lcom/google/android/libraries/bind/data/ad;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, v2, Lcom/google/android/libraries/bind/data/ad;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, v2, Lcom/google/android/libraries/bind/data/ad;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/libraries/bind/data/ae;

    iget-object v0, v0, Lcom/google/android/libraries/bind/data/ae;->a:Landroid/database/DataSetObserver;

    if-ne v0, p1, :cond_3

    iget-object v0, v2, Lcom/google/android/libraries/bind/data/ad;->a:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_1
    iget-object v0, v2, Lcom/google/android/libraries/bind/data/ad;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    .line 268
    :cond_2
    invoke-direct {p0}, Lcom/google/android/libraries/bind/data/i;->b()V

    .line 269
    return-void

    .line 267
    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method
