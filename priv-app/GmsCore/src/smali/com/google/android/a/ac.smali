.class final Lcom/google/android/a/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/a/z;


# instance fields
.field private a:Lcom/google/protobuf/nano/b;

.field private b:[B

.field private final c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    const/16 v0, 0xef

    iput v0, p0, Lcom/google/android/a/ac;->c:I

    .line 24
    invoke-virtual {p0}, Lcom/google/android/a/ac;->a()V

    .line 25
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 29
    iget v0, p0, Lcom/google/android/a/ac;->c:I

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/a/ac;->b:[B

    .line 30
    iget-object v0, p0, Lcom/google/android/a/ac;->b:[B

    invoke-static {v0}, Lcom/google/protobuf/nano/b;->a([B)Lcom/google/protobuf/nano/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/ac;->a:Lcom/google/protobuf/nano/b;

    .line 31
    return-void
.end method

.method public final a(IJ)V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/a/ac;->a:Lcom/google/protobuf/nano/b;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 36
    return-void
.end method

.method public final a(ILjava/lang/String;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/a/ac;->a:Lcom/google/protobuf/nano/b;

    invoke-virtual {v0, p1, p2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 41
    return-void
.end method

.method public final b()[B
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 45
    iget-object v0, p0, Lcom/google/android/a/ac;->a:Lcom/google/protobuf/nano/b;

    invoke-virtual {v0}, Lcom/google/protobuf/nano/b;->a()I

    move-result v0

    .line 46
    if-gez v0, :cond_0

    .line 47
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0

    .line 48
    :cond_0
    if-nez v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/google/android/a/ac;->b:[B

    .line 54
    :goto_0
    return-object v0

    .line 52
    :cond_1
    iget-object v1, p0, Lcom/google/android/a/ac;->b:[B

    array-length v1, v1

    sub-int v0, v1, v0

    new-array v0, v0, [B

    .line 53
    iget-object v1, p0, Lcom/google/android/a/ac;->b:[B

    array-length v2, v0

    invoke-static {v1, v3, v0, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method
