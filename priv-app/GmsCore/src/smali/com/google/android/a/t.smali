.class public Lcom/google/android/a/t;
.super Lcom/google/android/a/r;
.source "SourceFile"


# static fields
.field private static e:Lcom/google/android/gms/ads/identifier/a;

.field private static f:Ljava/util/concurrent/CountDownLatch;

.field private static g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 50
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/a/t;->e:Lcom/google/android/gms/ads/identifier/a;

    .line 51
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    sput-object v0, Lcom/google/android/a/t;->f:Ljava/util/concurrent/CountDownLatch;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/a/y;Lcom/google/android/a/z;)V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/a/r;-><init>(Landroid/content/Context;Lcom/google/android/a/y;Lcom/google/android/a/z;)V

    .line 125
    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/content/Context;)Lcom/google/android/a/t;
    .locals 4

    .prologue
    .line 107
    new-instance v0, Lcom/google/android/a/a;

    invoke-direct {v0}, Lcom/google/android/a/a;-><init>()V

    .line 109
    invoke-static {p0, p1, v0}, Lcom/google/android/a/t;->a(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/a/y;)V

    .line 111
    const-class v1, Lcom/google/android/a/t;

    monitor-enter v1

    .line 112
    :try_start_0
    sget-object v2, Lcom/google/android/a/t;->e:Lcom/google/android/gms/ads/identifier/a;

    if-nez v2, :cond_0

    .line 113
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/google/android/a/v;

    invoke-direct {v3, p1}, Lcom/google/android/a/v;-><init>(Landroid/content/Context;)V

    invoke-direct {v2, v3}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V

    .line 115
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 117
    new-instance v1, Lcom/google/android/a/t;

    new-instance v2, Lcom/google/android/a/ac;

    invoke-direct {v2}, Lcom/google/android/a/ac;-><init>()V

    invoke-direct {v1, p1, v0, v2}, Lcom/google/android/a/t;-><init>(Landroid/content/Context;Lcom/google/android/a/y;Lcom/google/android/a/z;)V

    return-object v1

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/ads/identifier/a;)Lcom/google/android/gms/ads/identifier/a;
    .locals 0

    .prologue
    .line 25
    sput-object p0, Lcom/google/android/a/t;->e:Lcom/google/android/gms/ads/identifier/a;

    return-object p0
.end method

.method static synthetic b()Lcom/google/android/gms/ads/identifier/a;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/a/t;->e:Lcom/google/android/gms/ads/identifier/a;

    return-object v0
.end method

.method static synthetic c()Z
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/a/t;->g:Z

    return v0
.end method

.method static synthetic d()Ljava/util/concurrent/CountDownLatch;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/google/android/a/t;->f:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method

.method private e()Lcom/google/android/a/u;
    .locals 8

    .prologue
    const/16 v7, 0x10

    const/4 v0, 0x0

    .line 133
    const-class v1, Lcom/google/android/a/t;

    monitor-enter v1

    .line 137
    :try_start_0
    sget-object v2, Lcom/google/android/a/t;->f:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v4, 0x2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, v4, v5, v3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v2

    .line 139
    if-nez v2, :cond_0

    .line 142
    new-instance v0, Lcom/google/android/a/u;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/a/u;-><init>(Lcom/google/android/a/t;Ljava/lang/String;Z)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    monitor-exit v1

    .line 177
    :goto_0
    return-object v0

    .line 145
    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/a/u;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/a/u;-><init>(Lcom/google/android/a/t;Ljava/lang/String;Z)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 154
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 148
    :cond_0
    :try_start_2
    sget-object v2, Lcom/google/android/a/t;->e:Lcom/google/android/gms/ads/identifier/a;

    if-nez v2, :cond_1

    .line 150
    new-instance v0, Lcom/google/android/a/u;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, p0, v2, v3}, Lcom/google/android/a/u;-><init>(Lcom/google/android/a/t;Ljava/lang/String;Z)V

    monitor-exit v1

    goto :goto_0

    .line 153
    :cond_1
    sget-object v2, Lcom/google/android/a/t;->e:Lcom/google/android/gms/ads/identifier/a;

    invoke-virtual {v2}, Lcom/google/android/gms/ads/identifier/a;->b()Lcom/google/android/gms/ads/identifier/c;

    move-result-object v3

    .line 154
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 156
    invoke-virtual {v3}, Lcom/google/android/gms/ads/identifier/c;->a()Ljava/lang/String;

    move-result-object v2

    .line 157
    if-eqz v2, :cond_5

    const-string v1, "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$"

    invoke-virtual {v2, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 160
    new-array v4, v7, [B

    move v1, v0

    .line 163
    :goto_1
    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v5

    if-ge v0, v5, :cond_4

    .line 164
    const/16 v5, 0x8

    if-eq v0, v5, :cond_2

    const/16 v5, 0xd

    if-eq v0, v5, :cond_2

    const/16 v5, 0x12

    if-eq v0, v5, :cond_2

    const/16 v5, 0x17

    if-ne v0, v5, :cond_3

    .line 166
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 169
    :cond_3
    invoke-virtual {v2, v0}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5, v7}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    shl-int/lit8 v5, v5, 0x4

    add-int/lit8 v6, v0, 0x1

    .line 170
    invoke-virtual {v2, v6}, Ljava/lang/String;->charAt(I)C

    move-result v6

    invoke-static {v6, v7}, Ljava/lang/Character;->digit(CI)I

    move-result v6

    add-int/2addr v5, v6

    int-to-byte v5, v5

    aput-byte v5, v4, v1

    .line 171
    add-int/lit8 v1, v1, 0x1

    .line 172
    add-int/lit8 v0, v0, 0x2

    goto :goto_1

    .line 174
    :cond_4
    iget-object v0, p0, Lcom/google/android/a/t;->c:Lcom/google/android/a/y;

    invoke-interface {v0, v4}, Lcom/google/android/a/y;->a([B)Ljava/lang/String;

    move-result-object v0

    .line 177
    :goto_2
    new-instance v1, Lcom/google/android/a/u;

    invoke-virtual {v3}, Lcom/google/android/gms/ads/identifier/c;->b()Z

    move-result v2

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/a/u;-><init>(Lcom/google/android/a/t;Ljava/lang/String;Z)V

    move-object v0, v1

    goto :goto_0

    :cond_5
    move-object v0, v2

    goto :goto_2
.end method


# virtual methods
.method protected final b(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 182
    invoke-super {p0, p1}, Lcom/google/android/a/r;->b(Landroid/content/Context;)V

    .line 189
    :try_start_0
    sget-boolean v0, Lcom/google/android/a/t;->g:Z

    if-eqz v0, :cond_1

    .line 198
    const/16 v0, 0x18

    invoke-static {p1}, Lcom/google/android/a/t;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/a/t;->a(ILjava/lang/String;)V

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 203
    :cond_1
    invoke-direct {p0}, Lcom/google/android/a/t;->e()Lcom/google/android/a/u;

    move-result-object v2

    .line 205
    const/16 v3, 0x1c

    iget-boolean v0, v2, Lcom/google/android/a/u;->b:Z

    if-eqz v0, :cond_2

    const-wide/16 v0, 0x1

    :goto_1
    invoke-virtual {p0, v3, v0, v1}, Lcom/google/android/a/t;->a(IJ)V

    .line 207
    iget-object v0, v2, Lcom/google/android/a/u;->a:Ljava/lang/String;

    .line 208
    if-eqz v0, :cond_0

    .line 209
    const/16 v1, 0x1a

    const-wide/16 v2, 0x5

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/a/t;->a(IJ)V

    .line 210
    const/16 v1, 0x18

    invoke-virtual {p0, v1, v0}, Lcom/google/android/a/t;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/a/s; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 216
    :catch_0
    move-exception v0

    goto :goto_0

    .line 205
    :cond_2
    const-wide/16 v0, 0x0

    goto :goto_1

    .line 219
    :catch_1
    move-exception v0

    goto :goto_0
.end method
