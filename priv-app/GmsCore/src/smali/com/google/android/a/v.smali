.class final Lcom/google/android/a/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/a/v;->a:Landroid/content/Context;

    .line 63
    iget-object v0, p0, Lcom/google/android/a/v;->a:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 64
    iput-object p1, p0, Lcom/google/android/a/v;->a:Landroid/content/Context;

    .line 66
    :cond_0
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 71
    :try_start_0
    new-instance v0, Lcom/google/android/gms/ads/identifier/a;

    iget-object v1, p0, Lcom/google/android/a/v;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/ads/identifier/a;-><init>(Landroid/content/Context;)V

    .line 72
    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/a;->a()V

    .line 73
    const-class v1, Lcom/google/android/a/t;

    monitor-enter v1
    :try_end_0
    .catch Lcom/google/android/gms/common/eu; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/gms/common/ev; {:try_start_0 .. :try_end_0} :catch_1

    .line 74
    :try_start_1
    invoke-static {}, Lcom/google/android/a/t;->b()Lcom/google/android/gms/ads/identifier/a;

    move-result-object v2

    if-nez v2, :cond_0

    .line 76
    invoke-static {v0}, Lcom/google/android/a/t;->a(Lcom/google/android/gms/ads/identifier/a;)Lcom/google/android/gms/ads/identifier/a;

    .line 81
    :goto_0
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 89
    :goto_1
    invoke-static {}, Lcom/google/android/a/t;->d()Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 90
    return-void

    .line 79
    :cond_0
    :try_start_2
    invoke-virtual {v0}, Lcom/google/android/gms/ads/identifier/a;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 81
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catch Lcom/google/android/gms/common/eu; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Lcom/google/android/gms/common/ev; {:try_start_3 .. :try_end_3} :catch_1

    .line 83
    :catch_0
    move-exception v0

    invoke-static {}, Lcom/google/android/a/t;->c()Z

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_1

    .line 88
    :catch_2
    move-exception v0

    goto :goto_1
.end method
