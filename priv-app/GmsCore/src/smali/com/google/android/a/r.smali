.class public abstract Lcom/google/android/a/r;
.super Lcom/google/android/a/q;
.source "SourceFile"


# static fields
.field static d:Z

.field private static e:Ljava/lang/reflect/Method;

.field private static f:Ljava/lang/reflect/Method;

.field private static g:Ljava/lang/reflect/Method;

.field private static h:Ljava/lang/reflect/Method;

.field private static i:Ljava/lang/reflect/Method;

.field private static j:Ljava/lang/reflect/Method;

.field private static k:Ljava/lang/reflect/Method;

.field private static l:Ljava/lang/reflect/Method;

.field private static m:Ljava/lang/reflect/Method;

.field private static n:Ljava/lang/String;

.field private static o:Ljava/lang/String;

.field private static p:Ljava/lang/String;

.field private static q:J

.field private static r:Lcom/google/android/a/aa;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 55
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/google/android/a/r;->q:J

    .line 60
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/a/r;->d:Z

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Lcom/google/android/a/y;Lcom/google/android/a/z;)V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/a/q;-><init>(Landroid/content/Context;Lcom/google/android/a/y;Lcom/google/android/a/z;)V

    .line 115
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/a/y;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 306
    sget-object v0, Lcom/google/android/a/r;->o:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 307
    sget-object v0, Lcom/google/android/a/r;->o:Ljava/lang/String;

    .line 322
    :goto_0
    return-object v0

    .line 310
    :cond_0
    sget-object v0, Lcom/google/android/a/r;->h:Ljava/lang/reflect/Method;

    if-nez v0, :cond_1

    .line 311
    new-instance v0, Lcom/google/android/a/s;

    invoke-direct {v0}, Lcom/google/android/a/s;-><init>()V

    throw v0

    .line 315
    :cond_1
    :try_start_0
    sget-object v0, Lcom/google/android/a/r;->h:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    .line 316
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 317
    if-nez v0, :cond_2

    .line 318
    new-instance v0, Lcom/google/android/a/s;

    invoke-direct {v0}, Lcom/google/android/a/s;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 323
    :catch_0
    move-exception v0

    .line 324
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 321
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/a/y;->a([B)Ljava/lang/String;

    move-result-object v0

    .line 322
    sput-object v0, Lcom/google/android/a/r;->o:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 325
    :catch_1
    move-exception v0

    .line 326
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a([BLjava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 416
    :try_start_0
    new-instance v0, Ljava/lang/String;

    sget-object v1, Lcom/google/android/a/r;->r:Lcom/google/android/a/aa;

    .line 417
    invoke-virtual {v1, p0, p1}, Lcom/google/android/a/aa;->a([BLjava/lang/String;)[B

    move-result-object v1

    const-string v2, "UTF-8"

    invoke-direct {v0, v1, v2}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/a/ab; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 419
    :catch_0
    move-exception v0

    .line 420
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 421
    :catch_1
    move-exception v0

    .line 422
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static a(Landroid/view/MotionEvent;Landroid/util/DisplayMetrics;)Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 278
    sget-object v0, Lcom/google/android/a/r;->i:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    if-nez p0, :cond_1

    .line 280
    :cond_0
    new-instance v0, Lcom/google/android/a/s;

    invoke-direct {v0}, Lcom/google/android/a/s;-><init>()V

    throw v0

    .line 285
    :cond_1
    :try_start_0
    sget-object v0, Lcom/google/android/a/r;->i:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    .line 286
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 287
    return-object v0

    .line 288
    :catch_0
    move-exception v0

    .line 289
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 290
    :catch_1
    move-exception v0

    .line 291
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected static declared-synchronized a(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/a/y;)V
    .locals 17

    .prologue
    .line 88
    const-class v4, Lcom/google/android/a/r;

    monitor-enter v4

    :try_start_0
    sget-boolean v2, Lcom/google/android/a/r;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v2, :cond_0

    .line 91
    :try_start_1
    new-instance v2, Lcom/google/android/a/aa;

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Lcom/google/android/a/aa;-><init>(Lcom/google/android/a/y;)V

    sput-object v2, Lcom/google/android/a/r;->r:Lcom/google/android/a/aa;

    .line 94
    sput-object p0, Lcom/google/android/a/r;->n:Ljava/lang/String;
    :try_end_1
    .catch Lcom/google/android/a/s; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 97
    :try_start_2
    sget-object v2, Lcom/google/android/a/r;->r:Lcom/google/android/a/aa;

    const-string v3, "tFlGbwnjUkYv5d+C74Vc55azS9XqVBYSunrZpzmWQOk="

    invoke-virtual {v2, v3}, Lcom/google/android/a/aa;->a(Ljava/lang/String;)[B

    move-result-object v5

    sget-object v2, Lcom/google/android/a/r;->r:Lcom/google/android/a/aa;

    const-string v3, "oFgNQbbMOiYS0Wtkp2p3nb1NN9ZefwPNdItxXjI3+fI/NRuInwhhRVbAA2g1srkJYfDrn6UTtNReNpim9lE+pdS3ip1gCC4tZZrpMWNL/uqMMyv0Xnr9IrDCnl9orA0aVHtOWMxPS7wJdAm3dE7H+/jRCnXUYQnu11qsr2B/Avc/D+NHvYck2MuwLFAFACCdi2a+b0PDGyxBPOi7v0/GUILmQtMUoHNn9wBXqkaqN086fQySpMcc/K56UkfmPTCJTbutgnTFWqWL9SO16wwduWOK4xVqnIDiGO0s2G5WabmTtb8zxN5sUiI7XPFrgiUMLcRIPIPzH4RQOCS3saQUH42hPNxBPANshUskZERvwp2LpAMNZDzqTSFyEm0NGwAavNqjxpYk+h1YazGtTLvT96XaV2nBSAOmfFcisoSUpO+zAB7zJ4Lt9LERM2X4/ekohopUzV8yKhi1tyPkTigKlfu4xmIJEYAIiGimTAShdHevaMfSfQVzihOo1ZlCbRqchVbF+vBE7y3DMJj1a1rDhSBp3SDR6TmCxDq4VZ8N/6zoGeRJ7hr8o2L5wWVYOj+lBaExa+AIjIzYvXY/I2BC/wIeTJuwvSXkalFk3IwrkfZxPAe7e8+0eHjsWSVZbjzBpCiXW0McQq51e93f8KElQHZQQdUwLdrndhltdTmFp9HzaRDUlGV46dyKjHOIWnMdTX8Z4WoxW4ioeawA2CAooURuit9Lqp8KGffDBWHgX1X9/q4o/qFwZZqzNJ2TwQ9/MfRMUQdW7C2/A8yed7mPS4ChVebsD7dEh+IQY/y2Gqt4UW68lrwyvOlr6k/FdNfNwuDMjTz9QAIcs12JjdiBQNzTyHATspCnfA0V/sJVLu4TeJBGNgKR9wCPqljNNLTI3bJ+K0GusR6ulPWaAnHQUJLaDwcL5Dur9EsfrvU/dyvAklNQLDWxOr90V5YzhNOxrjXbgAfzmW3PrdVbsf//ZplNN75ReFpsjCoZsGw7FiX/dM+BglKr8EEbtyU6HojhnA8brtLIJPc5vEZK+KU7xYE+KwWvWNLmpQefoRqTZo35apnhA/q4lWyg7VoFmbrUazT5PBgi458x7XLCbV7VxfZBzM8aPvSVB2/EwIv40RzzdvVRd9OJeQEtP9pxUcsbRINa8u+DmpixYAEkaWEut2NZ3OpX2k36FaBGANhEtwuwyivR6o/Hx4AZfwM9g0uzcL/8mHcNXrgDCX9FF8aR+RaG+svaO/OC4MbxvGyqzGbHRKk2fzkEYBW0nup0PtvCaCup3u9UIrMHORnJvCkBv3ohdJncXbC6xuao7cWCbIiQ4BZ5jSv536+FLTiUj2oR/K1S2NDDq1+DQ5Ud3zOtaurra/ayNAGFCteKb8fBv5Pi3VGW6QTDAXfLjP5Hr/Q8I3B2C56GnI9YcQPguUiyYj6RopF0ivCSz+4QE+hd8IqauJQO56Zf3PtTnGx154qe3uUqOwEGG4ZkgfFdfKMUOBxIum2dDe2esVf7PHDCnpuM83dWdZLhRxdAOapbSiWSrJmtaWXG4Xyd3Mk9y9apTEVDGWStufE4IeKK+nNgSYkoWYSyYGddyaRhrOXh/B1m5ujMg7J4jJK40HxIwwhbVvZUIIv4vXGP1NKlEpQTyk1lbsnyDTUEhXAcWzbD1g8vx/Unw67Foi7VeGUgNS06bzrdKtBr5Y7eQvCsE1tVHinwFaJXl6yBibz7Q5oQF1J8VLW7TeF8qJzVvL4drfsT+H4yj6hPdjyNP/R/zxOPHrgwMNmH9wFFEqa63G0rVgKI4Crn2Prm5vPtCHFh4Ruwf7cH87pwq8XSppcFUDcCiS69Hy8/J+yI8e02qHPgcyxlIAWeJRM0DmYcUvBxAcZa+JII15TJ5X+C4hluxm4nEBgLRMcy2Re3REnxVDCMsGd9/wX0KnB+yJaYlfOMi2fxkUg23hWANokKkF2Gx6ehRgyahpfYTA/BMYE4Qju4xOjc1EtWOoYT3REbkcoX/WlB2sQSJ/CPjvRbFyd9a8E+mAN46pgO++bmEMympW1UjSQgv9vqqnzV2dg3Di2Fy8cz9yKP82mcuhxcaWn/Sp2sq8OG6LhGMlJFMTMcOhZzoMvvekcfAQ1kVCOjFz8xA6Sqw5htE1POjLLN0xW2QSusPSvDxtF/OesjK2pOkfPWgjFw+XOLXktx/TJfiuPVGmVpf+qzI/pnjMKtuia82kf/ZeKRxm4k1dEAIkY/HonNP/TamzRuY0m6TezKJe1GMjZUrESNMBrhyVwdnqzaTUEaEf+exHwyOJQvE92YmdPRKIeYI50eDzWi9DaTmMDaJq23LJcIoyicfS7gZZb+Fjs/tkFMfQmkn/C5rnVA1VWJMNQsJqVV0WZ1t2o3S0LUXy4YOWTiKV9NyUEjP2z3vviz1Woqk3bHas3fdGGZkVM27eZf8E7yFjR2zV70Awc9DFc/WIRLd+ZEDtjEud+WToiU4zgCb/yt/TAtUj0krbzv/owAHpIeKcRoLv9ZlpwDBGXfSg+KFAg9F7yjNuuEy60l8PhMF1qKqNoXpjmLIn034HAUb8FV5rFIR4bcloImWNwvqWHrSu9eZLZRR2HLqOuhWPJf5q3kKA1Ft9BkJKOZUr7ceGDtpCX7Zy+LgWf5chVuZio9fizEVOuX5c3SnvkMudbScqRRihy3gh9vQrCOXoJ/bad0yfldcZ0dN43TJhaR5/nhf0n+ZYuZ8XJ1qhUXzfO8Qes8PqRIZ/Hif6PrdGHajkGdZ4sJaoHESumLjx7GzQ0eoIzPl/pO38OKrH2rsnwBvHYibv+DuaUqIcQfqr5JSK+IJgs/1YkUy/6Y2o6RqGV+TE4Xwt7kXZ1v6FqIGoTOjRBB7GJAfo0mlNCTri7iCgR11Anme5fj5DGckJuT2PzbG1W/XDdatW2xjyJI9PFM80Hhxx/YsvEaDFxz5Po4i+2uAUaxphRWhoZqpVrardeRUThqOIKaOmsaeCW+oYMnDgcwm+DRqWvd39DcbmCDXFarDDIevnXOUl6cF77XC5IQprTZXY2BKlI6bRFIkNIlG1WPFkjuVqETHZyLVd2s/iIzA4fwiKhhkLSvw1tsYb8T5bLAT12YB8pOaw8SOEwL/hGf654j0GrBXMjq2qUSoOula1MgmPlANHehxDjJdBNdqk9MVRGxSx9D6j9NddQMvBEHkTXf5hFwoiAZSALoAHYtLm/7VpD8lMF7MwdVf2t/2WufnpAEMdtNhx5MPWAUai6Wcz5YzO6qX0gSKYUvOVQXhskFthZCAzO+a8680r1VzKWcnciSjJQl/JQeKWCNtjJk7mT2P+mWPZNJXgVQR6I0QbDH4FEW3PgQSYzBIKG49U+jsmJtyqRsutcpunEUqmFGifpcZBnQXUcJ2UxPjf4e2QKkCKa08GckCFDrYy1B963xrSi+RH/U88GlkwmxY63khy16nbQxp63WkaqWE4fU2nz6XD54UdLbJtqQcw5w+6TU7NK6fYKGnE7QLD2eTNVnGZEPrSDy8IzpdjOO4PS3jbISWddnBhRCD0Gn35EL7KqSgmITk5b7kSyQPca3l56ux+AUrBmmO6kWiIsM4YmuK8BdwLGRniw+TW600L1OixC9iAEhHKHb2UPfy9FO32CgdupZMKRdQWfE8KOlfk4dxRnjSa90nwB2uJMzPWhbBNUjfW/a24l1svS7nu4tAptWJubLgFQBMi3P/kVu2tHEOwddU/TLZJzWCyjbZV71riBY5jnuClAcQx+WhT/IwBwFxyxNLB3mZ+BGhQARay7jGI/64ZirisWlKUvhgVGjRStti+I7Tc3V+ENbtjeo7pbIJDXssZaYkwP/ANiaxOUTwEeuY/kpEQDVe09gKE0wMIg0B57I+cZXFhhNIAlv+E5HLysFoi7hEdT5A3f9UOAeiT7xtVLpdNtq83DNdY05rzRPUYxOuYbos0WRNHJ8SVoSBbACj9UU+rd9IIDPMYrYwWgg02WMkyTSJAkJozp30Tj0SJxp5UlzoaTP0yDFC9bJW5fmOPdORnPCdK4HNFgsTNki4yPY0elIBxjNt3CGAZDjRMtjVxsRjLbSTGYoPI6pMf1C4pa2XiOrvo2LJF13e4AkQnpbMeLvIrc1FlECG+63yBfhE9E6JjKb4qxQkFhO7tH6qSKYNA40yEGoMGANMXu5S+nvaTJToAPaayiHxNAzU6YuZohWJds4nAC1um+Ik5u+fMqGH/QVzt5D/Fc66GLGZQm+UltZqNKVeZeHEUa01kU2dzNzeCmKJ7+95teOhHjYfBoqyJLKLFStSa2rPoEGyPt3uQQ+DRmH1KqDhPogpcv2ftbo+zII+EQbpD02i/r6UGlTGxGMUOQHraaJe+FCTYEStyl+q68PzVji25Ymgau8xSmCdwe1Wo76jUn3RPfykUYQz04qhM6PJht5OcXq7by+MjuP/+8MEI5WY9A8+hE+oRthCyvYcd3eBx4se21DnBE+PqQOJ9TiQDIExK+SIP2NM1fj8F9T+lK6+Jl0UywiUAtfE/y0wE2/lcnHrY1hrmFXTkLsyMn0CXUGw9x1oZzn6jp5jOPBGYy/qgj9vvoHOGFSDCDwL5ny0WLbXaGS3+M+AbIOuWgz6vXolB1XNdPtP87s8595VdAMiEXcfJjTr36NhViNI8V4xmWT4NC0oBMt8YR9QhPLIr5wum7uaElsnE3fqxgAfLDC8Z5rEi438FJJt41LVQ4DjKI6gG9FwcGSkYJsgw0KLHvDbBEAAgtz/AV57OhoYg4fcXd/yvsRQE+OJWxfGYFhwCMRlmgzJIbaNfLrgb56wB5zd03yIWjQgzpBkfVUXJH/ADgrxBT1PHzRv5B4jO9+SvU+hBGbhIB/w+E2M/RzKaAe0DjJz1hjhiLbD7Lfm/j1BBySKj1Spc5CsPLXbldeGFwQTKnoiJJ1u0L6uzuTRtaoXjfeBufx1TJ6uX1JwfQhHishYVByhimMhmUaPDsI3bgqekP9VTkuOxZsCxnSsJGl/z2x3Yksq8kg0nEWyXdz6r+CZlZaheWtgVIL5aFtJv/dKELSBb9Qtt8iL0ZK7QUgS6sPfll4AIsTYfDT44xs8YDUOw8KfOjdh/YpLhTk3uPrhKkC/QkGNqsycZA8srRbMHLxr0fMqUWtmk94ofkCpcCQ5EpFQjzi6ByJrEFx9sBScfxFf5E/eX5PFU0/Ouz7v0eyJtxa15U+DUZZFwgDNzSzyE6rD32Dc0n+LqCethknbzv9M/jCack5lBYuQ8+ftQN/CfVqVbcz2bhve+C0DF6iy9/TNpBDuBiZBnLD2PLDAPvFIsjWeTpmhbdAsuNuYJhOu7dHAthEEladumofcR4yZXZ7W+uUf590848ruF3AXINv+50b9KkO0w5q0yfOx0OdnkiO9KUnjt0jBwNnQg++7XwQHD/NzHUVTNTBqOOAHvZh6uy4F/PQ7RENThDd02wWm08SR01je/Azey8FDU6nRsLJ0ESVSNUMIkR7kajkmCeAr8zM6g1YpNbdY9XPip+IxQ6hzEKfKf26sA=="

    invoke-virtual {v2, v5, v3}, Lcom/google/android/a/aa;->a([BLjava/lang/String;)[B

    move-result-object v6

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_1

    const-string v2, "dex"

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v2, v3}, Landroid/content/Context;->getDir(Ljava/lang/String;I)Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_1

    new-instance v2, Lcom/google/android/a/s;

    invoke-direct {v2}, Lcom/google/android/a/s;-><init>()V

    throw v2
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Lcom/google/android/a/ab; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Lcom/google/android/a/s; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    :catch_0
    move-exception v2

    :try_start_3
    new-instance v3, Lcom/google/android/a/s;

    invoke-direct {v3, v2}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_3
    .catch Lcom/google/android/a/s; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 107
    :catch_1
    move-exception v2

    .line 109
    :cond_0
    :goto_0
    monitor-exit v4

    return-void

    :cond_1
    move-object v3, v2

    .line 97
    :try_start_4
    const-string v2, "ads"

    const-string v7, ".jar"

    invoke-static {v2, v7, v3}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v7

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v7}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v8, 0x0

    array-length v9, v6

    invoke-virtual {v2, v6, v8, v9}, Ljava/io/FileOutputStream;->write([BII)V

    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Lcom/google/android/a/ab; {:try_start_4 .. :try_end_4} :catch_5
    .catch Ljava/lang/NoSuchMethodException; {:try_start_4 .. :try_end_4} :catch_6
    .catch Ljava/lang/NullPointerException; {:try_start_4 .. :try_end_4} :catch_7
    .catch Lcom/google/android/a/s; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :try_start_5
    new-instance v2, Ldalvik/system/DexClassLoader;

    invoke-virtual {v7}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v10

    invoke-direct {v2, v6, v8, v9, v10}, Ldalvik/system/DexClassLoader;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/ClassLoader;)V

    const-string v6, "+oTkeB+CNL+0mM6Pl69wsEl+EhI6mOsNLl1XtD69iKEZHX+7J9RoAbDW6W3Si+64"

    invoke-static {v5, v6}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v6

    const-string v8, "YW7Z1xUuw/QvLAHgZUKWt+5ToYVFs+2TE7UFrJ7myst5XbB/onGRQymvF99YNQNl"

    invoke-static {v5, v8}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v2, v8}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v8

    const-string v9, "BLUIwa2VKFXRYf9W6xM9MaYaIEWLHdk5NjJAyUuNFWnkkCLkZ8aWVUBq4lBkYzwv"

    invoke-static {v5, v9}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v2, v9}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v9

    const-string v10, "L4Q2VQ4ybxahYlYQ5CVdWnJuh8tE/H2BSMv2yjA8TmyXWNeJBn/t3Kwboj/HmSeV"

    invoke-static {v5, v10}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v10

    const-string v11, "aGaGvmhxIQYuDHcZoJRsjnjOiNJdWhqllYlOfMf4NN2/Mol3uIeGs5NVwtDXhOuw"

    invoke-static {v5, v11}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v2, v11}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v11

    const-string v12, "KEUrfSL7q3VD9muAdlzrroDsLlYCoGTRoFVQYJ1czpp/veWhhRbjEDIeeigyK09H"

    invoke-static {v5, v12}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v2, v12}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v12

    const-string v13, "3/Iyk+JIL0hO+Td9skreewaShNgd0/gnJaYZSoLRBSLWlAMCwSL5PMlXThAHRUcN"

    invoke-static {v5, v13}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v2, v13}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v13

    const-string v14, "c55gvAgEs0pkahbk0pUQ3HRRujv7wKlHzSTP6yTLqLknx5lnbjQsQui9vwDdsF2I"

    invoke-static {v5, v14}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v2, v14}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v14

    const-string v15, "8phHgZsVZadb5ldAOffWpYwbaIBA43N57gDyTO6PFHpgOXRido0XmYVgT+yZwaoo"

    invoke-static {v5, v15}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v2, v15}, Ldalvik/system/DexClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v2

    const-string v15, "o+KLhR/F0UCgsxf1hHefTPtJt5+ir73hrD8AW8KWXFY="

    invoke-static {v5, v15}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v15

    const/16 v16, 0x0

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Class;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-virtual {v6, v15, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    sput-object v6, Lcom/google/android/a/r;->e:Ljava/lang/reflect/Method;

    const-string v6, "y4CHUE3DVWRJRFFKtvXCDNiYMmvS3xrd4rQiZYI1yOI="

    invoke-static {v5, v6}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v15, 0x0

    new-array v15, v15, [Ljava/lang/Class;

    invoke-virtual {v8, v6, v15}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    sput-object v6, Lcom/google/android/a/r;->f:Ljava/lang/reflect/Method;

    const-string v6, "++aammx9NTDE22NYDKTK2mf0X7APj5JAOgDEx2V6wRE="

    invoke-static {v5, v6}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Class;

    invoke-virtual {v9, v6, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    sput-object v6, Lcom/google/android/a/r;->g:Ljava/lang/reflect/Method;

    const-string v6, "CH52ZV7JwZaoUVA8fZhYLpi0qiAlNraBiJlJ4ZUSTKE="

    invoke-static {v5, v6}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v15, Landroid/content/Context;

    aput-object v15, v8, v9

    invoke-virtual {v10, v6, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    sput-object v6, Lcom/google/android/a/r;->h:Ljava/lang/reflect/Method;

    const-string v6, "Q43J6Bu1T8T140YgEnxmU3hT6DwzW+UvZJWnVnNIn7Q="

    invoke-static {v5, v6}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Landroid/view/MotionEvent;

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-class v10, Landroid/util/DisplayMetrics;

    aput-object v10, v8, v9

    invoke-virtual {v11, v6, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    sput-object v6, Lcom/google/android/a/r;->i:Ljava/lang/reflect/Method;

    const-string v6, "9o4Wq8N2AbE7AyeVBZIAkPPm0dx0hWmTI7btxGEstjU="

    invoke-static {v5, v6}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Landroid/content/Context;

    aput-object v10, v8, v9

    invoke-virtual {v12, v6, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    sput-object v6, Lcom/google/android/a/r;->j:Ljava/lang/reflect/Method;

    const-string v6, "jKlqQ5tLbIscjwRdjlIzbQvS5G7eIUvH6aOhZ9N3y2Q="

    invoke-static {v5, v6}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Landroid/content/Context;

    aput-object v10, v8, v9

    invoke-virtual {v13, v6, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    sput-object v6, Lcom/google/android/a/r;->k:Ljava/lang/reflect/Method;

    const-string v6, "FU2+z9NGTU6sQZrhAXfjEJYibS+Uz+51NPRq8aVJzZs="

    invoke-static {v5, v6}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Class;

    const/4 v9, 0x0

    const-class v10, Landroid/content/Context;

    aput-object v10, v8, v9

    invoke-virtual {v14, v6, v8}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v6

    sput-object v6, Lcom/google/android/a/r;->l:Ljava/lang/reflect/Method;

    const-string v6, "gszshh8I5QzwohavUBV66H+EFc/Eb81UomVy98b0+wI="

    invoke-static {v5, v6}, Lcom/google/android/a/r;->a([BLjava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Class;

    const/4 v8, 0x0

    const-class v9, Landroid/content/Context;

    aput-object v9, v6, v8

    invoke-virtual {v2, v5, v6}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v2

    sput-object v2, Lcom/google/android/a/r;->m:Ljava/lang/reflect/Method;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    new-instance v5, Ljava/io/File;

    const-string v6, ".jar"

    const-string v7, ".dex"

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v5, v3, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_0
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Lcom/google/android/a/ab; {:try_start_6 .. :try_end_6} :catch_5
    .catch Ljava/lang/NoSuchMethodException; {:try_start_6 .. :try_end_6} :catch_6
    .catch Ljava/lang/NullPointerException; {:try_start_6 .. :try_end_6} :catch_7
    .catch Lcom/google/android/a/s; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_6 .. :try_end_6} :catch_2
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    .line 100
    :try_start_7
    invoke-static {}, Lcom/google/android/a/r;->c()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sput-wide v2, Lcom/google/android/a/r;->q:J

    .line 102
    const/4 v2, 0x1

    sput-boolean v2, Lcom/google/android/a/r;->d:Z
    :try_end_7
    .catch Lcom/google/android/a/s; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto/16 :goto_0

    :catch_2
    move-exception v2

    goto/16 :goto_0

    .line 97
    :catchall_0
    move-exception v2

    :try_start_8
    invoke-virtual {v7}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v7}, Ljava/io/File;->delete()Z

    new-instance v6, Ljava/io/File;

    const-string v7, ".jar"

    const-string v8, ".dex"

    invoke-virtual {v5, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v6, v3, v5}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v6}, Ljava/io/File;->delete()Z

    throw v2
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_0
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3
    .catch Ljava/lang/ClassNotFoundException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Lcom/google/android/a/ab; {:try_start_8 .. :try_end_8} :catch_5
    .catch Ljava/lang/NoSuchMethodException; {:try_start_8 .. :try_end_8} :catch_6
    .catch Ljava/lang/NullPointerException; {:try_start_8 .. :try_end_8} :catch_7
    .catch Lcom/google/android/a/s; {:try_start_8 .. :try_end_8} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_8 .. :try_end_8} :catch_2
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catch_3
    move-exception v2

    :try_start_9
    new-instance v3, Lcom/google/android/a/s;

    invoke-direct {v3, v2}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_9
    .catch Lcom/google/android/a/s; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    .line 88
    :catchall_1
    move-exception v2

    monitor-exit v4

    throw v2

    .line 97
    :catch_4
    move-exception v2

    :try_start_a
    new-instance v3, Lcom/google/android/a/s;

    invoke-direct {v3, v2}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v3

    :catch_5
    move-exception v2

    new-instance v3, Lcom/google/android/a/s;

    invoke-direct {v3, v2}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v3

    :catch_6
    move-exception v2

    new-instance v3, Lcom/google/android/a/s;

    invoke-direct {v3, v2}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v3

    :catch_7
    move-exception v2

    new-instance v3, Lcom/google/android/a/s;

    invoke-direct {v3, v2}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v3
    :try_end_a
    .catch Lcom/google/android/a/s; {:try_start_a .. :try_end_a} :catch_1
    .catch Ljava/lang/UnsupportedOperationException; {:try_start_a .. :try_end_a} :catch_2
    .catchall {:try_start_a .. :try_end_a} :catchall_1
.end method

.method private static b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 221
    sget-object v0, Lcom/google/android/a/r;->n:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 222
    new-instance v0, Lcom/google/android/a/s;

    invoke-direct {v0}, Lcom/google/android/a/s;-><init>()V

    throw v0

    .line 225
    :cond_0
    sget-object v0, Lcom/google/android/a/r;->n:Ljava/lang/String;

    return-object v0
.end method

.method private static b(Landroid/content/Context;Lcom/google/android/a/y;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 362
    sget-object v0, Lcom/google/android/a/r;->p:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 363
    sget-object v0, Lcom/google/android/a/r;->p:Ljava/lang/String;

    .line 377
    :goto_0
    return-object v0

    .line 366
    :cond_0
    sget-object v0, Lcom/google/android/a/r;->k:Ljava/lang/reflect/Method;

    if-nez v0, :cond_1

    .line 367
    new-instance v0, Lcom/google/android/a/s;

    invoke-direct {v0}, Lcom/google/android/a/s;-><init>()V

    throw v0

    .line 371
    :cond_1
    :try_start_0
    sget-object v0, Lcom/google/android/a/r;->k:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 372
    if-nez v0, :cond_2

    .line 373
    new-instance v0, Lcom/google/android/a/s;

    invoke-direct {v0}, Lcom/google/android/a/s;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 378
    :catch_0
    move-exception v0

    .line 379
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 376
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/a/y;->a([B)Ljava/lang/String;

    move-result-object v0

    .line 377
    sput-object v0, Lcom/google/android/a/r;->p:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 380
    :catch_1
    move-exception v0

    .line 381
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static c()Ljava/lang/Long;
    .locals 3

    .prologue
    .line 229
    sget-object v0, Lcom/google/android/a/r;->e:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 230
    new-instance v0, Lcom/google/android/a/s;

    invoke-direct {v0}, Lcom/google/android/a/s;-><init>()V

    throw v0

    .line 234
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/a/r;->e:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 235
    :catch_0
    move-exception v0

    .line 236
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 237
    :catch_1
    move-exception v0

    .line 238
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 244
    sget-object v0, Lcom/google/android/a/r;->j:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 245
    new-instance v0, Lcom/google/android/a/s;

    invoke-direct {v0}, Lcom/google/android/a/s;-><init>()V

    throw v0

    .line 249
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/a/r;->j:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 250
    if-nez v0, :cond_1

    .line 251
    new-instance v0, Lcom/google/android/a/s;

    invoke-direct {v0}, Lcom/google/android/a/s;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 255
    :catch_0
    move-exception v0

    .line 256
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 257
    :catch_1
    move-exception v0

    .line 258
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 254
    :cond_1
    return-object v0
.end method

.method private static d()Ljava/lang/String;
    .locals 3

    .prologue
    .line 263
    sget-object v0, Lcom/google/android/a/r;->g:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 264
    new-instance v0, Lcom/google/android/a/s;

    invoke-direct {v0}, Lcom/google/android/a/s;-><init>()V

    throw v0

    .line 268
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/a/r;->g:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 269
    :catch_0
    move-exception v0

    .line 270
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 271
    :catch_1
    move-exception v0

    .line 272
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static d(Landroid/content/Context;)Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 332
    sget-object v0, Lcom/google/android/a/r;->l:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 333
    new-instance v0, Lcom/google/android/a/s;

    invoke-direct {v0}, Lcom/google/android/a/s;-><init>()V

    throw v0

    .line 338
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/a/r;->l:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    .line 339
    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 340
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_2

    .line 341
    :cond_1
    new-instance v0, Lcom/google/android/a/s;

    invoke-direct {v0}, Lcom/google/android/a/s;-><init>()V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    .line 344
    :catch_0
    move-exception v0

    .line 345
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 346
    :catch_1
    move-exception v0

    .line 347
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 343
    :cond_2
    return-object v0
.end method

.method private static e()Ljava/lang/Long;
    .locals 3

    .prologue
    .line 386
    sget-object v0, Lcom/google/android/a/r;->f:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 387
    new-instance v0, Lcom/google/android/a/s;

    invoke-direct {v0}, Lcom/google/android/a/s;-><init>()V

    throw v0

    .line 391
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/a/r;->f:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 392
    :catch_0
    move-exception v0

    .line 393
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 394
    :catch_1
    move-exception v0

    .line 395
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static e(Landroid/content/Context;)[I
    .locals 4

    .prologue
    .line 400
    sget-object v0, Lcom/google/android/a/r;->m:Ljava/lang/reflect/Method;

    if-nez v0, :cond_0

    .line 401
    new-instance v0, Lcom/google/android/a/s;

    invoke-direct {v0}, Lcom/google/android/a/s;-><init>()V

    throw v0

    .line 405
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/a/r;->m:Ljava/lang/reflect/Method;

    const/4 v1, 0x0

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 406
    :catch_0
    move-exception v0

    .line 407
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 408
    :catch_1
    move-exception v0

    .line 409
    new-instance v1, Lcom/google/android/a/s;

    invoke-direct {v1, v0}, Lcom/google/android/a/s;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method protected final a()V
    .locals 6

    .prologue
    .line 186
    const/4 v0, 0x2

    :try_start_0
    invoke-static {}, Lcom/google/android/a/r;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/a/r;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/a/s; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 192
    :goto_0
    const/4 v0, 0x1

    :try_start_1
    invoke-static {}, Lcom/google/android/a/r;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/a/r;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Lcom/google/android/a/s; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 198
    :goto_1
    const/16 v0, 0x19

    :try_start_2
    invoke-static {}, Lcom/google/android/a/r;->c()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/a/r;->a(IJ)V
    :try_end_2
    .catch Lcom/google/android/a/s; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 204
    :goto_2
    :try_start_3
    iget-object v0, p0, Lcom/google/android/a/r;->a:Landroid/view/MotionEvent;

    iget-object v1, p0, Lcom/google/android/a/r;->b:Landroid/util/DisplayMetrics;

    invoke-static {v0, v1}, Lcom/google/android/a/r;->a(Landroid/view/MotionEvent;Landroid/util/DisplayMetrics;)Ljava/util/ArrayList;

    move-result-object v1

    .line 205
    const/16 v2, 0xe

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v2, v4, v5}, Lcom/google/android/a/r;->a(IJ)V

    .line 206
    const/16 v2, 0xf

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v2, v4, v5}, Lcom/google/android/a/r;->a(IJ)V

    .line 207
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v2, 0x3

    if-lt v0, v2, :cond_0

    .line 209
    const/16 v2, 0x10

    const/4 v0, 0x2

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/a/r;->a(IJ)V
    :try_end_3
    .catch Lcom/google/android/a/s; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 217
    :cond_0
    :goto_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_3

    :catch_2
    move-exception v0

    goto :goto_2

    :catch_3
    move-exception v0

    goto :goto_1

    :catch_4
    move-exception v0

    goto :goto_0
.end method

.method protected b(Landroid/content/Context;)V
    .locals 6

    .prologue
    .line 121
    const/4 v0, 0x1

    :try_start_0
    invoke-static {}, Lcom/google/android/a/r;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/a/r;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/a/s; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 127
    :goto_0
    const/4 v0, 0x2

    :try_start_1
    invoke-static {}, Lcom/google/android/a/r;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/a/r;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Lcom/google/android/a/s; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 133
    :goto_1
    :try_start_2
    invoke-static {}, Lcom/google/android/a/r;->c()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 134
    const/16 v2, 0x19

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/a/r;->a(IJ)V

    .line 136
    sget-wide v2, Lcom/google/android/a/r;->q:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    .line 137
    const/16 v2, 0x11

    sget-wide v4, Lcom/google/android/a/r;->q:J

    sub-long/2addr v0, v4

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/a/r;->a(IJ)V

    .line 138
    const/16 v0, 0x17

    sget-wide v2, Lcom/google/android/a/r;->q:J

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/a/r;->a(IJ)V
    :try_end_2
    .catch Lcom/google/android/a/s; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 145
    :cond_0
    :goto_2
    :try_start_3
    invoke-static {p1}, Lcom/google/android/a/r;->d(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v1

    .line 146
    const/16 v2, 0x1f

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v2, v4, v5}, Lcom/google/android/a/r;->a(IJ)V

    .line 147
    const/16 v2, 0x20

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v2, v0, v1}, Lcom/google/android/a/r;->a(IJ)V
    :try_end_3
    .catch Lcom/google/android/a/s; {:try_start_3 .. :try_end_3} :catch_5
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    .line 153
    :goto_3
    const/16 v0, 0x21

    :try_start_4
    invoke-static {}, Lcom/google/android/a/r;->e()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/a/r;->a(IJ)V
    :try_end_4
    .catch Lcom/google/android/a/s; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    .line 159
    :goto_4
    const/16 v0, 0x1b

    :try_start_5
    iget-object v1, p0, Lcom/google/android/a/r;->c:Lcom/google/android/a/y;

    invoke-static {p1, v1}, Lcom/google/android/a/r;->a(Landroid/content/Context;Lcom/google/android/a/y;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/a/r;->a(ILjava/lang/String;)V
    :try_end_5
    .catch Lcom/google/android/a/s; {:try_start_5 .. :try_end_5} :catch_3
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    .line 165
    :goto_5
    const/16 v0, 0x1d

    :try_start_6
    iget-object v1, p0, Lcom/google/android/a/r;->c:Lcom/google/android/a/y;

    invoke-static {p1, v1}, Lcom/google/android/a/r;->b(Landroid/content/Context;Lcom/google/android/a/y;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/a/r;->a(ILjava/lang/String;)V
    :try_end_6
    .catch Lcom/google/android/a/s; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0

    .line 171
    :goto_6
    :try_start_7
    invoke-static {p1}, Lcom/google/android/a/r;->e(Landroid/content/Context;)[I

    move-result-object v0

    .line 172
    const/4 v1, 0x5

    const/4 v2, 0x0

    aget v2, v0, v2

    int-to-long v2, v2

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/a/r;->a(IJ)V

    .line 173
    const/4 v1, 0x6

    const/4 v2, 0x1

    aget v0, v0, v2

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/a/r;->a(IJ)V
    :try_end_7
    .catch Lcom/google/android/a/s; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    .line 180
    :goto_7
    return-void

    :catch_0
    move-exception v0

    goto :goto_7

    :catch_1
    move-exception v0

    goto :goto_7

    :catch_2
    move-exception v0

    goto :goto_6

    :catch_3
    move-exception v0

    goto :goto_5

    :catch_4
    move-exception v0

    goto :goto_4

    :catch_5
    move-exception v0

    goto :goto_3

    :catch_6
    move-exception v0

    goto :goto_2

    :catch_7
    move-exception v0

    goto/16 :goto_1

    :catch_8
    move-exception v0

    goto/16 :goto_0
.end method
