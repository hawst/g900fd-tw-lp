.class public final Lcom/google/g/a/e;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/g/a/b;

.field public b:Lcom/google/g/a/d;

.field public c:Lcom/google/g/a/c;

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 15
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 20
    iput-object v0, p0, Lcom/google/g/a/e;->a:Lcom/google/g/a/b;

    .line 40
    iput-object v0, p0, Lcom/google/g/a/e;->b:Lcom/google/g/a/d;

    .line 60
    iput-object v0, p0, Lcom/google/g/a/e;->c:Lcom/google/g/a/c;

    .line 109
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/g/a/e;->g:I

    .line 15
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 112
    iget v0, p0, Lcom/google/g/a/e;->g:I

    if-gez v0, :cond_0

    .line 114
    invoke-virtual {p0}, Lcom/google/g/a/e;->b()I

    .line 116
    :cond_0
    iget v0, p0, Lcom/google/g/a/e;->g:I

    return v0
.end method

.method public final a(Lcom/google/g/a/b;)Lcom/google/g/a/e;
    .locals 1

    .prologue
    .line 24
    if-nez p1, :cond_0

    .line 25
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 27
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/g/a/e;->d:Z

    .line 28
    iput-object p1, p0, Lcom/google/g/a/e;->a:Lcom/google/g/a/b;

    .line 29
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 12
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/g/a/b;

    invoke-direct {v0}, Lcom/google/g/a/b;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/g/a/e;->a(Lcom/google/g/a/b;)Lcom/google/g/a/e;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/g/a/d;

    invoke-direct {v0}, Lcom/google/g/a/d;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iput-boolean v1, p0, Lcom/google/g/a/e;->e:Z

    iput-object v0, p0, Lcom/google/g/a/e;->b:Lcom/google/g/a/d;

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/g/a/c;

    invoke-direct {v0}, Lcom/google/g/a/c;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iput-boolean v1, p0, Lcom/google/g/a/e;->f:Z

    iput-object v0, p0, Lcom/google/g/a/e;->c:Lcom/google/g/a/c;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 98
    iget-boolean v0, p0, Lcom/google/g/a/e;->d:Z

    if-eqz v0, :cond_0

    .line 99
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/g/a/e;->a:Lcom/google/g/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 101
    :cond_0
    iget-boolean v0, p0, Lcom/google/g/a/e;->e:Z

    if-eqz v0, :cond_1

    .line 102
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/g/a/e;->b:Lcom/google/g/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 104
    :cond_1
    iget-boolean v0, p0, Lcom/google/g/a/e;->f:Z

    if-eqz v0, :cond_2

    .line 105
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/g/a/e;->c:Lcom/google/g/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 107
    :cond_2
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 121
    const/4 v0, 0x0

    .line 122
    iget-boolean v1, p0, Lcom/google/g/a/e;->d:Z

    if-eqz v1, :cond_0

    .line 123
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/g/a/e;->a:Lcom/google/g/a/b;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 126
    :cond_0
    iget-boolean v1, p0, Lcom/google/g/a/e;->e:Z

    if-eqz v1, :cond_1

    .line 127
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/g/a/e;->b:Lcom/google/g/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 130
    :cond_1
    iget-boolean v1, p0, Lcom/google/g/a/e;->f:Z

    if-eqz v1, :cond_2

    .line 131
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/g/a/e;->c:Lcom/google/g/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 134
    :cond_2
    iput v0, p0, Lcom/google/g/a/e;->g:I

    .line 135
    return v0
.end method
