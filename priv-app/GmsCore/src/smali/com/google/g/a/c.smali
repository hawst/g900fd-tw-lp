.class public final Lcom/google/g/a/c;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:I

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 495
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 505
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/g/a/c;->a:I

    .line 538
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/g/a/c;->c:I

    .line 495
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 541
    iget v0, p0, Lcom/google/g/a/c;->c:I

    if-gez v0, :cond_0

    .line 543
    invoke-virtual {p0}, Lcom/google/g/a/c;->b()I

    .line 545
    :cond_0
    iget v0, p0, Lcom/google/g/a/c;->c:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 492
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/g/a/c;->b:Z

    iput v0, p0, Lcom/google/g/a/c;->a:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 533
    iget-boolean v0, p0, Lcom/google/g/a/c;->b:Z

    if-eqz v0, :cond_0

    .line 534
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/g/a/c;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 536
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 550
    const/4 v0, 0x0

    .line 551
    iget-boolean v1, p0, Lcom/google/g/a/c;->b:Z

    if-eqz v1, :cond_0

    .line 552
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/g/a/c;->a:I

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 555
    :cond_0
    iput v0, p0, Lcom/google/g/a/c;->c:I

    .line 556
    return v0
.end method
