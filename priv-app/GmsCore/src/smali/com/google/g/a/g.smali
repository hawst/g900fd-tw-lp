.class public final Lcom/google/g/a/g;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Ljava/lang/String;

.field public d:Lcom/google/protobuf/a/a;

.field private e:Z

.field private f:I

.field private g:Z

.field private h:Ljava/lang/String;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 23
    iput v1, p0, Lcom/google/g/a/g;->f:I

    .line 40
    const-string v0, ""

    iput-object v0, p0, Lcom/google/g/a/g;->h:Ljava/lang/String;

    .line 57
    const-string v0, ""

    iput-object v0, p0, Lcom/google/g/a/g;->j:Ljava/lang/String;

    .line 74
    const-string v0, ""

    iput-object v0, p0, Lcom/google/g/a/g;->a:Ljava/lang/String;

    .line 91
    iput v1, p0, Lcom/google/g/a/g;->b:I

    .line 108
    const-string v0, ""

    iput-object v0, p0, Lcom/google/g/a/g;->c:Ljava/lang/String;

    .line 125
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/g/a/g;->d:Lcom/google/protobuf/a/a;

    .line 186
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/g/a/g;->o:I

    .line 10
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 189
    iget v0, p0, Lcom/google/g/a/g;->o:I

    if-gez v0, :cond_0

    .line 191
    invoke-virtual {p0}, Lcom/google/g/a/g;->b()I

    .line 193
    :cond_0
    iget v0, p0, Lcom/google/g/a/g;->o:I

    return v0
.end method

.method public final a(I)Lcom/google/g/a/g;
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/g/a/g;->e:Z

    .line 28
    iput p1, p0, Lcom/google/g/a/g;->f:I

    .line 29
    return-object p0
.end method

.method public final a(Lcom/google/protobuf/a/a;)Lcom/google/g/a/g;
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/g/a/g;->n:Z

    .line 130
    iput-object p1, p0, Lcom/google/g/a/g;->d:Lcom/google/protobuf/a/a;

    .line 131
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/g/a/g;
    .locals 1

    .prologue
    .line 44
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/g/a/g;->g:Z

    .line 45
    iput-object p1, p0, Lcom/google/g/a/g;->h:Ljava/lang/String;

    .line 46
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 1

    .prologue
    .line 7
    invoke-virtual {p0, p1}, Lcom/google/g/a/g;->b(Lcom/google/protobuf/a/b;)Lcom/google/g/a/g;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 163
    iget-boolean v0, p0, Lcom/google/g/a/g;->e:Z

    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/g/a/g;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 166
    :cond_0
    iget-boolean v0, p0, Lcom/google/g/a/g;->g:Z

    if-eqz v0, :cond_1

    .line 167
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/g/a/g;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 169
    :cond_1
    iget-boolean v0, p0, Lcom/google/g/a/g;->i:Z

    if-eqz v0, :cond_2

    .line 170
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/g/a/g;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 172
    :cond_2
    iget-boolean v0, p0, Lcom/google/g/a/g;->k:Z

    if-eqz v0, :cond_3

    .line 173
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/g/a/g;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 175
    :cond_3
    iget-boolean v0, p0, Lcom/google/g/a/g;->l:Z

    if-eqz v0, :cond_4

    .line 176
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/g/a/g;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 178
    :cond_4
    iget-boolean v0, p0, Lcom/google/g/a/g;->m:Z

    if-eqz v0, :cond_5

    .line 179
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/g/a/g;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 181
    :cond_5
    iget-boolean v0, p0, Lcom/google/g/a/g;->n:Z

    if-eqz v0, :cond_6

    .line 182
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/g/a/g;->d:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 184
    :cond_6
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 198
    const/4 v0, 0x0

    .line 199
    iget-boolean v1, p0, Lcom/google/g/a/g;->e:Z

    if-eqz v1, :cond_0

    .line 200
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/g/a/g;->f:I

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 203
    :cond_0
    iget-boolean v1, p0, Lcom/google/g/a/g;->g:Z

    if-eqz v1, :cond_1

    .line 204
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/g/a/g;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 207
    :cond_1
    iget-boolean v1, p0, Lcom/google/g/a/g;->i:Z

    if-eqz v1, :cond_2

    .line 208
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/g/a/g;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_2
    iget-boolean v1, p0, Lcom/google/g/a/g;->k:Z

    if-eqz v1, :cond_3

    .line 212
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/g/a/g;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    :cond_3
    iget-boolean v1, p0, Lcom/google/g/a/g;->l:Z

    if-eqz v1, :cond_4

    .line 216
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/g/a/g;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_4
    iget-boolean v1, p0, Lcom/google/g/a/g;->m:Z

    if-eqz v1, :cond_5

    .line 220
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/g/a/g;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_5
    iget-boolean v1, p0, Lcom/google/g/a/g;->n:Z

    if-eqz v1, :cond_6

    .line 224
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/g/a/g;->d:Lcom/google/protobuf/a/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_6
    iput v0, p0, Lcom/google/g/a/g;->o:I

    .line 228
    return v0
.end method

.method public final b(I)Lcom/google/g/a/g;
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/g/a/g;->l:Z

    .line 96
    iput p1, p0, Lcom/google/g/a/g;->b:I

    .line 97
    return-object p0
.end method

.method public final b(Lcom/google/protobuf/a/b;)Lcom/google/g/a/g;
    .locals 1

    .prologue
    .line 236
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    .line 237
    sparse-switch v0, :sswitch_data_0

    .line 241
    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 242
    :sswitch_0
    return-object p0

    .line 247
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/g/a/g;->a(I)Lcom/google/g/a/g;

    goto :goto_0

    .line 251
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/g/a/g;->a(Ljava/lang/String;)Lcom/google/g/a/g;

    goto :goto_0

    .line 255
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/g/a/g;->b(Ljava/lang/String;)Lcom/google/g/a/g;

    goto :goto_0

    .line 259
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/g/a/g;->c(Ljava/lang/String;)Lcom/google/g/a/g;

    goto :goto_0

    .line 263
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/g/a/g;->b(I)Lcom/google/g/a/g;

    goto :goto_0

    .line 267
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/g/a/g;->d(Ljava/lang/String;)Lcom/google/g/a/g;

    goto :goto_0

    .line 271
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/g/a/g;->a(Lcom/google/protobuf/a/a;)Lcom/google/g/a/g;

    goto :goto_0

    .line 237
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final b(Ljava/lang/String;)Lcom/google/g/a/g;
    .locals 1

    .prologue
    .line 61
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/g/a/g;->i:Z

    .line 62
    iput-object p1, p0, Lcom/google/g/a/g;->j:Ljava/lang/String;

    .line 63
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/g/a/g;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/g/a/g;->k:Z

    .line 79
    iput-object p1, p0, Lcom/google/g/a/g;->a:Ljava/lang/String;

    .line 80
    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/g/a/g;
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/g/a/g;->m:Z

    .line 113
    iput-object p1, p0, Lcom/google/g/a/g;->c:Ljava/lang/String;

    .line 114
    return-object p0
.end method
