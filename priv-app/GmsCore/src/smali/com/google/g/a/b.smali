.class public final Lcom/google/g/a/b;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 191
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 196
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/g/a/b;->b:I

    .line 228
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/g/a/b;->c:I

    .line 191
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 231
    iget v0, p0, Lcom/google/g/a/b;->c:I

    if-gez v0, :cond_0

    .line 233
    invoke-virtual {p0}, Lcom/google/g/a/b;->b()I

    .line 235
    :cond_0
    iget v0, p0, Lcom/google/g/a/b;->c:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 188
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/g/a/b;->a:Z

    iput v0, p0, Lcom/google/g/a/b;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 223
    iget-boolean v0, p0, Lcom/google/g/a/b;->a:Z

    if-eqz v0, :cond_0

    .line 224
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/g/a/b;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 226
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 240
    const/4 v0, 0x0

    .line 241
    iget-boolean v1, p0, Lcom/google/g/a/b;->a:Z

    if-eqz v1, :cond_0

    .line 242
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/g/a/b;->b:I

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 245
    :cond_0
    iput v0, p0, Lcom/google/g/a/b;->c:I

    .line 246
    return v0
.end method
