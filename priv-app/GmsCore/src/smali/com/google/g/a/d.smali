.class public final Lcom/google/g/a/d;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/protobuf/a/a;

.field public b:Lcom/google/protobuf/a/a;

.field public c:Ljava/util/List;

.field public d:I

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 288
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 293
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/g/a/d;->a:Lcom/google/protobuf/a/a;

    .line 310
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/g/a/d;->b:Lcom/google/protobuf/a/a;

    .line 326
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/g/a/d;->c:Ljava/util/List;

    .line 360
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/g/a/d;->d:I

    .line 406
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/g/a/d;->h:I

    .line 288
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 409
    iget v0, p0, Lcom/google/g/a/d;->h:I

    if-gez v0, :cond_0

    .line 411
    invoke-virtual {p0}, Lcom/google/g/a/d;->b()I

    .line 413
    :cond_0
    iget v0, p0, Lcom/google/g/a/d;->h:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 285
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/g/a/d;->e:Z

    iput-object v0, p0, Lcom/google/g/a/d;->a:Lcom/google/protobuf/a/a;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/g/a/d;->f:Z

    iput-object v0, p0, Lcom/google/g/a/d;->b:Lcom/google/protobuf/a/a;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/google/g/a/d;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/g/a/d;->c:Ljava/util/List;

    :cond_2
    iget-object v1, p0, Lcom/google/g/a/d;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->c()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/g/a/d;->g:Z

    iput v0, p0, Lcom/google/g/a/d;->d:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 3

    .prologue
    .line 392
    iget-boolean v0, p0, Lcom/google/g/a/d;->e:Z

    if-eqz v0, :cond_0

    .line 393
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/g/a/d;->a:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 395
    :cond_0
    iget-boolean v0, p0, Lcom/google/g/a/d;->f:Z

    if-eqz v0, :cond_1

    .line 396
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/g/a/d;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 398
    :cond_1
    iget-object v0, p0, Lcom/google/g/a/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/a/a;

    .line 399
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    goto :goto_0

    .line 401
    :cond_2
    iget-boolean v0, p0, Lcom/google/g/a/d;->g:Z

    if-eqz v0, :cond_3

    .line 402
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/g/a/d;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 404
    :cond_3
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 418
    .line 419
    iget-boolean v0, p0, Lcom/google/g/a/d;->e:Z

    if-eqz v0, :cond_3

    .line 420
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/g/a/d;->a:Lcom/google/protobuf/a/a;

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 423
    :goto_0
    iget-boolean v2, p0, Lcom/google/g/a/d;->f:Z

    if-eqz v2, :cond_2

    .line 424
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/g/a/d;->b:Lcom/google/protobuf/a/a;

    invoke-static {v2, v3}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v2

    add-int/2addr v0, v2

    move v2, v0

    .line 429
    :goto_1
    iget-object v0, p0, Lcom/google/g/a/d;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/a/a;

    .line 430
    invoke-static {v0}, Lcom/google/protobuf/a/c;->a(Lcom/google/protobuf/a/a;)I

    move-result v0

    add-int/2addr v1, v0

    .line 432
    goto :goto_2

    .line 433
    :cond_0
    add-int v0, v2, v1

    .line 434
    iget-object v1, p0, Lcom/google/g/a/d;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 436
    iget-boolean v1, p0, Lcom/google/g/a/d;->g:Z

    if-eqz v1, :cond_1

    .line 437
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/g/a/d;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 440
    :cond_1
    iput v0, p0, Lcom/google/g/a/d;->h:I

    .line 441
    return v0

    :cond_2
    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method
