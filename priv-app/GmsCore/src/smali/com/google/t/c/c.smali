.class public final Lcom/google/t/c/c;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:F

.field public b:F

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 713
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 714
    iput v0, p0, Lcom/google/t/c/c;->a:F

    iput v0, p0, Lcom/google/t/c/c;->b:F

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/t/c/c;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/t/c/c;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/t/c/c;->cachedSize:I

    .line 715
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 784
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 785
    iget v1, p0, Lcom/google/t/c/c;->a:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 787
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/t/c/c;->a:F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 790
    :cond_0
    iget v1, p0, Lcom/google/t/c/c;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-eq v1, v2, :cond_1

    .line 792
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/t/c/c;->b:F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 795
    :cond_1
    iget v1, p0, Lcom/google/t/c/c;->c:I

    if-eqz v1, :cond_2

    .line 796
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/t/c/c;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 799
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 728
    if-ne p1, p0, :cond_1

    .line 729
    const/4 v0, 0x1

    .line 750
    :cond_0
    :goto_0
    return v0

    .line 731
    :cond_1
    instance-of v1, p1, Lcom/google/t/c/c;

    if-eqz v1, :cond_0

    .line 734
    check-cast p1, Lcom/google/t/c/c;

    .line 736
    iget v1, p0, Lcom/google/t/c/c;->a:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    .line 737
    iget v2, p1, Lcom/google/t/c/c;->a:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 742
    iget v1, p0, Lcom/google/t/c/c;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    .line 743
    iget v2, p1, Lcom/google/t/c/c;->b:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 747
    iget v1, p0, Lcom/google/t/c/c;->c:I

    iget v2, p1, Lcom/google/t/c/c;->c:I

    if-ne v1, v2, :cond_0

    .line 750
    invoke-virtual {p0, p1}, Lcom/google/t/c/c;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 755
    iget v0, p0, Lcom/google/t/c/c;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 758
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/t/c/c;->b:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 760
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/t/c/c;->c:I

    add-int/2addr v0, v1

    .line 761
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/t/c/c;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 762
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 687
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/t/c/c;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/t/c/c;->a:F

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/t/c/c;->b:F

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/t/c/c;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 768
    iget v0, p0, Lcom/google/t/c/c;->a:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 770
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/t/c/c;->a:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 772
    :cond_0
    iget v0, p0, Lcom/google/t/c/c;->b:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    if-eq v0, v1, :cond_1

    .line 774
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/t/c/c;->b:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 776
    :cond_1
    iget v0, p0, Lcom/google/t/c/c;->c:I

    if-eqz v0, :cond_2

    .line 777
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/t/c/c;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 779
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 780
    return-void
.end method
