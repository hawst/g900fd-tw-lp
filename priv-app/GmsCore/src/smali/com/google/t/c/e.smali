.class public final Lcom/google/t/c/e;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/t/c/d;

.field public b:Lcom/google/t/c/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 563
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 564
    iput-object v0, p0, Lcom/google/t/c/e;->a:Lcom/google/t/c/d;

    iput-object v0, p0, Lcom/google/t/c/e;->b:Lcom/google/t/c/d;

    iput-object v0, p0, Lcom/google/t/c/e;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/t/c/e;->cachedSize:I

    .line 565
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 630
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 631
    iget-object v1, p0, Lcom/google/t/c/e;->a:Lcom/google/t/c/d;

    if-eqz v1, :cond_0

    .line 632
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/t/c/e;->a:Lcom/google/t/c/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 635
    :cond_0
    iget-object v1, p0, Lcom/google/t/c/e;->b:Lcom/google/t/c/d;

    if-eqz v1, :cond_1

    .line 636
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/t/c/e;->b:Lcom/google/t/c/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 639
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 577
    if-ne p1, p0, :cond_1

    .line 578
    const/4 v0, 0x1

    .line 602
    :cond_0
    :goto_0
    return v0

    .line 580
    :cond_1
    instance-of v1, p1, Lcom/google/t/c/e;

    if-eqz v1, :cond_0

    .line 583
    check-cast p1, Lcom/google/t/c/e;

    .line 584
    iget-object v1, p0, Lcom/google/t/c/e;->a:Lcom/google/t/c/d;

    if-nez v1, :cond_4

    .line 585
    iget-object v1, p1, Lcom/google/t/c/e;->a:Lcom/google/t/c/d;

    if-nez v1, :cond_0

    .line 593
    :cond_2
    iget-object v1, p0, Lcom/google/t/c/e;->b:Lcom/google/t/c/d;

    if-nez v1, :cond_5

    .line 594
    iget-object v1, p1, Lcom/google/t/c/e;->b:Lcom/google/t/c/d;

    if-nez v1, :cond_0

    .line 602
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/t/c/e;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 589
    :cond_4
    iget-object v1, p0, Lcom/google/t/c/e;->a:Lcom/google/t/c/d;

    iget-object v2, p1, Lcom/google/t/c/e;->a:Lcom/google/t/c/d;

    invoke-virtual {v1, v2}, Lcom/google/t/c/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 598
    :cond_5
    iget-object v1, p0, Lcom/google/t/c/e;->b:Lcom/google/t/c/d;

    iget-object v2, p1, Lcom/google/t/c/e;->b:Lcom/google/t/c/d;

    invoke-virtual {v1, v2}, Lcom/google/t/c/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 607
    iget-object v0, p0, Lcom/google/t/c/e;->a:Lcom/google/t/c/d;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 610
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/t/c/e;->b:Lcom/google/t/c/d;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 612
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/t/c/e;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 613
    return v0

    .line 607
    :cond_0
    iget-object v0, p0, Lcom/google/t/c/e;->a:Lcom/google/t/c/d;

    invoke-virtual {v0}, Lcom/google/t/c/d;->hashCode()I

    move-result v0

    goto :goto_0

    .line 610
    :cond_1
    iget-object v1, p0, Lcom/google/t/c/e;->b:Lcom/google/t/c/d;

    invoke-virtual {v1}, Lcom/google/t/c/d;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 540
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/t/c/e;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/t/c/e;->a:Lcom/google/t/c/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/t/c/d;

    invoke-direct {v0}, Lcom/google/t/c/d;-><init>()V

    iput-object v0, p0, Lcom/google/t/c/e;->a:Lcom/google/t/c/d;

    :cond_1
    iget-object v0, p0, Lcom/google/t/c/e;->a:Lcom/google/t/c/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/t/c/e;->b:Lcom/google/t/c/d;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/t/c/d;

    invoke-direct {v0}, Lcom/google/t/c/d;-><init>()V

    iput-object v0, p0, Lcom/google/t/c/e;->b:Lcom/google/t/c/d;

    :cond_2
    iget-object v0, p0, Lcom/google/t/c/e;->b:Lcom/google/t/c/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 619
    iget-object v0, p0, Lcom/google/t/c/e;->a:Lcom/google/t/c/d;

    if-eqz v0, :cond_0

    .line 620
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/t/c/e;->a:Lcom/google/t/c/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 622
    :cond_0
    iget-object v0, p0, Lcom/google/t/c/e;->b:Lcom/google/t/c/d;

    if-eqz v0, :cond_1

    .line 623
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/t/c/e;->b:Lcom/google/t/c/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 625
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 626
    return-void
.end method
