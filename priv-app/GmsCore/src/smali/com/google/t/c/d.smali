.class public final Lcom/google/t/c/d;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 436
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 437
    iput v0, p0, Lcom/google/t/c/d;->a:I

    iput v0, p0, Lcom/google/t/c/d;->b:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/t/c/d;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/t/c/d;->cachedSize:I

    .line 438
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 489
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 490
    iget v1, p0, Lcom/google/t/c/d;->a:I

    if-eqz v1, :cond_0

    .line 491
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/t/c/d;->a:I

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 494
    :cond_0
    iget v1, p0, Lcom/google/t/c/d;->b:I

    if-eqz v1, :cond_1

    .line 495
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/t/c/d;->b:I

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 498
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 450
    if-ne p1, p0, :cond_1

    .line 451
    const/4 v0, 0x1

    .line 463
    :cond_0
    :goto_0
    return v0

    .line 453
    :cond_1
    instance-of v1, p1, Lcom/google/t/c/d;

    if-eqz v1, :cond_0

    .line 456
    check-cast p1, Lcom/google/t/c/d;

    .line 457
    iget v1, p0, Lcom/google/t/c/d;->a:I

    iget v2, p1, Lcom/google/t/c/d;->a:I

    if-ne v1, v2, :cond_0

    .line 460
    iget v1, p0, Lcom/google/t/c/d;->b:I

    iget v2, p1, Lcom/google/t/c/d;->b:I

    if-ne v1, v2, :cond_0

    .line 463
    invoke-virtual {p0, p1}, Lcom/google/t/c/d;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 468
    iget v0, p0, Lcom/google/t/c/d;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 470
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/t/c/d;->b:I

    add-int/2addr v0, v1

    .line 471
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/t/c/d;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 472
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 413
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/t/c/d;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    iput v0, p0, Lcom/google/t/c/d;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    iput v0, p0, Lcom/google/t/c/d;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xd -> :sswitch_1
        0x15 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 478
    iget v0, p0, Lcom/google/t/c/d;->a:I

    if-eqz v0, :cond_0

    .line 479
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/t/c/d;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->d(II)V

    .line 481
    :cond_0
    iget v0, p0, Lcom/google/t/c/d;->b:I

    if-eqz v0, :cond_1

    .line 482
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/t/c/d;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->d(II)V

    .line 484
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 485
    return-void
.end method
