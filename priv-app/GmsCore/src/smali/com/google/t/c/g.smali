.class public final Lcom/google/t/c/g;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:J

.field public d:Ljava/lang/String;

.field public e:Lcom/google/t/c/d;

.field public f:Lcom/google/t/c/d;

.field public g:Lcom/google/t/c/e;

.field public h:F

.field public i:I

.field public j:Lcom/google/t/c/b;

.field public k:J

.field public l:Lcom/google/t/c/b;

.field public m:F

.field public n:Ljava/lang/String;

.field public o:I

.field public p:I

.field public q:I

.field public r:I

.field public s:Lcom/google/t/c/f;

.field public t:Ljava/lang/String;

.field public u:[I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1331
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1332
    iput v2, p0, Lcom/google/t/c/g;->a:I

    iput v2, p0, Lcom/google/t/c/g;->b:I

    iput-wide v4, p0, Lcom/google/t/c/g;->c:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/t/c/g;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    iput-object v1, p0, Lcom/google/t/c/g;->f:Lcom/google/t/c/d;

    iput-object v1, p0, Lcom/google/t/c/g;->g:Lcom/google/t/c/e;

    iput v3, p0, Lcom/google/t/c/g;->h:F

    const/16 v0, 0x64

    iput v0, p0, Lcom/google/t/c/g;->i:I

    iput-object v1, p0, Lcom/google/t/c/g;->j:Lcom/google/t/c/b;

    iput-wide v4, p0, Lcom/google/t/c/g;->k:J

    iput-object v1, p0, Lcom/google/t/c/g;->l:Lcom/google/t/c/b;

    iput v3, p0, Lcom/google/t/c/g;->m:F

    const-string v0, ""

    iput-object v0, p0, Lcom/google/t/c/g;->n:Ljava/lang/String;

    iput v2, p0, Lcom/google/t/c/g;->o:I

    iput v2, p0, Lcom/google/t/c/g;->p:I

    iput v2, p0, Lcom/google/t/c/g;->q:I

    iput v2, p0, Lcom/google/t/c/g;->r:I

    iput-object v1, p0, Lcom/google/t/c/g;->s:Lcom/google/t/c/f;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/t/c/g;->t:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/t/c/g;->u:[I

    iput-object v1, p0, Lcom/google/t/c/g;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/t/c/g;->cachedSize:I

    .line 1333
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 1609
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1610
    iget v2, p0, Lcom/google/t/c/g;->a:I

    if-eqz v2, :cond_0

    .line 1611
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/t/c/g;->a:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1614
    :cond_0
    iget v2, p0, Lcom/google/t/c/g;->b:I

    if-eqz v2, :cond_1

    .line 1615
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/t/c/g;->b:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1618
    :cond_1
    iget-wide v2, p0, Lcom/google/t/c/g;->c:J

    cmp-long v2, v2, v8

    if-eqz v2, :cond_2

    .line 1619
    const/4 v2, 0x3

    iget-wide v4, p0, Lcom/google/t/c/g;->c:J

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1622
    :cond_2
    iget-object v2, p0, Lcom/google/t/c/g;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1623
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/t/c/g;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1626
    :cond_3
    iget-object v2, p0, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    if-eqz v2, :cond_4

    .line 1627
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1630
    :cond_4
    iget-object v2, p0, Lcom/google/t/c/g;->f:Lcom/google/t/c/d;

    if-eqz v2, :cond_5

    .line 1631
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/t/c/g;->f:Lcom/google/t/c/d;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1634
    :cond_5
    iget v2, p0, Lcom/google/t/c/g;->h:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_6

    .line 1636
    const/4 v2, 0x7

    iget v3, p0, Lcom/google/t/c/g;->h:F

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 1639
    :cond_6
    iget v2, p0, Lcom/google/t/c/g;->i:I

    const/16 v3, 0x64

    if-eq v2, v3, :cond_7

    .line 1640
    const/16 v2, 0x8

    iget v3, p0, Lcom/google/t/c/g;->i:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1643
    :cond_7
    iget v2, p0, Lcom/google/t/c/g;->o:I

    if-eqz v2, :cond_8

    .line 1644
    const/16 v2, 0x9

    iget v3, p0, Lcom/google/t/c/g;->o:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1647
    :cond_8
    iget-object v2, p0, Lcom/google/t/c/g;->j:Lcom/google/t/c/b;

    if-eqz v2, :cond_9

    .line 1648
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/t/c/g;->j:Lcom/google/t/c/b;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1651
    :cond_9
    iget-object v2, p0, Lcom/google/t/c/g;->n:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 1652
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/t/c/g;->n:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1655
    :cond_a
    iget v2, p0, Lcom/google/t/c/g;->p:I

    if-eqz v2, :cond_b

    .line 1656
    const/16 v2, 0xc

    iget v3, p0, Lcom/google/t/c/g;->p:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1659
    :cond_b
    iget v2, p0, Lcom/google/t/c/g;->q:I

    if-eqz v2, :cond_c

    .line 1660
    const/16 v2, 0xd

    iget v3, p0, Lcom/google/t/c/g;->q:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1663
    :cond_c
    iget-object v2, p0, Lcom/google/t/c/g;->g:Lcom/google/t/c/e;

    if-eqz v2, :cond_d

    .line 1664
    const/16 v2, 0xe

    iget-object v3, p0, Lcom/google/t/c/g;->g:Lcom/google/t/c/e;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1667
    :cond_d
    iget v2, p0, Lcom/google/t/c/g;->r:I

    if-eqz v2, :cond_e

    .line 1668
    const/16 v2, 0xf

    iget v3, p0, Lcom/google/t/c/g;->r:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1671
    :cond_e
    iget-wide v2, p0, Lcom/google/t/c/g;->k:J

    cmp-long v2, v2, v8

    if-eqz v2, :cond_f

    .line 1672
    const/16 v2, 0x10

    iget-wide v4, p0, Lcom/google/t/c/g;->k:J

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1675
    :cond_f
    iget-object v2, p0, Lcom/google/t/c/g;->l:Lcom/google/t/c/b;

    if-eqz v2, :cond_10

    .line 1676
    const/16 v2, 0x11

    iget-object v3, p0, Lcom/google/t/c/g;->l:Lcom/google/t/c/b;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1679
    :cond_10
    iget v2, p0, Lcom/google/t/c/g;->m:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    invoke-static {v6}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_11

    .line 1681
    const/16 v2, 0x12

    iget v3, p0, Lcom/google/t/c/g;->m:F

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x4

    add-int/2addr v0, v2

    .line 1684
    :cond_11
    iget-object v2, p0, Lcom/google/t/c/g;->s:Lcom/google/t/c/f;

    if-eqz v2, :cond_12

    .line 1685
    const/16 v2, 0x13

    iget-object v3, p0, Lcom/google/t/c/g;->s:Lcom/google/t/c/f;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1688
    :cond_12
    iget-object v2, p0, Lcom/google/t/c/g;->t:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    .line 1689
    const/16 v2, 0x14

    iget-object v3, p0, Lcom/google/t/c/g;->t:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1692
    :cond_13
    iget-object v2, p0, Lcom/google/t/c/g;->u:[I

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/google/t/c/g;->u:[I

    array-length v2, v2

    if-lez v2, :cond_15

    move v2, v1

    .line 1694
    :goto_0
    iget-object v3, p0, Lcom/google/t/c/g;->u:[I

    array-length v3, v3

    if-ge v1, v3, :cond_14

    .line 1695
    iget-object v3, p0, Lcom/google/t/c/g;->u:[I

    aget v3, v3, v1

    .line 1696
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 1694
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1699
    :cond_14
    add-int/2addr v0, v2

    .line 1700
    iget-object v1, p0, Lcom/google/t/c/g;->u:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    .line 1702
    :cond_15
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1364
    if-ne p1, p0, :cond_1

    .line 1365
    const/4 v0, 0x1

    .line 1489
    :cond_0
    :goto_0
    return v0

    .line 1367
    :cond_1
    instance-of v1, p1, Lcom/google/t/c/g;

    if-eqz v1, :cond_0

    .line 1370
    check-cast p1, Lcom/google/t/c/g;

    .line 1371
    iget v1, p0, Lcom/google/t/c/g;->a:I

    iget v2, p1, Lcom/google/t/c/g;->a:I

    if-ne v1, v2, :cond_0

    .line 1374
    iget v1, p0, Lcom/google/t/c/g;->b:I

    iget v2, p1, Lcom/google/t/c/g;->b:I

    if-ne v1, v2, :cond_0

    .line 1377
    iget-wide v2, p0, Lcom/google/t/c/g;->c:J

    iget-wide v4, p1, Lcom/google/t/c/g;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1380
    iget-object v1, p0, Lcom/google/t/c/g;->d:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 1381
    iget-object v1, p1, Lcom/google/t/c/g;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1387
    :cond_2
    iget-object v1, p0, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    if-nez v1, :cond_c

    .line 1388
    iget-object v1, p1, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    if-nez v1, :cond_0

    .line 1396
    :cond_3
    iget-object v1, p0, Lcom/google/t/c/g;->f:Lcom/google/t/c/d;

    if-nez v1, :cond_d

    .line 1397
    iget-object v1, p1, Lcom/google/t/c/g;->f:Lcom/google/t/c/d;

    if-nez v1, :cond_0

    .line 1405
    :cond_4
    iget-object v1, p0, Lcom/google/t/c/g;->g:Lcom/google/t/c/e;

    if-nez v1, :cond_e

    .line 1406
    iget-object v1, p1, Lcom/google/t/c/g;->g:Lcom/google/t/c/e;

    if-nez v1, :cond_0

    .line 1415
    :cond_5
    iget v1, p0, Lcom/google/t/c/g;->h:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    .line 1416
    iget v2, p1, Lcom/google/t/c/g;->h:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 1420
    iget v1, p0, Lcom/google/t/c/g;->i:I

    iget v2, p1, Lcom/google/t/c/g;->i:I

    if-ne v1, v2, :cond_0

    .line 1423
    iget-object v1, p0, Lcom/google/t/c/g;->j:Lcom/google/t/c/b;

    if-nez v1, :cond_f

    .line 1424
    iget-object v1, p1, Lcom/google/t/c/g;->j:Lcom/google/t/c/b;

    if-nez v1, :cond_0

    .line 1432
    :cond_6
    iget-wide v2, p0, Lcom/google/t/c/g;->k:J

    iget-wide v4, p1, Lcom/google/t/c/g;->k:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1435
    iget-object v1, p0, Lcom/google/t/c/g;->l:Lcom/google/t/c/b;

    if-nez v1, :cond_10

    .line 1436
    iget-object v1, p1, Lcom/google/t/c/g;->l:Lcom/google/t/c/b;

    if-nez v1, :cond_0

    .line 1445
    :cond_7
    iget v1, p0, Lcom/google/t/c/g;->m:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    .line 1446
    iget v2, p1, Lcom/google/t/c/g;->m:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 1450
    iget-object v1, p0, Lcom/google/t/c/g;->n:Ljava/lang/String;

    if-nez v1, :cond_11

    .line 1451
    iget-object v1, p1, Lcom/google/t/c/g;->n:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1457
    :cond_8
    iget v1, p0, Lcom/google/t/c/g;->o:I

    iget v2, p1, Lcom/google/t/c/g;->o:I

    if-ne v1, v2, :cond_0

    .line 1460
    iget v1, p0, Lcom/google/t/c/g;->p:I

    iget v2, p1, Lcom/google/t/c/g;->p:I

    if-ne v1, v2, :cond_0

    .line 1463
    iget v1, p0, Lcom/google/t/c/g;->q:I

    iget v2, p1, Lcom/google/t/c/g;->q:I

    if-ne v1, v2, :cond_0

    .line 1466
    iget v1, p0, Lcom/google/t/c/g;->r:I

    iget v2, p1, Lcom/google/t/c/g;->r:I

    if-ne v1, v2, :cond_0

    .line 1469
    iget-object v1, p0, Lcom/google/t/c/g;->s:Lcom/google/t/c/f;

    if-nez v1, :cond_12

    .line 1470
    iget-object v1, p1, Lcom/google/t/c/g;->s:Lcom/google/t/c/f;

    if-nez v1, :cond_0

    .line 1478
    :cond_9
    iget-object v1, p0, Lcom/google/t/c/g;->t:Ljava/lang/String;

    if-nez v1, :cond_13

    .line 1479
    iget-object v1, p1, Lcom/google/t/c/g;->t:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1485
    :cond_a
    iget-object v1, p0, Lcom/google/t/c/g;->u:[I

    iget-object v2, p1, Lcom/google/t/c/g;->u:[I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1489
    invoke-virtual {p0, p1}, Lcom/google/t/c/g;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto/16 :goto_0

    .line 1384
    :cond_b
    iget-object v1, p0, Lcom/google/t/c/g;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/t/c/g;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0

    .line 1392
    :cond_c
    iget-object v1, p0, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    iget-object v2, p1, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    invoke-virtual {v1, v2}, Lcom/google/t/c/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 1401
    :cond_d
    iget-object v1, p0, Lcom/google/t/c/g;->f:Lcom/google/t/c/d;

    iget-object v2, p1, Lcom/google/t/c/g;->f:Lcom/google/t/c/d;

    invoke-virtual {v1, v2}, Lcom/google/t/c/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 1410
    :cond_e
    iget-object v1, p0, Lcom/google/t/c/g;->g:Lcom/google/t/c/e;

    iget-object v2, p1, Lcom/google/t/c/g;->g:Lcom/google/t/c/e;

    invoke-virtual {v1, v2}, Lcom/google/t/c/e;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 1428
    :cond_f
    iget-object v1, p0, Lcom/google/t/c/g;->j:Lcom/google/t/c/b;

    iget-object v2, p1, Lcom/google/t/c/g;->j:Lcom/google/t/c/b;

    invoke-virtual {v1, v2}, Lcom/google/t/c/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 1440
    :cond_10
    iget-object v1, p0, Lcom/google/t/c/g;->l:Lcom/google/t/c/b;

    iget-object v2, p1, Lcom/google/t/c/g;->l:Lcom/google/t/c/b;

    invoke-virtual {v1, v2}, Lcom/google/t/c/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 1454
    :cond_11
    iget-object v1, p0, Lcom/google/t/c/g;->n:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/t/c/g;->n:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 1474
    :cond_12
    iget-object v1, p0, Lcom/google/t/c/g;->s:Lcom/google/t/c/f;

    iget-object v2, p1, Lcom/google/t/c/g;->s:Lcom/google/t/c/f;

    invoke-virtual {v1, v2}, Lcom/google/t/c/f;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 1482
    :cond_13
    iget-object v1, p0, Lcom/google/t/c/g;->t:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/t/c/g;->t:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 1494
    iget v0, p0, Lcom/google/t/c/g;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 1496
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/t/c/g;->b:I

    add-int/2addr v0, v2

    .line 1497
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/t/c/g;->c:J

    iget-wide v4, p0, Lcom/google/t/c/g;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 1499
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/t/c/g;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 1501
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1503
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/t/c/g;->f:Lcom/google/t/c/d;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1505
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/t/c/g;->g:Lcom/google/t/c/e;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 1507
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/t/c/g;->h:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 1509
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/t/c/g;->i:I

    add-int/2addr v0, v2

    .line 1510
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/t/c/g;->j:Lcom/google/t/c/b;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 1512
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/t/c/g;->k:J

    iget-wide v4, p0, Lcom/google/t/c/g;->k:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 1514
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/t/c/g;->l:Lcom/google/t/c/b;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 1516
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/t/c/g;->m:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    add-int/2addr v0, v2

    .line 1518
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/t/c/g;->n:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 1520
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/t/c/g;->o:I

    add-int/2addr v0, v2

    .line 1521
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/t/c/g;->p:I

    add-int/2addr v0, v2

    .line 1522
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/t/c/g;->q:I

    add-int/2addr v0, v2

    .line 1523
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/t/c/g;->r:I

    add-int/2addr v0, v2

    .line 1524
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/t/c/g;->s:Lcom/google/t/c/f;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 1526
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/t/c/g;->t:Ljava/lang/String;

    if-nez v2, :cond_8

    :goto_8
    add-int/2addr v0, v1

    .line 1528
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/t/c/g;->u:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1530
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/t/c/g;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1531
    return v0

    .line 1499
    :cond_0
    iget-object v0, p0, Lcom/google/t/c/g;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 1501
    :cond_1
    iget-object v0, p0, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    invoke-virtual {v0}, Lcom/google/t/c/d;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 1503
    :cond_2
    iget-object v0, p0, Lcom/google/t/c/g;->f:Lcom/google/t/c/d;

    invoke-virtual {v0}, Lcom/google/t/c/d;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 1505
    :cond_3
    iget-object v0, p0, Lcom/google/t/c/g;->g:Lcom/google/t/c/e;

    invoke-virtual {v0}, Lcom/google/t/c/e;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 1510
    :cond_4
    iget-object v0, p0, Lcom/google/t/c/g;->j:Lcom/google/t/c/b;

    invoke-virtual {v0}, Lcom/google/t/c/b;->hashCode()I

    move-result v0

    goto :goto_4

    .line 1514
    :cond_5
    iget-object v0, p0, Lcom/google/t/c/g;->l:Lcom/google/t/c/b;

    invoke-virtual {v0}, Lcom/google/t/c/b;->hashCode()I

    move-result v0

    goto :goto_5

    .line 1518
    :cond_6
    iget-object v0, p0, Lcom/google/t/c/g;->n:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 1524
    :cond_7
    iget-object v0, p0, Lcom/google/t/c/g;->s:Lcom/google/t/c/f;

    invoke-virtual {v0}, Lcom/google/t/c/f;->hashCode()I

    move-result v0

    goto :goto_7

    .line 1526
    :cond_8
    iget-object v1, p0, Lcom/google/t/c/g;->t:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1251
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/t/c/g;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/t/c/g;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/t/c/g;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/t/c/g;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/t/c/g;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/t/c/d;

    invoke-direct {v0}, Lcom/google/t/c/d;-><init>()V

    iput-object v0, p0, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    :cond_1
    iget-object v0, p0, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/t/c/g;->f:Lcom/google/t/c/d;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/t/c/d;

    invoke-direct {v0}, Lcom/google/t/c/d;-><init>()V

    iput-object v0, p0, Lcom/google/t/c/g;->f:Lcom/google/t/c/d;

    :cond_2
    iget-object v0, p0, Lcom/google/t/c/g;->f:Lcom/google/t/c/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/t/c/g;->h:F

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/t/c/g;->i:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    iput v0, p0, Lcom/google/t/c/g;->o:I

    goto :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/t/c/g;->j:Lcom/google/t/c/b;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/t/c/b;

    invoke-direct {v0}, Lcom/google/t/c/b;-><init>()V

    iput-object v0, p0, Lcom/google/t/c/g;->j:Lcom/google/t/c/b;

    :cond_3
    iget-object v0, p0, Lcom/google/t/c/g;->j:Lcom/google/t/c/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/t/c/g;->n:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_0

    :pswitch_3
    iput v0, p0, Lcom/google/t/c/g;->p:I

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_4

    goto/16 :goto_0

    :pswitch_4
    iput v0, p0, Lcom/google/t/c/g;->q:I

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lcom/google/t/c/g;->g:Lcom/google/t/c/e;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/t/c/e;

    invoke-direct {v0}, Lcom/google/t/c/e;-><init>()V

    iput-object v0, p0, Lcom/google/t/c/g;->g:Lcom/google/t/c/e;

    :cond_4
    iget-object v0, p0, Lcom/google/t/c/g;->g:Lcom/google/t/c/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/t/c/g;->r:I

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/t/c/g;->k:J

    goto/16 :goto_0

    :sswitch_11
    iget-object v0, p0, Lcom/google/t/c/g;->l:Lcom/google/t/c/b;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/t/c/b;

    invoke-direct {v0}, Lcom/google/t/c/b;-><init>()V

    iput-object v0, p0, Lcom/google/t/c/g;->l:Lcom/google/t/c/b;

    :cond_5
    iget-object v0, p0, Lcom/google/t/c/g;->l:Lcom/google/t/c/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Lcom/google/t/c/g;->m:F

    goto/16 :goto_0

    :sswitch_13
    iget-object v0, p0, Lcom/google/t/c/g;->s:Lcom/google/t/c/f;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/t/c/f;

    invoke-direct {v0}, Lcom/google/t/c/f;-><init>()V

    iput-object v0, p0, Lcom/google/t/c/g;->s:Lcom/google/t/c/f;

    :cond_6
    iget-object v0, p0, Lcom/google/t/c/g;->s:Lcom/google/t/c/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/t/c/g;->t:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_15
    const/16 v0, 0xa8

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_8

    if-eqz v3, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_5

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_5
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_8
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/t/c/g;->u:[I

    if-nez v0, :cond_9

    move v0, v2

    :goto_3
    if-nez v0, :cond_a

    array-length v3, v5

    if-ne v1, v3, :cond_a

    iput-object v5, p0, Lcom/google/t/c/g;->u:[I

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/google/t/c/g;->u:[I

    array-length v0, v0

    goto :goto_3

    :cond_a
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_b

    iget-object v4, p0, Lcom/google/t/c/g;->u:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/t/c/g;->u:[I

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_c

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_6

    goto :goto_4

    :pswitch_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_c
    if-eqz v0, :cond_10

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/t/c/g;->u:[I

    if-nez v1, :cond_e

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_d

    iget-object v0, p0, Lcom/google/t/c/g;->u:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_d
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_f

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_7

    goto :goto_6

    :pswitch_7
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_e
    iget-object v1, p0, Lcom/google/t/c/g;->u:[I

    array-length v1, v1

    goto :goto_5

    :cond_f
    iput-object v4, p0, Lcom/google/t/c/g;->u:[I

    :cond_10
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3d -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x72 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x8a -> :sswitch_11
        0x95 -> :sswitch_12
        0x9a -> :sswitch_13
        0xa2 -> :sswitch_14
        0xa8 -> :sswitch_15
        0xaa -> :sswitch_16
    .end sparse-switch

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch -0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch -0x1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x0
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_7
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    .line 1537
    iget v0, p0, Lcom/google/t/c/g;->a:I

    if-eqz v0, :cond_0

    .line 1538
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/t/c/g;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1540
    :cond_0
    iget v0, p0, Lcom/google/t/c/g;->b:I

    if-eqz v0, :cond_1

    .line 1541
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/t/c/g;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1543
    :cond_1
    iget-wide v0, p0, Lcom/google/t/c/g;->c:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_2

    .line 1544
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/t/c/g;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 1546
    :cond_2
    iget-object v0, p0, Lcom/google/t/c/g;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1547
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/t/c/g;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1549
    :cond_3
    iget-object v0, p0, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    if-eqz v0, :cond_4

    .line 1550
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1552
    :cond_4
    iget-object v0, p0, Lcom/google/t/c/g;->f:Lcom/google/t/c/d;

    if-eqz v0, :cond_5

    .line 1553
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/t/c/g;->f:Lcom/google/t/c/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1555
    :cond_5
    iget v0, p0, Lcom/google/t/c/g;->h:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    if-eq v0, v1, :cond_6

    .line 1557
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/t/c/g;->h:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 1559
    :cond_6
    iget v0, p0, Lcom/google/t/c/g;->i:I

    const/16 v1, 0x64

    if-eq v0, v1, :cond_7

    .line 1560
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/t/c/g;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1562
    :cond_7
    iget v0, p0, Lcom/google/t/c/g;->o:I

    if-eqz v0, :cond_8

    .line 1563
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/t/c/g;->o:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1565
    :cond_8
    iget-object v0, p0, Lcom/google/t/c/g;->j:Lcom/google/t/c/b;

    if-eqz v0, :cond_9

    .line 1566
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/t/c/g;->j:Lcom/google/t/c/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1568
    :cond_9
    iget-object v0, p0, Lcom/google/t/c/g;->n:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1569
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/t/c/g;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1571
    :cond_a
    iget v0, p0, Lcom/google/t/c/g;->p:I

    if-eqz v0, :cond_b

    .line 1572
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/t/c/g;->p:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1574
    :cond_b
    iget v0, p0, Lcom/google/t/c/g;->q:I

    if-eqz v0, :cond_c

    .line 1575
    const/16 v0, 0xd

    iget v1, p0, Lcom/google/t/c/g;->q:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1577
    :cond_c
    iget-object v0, p0, Lcom/google/t/c/g;->g:Lcom/google/t/c/e;

    if-eqz v0, :cond_d

    .line 1578
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/t/c/g;->g:Lcom/google/t/c/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1580
    :cond_d
    iget v0, p0, Lcom/google/t/c/g;->r:I

    if-eqz v0, :cond_e

    .line 1581
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/t/c/g;->r:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1583
    :cond_e
    iget-wide v0, p0, Lcom/google/t/c/g;->k:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_f

    .line 1584
    const/16 v0, 0x10

    iget-wide v2, p0, Lcom/google/t/c/g;->k:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 1586
    :cond_f
    iget-object v0, p0, Lcom/google/t/c/g;->l:Lcom/google/t/c/b;

    if-eqz v0, :cond_10

    .line 1587
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/t/c/g;->l:Lcom/google/t/c/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1589
    :cond_10
    iget v0, p0, Lcom/google/t/c/g;->m:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    invoke-static {v4}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    if-eq v0, v1, :cond_11

    .line 1591
    const/16 v0, 0x12

    iget v1, p0, Lcom/google/t/c/g;->m:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 1593
    :cond_11
    iget-object v0, p0, Lcom/google/t/c/g;->s:Lcom/google/t/c/f;

    if-eqz v0, :cond_12

    .line 1594
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/t/c/g;->s:Lcom/google/t/c/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1596
    :cond_12
    iget-object v0, p0, Lcom/google/t/c/g;->t:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 1597
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/t/c/g;->t:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1599
    :cond_13
    iget-object v0, p0, Lcom/google/t/c/g;->u:[I

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/t/c/g;->u:[I

    array-length v0, v0

    if-lez v0, :cond_14

    .line 1600
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/t/c/g;->u:[I

    array-length v1, v1

    if-ge v0, v1, :cond_14

    .line 1601
    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/t/c/g;->u:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1600
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1604
    :cond_14
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1605
    return-void
.end method
