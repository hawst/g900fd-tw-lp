.class public final Lcom/google/t/c/f;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:D

.field public h:Lcom/google/t/c/c;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 1024
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1025
    iput v0, p0, Lcom/google/t/c/f;->a:I

    iput v0, p0, Lcom/google/t/c/f;->b:I

    iput v0, p0, Lcom/google/t/c/f;->c:I

    iput v0, p0, Lcom/google/t/c/f;->d:I

    iput v0, p0, Lcom/google/t/c/f;->e:I

    iput v0, p0, Lcom/google/t/c/f;->f:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/t/c/f;->g:D

    iput-object v2, p0, Lcom/google/t/c/f;->h:Lcom/google/t/c/c;

    iput-object v2, p0, Lcom/google/t/c/f;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/t/c/f;->cachedSize:I

    .line 1026
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 1139
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1140
    iget v1, p0, Lcom/google/t/c/f;->a:I

    if-eqz v1, :cond_0

    .line 1141
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/t/c/f;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1144
    :cond_0
    iget v1, p0, Lcom/google/t/c/f;->b:I

    if-eqz v1, :cond_1

    .line 1145
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/t/c/f;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1148
    :cond_1
    iget v1, p0, Lcom/google/t/c/f;->c:I

    if-eqz v1, :cond_2

    .line 1149
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/t/c/f;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1152
    :cond_2
    iget v1, p0, Lcom/google/t/c/f;->d:I

    if-eqz v1, :cond_3

    .line 1153
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/t/c/f;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1156
    :cond_3
    iget v1, p0, Lcom/google/t/c/f;->e:I

    if-eqz v1, :cond_4

    .line 1157
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/t/c/f;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1160
    :cond_4
    iget v1, p0, Lcom/google/t/c/f;->f:I

    if-eqz v1, :cond_5

    .line 1161
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/t/c/f;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1164
    :cond_5
    iget-wide v2, p0, Lcom/google/t/c/f;->g:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    const-wide/16 v4, 0x0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_6

    .line 1166
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/t/c/f;->g:D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 1169
    :cond_6
    iget-object v1, p0, Lcom/google/t/c/f;->h:Lcom/google/t/c/c;

    if-eqz v1, :cond_7

    .line 1170
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/t/c/f;->h:Lcom/google/t/c/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1173
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 1044
    if-ne p1, p0, :cond_1

    .line 1045
    const/4 v0, 0x1

    .line 1084
    :cond_0
    :goto_0
    return v0

    .line 1047
    :cond_1
    instance-of v1, p1, Lcom/google/t/c/f;

    if-eqz v1, :cond_0

    .line 1050
    check-cast p1, Lcom/google/t/c/f;

    .line 1051
    iget v1, p0, Lcom/google/t/c/f;->a:I

    iget v2, p1, Lcom/google/t/c/f;->a:I

    if-ne v1, v2, :cond_0

    .line 1054
    iget v1, p0, Lcom/google/t/c/f;->b:I

    iget v2, p1, Lcom/google/t/c/f;->b:I

    if-ne v1, v2, :cond_0

    .line 1057
    iget v1, p0, Lcom/google/t/c/f;->c:I

    iget v2, p1, Lcom/google/t/c/f;->c:I

    if-ne v1, v2, :cond_0

    .line 1060
    iget v1, p0, Lcom/google/t/c/f;->d:I

    iget v2, p1, Lcom/google/t/c/f;->d:I

    if-ne v1, v2, :cond_0

    .line 1063
    iget v1, p0, Lcom/google/t/c/f;->e:I

    iget v2, p1, Lcom/google/t/c/f;->e:I

    if-ne v1, v2, :cond_0

    .line 1066
    iget v1, p0, Lcom/google/t/c/f;->f:I

    iget v2, p1, Lcom/google/t/c/f;->f:I

    if-ne v1, v2, :cond_0

    .line 1070
    iget-wide v2, p0, Lcom/google/t/c/f;->g:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 1071
    iget-wide v4, p1, Lcom/google/t/c/f;->g:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 1075
    iget-object v1, p0, Lcom/google/t/c/f;->h:Lcom/google/t/c/c;

    if-nez v1, :cond_3

    .line 1076
    iget-object v1, p1, Lcom/google/t/c/f;->h:Lcom/google/t/c/c;

    if-nez v1, :cond_0

    .line 1084
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/t/c/f;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1080
    :cond_3
    iget-object v1, p0, Lcom/google/t/c/f;->h:Lcom/google/t/c/c;

    iget-object v2, p1, Lcom/google/t/c/f;->h:Lcom/google/t/c/c;

    invoke-virtual {v1, v2}, Lcom/google/t/c/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 1089
    iget v0, p0, Lcom/google/t/c/f;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 1091
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/t/c/f;->b:I

    add-int/2addr v0, v1

    .line 1092
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/t/c/f;->c:I

    add-int/2addr v0, v1

    .line 1093
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/t/c/f;->d:I

    add-int/2addr v0, v1

    .line 1094
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/t/c/f;->e:I

    add-int/2addr v0, v1

    .line 1095
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/t/c/f;->f:I

    add-int/2addr v0, v1

    .line 1097
    iget-wide v2, p0, Lcom/google/t/c/f;->g:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 1098
    mul-int/lit8 v0, v0, 0x1f

    const/16 v1, 0x20

    ushr-long v4, v2, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 1100
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/t/c/f;->h:Lcom/google/t/c/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 1102
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/t/c/f;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1103
    return v0

    .line 1100
    :cond_0
    iget-object v0, p0, Lcom/google/t/c/f;->h:Lcom/google/t/c/c;

    invoke-virtual {v0}, Lcom/google/t/c/c;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 974
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/t/c/f;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/t/c/f;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/t/c/f;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/t/c/f;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/t/c/f;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/t/c/f;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/t/c/f;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/t/c/f;->g:D

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/t/c/f;->h:Lcom/google/t/c/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/t/c/c;

    invoke-direct {v0}, Lcom/google/t/c/c;-><init>()V

    iput-object v0, p0, Lcom/google/t/c/f;->h:Lcom/google/t/c/c;

    :cond_1
    iget-object v0, p0, Lcom/google/t/c/f;->h:Lcom/google/t/c/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x39 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 1109
    iget v0, p0, Lcom/google/t/c/f;->a:I

    if-eqz v0, :cond_0

    .line 1110
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/t/c/f;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1112
    :cond_0
    iget v0, p0, Lcom/google/t/c/f;->b:I

    if-eqz v0, :cond_1

    .line 1113
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/t/c/f;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1115
    :cond_1
    iget v0, p0, Lcom/google/t/c/f;->c:I

    if-eqz v0, :cond_2

    .line 1116
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/t/c/f;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1118
    :cond_2
    iget v0, p0, Lcom/google/t/c/f;->d:I

    if-eqz v0, :cond_3

    .line 1119
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/t/c/f;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1121
    :cond_3
    iget v0, p0, Lcom/google/t/c/f;->e:I

    if-eqz v0, :cond_4

    .line 1122
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/t/c/f;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1124
    :cond_4
    iget v0, p0, Lcom/google/t/c/f;->f:I

    if-eqz v0, :cond_5

    .line 1125
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/t/c/f;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1127
    :cond_5
    iget-wide v0, p0, Lcom/google/t/c/f;->g:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_6

    .line 1129
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/t/c/f;->g:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 1131
    :cond_6
    iget-object v0, p0, Lcom/google/t/c/f;->h:Lcom/google/t/c/c;

    if-eqz v0, :cond_7

    .line 1132
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/t/c/f;->h:Lcom/google/t/c/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1134
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1135
    return-void
.end method
