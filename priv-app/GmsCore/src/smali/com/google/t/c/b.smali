.class public final Lcom/google/t/c/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 868
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 869
    iput-wide v0, p0, Lcom/google/t/c/b;->a:J

    iput-wide v0, p0, Lcom/google/t/c/b;->b:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/t/c/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/t/c/b;->cachedSize:I

    .line 870
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 923
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 924
    iget-wide v2, p0, Lcom/google/t/c/b;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 925
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/t/c/b;->a:J

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 928
    :cond_0
    iget-wide v2, p0, Lcom/google/t/c/b;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 929
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/t/c/b;->b:J

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 932
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 882
    if-ne p1, p0, :cond_1

    .line 883
    const/4 v0, 0x1

    .line 895
    :cond_0
    :goto_0
    return v0

    .line 885
    :cond_1
    instance-of v1, p1, Lcom/google/t/c/b;

    if-eqz v1, :cond_0

    .line 888
    check-cast p1, Lcom/google/t/c/b;

    .line 889
    iget-wide v2, p0, Lcom/google/t/c/b;->a:J

    iget-wide v4, p1, Lcom/google/t/c/b;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 892
    iget-wide v2, p0, Lcom/google/t/c/b;->b:J

    iget-wide v4, p1, Lcom/google/t/c/b;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 895
    invoke-virtual {p0, p1}, Lcom/google/t/c/b;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 900
    iget-wide v0, p0, Lcom/google/t/c/b;->a:J

    iget-wide v2, p0, Lcom/google/t/c/b;->a:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 903
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/t/c/b;->b:J

    iget-wide v4, p0, Lcom/google/t/c/b;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 905
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/t/c/b;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 906
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 845
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/t/c/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/t/c/b;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/t/c/b;->b:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 912
    iget-wide v0, p0, Lcom/google/t/c/b;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 913
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/t/c/b;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 915
    :cond_0
    iget-wide v0, p0, Lcom/google/t/c/b;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 916
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/t/c/b;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 918
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 919
    return-void
.end method
