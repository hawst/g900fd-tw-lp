.class public final Lcom/google/l/a/a/e;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field public d:I

.field public e:Z

.field public f:I

.field public g:Lcom/google/l/a/a/f;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 192
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 193
    const-string v0, ""

    iput-object v0, p0, Lcom/google/l/a/a/e;->a:Ljava/lang/String;

    iput v1, p0, Lcom/google/l/a/a/e;->b:I

    iput v1, p0, Lcom/google/l/a/a/e;->c:I

    iput v1, p0, Lcom/google/l/a/a/e;->d:I

    iput-boolean v1, p0, Lcom/google/l/a/a/e;->e:Z

    iput v1, p0, Lcom/google/l/a/a/e;->f:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/l/a/a/e;->cachedSize:I

    .line 194
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 295
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 296
    iget-object v1, p0, Lcom/google/l/a/a/e;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 297
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/l/a/a/e;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 300
    :cond_0
    iget v1, p0, Lcom/google/l/a/a/e;->b:I

    if-eqz v1, :cond_1

    .line 301
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/l/a/a/e;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 304
    :cond_1
    iget v1, p0, Lcom/google/l/a/a/e;->c:I

    if-eqz v1, :cond_2

    .line 305
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/l/a/a/e;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 308
    :cond_2
    iget v1, p0, Lcom/google/l/a/a/e;->d:I

    if-eqz v1, :cond_3

    .line 309
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/l/a/a/e;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 312
    :cond_3
    iget-boolean v1, p0, Lcom/google/l/a/a/e;->e:Z

    if-eqz v1, :cond_4

    .line 313
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/l/a/a/e;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 316
    :cond_4
    iget v1, p0, Lcom/google/l/a/a/e;->f:I

    if-eqz v1, :cond_5

    .line 317
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/l/a/a/e;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 320
    :cond_5
    iget-object v1, p0, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    if-eqz v1, :cond_6

    .line 321
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 324
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 210
    if-ne p1, p0, :cond_1

    .line 248
    :cond_0
    :goto_0
    return v0

    .line 213
    :cond_1
    instance-of v2, p1, Lcom/google/l/a/a/e;

    if-nez v2, :cond_2

    move v0, v1

    .line 214
    goto :goto_0

    .line 216
    :cond_2
    check-cast p1, Lcom/google/l/a/a/e;

    .line 217
    iget-object v2, p0, Lcom/google/l/a/a/e;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 218
    iget-object v2, p1, Lcom/google/l/a/a/e;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 219
    goto :goto_0

    .line 221
    :cond_3
    iget-object v2, p0, Lcom/google/l/a/a/e;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/l/a/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 222
    goto :goto_0

    .line 224
    :cond_4
    iget v2, p0, Lcom/google/l/a/a/e;->b:I

    iget v3, p1, Lcom/google/l/a/a/e;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 225
    goto :goto_0

    .line 227
    :cond_5
    iget v2, p0, Lcom/google/l/a/a/e;->c:I

    iget v3, p1, Lcom/google/l/a/a/e;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 228
    goto :goto_0

    .line 230
    :cond_6
    iget v2, p0, Lcom/google/l/a/a/e;->d:I

    iget v3, p1, Lcom/google/l/a/a/e;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 231
    goto :goto_0

    .line 233
    :cond_7
    iget-boolean v2, p0, Lcom/google/l/a/a/e;->e:Z

    iget-boolean v3, p1, Lcom/google/l/a/a/e;->e:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 234
    goto :goto_0

    .line 236
    :cond_8
    iget v2, p0, Lcom/google/l/a/a/e;->f:I

    iget v3, p1, Lcom/google/l/a/a/e;->f:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 237
    goto :goto_0

    .line 239
    :cond_9
    iget-object v2, p0, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    if-nez v2, :cond_a

    .line 240
    iget-object v2, p1, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    if-eqz v2, :cond_0

    move v0, v1

    .line 241
    goto :goto_0

    .line 244
    :cond_a
    iget-object v2, p0, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    iget-object v3, p1, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    invoke-virtual {v2, v3}, Lcom/google/l/a/a/f;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 245
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 253
    iget-object v0, p0, Lcom/google/l/a/a/e;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 256
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/l/a/a/e;->b:I

    add-int/2addr v0, v2

    .line 257
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/l/a/a/e;->c:I

    add-int/2addr v0, v2

    .line 258
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/l/a/a/e;->d:I

    add-int/2addr v0, v2

    .line 259
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/l/a/a/e;->e:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v2

    .line 260
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/l/a/a/e;->f:I

    add-int/2addr v0, v2

    .line 261
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 263
    return v0

    .line 253
    :cond_0
    iget-object v0, p0, Lcom/google/l/a/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 259
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1

    .line 261
    :cond_2
    iget-object v1, p0, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    invoke-virtual {v1}, Lcom/google/l/a/a/f;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/l/a/a/e;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/l/a/a/e;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/l/a/a/e;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/l/a/a/e;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/l/a/a/e;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/l/a/a/e;->f:I

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/l/a/a/f;

    invoke-direct {v0}, Lcom/google/l/a/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    :cond_1
    iget-object v0, p0, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/l/a/a/e;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/l/a/a/e;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 272
    :cond_0
    iget v0, p0, Lcom/google/l/a/a/e;->b:I

    if-eqz v0, :cond_1

    .line 273
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/l/a/a/e;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 275
    :cond_1
    iget v0, p0, Lcom/google/l/a/a/e;->c:I

    if-eqz v0, :cond_2

    .line 276
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/l/a/a/e;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 278
    :cond_2
    iget v0, p0, Lcom/google/l/a/a/e;->d:I

    if-eqz v0, :cond_3

    .line 279
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/l/a/a/e;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 281
    :cond_3
    iget-boolean v0, p0, Lcom/google/l/a/a/e;->e:Z

    if-eqz v0, :cond_4

    .line 282
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/l/a/a/e;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 284
    :cond_4
    iget v0, p0, Lcom/google/l/a/a/e;->f:I

    if-eqz v0, :cond_5

    .line 285
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/l/a/a/e;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 287
    :cond_5
    iget-object v0, p0, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    if-eqz v0, :cond_6

    .line 288
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/l/a/a/e;->g:Lcom/google/l/a/a/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 290
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 291
    return-void
.end method
