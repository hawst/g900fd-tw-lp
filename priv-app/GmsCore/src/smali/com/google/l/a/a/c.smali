.class public final Lcom/google/l/a/a/c;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 267
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 268
    const-string v0, ""

    iput-object v0, p0, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/l/a/a/c;->b:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/l/a/a/c;->cachedSize:I

    .line 269
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 324
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 325
    iget-object v1, p0, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 326
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 329
    :cond_0
    iget-wide v2, p0, Lcom/google/l/a/a/c;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 330
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/l/a/a/c;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 333
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 280
    if-ne p1, p0, :cond_1

    .line 297
    :cond_0
    :goto_0
    return v0

    .line 283
    :cond_1
    instance-of v2, p1, Lcom/google/l/a/a/c;

    if-nez v2, :cond_2

    move v0, v1

    .line 284
    goto :goto_0

    .line 286
    :cond_2
    check-cast p1, Lcom/google/l/a/a/c;

    .line 287
    iget-object v2, p0, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 288
    iget-object v2, p1, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 289
    goto :goto_0

    .line 291
    :cond_3
    iget-object v2, p0, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 292
    goto :goto_0

    .line 294
    :cond_4
    iget-wide v2, p0, Lcom/google/l/a/a/c;->b:J

    iget-wide v4, p1, Lcom/google/l/a/a/c;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 295
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 302
    iget-object v0, p0, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 305
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/l/a/a/c;->b:J

    iget-wide v4, p0, Lcom/google/l/a/a/c;->b:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 307
    return v0

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 244
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/l/a/a/c;->b:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 314
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/l/a/a/c;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 316
    :cond_0
    iget-wide v0, p0, Lcom/google/l/a/a/c;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 317
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/l/a/a/c;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 319
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 320
    return-void
.end method
