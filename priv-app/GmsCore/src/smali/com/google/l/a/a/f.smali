.class public final Lcom/google/l/a/a/f;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Z

.field public c:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 37
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 38
    iput-wide v2, p0, Lcom/google/l/a/a/f;->a:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/l/a/a/f;->b:Z

    iput-wide v2, p0, Lcom/google/l/a/a/f;->c:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/l/a/a/f;->cachedSize:I

    .line 39
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 98
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 99
    iget-wide v2, p0, Lcom/google/l/a/a/f;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 100
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/l/a/a/f;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_0
    iget-boolean v1, p0, Lcom/google/l/a/a/f;->b:Z

    if-eqz v1, :cond_1

    .line 104
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/l/a/a/f;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 107
    :cond_1
    iget-wide v2, p0, Lcom/google/l/a/a/f;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 108
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/l/a/a/f;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 111
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 51
    if-ne p1, p0, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    instance-of v2, p1, Lcom/google/l/a/a/f;

    if-nez v2, :cond_2

    move v0, v1

    .line 55
    goto :goto_0

    .line 57
    :cond_2
    check-cast p1, Lcom/google/l/a/a/f;

    .line 58
    iget-wide v2, p0, Lcom/google/l/a/a/f;->a:J

    iget-wide v4, p1, Lcom/google/l/a/a/f;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 59
    goto :goto_0

    .line 61
    :cond_3
    iget-boolean v2, p0, Lcom/google/l/a/a/f;->b:Z

    iget-boolean v3, p1, Lcom/google/l/a/a/f;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 62
    goto :goto_0

    .line 64
    :cond_4
    iget-wide v2, p0, Lcom/google/l/a/a/f;->c:J

    iget-wide v4, p1, Lcom/google/l/a/a/f;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 65
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 72
    iget-wide v0, p0, Lcom/google/l/a/a/f;->a:J

    iget-wide v2, p0, Lcom/google/l/a/a/f;->a:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 75
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/l/a/a/f;->b:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 76
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/l/a/a/f;->c:J

    iget-wide v4, p0, Lcom/google/l/a/a/f;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 78
    return v0

    .line 75
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 11
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/l/a/a/f;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/l/a/a/f;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/l/a/a/f;->c:J

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 84
    iget-wide v0, p0, Lcom/google/l/a/a/f;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 85
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/l/a/a/f;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 87
    :cond_0
    iget-boolean v0, p0, Lcom/google/l/a/a/f;->b:Z

    if-eqz v0, :cond_1

    .line 88
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/l/a/a/f;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 90
    :cond_1
    iget-wide v0, p0, Lcom/google/l/a/a/f;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 91
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/l/a/a/f;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 93
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 94
    return-void
.end method
