.class public final Lcom/google/af/a/d/a/a/n;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1983
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1984
    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/n;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/n;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/n;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/n;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/n;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/n;->f:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/n;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/d/a/a/n;->cachedSize:I

    .line 1985
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2098
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2099
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2100
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/af/a/d/a/a/n;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2103
    :cond_0
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2104
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/af/a/d/a/a/n;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2107
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2108
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/af/a/d/a/a/n;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2111
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2112
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/af/a/d/a/a/n;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2115
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 2116
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/af/a/d/a/a/n;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2119
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2120
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/af/a/d/a/a/n;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2123
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2001
    if-ne p1, p0, :cond_1

    .line 2002
    const/4 v0, 0x1

    .line 2050
    :cond_0
    :goto_0
    return v0

    .line 2004
    :cond_1
    instance-of v1, p1, Lcom/google/af/a/d/a/a/n;

    if-eqz v1, :cond_0

    .line 2007
    check-cast p1, Lcom/google/af/a/d/a/a/n;

    .line 2008
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->a:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 2009
    iget-object v1, p1, Lcom/google/af/a/d/a/a/n;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2015
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->b:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 2016
    iget-object v1, p1, Lcom/google/af/a/d/a/a/n;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2022
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->c:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 2023
    iget-object v1, p1, Lcom/google/af/a/d/a/a/n;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2029
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->d:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 2030
    iget-object v1, p1, Lcom/google/af/a/d/a/a/n;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2036
    :cond_5
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->e:Ljava/lang/String;

    if-nez v1, :cond_c

    .line 2037
    iget-object v1, p1, Lcom/google/af/a/d/a/a/n;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2043
    :cond_6
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->f:Ljava/lang/String;

    if-nez v1, :cond_d

    .line 2044
    iget-object v1, p1, Lcom/google/af/a/d/a/a/n;->f:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2050
    :cond_7
    invoke-virtual {p0, p1}, Lcom/google/af/a/d/a/a/n;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2012
    :cond_8
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/n;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2019
    :cond_9
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/n;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 2026
    :cond_a
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/n;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 2033
    :cond_b
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/n;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 2040
    :cond_c
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/n;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 2047
    :cond_d
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/n;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2055
    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2058
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2060
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2062
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 2064
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 2066
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/d/a/a/n;->f:Ljava/lang/String;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 2068
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/af/a/d/a/a/n;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2069
    return v0

    .line 2055
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2058
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2060
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2062
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 2064
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 2066
    :cond_5
    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1948
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/af/a/d/a/a/n;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/n;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/n;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/n;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/n;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/n;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/n;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2075
    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2076
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2078
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2079
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2081
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2082
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2084
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2085
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2087
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2088
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2090
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/d/a/a/n;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2091
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/af/a/d/a/a/n;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2093
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2094
    return-void
.end method
