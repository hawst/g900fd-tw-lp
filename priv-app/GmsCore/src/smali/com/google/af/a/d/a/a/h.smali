.class public final Lcom/google/af/a/d/a/a/h;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[B

.field public b:[[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 845
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 846
    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/af/a/d/a/a/h;->a:[B

    sget-object v0, Lcom/google/protobuf/nano/m;->g:[[B

    iput-object v0, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/h;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/d/a/a/h;->cachedSize:I

    .line 847
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 905
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 906
    iget-object v2, p0, Lcom/google/af/a/d/a/a/h;->a:[B

    sget-object v3, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    .line 907
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/af/a/d/a/a/h;->a:[B

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v2

    add-int/2addr v0, v2

    .line 910
    :cond_0
    iget-object v2, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    move v3, v1

    .line 913
    :goto_0
    iget-object v4, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 914
    iget-object v4, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    aget-object v4, v4, v1

    .line 915
    if-eqz v4, :cond_1

    .line 916
    add-int/lit8 v3, v3, 0x1

    .line 917
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b([B)I

    move-result v4

    add-int/2addr v2, v4

    .line 913
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 921
    :cond_2
    add-int/2addr v0, v2

    .line 922
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 924
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 859
    if-ne p1, p0, :cond_1

    .line 860
    const/4 v0, 0x1

    .line 873
    :cond_0
    :goto_0
    return v0

    .line 862
    :cond_1
    instance-of v1, p1, Lcom/google/af/a/d/a/a/h;

    if-eqz v1, :cond_0

    .line 865
    check-cast p1, Lcom/google/af/a/d/a/a/h;

    .line 866
    iget-object v1, p0, Lcom/google/af/a/d/a/a/h;->a:[B

    iget-object v2, p1, Lcom/google/af/a/d/a/a/h;->a:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 869
    iget-object v1, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    iget-object v2, p1, Lcom/google/af/a/d/a/a/h;->b:[[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([[B[[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 873
    invoke-virtual {p0, p1}, Lcom/google/af/a/d/a/a/h;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 878
    iget-object v0, p0, Lcom/google/af/a/d/a/a/h;->a:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 880
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 882
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/af/a/d/a/a/h;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 883
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 822
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/af/a/d/a/a/h;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/h;->a:[B

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [[B

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 889
    iget-object v0, p0, Lcom/google/af/a/d/a/a/h;->a:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 890
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/af/a/d/a/a/h;->a:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 892
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    array-length v0, v0

    if-lez v0, :cond_2

    .line 893
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 894
    iget-object v1, p0, Lcom/google/af/a/d/a/a/h;->b:[[B

    aget-object v1, v1, v0

    .line 895
    if-eqz v1, :cond_1

    .line 896
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 893
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 900
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 901
    return-void
.end method
