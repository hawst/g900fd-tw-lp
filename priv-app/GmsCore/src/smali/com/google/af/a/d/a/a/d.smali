.class public final Lcom/google/af/a/d/a/a/d;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2619
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2620
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/af/a/d/a/a/d;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/d;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/d;->c:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/d;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/d/a/a/d;->cachedSize:I

    .line 2621
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2690
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2691
    iget v1, p0, Lcom/google/af/a/d/a/a/d;->a:I

    if-eqz v1, :cond_0

    .line 2692
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/af/a/d/a/a/d;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2695
    :cond_0
    iget-object v1, p0, Lcom/google/af/a/d/a/a/d;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2696
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/af/a/d/a/a/d;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2699
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/d/a/a/d;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2700
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/af/a/d/a/a/d;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2703
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2634
    if-ne p1, p0, :cond_1

    .line 2635
    const/4 v0, 0x1

    .line 2658
    :cond_0
    :goto_0
    return v0

    .line 2637
    :cond_1
    instance-of v1, p1, Lcom/google/af/a/d/a/a/d;

    if-eqz v1, :cond_0

    .line 2640
    check-cast p1, Lcom/google/af/a/d/a/a/d;

    .line 2641
    iget v1, p0, Lcom/google/af/a/d/a/a/d;->a:I

    iget v2, p1, Lcom/google/af/a/d/a/a/d;->a:I

    if-ne v1, v2, :cond_0

    .line 2644
    iget-object v1, p0, Lcom/google/af/a/d/a/a/d;->b:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 2645
    iget-object v1, p1, Lcom/google/af/a/d/a/a/d;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2651
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/d/a/a/d;->c:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 2652
    iget-object v1, p1, Lcom/google/af/a/d/a/a/d;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2658
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/af/a/d/a/a/d;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2648
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/d/a/a/d;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2655
    :cond_5
    iget-object v1, p0, Lcom/google/af/a/d/a/a/d;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/d;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2663
    iget v0, p0, Lcom/google/af/a/d/a/a/d;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 2665
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/d;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 2667
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/d/a/a/d;->c:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 2669
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/af/a/d/a/a/d;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2670
    return v0

    .line 2665
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/d/a/a/d;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2667
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/d/a/a/d;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2563
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/af/a/d/a/a/d;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/af/a/d/a/a/d;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/d;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/d;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2676
    iget v0, p0, Lcom/google/af/a/d/a/a/d;->a:I

    if-eqz v0, :cond_0

    .line 2677
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/af/a/d/a/a/d;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2679
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/d/a/a/d;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2680
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/af/a/d/a/a/d;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2682
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/d/a/a/d;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2683
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/af/a/d/a/a/d;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2685
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2686
    return-void
.end method
