.class public final Lcom/google/af/a/d/a/a/e;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Lcom/google/af/a/d/a/a/c;

.field public c:Lcom/google/af/a/d/a/a/f;

.field public d:Lcom/google/af/a/d/a/a/i;

.field public e:Lcom/google/af/a/d/a/a/n;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2223
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2224
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/af/a/d/a/a/e;->a:I

    iput-object v1, p0, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    iput-object v1, p0, Lcom/google/af/a/d/a/a/e;->c:Lcom/google/af/a/d/a/a/f;

    iput-object v1, p0, Lcom/google/af/a/d/a/a/e;->d:Lcom/google/af/a/d/a/a/i;

    iput-object v1, p0, Lcom/google/af/a/d/a/a/e;->e:Lcom/google/af/a/d/a/a/n;

    iput-object v1, p0, Lcom/google/af/a/d/a/a/e;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/d/a/a/e;->cachedSize:I

    .line 2225
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2328
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2329
    iget v1, p0, Lcom/google/af/a/d/a/a/e;->a:I

    if-eqz v1, :cond_0

    .line 2330
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/af/a/d/a/a/e;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2333
    :cond_0
    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    if-eqz v1, :cond_1

    .line 2334
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2337
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->c:Lcom/google/af/a/d/a/a/f;

    if-eqz v1, :cond_2

    .line 2338
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/af/a/d/a/a/e;->c:Lcom/google/af/a/d/a/a/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2341
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->d:Lcom/google/af/a/d/a/a/i;

    if-eqz v1, :cond_3

    .line 2342
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/af/a/d/a/a/e;->d:Lcom/google/af/a/d/a/a/i;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2345
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->e:Lcom/google/af/a/d/a/a/n;

    if-eqz v1, :cond_4

    .line 2346
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/af/a/d/a/a/e;->e:Lcom/google/af/a/d/a/a/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2349
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2240
    if-ne p1, p0, :cond_1

    .line 2241
    const/4 v0, 0x1

    .line 2286
    :cond_0
    :goto_0
    return v0

    .line 2243
    :cond_1
    instance-of v1, p1, Lcom/google/af/a/d/a/a/e;

    if-eqz v1, :cond_0

    .line 2246
    check-cast p1, Lcom/google/af/a/d/a/a/e;

    .line 2247
    iget v1, p0, Lcom/google/af/a/d/a/a/e;->a:I

    iget v2, p1, Lcom/google/af/a/d/a/a/e;->a:I

    if-ne v1, v2, :cond_0

    .line 2250
    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    if-nez v1, :cond_6

    .line 2251
    iget-object v1, p1, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    if-nez v1, :cond_0

    .line 2259
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->c:Lcom/google/af/a/d/a/a/f;

    if-nez v1, :cond_7

    .line 2260
    iget-object v1, p1, Lcom/google/af/a/d/a/a/e;->c:Lcom/google/af/a/d/a/a/f;

    if-nez v1, :cond_0

    .line 2268
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->d:Lcom/google/af/a/d/a/a/i;

    if-nez v1, :cond_8

    .line 2269
    iget-object v1, p1, Lcom/google/af/a/d/a/a/e;->d:Lcom/google/af/a/d/a/a/i;

    if-nez v1, :cond_0

    .line 2277
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->e:Lcom/google/af/a/d/a/a/n;

    if-nez v1, :cond_9

    .line 2278
    iget-object v1, p1, Lcom/google/af/a/d/a/a/e;->e:Lcom/google/af/a/d/a/a/n;

    if-nez v1, :cond_0

    .line 2286
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/af/a/d/a/a/e;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2255
    :cond_6
    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    invoke-virtual {v1, v2}, Lcom/google/af/a/d/a/a/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2264
    :cond_7
    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->c:Lcom/google/af/a/d/a/a/f;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/e;->c:Lcom/google/af/a/d/a/a/f;

    invoke-virtual {v1, v2}, Lcom/google/af/a/d/a/a/f;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 2273
    :cond_8
    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->d:Lcom/google/af/a/d/a/a/i;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/e;->d:Lcom/google/af/a/d/a/a/i;

    invoke-virtual {v1, v2}, Lcom/google/af/a/d/a/a/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 2282
    :cond_9
    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->e:Lcom/google/af/a/d/a/a/n;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/e;->e:Lcom/google/af/a/d/a/a/n;

    invoke-virtual {v1, v2}, Lcom/google/af/a/d/a/a/n;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2291
    iget v0, p0, Lcom/google/af/a/d/a/a/e;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 2293
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 2295
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->c:Lcom/google/af/a/d/a/a/f;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2297
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->d:Lcom/google/af/a/d/a/a/i;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2299
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/d/a/a/e;->e:Lcom/google/af/a/d/a/a/n;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 2301
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/af/a/d/a/a/e;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2302
    return v0

    .line 2293
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    invoke-virtual {v0}, Lcom/google/af/a/d/a/a/c;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2295
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->c:Lcom/google/af/a/d/a/a/f;

    invoke-virtual {v0}, Lcom/google/af/a/d/a/a/f;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2297
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->d:Lcom/google/af/a/d/a/a/i;

    invoke-virtual {v0}, Lcom/google/af/a/d/a/a/i;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2299
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->e:Lcom/google/af/a/d/a/a/n;

    invoke-virtual {v1}, Lcom/google/af/a/d/a/a/n;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2181
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/af/a/d/a/a/e;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/af/a/d/a/a/e;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/af/a/d/a/a/c;

    invoke-direct {v0}, Lcom/google/af/a/d/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    :cond_1
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->c:Lcom/google/af/a/d/a/a/f;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/af/a/d/a/a/f;

    invoke-direct {v0}, Lcom/google/af/a/d/a/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/d/a/a/e;->c:Lcom/google/af/a/d/a/a/f;

    :cond_2
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->c:Lcom/google/af/a/d/a/a/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->d:Lcom/google/af/a/d/a/a/i;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/af/a/d/a/a/i;

    invoke-direct {v0}, Lcom/google/af/a/d/a/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/d/a/a/e;->d:Lcom/google/af/a/d/a/a/i;

    :cond_3
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->d:Lcom/google/af/a/d/a/a/i;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->e:Lcom/google/af/a/d/a/a/n;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/af/a/d/a/a/n;

    invoke-direct {v0}, Lcom/google/af/a/d/a/a/n;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/d/a/a/e;->e:Lcom/google/af/a/d/a/a/n;

    :cond_4
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->e:Lcom/google/af/a/d/a/a/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2308
    iget v0, p0, Lcom/google/af/a/d/a/a/e;->a:I

    if-eqz v0, :cond_0

    .line 2309
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/af/a/d/a/a/e;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2311
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    if-eqz v0, :cond_1

    .line 2312
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->b:Lcom/google/af/a/d/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2314
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->c:Lcom/google/af/a/d/a/a/f;

    if-eqz v0, :cond_2

    .line 2315
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->c:Lcom/google/af/a/d/a/a/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2317
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->d:Lcom/google/af/a/d/a/a/i;

    if-eqz v0, :cond_3

    .line 2318
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->d:Lcom/google/af/a/d/a/a/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2320
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/d/a/a/e;->e:Lcom/google/af/a/d/a/a/n;

    if-eqz v0, :cond_4

    .line 2321
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/af/a/d/a/a/e;->e:Lcom/google/af/a/d/a/a/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2323
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2324
    return-void
.end method
