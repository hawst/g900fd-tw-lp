.class public final Lcom/google/af/a/d/a/a/j;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile o:[Lcom/google/af/a/d/a/a/j;


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:Z

.field public f:[Lcom/google/af/a/d/a/a/k;

.field public g:Lcom/google/af/a/d/a/a/d;

.field public h:[B

.field public i:[B

.field public j:[B

.field public k:Lcom/google/af/a/d/a/a/b;

.field public l:Ljava/lang/String;

.field public m:J

.field public n:Lcom/google/af/a/d/a/a/h;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 456
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 457
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/af/a/d/a/a/j;->a:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/j;->b:Ljava/lang/String;

    iput v3, p0, Lcom/google/af/a/d/a/a/j;->c:I

    iput v3, p0, Lcom/google/af/a/d/a/a/j;->d:I

    iput-boolean v3, p0, Lcom/google/af/a/d/a/a/j;->e:Z

    invoke-static {}, Lcom/google/af/a/d/a/a/k;->a()[Lcom/google/af/a/d/a/a/k;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    iput-object v2, p0, Lcom/google/af/a/d/a/a/j;->g:Lcom/google/af/a/d/a/a/d;

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/af/a/d/a/a/j;->h:[B

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/af/a/d/a/a/j;->i:[B

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/af/a/d/a/a/j;->j:[B

    iput-object v2, p0, Lcom/google/af/a/d/a/a/j;->k:Lcom/google/af/a/d/a/a/b;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/j;->l:Ljava/lang/String;

    const-wide/32 v0, 0x2bf20

    iput-wide v0, p0, Lcom/google/af/a/d/a/a/j;->m:J

    iput-object v2, p0, Lcom/google/af/a/d/a/a/j;->n:Lcom/google/af/a/d/a/a/h;

    iput-object v2, p0, Lcom/google/af/a/d/a/a/j;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/d/a/a/j;->cachedSize:I

    .line 458
    return-void
.end method

.method public static a()[Lcom/google/af/a/d/a/a/j;
    .locals 2

    .prologue
    .line 403
    sget-object v0, Lcom/google/af/a/d/a/a/j;->o:[Lcom/google/af/a/d/a/a/j;

    if-nez v0, :cond_1

    .line 404
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 406
    :try_start_0
    sget-object v0, Lcom/google/af/a/d/a/a/j;->o:[Lcom/google/af/a/d/a/a/j;

    if-nez v0, :cond_0

    .line 407
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/af/a/d/a/a/j;

    sput-object v0, Lcom/google/af/a/d/a/a/j;->o:[Lcom/google/af/a/d/a/a/j;

    .line 409
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 411
    :cond_1
    sget-object v0, Lcom/google/af/a/d/a/a/j;->o:[Lcom/google/af/a/d/a/a/j;

    return-object v0

    .line 409
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 7

    .prologue
    .line 645
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 646
    iget-wide v2, p0, Lcom/google/af/a/d/a/a/j;->a:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 647
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/af/a/d/a/a/j;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 650
    :cond_0
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 651
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 654
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 655
    const/4 v1, 0x0

    move v6, v1

    move v1, v0

    move v0, v6

    :goto_0
    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 656
    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    aget-object v2, v2, v0

    .line 657
    if-eqz v2, :cond_2

    .line 658
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 655
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 663
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->h:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_5

    .line 664
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->h:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 667
    :cond_5
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->k:Lcom/google/af/a/d/a/a/b;

    if-eqz v1, :cond_6

    .line 668
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->k:Lcom/google/af/a/d/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 671
    :cond_6
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->i:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_7

    .line 672
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->i:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 675
    :cond_7
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->g:Lcom/google/af/a/d/a/a/d;

    if-eqz v1, :cond_8

    .line 676
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->g:Lcom/google/af/a/d/a/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 679
    :cond_8
    iget-boolean v1, p0, Lcom/google/af/a/d/a/a/j;->e:Z

    if-eqz v1, :cond_9

    .line 680
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/af/a/d/a/a/j;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 683
    :cond_9
    iget v1, p0, Lcom/google/af/a/d/a/a/j;->c:I

    if-eqz v1, :cond_a

    .line 684
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/af/a/d/a/a/j;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 687
    :cond_a
    iget v1, p0, Lcom/google/af/a/d/a/a/j;->d:I

    if-eqz v1, :cond_b

    .line 688
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/af/a/d/a/a/j;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 691
    :cond_b
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->j:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_c

    .line 692
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->j:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 695
    :cond_c
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->l:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 696
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 699
    :cond_d
    iget-wide v2, p0, Lcom/google/af/a/d/a/a/j;->m:J

    const-wide/32 v4, 0x2bf20

    cmp-long v1, v2, v4

    if-eqz v1, :cond_e

    .line 700
    const/16 v1, 0xf

    iget-wide v2, p0, Lcom/google/af/a/d/a/a/j;->m:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->g(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 703
    :cond_e
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->n:Lcom/google/af/a/d/a/a/h;

    if-eqz v1, :cond_f

    .line 704
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->n:Lcom/google/af/a/d/a/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 707
    :cond_f
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 482
    if-ne p1, p0, :cond_1

    .line 483
    const/4 v0, 0x1

    .line 558
    :cond_0
    :goto_0
    return v0

    .line 485
    :cond_1
    instance-of v1, p1, Lcom/google/af/a/d/a/a/j;

    if-eqz v1, :cond_0

    .line 488
    check-cast p1, Lcom/google/af/a/d/a/a/j;

    .line 489
    iget-wide v2, p0, Lcom/google/af/a/d/a/a/j;->a:J

    iget-wide v4, p1, Lcom/google/af/a/d/a/a/j;->a:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 492
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->b:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 493
    iget-object v1, p1, Lcom/google/af/a/d/a/a/j;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 499
    :cond_2
    iget v1, p0, Lcom/google/af/a/d/a/a/j;->c:I

    iget v2, p1, Lcom/google/af/a/d/a/a/j;->c:I

    if-ne v1, v2, :cond_0

    .line 502
    iget v1, p0, Lcom/google/af/a/d/a/a/j;->d:I

    iget v2, p1, Lcom/google/af/a/d/a/a/j;->d:I

    if-ne v1, v2, :cond_0

    .line 505
    iget-boolean v1, p0, Lcom/google/af/a/d/a/a/j;->e:Z

    iget-boolean v2, p1, Lcom/google/af/a/d/a/a/j;->e:Z

    if-ne v1, v2, :cond_0

    .line 508
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 512
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->g:Lcom/google/af/a/d/a/a/d;

    if-nez v1, :cond_8

    .line 513
    iget-object v1, p1, Lcom/google/af/a/d/a/a/j;->g:Lcom/google/af/a/d/a/a/d;

    if-nez v1, :cond_0

    .line 521
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->h:[B

    iget-object v2, p1, Lcom/google/af/a/d/a/a/j;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 524
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->i:[B

    iget-object v2, p1, Lcom/google/af/a/d/a/a/j;->i:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 527
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->j:[B

    iget-object v2, p1, Lcom/google/af/a/d/a/a/j;->j:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 530
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->k:Lcom/google/af/a/d/a/a/b;

    if-nez v1, :cond_9

    .line 531
    iget-object v1, p1, Lcom/google/af/a/d/a/a/j;->k:Lcom/google/af/a/d/a/a/b;

    if-nez v1, :cond_0

    .line 539
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->l:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 540
    iget-object v1, p1, Lcom/google/af/a/d/a/a/j;->l:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 546
    :cond_5
    iget-wide v2, p0, Lcom/google/af/a/d/a/a/j;->m:J

    iget-wide v4, p1, Lcom/google/af/a/d/a/a/j;->m:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 549
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->n:Lcom/google/af/a/d/a/a/h;

    if-nez v1, :cond_b

    .line 550
    iget-object v1, p1, Lcom/google/af/a/d/a/a/j;->n:Lcom/google/af/a/d/a/a/h;

    if-nez v1, :cond_0

    .line 558
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/af/a/d/a/a/j;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 496
    :cond_7
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/j;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0

    .line 517
    :cond_8
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->g:Lcom/google/af/a/d/a/a/d;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/j;->g:Lcom/google/af/a/d/a/a/d;

    invoke-virtual {v1, v2}, Lcom/google/af/a/d/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 535
    :cond_9
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->k:Lcom/google/af/a/d/a/a/b;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/j;->k:Lcom/google/af/a/d/a/a/b;

    invoke-virtual {v1, v2}, Lcom/google/af/a/d/a/a/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 543
    :cond_a
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->l:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/j;->l:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 554
    :cond_b
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->n:Lcom/google/af/a/d/a/a/h;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/j;->n:Lcom/google/af/a/d/a/a/h;

    invoke-virtual {v1, v2}, Lcom/google/af/a/d/a/a/h;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 563
    iget-wide v2, p0, Lcom/google/af/a/d/a/a/j;->a:J

    iget-wide v4, p0, Lcom/google/af/a/d/a/a/j;->a:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit16 v0, v0, 0x20f

    .line 566
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 568
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/af/a/d/a/a/j;->c:I

    add-int/2addr v0, v2

    .line 569
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/af/a/d/a/a/j;->d:I

    add-int/2addr v0, v2

    .line 570
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/af/a/d/a/a/j;->e:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v2

    .line 571
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 573
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->g:Lcom/google/af/a/d/a/a/d;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 575
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->h:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 576
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->i:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 577
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->j:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 578
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->k:Lcom/google/af/a/d/a/a/b;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 580
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->l:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 582
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/af/a/d/a/a/j;->m:J

    iget-wide v4, p0, Lcom/google/af/a/d/a/a/j;->m:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 584
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/d/a/a/j;->n:Lcom/google/af/a/d/a/a/h;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 586
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/af/a/d/a/a/j;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 587
    return v0

    .line 566
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 570
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1

    .line 573
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->g:Lcom/google/af/a/d/a/a/d;

    invoke-virtual {v0}, Lcom/google/af/a/d/a/a/d;->hashCode()I

    move-result v0

    goto :goto_2

    .line 578
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->k:Lcom/google/af/a/d/a/a/b;

    invoke-virtual {v0}, Lcom/google/af/a/d/a/a/b;->hashCode()I

    move-result v0

    goto :goto_3

    .line 580
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->l:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 584
    :cond_5
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->n:Lcom/google/af/a/d/a/a/h;

    invoke-virtual {v1}, Lcom/google/af/a/d/a/a/h;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 397
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/af/a/d/a/a/j;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/af/a/d/a/a/j;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/j;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/af/a/d/a/a/k;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/af/a/d/a/a/k;

    invoke-direct {v3}, Lcom/google/af/a/d/a/a/k;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/af/a/d/a/a/k;

    invoke-direct {v3}, Lcom/google/af/a/d/a/a/k;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/j;->h:[B

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->k:Lcom/google/af/a/d/a/a/b;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/af/a/d/a/a/b;

    invoke-direct {v0}, Lcom/google/af/a/d/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/d/a/a/j;->k:Lcom/google/af/a/d/a/a/b;

    :cond_4
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->k:Lcom/google/af/a/d/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/j;->i:[B

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->g:Lcom/google/af/a/d/a/a/d;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/af/a/d/a/a/d;

    invoke-direct {v0}, Lcom/google/af/a/d/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/d/a/a/j;->g:Lcom/google/af/a/d/a/a/d;

    :cond_5
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->g:Lcom/google/af/a/d/a/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/af/a/d/a/a/j;->e:Z

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/d/a/a/j;->c:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/af/a/d/a/a/j;->d:I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/j;->j:[B

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/j;->l:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->h()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/af/a/d/a/a/j;->m:J

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->n:Lcom/google/af/a/d/a/a/h;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/af/a/d/a/a/h;

    invoke-direct {v0}, Lcom/google/af/a/d/a/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/af/a/d/a/a/j;->n:Lcom/google/af/a/d/a/a/h;

    :cond_6
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->n:Lcom/google/af/a/d/a/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x32 -> :sswitch_4
        0x3a -> :sswitch_5
        0x42 -> :sswitch_6
        0x4a -> :sswitch_7
        0x50 -> :sswitch_8
        0x58 -> :sswitch_9
        0x60 -> :sswitch_a
        0x6a -> :sswitch_b
        0x72 -> :sswitch_c
        0x78 -> :sswitch_d
        0x82 -> :sswitch_e
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 593
    iget-wide v0, p0, Lcom/google/af/a/d/a/a/j;->a:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 594
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/af/a/d/a/a/j;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 596
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 597
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 599
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 600
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 601
    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->f:[Lcom/google/af/a/d/a/a/k;

    aget-object v1, v1, v0

    .line 602
    if-eqz v1, :cond_2

    .line 603
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 600
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 607
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->h:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_4

    .line 608
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->h:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 610
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->k:Lcom/google/af/a/d/a/a/b;

    if-eqz v0, :cond_5

    .line 611
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->k:Lcom/google/af/a/d/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 613
    :cond_5
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->i:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_6

    .line 614
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->i:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 616
    :cond_6
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->g:Lcom/google/af/a/d/a/a/d;

    if-eqz v0, :cond_7

    .line 617
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->g:Lcom/google/af/a/d/a/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 619
    :cond_7
    iget-boolean v0, p0, Lcom/google/af/a/d/a/a/j;->e:Z

    if-eqz v0, :cond_8

    .line 620
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/af/a/d/a/a/j;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 622
    :cond_8
    iget v0, p0, Lcom/google/af/a/d/a/a/j;->c:I

    if-eqz v0, :cond_9

    .line 623
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/af/a/d/a/a/j;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 625
    :cond_9
    iget v0, p0, Lcom/google/af/a/d/a/a/j;->d:I

    if-eqz v0, :cond_a

    .line 626
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/af/a/d/a/a/j;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 628
    :cond_a
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->j:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_b

    .line 629
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->j:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 631
    :cond_b
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->l:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 632
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 634
    :cond_c
    iget-wide v0, p0, Lcom/google/af/a/d/a/a/j;->m:J

    const-wide/32 v2, 0x2bf20

    cmp-long v0, v0, v2

    if-eqz v0, :cond_d

    .line 635
    const/16 v0, 0xf

    iget-wide v2, p0, Lcom/google/af/a/d/a/a/j;->m:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->d(IJ)V

    .line 637
    :cond_d
    iget-object v0, p0, Lcom/google/af/a/d/a/a/j;->n:Lcom/google/af/a/d/a/a/h;

    if-eqz v0, :cond_e

    .line 638
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/af/a/d/a/a/j;->n:Lcom/google/af/a/d/a/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 640
    :cond_e
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 641
    return-void
.end method
