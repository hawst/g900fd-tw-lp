.class public final Lcom/google/af/a/d/a/a/f;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1541
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1542
    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/f;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/f;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/f;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/f;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/f;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/af/a/d/a/a/f;->f:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/f;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/af/a/d/a/a/f;->cachedSize:I

    .line 1543
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1656
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1657
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1658
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/af/a/d/a/a/f;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1661
    :cond_0
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1662
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/af/a/d/a/a/f;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1665
    :cond_1
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1666
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/af/a/d/a/a/f;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1669
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1670
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/af/a/d/a/a/f;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1673
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1674
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/af/a/d/a/a/f;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1677
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1678
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/af/a/d/a/a/f;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1681
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1559
    if-ne p1, p0, :cond_1

    .line 1560
    const/4 v0, 0x1

    .line 1608
    :cond_0
    :goto_0
    return v0

    .line 1562
    :cond_1
    instance-of v1, p1, Lcom/google/af/a/d/a/a/f;

    if-eqz v1, :cond_0

    .line 1565
    check-cast p1, Lcom/google/af/a/d/a/a/f;

    .line 1566
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->a:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 1567
    iget-object v1, p1, Lcom/google/af/a/d/a/a/f;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1573
    :cond_2
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->b:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 1574
    iget-object v1, p1, Lcom/google/af/a/d/a/a/f;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1580
    :cond_3
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->c:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 1581
    iget-object v1, p1, Lcom/google/af/a/d/a/a/f;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1587
    :cond_4
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->d:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 1588
    iget-object v1, p1, Lcom/google/af/a/d/a/a/f;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1594
    :cond_5
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->e:Ljava/lang/String;

    if-nez v1, :cond_c

    .line 1595
    iget-object v1, p1, Lcom/google/af/a/d/a/a/f;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1601
    :cond_6
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->f:Ljava/lang/String;

    if-nez v1, :cond_d

    .line 1602
    iget-object v1, p1, Lcom/google/af/a/d/a/a/f;->f:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1608
    :cond_7
    invoke-virtual {p0, p1}, Lcom/google/af/a/d/a/a/f;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1570
    :cond_8
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1577
    :cond_9
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/f;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 1584
    :cond_a
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/f;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 1591
    :cond_b
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/f;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 1598
    :cond_c
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/f;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 1605
    :cond_d
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/af/a/d/a/a/f;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1613
    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1616
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1618
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1620
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 1622
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 1624
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/af/a/d/a/a/f;->f:Ljava/lang/String;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 1626
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/af/a/d/a/a/f;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1627
    return v0

    .line 1613
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1616
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1618
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1620
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 1622
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 1624
    :cond_5
    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1506
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/af/a/d/a/a/f;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/f;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/f;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/f;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/f;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/f;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/af/a/d/a/a/f;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1633
    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1634
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1636
    :cond_0
    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1637
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1639
    :cond_1
    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1640
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1642
    :cond_2
    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1643
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1645
    :cond_3
    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1646
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1648
    :cond_4
    iget-object v0, p0, Lcom/google/af/a/d/a/a/f;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1649
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/af/a/d/a/a/f;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1651
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1652
    return-void
.end method
