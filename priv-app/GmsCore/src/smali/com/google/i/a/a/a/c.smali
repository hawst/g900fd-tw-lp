.class public final Lcom/google/i/a/a/a/c;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Lcom/google/protobuf/a/a;

.field private e:Z

.field private f:Z

.field private g:Z

.field private h:Z

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/a/a/c;->a:Ljava/lang/String;

    .line 30
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/a/a/c;->b:Ljava/lang/String;

    .line 47
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/i/a/a/a/c;->c:I

    .line 64
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/i/a/a/a/c;->d:Lcom/google/protobuf/a/a;

    .line 108
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/i/a/a/a/c;->i:I

    .line 8
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 111
    iget v0, p0, Lcom/google/i/a/a/a/c;->i:I

    if-gez v0, :cond_0

    .line 113
    invoke-virtual {p0}, Lcom/google/i/a/a/a/c;->b()I

    .line 115
    :cond_0
    iget v0, p0, Lcom/google/i/a/a/a/c;->i:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Lcom/google/i/a/a/a/c;->e:Z

    iput-object v0, p0, Lcom/google/i/a/a/a/c;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v1, p0, Lcom/google/i/a/a/a/c;->f:Z

    iput-object v0, p0, Lcom/google/i/a/a/a/c;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v1, p0, Lcom/google/i/a/a/a/c;->g:Z

    iput v0, p0, Lcom/google/i/a/a/a/c;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    iput-boolean v1, p0, Lcom/google/i/a/a/a/c;->h:Z

    iput-object v0, p0, Lcom/google/i/a/a/a/c;->d:Lcom/google/protobuf/a/a;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/i/a/a/a/c;->e:Z

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/i/a/a/a/c;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 97
    :cond_0
    iget-boolean v0, p0, Lcom/google/i/a/a/a/c;->f:Z

    if-eqz v0, :cond_1

    .line 98
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/i/a/a/a/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 100
    :cond_1
    iget-boolean v0, p0, Lcom/google/i/a/a/a/c;->g:Z

    if-eqz v0, :cond_2

    .line 101
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/i/a/a/a/c;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 103
    :cond_2
    iget-boolean v0, p0, Lcom/google/i/a/a/a/c;->h:Z

    if-eqz v0, :cond_3

    .line 104
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/i/a/a/a/c;->d:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 106
    :cond_3
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 120
    const/4 v0, 0x0

    .line 121
    iget-boolean v1, p0, Lcom/google/i/a/a/a/c;->e:Z

    if-eqz v1, :cond_0

    .line 122
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/i/a/a/a/c;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 125
    :cond_0
    iget-boolean v1, p0, Lcom/google/i/a/a/a/c;->f:Z

    if-eqz v1, :cond_1

    .line 126
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/i/a/a/a/c;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    :cond_1
    iget-boolean v1, p0, Lcom/google/i/a/a/a/c;->g:Z

    if-eqz v1, :cond_2

    .line 130
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/i/a/a/a/c;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    :cond_2
    iget-boolean v1, p0, Lcom/google/i/a/a/a/c;->h:Z

    if-eqz v1, :cond_3

    .line 134
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/i/a/a/a/c;->d:Lcom/google/protobuf/a/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_3
    iput v0, p0, Lcom/google/i/a/a/a/c;->i:I

    .line 138
    return v0
.end method
