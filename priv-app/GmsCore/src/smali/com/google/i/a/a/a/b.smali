.class public final Lcom/google/i/a/a/a/b;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Lcom/google/protobuf/a/a;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/a/a/b;->b:Ljava/lang/String;

    .line 30
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/i/a/a/a/b;->d:Lcom/google/protobuf/a/a;

    .line 66
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/i/a/a/a/b;->e:I

    .line 8
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/google/i/a/a/a/b;->e:I

    if-gez v0, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/google/i/a/a/a/b;->b()I

    .line 73
    :cond_0
    iget v0, p0, Lcom/google/i/a/a/a/b;->e:I

    return v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/i/a/a/a/b;
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/i/a/a/a/b;->a:Z

    .line 18
    iput-object p1, p0, Lcom/google/i/a/a/a/b;->b:Ljava/lang/String;

    .line 19
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/i/a/a/a/b;->a(Ljava/lang/String;)Lcom/google/i/a/a/a/b;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/i/a/a/a/b;->c:Z

    iput-object v0, p0, Lcom/google/i/a/a/a/b;->d:Lcom/google/protobuf/a/a;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x2a -> :sswitch_1
        0x32 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 58
    iget-boolean v0, p0, Lcom/google/i/a/a/a/b;->a:Z

    if-eqz v0, :cond_0

    .line 59
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/i/a/a/a/b;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 61
    :cond_0
    iget-boolean v0, p0, Lcom/google/i/a/a/a/b;->c:Z

    if-eqz v0, :cond_1

    .line 62
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/i/a/a/a/b;->d:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 64
    :cond_1
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 78
    const/4 v0, 0x0

    .line 79
    iget-boolean v1, p0, Lcom/google/i/a/a/a/b;->a:Z

    if-eqz v1, :cond_0

    .line 80
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/i/a/a/a/b;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 83
    :cond_0
    iget-boolean v1, p0, Lcom/google/i/a/a/a/b;->c:Z

    if-eqz v1, :cond_1

    .line 84
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/i/a/a/a/b;->d:Lcom/google/protobuf/a/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v1

    add-int/2addr v0, v1

    .line 87
    :cond_1
    iput v0, p0, Lcom/google/i/a/a/a/b;->e:I

    .line 88
    return v0
.end method
