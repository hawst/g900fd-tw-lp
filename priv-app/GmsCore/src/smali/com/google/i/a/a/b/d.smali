.class public final Lcom/google/i/a/a/b/d;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Z

.field private b:Z

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/i/a/a/b/d;->a:Z

    .line 45
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/i/a/a/b/d;->c:I

    .line 8
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/google/i/a/a/b/d;->c:I

    if-gez v0, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/google/i/a/a/b/d;->b()I

    .line 52
    :cond_0
    iget v0, p0, Lcom/google/i/a/a/b/d;->c:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/i/a/a/b/d;->b:Z

    iput-boolean v0, p0, Lcom/google/i/a/a/b/d;->a:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/i/a/a/b/d;->b:Z

    if-eqz v0, :cond_0

    .line 41
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/i/a/a/b/d;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 43
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 57
    const/4 v0, 0x0

    .line 58
    iget-boolean v1, p0, Lcom/google/i/a/a/b/d;->b:Z

    if-eqz v1, :cond_0

    .line 59
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/i/a/a/b/d;->a:Z

    invoke-static {v0}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 62
    :cond_0
    iput v0, p0, Lcom/google/i/a/a/b/d;->c:I

    .line 63
    return v0
.end method
