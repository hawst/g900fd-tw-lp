.class public final Lcom/google/i/a/a/b/b;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Lcom/google/i/a/a/a/c;

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/i/a/a/b/b;->b:Lcom/google/i/a/a/a/c;

    .line 48
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/i/a/a/b/b;->c:I

    .line 8
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 51
    iget v0, p0, Lcom/google/i/a/a/b/b;->c:I

    if-gez v0, :cond_0

    .line 53
    invoke-virtual {p0}, Lcom/google/i/a/a/b/b;->b()I

    .line 55
    :cond_0
    iget v0, p0, Lcom/google/i/a/a/b/b;->c:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/i/a/a/a/c;

    invoke-direct {v0}, Lcom/google/i/a/a/a/c;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/i/a/a/b/b;->a:Z

    iput-object v0, p0, Lcom/google/i/a/a/b/b;->b:Lcom/google/i/a/a/a/c;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 43
    iget-boolean v0, p0, Lcom/google/i/a/a/b/b;->a:Z

    if-eqz v0, :cond_0

    .line 44
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/i/a/a/b/b;->b:Lcom/google/i/a/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 46
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 60
    const/4 v0, 0x0

    .line 61
    iget-boolean v1, p0, Lcom/google/i/a/a/b/b;->a:Z

    if-eqz v1, :cond_0

    .line 62
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/i/a/a/b/b;->b:Lcom/google/i/a/a/a/c;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 65
    :cond_0
    iput v0, p0, Lcom/google/i/a/a/b/b;->c:I

    .line 66
    return v0
.end method
