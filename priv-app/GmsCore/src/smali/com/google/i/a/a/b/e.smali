.class public final Lcom/google/i/a/a/b/e;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Lcom/google/i/a/a/a/a;

.field private c:Z

.field private d:Lcom/google/i/a/a/a/b;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 13
    iput-object v0, p0, Lcom/google/i/a/a/b/e;->b:Lcom/google/i/a/a/a/a;

    .line 33
    iput-object v0, p0, Lcom/google/i/a/a/b/e;->d:Lcom/google/i/a/a/a/b;

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/i/a/a/b/e;->e:I

    .line 8
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 75
    iget v0, p0, Lcom/google/i/a/a/b/e;->e:I

    if-gez v0, :cond_0

    .line 77
    invoke-virtual {p0}, Lcom/google/i/a/a/b/e;->b()I

    .line 79
    :cond_0
    iget v0, p0, Lcom/google/i/a/a/b/e;->e:I

    return v0
.end method

.method public final a(Lcom/google/i/a/a/a/a;)Lcom/google/i/a/a/b/e;
    .locals 1

    .prologue
    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/i/a/a/b/e;->a:Z

    .line 21
    iput-object p1, p0, Lcom/google/i/a/a/b/e;->b:Lcom/google/i/a/a/a/a;

    .line 22
    return-object p0
.end method

.method public final a(Lcom/google/i/a/a/a/b;)Lcom/google/i/a/a/b/e;
    .locals 1

    .prologue
    .line 37
    if-nez p1, :cond_0

    .line 38
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 40
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/i/a/a/b/e;->c:Z

    .line 41
    iput-object p1, p0, Lcom/google/i/a/a/b/e;->d:Lcom/google/i/a/a/a/b;

    .line 42
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/i/a/a/a/a;

    invoke-direct {v0}, Lcom/google/i/a/a/a/a;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/i/a/a/b/e;->a(Lcom/google/i/a/a/a/a;)Lcom/google/i/a/a/b/e;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/i/a/a/a/b;

    invoke-direct {v0}, Lcom/google/i/a/a/a/b;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/i/a/a/b/e;->a(Lcom/google/i/a/a/a/b;)Lcom/google/i/a/a/b/e;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 64
    iget-boolean v0, p0, Lcom/google/i/a/a/b/e;->a:Z

    if-eqz v0, :cond_0

    .line 65
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/i/a/a/b/e;->b:Lcom/google/i/a/a/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 67
    :cond_0
    iget-boolean v0, p0, Lcom/google/i/a/a/b/e;->c:Z

    if-eqz v0, :cond_1

    .line 68
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/i/a/a/b/e;->d:Lcom/google/i/a/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 70
    :cond_1
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 84
    const/4 v0, 0x0

    .line 85
    iget-boolean v1, p0, Lcom/google/i/a/a/b/e;->a:Z

    if-eqz v1, :cond_0

    .line 86
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/i/a/a/b/e;->b:Lcom/google/i/a/a/a/a;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 89
    :cond_0
    iget-boolean v1, p0, Lcom/google/i/a/a/b/e;->c:Z

    if-eqz v1, :cond_1

    .line 90
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/i/a/a/b/e;->d:Lcom/google/i/a/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 93
    :cond_1
    iput v0, p0, Lcom/google/i/a/a/b/e;->e:I

    .line 94
    return v0
.end method
