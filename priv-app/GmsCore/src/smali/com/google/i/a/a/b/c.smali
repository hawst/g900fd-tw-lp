.class public final Lcom/google/i/a/a/b/c;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Lcom/google/i/a/a/a/a;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/i/a/a/b/c;->b:Lcom/google/i/a/a/a/a;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/a/b/c;->d:Ljava/lang/String;

    .line 69
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/i/a/a/b/c;->e:I

    .line 8
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/i/a/a/b/c;->e:I

    if-gez v0, :cond_0

    .line 74
    invoke-virtual {p0}, Lcom/google/i/a/a/b/c;->b()I

    .line 76
    :cond_0
    iget v0, p0, Lcom/google/i/a/a/b/c;->e:I

    return v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/i/a/a/b/c;
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/i/a/a/b/c;->c:Z

    .line 38
    iput-object p1, p0, Lcom/google/i/a/a/b/c;->d:Ljava/lang/String;

    .line 39
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/i/a/a/a/a;

    invoke-direct {v0}, Lcom/google/i/a/a/a/a;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/i/a/a/b/c;->a:Z

    iput-object v0, p0, Lcom/google/i/a/a/b/c;->b:Lcom/google/i/a/a/a/a;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/i/a/a/b/c;->a(Ljava/lang/String;)Lcom/google/i/a/a/b/c;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 61
    iget-boolean v0, p0, Lcom/google/i/a/a/b/c;->a:Z

    if-eqz v0, :cond_0

    .line 62
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/i/a/a/b/c;->b:Lcom/google/i/a/a/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 64
    :cond_0
    iget-boolean v0, p0, Lcom/google/i/a/a/b/c;->c:Z

    if-eqz v0, :cond_1

    .line 65
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/i/a/a/b/c;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 67
    :cond_1
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 81
    const/4 v0, 0x0

    .line 82
    iget-boolean v1, p0, Lcom/google/i/a/a/b/c;->a:Z

    if-eqz v1, :cond_0

    .line 83
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/i/a/a/b/c;->b:Lcom/google/i/a/a/a/a;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 86
    :cond_0
    iget-boolean v1, p0, Lcom/google/i/a/a/b/c;->c:Z

    if-eqz v1, :cond_1

    .line 87
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/i/a/a/b/c;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_1
    iput v0, p0, Lcom/google/i/a/a/b/c;->e:I

    .line 91
    return v0
.end method
