.class public final Lcom/google/i/a/a/b/a;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Lcom/google/i/a/a/a/a;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:I

.field private g:Z

.field private h:Lcom/google/protobuf/a/a;

.field private i:Z

.field private j:Ljava/lang/String;

.field private k:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 13
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/i/a/a/b/a;->b:Lcom/google/i/a/a/a/a;

    .line 33
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/a/b/a;->d:Ljava/lang/String;

    .line 50
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/i/a/a/b/a;->f:I

    .line 67
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/i/a/a/b/a;->h:Lcom/google/protobuf/a/a;

    .line 84
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/a/b/a;->j:Ljava/lang/String;

    .line 132
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/i/a/a/b/a;->k:I

    .line 8
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 135
    iget v0, p0, Lcom/google/i/a/a/b/a;->k:I

    if-gez v0, :cond_0

    .line 137
    invoke-virtual {p0}, Lcom/google/i/a/a/b/a;->b()I

    .line 139
    :cond_0
    iget v0, p0, Lcom/google/i/a/a/b/a;->k:I

    return v0
.end method

.method public final a(I)Lcom/google/i/a/a/b/a;
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/i/a/a/b/a;->e:Z

    .line 55
    iput p1, p0, Lcom/google/i/a/a/b/a;->f:I

    .line 56
    return-object p0
.end method

.method public final a(Lcom/google/i/a/a/a/a;)Lcom/google/i/a/a/b/a;
    .locals 1

    .prologue
    .line 17
    if-nez p1, :cond_0

    .line 18
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 20
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/i/a/a/b/a;->a:Z

    .line 21
    iput-object p1, p0, Lcom/google/i/a/a/b/a;->b:Lcom/google/i/a/a/a/a;

    .line 22
    return-object p0
.end method

.method public final a(Lcom/google/protobuf/a/a;)Lcom/google/i/a/a/b/a;
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/i/a/a/b/a;->g:Z

    .line 72
    iput-object p1, p0, Lcom/google/i/a/a/b/a;->h:Lcom/google/protobuf/a/a;

    .line 73
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/i/a/a/b/a;
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/i/a/a/b/a;->c:Z

    .line 38
    iput-object p1, p0, Lcom/google/i/a/a/b/a;->d:Ljava/lang/String;

    .line 39
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/i/a/a/a/a;

    invoke-direct {v0}, Lcom/google/i/a/a/a/a;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/i/a/a/b/a;->a(Lcom/google/i/a/a/a/a;)Lcom/google/i/a/a/b/a;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/i/a/a/b/a;->a(Ljava/lang/String;)Lcom/google/i/a/a/b/a;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/i/a/a/b/a;->a(I)Lcom/google/i/a/a/b/a;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/i/a/a/b/a;->a(Lcom/google/protobuf/a/a;)Lcom/google/i/a/a/b/a;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/i/a/a/b/a;->b(Ljava/lang/String;)Lcom/google/i/a/a/b/a;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/google/i/a/a/b/a;->a:Z

    if-eqz v0, :cond_0

    .line 116
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/i/a/a/b/a;->b:Lcom/google/i/a/a/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 118
    :cond_0
    iget-boolean v0, p0, Lcom/google/i/a/a/b/a;->c:Z

    if-eqz v0, :cond_1

    .line 119
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/i/a/a/b/a;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 121
    :cond_1
    iget-boolean v0, p0, Lcom/google/i/a/a/b/a;->e:Z

    if-eqz v0, :cond_2

    .line 122
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/i/a/a/b/a;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 124
    :cond_2
    iget-boolean v0, p0, Lcom/google/i/a/a/b/a;->g:Z

    if-eqz v0, :cond_3

    .line 125
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/i/a/a/b/a;->h:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 127
    :cond_3
    iget-boolean v0, p0, Lcom/google/i/a/a/b/a;->i:Z

    if-eqz v0, :cond_4

    .line 128
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/i/a/a/b/a;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 130
    :cond_4
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 144
    const/4 v0, 0x0

    .line 145
    iget-boolean v1, p0, Lcom/google/i/a/a/b/a;->a:Z

    if-eqz v1, :cond_0

    .line 146
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/i/a/a/b/a;->b:Lcom/google/i/a/a/a/a;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 149
    :cond_0
    iget-boolean v1, p0, Lcom/google/i/a/a/b/a;->c:Z

    if-eqz v1, :cond_1

    .line 150
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/i/a/a/b/a;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 153
    :cond_1
    iget-boolean v1, p0, Lcom/google/i/a/a/b/a;->e:Z

    if-eqz v1, :cond_2

    .line 154
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/i/a/a/b/a;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    :cond_2
    iget-boolean v1, p0, Lcom/google/i/a/a/b/a;->g:Z

    if-eqz v1, :cond_3

    .line 158
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/i/a/a/b/a;->h:Lcom/google/protobuf/a/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    :cond_3
    iget-boolean v1, p0, Lcom/google/i/a/a/b/a;->i:Z

    if-eqz v1, :cond_4

    .line 162
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/i/a/a/b/a;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    :cond_4
    iput v0, p0, Lcom/google/i/a/a/b/a;->k:I

    .line 166
    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/i/a/a/b/a;
    .locals 1

    .prologue
    .line 88
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/i/a/a/b/a;->i:Z

    .line 89
    iput-object p1, p0, Lcom/google/i/a/a/b/a;->j:Ljava/lang/String;

    .line 90
    return-object p0
.end method
