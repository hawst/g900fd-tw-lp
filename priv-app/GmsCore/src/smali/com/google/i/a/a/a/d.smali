.class public final Lcom/google/i/a/a/a/d;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:I

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/google/i/a/a/a/d;->a:Ljava/lang/String;

    .line 30
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/i/a/a/a/d;->b:I

    .line 47
    const/16 v0, 0x960

    iput v0, p0, Lcom/google/i/a/a/a/d;->c:I

    .line 87
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/i/a/a/a/d;->g:I

    .line 8
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 90
    iget v0, p0, Lcom/google/i/a/a/a/d;->g:I

    if-gez v0, :cond_0

    .line 92
    invoke-virtual {p0}, Lcom/google/i/a/a/a/d;->b()I

    .line 94
    :cond_0
    iget v0, p0, Lcom/google/i/a/a/a/d;->g:I

    return v0
.end method

.method public final a(I)Lcom/google/i/a/a/a/d;
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/i/a/a/a/d;->e:Z

    .line 35
    iput p1, p0, Lcom/google/i/a/a/a/d;->b:I

    .line 36
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/i/a/a/a/d;
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/i/a/a/a/d;->d:Z

    .line 18
    iput-object p1, p0, Lcom/google/i/a/a/a/d;->a:Ljava/lang/String;

    .line 19
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/i/a/a/a/d;->a(I)Lcom/google/i/a/a/a/d;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/i/a/a/a/d;->a(Ljava/lang/String;)Lcom/google/i/a/a/a/d;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/i/a/a/a/d;->b(I)Lcom/google/i/a/a/a/d;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1a -> :sswitch_2
        0x20 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 76
    iget-boolean v0, p0, Lcom/google/i/a/a/a/d;->e:Z

    if-eqz v0, :cond_0

    .line 77
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/i/a/a/a/d;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 79
    :cond_0
    iget-boolean v0, p0, Lcom/google/i/a/a/a/d;->d:Z

    if-eqz v0, :cond_1

    .line 80
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/i/a/a/a/d;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 82
    :cond_1
    iget-boolean v0, p0, Lcom/google/i/a/a/a/d;->f:Z

    if-eqz v0, :cond_2

    .line 83
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/i/a/a/a/d;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 85
    :cond_2
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 99
    const/4 v0, 0x0

    .line 100
    iget-boolean v1, p0, Lcom/google/i/a/a/a/d;->e:Z

    if-eqz v1, :cond_0

    .line 101
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/i/a/a/a/d;->b:I

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 104
    :cond_0
    iget-boolean v1, p0, Lcom/google/i/a/a/a/d;->d:Z

    if-eqz v1, :cond_1

    .line 105
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/i/a/a/a/d;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 108
    :cond_1
    iget-boolean v1, p0, Lcom/google/i/a/a/a/d;->f:Z

    if-eqz v1, :cond_2

    .line 109
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/i/a/a/a/d;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 112
    :cond_2
    iput v0, p0, Lcom/google/i/a/a/a/d;->g:I

    .line 113
    return v0
.end method

.method public final b(I)Lcom/google/i/a/a/a/d;
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/i/a/a/a/d;->f:Z

    .line 52
    iput p1, p0, Lcom/google/i/a/a/a/d;->c:I

    .line 53
    return-object p0
.end method
