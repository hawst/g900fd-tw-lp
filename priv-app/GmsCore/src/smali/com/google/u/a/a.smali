.class public final Lcom/google/u/a/a;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile k:[Lcom/google/u/a/a;


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;

.field public e:Lcom/google/u/a/b;

.field public f:Lcom/google/u/a/c;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Boolean;

.field public j:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 53
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 54
    iput-object v0, p0, Lcom/google/u/a/a;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/u/a/a;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/u/a/a;->c:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/u/a/a;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/u/a/a;->e:Lcom/google/u/a/b;

    iput-object v0, p0, Lcom/google/u/a/a;->f:Lcom/google/u/a/c;

    iput-object v0, p0, Lcom/google/u/a/a;->g:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/u/a/a;->h:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/u/a/a;->i:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/u/a/a;->j:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/u/a/a;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/u/a/a;->cachedSize:I

    .line 55
    return-void
.end method

.method public static a()[Lcom/google/u/a/a;
    .locals 2

    .prologue
    .line 12
    sget-object v0, Lcom/google/u/a/a;->k:[Lcom/google/u/a/a;

    if-nez v0, :cond_1

    .line 13
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 15
    :try_start_0
    sget-object v0, Lcom/google/u/a/a;->k:[Lcom/google/u/a/a;

    if-nez v0, :cond_0

    .line 16
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/u/a/a;

    sput-object v0, Lcom/google/u/a/a;->k:[Lcom/google/u/a/a;

    .line 18
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 20
    :cond_1
    sget-object v0, Lcom/google/u/a/a;->k:[Lcom/google/u/a/a;

    return-object v0

    .line 18
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 224
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 225
    iget-object v1, p0, Lcom/google/u/a/a;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 226
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/u/a/a;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 229
    :cond_0
    iget-object v1, p0, Lcom/google/u/a/a;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 230
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/u/a/a;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    :cond_1
    iget-object v1, p0, Lcom/google/u/a/a;->e:Lcom/google/u/a/b;

    if-eqz v1, :cond_2

    .line 234
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/u/a/a;->e:Lcom/google/u/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 237
    :cond_2
    iget-object v1, p0, Lcom/google/u/a/a;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 238
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/u/a/a;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 241
    :cond_3
    iget-object v1, p0, Lcom/google/u/a/a;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 242
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/u/a/a;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 245
    :cond_4
    iget-object v1, p0, Lcom/google/u/a/a;->i:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 246
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/u/a/a;->i:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 249
    :cond_5
    iget-object v1, p0, Lcom/google/u/a/a;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 250
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/u/a/a;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    :cond_6
    iget-object v1, p0, Lcom/google/u/a/a;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 254
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/u/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 257
    :cond_7
    iget-object v1, p0, Lcom/google/u/a/a;->f:Lcom/google/u/a/c;

    if-eqz v1, :cond_8

    .line 258
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/u/a/a;->f:Lcom/google/u/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    :cond_8
    iget-object v1, p0, Lcom/google/u/a/a;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_9

    .line 262
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/u/a/a;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 265
    :cond_9
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 75
    if-ne p1, p0, :cond_1

    .line 76
    const/4 v0, 0x1

    .line 156
    :cond_0
    :goto_0
    return v0

    .line 78
    :cond_1
    instance-of v1, p1, Lcom/google/u/a/a;

    if-eqz v1, :cond_0

    .line 81
    check-cast p1, Lcom/google/u/a/a;

    .line 82
    iget-object v1, p0, Lcom/google/u/a/a;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_c

    .line 83
    iget-object v1, p1, Lcom/google/u/a/a;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 89
    :cond_2
    iget-object v1, p0, Lcom/google/u/a/a;->b:Ljava/lang/Integer;

    if-nez v1, :cond_d

    .line 90
    iget-object v1, p1, Lcom/google/u/a/a;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 96
    :cond_3
    iget-object v1, p0, Lcom/google/u/a/a;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_e

    .line 97
    iget-object v1, p1, Lcom/google/u/a/a;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 103
    :cond_4
    iget-object v1, p0, Lcom/google/u/a/a;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_f

    .line 104
    iget-object v1, p1, Lcom/google/u/a/a;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 110
    :cond_5
    iget-object v1, p0, Lcom/google/u/a/a;->e:Lcom/google/u/a/b;

    if-nez v1, :cond_10

    .line 111
    iget-object v1, p1, Lcom/google/u/a/a;->e:Lcom/google/u/a/b;

    if-nez v1, :cond_0

    .line 119
    :cond_6
    iget-object v1, p0, Lcom/google/u/a/a;->f:Lcom/google/u/a/c;

    if-nez v1, :cond_11

    .line 120
    iget-object v1, p1, Lcom/google/u/a/a;->f:Lcom/google/u/a/c;

    if-nez v1, :cond_0

    .line 128
    :cond_7
    iget-object v1, p0, Lcom/google/u/a/a;->g:Ljava/lang/Boolean;

    if-nez v1, :cond_12

    .line 129
    iget-object v1, p1, Lcom/google/u/a/a;->g:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 135
    :cond_8
    iget-object v1, p0, Lcom/google/u/a/a;->h:Ljava/lang/Boolean;

    if-nez v1, :cond_13

    .line 136
    iget-object v1, p1, Lcom/google/u/a/a;->h:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 142
    :cond_9
    iget-object v1, p0, Lcom/google/u/a/a;->i:Ljava/lang/Boolean;

    if-nez v1, :cond_14

    .line 143
    iget-object v1, p1, Lcom/google/u/a/a;->i:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 149
    :cond_a
    iget-object v1, p0, Lcom/google/u/a/a;->j:Ljava/lang/Integer;

    if-nez v1, :cond_15

    .line 150
    iget-object v1, p1, Lcom/google/u/a/a;->j:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 156
    :cond_b
    invoke-virtual {p0, p1}, Lcom/google/u/a/a;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 86
    :cond_c
    iget-object v1, p0, Lcom/google/u/a/a;->a:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/u/a/a;->a:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 93
    :cond_d
    iget-object v1, p0, Lcom/google/u/a/a;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/u/a/a;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 100
    :cond_e
    iget-object v1, p0, Lcom/google/u/a/a;->c:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/u/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 107
    :cond_f
    iget-object v1, p0, Lcom/google/u/a/a;->d:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/u/a/a;->d:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 115
    :cond_10
    iget-object v1, p0, Lcom/google/u/a/a;->e:Lcom/google/u/a/b;

    iget-object v2, p1, Lcom/google/u/a/a;->e:Lcom/google/u/a/b;

    invoke-virtual {v1, v2}, Lcom/google/u/a/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 124
    :cond_11
    iget-object v1, p0, Lcom/google/u/a/a;->f:Lcom/google/u/a/c;

    iget-object v2, p1, Lcom/google/u/a/a;->f:Lcom/google/u/a/c;

    invoke-virtual {v1, v2}, Lcom/google/u/a/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 132
    :cond_12
    iget-object v1, p0, Lcom/google/u/a/a;->g:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/u/a/a;->g:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 139
    :cond_13
    iget-object v1, p0, Lcom/google/u/a/a;->h:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/u/a/a;->h:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 146
    :cond_14
    iget-object v1, p0, Lcom/google/u/a/a;->i:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/u/a/a;->i:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 153
    :cond_15
    iget-object v1, p0, Lcom/google/u/a/a;->j:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/u/a/a;->j:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 161
    iget-object v0, p0, Lcom/google/u/a/a;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 164
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/u/a/a;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 166
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/u/a/a;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 168
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/u/a/a;->d:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 170
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/u/a/a;->e:Lcom/google/u/a/b;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 172
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/u/a/a;->f:Lcom/google/u/a/c;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 174
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/u/a/a;->g:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 176
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/u/a/a;->h:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 178
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/u/a/a;->i:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 180
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/u/a/a;->j:Ljava/lang/Integer;

    if-nez v2, :cond_9

    :goto_9
    add-int/2addr v0, v1

    .line 182
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/u/a/a;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 183
    return v0

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/u/a/a;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0

    .line 164
    :cond_1
    iget-object v0, p0, Lcom/google/u/a/a;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_1

    .line 166
    :cond_2
    iget-object v0, p0, Lcom/google/u/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_2

    .line 168
    :cond_3
    iget-object v0, p0, Lcom/google/u/a/a;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_3

    .line 170
    :cond_4
    iget-object v0, p0, Lcom/google/u/a/a;->e:Lcom/google/u/a/b;

    invoke-virtual {v0}, Lcom/google/u/a/b;->hashCode()I

    move-result v0

    goto :goto_4

    .line 172
    :cond_5
    iget-object v0, p0, Lcom/google/u/a/a;->f:Lcom/google/u/a/c;

    invoke-virtual {v0}, Lcom/google/u/a/c;->hashCode()I

    move-result v0

    goto :goto_5

    .line 174
    :cond_6
    iget-object v0, p0, Lcom/google/u/a/a;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_6

    .line 176
    :cond_7
    iget-object v0, p0, Lcom/google/u/a/a;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_7

    .line 178
    :cond_8
    iget-object v0, p0, Lcom/google/u/a/a;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_8

    .line 180
    :cond_9
    iget-object v1, p0, Lcom/google/u/a/a;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_9
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/u/a/a;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/u/a/a;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/u/a/a;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/u/a/a;->e:Lcom/google/u/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/u/a/b;

    invoke-direct {v0}, Lcom/google/u/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/u/a/a;->e:Lcom/google/u/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/u/a/a;->e:Lcom/google/u/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/u/a/a;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/u/a/a;->h:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/u/a/a;->i:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/u/a/a;->j:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/u/a/a;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/u/a/a;->f:Lcom/google/u/a/c;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/u/a/c;

    invoke-direct {v0}, Lcom/google/u/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/u/a/a;->f:Lcom/google/u/a/c;

    :cond_2
    iget-object v0, p0, Lcom/google/u/a/a;->f:Lcom/google/u/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/u/a/a;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x30 -> :sswitch_4
        0x38 -> :sswitch_5
        0x40 -> :sswitch_6
        0x48 -> :sswitch_7
        0x50 -> :sswitch_8
        0x5a -> :sswitch_9
        0x60 -> :sswitch_a
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 189
    iget-object v0, p0, Lcom/google/u/a/a;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 190
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/u/a/a;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 192
    :cond_0
    iget-object v0, p0, Lcom/google/u/a/a;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 193
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/u/a/a;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 195
    :cond_1
    iget-object v0, p0, Lcom/google/u/a/a;->e:Lcom/google/u/a/b;

    if-eqz v0, :cond_2

    .line 196
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/u/a/a;->e:Lcom/google/u/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 198
    :cond_2
    iget-object v0, p0, Lcom/google/u/a/a;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 199
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/u/a/a;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 201
    :cond_3
    iget-object v0, p0, Lcom/google/u/a/a;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 202
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/u/a/a;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 204
    :cond_4
    iget-object v0, p0, Lcom/google/u/a/a;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 205
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/u/a/a;->i:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 207
    :cond_5
    iget-object v0, p0, Lcom/google/u/a/a;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 208
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/u/a/a;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 210
    :cond_6
    iget-object v0, p0, Lcom/google/u/a/a;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 211
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/u/a/a;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 213
    :cond_7
    iget-object v0, p0, Lcom/google/u/a/a;->f:Lcom/google/u/a/c;

    if-eqz v0, :cond_8

    .line 214
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/u/a/a;->f:Lcom/google/u/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 216
    :cond_8
    iget-object v0, p0, Lcom/google/u/a/a;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 217
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/u/a/a;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 219
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 220
    return-void
.end method
