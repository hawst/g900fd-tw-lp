.class public final Lcom/google/r/a/a/c;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile f:[Lcom/google/r/a/a/c;


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Z

.field public d:Ljava/lang/String;

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 41
    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    iput v1, p0, Lcom/google/r/a/a/c;->b:I

    iput-boolean v1, p0, Lcom/google/r/a/a/c;->c:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    iput v1, p0, Lcom/google/r/a/a/c;->e:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/r/a/a/c;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/r/a/a/c;->cachedSize:I

    .line 42
    return-void
.end method

.method public static a()[Lcom/google/r/a/a/c;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/r/a/a/c;->f:[Lcom/google/r/a/a/c;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/r/a/a/c;->f:[Lcom/google/r/a/a/c;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/r/a/a/c;

    sput-object v0, Lcom/google/r/a/a/c;->f:[Lcom/google/r/a/a/c;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/r/a/a/c;->f:[Lcom/google/r/a/a/c;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 127
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 128
    iget-object v1, p0, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 129
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_0
    iget-boolean v1, p0, Lcom/google/r/a/a/c;->c:Z

    if-eqz v1, :cond_1

    .line 133
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/r/a/a/c;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 136
    :cond_1
    iget-object v1, p0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 137
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 140
    :cond_2
    iget v1, p0, Lcom/google/r/a/a/c;->e:I

    if-eqz v1, :cond_3

    .line 141
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/r/a/a/c;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    :cond_3
    iget v1, p0, Lcom/google/r/a/a/c;->b:I

    if-eqz v1, :cond_4

    .line 145
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/r/a/a/c;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 57
    if-ne p1, p0, :cond_1

    .line 58
    const/4 v0, 0x1

    .line 87
    :cond_0
    :goto_0
    return v0

    .line 60
    :cond_1
    instance-of v1, p1, Lcom/google/r/a/a/c;

    if-eqz v1, :cond_0

    .line 63
    check-cast p1, Lcom/google/r/a/a/c;

    .line 64
    iget-object v1, p0, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 65
    iget-object v1, p1, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 71
    :cond_2
    iget v1, p0, Lcom/google/r/a/a/c;->b:I

    iget v2, p1, Lcom/google/r/a/a/c;->b:I

    if-ne v1, v2, :cond_0

    .line 74
    iget-boolean v1, p0, Lcom/google/r/a/a/c;->c:Z

    iget-boolean v2, p1, Lcom/google/r/a/a/c;->c:Z

    if-ne v1, v2, :cond_0

    .line 77
    iget-object v1, p0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 78
    iget-object v1, p1, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 84
    :cond_3
    iget v1, p0, Lcom/google/r/a/a/c;->e:I

    iget v2, p1, Lcom/google/r/a/a/c;->e:I

    if-ne v1, v2, :cond_0

    .line 87
    invoke-virtual {p0, p1}, Lcom/google/r/a/a/c;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 68
    :cond_4
    iget-object v1, p0, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 81
    :cond_5
    iget-object v1, p0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 92
    iget-object v0, p0, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 95
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/r/a/a/c;->b:I

    add-int/2addr v0, v2

    .line 96
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/r/a/a/c;->c:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v2

    .line 97
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 99
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/r/a/a/c;->e:I

    add-int/2addr v0, v1

    .line 100
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/r/a/a/c;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 101
    return v0

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 96
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1

    .line 97
    :cond_2
    iget-object v1, p0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/r/a/a/c;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/r/a/a/c;->c:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/r/a/a/c;->e:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/r/a/a/c;->b:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/r/a/a/c;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 110
    :cond_0
    iget-boolean v0, p0, Lcom/google/r/a/a/c;->c:Z

    if-eqz v0, :cond_1

    .line 111
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/r/a/a/c;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 113
    :cond_1
    iget-object v0, p0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 114
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 116
    :cond_2
    iget v0, p0, Lcom/google/r/a/a/c;->e:I

    if-eqz v0, :cond_3

    .line 117
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/r/a/a/c;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 119
    :cond_3
    iget v0, p0, Lcom/google/r/a/a/c;->b:I

    if-eqz v0, :cond_4

    .line 120
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/r/a/a/c;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 122
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 123
    return-void
.end method
