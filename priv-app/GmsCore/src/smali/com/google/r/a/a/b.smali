.class public final Lcom/google/r/a/a/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/r/a/a/c;

.field public b:J

.field public c:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 228
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 229
    invoke-static {}, Lcom/google/r/a/a/c;->a()[Lcom/google/r/a/a/c;

    move-result-object v0

    iput-object v0, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/r/a/a/b;->b:J

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/r/a/a/b;->c:[I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/r/a/a/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/r/a/a/b;->cachedSize:I

    .line 230
    return-void
.end method

.method public static a([B)Lcom/google/r/a/a/b;
    .locals 1

    .prologue
    .line 413
    new-instance v0, Lcom/google/r/a/a/b;

    invoke-direct {v0}, Lcom/google/r/a/a/b;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/r/a/a/b;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 301
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 302
    iget-object v2, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 303
    :goto_0
    iget-object v3, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 304
    iget-object v3, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    aget-object v3, v3, v0

    .line 305
    if-eqz v3, :cond_0

    .line 306
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 303
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 311
    :cond_2
    iget-wide v2, p0, Lcom/google/r/a/a/b;->b:J

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    .line 312
    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/google/r/a/a/b;->b:J

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 315
    :cond_3
    iget-object v2, p0, Lcom/google/r/a/a/b;->c:[I

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/r/a/a/b;->c:[I

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v1

    .line 317
    :goto_1
    iget-object v3, p0, Lcom/google/r/a/a/b;->c:[I

    array-length v3, v3

    if-ge v1, v3, :cond_4

    .line 318
    iget-object v3, p0, Lcom/google/r/a/a/b;->c:[I

    aget v3, v3, v1

    .line 319
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 317
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 322
    :cond_4
    add-int/2addr v0, v2

    .line 323
    iget-object v1, p0, Lcom/google/r/a/a/b;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 325
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 243
    if-ne p1, p0, :cond_1

    .line 244
    const/4 v0, 0x1

    .line 261
    :cond_0
    :goto_0
    return v0

    .line 246
    :cond_1
    instance-of v1, p1, Lcom/google/r/a/a/b;

    if-eqz v1, :cond_0

    .line 249
    check-cast p1, Lcom/google/r/a/a/b;

    .line 250
    iget-object v1, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    iget-object v2, p1, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 254
    iget-wide v2, p0, Lcom/google/r/a/a/b;->b:J

    iget-wide v4, p1, Lcom/google/r/a/a/b;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_0

    .line 257
    iget-object v1, p0, Lcom/google/r/a/a/b;->c:[I

    iget-object v2, p1, Lcom/google/r/a/a/b;->c:[I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 261
    invoke-virtual {p0, p1}, Lcom/google/r/a/a/b;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 269
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/r/a/a/b;->b:J

    iget-wide v4, p0, Lcom/google/r/a/a/b;->b:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 271
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/r/a/a/b;->c:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/r/a/a/b;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 202
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/r/a/a/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/r/a/a/c;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/r/a/a/c;

    invoke-direct {v3}, Lcom/google/r/a/a/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/r/a/a/c;

    invoke-direct {v3}, Lcom/google/r/a/a/c;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/r/a/a/b;->b:J

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/r/a/a/b;->c:[I

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/r/a/a/b;->c:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/r/a/a/b;->c:[I

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/r/a/a/b;->c:[I

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/r/a/a/b;->c:[I

    if-nez v2, :cond_9

    move v2, v1

    :goto_6
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_8

    iget-object v4, p0, Lcom/google/r/a/a/b;->c:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_7
    array-length v4, v0

    if-ge v2, v4, :cond_a

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_9
    iget-object v2, p0, Lcom/google/r/a/a/b;->c:[I

    array-length v2, v2

    goto :goto_6

    :cond_a
    iput-object v0, p0, Lcom/google/r/a/a/b;->c:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 280
    iget-object v0, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 281
    :goto_0
    iget-object v2, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 282
    iget-object v2, p0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    aget-object v2, v2, v0

    .line 283
    if-eqz v2, :cond_0

    .line 284
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 281
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 288
    :cond_1
    iget-wide v2, p0, Lcom/google/r/a/a/b;->b:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    .line 289
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/r/a/a/b;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 291
    :cond_2
    iget-object v0, p0, Lcom/google/r/a/a/b;->c:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/r/a/a/b;->c:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 292
    :goto_1
    iget-object v0, p0, Lcom/google/r/a/a/b;->c:[I

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 293
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/r/a/a/b;->c:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 292
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 296
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 297
    return-void
.end method
