.class final Landroid/support/v7/widget/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/widget/u;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/widget/r;)F
    .locals 1

    .prologue
    .line 52
    invoke-interface {p1}, Landroid/support/v7/widget/r;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cv;

    invoke-virtual {v0}, Landroid/support/v7/widget/cv;->a()F

    move-result v0

    return v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 41
    return-void
.end method

.method public final a(Landroid/support/v7/widget/r;Landroid/content/Context;IFFF)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 26
    new-instance v0, Landroid/support/v7/widget/cv;

    invoke-direct {v0, p3, p4}, Landroid/support/v7/widget/cv;-><init>(IF)V

    .line 27
    invoke-interface {p1, v0}, Landroid/support/v7/widget/r;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object v0, p1

    .line 28
    check-cast v0, Landroid/view/View;

    .line 29
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setClipToOutline(Z)V

    .line 30
    invoke-virtual {v0, p5}, Landroid/view/View;->setElevation(F)V

    .line 31
    invoke-interface {p1}, Landroid/support/v7/widget/r;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cv;

    invoke-interface {p1}, Landroid/support/v7/widget/r;->a()Z

    move-result v1

    invoke-interface {p1}, Landroid/support/v7/widget/r;->d()Z

    move-result v2

    invoke-virtual {v0, p6, v1, v2}, Landroid/support/v7/widget/cv;->a(FZZ)V

    invoke-interface {p1}, Landroid/support/v7/widget/r;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1, v3, v3, v3, v3}, Landroid/support/v7/widget/r;->a(IIII)V

    .line 32
    :goto_0
    return-void

    .line 31
    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/q;->a(Landroid/support/v7/widget/r;)F

    move-result v0

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/q;->d(Landroid/support/v7/widget/r;)F

    move-result v1

    invoke-interface {p1}, Landroid/support/v7/widget/r;->d()Z

    move-result v2

    invoke-static {v0, v1, v2}, Landroid/support/v7/widget/cw;->b(FFZ)F

    move-result v2

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    invoke-interface {p1}, Landroid/support/v7/widget/r;->d()Z

    move-result v3

    invoke-static {v0, v1, v3}, Landroid/support/v7/widget/cw;->a(FFZ)F

    move-result v0

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v0

    double-to-int v0, v0

    invoke-interface {p1, v2, v0, v2, v0}, Landroid/support/v7/widget/r;->a(IIII)V

    goto :goto_0
.end method

.method public final b(Landroid/support/v7/widget/r;)F
    .locals 2

    .prologue
    .line 57
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/q;->d(Landroid/support/v7/widget/r;)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public final c(Landroid/support/v7/widget/r;)F
    .locals 2

    .prologue
    .line 62
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/q;->d(Landroid/support/v7/widget/r;)F

    move-result v0

    const/high16 v1, 0x40000000    # 2.0f

    mul-float/2addr v0, v1

    return v0
.end method

.method public final d(Landroid/support/v7/widget/r;)F
    .locals 1

    .prologue
    .line 67
    invoke-interface {p1}, Landroid/support/v7/widget/r;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cv;

    invoke-virtual {v0}, Landroid/support/v7/widget/cv;->b()F

    move-result v0

    return v0
.end method
