.class final Landroid/support/v7/widget/bu;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/widget/o;


# instance fields
.field final synthetic a:Landroid/support/v7/widget/RecyclerView;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 373
    iput-object p1, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private c(Landroid/support/v7/widget/p;)V
    .locals 5

    .prologue
    .line 404
    iget v0, p1, Landroid/support/v7/widget/p;->a:I

    packed-switch v0, :pswitch_data_0

    .line 418
    :goto_0
    return-void

    .line 406
    :pswitch_0
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/ce;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    iget v2, p1, Landroid/support/v7/widget/p;->b:I

    iget v3, p1, Landroid/support/v7/widget/p;->c:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/RecyclerView;II)V

    goto :goto_0

    .line 409
    :pswitch_1
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/ce;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    iget v2, p1, Landroid/support/v7/widget/p;->b:I

    iget v3, p1, Landroid/support/v7/widget/p;->c:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/ce;->b(Landroid/support/v7/widget/RecyclerView;II)V

    goto :goto_0

    .line 412
    :pswitch_2
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/ce;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    iget v2, p1, Landroid/support/v7/widget/p;->b:I

    iget v3, p1, Landroid/support/v7/widget/p;->c:I

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v7/widget/ce;->c(Landroid/support/v7/widget/RecyclerView;II)V

    goto :goto_0

    .line 415
    :pswitch_3
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/ce;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    iget v2, p1, Landroid/support/v7/widget/p;->b:I

    iget v3, p1, Landroid/support/v7/widget/p;->c:I

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/RecyclerView;III)V

    goto :goto_0

    .line 404
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public final a(I)Landroid/support/v7/widget/cs;
    .locals 1

    .prologue
    .line 376
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView;->b(I)Landroid/support/v7/widget/cs;

    move-result-object v0

    return-object v0
.end method

.method public final a(II)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 381
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v7/widget/RecyclerView;->a(IIZ)V

    .line 382
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->g:Z

    .line 383
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget v1, v0, Landroid/support/v7/widget/cp;->g:I

    add-int/2addr v1, p2

    iput v1, v0, Landroid/support/v7/widget/cp;->g:I

    .line 384
    return-void
.end method

.method public final a(Landroid/support/v7/widget/p;)V
    .locals 0

    .prologue
    .line 400
    invoke-direct {p0, p1}, Landroid/support/v7/widget/bu;->c(Landroid/support/v7/widget/p;)V

    .line 401
    return-void
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 388
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, p2, v1}, Landroid/support/v7/widget/RecyclerView;->a(IIZ)V

    .line 389
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->g:Z

    .line 390
    return-void
.end method

.method public final b(Landroid/support/v7/widget/p;)V
    .locals 0

    .prologue
    .line 422
    invoke-direct {p0, p1}, Landroid/support/v7/widget/bu;->c(Landroid/support/v7/widget/p;)V

    .line 423
    return-void
.end method

.method public final c(II)V
    .locals 2

    .prologue
    .line 394
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->e(II)V

    .line 395
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->h:Z

    .line 396
    return-void
.end method

.method public final d(II)V
    .locals 2

    .prologue
    .line 427
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->d(II)V

    .line 428
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->g:Z

    .line 429
    return-void
.end method

.method public final e(II)V
    .locals 2

    .prologue
    .line 433
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->c(II)V

    .line 435
    iget-object v0, p0, Landroid/support/v7/widget/bu;->a:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/RecyclerView;->g:Z

    .line 436
    return-void
.end method
