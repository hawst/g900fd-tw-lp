.class public Landroid/support/v7/widget/RecyclerView;
.super Landroid/view/ViewGroup;
.source "SourceFile"


# static fields
.field private static final ab:Landroid/view/animation/Interpolator;

.field private static final i:Z


# instance fields
.field private final A:Z

.field private final B:Landroid/view/accessibility/AccessibilityManager;

.field private C:Z

.field private D:Z

.field private E:Landroid/support/v4/widget/x;

.field private F:Landroid/support/v4/widget/x;

.field private G:Landroid/support/v4/widget/x;

.field private H:Landroid/support/v4/widget/x;

.field private I:I

.field private J:I

.field private K:Landroid/view/VelocityTracker;

.field private L:I

.field private M:I

.field private N:I

.field private O:I

.field private final P:I

.field private final Q:I

.field private final R:I

.field private final S:Landroid/support/v7/widget/cr;

.field private T:Landroid/support/v7/widget/ch;

.field private U:Landroid/support/v7/widget/ca;

.field private V:Z

.field private W:Landroid/support/v7/widget/ct;

.field final a:Landroid/support/v7/widget/cj;

.field private aa:Ljava/lang/Runnable;

.field b:Landroid/support/v7/widget/n;

.field c:Landroid/support/v7/widget/x;

.field final d:Ljava/util/List;

.field e:Landroid/support/v7/widget/by;

.field final f:Landroid/support/v7/widget/cp;

.field g:Z

.field h:Z

.field private final j:Landroid/support/v7/widget/cl;

.field private k:Landroid/support/v7/widget/RecyclerView$SavedState;

.field private l:Z

.field private final m:Ljava/lang/Runnable;

.field private final n:Landroid/graphics/Rect;

.field private o:Landroid/support/v7/widget/bv;

.field private p:Landroid/support/v7/widget/ce;

.field private q:Landroid/support/v7/widget/ck;

.field private final r:Ljava/util/ArrayList;

.field private final s:Ljava/util/ArrayList;

.field private t:Landroid/support/v7/widget/cg;

.field private u:Z

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 98
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-eq v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x14

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Landroid/support/v7/widget/RecyclerView;->i:Z

    .line 254
    new-instance v0, Landroid/support/v7/widget/bs;

    invoke-direct {v0}, Landroid/support/v7/widget/bs;-><init>()V

    sput-object v0, Landroid/support/v7/widget/RecyclerView;->ab:Landroid/view/animation/Interpolator;

    return-void

    .line 98
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 263
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v7/widget/RecyclerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 267
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 270
    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 111
    new-instance v0, Landroid/support/v7/widget/cl;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/widget/cl;-><init>(Landroid/support/v7/widget/RecyclerView;B)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/support/v7/widget/cl;

    .line 113
    new-instance v0, Landroid/support/v7/widget/cj;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/cj;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    .line 122
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Ljava/util/List;

    .line 136
    new-instance v0, Landroid/support/v7/widget/bq;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/bq;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Ljava/lang/Runnable;

    .line 160
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    .line 164
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    .line 165
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->s:Ljava/util/ArrayList;

    .line 181
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->C:Z

    .line 191
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->D:Z

    .line 195
    new-instance v0, Landroid/support/v7/widget/aa;

    invoke-direct {v0}, Landroid/support/v7/widget/aa;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    .line 220
    iput v2, p0, Landroid/support/v7/widget/RecyclerView;->I:I

    .line 221
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->J:I

    .line 231
    new-instance v0, Landroid/support/v7/widget/cr;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/cr;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->S:Landroid/support/v7/widget/cr;

    .line 233
    new-instance v0, Landroid/support/v7/widget/cp;

    invoke-direct {v0}, Landroid/support/v7/widget/cp;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    .line 238
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->g:Z

    .line 239
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->h:Z

    .line 240
    new-instance v0, Landroid/support/v7/widget/cb;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/widget/cb;-><init>(Landroid/support/v7/widget/RecyclerView;B)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->U:Landroid/support/v7/widget/ca;

    .line 242
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    .line 244
    new-instance v0, Landroid/support/v7/widget/br;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/br;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aa:Ljava/lang/Runnable;

    .line 272
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    .line 273
    const/16 v3, 0x10

    if-lt v0, v3, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Z

    .line 275
    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    .line 276
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v3

    iput v3, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    .line 277
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMinimumFlingVelocity()I

    move-result v3

    iput v3, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    .line 278
    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledMaximumFlingVelocity()I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    .line 279
    invoke-static {p0}, Landroid/support/v4/view/ay;->a(Landroid/view/View;)I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v2, v1

    :cond_0
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->setWillNotDraw(Z)V

    .line 281
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->U:Landroid/support/v7/widget/ca;

    iput-object v2, v0, Landroid/support/v7/widget/by;->h:Landroid/support/v7/widget/ca;

    .line 282
    new-instance v0, Landroid/support/v7/widget/n;

    new-instance v2, Landroid/support/v7/widget/bu;

    invoke-direct {v2, p0}, Landroid/support/v7/widget/bu;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-direct {v0, v2}, Landroid/support/v7/widget/n;-><init>(Landroid/support/v7/widget/o;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    .line 283
    new-instance v0, Landroid/support/v7/widget/x;

    new-instance v2, Landroid/support/v7/widget/bt;

    invoke-direct {v2, p0}, Landroid/support/v7/widget/bt;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    invoke-direct {v0, v2}, Landroid/support/v7/widget/x;-><init>(Landroid/support/v7/widget/z;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    .line 285
    invoke-static {p0}, Landroid/support/v4/view/ay;->e(Landroid/view/View;)I

    move-result v0

    if-nez v0, :cond_1

    .line 287
    invoke-static {p0, v1}, Landroid/support/v4/view/ay;->c(Landroid/view/View;I)V

    .line 290
    :cond_1
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "accessibility"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/view/accessibility/AccessibilityManager;

    .line 292
    new-instance v0, Landroid/support/v7/widget/ct;

    invoke-direct {v0, p0}, Landroid/support/v7/widget/ct;-><init>(Landroid/support/v7/widget/RecyclerView;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/support/v7/widget/ct;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/support/v7/widget/ct;

    invoke-static {p0, v0}, Landroid/support/v4/view/ay;->a(Landroid/view/View;Landroid/support/v4/view/a;)V

    .line 293
    return-void

    :cond_2
    move v0, v2

    .line 273
    goto :goto_0
.end method

.method private a(Landroid/support/v7/widget/cs;)J
    .locals 2

    .prologue
    .line 2036
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    iget-boolean v0, v0, Landroid/support/v7/widget/bv;->b:Z

    if-eqz v0, :cond_0

    iget-wide v0, p1, Landroid/support/v7/widget/cs;->d:J

    :goto_0
    return-wide v0

    :cond_0
    iget v0, p1, Landroid/support/v7/widget/cs;->b:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method private a(Landroid/support/v4/g/a;)V
    .locals 8

    .prologue
    .line 2044
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v7

    .line 2045
    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_3

    .line 2046
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Ljava/util/List;

    invoke-interface {v0, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/view/View;

    .line 2047
    invoke-static {v5}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v1

    .line 2048
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->b:Landroid/support/v4/g/a;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cd;

    .line 2049
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-boolean v2, v2, Landroid/support/v7/widget/cp;->i:Z

    if-nez v2, :cond_0

    .line 2050
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v2, v2, Landroid/support/v7/widget/cp;->c:Landroid/support/v4/g/a;

    invoke-virtual {v2, v1}, Landroid/support/v4/g/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 2052
    :cond_0
    invoke-virtual {p1, v5}, Landroid/support/v4/g/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 2053
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    invoke-virtual {v0, v5, v1}, Landroid/support/v7/widget/ce;->a(Landroid/view/View;Landroid/support/v7/widget/cj;)V

    .line 2045
    :goto_1
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 2056
    :cond_1
    if-eqz v0, :cond_2

    .line 2057
    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/cd;)V

    goto :goto_1

    .line 2060
    :cond_2
    new-instance v0, Landroid/support/v7/widget/cd;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/cd;-><init>(Landroid/support/v7/widget/cs;IIII)V

    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/cd;)V

    goto :goto_1

    .line 2064
    :cond_3
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 2065
    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 0

    .prologue
    .line 86
    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->detachViewFromParent(I)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->g(II)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    :cond_0
    return-void
.end method

.method static synthetic a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 0

    .prologue
    .line 86
    invoke-virtual {p0, p1, p2, p3}, Landroid/support/v7/widget/RecyclerView;->attachViewToParent(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(Landroid/support/v7/widget/cd;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 2094
    iget-object v0, p1, Landroid/support/v7/widget/cd;->a:Landroid/support/v7/widget/cs;

    iget-object v0, v0, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    .line 2095
    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/view/View;)V

    .line 2096
    iget v2, p1, Landroid/support/v7/widget/cd;->b:I

    .line 2097
    iget v3, p1, Landroid/support/v7/widget/cd;->c:I

    .line 2098
    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v4

    .line 2099
    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v5

    .line 2100
    if-ne v2, v4, :cond_0

    if-eq v3, v5, :cond_2

    .line 2101
    :cond_0
    iget-object v1, p1, Landroid/support/v7/widget/cd;->a:Landroid/support/v7/widget/cs;

    invoke-virtual {v1, v6}, Landroid/support/v7/widget/cs;->a(Z)V

    .line 2102
    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v1

    add-int/2addr v1, v4

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v6

    add-int/2addr v6, v5

    invoke-virtual {v0, v4, v5, v1, v6}, Landroid/view/View;->layout(IIII)V

    .line 2109
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    iget-object v1, p1, Landroid/support/v7/widget/cd;->a:Landroid/support/v7/widget/cs;

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/by;->a(Landroid/support/v7/widget/cs;IIII)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 2111
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->u()V

    .line 2123
    :cond_1
    :goto_0
    return-void

    .line 2118
    :cond_2
    iget-object v0, p1, Landroid/support/v7/widget/cd;->a:Landroid/support/v7/widget/cs;

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/cs;->a(Z)V

    .line 2119
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    iget-object v1, p1, Landroid/support/v7/widget/cd;->a:Landroid/support/v7/widget/cs;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/by;->a(Landroid/support/v7/widget/cs;)Z

    .line 2120
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->u()V

    goto :goto_0
.end method

.method private a(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f000000    # 0.5f

    .line 1657
    invoke-static {p1}, Landroid/support/v4/view/ao;->b(Landroid/view/MotionEvent;)I

    move-result v0

    .line 1658
    invoke-static {p1, v0}, Landroid/support/v4/view/ao;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->J:I

    if-ne v1, v2, :cond_0

    .line 1660
    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 1661
    :goto_0
    invoke-static {p1, v0}, Landroid/support/v4/view/ao;->b(Landroid/view/MotionEvent;I)I

    move-result v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->J:I

    .line 1662
    invoke-static {p1, v0}, Landroid/support/v4/view/ao;->c(Landroid/view/MotionEvent;I)F

    move-result v1

    add-float/2addr v1, v3

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    iput v1, p0, Landroid/support/v7/widget/RecyclerView;->L:I

    .line 1663
    invoke-static {p1, v0}, Landroid/support/v4/view/ao;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v3

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->O:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    .line 1665
    :cond_0
    return-void

    .line 1660
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    return v0
.end method

.method static synthetic a(Landroid/support/v7/widget/RecyclerView;Z)Z
    .locals 0

    .prologue
    .line 86
    iput-boolean p1, p0, Landroid/support/v7/widget/RecyclerView;->D:Z

    return p1
.end method

.method static b(Landroid/view/View;)Landroid/support/v7/widget/cs;
    .locals 1

    .prologue
    .line 2494
    if-nez p0, :cond_0

    .line 2495
    const/4 v0, 0x0

    .line 2497
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cf;

    iget-object v0, v0, Landroid/support/v7/widget/cf;->a:Landroid/support/v7/widget/cs;

    goto :goto_0
.end method

.method static synthetic b(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->e(I)V

    return-void
.end method

.method static synthetic b(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 0

    .prologue
    .line 86
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->setMeasuredDimension(II)V

    return-void
.end method

.method static synthetic b(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V
    .locals 2

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bv;->a(Landroid/support/v7/widget/cs;)V

    :cond_0
    return-void
.end method

.method static synthetic b(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Z

    return v0
.end method

.method public static c(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 2507
    invoke-static {p0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v0

    .line 2508
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/cs;->c()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method static synthetic c(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic c(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Z

    return v0
.end method

.method static synthetic d(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic d(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    return v0
.end method

.method static synthetic e(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/ce;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    return-object v0
.end method

.method private e(I)V
    .locals 1

    .prologue
    .line 740
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->I:I

    if-ne p1, v0, :cond_0

    .line 754
    :goto_0
    return-void

    .line 746
    :cond_0
    iput p1, p0, Landroid/support/v7/widget/RecyclerView;->I:I

    .line 747
    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    .line 748
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->l()V

    .line 750
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->T:Landroid/support/v7/widget/ch;

    if-eqz v0, :cond_2

    .line 751
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->T:Landroid/support/v7/widget/ch;

    invoke-virtual {v0, p0, p1}, Landroid/support/v7/widget/ch;->a(Landroid/support/v7/widget/RecyclerView;I)V

    .line 753
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/ce;->g(I)V

    goto :goto_0
.end method

.method static synthetic e(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method private e(Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 642
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-ne v0, p0, :cond_0

    move v0, v1

    .line 643
    :goto_0
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    invoke-virtual {p0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/cj;->b(Landroid/support/v7/widget/cs;)V

    .line 644
    if-nez v0, :cond_1

    .line 645
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    const/4 v2, -0x1

    invoke-virtual {v0, p1, v2, v1}, Landroid/support/v7/widget/x;->a(Landroid/view/View;IZ)V

    .line 649
    :goto_1
    return-void

    .line 642
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 647
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    iget-object v1, v0, Landroid/support/v7/widget/x;->a:Landroid/support/v7/widget/z;

    invoke-interface {v1, p1}, Landroid/support/v7/widget/z;->a(Landroid/view/View;)I

    move-result v1

    if-gez v1, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "view is not a child, cannot hide "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v2, v0, Landroid/support/v7/widget/x;->b:Landroid/support/v7/widget/y;

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/y;->a(I)V

    iget-object v0, v0, Landroid/support/v7/widget/x;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method private f(II)V
    .locals 13

    .prologue
    const/4 v1, 0x0

    .line 904
    .line 906
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->j()V

    .line 907
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    if-eqz v0, :cond_10

    .line 908
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    .line 909
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->D:Z

    .line 910
    if-eqz p1, :cond_f

    .line 911
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    invoke-virtual {v0, p1, v2, v3}, Landroid/support/v7/widget/ce;->a(ILandroid/support/v7/widget/cj;Landroid/support/v7/widget/cp;)I

    move-result v3

    .line 912
    sub-int v4, p1, v3

    .line 914
    :goto_0
    if-eqz p2, :cond_e

    .line 915
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    invoke-virtual {v0, p2, v2, v5}, Landroid/support/v7/widget/ce;->b(ILandroid/support/v7/widget/cj;Landroid/support/v7/widget/cp;)I

    move-result v0

    .line 916
    sub-int v2, p2, v0

    .line 918
    :goto_1
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->t()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 920
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v5}, Landroid/support/v7/widget/x;->a()I

    move-result v7

    move v6, v1

    .line 921
    :goto_2
    if-ge v6, v7, :cond_3

    .line 922
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/x;->b(I)Landroid/view/View;

    move-result-object v8

    .line 923
    invoke-virtual {p0, v8}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v5

    .line 924
    if-eqz v5, :cond_1

    iget-object v9, v5, Landroid/support/v7/widget/cs;->h:Landroid/support/v7/widget/cs;

    if-eqz v9, :cond_1

    .line 925
    iget-object v5, v5, Landroid/support/v7/widget/cs;->h:Landroid/support/v7/widget/cs;

    .line 926
    if-eqz v5, :cond_2

    iget-object v5, v5, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    .line 927
    :goto_3
    if-eqz v5, :cond_1

    .line 928
    invoke-virtual {v8}, Landroid/view/View;->getLeft()I

    move-result v9

    .line 929
    invoke-virtual {v8}, Landroid/view/View;->getTop()I

    move-result v8

    .line 930
    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v10

    if-ne v9, v10, :cond_0

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v10

    if-eq v8, v10, :cond_1

    .line 931
    :cond_0
    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v10

    add-int/2addr v10, v9

    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v11

    add-int/2addr v11, v8

    invoke-virtual {v5, v9, v8, v10, v11}, Landroid/view/View;->layout(IIII)V

    .line 921
    :cond_1
    add-int/lit8 v5, v6, 0x1

    move v6, v5

    goto :goto_2

    .line 926
    :cond_2
    const/4 v5, 0x0

    goto :goto_3

    .line 939
    :cond_3
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->D:Z

    .line 940
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    move v12, v3

    move v3, v2

    move v2, v12

    .line 942
    :goto_4
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_4

    .line 943
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 945
    :cond_4
    invoke-static {p0}, Landroid/support/v4/view/ay;->a(Landroid/view/View;)I

    move-result v5

    const/4 v6, 0x2

    if-eq v5, v6, :cond_8

    .line 946
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->g(II)V

    .line 947
    if-gez v4, :cond_c

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->n()V

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    neg-int v6, v4

    int-to-float v6, v6

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/support/v4/widget/x;->a(F)Z

    :cond_5
    :goto_5
    if-gez v3, :cond_d

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->p()V

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    neg-int v6, v3

    int-to-float v6, v6

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/support/v4/widget/x;->a(F)Z

    :cond_6
    :goto_6
    if-nez v4, :cond_7

    if-eqz v3, :cond_8

    :cond_7
    invoke-static {p0}, Landroid/support/v4/view/ay;->d(Landroid/view/View;)V

    .line 949
    :cond_8
    if-nez v2, :cond_9

    if-eqz v0, :cond_a

    .line 950
    :cond_9
    invoke-virtual {p0, v1, v1, v1, v1}, Landroid/support/v7/widget/RecyclerView;->onScrollChanged(IIII)V

    .line 951
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->T:Landroid/support/v7/widget/ch;

    if-eqz v1, :cond_a

    .line 952
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->T:Landroid/support/v7/widget/ch;

    invoke-virtual {v1, p0, v2, v0}, Landroid/support/v7/widget/ch;->a(Landroid/support/v7/widget/RecyclerView;II)V

    .line 955
    :cond_a
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    move-result v0

    if-nez v0, :cond_b

    .line 956
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->invalidate()V

    .line 958
    :cond_b
    return-void

    .line 947
    :cond_c
    if-lez v4, :cond_5

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->o()V

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    int-to-float v6, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/support/v4/widget/x;->a(F)Z

    goto :goto_5

    :cond_d
    if-lez v3, :cond_6

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->q()V

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    int-to-float v6, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float/2addr v6, v7

    invoke-virtual {v5, v6}, Landroid/support/v4/widget/x;->a(F)Z

    goto :goto_6

    :cond_e
    move v0, v1

    move v2, v1

    goto/16 :goto_1

    :cond_f
    move v3, v1

    move v4, v1

    goto/16 :goto_0

    :cond_10
    move v0, v1

    move v2, v1

    move v3, v1

    move v4, v1

    goto/16 :goto_4
.end method

.method static synthetic f(Landroid/support/v7/widget/RecyclerView;)V
    .locals 0

    .prologue
    .line 86
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->j()V

    return-void
.end method

.method static synthetic f(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic g(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/bv;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    return-object v0
.end method

.method private g(II)V
    .locals 2

    .prologue
    .line 1199
    const/4 v0, 0x0

    .line 1200
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    invoke-virtual {v1}, Landroid/support/v4/widget/x;->a()Z

    move-result v1

    if-nez v1, :cond_0

    if-lez p1, :cond_0

    .line 1201
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    invoke-virtual {v0}, Landroid/support/v4/widget/x;->c()Z

    move-result v0

    .line 1203
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    invoke-virtual {v1}, Landroid/support/v4/widget/x;->a()Z

    move-result v1

    if-nez v1, :cond_1

    if-gez p1, :cond_1

    .line 1204
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    invoke-virtual {v1}, Landroid/support/v4/widget/x;->c()Z

    move-result v1

    or-int/2addr v0, v1

    .line 1206
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    invoke-virtual {v1}, Landroid/support/v4/widget/x;->a()Z

    move-result v1

    if-nez v1, :cond_2

    if-lez p2, :cond_2

    .line 1207
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    invoke-virtual {v1}, Landroid/support/v4/widget/x;->c()Z

    move-result v1

    or-int/2addr v0, v1

    .line 1209
    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    invoke-virtual {v1}, Landroid/support/v4/widget/x;->a()Z

    move-result v1

    if-nez v1, :cond_3

    if-gez p2, :cond_3

    .line 1210
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    invoke-virtual {v1}, Landroid/support/v4/widget/x;->c()Z

    move-result v1

    or-int/2addr v0, v1

    .line 1212
    :cond_3
    if-eqz v0, :cond_4

    .line 1213
    invoke-static {p0}, Landroid/support/v4/view/ay;->d(Landroid/view/View;)V

    .line 1215
    :cond_4
    return-void
.end method

.method static synthetic g(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic h()Landroid/view/animation/Interpolator;
    .locals 1

    .prologue
    .line 86
    sget-object v0, Landroid/support/v7/widget/RecyclerView;->ab:Landroid/view/animation/Interpolator;

    return-object v0
.end method

.method static synthetic h(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 86
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    iget-object v3, v2, Landroid/support/v7/widget/x;->a:Landroid/support/v7/widget/z;

    invoke-interface {v3, p1}, Landroid/support/v7/widget/z;->a(Landroid/view/View;)I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    iget-object v2, v2, Landroid/support/v7/widget/x;->c:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    :goto_0
    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v0

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/cj;->b(Landroid/support/v7/widget/cs;)V

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/cj;->a(Landroid/support/v7/widget/cs;)V

    :cond_0
    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    return-void

    :cond_1
    iget-object v4, v2, Landroid/support/v7/widget/x;->b:Landroid/support/v7/widget/y;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/y;->b(I)Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v4, v2, Landroid/support/v7/widget/x;->b:Landroid/support/v7/widget/y;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/y;->c(I)Z

    iget-object v4, v2, Landroid/support/v7/widget/x;->a:Landroid/support/v7/widget/z;

    invoke-interface {v4, v3}, Landroid/support/v7/widget/z;->a(I)V

    iget-object v2, v2, Landroid/support/v7/widget/x;->c:Ljava/util/List;

    invoke-interface {v2, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method static synthetic h(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 86
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->t()Z

    move-result v0

    return v0
.end method

.method static synthetic i(Landroid/support/v7/widget/RecyclerView;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic i(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;)V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    return-void
.end method

.method static synthetic i()Z
    .locals 1

    .prologue
    .line 86
    sget-boolean v0, Landroid/support/v7/widget/RecyclerView;->i:Z

    return v0
.end method

.method private j()V
    .locals 1

    .prologue
    .line 895
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    invoke-virtual {v0}, Landroid/support/v7/widget/n;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 896
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 898
    :cond_0
    return-void
.end method

.method static synthetic j(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 86
    invoke-virtual {p0, v0, v0, v0, v0}, Landroid/support/v7/widget/RecyclerView;->onScrollChanged(IIII)V

    return-void
.end method

.method static synthetic k(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/ch;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->T:Landroid/support/v7/widget/ch;

    return-object v0
.end method

.method private k()V
    .locals 1

    .prologue
    .line 1150
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->e(I)V

    .line 1151
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->l()V

    .line 1152
    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    .line 1158
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->S:Landroid/support/v7/widget/cr;

    invoke-virtual {v0}, Landroid/support/v7/widget/cr;->a()V

    .line 1159
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->r()V

    .line 1160
    return-void
.end method

.method static synthetic l(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 86
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    move-result v0

    return v0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 1188
    const/4 v0, 0x0

    .line 1189
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    invoke-virtual {v0}, Landroid/support/v4/widget/x;->c()Z

    move-result v0

    .line 1190
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_1

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    invoke-virtual {v1}, Landroid/support/v4/widget/x;->c()Z

    move-result v1

    or-int/2addr v0, v1

    .line 1191
    :cond_1
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    invoke-virtual {v1}, Landroid/support/v4/widget/x;->c()Z

    move-result v1

    or-int/2addr v0, v1

    .line 1192
    :cond_2
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    if-eqz v1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    invoke-virtual {v1}, Landroid/support/v4/widget/x;->c()Z

    move-result v1

    or-int/2addr v0, v1

    .line 1193
    :cond_3
    if-eqz v0, :cond_4

    .line 1194
    invoke-static {p0}, Landroid/support/v4/view/ay;->d(Landroid/view/View;)V

    .line 1196
    :cond_4
    return-void
.end method

.method static synthetic m(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Z

    return v0
.end method

.method private n()V
    .locals 4

    .prologue
    .line 1240
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    if-eqz v0, :cond_0

    .line 1250
    :goto_0
    return-void

    .line 1243
    :cond_0
    new-instance v0, Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    .line 1244
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v0, :cond_1

    .line 1245
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/x;->a(II)V

    goto :goto_0

    .line 1248
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/x;->a(II)V

    goto :goto_0
.end method

.method static synthetic n(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->A:Z

    return v0
.end method

.method private o()V
    .locals 4

    .prologue
    .line 1253
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    if-eqz v0, :cond_0

    .line 1263
    :goto_0
    return-void

    .line 1256
    :cond_0
    new-instance v0, Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    .line 1257
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v0, :cond_1

    .line 1258
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/x;->a(II)V

    goto :goto_0

    .line 1261
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/x;->a(II)V

    goto :goto_0
.end method

.method static synthetic o(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    return v0
.end method

.method private p()V
    .locals 4

    .prologue
    .line 1266
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    if-eqz v0, :cond_0

    .line 1277
    :goto_0
    return-void

    .line 1269
    :cond_0
    new-instance v0, Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    .line 1270
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v0, :cond_1

    .line 1271
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/x;->a(II)V

    goto :goto_0

    .line 1274
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/x;->a(II)V

    goto :goto_0
.end method

.method static synthetic p(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    return v0
.end method

.method static synthetic q(Landroid/support/v7/widget/RecyclerView;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->m:Ljava/lang/Runnable;

    return-object v0
.end method

.method private q()V
    .locals 4

    .prologue
    .line 1280
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    if-eqz v0, :cond_0

    .line 1290
    :goto_0
    return-void

    .line 1283
    :cond_0
    new-instance v0, Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v4/widget/x;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    .line 1284
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v0, :cond_1

    .line 1285
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/x;->a(II)V

    goto :goto_0

    .line 1288
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/widget/x;->a(II)V

    goto :goto_0
.end method

.method private r()V
    .locals 1

    .prologue
    .line 1293
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    .line 1294
    return-void
.end method

.method static synthetic r(Landroid/support/v7/widget/RecyclerView;)Z
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    return v0
.end method

.method static synthetic s(Landroid/support/v7/widget/RecyclerView;)Landroid/view/accessibility/AccessibilityManager;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->B:Landroid/view/accessibility/AccessibilityManager;

    return-object v0
.end method

.method private s()V
    .locals 1

    .prologue
    .line 1649
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/view/VelocityTracker;

    if-eqz v0, :cond_0

    .line 1650
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 1652
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->m()V

    .line 1653
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->e(I)V

    .line 1654
    return-void
.end method

.method static synthetic t(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/ct;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->W:Landroid/support/v7/widget/ct;

    return-object v0
.end method

.method private t()Z
    .locals 1

    .prologue
    .line 1742
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    iget-boolean v0, v0, Landroid/support/v7/widget/by;->n:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic u(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/ck;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->q:Landroid/support/v7/widget/ck;

    return-object v0
.end method

.method private u()V
    .locals 1

    .prologue
    .line 1750
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    if-eqz v0, :cond_0

    .line 1751
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aa:Ljava/lang/Runnable;

    invoke-static {p0, v0}, Landroid/support/v4/view/ay;->a(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 1752
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    .line 1754
    :cond_0
    return-void
.end method

.method static synthetic v(Landroid/support/v7/widget/RecyclerView;)Landroid/support/v7/widget/cr;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->S:Landroid/support/v7/widget/cr;

    return-object v0
.end method

.method private v()V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 1767
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Z

    if-eqz v0, :cond_0

    .line 1770
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    invoke-virtual {v0}, Landroid/support/v7/widget/n;->a()V

    .line 1771
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->y()V

    .line 1772
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/RecyclerView;)V

    .line 1777
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->c()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1778
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    invoke-virtual {v0}, Landroid/support/v7/widget/n;->b()V

    .line 1782
    :goto_0
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Z

    if-eqz v0, :cond_2

    :cond_1
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->g:Z

    if-nez v0, :cond_2

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Z

    if-eqz v0, :cond_6

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->t()Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_2
    move v0, v2

    .line 1784
    :goto_1
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    if-eqz v3, :cond_7

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    if-eqz v3, :cond_7

    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->C:Z

    if-nez v3, :cond_3

    if-nez v0, :cond_3

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-static {v3}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/ce;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_3
    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->C:Z

    if-eqz v3, :cond_4

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    iget-boolean v3, v3, Landroid/support/v7/widget/bv;->b:Z

    if-eqz v3, :cond_7

    :cond_4
    move v3, v2

    :goto_2
    iput-boolean v3, v4, Landroid/support/v7/widget/cp;->j:Z

    .line 1788
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-boolean v4, v4, Landroid/support/v7/widget/cp;->j:Z

    if-eqz v4, :cond_9

    if-eqz v0, :cond_9

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->C:Z

    if-nez v0, :cond_9

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    if-eqz v0, :cond_8

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->c()Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v2

    :goto_3
    if-eqz v0, :cond_9

    :goto_4
    iput-boolean v2, v3, Landroid/support/v7/widget/cp;->k:Z

    .line 1791
    return-void

    .line 1780
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    invoke-virtual {v0}, Landroid/support/v7/widget/n;->e()V

    goto :goto_0

    :cond_6
    move v0, v1

    .line 1782
    goto :goto_1

    :cond_7
    move v3, v1

    .line 1784
    goto :goto_2

    :cond_8
    move v0, v1

    .line 1788
    goto :goto_3

    :cond_9
    move v2, v1

    goto :goto_4
.end method

.method private w()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 2170
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->b()I

    move-result v3

    move v2, v1

    .line 2171
    :goto_0
    if-ge v2, v3, :cond_0

    .line 2172
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/x;->c(I)Landroid/view/View;

    move-result-object v0

    .line 2173
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cf;

    iput-boolean v4, v0, Landroid/support/v7/widget/cf;->c:Z

    .line 2171
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2175
    :cond_0
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v0, v2, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    :goto_1
    if-ge v1, v3, :cond_2

    iget-object v0, v2, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cs;

    iget-object v0, v0, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cf;

    if-eqz v0, :cond_1

    iput-boolean v4, v0, Landroid/support/v7/widget/cf;->c:Z

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2176
    :cond_2
    return-void
.end method

.method private x()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2293
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->b()I

    move-result v2

    move v0, v1

    .line 2294
    :goto_0
    if-ge v0, v2, :cond_1

    .line 2295
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/x;->c(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v3

    .line 2296
    invoke-virtual {v3}, Landroid/support/v7/widget/cs;->b()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2297
    invoke-virtual {v3}, Landroid/support/v7/widget/cs;->a()V

    .line 2294
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2300
    :cond_1
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v0, v3, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    iget-object v0, v3, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cs;

    invoke-virtual {v0}, Landroid/support/v7/widget/cs;->a()V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    iget-object v0, v3, Landroid/support/v7/widget/cj;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_2
    if-ge v2, v4, :cond_3

    iget-object v0, v3, Landroid/support/v7/widget/cj;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cs;

    invoke-virtual {v0}, Landroid/support/v7/widget/cs;->a()V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_3
    iget-object v0, v3, Landroid/support/v7/widget/cj;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, v3, Landroid/support/v7/widget/cj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_3
    if-ge v1, v2, :cond_4

    iget-object v0, v3, Landroid/support/v7/widget/cj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cs;

    invoke-virtual {v0}, Landroid/support/v7/widget/cs;->a()V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 2301
    :cond_4
    return-void
.end method

.method private y()V
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v5, 0x6

    .line 2451
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v1}, Landroid/support/v7/widget/x;->b()I

    move-result v2

    move v1, v0

    .line 2452
    :goto_0
    if-ge v1, v2, :cond_1

    .line 2453
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/x;->c(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v3

    .line 2454
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/cs;->b()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2455
    invoke-virtual {v3, v5}, Landroid/support/v7/widget/cs;->a(I)V

    .line 2452
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2458
    :cond_1
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->w()V

    .line 2459
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v1, v2, Landroid/support/v7/widget/cj;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    if-eqz v1, :cond_3

    iget-object v1, v2, Landroid/support/v7/widget/cj;->e:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    iget-boolean v1, v1, Landroid/support/v7/widget/bv;->b:Z

    if-eqz v1, :cond_3

    iget-object v1, v2, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_5

    iget-object v0, v2, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cs;

    if-eqz v0, :cond_2

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/cs;->a(I)V

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    iget-object v0, v2, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_5

    invoke-virtual {v2, v1}, Landroid/support/v7/widget/cj;->c(I)Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, v2, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cs;

    invoke-virtual {v0, v5}, Landroid/support/v7/widget/cs;->a(I)V

    :cond_4
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    .line 2460
    :cond_5
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)Landroid/support/v7/widget/cs;
    .locals 3

    .prologue
    .line 2485
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .line 2486
    if-eqz v0, :cond_0

    if-eq v0, p0, :cond_0

    .line 2487
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "View "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a direct child of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2490
    :cond_0
    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 448
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->v:Z

    .line 449
    return-void
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 843
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->k()V

    .line 844
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ce;->c(I)V

    .line 845
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->awakenScrollBars()Z

    .line 846
    return-void
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 1115
    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    .line 1116
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->S:Landroid/support/v7/widget/cr;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/cr;->b(II)V

    .line 1118
    :cond_1
    return-void
.end method

.method final a(IIZ)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    .line 2356
    add-int v2, p1, p2

    .line 2357
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->b()I

    move-result v1

    .line 2358
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    .line 2359
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/x;->c(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v3

    .line 2360
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/cs;->b()Z

    move-result v4

    if-nez v4, :cond_0

    .line 2361
    iget v4, v3, Landroid/support/v7/widget/cs;->b:I

    if-lt v4, v2, :cond_1

    .line 2367
    neg-int v4, p2

    invoke-virtual {v3, v4, p3}, Landroid/support/v7/widget/cs;->a(IZ)V

    .line 2368
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput-boolean v7, v3, Landroid/support/v7/widget/cp;->h:Z

    .line 2358
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2369
    :cond_1
    iget v4, v3, Landroid/support/v7/widget/cs;->b:I

    if-lt v4, p1, :cond_0

    .line 2374
    add-int/lit8 v4, p1, -0x1

    neg-int v5, p2

    const/16 v6, 0x8

    invoke-virtual {v3, v6}, Landroid/support/v7/widget/cs;->a(I)V

    invoke-virtual {v3, v5, p3}, Landroid/support/v7/widget/cs;->a(IZ)V

    iput v4, v3, Landroid/support/v7/widget/cs;->b:I

    .line 2376
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput-boolean v7, v3, Landroid/support/v7/widget/cp;->h:Z

    goto :goto_1

    .line 2380
    :cond_2
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v0, v3, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_2
    if-ltz v1, :cond_5

    iget-object v0, v3, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cs;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/support/v7/widget/cs;->c()I

    move-result v4

    if-lt v4, v2, :cond_4

    neg-int v4, p2

    invoke-virtual {v0, v4, p3}, Landroid/support/v7/widget/cs;->a(IZ)V

    :cond_3
    :goto_3
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_2

    :cond_4
    invoke-virtual {v0}, Landroid/support/v7/widget/cs;->c()I

    move-result v4

    if-lt v4, p1, :cond_3

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/cj;->c(I)Z

    move-result v4

    if-nez v4, :cond_3

    const/4 v4, 0x4

    invoke-virtual {v0, v4}, Landroid/support/v7/widget/cs;->a(I)V

    goto :goto_3

    .line 2381
    :cond_5
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 2382
    return-void
.end method

.method public final a(Landroid/support/v7/widget/bv;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 500
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/support/v7/widget/cl;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bv;->b(Landroid/support/v7/widget/bx;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    invoke-virtual {v0}, Landroid/support/v7/widget/by;->d()V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ce;->b(Landroid/support/v7/widget/cj;)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    invoke-virtual {v0, v1, v3}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/cj;Z)V

    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    invoke-virtual {v0}, Landroid/support/v7/widget/n;->a()V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    if-eqz p1, :cond_3

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->j:Landroid/support/v7/widget/cl;

    invoke-virtual {p1, v1}, Landroid/support/v7/widget/bv;->a(Landroid/support/v7/widget/bx;)V

    :cond_3
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    if-eqz v1, :cond_4

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    invoke-virtual {v1, v0, v2}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/bv;Landroid/support/v7/widget/bv;)V

    :cond_4
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    invoke-virtual {v1}, Landroid/support/v7/widget/cj;->a()V

    invoke-virtual {v1}, Landroid/support/v7/widget/cj;->b()Landroid/support/v7/widget/ci;

    move-result-object v1

    if-eqz v0, :cond_5

    iget v0, v1, Landroid/support/v7/widget/ci;->b:I

    add-int/lit8 v0, v0, -0x1

    iput v0, v1, Landroid/support/v7/widget/ci;->b:I

    :cond_5
    iget v0, v1, Landroid/support/v7/widget/ci;->b:I

    if-nez v0, :cond_6

    iget-object v0, v1, Landroid/support/v7/widget/ci;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    :cond_6
    if-eqz v2, :cond_7

    iget v0, v1, Landroid/support/v7/widget/ci;->b:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Landroid/support/v7/widget/ci;->b:I

    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput-boolean v3, v0, Landroid/support/v7/widget/cp;->h:Z

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->y()V

    .line 501
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 502
    return-void
.end method

.method public final a(Landroid/support/v7/widget/by;)V
    .locals 2

    .prologue
    .line 1718
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    if-eqz v0, :cond_0

    .line 1719
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    invoke-virtual {v0}, Landroid/support/v7/widget/by;->d()V

    .line 1720
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/by;->h:Landroid/support/v7/widget/ca;

    .line 1722
    :cond_0
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    .line 1723
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    if-eqz v0, :cond_1

    .line 1724
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->U:Landroid/support/v7/widget/ca;

    iput-object v1, v0, Landroid/support/v7/widget/by;->h:Landroid/support/v7/widget/ca;

    .line 1726
    :cond_1
    return-void
.end method

.method public final a(Landroid/support/v7/widget/cc;)V
    .locals 2

    .prologue
    .line 800
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    const-string v1, "Cannot add item decoration during a scroll  or layout"

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ce;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->setWillNotDraw(Z)V

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->w()V

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 801
    return-void
.end method

.method public final a(Landroid/support/v7/widget/ce;)V
    .locals 4

    .prologue
    .line 583
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    if-ne p1, v0, :cond_0

    .line 608
    :goto_0
    return-void

    .line 588
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_2

    .line 589
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    if-eqz v0, :cond_1

    .line 590
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    invoke-virtual {v0, p0, v1}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/cj;)V

    .line 592
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ce;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 594
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    invoke-virtual {v0}, Landroid/support/v7/widget/cj;->a()V

    .line 595
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    iget-object v0, v1, Landroid/support/v7/widget/x;->a:Landroid/support/v7/widget/z;

    invoke-interface {v0}, Landroid/support/v7/widget/z;->b()V

    iget-object v0, v1, Landroid/support/v7/widget/x;->b:Landroid/support/v7/widget/y;

    :goto_1
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Landroid/support/v7/widget/y;->a:J

    iget-object v2, v0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/y;

    if-eqz v2, :cond_3

    iget-object v0, v0, Landroid/support/v7/widget/y;->b:Landroid/support/v7/widget/y;

    goto :goto_1

    :cond_3
    iget-object v0, v1, Landroid/support/v7/widget/x;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 596
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    .line 597
    if-eqz p1, :cond_5

    .line 598
    iget-object v0, p1, Landroid/support/v7/widget/ce;->q:Landroid/support/v7/widget/RecyclerView;

    if-eqz v0, :cond_4

    .line 599
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "LayoutManager "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is already attached to a RecyclerView: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Landroid/support/v7/widget/ce;->q:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 602
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0, p0}, Landroid/support/v7/widget/ce;->b(Landroid/support/v7/widget/RecyclerView;)V

    .line 603
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    if-eqz v0, :cond_5

    .line 604
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    .line 607
    :cond_5
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/widget/ch;)V
    .locals 0

    .prologue
    .line 831
    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->T:Landroid/support/v7/widget/ch;

    .line 832
    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1390
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->D:Z

    if-eqz v0, :cond_1

    .line 1391
    if-nez p1, :cond_0

    .line 1392
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot call this method while RecyclerView is computing a layout or scrolling"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1395
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0, p1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1397
    :cond_1
    return-void
.end method

.method final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1098
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-eqz v0, :cond_1

    .line 1099
    if-eqz p1, :cond_0

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    if-eqz v0, :cond_0

    .line 1101
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->f()V

    .line 1103
    :cond_0
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    .line 1104
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->y:Z

    .line 1106
    :cond_1
    return-void
.end method

.method public addFocusables(Ljava/util/ArrayList;II)V
    .locals 1

    .prologue
    .line 1332
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    .line 1333
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addFocusables(Ljava/util/ArrayList;II)V

    .line 1335
    return-void
.end method

.method public final b()Landroid/support/v7/widget/bv;
    .locals 1

    .prologue
    .line 553
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    return-object v0
.end method

.method final b(I)Landroid/support/v7/widget/cs;
    .locals 4

    .prologue
    .line 2536
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->b()I

    move-result v2

    .line 2537
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 2538
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/x;->c(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v0

    .line 2539
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v7/widget/cs;->l()Z

    move-result v3

    if-nez v3, :cond_0

    .line 2540
    iget v3, v0, Landroid/support/v7/widget/cs;->b:I

    if-ne v3, p1, :cond_0

    .line 2552
    :goto_1
    return-object v0

    .line 2544
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2552
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method final b(II)V
    .locals 2

    .prologue
    .line 1218
    if-gez p1, :cond_4

    .line 1219
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->n()V

    .line 1220
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    neg-int v1, p1

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/x;->a(I)Z

    .line 1226
    :cond_0
    :goto_0
    if-gez p2, :cond_5

    .line 1227
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->p()V

    .line 1228
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    neg-int v1, p2

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/x;->a(I)Z

    .line 1234
    :cond_1
    :goto_1
    if-nez p1, :cond_2

    if-eqz p2, :cond_3

    .line 1235
    :cond_2
    invoke-static {p0}, Landroid/support/v4/view/ay;->d(Landroid/view/View;)V

    .line 1237
    :cond_3
    return-void

    .line 1221
    :cond_4
    if-lez p1, :cond_0

    .line 1222
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->o()V

    .line 1223
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/x;->a(I)Z

    goto :goto_0

    .line 1229
    :cond_5
    if-lez p2, :cond_1

    .line 1230
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->q()V

    .line 1231
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    invoke-virtual {v0, p2}, Landroid/support/v4/widget/x;->a(I)Z

    goto :goto_1
.end method

.method public final c()Landroid/support/v7/widget/ce;
    .locals 1

    .prologue
    .line 676
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    return-object v0
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 2607
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->a()I

    move-result v1

    .line 2608
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2609
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/x;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 2608
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2611
    :cond_0
    return-void
.end method

.method final c(II)V
    .locals 10

    .prologue
    const/4 v1, -0x1

    const/4 v2, 0x1

    const/4 v5, 0x0

    .line 2304
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->b()I

    move-result v7

    .line 2306
    if-ge p1, p2, :cond_1

    move v0, v1

    move v3, p2

    move v4, p1

    :goto_0
    move v6, v5

    .line 2316
    :goto_1
    if-ge v6, v7, :cond_3

    .line 2317
    iget-object v8, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v8, v6}, Landroid/support/v7/widget/x;->c(I)Landroid/view/View;

    move-result-object v8

    invoke-static {v8}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v8

    .line 2318
    if-eqz v8, :cond_0

    iget v9, v8, Landroid/support/v7/widget/cs;->b:I

    if-lt v9, v4, :cond_0

    iget v9, v8, Landroid/support/v7/widget/cs;->b:I

    if-gt v9, v3, :cond_0

    .line 2319
    iget v9, v8, Landroid/support/v7/widget/cs;->b:I

    if-ne v9, p1, :cond_2

    .line 2326
    sub-int v9, p2, p1

    invoke-virtual {v8, v9, v5}, Landroid/support/v7/widget/cs;->a(IZ)V

    .line 2331
    :goto_2
    iget-object v8, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput-boolean v2, v8, Landroid/support/v7/widget/cp;->h:Z

    .line 2316
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    :cond_1
    move v0, v2

    move v3, p1

    move v4, p2

    .line 2313
    goto :goto_0

    .line 2328
    :cond_2
    invoke-virtual {v8, v0, v5}, Landroid/support/v7/widget/cs;->a(IZ)V

    goto :goto_2

    .line 2333
    :cond_3
    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    if-ge p1, p2, :cond_5

    move v2, p2

    move v3, p1

    :goto_3
    iget-object v0, v6, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v4, v5

    :goto_4
    if-ge v4, v7, :cond_7

    iget-object v0, v6, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cs;

    if-eqz v0, :cond_4

    iget v8, v0, Landroid/support/v7/widget/cs;->b:I

    if-lt v8, v3, :cond_4

    iget v8, v0, Landroid/support/v7/widget/cs;->b:I

    if-gt v8, v2, :cond_4

    iget v8, v0, Landroid/support/v7/widget/cs;->b:I

    if-ne v8, p1, :cond_6

    sub-int v8, p2, p1

    invoke-virtual {v0, v8, v5}, Landroid/support/v7/widget/cs;->a(IZ)V

    :cond_4
    :goto_5
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_4

    :cond_5
    move v1, v2

    move v3, p2

    move v2, p1

    goto :goto_3

    :cond_6
    invoke-virtual {v0, v1, v5}, Landroid/support/v7/widget/cs;->a(IZ)V

    goto :goto_5

    .line 2334
    :cond_7
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 2335
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    .prologue
    .line 2251
    instance-of v0, p1, Landroid/support/v7/widget/cf;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    check-cast p1, Landroid/support/v7/widget/cf;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/cf;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected computeHorizontalScrollExtent()I
    .locals 2

    .prologue
    .line 1003
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ce;->c(Landroid/support/v7/widget/cp;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected computeHorizontalScrollOffset()I
    .locals 2

    .prologue
    .line 980
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/cp;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected computeHorizontalScrollRange()I
    .locals 2

    .prologue
    .line 1023
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ce;->e(Landroid/support/v7/widget/cp;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected computeVerticalScrollExtent()I
    .locals 2

    .prologue
    .line 1066
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ce;->d(Landroid/support/v7/widget/cp;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected computeVerticalScrollOffset()I
    .locals 2

    .prologue
    .line 1045
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ce;->b(Landroid/support/v7/widget/cp;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected computeVerticalScrollRange()I
    .locals 2

    .prologue
    .line 1086
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ce;->f(Landroid/support/v7/widget/cp;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final d(Landroid/view/View;)Landroid/graphics/Rect;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 2652
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cf;

    .line 2653
    iget-boolean v1, v0, Landroid/support/v7/widget/cf;->c:Z

    if-nez v1, :cond_0

    .line 2654
    iget-object v0, v0, Landroid/support/v7/widget/cf;->b:Landroid/graphics/Rect;

    .line 2669
    :goto_0
    return-object v0

    .line 2657
    :cond_0
    iget-object v2, v0, Landroid/support/v7/widget/cf;->b:Landroid/graphics/Rect;

    .line 2658
    invoke-virtual {v2, v4, v4, v4, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2659
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v4

    .line 2660
    :goto_1
    if-ge v3, v5, :cond_1

    .line 2661
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/graphics/Rect;->set(IIII)V

    .line 2662
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/cc;

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget-object v7, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    invoke-virtual {v1, v6, p1, p0}, Landroid/support/v7/widget/cc;->a(Landroid/graphics/Rect;Landroid/view/View;Landroid/support/v7/widget/RecyclerView;)V

    .line 2663
    iget v1, v2, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->left:I

    add-int/2addr v1, v6

    iput v1, v2, Landroid/graphics/Rect;->left:I

    .line 2664
    iget v1, v2, Landroid/graphics/Rect;->top:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->top:I

    add-int/2addr v1, v6

    iput v1, v2, Landroid/graphics/Rect;->top:I

    .line 2665
    iget v1, v2, Landroid/graphics/Rect;->right:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v6

    iput v1, v2, Landroid/graphics/Rect;->right:I

    .line 2666
    iget v1, v2, Landroid/graphics/Rect;->bottom:I

    iget-object v6, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v6

    iput v1, v2, Landroid/graphics/Rect;->bottom:I

    .line 2660
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_1

    .line 2668
    :cond_1
    iput-boolean v4, v0, Landroid/support/v7/widget/cf;->c:Z

    move-object v0, v2

    .line 2669
    goto :goto_0
.end method

.method final d()V
    .locals 1

    .prologue
    .line 1091
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-nez v0, :cond_0

    .line 1092
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    .line 1093
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Z

    .line 1095
    :cond_0
    return-void
.end method

.method public final d(I)V
    .locals 3

    .prologue
    .line 2645
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->a()I

    move-result v1

    .line 2646
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 2647
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/x;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 2646
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2649
    :cond_0
    return-void
.end method

.method final d(II)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v0, 0x0

    .line 2338
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v1}, Landroid/support/v7/widget/x;->b()I

    move-result v2

    move v1, v0

    .line 2339
    :goto_0
    if-ge v1, v2, :cond_1

    .line 2340
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v3, v1}, Landroid/support/v7/widget/x;->c(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v3

    .line 2341
    if-eqz v3, :cond_0

    invoke-virtual {v3}, Landroid/support/v7/widget/cs;->b()Z

    move-result v4

    if-nez v4, :cond_0

    iget v4, v3, Landroid/support/v7/widget/cs;->b:I

    if-lt v4, p1, :cond_0

    .line 2346
    invoke-virtual {v3, p2, v0}, Landroid/support/v7/widget/cs;->a(IZ)V

    .line 2347
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput-boolean v5, v3, Landroid/support/v7/widget/cp;->h:Z

    .line 2339
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2350
    :cond_1
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v1, v2, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    iget-object v0, v2, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cs;

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/support/v7/widget/cs;->c()I

    move-result v4

    if-lt v4, p1, :cond_2

    invoke-virtual {v0, p2, v5}, Landroid/support/v7/widget/cs;->a(IZ)V

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2351
    :cond_3
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 2352
    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 2180
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    .line 2182
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v0, v1

    .line 2183
    :goto_0
    if-ge v0, v3, :cond_0

    .line 2184
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    .line 2183
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2189
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    if-eqz v0, :cond_e

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    invoke-virtual {v0}, Landroid/support/v4/widget/x;->a()Z

    move-result v0

    if-nez v0, :cond_e

    .line 2190
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 2191
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v0

    .line 2192
    :goto_1
    const/high16 v4, 0x43870000    # 270.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 2193
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v4

    neg-int v4, v4

    add-int/2addr v0, v4

    int-to-float v0, v0

    const/4 v4, 0x0

    invoke-virtual {p1, v0, v4}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2194
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    if-eqz v0, :cond_8

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->E:Landroid/support/v4/widget/x;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/x;->a(Landroid/graphics/Canvas;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v2

    .line 2195
    :goto_2
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2197
    :goto_3
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    if-eqz v3, :cond_2

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    invoke-virtual {v3}, Landroid/support/v4/widget/x;->a()Z

    move-result v3

    if-nez v3, :cond_2

    .line 2198
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    .line 2199
    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v3, :cond_1

    .line 2200
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v5

    int-to-float v5, v5

    invoke-virtual {p1, v3, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2202
    :cond_1
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    if-eqz v3, :cond_9

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->F:Landroid/support/v4/widget/x;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/x;->a(Landroid/graphics/Canvas;)Z

    move-result v3

    if-eqz v3, :cond_9

    move v3, v2

    :goto_4
    or-int/2addr v0, v3

    .line 2203
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2205
    :cond_2
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    if-eqz v3, :cond_3

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    invoke-virtual {v3}, Landroid/support/v4/widget/x;->a()Z

    move-result v3

    if-nez v3, :cond_3

    .line 2206
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v4

    .line 2207
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v5

    .line 2208
    iget-boolean v3, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v3, :cond_a

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    .line 2209
    :goto_5
    const/high16 v6, 0x42b40000    # 90.0f

    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->rotate(F)V

    .line 2210
    neg-int v3, v3

    int-to-float v3, v3

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {p1, v3, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2211
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    if-eqz v3, :cond_b

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->G:Landroid/support/v4/widget/x;

    invoke-virtual {v3, p1}, Landroid/support/v4/widget/x;->a(Landroid/graphics/Canvas;)Z

    move-result v3

    if-eqz v3, :cond_b

    move v3, v2

    :goto_6
    or-int/2addr v0, v3

    .line 2212
    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2214
    :cond_3
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    if-eqz v3, :cond_5

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    invoke-virtual {v3}, Landroid/support/v4/widget/x;->a()Z

    move-result v3

    if-nez v3, :cond_5

    .line 2215
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v3

    .line 2216
    const/high16 v4, 0x43340000    # 180.0f

    invoke-virtual {p1, v4}, Landroid/graphics/Canvas;->rotate(F)V

    .line 2217
    iget-boolean v4, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eqz v4, :cond_c

    .line 2218
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v4

    neg-int v4, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v5

    add-int/2addr v4, v5

    int-to-float v4, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v5

    neg-int v5, v5

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v6

    add-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    .line 2222
    :goto_7
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    if-eqz v4, :cond_4

    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->H:Landroid/support/v4/widget/x;

    invoke-virtual {v4, p1}, Landroid/support/v4/widget/x;->a(Landroid/graphics/Canvas;)Z

    move-result v4

    if-eqz v4, :cond_4

    move v1, v2

    :cond_4
    or-int/2addr v0, v1

    .line 2223
    invoke-virtual {p1, v3}, Landroid/graphics/Canvas;->restoreToCount(I)V

    .line 2229
    :cond_5
    if-nez v0, :cond_d

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    if-eqz v1, :cond_d

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_d

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    invoke-virtual {v1}, Landroid/support/v7/widget/by;->b()Z

    move-result v1

    if-eqz v1, :cond_d

    .line 2234
    :goto_8
    if-eqz v2, :cond_6

    .line 2235
    invoke-static {p0}, Landroid/support/v4/view/ay;->d(Landroid/view/View;)V

    .line 2237
    :cond_6
    return-void

    :cond_7
    move v0, v1

    .line 2191
    goto/16 :goto_1

    :cond_8
    move v0, v1

    .line 2194
    goto/16 :goto_2

    :cond_9
    move v3, v1

    .line 2202
    goto/16 :goto_4

    :cond_a
    move v3, v1

    .line 2208
    goto/16 :goto_5

    :cond_b
    move v3, v1

    .line 2211
    goto :goto_6

    .line 2220
    :cond_c
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getWidth()I

    move-result v4

    neg-int v4, v4

    int-to-float v4, v4

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getHeight()I

    move-result v5

    neg-int v5, v5

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_7

    :cond_d
    move v2, v0

    goto :goto_8

    :cond_e
    move v0, v1

    goto/16 :goto_3
.end method

.method public final e()Landroid/support/v7/widget/by;
    .locals 1

    .prologue
    .line 1738
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    return-object v0
.end method

.method final e(II)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x0

    .line 2391
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->b()I

    move-result v3

    .line 2392
    add-int v4, p1, p2

    move v2, v1

    .line 2394
    :goto_0
    if-ge v2, v3, :cond_2

    .line 2395
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/x;->c(I)Landroid/view/View;

    move-result-object v0

    .line 2396
    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v5

    .line 2397
    if-eqz v5, :cond_1

    invoke-virtual {v5}, Landroid/support/v7/widget/cs;->b()Z

    move-result v6

    if-nez v6, :cond_1

    .line 2398
    iget v6, v5, Landroid/support/v7/widget/cs;->b:I

    if-lt v6, p1, :cond_1

    iget v6, v5, Landroid/support/v7/widget/cs;->b:I

    if-ge v6, v4, :cond_1

    .line 2403
    invoke-virtual {v5, v7}, Landroid/support/v7/widget/cs;->a(I)V

    .line 2404
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->t()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 2405
    const/16 v6, 0x40

    invoke-virtual {v5, v6}, Landroid/support/v7/widget/cs;->a(I)V

    .line 2408
    :cond_0
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cf;

    const/4 v5, 0x1

    iput-boolean v5, v0, Landroid/support/v7/widget/cf;->c:Z

    .line 2394
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 2411
    :cond_2
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v0, v2, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    :goto_1
    if-ge v1, v3, :cond_4

    iget-object v0, v2, Landroid/support/v7/widget/cj;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cs;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Landroid/support/v7/widget/cs;->c()I

    move-result v5

    if-lt v5, p1, :cond_3

    if-ge v5, v4, :cond_3

    invoke-virtual {v0, v7}, Landroid/support/v7/widget/cs;->a(I)V

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2412
    :cond_4
    return-void
.end method

.method final f()V
    .locals 14

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 1816
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    if-nez v0, :cond_0

    .line 1817
    const-string v0, "RecyclerView"

    const-string v1, "No adapter attached; skipping layout"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2029
    :goto_0
    return-void

    .line 1820
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1821
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    .line 1822
    iput-boolean v9, p0, Landroid/support/v7/widget/RecyclerView;->D:Z

    .line 1824
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->v()V

    .line 1826
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-boolean v0, v0, Landroid/support/v7/widget/cp;->j:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->h:Z

    if-eqz v0, :cond_3

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->t()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Landroid/support/v4/g/a;

    invoke-direct {v0}, Landroid/support/v4/g/a;-><init>()V

    :goto_1
    iput-object v0, v1, Landroid/support/v7/widget/cp;->d:Landroid/support/v4/g/a;

    .line 1828
    iput-boolean v8, p0, Landroid/support/v7/widget/RecyclerView;->h:Z

    iput-boolean v8, p0, Landroid/support/v7/widget/RecyclerView;->g:Z

    .line 1830
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-boolean v1, v1, Landroid/support/v7/widget/cp;->k:Z

    iput-boolean v1, v0, Landroid/support/v7/widget/cp;->i:Z

    .line 1831
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    invoke-virtual {v1}, Landroid/support/v7/widget/bv;->a()I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/cp;->e:I

    .line 1833
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-boolean v0, v0, Landroid/support/v7/widget/cp;->j:Z

    if-eqz v0, :cond_4

    .line 1835
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->b:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->clear()V

    .line 1836
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->c:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->clear()V

    .line 1837
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->a()I

    move-result v10

    move v6, v8

    .line 1838
    :goto_2
    if-ge v6, v10, :cond_4

    .line 1839
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/x;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v1

    .line 1840
    invoke-virtual {v1}, Landroid/support/v7/widget/cs;->b()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v1}, Landroid/support/v7/widget/cs;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    iget-boolean v0, v0, Landroid/support/v7/widget/bv;->b:Z

    if-eqz v0, :cond_2

    .line 1841
    :cond_1
    iget-object v5, v1, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    .line 1844
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v11, v0, Landroid/support/v7/widget/cp;->b:Landroid/support/v4/g/a;

    new-instance v0, Landroid/support/v7/widget/cd;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/cd;-><init>(Landroid/support/v7/widget/cs;IIII)V

    invoke-virtual {v11, v1, v0}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1838
    :cond_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_2

    :cond_3
    move-object v0, v7

    .line 1826
    goto :goto_1

    .line 1848
    :cond_4
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-boolean v0, v0, Landroid/support/v7/widget/cp;->k:Z

    if-eqz v0, :cond_d

    .line 1855
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->b()I

    move-result v1

    move v0, v8

    :goto_3
    if-ge v0, v1, :cond_6

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/x;->c(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v7/widget/cs;->b()Z

    move-result v3

    if-nez v3, :cond_5

    iget v3, v2, Landroid/support/v7/widget/cs;->c:I

    const/4 v4, -0x1

    if-ne v3, v4, :cond_5

    iget v3, v2, Landroid/support/v7/widget/cs;->b:I

    iput v3, v2, Landroid/support/v7/widget/cs;->c:I

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 1857
    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->d:Landroid/support/v4/g/a;

    if-eqz v0, :cond_8

    .line 1858
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->a()I

    move-result v1

    move v0, v8

    .line 1859
    :goto_4
    if-ge v0, v1, :cond_8

    .line 1860
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/x;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v2

    .line 1861
    invoke-virtual {v2}, Landroid/support/v7/widget/cs;->j()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-virtual {v2}, Landroid/support/v7/widget/cs;->l()Z

    move-result v3

    if-nez v3, :cond_7

    invoke-virtual {v2}, Landroid/support/v7/widget/cs;->b()Z

    move-result v3

    if-nez v3, :cond_7

    .line 1862
    invoke-direct {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/cs;)J

    move-result-wide v4

    .line 1863
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v3, v3, Landroid/support/v7/widget/cp;->d:Landroid/support/v4/g/a;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1864
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v3, v3, Landroid/support/v7/widget/cp;->b:Landroid/support/v4/g/a;

    invoke-virtual {v3, v2}, Landroid/support/v4/g/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1859
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 1869
    :cond_8
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-boolean v0, v0, Landroid/support/v7/widget/cp;->h:Z

    .line 1870
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput-boolean v8, v1, Landroid/support/v7/widget/cp;->h:Z

    .line 1872
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    invoke-virtual {v1, v2, v3}, Landroid/support/v7/widget/ce;->c(Landroid/support/v7/widget/cj;Landroid/support/v7/widget/cp;)V

    .line 1873
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput-boolean v0, v1, Landroid/support/v7/widget/cp;->h:Z

    .line 1875
    new-instance v3, Landroid/support/v4/g/a;

    invoke-direct {v3}, Landroid/support/v4/g/a;-><init>()V

    move v1, v8

    .line 1876
    :goto_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->a()I

    move-result v0

    if-ge v1, v0, :cond_b

    .line 1878
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/x;->b(I)Landroid/view/View;

    move-result-object v4

    .line 1879
    invoke-static {v4}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/cs;->b()Z

    move-result v0

    if-nez v0, :cond_9

    move v2, v8

    .line 1880
    :goto_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->b:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->size()I

    move-result v0

    if-ge v2, v0, :cond_26

    .line 1883
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->b:Landroid/support/v4/g/a;

    invoke-virtual {v0, v2}, Landroid/support/v4/g/a;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cs;

    .line 1884
    iget-object v0, v0, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    if-ne v0, v4, :cond_a

    move v0, v9

    .line 1889
    :goto_7
    if-nez v0, :cond_9

    .line 1890
    new-instance v0, Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v4}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-virtual {v4}, Landroid/view/View;->getRight()I

    move-result v6

    invoke-virtual {v4}, Landroid/view/View;->getBottom()I

    move-result v10

    invoke-direct {v0, v2, v5, v6, v10}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v3, v4, v0}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1876
    :cond_9
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_5

    .line 1882
    :cond_a
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_6

    .line 1895
    :cond_b
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->x()V

    .line 1896
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    invoke-virtual {v0}, Landroid/support/v7/widget/n;->c()V

    move-object v6, v3

    .line 1913
    :goto_8
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    invoke-virtual {v1}, Landroid/support/v7/widget/bv;->a()I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/cp;->e:I

    .line 1914
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput v8, v0, Landroid/support/v7/widget/cp;->g:I

    .line 1917
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput-boolean v8, v0, Landroid/support/v7/widget/cp;->i:Z

    .line 1918
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/widget/ce;->c(Landroid/support/v7/widget/cj;Landroid/support/v7/widget/cp;)V

    .line 1920
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput-boolean v8, v0, Landroid/support/v7/widget/cp;->h:Z

    .line 1921
    iput-object v7, p0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/support/v7/widget/RecyclerView$SavedState;

    .line 1924
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-boolean v0, v0, Landroid/support/v7/widget/cp;->j:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    if-eqz v0, :cond_f

    move v0, v9

    :goto_9
    iput-boolean v0, v1, Landroid/support/v7/widget/cp;->j:Z

    .line 1926
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-boolean v0, v0, Landroid/support/v7/widget/cp;->j:Z

    if-eqz v0, :cond_22

    .line 1928
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->d:Landroid/support/v4/g/a;

    if-eqz v0, :cond_10

    new-instance v0, Landroid/support/v4/g/a;

    invoke-direct {v0}, Landroid/support/v4/g/a;-><init>()V

    move-object v10, v0

    .line 1930
    :goto_a
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->a()I

    move-result v12

    move v11, v8

    .line 1931
    :goto_b
    if-ge v11, v12, :cond_12

    .line 1932
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0, v11}, Landroid/support/v7/widget/x;->b(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v1

    .line 1933
    invoke-virtual {v1}, Landroid/support/v7/widget/cs;->b()Z

    move-result v0

    if-nez v0, :cond_c

    .line 1934
    iget-object v5, v1, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    .line 1937
    invoke-direct {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/cs;)J

    move-result-wide v2

    .line 1938
    if-eqz v10, :cond_11

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->d:Landroid/support/v4/g/a;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_11

    .line 1939
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v10, v0, v1}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1931
    :cond_c
    :goto_c
    add-int/lit8 v0, v11, 0x1

    move v11, v0

    goto :goto_b

    .line 1898
    :cond_d
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->x()V

    .line 1900
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    invoke-virtual {v0}, Landroid/support/v7/widget/n;->e()V

    .line 1901
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->d:Landroid/support/v4/g/a;

    if-eqz v0, :cond_25

    .line 1902
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->a()I

    move-result v1

    move v0, v8

    .line 1903
    :goto_d
    if-ge v0, v1, :cond_25

    .line 1904
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/x;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v2

    .line 1905
    invoke-virtual {v2}, Landroid/support/v7/widget/cs;->j()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-virtual {v2}, Landroid/support/v7/widget/cs;->l()Z

    move-result v3

    if-nez v3, :cond_e

    invoke-virtual {v2}, Landroid/support/v7/widget/cs;->b()Z

    move-result v3

    if-nez v3, :cond_e

    .line 1906
    invoke-direct {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/cs;)J

    move-result-wide v4

    .line 1907
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v3, v3, Landroid/support/v7/widget/cp;->d:Landroid/support/v4/g/a;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v4, v2}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1908
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v3, v3, Landroid/support/v7/widget/cp;->b:Landroid/support/v4/g/a;

    invoke-virtual {v3, v2}, Landroid/support/v4/g/a;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1903
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_d

    :cond_f
    move v0, v8

    .line 1924
    goto/16 :goto_9

    :cond_10
    move-object v10, v7

    .line 1928
    goto/16 :goto_a

    .line 1941
    :cond_11
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v13, v0, Landroid/support/v7/widget/cp;->c:Landroid/support/v4/g/a;

    new-instance v0, Landroid/support/v7/widget/cd;

    invoke-virtual {v5}, Landroid/view/View;->getLeft()I

    move-result v2

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v3

    invoke-virtual {v5}, Landroid/view/View;->getRight()I

    move-result v4

    invoke-virtual {v5}, Landroid/view/View;->getBottom()I

    move-result v5

    invoke-direct/range {v0 .. v5}, Landroid/support/v7/widget/cd;-><init>(Landroid/support/v7/widget/cs;IIII)V

    invoke-virtual {v13, v1, v0}, Landroid/support/v4/g/a;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_c

    .line 1945
    :cond_12
    invoke-direct {p0, v6}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v4/g/a;)V

    .line 1947
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->b:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->size()I

    move-result v0

    .line 1948
    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_e
    if-ltz v1, :cond_14

    .line 1949
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->b:Landroid/support/v4/g/a;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/a;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cs;

    .line 1950
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v2, v2, Landroid/support/v7/widget/cp;->c:Landroid/support/v4/g/a;

    invoke-virtual {v2, v0}, Landroid/support/v4/g/a;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 1951
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->b:Landroid/support/v4/g/a;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/a;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cd;

    .line 1952
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v2, v2, Landroid/support/v7/widget/cp;->b:Landroid/support/v4/g/a;

    invoke-virtual {v2, v1}, Landroid/support/v4/g/a;->d(I)Ljava/lang/Object;

    .line 1954
    iget-object v2, v0, Landroid/support/v7/widget/cd;->a:Landroid/support/v7/widget/cs;

    iget-object v2, v2, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    .line 1955
    invoke-virtual {p0, v2, v8}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    .line 1956
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v3, v0, Landroid/support/v7/widget/cd;->a:Landroid/support/v7/widget/cs;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/cj;->b(Landroid/support/v7/widget/cs;)V

    .line 1958
    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/support/v7/widget/cd;)V

    .line 1948
    :cond_13
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_e

    .line 1962
    :cond_14
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->c:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->size()I

    move-result v0

    .line 1963
    if-lez v0, :cond_1a

    .line 1964
    add-int/lit8 v0, v0, -0x1

    move v11, v0

    :goto_f
    if-ltz v11, :cond_1a

    .line 1965
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->c:Landroid/support/v4/g/a;

    invoke-virtual {v0, v11}, Landroid/support/v4/g/a;->b(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/cs;

    .line 1966
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->c:Landroid/support/v4/g/a;

    invoke-virtual {v0, v11}, Landroid/support/v4/g/a;->c(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cd;

    .line 1967
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v2, v2, Landroid/support/v7/widget/cp;->b:Landroid/support/v4/g/a;

    invoke-virtual {v2}, Landroid/support/v4/g/a;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_15

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v2, v2, Landroid/support/v7/widget/cp;->b:Landroid/support/v4/g/a;

    invoke-virtual {v2, v1}, Landroid/support/v4/g/a;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    .line 1969
    :cond_15
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v2, v2, Landroid/support/v7/widget/cp;->c:Landroid/support/v4/g/a;

    invoke-virtual {v2, v11}, Landroid/support/v4/g/a;->d(I)Ljava/lang/Object;

    .line 1970
    if-eqz v6, :cond_18

    iget-object v2, v1, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    invoke-virtual {v6, v2}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Rect;

    move-object v3, v2

    .line 1972
    :goto_10
    iget v4, v0, Landroid/support/v7/widget/cd;->b:I

    iget v5, v0, Landroid/support/v7/widget/cd;->c:I

    iget-object v0, v1, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    if-eqz v3, :cond_19

    iget v0, v3, Landroid/graphics/Rect;->left:I

    if-ne v0, v4, :cond_16

    iget v0, v3, Landroid/graphics/Rect;->top:I

    if-eq v0, v5, :cond_19

    :cond_16
    invoke-virtual {v1, v8}, Landroid/support/v7/widget/cs;->a(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    iget v2, v3, Landroid/graphics/Rect;->left:I

    iget v3, v3, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/by;->a(Landroid/support/v7/widget/cs;IIII)Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->u()V

    .line 1964
    :cond_17
    :goto_11
    add-int/lit8 v0, v11, -0x1

    move v11, v0

    goto :goto_f

    :cond_18
    move-object v3, v7

    .line 1970
    goto :goto_10

    .line 1972
    :cond_19
    invoke-virtual {v1, v8}, Landroid/support/v7/widget/cs;->a(Z)V

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/by;->b(Landroid/support/v7/widget/cs;)Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->u()V

    goto :goto_11

    .line 1978
    :cond_1a
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->c:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->size()I

    move-result v11

    move v6, v8

    .line 1979
    :goto_12
    if-ge v6, v11, :cond_1d

    .line 1980
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->c:Landroid/support/v4/g/a;

    invoke-virtual {v0, v6}, Landroid/support/v4/g/a;->b(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/cs;

    .line 1981
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->c:Landroid/support/v4/g/a;

    invoke-virtual {v0, v6}, Landroid/support/v4/g/a;->c(I)Ljava/lang/Object;

    move-result-object v0

    move-object v5, v0

    check-cast v5, Landroid/support/v7/widget/cd;

    .line 1982
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->b:Landroid/support/v4/g/a;

    invoke-virtual {v0, v1}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v3, v0

    check-cast v3, Landroid/support/v7/widget/cd;

    .line 1983
    if-eqz v3, :cond_1c

    if-eqz v5, :cond_1c

    .line 1984
    iget v0, v3, Landroid/support/v7/widget/cd;->b:I

    iget v2, v5, Landroid/support/v7/widget/cd;->b:I

    if-ne v0, v2, :cond_1b

    iget v0, v3, Landroid/support/v7/widget/cd;->c:I

    iget v2, v5, Landroid/support/v7/widget/cd;->c:I

    if-eq v0, v2, :cond_1c

    .line 1985
    :cond_1b
    invoke-virtual {v1, v8}, Landroid/support/v7/widget/cs;->a(Z)V

    .line 1990
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    iget v2, v3, Landroid/support/v7/widget/cd;->b:I

    iget v3, v3, Landroid/support/v7/widget/cd;->c:I

    iget v4, v5, Landroid/support/v7/widget/cd;->b:I

    iget v5, v5, Landroid/support/v7/widget/cd;->c:I

    invoke-virtual/range {v0 .. v5}, Landroid/support/v7/widget/by;->a(Landroid/support/v7/widget/cs;IIII)Z

    move-result v0

    if-eqz v0, :cond_1c

    .line 1992
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->u()V

    .line 1979
    :cond_1c
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_12

    .line 1998
    :cond_1d
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->d:Landroid/support/v4/g/a;

    if-eqz v0, :cond_20

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->d:Landroid/support/v4/g/a;

    invoke-virtual {v0}, Landroid/support/v4/g/a;->size()I

    move-result v0

    .line 2000
    :goto_13
    add-int/lit8 v0, v0, -0x1

    move v11, v0

    :goto_14
    if-ltz v11, :cond_22

    .line 2001
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->d:Landroid/support/v4/g/a;

    invoke-virtual {v0, v11}, Landroid/support/v4/g/a;->b(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    .line 2002
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, v0, Landroid/support/v7/widget/cp;->d:Landroid/support/v4/g/a;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/cs;

    .line 2003
    iget-object v0, v1, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    .line 2004
    invoke-virtual {v1}, Landroid/support/v7/widget/cs;->b()Z

    move-result v0

    if-nez v0, :cond_1f

    .line 2005
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v0, v0, Landroid/support/v7/widget/cj;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_1f

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v0, v0, Landroid/support/v7/widget/cj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1f

    .line 2011
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/support/v4/g/a;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/cs;

    invoke-virtual {v1, v8}, Landroid/support/v7/widget/cs;->a(Z)V

    iget-object v0, v1, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    invoke-virtual {p0, v0, v8}, Landroid/support/v7/widget/RecyclerView;->removeDetachedView(Landroid/view/View;Z)V

    iget-object v0, v1, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    invoke-direct {p0, v0}, Landroid/support/v7/widget/RecyclerView;->e(Landroid/view/View;)V

    iput-object v2, v1, Landroid/support/v7/widget/cs;->g:Landroid/support/v7/widget/cs;

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/cj;->b(Landroid/support/v7/widget/cs;)V

    iget-object v0, v1, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v3

    iget-object v0, v1, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v4

    if-eqz v2, :cond_1e

    invoke-virtual {v2}, Landroid/support/v7/widget/cs;->b()Z

    move-result v0

    if-eqz v0, :cond_21

    :cond_1e
    move v6, v4

    move v5, v3

    :goto_15
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    invoke-virtual/range {v0 .. v6}, Landroid/support/v7/widget/by;->a(Landroid/support/v7/widget/cs;Landroid/support/v7/widget/cs;IIII)Z

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->u()V

    .line 2000
    :cond_1f
    add-int/lit8 v0, v11, -0x1

    move v11, v0

    goto :goto_14

    :cond_20
    move v0, v8

    .line 1998
    goto :goto_13

    .line 2011
    :cond_21
    iget-object v0, v2, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLeft()I

    move-result v5

    iget-object v0, v2, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v6

    invoke-virtual {v2, v8}, Landroid/support/v7/widget/cs;->a(Z)V

    iput-object v1, v2, Landroid/support/v7/widget/cs;->h:Landroid/support/v7/widget/cs;

    goto :goto_15

    .line 2017
    :cond_22
    invoke-virtual {p0, v8}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 2018
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-boolean v2, v2, Landroid/support/v7/widget/cp;->k:Z

    if-nez v2, :cond_24

    :goto_16
    invoke-virtual {v0, v1, v9}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/cj;Z)V

    .line 2019
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget v1, v1, Landroid/support/v7/widget/cp;->e:I

    iput v1, v0, Landroid/support/v7/widget/cp;->f:I

    .line 2020
    iput-boolean v8, p0, Landroid/support/v7/widget/RecyclerView;->C:Z

    .line 2021
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput-boolean v8, v0, Landroid/support/v7/widget/cp;->j:Z

    .line 2022
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput-boolean v8, v0, Landroid/support/v7/widget/cp;->k:Z

    .line 2023
    iput-boolean v8, p0, Landroid/support/v7/widget/RecyclerView;->D:Z

    .line 2024
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-static {v0}, Landroid/support/v7/widget/ce;->b(Landroid/support/v7/widget/ce;)Z

    .line 2025
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v0, v0, Landroid/support/v7/widget/cj;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_23

    .line 2026
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v0, v0, Landroid/support/v7/widget/cj;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2028
    :cond_23
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput-object v7, v0, Landroid/support/v7/widget/cp;->d:Landroid/support/v4/g/a;

    goto/16 :goto_0

    :cond_24
    move v9, v8

    .line 2018
    goto :goto_16

    :cond_25
    move-object v6, v7

    goto/16 :goto_8

    :cond_26
    move v0, v8

    goto/16 :goto_7
.end method

.method public focusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 3

    .prologue
    .line 1300
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    .line 1301
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v0

    .line 1305
    invoke-virtual {v0, p0, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    .line 1306
    if-nez v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    if-eqz v1, :cond_0

    .line 1307
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    .line 1308
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    invoke-virtual {v0, p2, v1, v2}, Landroid/support/v7/widget/ce;->c(ILandroid/support/v7/widget/cj;Landroid/support/v7/widget/cp;)Landroid/view/View;

    move-result-object v0

    .line 1309
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 1311
    :cond_0
    if-eqz v0, :cond_1

    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->focusSearch(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method final g()V
    .locals 5

    .prologue
    .line 2415
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v0}, Landroid/support/v7/widget/x;->a()I

    move-result v1

    .line 2416
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_6

    .line 2417
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->c:Landroid/support/v7/widget/x;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/x;->b(I)Landroid/view/View;

    move-result-object v2

    invoke-static {v2}, Landroid/support/v7/widget/RecyclerView;->b(Landroid/view/View;)Landroid/support/v7/widget/cs;

    move-result-object v2

    .line 2419
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/support/v7/widget/cs;->b()Z

    move-result v3

    if-nez v3, :cond_1

    .line 2420
    invoke-virtual {v2}, Landroid/support/v7/widget/cs;->l()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Landroid/support/v7/widget/cs;->h()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 2423
    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 2416
    :cond_1
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2424
    :cond_2
    invoke-virtual {v2}, Landroid/support/v7/widget/cs;->i()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 2425
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    iget v4, v2, Landroid/support/v7/widget/cs;->b:I

    invoke-virtual {v3, v4}, Landroid/support/v7/widget/bv;->a(I)I

    move-result v3

    .line 2426
    iget v4, v2, Landroid/support/v7/widget/cs;->e:I

    if-ne v4, v3, :cond_5

    .line 2428
    invoke-virtual {v2}, Landroid/support/v7/widget/cs;->j()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->t()Z

    move-result v3

    if-nez v3, :cond_4

    .line 2429
    :cond_3
    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    iget v4, v2, Landroid/support/v7/widget/cs;->b:I

    invoke-virtual {v3, v2, v4}, Landroid/support/v7/widget/bv;->b(Landroid/support/v7/widget/cs;I)V

    goto :goto_1

    .line 2434
    :cond_4
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto :goto_1

    .line 2439
    :cond_5
    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/cs;->a(I)V

    .line 2440
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto :goto_1

    .line 2444
    :cond_6
    return-void
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 2256
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    if-nez v0, :cond_0

    .line 2257
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecyclerView has no LayoutManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2259
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->a()Landroid/support/v7/widget/cf;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 2264
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    if-nez v0, :cond_0

    .line 2265
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecyclerView has no LayoutManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2267
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v7/widget/ce;->a(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/v7/widget/cf;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    .prologue
    .line 2272
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    if-nez v0, :cond_0

    .line 2273
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "RecyclerView has no LayoutManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2275
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/ce;->a(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/cf;

    move-result-object v0

    return-object v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1339
    invoke-super {p0}, Landroid/view/ViewGroup;->onAttachedToWindow()V

    .line 1340
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    .line 1341
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    .line 1342
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_0

    .line 1343
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    .line 1345
    :cond_0
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->V:Z

    .line 1346
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1350
    invoke-super {p0}, Landroid/view/ViewGroup;->onDetachedFromWindow()V

    .line 1351
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    if-eqz v0, :cond_0

    .line 1352
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->e:Landroid/support/v7/widget/by;

    invoke-virtual {v0}, Landroid/support/v7/widget/by;->d()V

    .line 1354
    :cond_0
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    .line 1356
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->k()V

    .line 1357
    iput-boolean v1, p0, Landroid/support/v7/widget/RecyclerView;->u:Z

    .line 1358
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_1

    .line 1359
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    invoke-virtual {v0, p0, v1}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/RecyclerView;Landroid/support/v7/widget/cj;)V

    .line 1361
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->aa:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1362
    return-void
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 4

    .prologue
    .line 2241
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->onDraw(Landroid/graphics/Canvas;)V

    .line 2243
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2244
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 2245
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->r:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cc;

    iget-object v3, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    invoke-virtual {v0, p1, p0}, Landroid/support/v7/widget/cc;->a(Landroid/graphics/Canvas;Landroid/support/v7/widget/RecyclerView;)V

    .line 2244
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 2247
    :cond_0
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v1, -0x1

    const/4 v3, 0x0

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v2, 0x1

    .line 1477
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    if-eq v5, v9, :cond_0

    if-nez v5, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v7/widget/cg;

    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v4, v3

    :goto_0
    if-ge v4, v6, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cg;

    invoke-interface {v0}, Landroid/support/v7/widget/cg;->a()Z

    move-result v7

    if-eqz v7, :cond_3

    if-eq v5, v9, :cond_3

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v7/widget/cg;

    move v0, v2

    :goto_1
    if-eqz v0, :cond_5

    .line 1478
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->s()V

    .line 1552
    :cond_2
    :goto_2
    return v2

    .line 1477
    :cond_3
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_4
    move v0, v3

    goto :goto_1

    .line 1482
    :cond_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->e()Z

    move-result v0

    .line 1483
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v4}, Landroid/support/v7/widget/ce;->f()Z

    move-result v4

    .line 1485
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/view/VelocityTracker;

    if-nez v5, :cond_6

    .line 1486
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v5

    iput-object v5, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/view/VelocityTracker;

    .line 1488
    :cond_6
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/view/VelocityTracker;

    invoke-virtual {v5, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1490
    invoke-static {p1}, Landroid/support/v4/view/ao;->a(Landroid/view/MotionEvent;)I

    move-result v5

    .line 1491
    invoke-static {p1}, Landroid/support/v4/view/ao;->b(Landroid/view/MotionEvent;)I

    move-result v6

    .line 1493
    packed-switch v5, :pswitch_data_0

    .line 1552
    :cond_7
    :goto_3
    :pswitch_0
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->I:I

    if-eq v0, v2, :cond_2

    move v2, v3

    goto :goto_2

    .line 1495
    :pswitch_1
    invoke-static {p1, v3}, Landroid/support/v4/view/ao;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->J:I

    .line 1496
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    add-float/2addr v0, v8

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->L:I

    .line 1497
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    add-float/2addr v0, v8

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->O:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    .line 1499
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->I:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_7

    .line 1500
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1501
    invoke-direct {p0, v2}, Landroid/support/v7/widget/RecyclerView;->e(I)V

    goto :goto_3

    .line 1506
    :pswitch_2
    invoke-static {p1, v6}, Landroid/support/v4/view/ao;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->J:I

    .line 1507
    invoke-static {p1, v6}, Landroid/support/v4/view/ao;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v8

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->L:I

    .line 1508
    invoke-static {p1, v6}, Landroid/support/v4/view/ao;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v8

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->O:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    goto :goto_3

    .line 1512
    :pswitch_3
    iget v5, p0, Landroid/support/v7/widget/RecyclerView;->J:I

    invoke-static {p1, v5}, Landroid/support/v4/view/ao;->a(Landroid/view/MotionEvent;I)I

    move-result v5

    .line 1513
    if-gez v5, :cond_8

    .line 1514
    const-string v0, "RecyclerView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error processing scroll; pointer index for id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->J:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found. Did any MotionEvents get skipped?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 1516
    goto/16 :goto_2

    .line 1519
    :cond_8
    invoke-static {p1, v5}, Landroid/support/v4/view/ao;->c(Landroid/view/MotionEvent;I)F

    move-result v6

    add-float/2addr v6, v8

    float-to-int v6, v6

    .line 1520
    invoke-static {p1, v5}, Landroid/support/v4/view/ao;->d(Landroid/view/MotionEvent;I)F

    move-result v5

    add-float/2addr v5, v8

    float-to-int v5, v5

    .line 1521
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->I:I

    if-eq v7, v2, :cond_7

    .line 1522
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->L:I

    sub-int/2addr v6, v7

    .line 1523
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    sub-int/2addr v5, v7

    .line 1525
    if-eqz v0, :cond_c

    invoke-static {v6}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    if-le v0, v7, :cond_c

    .line 1526
    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->L:I

    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    if-gez v6, :cond_a

    move v0, v1

    :goto_4
    mul-int/2addr v0, v8

    add-int/2addr v0, v7

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    move v0, v2

    .line 1529
    :goto_5
    if-eqz v4, :cond_9

    invoke-static {v5}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v6, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    if-le v4, v6, :cond_9

    .line 1530
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    if-gez v5, :cond_b

    :goto_6
    mul-int/2addr v1, v4

    add-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->O:I

    move v0, v2

    .line 1533
    :cond_9
    if-eqz v0, :cond_7

    .line 1534
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1535
    invoke-direct {p0, v2}, Landroid/support/v7/widget/RecyclerView;->e(I)V

    goto/16 :goto_3

    :cond_a
    move v0, v2

    .line 1526
    goto :goto_4

    :cond_b
    move v1, v2

    .line 1530
    goto :goto_6

    .line 1541
    :pswitch_4
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_3

    .line 1545
    :pswitch_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    goto/16 :goto_3

    .line 1549
    :pswitch_6
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->s()V

    goto/16 :goto_3

    :cond_c
    move v0, v3

    goto :goto_5

    .line 1493
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    .prologue
    .line 2154
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    .line 2155
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->f()V

    .line 2156
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 2157
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    .line 2158
    return-void
.end method

.method protected onMeasure(II)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1669
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    if-eqz v0, :cond_0

    .line 1670
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->d()V

    .line 1671
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->v()V

    .line 1673
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-boolean v0, v0, Landroid/support/v7/widget/cp;->k:Z

    if-eqz v0, :cond_1

    .line 1678
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    const/4 v1, 0x1

    iput-boolean v1, v0, Landroid/support/v7/widget/cp;->i:Z

    .line 1684
    :goto_0
    iput-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->z:Z

    .line 1685
    invoke-virtual {p0, v2}, Landroid/support/v7/widget/RecyclerView;->a(Z)V

    .line 1688
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    if-eqz v0, :cond_2

    .line 1689
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->o:Landroid/support/v7/widget/bv;

    invoke-virtual {v1}, Landroid/support/v7/widget/bv;->a()I

    move-result v1

    iput v1, v0, Landroid/support/v7/widget/cp;->e:I

    .line 1694
    :goto_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->a:Landroid/support/v7/widget/cj;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/ce;->a(II)V

    .line 1695
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput-boolean v2, v0, Landroid/support/v7/widget/cp;->i:Z

    .line 1696
    return-void

    .line 1681
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->b:Landroid/support/v7/widget/n;

    invoke-virtual {v0}, Landroid/support/v7/widget/n;->e()V

    .line 1682
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput-boolean v2, v0, Landroid/support/v7/widget/cp;->i:Z

    goto :goto_0

    .line 1691
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput v2, v0, Landroid/support/v7/widget/cp;->e:I

    goto :goto_1
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    .line 626
    check-cast p1, Landroid/support/v7/widget/RecyclerView$SavedState;

    iput-object p1, p0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/support/v7/widget/RecyclerView$SavedState;

    .line 627
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/support/v7/widget/RecyclerView$SavedState;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$SavedState;->getSuperState()Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/view/ViewGroup;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 628
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/support/v7/widget/RecyclerView$SavedState;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView$SavedState;->a:Landroid/os/Parcelable;

    if-eqz v0, :cond_0

    .line 629
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/support/v7/widget/RecyclerView$SavedState;

    iget-object v1, v1, Landroid/support/v7/widget/RecyclerView$SavedState;->a:Landroid/os/Parcelable;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/ce;->a(Landroid/os/Parcelable;)V

    .line 631
    :cond_0
    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 2

    .prologue
    .line 612
    new-instance v0, Landroid/support/v7/widget/RecyclerView$SavedState;

    invoke-super {p0}, Landroid/view/ViewGroup;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v7/widget/RecyclerView$SavedState;-><init>(Landroid/os/Parcelable;)V

    .line 613
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/support/v7/widget/RecyclerView$SavedState;

    if-eqz v1, :cond_0

    .line 614
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->k:Landroid/support/v7/widget/RecyclerView$SavedState;

    invoke-static {v0, v1}, Landroid/support/v7/widget/RecyclerView$SavedState;->a(Landroid/support/v7/widget/RecyclerView$SavedState;Landroid/support/v7/widget/RecyclerView$SavedState;)V

    .line 621
    :goto_0
    return-object v0

    .line 615
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    if-eqz v1, :cond_1

    .line 616
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v1}, Landroid/support/v7/widget/ce;->d()Landroid/os/Parcelable;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v7/widget/RecyclerView$SavedState;->a:Landroid/os/Parcelable;

    goto :goto_0

    .line 618
    :cond_1
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/widget/RecyclerView$SavedState;->a:Landroid/os/Parcelable;

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 0

    .prologue
    .line 1700
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/ViewGroup;->onSizeChanged(IIII)V

    .line 1701
    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_1

    .line 1702
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->r()V

    .line 1704
    :cond_1
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    .prologue
    const/4 v1, -0x1

    const/4 v4, 0x0

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1557
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v7/widget/cg;

    if-eqz v5, :cond_0

    if-nez v0, :cond_1

    const/4 v5, 0x0

    iput-object v5, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v7/widget/cg;

    :cond_0
    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v5, v3

    :goto_0
    if-ge v5, v6, :cond_5

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->s:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/cg;

    invoke-interface {v0}, Landroid/support/v7/widget/cg;->a()Z

    move-result v7

    if-eqz v7, :cond_4

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v7/widget/cg;

    move v0, v2

    :goto_1
    if-eqz v0, :cond_6

    .line 1558
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->s()V

    .line 1645
    :goto_2
    return v2

    .line 1557
    :cond_1
    iget-object v5, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v7/widget/cg;

    const/4 v5, 0x3

    if-eq v0, v5, :cond_2

    if-ne v0, v2, :cond_3

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->t:Landroid/support/v7/widget/cg;

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    :cond_5
    move v0, v3

    goto :goto_1

    .line 1562
    :cond_6
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->e()Z

    move-result v5

    .line 1563
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0}, Landroid/support/v7/widget/ce;->f()Z

    move-result v6

    .line 1565
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/view/VelocityTracker;

    if-nez v0, :cond_7

    .line 1566
    invoke-static {}, Landroid/view/VelocityTracker;->obtain()Landroid/view/VelocityTracker;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/view/VelocityTracker;

    .line 1568
    :cond_7
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/view/VelocityTracker;

    invoke-virtual {v0, p1}, Landroid/view/VelocityTracker;->addMovement(Landroid/view/MotionEvent;)V

    .line 1570
    invoke-static {p1}, Landroid/support/v4/view/ao;->a(Landroid/view/MotionEvent;)I

    move-result v0

    .line 1571
    invoke-static {p1}, Landroid/support/v4/view/ao;->b(Landroid/view/MotionEvent;)I

    move-result v7

    .line 1573
    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_2

    .line 1575
    :pswitch_1
    invoke-static {p1, v3}, Landroid/support/v4/view/ao;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->J:I

    .line 1576
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    add-float/2addr v0, v8

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->L:I

    .line 1577
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    add-float/2addr v0, v8

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->O:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    goto :goto_2

    .line 1581
    :pswitch_2
    invoke-static {p1, v7}, Landroid/support/v4/view/ao;->b(Landroid/view/MotionEvent;I)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->J:I

    .line 1582
    invoke-static {p1, v7}, Landroid/support/v4/view/ao;->c(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v8

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->L:I

    .line 1583
    invoke-static {p1, v7}, Landroid/support/v4/view/ao;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v8

    float-to-int v0, v0

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->O:I

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    goto :goto_2

    .line 1587
    :pswitch_3
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->J:I

    invoke-static {p1, v0}, Landroid/support/v4/view/ao;->a(Landroid/view/MotionEvent;I)I

    move-result v0

    .line 1588
    if-gez v0, :cond_8

    .line 1589
    const-string v0, "RecyclerView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error processing scroll; pointer index for id "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Landroid/support/v7/widget/RecyclerView;->J:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not found. Did any MotionEvents get skipped?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v2, v3

    .line 1591
    goto/16 :goto_2

    .line 1594
    :cond_8
    invoke-static {p1, v0}, Landroid/support/v4/view/ao;->c(Landroid/view/MotionEvent;I)F

    move-result v4

    add-float/2addr v4, v8

    float-to-int v4, v4

    .line 1595
    invoke-static {p1, v0}, Landroid/support/v4/view/ao;->d(Landroid/view/MotionEvent;I)F

    move-result v0

    add-float/2addr v0, v8

    float-to-int v7, v0

    .line 1596
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->I:I

    if-eq v0, v2, :cond_a

    .line 1597
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->L:I

    sub-int v0, v4, v0

    .line 1598
    iget v8, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    sub-int v8, v7, v8

    .line 1600
    if-eqz v5, :cond_19

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v9

    iget v10, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    if-le v9, v10, :cond_19

    .line 1601
    iget v9, p0, Landroid/support/v7/widget/RecyclerView;->L:I

    iget v10, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    if-gez v0, :cond_c

    move v0, v1

    :goto_3
    mul-int/2addr v0, v10

    add-int/2addr v0, v9

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    move v0, v2

    .line 1604
    :goto_4
    if-eqz v6, :cond_9

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v9

    iget v10, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    if-le v9, v10, :cond_9

    .line 1605
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->M:I

    iget v9, p0, Landroid/support/v7/widget/RecyclerView;->P:I

    if-gez v8, :cond_d

    :goto_5
    mul-int/2addr v1, v9

    add-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/RecyclerView;->O:I

    move v0, v2

    .line 1608
    :cond_9
    if-eqz v0, :cond_a

    .line 1609
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    invoke-interface {v0, v2}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    .line 1610
    invoke-direct {p0, v2}, Landroid/support/v7/widget/RecyclerView;->e(I)V

    .line 1613
    :cond_a
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->I:I

    if-ne v0, v2, :cond_b

    .line 1614
    iget v0, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    sub-int v0, v4, v0

    .line 1615
    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->O:I

    sub-int v8, v7, v1

    .line 1616
    if-eqz v5, :cond_e

    neg-int v0, v0

    move v1, v0

    :goto_6
    if-eqz v6, :cond_f

    neg-int v0, v8

    :goto_7
    invoke-direct {p0, v1, v0}, Landroid/support/v7/widget/RecyclerView;->f(II)V

    .line 1619
    :cond_b
    iput v4, p0, Landroid/support/v7/widget/RecyclerView;->N:I

    .line 1620
    iput v7, p0, Landroid/support/v7/widget/RecyclerView;->O:I

    goto/16 :goto_2

    :cond_c
    move v0, v2

    .line 1601
    goto :goto_3

    :cond_d
    move v1, v2

    .line 1605
    goto :goto_5

    :cond_e
    move v1, v3

    .line 1616
    goto :goto_6

    :cond_f
    move v0, v3

    goto :goto_7

    .line 1624
    :pswitch_4
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_2

    .line 1628
    :pswitch_5
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/view/VelocityTracker;

    const/16 v1, 0x3e8

    iget v7, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    int-to-float v7, v7

    invoke-virtual {v0, v1, v7}, Landroid/view/VelocityTracker;->computeCurrentVelocity(IF)V

    .line 1629
    if-eqz v5, :cond_16

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/view/VelocityTracker;

    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->J:I

    invoke-static {v0, v1}, Landroid/support/v4/view/au;->a(Landroid/view/VelocityTracker;I)F

    move-result v0

    neg-float v0, v0

    move v5, v0

    .line 1631
    :goto_8
    if-eqz v6, :cond_17

    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/view/VelocityTracker;

    iget v1, p0, Landroid/support/v7/widget/RecyclerView;->J:I

    invoke-static {v0, v1}, Landroid/support/v4/view/au;->b(Landroid/view/VelocityTracker;I)F

    move-result v0

    neg-float v0, v0

    move v1, v0

    .line 1633
    :goto_9
    cmpl-float v0, v5, v4

    if-nez v0, :cond_10

    cmpl-float v0, v1, v4

    if-eqz v0, :cond_14

    :cond_10
    float-to-int v0, v5

    float-to-int v1, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v5, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    if-ge v4, v5, :cond_11

    move v0, v3

    :cond_11
    invoke-static {v1}, Ljava/lang/Math;->abs(I)I

    move-result v4

    iget v5, p0, Landroid/support/v7/widget/RecyclerView;->Q:I

    if-ge v4, v5, :cond_12

    move v1, v3

    :cond_12
    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    neg-int v4, v4

    iget v5, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v4, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    neg-int v4, v4

    iget v5, p0, Landroid/support/v7/widget/RecyclerView;->R:I

    invoke-static {v1, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    if-nez v0, :cond_13

    if-eqz v1, :cond_18

    :cond_13
    iget-object v4, p0, Landroid/support/v7/widget/RecyclerView;->S:Landroid/support/v7/widget/cr;

    invoke-virtual {v4, v0, v1}, Landroid/support/v7/widget/cr;->a(II)V

    move v0, v2

    :goto_a
    if-nez v0, :cond_15

    .line 1634
    :cond_14
    invoke-direct {p0, v3}, Landroid/support/v7/widget/RecyclerView;->e(I)V

    .line 1636
    :cond_15
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->K:Landroid/view/VelocityTracker;

    invoke-virtual {v0}, Landroid/view/VelocityTracker;->clear()V

    .line 1637
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->m()V

    goto/16 :goto_2

    :cond_16
    move v5, v4

    .line 1629
    goto :goto_8

    :cond_17
    move v1, v4

    .line 1631
    goto :goto_9

    :cond_18
    move v0, v3

    .line 1633
    goto :goto_a

    .line 1641
    :pswitch_6
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->s()V

    goto/16 :goto_2

    :cond_19
    move v0, v3

    goto/16 :goto_4

    .line 1573
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_5
        :pswitch_3
        :pswitch_6
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public requestChildFocus(Landroid/view/View;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1316
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    if-eqz p2, :cond_1

    .line 1317
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v2

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {v1, v0, v0, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1318
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    invoke-virtual {p0, p2, v1}, Landroid/support/v7/widget/RecyclerView;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1319
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    invoke-virtual {p0, p1, v1}, Landroid/support/v7/widget/RecyclerView;->offsetRectIntoDescendantCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 1320
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->n:Landroid/graphics/Rect;

    iget-boolean v2, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    if-nez v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {p0, p1, v1, v0}, Landroid/support/v7/widget/RecyclerView;->requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z

    .line 1322
    :cond_1
    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->requestChildFocus(Landroid/view/View;Landroid/view/View;)V

    .line 1323
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 1

    .prologue
    .line 1327
    iget-object v0, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v0, p0, p1, p2, p3}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;Z)Z

    move-result v0

    return v0
.end method

.method public requestLayout()V
    .locals 1

    .prologue
    .line 2162
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->x:Z

    if-nez v0, :cond_0

    .line 2163
    invoke-super {p0}, Landroid/view/ViewGroup;->requestLayout()V

    .line 2167
    :goto_0
    return-void

    .line 2165
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->y:Z

    goto :goto_0
.end method

.method public scrollBy(II)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 875
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    if-nez v1, :cond_0

    .line 876
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot scroll without a LayoutManager set. Call setLayoutManager with a non-null argument."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 879
    :cond_0
    iget-object v1, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v1}, Landroid/support/v7/widget/ce;->e()Z

    move-result v1

    .line 880
    iget-object v2, p0, Landroid/support/v7/widget/RecyclerView;->p:Landroid/support/v7/widget/ce;

    invoke-virtual {v2}, Landroid/support/v7/widget/ce;->f()Z

    move-result v2

    .line 881
    if-nez v1, :cond_1

    if-eqz v2, :cond_2

    .line 882
    :cond_1
    if-eqz v1, :cond_3

    :goto_0
    if-eqz v2, :cond_4

    :goto_1
    invoke-direct {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView;->f(II)V

    .line 884
    :cond_2
    return-void

    :cond_3
    move p1, v0

    .line 882
    goto :goto_0

    :cond_4
    move p2, v0

    goto :goto_1
.end method

.method public scrollTo(II)V
    .locals 2

    .prologue
    .line 869
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "RecyclerView does not support scrolling to an absolute position."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setClipToPadding(Z)V
    .locals 1

    .prologue
    .line 461
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    if-eq p1, v0, :cond_0

    .line 462
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView;->r()V

    .line 464
    :cond_0
    iput-boolean p1, p0, Landroid/support/v7/widget/RecyclerView;->l:Z

    .line 465
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->setClipToPadding(Z)V

    .line 466
    iget-boolean v0, p0, Landroid/support/v7/widget/RecyclerView;->w:Z

    if-eqz v0, :cond_1

    .line 467
    invoke-virtual {p0}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    .line 469
    :cond_1
    return-void
.end method
