.class public final Landroid/support/v7/widget/bw;
.super Landroid/database/Observable;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 7354
    invoke-direct {p0}, Landroid/database/Observable;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 7364
    iget-object v0, p0, Landroid/support/v7/widget/bw;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 7365
    iget-object v0, p0, Landroid/support/v7/widget/bw;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/bx;

    invoke-virtual {v0}, Landroid/support/v7/widget/bx;->a()V

    .line 7364
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 7367
    :cond_0
    return-void
.end method

.method public final a(II)V
    .locals 2

    .prologue
    .line 7374
    iget-object v0, p0, Landroid/support/v7/widget/bw;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 7375
    iget-object v0, p0, Landroid/support/v7/widget/bw;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/bx;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/bx;->a(II)V

    .line 7374
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 7377
    :cond_0
    return-void
.end method

.method public final b(II)V
    .locals 2

    .prologue
    .line 7384
    iget-object v0, p0, Landroid/support/v7/widget/bw;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 7385
    iget-object v0, p0, Landroid/support/v7/widget/bw;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/bx;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/bx;->b(II)V

    .line 7384
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 7387
    :cond_0
    return-void
.end method

.method public final c(II)V
    .locals 2

    .prologue
    .line 7394
    iget-object v0, p0, Landroid/support/v7/widget/bw;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 7395
    iget-object v0, p0, Landroid/support/v7/widget/bw;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/bx;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/bx;->c(II)V

    .line 7394
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 7397
    :cond_0
    return-void
.end method

.method public final d(II)V
    .locals 2

    .prologue
    .line 7400
    iget-object v0, p0, Landroid/support/v7/widget/bw;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 7401
    iget-object v0, p0, Landroid/support/v7/widget/bw;->mObservers:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/bx;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/bx;->d(II)V

    .line 7400
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 7403
    :cond_0
    return-void
.end method
