.class final Landroid/support/v7/widget/dt;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/util/ArrayList;

.field b:I

.field c:I

.field d:I

.field final e:I

.field final synthetic f:Landroid/support/v7/widget/StaggeredGridLayoutManager;


# direct methods
.method private f()V
    .locals 3

    .prologue
    .line 1951
    iget-object v0, p0, Landroid/support/v7/widget/dt;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1952
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/dq;

    .line 1953
    iget-object v2, p0, Landroid/support/v7/widget/dt;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/bk;->a(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/dt;->b:I

    .line 1954
    iget-boolean v0, v1, Landroid/support/v7/widget/dq;->f:Z

    if-eqz v0, :cond_0

    .line 1955
    iget-object v0, p0, Landroid/support/v7/widget/dt;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    invoke-virtual {v1}, Landroid/support/v7/widget/dq;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->d(I)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    move-result-object v0

    .line 1957
    if-eqz v0, :cond_0

    iget v1, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->b:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    .line 1958
    iget v1, p0, Landroid/support/v7/widget/dt;->b:I

    iget v2, p0, Landroid/support/v7/widget/dt;->e:I

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a(I)I

    move-result v0

    sub-int v0, v1, v0

    iput v0, p0, Landroid/support/v7/widget/dt;->b:I

    .line 1961
    :cond_0
    return-void
.end method

.method private g()V
    .locals 3

    .prologue
    .line 1985
    iget-object v0, p0, Landroid/support/v7/widget/dt;->a:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/dt;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 1986
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/dq;

    .line 1987
    iget-object v2, p0, Landroid/support/v7/widget/dt;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/bk;->b(Landroid/view/View;)I

    move-result v0

    iput v0, p0, Landroid/support/v7/widget/dt;->c:I

    .line 1988
    iget-boolean v0, v1, Landroid/support/v7/widget/dq;->f:Z

    if-eqz v0, :cond_0

    .line 1989
    iget-object v0, p0, Landroid/support/v7/widget/dt;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v0, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;

    invoke-virtual {v1}, Landroid/support/v7/widget/dq;->f()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup;->d(I)Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;

    move-result-object v0

    .line 1991
    if-eqz v0, :cond_0

    iget v1, v0, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->b:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 1992
    iget v1, p0, Landroid/support/v7/widget/dt;->c:I

    iget v2, p0, Landroid/support/v7/widget/dt;->e:I

    invoke-virtual {v0, v2}, Landroid/support/v7/widget/StaggeredGridLayoutManager$LazySpanLookup$FullSpanItem;->a(I)I

    move-result v0

    add-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/dt;->c:I

    .line 1995
    :cond_0
    return-void
.end method


# virtual methods
.method final a()I
    .locals 2

    .prologue
    .line 1965
    iget v0, p0, Landroid/support/v7/widget/dt;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 1966
    iget v0, p0, Landroid/support/v7/widget/dt;->b:I

    .line 1969
    :goto_0
    return v0

    .line 1968
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/dt;->f()V

    .line 1969
    iget v0, p0, Landroid/support/v7/widget/dt;->b:I

    goto :goto_0
.end method

.method final a(I)I
    .locals 2

    .prologue
    .line 1940
    iget v0, p0, Landroid/support/v7/widget/dt;->b:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 1941
    iget p1, p0, Landroid/support/v7/widget/dt;->b:I

    .line 1947
    :cond_0
    :goto_0
    return p1

    .line 1943
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/dt;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-eqz v0, :cond_0

    .line 1946
    invoke-direct {p0}, Landroid/support/v7/widget/dt;->f()V

    .line 1947
    iget p1, p0, Landroid/support/v7/widget/dt;->b:I

    goto :goto_0
.end method

.method final a(Landroid/view/View;)V
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 2007
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/dq;

    .line 2008
    iput-object p0, v0, Landroid/support/v7/widget/dq;->e:Landroid/support/v7/widget/dt;

    .line 2009
    iget-object v1, p0, Landroid/support/v7/widget/dt;->a:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    .line 2010
    iput v3, p0, Landroid/support/v7/widget/dt;->b:I

    .line 2011
    iget-object v1, p0, Landroid/support/v7/widget/dt;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2012
    iput v3, p0, Landroid/support/v7/widget/dt;->c:I

    .line 2014
    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/widget/dq;->d()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/dq;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2015
    :cond_1
    iget v0, p0, Landroid/support/v7/widget/dt;->d:I

    iget-object v1, p0, Landroid/support/v7/widget/dt;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v1, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/bk;->c(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/dt;->d:I

    .line 2017
    :cond_2
    return-void
.end method

.method final b()I
    .locals 2

    .prologue
    .line 1999
    iget v0, p0, Landroid/support/v7/widget/dt;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_0

    .line 2000
    iget v0, p0, Landroid/support/v7/widget/dt;->c:I

    .line 2003
    :goto_0
    return v0

    .line 2002
    :cond_0
    invoke-direct {p0}, Landroid/support/v7/widget/dt;->g()V

    .line 2003
    iget v0, p0, Landroid/support/v7/widget/dt;->c:I

    goto :goto_0
.end method

.method final b(I)I
    .locals 2

    .prologue
    .line 1973
    iget v0, p0, Landroid/support/v7/widget/dt;->c:I

    const/high16 v1, -0x80000000

    if-eq v0, v1, :cond_1

    .line 1974
    iget p1, p0, Landroid/support/v7/widget/dt;->c:I

    .line 1981
    :cond_0
    :goto_0
    return p1

    .line 1976
    :cond_1
    iget-object v0, p0, Landroid/support/v7/widget/dt;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1977
    if-eqz v0, :cond_0

    .line 1980
    invoke-direct {p0}, Landroid/support/v7/widget/dt;->g()V

    .line 1981
    iget p1, p0, Landroid/support/v7/widget/dt;->c:I

    goto :goto_0
.end method

.method final b(Landroid/view/View;)V
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 2020
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/dq;

    .line 2021
    iput-object p0, v0, Landroid/support/v7/widget/dq;->e:Landroid/support/v7/widget/dt;

    .line 2022
    iget-object v1, p0, Landroid/support/v7/widget/dt;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2023
    iput v3, p0, Landroid/support/v7/widget/dt;->c:I

    .line 2024
    iget-object v1, p0, Landroid/support/v7/widget/dt;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2025
    iput v3, p0, Landroid/support/v7/widget/dt;->b:I

    .line 2027
    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/widget/dq;->d()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Landroid/support/v7/widget/dq;->e()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2028
    :cond_1
    iget v0, p0, Landroid/support/v7/widget/dt;->d:I

    iget-object v1, p0, Landroid/support/v7/widget/dt;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v1, v1, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v1, p1}, Landroid/support/v7/widget/bk;->c(Landroid/view/View;)I

    move-result v1

    add-int/2addr v0, v1

    iput v0, p0, Landroid/support/v7/widget/dt;->d:I

    .line 2030
    :cond_2
    return-void
.end method

.method final c()V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 2055
    iget-object v0, p0, Landroid/support/v7/widget/dt;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 2056
    iput v1, p0, Landroid/support/v7/widget/dt;->b:I

    iput v1, p0, Landroid/support/v7/widget/dt;->c:I

    .line 2057
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v7/widget/dt;->d:I

    .line 2058
    return-void
.end method

.method final c(I)V
    .locals 0

    .prologue
    .line 2066
    iput p1, p0, Landroid/support/v7/widget/dt;->b:I

    iput p1, p0, Landroid/support/v7/widget/dt;->c:I

    .line 2067
    return-void
.end method

.method final d()V
    .locals 5

    .prologue
    const/high16 v4, -0x80000000

    .line 2070
    iget-object v0, p0, Landroid/support/v7/widget/dt;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 2071
    iget-object v0, p0, Landroid/support/v7/widget/dt;->a:Ljava/util/ArrayList;

    add-int/lit8 v1, v2, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2072
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/dq;

    .line 2073
    const/4 v3, 0x0

    iput-object v3, v1, Landroid/support/v7/widget/dq;->e:Landroid/support/v7/widget/dt;

    .line 2074
    invoke-virtual {v1}, Landroid/support/v7/widget/dq;->d()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Landroid/support/v7/widget/dq;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2075
    :cond_0
    iget v1, p0, Landroid/support/v7/widget/dt;->d:I

    iget-object v3, p0, Landroid/support/v7/widget/dt;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v3, v3, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/bk;->c(Landroid/view/View;)I

    move-result v0

    sub-int v0, v1, v0

    iput v0, p0, Landroid/support/v7/widget/dt;->d:I

    .line 2077
    :cond_1
    const/4 v0, 0x1

    if-ne v2, v0, :cond_2

    .line 2078
    iput v4, p0, Landroid/support/v7/widget/dt;->b:I

    .line 2080
    :cond_2
    iput v4, p0, Landroid/support/v7/widget/dt;->c:I

    .line 2081
    return-void
.end method

.method final d(I)V
    .locals 2

    .prologue
    const/high16 v1, -0x80000000

    .line 2105
    iget v0, p0, Landroid/support/v7/widget/dt;->b:I

    if-eq v0, v1, :cond_0

    .line 2106
    iget v0, p0, Landroid/support/v7/widget/dt;->b:I

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/widget/dt;->b:I

    .line 2108
    :cond_0
    iget v0, p0, Landroid/support/v7/widget/dt;->c:I

    if-eq v0, v1, :cond_1

    .line 2109
    iget v0, p0, Landroid/support/v7/widget/dt;->c:I

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v7/widget/dt;->c:I

    .line 2111
    :cond_1
    return-void
.end method

.method final e()V
    .locals 4

    .prologue
    const/high16 v3, -0x80000000

    .line 2084
    iget-object v0, p0, Landroid/support/v7/widget/dt;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 2085
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/dq;

    .line 2086
    const/4 v2, 0x0

    iput-object v2, v1, Landroid/support/v7/widget/dq;->e:Landroid/support/v7/widget/dt;

    .line 2087
    iget-object v2, p0, Landroid/support/v7/widget/dt;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_0

    .line 2088
    iput v3, p0, Landroid/support/v7/widget/dt;->c:I

    .line 2090
    :cond_0
    invoke-virtual {v1}, Landroid/support/v7/widget/dq;->d()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1}, Landroid/support/v7/widget/dq;->e()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2091
    :cond_1
    iget v1, p0, Landroid/support/v7/widget/dt;->d:I

    iget-object v2, p0, Landroid/support/v7/widget/dt;->f:Landroid/support/v7/widget/StaggeredGridLayoutManager;

    iget-object v2, v2, Landroid/support/v7/widget/StaggeredGridLayoutManager;->a:Landroid/support/v7/widget/bk;

    invoke-virtual {v2, v0}, Landroid/support/v7/widget/bk;->c(Landroid/view/View;)I

    move-result v0

    sub-int v0, v1, v0

    iput v0, p0, Landroid/support/v7/widget/dt;->d:I

    .line 2093
    :cond_2
    iput v3, p0, Landroid/support/v7/widget/dt;->b:I

    .line 2094
    return-void
.end method
