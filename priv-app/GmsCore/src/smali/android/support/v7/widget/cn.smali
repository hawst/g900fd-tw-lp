.class public abstract Landroid/support/v7/widget/cn;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field b:Landroid/support/v7/widget/RecyclerView;

.field c:Landroid/support/v7/widget/ce;

.field d:Z

.field e:Z

.field f:Landroid/view/View;

.field private final g:Landroid/support/v7/widget/co;


# direct methods
.method static synthetic a(Landroid/support/v7/widget/cn;II)V
    .locals 2

    .prologue
    .line 6993
    iget-boolean v0, p0, Landroid/support/v7/widget/cn;->e:Z

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v7/widget/cn;->a:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Landroid/support/v7/widget/cn;->a()V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/cn;->d:Z

    iget-object v0, p0, Landroid/support/v7/widget/cn;->f:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v7/widget/cn;->f:Landroid/view/View;

    invoke-virtual {p0, v0}, Landroid/support/v7/widget/cn;->a(Landroid/view/View;)I

    move-result v0

    iget v1, p0, Landroid/support/v7/widget/cn;->a:I

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Landroid/support/v7/widget/cn;->f:Landroid/view/View;

    iget-object v0, p0, Landroid/support/v7/widget/cn;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, p0, Landroid/support/v7/widget/cn;->g:Landroid/support/v7/widget/co;

    iget-object v0, p0, Landroid/support/v7/widget/cn;->g:Landroid/support/v7/widget/co;

    iget-object v1, p0, Landroid/support/v7/widget/cn;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v1}, Landroid/support/v7/widget/co;->a(Landroid/support/v7/widget/co;Landroid/support/v7/widget/RecyclerView;)V

    invoke-virtual {p0}, Landroid/support/v7/widget/cn;->a()V

    :cond_2
    :goto_0
    iget-boolean v0, p0, Landroid/support/v7/widget/cn;->e:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v7/widget/cn;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iget-object v0, p0, Landroid/support/v7/widget/cn;->g:Landroid/support/v7/widget/co;

    iget-object v0, p0, Landroid/support/v7/widget/cn;->g:Landroid/support/v7/widget/co;

    iget-object v1, p0, Landroid/support/v7/widget/cn;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v1}, Landroid/support/v7/widget/co;->a(Landroid/support/v7/widget/co;Landroid/support/v7/widget/RecyclerView;)V

    :cond_3
    return-void

    :cond_4
    const-string v0, "RecyclerView"

    const-string v1, "Passed over target position while smooth scrolling."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v7/widget/cn;->f:Landroid/view/View;

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;)I
    .locals 1

    .prologue
    .line 7128
    iget-object v0, p0, Landroid/support/v7/widget/cn;->b:Landroid/support/v7/widget/RecyclerView;

    invoke-static {p1}, Landroid/support/v7/widget/RecyclerView;->c(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method protected final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 7057
    iget-boolean v0, p0, Landroid/support/v7/widget/cn;->e:Z

    if-nez v0, :cond_0

    .line 7071
    :goto_0
    return-void

    .line 7060
    :cond_0
    iget-object v0, p0, Landroid/support/v7/widget/cn;->b:Landroid/support/v7/widget/RecyclerView;

    iget-object v0, v0, Landroid/support/v7/widget/RecyclerView;->f:Landroid/support/v7/widget/cp;

    iput v2, v0, Landroid/support/v7/widget/cp;->a:I

    .line 7062
    iput-object v1, p0, Landroid/support/v7/widget/cn;->f:Landroid/view/View;

    .line 7063
    iput v2, p0, Landroid/support/v7/widget/cn;->a:I

    .line 7064
    iput-boolean v3, p0, Landroid/support/v7/widget/cn;->d:Z

    .line 7065
    iput-boolean v3, p0, Landroid/support/v7/widget/cn;->e:Z

    .line 7067
    iget-object v0, p0, Landroid/support/v7/widget/cn;->c:Landroid/support/v7/widget/ce;

    invoke-static {v0, p0}, Landroid/support/v7/widget/ce;->a(Landroid/support/v7/widget/ce;Landroid/support/v7/widget/cn;)V

    .line 7069
    iput-object v1, p0, Landroid/support/v7/widget/cn;->c:Landroid/support/v7/widget/ce;

    .line 7070
    iput-object v1, p0, Landroid/support/v7/widget/cn;->b:Landroid/support/v7/widget/RecyclerView;

    goto :goto_0
.end method
