.class public abstract Landroid/support/v7/widget/bv;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/support/v7/widget/bw;

.field b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3978
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 3979
    new-instance v0, Landroid/support/v7/widget/bw;

    invoke-direct {v0}, Landroid/support/v7/widget/bw;-><init>()V

    iput-object v0, p0, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    .line 3980
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v7/widget/bv;->b:Z

    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method public a(I)I
    .locals 1

    .prologue
    .line 4066
    const/4 v0, 0x0

    return v0
.end method

.method public abstract a(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/cs;
.end method

.method public final a(II)V
    .locals 1

    .prologue
    .line 4260
    iget-object v0, p0, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/bw;->a(II)V

    .line 4261
    return-void
.end method

.method public final a(Landroid/support/v7/widget/bx;)V
    .locals 1

    .prologue
    .line 4181
    iget-object v0, p0, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/bw;->registerObserver(Ljava/lang/Object;)V

    .line 4182
    return-void
.end method

.method public a(Landroid/support/v7/widget/cs;)V
    .locals 0

    .prologue
    .line 4153
    return-void
.end method

.method public abstract a(Landroid/support/v7/widget/cs;I)V
.end method

.method public b(I)J
    .locals 2

    .prologue
    .line 4094
    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final b(II)V
    .locals 1

    .prologue
    .line 4292
    iget-object v0, p0, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/bw;->d(II)V

    .line 4293
    return-void
.end method

.method public final b(Landroid/support/v7/widget/bx;)V
    .locals 1

    .prologue
    .line 4195
    iget-object v0, p0, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/bw;->unregisterObserver(Ljava/lang/Object;)V

    .line 4196
    return-void
.end method

.method public final b(Landroid/support/v7/widget/cs;I)V
    .locals 2

    .prologue
    .line 4044
    iput p2, p1, Landroid/support/v7/widget/cs;->b:I

    .line 4045
    iget-boolean v0, p0, Landroid/support/v7/widget/bv;->b:Z

    if-eqz v0, :cond_0

    .line 4046
    invoke-virtual {p0, p2}, Landroid/support/v7/widget/bv;->b(I)J

    move-result-wide v0

    iput-wide v0, p1, Landroid/support/v7/widget/cs;->d:J

    .line 4048
    :cond_0
    invoke-virtual {p0, p1, p2}, Landroid/support/v7/widget/bv;->a(Landroid/support/v7/widget/cs;I)V

    .line 4049
    const/4 v0, 0x1

    const/4 v1, 0x7

    invoke-virtual {p1, v0, v1}, Landroid/support/v7/widget/cs;->a(II)V

    .line 4051
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 4243
    iget-object v0, p0, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/bw;->a(II)V

    .line 4244
    return-void
.end method

.method public final c(II)V
    .locals 1

    .prologue
    .line 4311
    iget-object v0, p0, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/bw;->b(II)V

    .line 4312
    return-void
.end method

.method public final d(I)V
    .locals 2

    .prologue
    .line 4277
    iget-object v0, p0, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/bw;->b(II)V

    .line 4278
    return-void
.end method

.method public final d(II)V
    .locals 1

    .prologue
    .line 4345
    iget-object v0, p0, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    invoke-virtual {v0, p1, p2}, Landroid/support/v7/widget/bw;->c(II)V

    .line 4346
    return-void
.end method

.method public final e(I)V
    .locals 2

    .prologue
    .line 4328
    iget-object v0, p0, Landroid/support/v7/widget/bv;->a:Landroid/support/v7/widget/bw;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/support/v7/widget/bw;->c(II)V

    .line 4329
    return-void
.end method
