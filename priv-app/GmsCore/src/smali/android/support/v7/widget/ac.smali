.class final Landroid/support/v7/widget/ac;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/ArrayList;

.field final synthetic b:Landroid/support/v7/widget/aa;


# direct methods
.method constructor <init>(Landroid/support/v7/widget/aa;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/aa;

    iput-object p2, p0, Landroid/support/v7/widget/ac;->a:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 143
    iget-object v0, p0, Landroid/support/v7/widget/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/aj;

    .line 144
    iget-object v3, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/aa;

    iget-object v1, v0, Landroid/support/v7/widget/aj;->a:Landroid/support/v7/widget/cs;

    iget-object v4, v1, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    iget-object v1, v0, Landroid/support/v7/widget/aj;->b:Landroid/support/v7/widget/cs;

    if-eqz v1, :cond_1

    iget-object v1, v1, Landroid/support/v7/widget/cs;->a:Landroid/view/View;

    :goto_1
    iget-object v5, v3, Landroid/support/v7/widget/aa;->g:Ljava/util/ArrayList;

    iget-object v6, v0, Landroid/support/v7/widget/aj;->a:Landroid/support/v7/widget/cs;

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v4}, Landroid/support/v4/view/ay;->s(Landroid/view/View;)Landroid/support/v4/view/cr;

    move-result-object v4

    iget-wide v6, v3, Landroid/support/v7/widget/by;->m:J

    invoke-virtual {v4, v6, v7}, Landroid/support/v4/view/cr;->a(J)Landroid/support/v4/view/cr;

    move-result-object v4

    iget v5, v0, Landroid/support/v7/widget/aj;->e:I

    iget v6, v0, Landroid/support/v7/widget/aj;->c:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/support/v4/view/cr;->b(F)Landroid/support/v4/view/cr;

    iget v5, v0, Landroid/support/v7/widget/aj;->f:I

    iget v6, v0, Landroid/support/v7/widget/aj;->d:I

    sub-int/2addr v5, v6

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/support/v4/view/cr;->c(F)Landroid/support/v4/view/cr;

    invoke-virtual {v4, v8}, Landroid/support/v4/view/cr;->a(F)Landroid/support/v4/view/cr;

    move-result-object v5

    new-instance v6, Landroid/support/v7/widget/ah;

    invoke-direct {v6, v3, v0, v4}, Landroid/support/v7/widget/ah;-><init>(Landroid/support/v7/widget/aa;Landroid/support/v7/widget/aj;Landroid/support/v4/view/cr;)V

    invoke-virtual {v5, v6}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/dg;)Landroid/support/v4/view/cr;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/view/cr;->c()V

    if-eqz v1, :cond_0

    iget-object v4, v3, Landroid/support/v7/widget/aa;->g:Ljava/util/ArrayList;

    iget-object v5, v0, Landroid/support/v7/widget/aj;->b:Landroid/support/v7/widget/cs;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v1}, Landroid/support/v4/view/ay;->s(Landroid/view/View;)Landroid/support/v4/view/cr;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/support/v4/view/cr;->b(F)Landroid/support/v4/view/cr;

    move-result-object v5

    invoke-virtual {v5, v8}, Landroid/support/v4/view/cr;->c(F)Landroid/support/v4/view/cr;

    move-result-object v5

    iget-wide v6, v3, Landroid/support/v7/widget/by;->m:J

    invoke-virtual {v5, v6, v7}, Landroid/support/v4/view/cr;->a(J)Landroid/support/v4/view/cr;

    move-result-object v5

    const/high16 v6, 0x3f800000    # 1.0f

    invoke-virtual {v5, v6}, Landroid/support/v4/view/cr;->a(F)Landroid/support/v4/view/cr;

    move-result-object v5

    new-instance v6, Landroid/support/v7/widget/ai;

    invoke-direct {v6, v3, v0, v4, v1}, Landroid/support/v7/widget/ai;-><init>(Landroid/support/v7/widget/aa;Landroid/support/v7/widget/aj;Landroid/support/v4/view/cr;Landroid/view/View;)V

    invoke-virtual {v5, v6}, Landroid/support/v4/view/cr;->a(Landroid/support/v4/view/dg;)Landroid/support/v4/view/cr;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/cr;->c()V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 146
    :cond_2
    iget-object v0, p0, Landroid/support/v7/widget/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 147
    iget-object v0, p0, Landroid/support/v7/widget/ac;->b:Landroid/support/v7/widget/aa;

    iget-object v0, v0, Landroid/support/v7/widget/aa;->c:Ljava/util/ArrayList;

    iget-object v1, p0, Landroid/support/v7/widget/ac;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 148
    return-void
.end method
