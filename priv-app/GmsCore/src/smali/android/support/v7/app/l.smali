.class final Landroid/support/v7/app/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/d/b;


# instance fields
.field final synthetic a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

.field private b:Landroid/support/v7/d/b;


# direct methods
.method public constructor <init>(Landroid/support/v7/app/ActionBarActivityDelegateBase;Landroid/support/v7/d/b;)V
    .locals 0

    .prologue
    .line 1362
    iput-object p1, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1363
    iput-object p2, p0, Landroid/support/v7/app/l;->b:Landroid/support/v7/d/b;

    .line 1364
    return-void
.end method


# virtual methods
.method public final a(Landroid/support/v7/d/a;)V
    .locals 2

    .prologue
    .line 1379
    iget-object v0, p0, Landroid/support/v7/app/l;->b:Landroid/support/v7/d/b;

    invoke-interface {v0, p1}, Landroid/support/v7/d/b;->a(Landroid/support/v7/d/a;)V

    .line 1380
    iget-object v0, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->l:Landroid/widget/PopupWindow;

    if-eqz v0, :cond_3

    .line 1381
    iget-object v0, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/d;

    invoke-virtual {v0}, Landroid/support/v7/app/d;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v1, v1, Landroid/support/v7/app/ActionBarActivityDelegateBase;->m:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 1382
    iget-object v0, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->l:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    .line 1389
    :cond_0
    :goto_0
    iget-object v0, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_1

    .line 1390
    iget-object v0, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->removeAllViews()V

    .line 1392
    :cond_1
    iget-object v0, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/d;

    if-eqz v0, :cond_2

    .line 1394
    :try_start_0
    iget-object v0, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->a:Landroid/support/v7/app/d;

    iget-object v0, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/d/a;
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    .line 1399
    :cond_2
    :goto_1
    iget-object v0, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->j:Landroid/support/v7/d/a;

    .line 1400
    return-void

    .line 1383
    :cond_3
    iget-object v0, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    if-eqz v0, :cond_0

    .line 1384
    iget-object v0, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/support/v7/internal/widget/ActionBarContextView;->setVisibility(I)V

    .line 1385
    iget-object v0, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1386
    iget-object v0, p0, Landroid/support/v7/app/l;->a:Landroid/support/v7/app/ActionBarActivityDelegateBase;

    iget-object v0, v0, Landroid/support/v7/app/ActionBarActivityDelegateBase;->k:Landroid/support/v7/internal/widget/ActionBarContextView;

    invoke-virtual {v0}, Landroid/support/v7/internal/widget/ActionBarContextView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    invoke-static {v0}, Landroid/support/v4/view/ay;->v(Landroid/view/View;)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final a(Landroid/support/v7/d/a;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 1367
    iget-object v0, p0, Landroid/support/v7/app/l;->b:Landroid/support/v7/d/b;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/d/b;->a(Landroid/support/v7/d/a;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method

.method public final a(Landroid/support/v7/d/a;Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 1375
    iget-object v0, p0, Landroid/support/v7/app/l;->b:Landroid/support/v7/d/b;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/d/b;->a(Landroid/support/v7/d/a;Landroid/view/MenuItem;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/support/v7/d/a;Landroid/view/Menu;)Z
    .locals 1

    .prologue
    .line 1371
    iget-object v0, p0, Landroid/support/v7/app/l;->b:Landroid/support/v7/d/b;

    invoke-interface {v0, p1, p2}, Landroid/support/v7/d/b;->b(Landroid/support/v7/d/a;Landroid/view/Menu;)Z

    move-result v0

    return v0
.end method
