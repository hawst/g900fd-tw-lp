.class public final Landroid/support/v7/internal/view/c;
.super Landroid/view/ActionMode;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field final a:Landroid/view/MenuInflater;

.field final b:Landroid/support/v7/d/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/support/v7/d/a;)V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Landroid/view/ActionMode;-><init>()V

    .line 44
    iput-object p2, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    .line 45
    new-instance v0, Landroid/support/v7/internal/view/e;

    invoke-direct {v0, p1}, Landroid/support/v7/internal/view/e;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Landroid/support/v7/internal/view/c;->a:Landroid/view/MenuInflater;

    .line 46
    return-void
.end method


# virtual methods
.method public final finish()V
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    invoke-virtual {v0}, Landroid/support/v7/d/a;->b()V

    .line 76
    return-void
.end method

.method public final getCustomView()Landroid/view/View;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    invoke-virtual {v0}, Landroid/support/v7/d/a;->h()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final getMenu()Landroid/view/Menu;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    invoke-virtual {v0}, Landroid/support/v7/d/a;->a()Landroid/view/Menu;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v7/internal/view/menu/ab;->a(Landroid/view/Menu;)Landroid/view/Menu;

    move-result-object v0

    return-object v0
.end method

.method public final getMenuInflater()Landroid/view/MenuInflater;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->a:Landroid/view/MenuInflater;

    return-object v0
.end method

.method public final getSubtitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 95
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    invoke-virtual {v0}, Landroid/support/v7/d/a;->f()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    iget-object v0, v0, Landroid/support/v7/d/a;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    invoke-virtual {v0}, Landroid/support/v7/d/a;->e()Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public final getTitleOptionalHint()Z
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    iget-boolean v0, v0, Landroid/support/v7/d/a;->c:Z

    return v0
.end method

.method public final invalidate()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    invoke-virtual {v0}, Landroid/support/v7/d/a;->c()V

    .line 71
    return-void
.end method

.method public final isTitleOptional()Z
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    invoke-virtual {v0}, Landroid/support/v7/d/a;->g()Z

    move-result v0

    return v0
.end method

.method public final setCustomView(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    invoke-virtual {v0, p1}, Landroid/support/v7/d/a;->a(Landroid/view/View;)V

    .line 111
    return-void
.end method

.method public final setSubtitle(I)V
    .locals 1

    .prologue
    .line 100
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    invoke-virtual {v0, p1}, Landroid/support/v7/d/a;->b(I)V

    .line 101
    return-void
.end method

.method public final setSubtitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    invoke-virtual {v0, p1}, Landroid/support/v7/d/a;->a(Ljava/lang/CharSequence;)V

    .line 66
    return-void
.end method

.method public final setTag(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    iput-object p1, v0, Landroid/support/v7/d/a;->b:Ljava/lang/Object;

    .line 56
    return-void
.end method

.method public final setTitle(I)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    invoke-virtual {v0, p1}, Landroid/support/v7/d/a;->a(I)V

    .line 91
    return-void
.end method

.method public final setTitle(Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    invoke-virtual {v0, p1}, Landroid/support/v7/d/a;->b(Ljava/lang/CharSequence;)V

    .line 61
    return-void
.end method

.method public final setTitleOptionalHint(Z)V
    .locals 1

    .prologue
    .line 125
    iget-object v0, p0, Landroid/support/v7/internal/view/c;->b:Landroid/support/v7/d/a;

    invoke-virtual {v0, p1}, Landroid/support/v7/d/a;->a(Z)V

    .line 126
    return-void
.end method
