.class public abstract Landroid/support/v7/c/n;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Z


# instance fields
.field private final b:Ljava/util/ArrayList;

.field private final c:Landroid/support/v7/c/s;

.field private final d:Landroid/os/Messenger;

.field private final e:Landroid/support/v7/c/q;

.field private final f:Landroid/support/v7/c/r;

.field private g:Landroid/support/v7/c/f;

.field private h:Landroid/support/v7/c/e;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 58
    const-string v0, "MediaRouteProviderSrv"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Landroid/support/v7/c/n;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 84
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 60
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/n;->b:Ljava/util/ArrayList;

    .line 85
    new-instance v0, Landroid/support/v7/c/s;

    invoke-direct {v0, p0}, Landroid/support/v7/c/s;-><init>(Landroid/support/v7/c/n;)V

    iput-object v0, p0, Landroid/support/v7/c/n;->c:Landroid/support/v7/c/s;

    .line 86
    new-instance v0, Landroid/os/Messenger;

    iget-object v1, p0, Landroid/support/v7/c/n;->c:Landroid/support/v7/c/s;

    invoke-direct {v0, v1}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Landroid/support/v7/c/n;->d:Landroid/os/Messenger;

    .line 87
    new-instance v0, Landroid/support/v7/c/q;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/c/q;-><init>(Landroid/support/v7/c/n;B)V

    iput-object v0, p0, Landroid/support/v7/c/n;->e:Landroid/support/v7/c/q;

    .line 88
    new-instance v0, Landroid/support/v7/c/r;

    invoke-direct {v0, p0, v2}, Landroid/support/v7/c/r;-><init>(Landroid/support/v7/c/n;B)V

    iput-object v0, p0, Landroid/support/v7/c/n;->f:Landroid/support/v7/c/r;

    .line 89
    return-void
.end method

.method static synthetic a(Landroid/support/v7/c/n;Landroid/os/Messenger;)I
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/support/v7/c/n;->c(Landroid/os/Messenger;)I

    move-result v0

    return v0
.end method

.method static synthetic a(Landroid/support/v7/c/n;)Landroid/support/v7/c/f;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Landroid/support/v7/c/n;->g:Landroid/support/v7/c/f;

    return-object v0
.end method

.method static synthetic a(Landroid/os/Messenger;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 56
    invoke-static {p0}, Landroid/support/v7/c/n;->d(Landroid/os/Messenger;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Landroid/os/Messenger;I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 56
    if-eqz p1, :cond_0

    move-object v0, p0

    move v2, p1

    move v3, v1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Landroid/support/v7/c/n;->a(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method

.method private static a(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 443
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 444
    iput p1, v0, Landroid/os/Message;->what:I

    .line 445
    iput p2, v0, Landroid/os/Message;->arg1:I

    .line 446
    iput p3, v0, Landroid/os/Message;->arg2:I

    .line 447
    iput-object p4, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 448
    invoke-virtual {v0, p5}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 450
    :try_start_0
    invoke-virtual {p0, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 456
    :goto_0
    return-void

    .line 453
    :catch_0
    move-exception v0

    .line 454
    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not send message to "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Landroid/support/v7/c/n;->d(Landroid/os/Messenger;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 455
    :catch_1
    move-exception v0

    goto :goto_0
.end method

.method static synthetic a(Landroid/os/Messenger;IILjava/lang/Object;Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 56
    const/4 v3, 0x0

    move-object v0, p0

    move v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Landroid/support/v7/c/n;->a(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic a(Landroid/support/v7/c/n;Landroid/support/v7/c/k;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    const/4 v2, 0x0

    .line 56
    if-eqz p1, :cond_1

    iget-object v4, p1, Landroid/support/v7/c/k;->a:Landroid/os/Bundle;

    :goto_0
    iget-object v0, p0, Landroid/support/v7/c/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v7, v2

    :goto_1
    if-ge v7, v8, :cond_2

    iget-object v0, p0, Landroid/support/v7/c/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/support/v7/c/p;

    iget-object v0, v6, Landroid/support/v7/c/p;->a:Landroid/os/Messenger;

    const/4 v1, 0x5

    move v3, v2

    invoke-static/range {v0 .. v5}, Landroid/support/v7/c/n;->a(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    sget-boolean v0, Landroid/support/v7/c/n;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MediaRouteProviderSrv"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": Sent descriptor change event, descriptor="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_1

    :cond_1
    move-object v4, v5

    goto :goto_0

    :cond_2
    return-void
.end method

.method private a(Landroid/os/Messenger;II)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 138
    if-lez p3, :cond_3

    .line 139
    invoke-direct {p0, p1}, Landroid/support/v7/c/n;->c(Landroid/os/Messenger;)I

    move-result v0

    .line 140
    if-gez v0, :cond_3

    .line 141
    new-instance v0, Landroid/support/v7/c/p;

    invoke-direct {v0, p0, p1, p3}, Landroid/support/v7/c/p;-><init>(Landroid/support/v7/c/n;Landroid/os/Messenger;I)V

    .line 142
    invoke-virtual {v0}, Landroid/support/v7/c/p;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 143
    iget-object v1, p0, Landroid/support/v7/c/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 144
    sget-boolean v1, Landroid/support/v7/c/n;->a:Z

    if-eqz v1, :cond_0

    .line 145
    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Registered, version="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_0
    if-eqz p2, :cond_1

    .line 148
    iget-object v0, p0, Landroid/support/v7/c/n;->g:Landroid/support/v7/c/f;

    iget-object v0, v0, Landroid/support/v7/c/f;->g:Landroid/support/v7/c/k;

    .line 149
    const/4 v1, 0x2

    if-eqz v0, :cond_2

    iget-object v4, v0, Landroid/support/v7/c/k;->a:Landroid/os/Bundle;

    :goto_0
    move-object v0, p1

    move v2, p2

    invoke-static/range {v0 .. v5}, Landroid/support/v7/c/n;->a(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    .line 157
    :cond_1
    :goto_1
    return v3

    :cond_2
    move-object v4, v5

    .line 149
    goto :goto_0

    .line 157
    :cond_3
    const/4 v3, 0x0

    goto :goto_1
.end method

.method static synthetic a(Landroid/support/v7/c/n;Landroid/os/Messenger;I)Z
    .locals 4

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/support/v7/c/n;->c(Landroid/os/Messenger;)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/c/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/p;

    sget-boolean v1, Landroid/support/v7/c/n;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": Unregistered"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/c/p;->b()V

    invoke-static {p1, p2}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/c/n;Landroid/os/Messenger;II)Z
    .locals 1

    .prologue
    .line 56
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v7/c/n;->a(Landroid/os/Messenger;II)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Landroid/support/v7/c/n;Landroid/os/Messenger;III)Z
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;)Landroid/support/v7/c/p;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Landroid/support/v7/c/p;->b(I)Landroid/support/v7/c/j;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, p4}, Landroid/support/v7/c/j;->a(I)V

    sget-boolean v1, Landroid/support/v7/c/n;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route volume changed, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", volume="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/c/n;Landroid/os/Messenger;IILandroid/content/Intent;)Z
    .locals 8

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;)Landroid/support/v7/c/p;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2, p3}, Landroid/support/v7/c/p;->b(I)Landroid/support/v7/c/j;

    move-result-object v7

    if-eqz v7, :cond_2

    const/4 v0, 0x0

    if-eqz p2, :cond_0

    new-instance v0, Landroid/support/v7/c/o;

    move-object v1, p0

    move v3, p3

    move-object v4, p4

    move-object v5, p1

    move v6, p2

    invoke-direct/range {v0 .. v6}, Landroid/support/v7/c/o;-><init>(Landroid/support/v7/c/n;Landroid/support/v7/c/p;ILandroid/content/Intent;Landroid/os/Messenger;I)V

    :cond_0
    invoke-virtual {v7, p4, v0}, Landroid/support/v7/c/j;->a(Landroid/content/Intent;Landroid/support/v7/c/w;)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-boolean v0, Landroid/support/v7/c/n;->a:Z

    if-eqz v0, :cond_1

    const-string v0, "MediaRouteProviderSrv"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Route control request delivered, controllerId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", intent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/c/n;Landroid/os/Messenger;IILjava/lang/String;)Z
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;)Landroid/support/v7/c/p;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p4, p3}, Landroid/support/v7/c/p;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Landroid/support/v7/c/n;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route controller created, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", routeId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Landroid/support/v7/c/n;Landroid/os/Messenger;ILandroid/support/v7/c/e;)Z
    .locals 4

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;)Landroid/support/v7/c/p;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Landroid/support/v7/c/p;->a(Landroid/support/v7/c/e;)Z

    move-result v1

    sget-boolean v2, Landroid/support/v7/c/n;->a:Z

    if-eqz v2, :cond_0

    const-string v2, "MediaRouteProviderSrv"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ": Set discovery request, request="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, ", actuallyChanged="

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", compositeDiscoveryRequest="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v7/c/n;->h:Landroid/support/v7/c/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Landroid/os/Messenger;)Landroid/support/v7/c/p;
    .locals 2

    .prologue
    .line 414
    invoke-direct {p0, p1}, Landroid/support/v7/c/n;->c(Landroid/os/Messenger;)I

    move-result v0

    .line 415
    if-ltz v0, :cond_0

    iget-object v1, p0, Landroid/support/v7/c/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/p;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/os/Messenger;I)V
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 436
    if-eqz p1, :cond_0

    .line 437
    const/4 v1, 0x1

    const/4 v3, 0x0

    move-object v0, p0

    move v2, p1

    move-object v5, v4

    invoke-static/range {v0 .. v5}, Landroid/support/v7/c/n;->a(Landroid/os/Messenger;IIILjava/lang/Object;Landroid/os/Bundle;)V

    .line 439
    :cond_0
    return-void
.end method

.method static synthetic b(Landroid/support/v7/c/n;Landroid/os/Messenger;)V
    .locals 4

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/support/v7/c/n;->c(Landroid/os/Messenger;)I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v1, p0, Landroid/support/v7/c/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/p;

    sget-boolean v1, Landroid/support/v7/c/n;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": Binder died"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {v0}, Landroid/support/v7/c/p;->b()V

    :cond_1
    return-void
.end method

.method static synthetic b()Z
    .locals 1

    .prologue
    .line 56
    sget-boolean v0, Landroid/support/v7/c/n;->a:Z

    return v0
.end method

.method static synthetic b(Landroid/support/v7/c/n;)Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 56
    iget-object v0, p0, Landroid/support/v7/c/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v7, v6

    move v1, v6

    move-object v3, v2

    :goto_0
    if-ge v7, v8, :cond_3

    iget-object v0, p0, Landroid/support/v7/c/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/p;

    iget-object v4, v0, Landroid/support/v7/c/p;->c:Landroid/support/v7/c/e;

    if-eqz v4, :cond_b

    invoke-virtual {v4}, Landroid/support/v7/c/e;->a()Landroid/support/v7/c/t;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/c/t;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Landroid/support/v7/c/e;->b()Z

    move-result v0

    if-eqz v0, :cond_b

    :cond_0
    invoke-virtual {v4}, Landroid/support/v7/c/e;->b()Z

    move-result v0

    or-int/2addr v0, v1

    if-nez v3, :cond_1

    move-object v1, v2

    move-object v2, v4

    :goto_1
    add-int/lit8 v3, v7, 0x1

    move v7, v3

    move-object v3, v2

    move-object v2, v1

    move v1, v0

    goto :goto_0

    :cond_1
    if-nez v2, :cond_a

    new-instance v1, Landroid/support/v7/c/u;

    invoke-virtual {v3}, Landroid/support/v7/c/e;->a()Landroid/support/v7/c/t;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v7/c/u;-><init>(Landroid/support/v7/c/t;)V

    :goto_2
    invoke-virtual {v4}, Landroid/support/v7/c/e;->a()Landroid/support/v7/c/t;

    move-result-object v2

    if-nez v2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "selector must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-virtual {v2}, Landroid/support/v7/c/t;->a()Ljava/util/List;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v7/c/u;->a(Ljava/util/Collection;)Landroid/support/v7/c/u;

    move-object v2, v3

    goto :goto_1

    :cond_3
    if-eqz v2, :cond_4

    new-instance v3, Landroid/support/v7/c/e;

    iget-object v0, v2, Landroid/support/v7/c/u;->a:Ljava/util/ArrayList;

    if-nez v0, :cond_7

    sget-object v0, Landroid/support/v7/c/t;->a:Landroid/support/v7/c/t;

    :goto_3
    invoke-direct {v3, v0, v1}, Landroid/support/v7/c/e;-><init>(Landroid/support/v7/c/t;Z)V

    :cond_4
    iget-object v0, p0, Landroid/support/v7/c/n;->h:Landroid/support/v7/c/e;

    if-eq v0, v3, :cond_9

    iget-object v0, p0, Landroid/support/v7/c/n;->h:Landroid/support/v7/c/e;

    if-eqz v0, :cond_5

    iget-object v0, p0, Landroid/support/v7/c/n;->h:Landroid/support/v7/c/e;

    invoke-virtual {v0, v3}, Landroid/support/v7/c/e;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    :cond_5
    iput-object v3, p0, Landroid/support/v7/c/n;->h:Landroid/support/v7/c/e;

    iget-object v0, p0, Landroid/support/v7/c/n;->g:Landroid/support/v7/c/f;

    invoke-static {}, Landroid/support/v7/c/v;->a()V

    iget-object v1, v0, Landroid/support/v7/c/f;->e:Landroid/support/v7/c/e;

    if-eq v1, v3, :cond_6

    iget-object v1, v0, Landroid/support/v7/c/f;->e:Landroid/support/v7/c/e;

    if-eqz v1, :cond_8

    iget-object v1, v0, Landroid/support/v7/c/f;->e:Landroid/support/v7/c/e;

    invoke-virtual {v1, v3}, Landroid/support/v7/c/e;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    :cond_6
    :goto_4
    move v0, v5

    :goto_5
    return v0

    :cond_7
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string v0, "controlCategories"

    iget-object v7, v2, Landroid/support/v7/c/u;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v0, v7}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    new-instance v0, Landroid/support/v7/c/t;

    iget-object v2, v2, Landroid/support/v7/c/u;->a:Ljava/util/ArrayList;

    invoke-direct {v0, v4, v2, v6}, Landroid/support/v7/c/t;-><init>(Landroid/os/Bundle;Ljava/util/List;B)V

    goto :goto_3

    :cond_8
    iput-object v3, v0, Landroid/support/v7/c/f;->e:Landroid/support/v7/c/e;

    iget-boolean v1, v0, Landroid/support/v7/c/f;->f:Z

    if-nez v1, :cond_6

    iput-boolean v5, v0, Landroid/support/v7/c/f;->f:Z

    iget-object v0, v0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v7/c/h;->sendEmptyMessage(I)Z

    goto :goto_4

    :cond_9
    move v0, v6

    goto :goto_5

    :cond_a
    move-object v1, v2

    goto :goto_2

    :cond_b
    move v0, v1

    move-object v1, v2

    move-object v2, v3

    goto/16 :goto_1
.end method

.method static synthetic b(Landroid/support/v7/c/n;Landroid/os/Messenger;II)Z
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;)Landroid/support/v7/c/p;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Landroid/support/v7/c/p;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-boolean v1, Landroid/support/v7/c/n;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route controller released, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Landroid/support/v7/c/n;Landroid/os/Messenger;III)Z
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;)Landroid/support/v7/c/p;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Landroid/support/v7/c/p;->b(I)Landroid/support/v7/c/j;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, p4}, Landroid/support/v7/c/j;->b(I)V

    sget-boolean v1, Landroid/support/v7/c/n;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route volume updated, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", delta="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Landroid/os/Messenger;)I
    .locals 3

    .prologue
    .line 419
    iget-object v0, p0, Landroid/support/v7/c/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 420
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 421
    iget-object v0, p0, Landroid/support/v7/c/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/p;

    .line 422
    invoke-virtual {v0, p1}, Landroid/support/v7/c/p;->a(Landroid/os/Messenger;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    .line 426
    :goto_1
    return v0

    .line 420
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 426
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method static synthetic c(Landroid/support/v7/c/n;)Landroid/support/v7/c/q;
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Landroid/support/v7/c/n;->e:Landroid/support/v7/c/q;

    return-object v0
.end method

.method static synthetic c(Landroid/support/v7/c/n;Landroid/os/Messenger;II)Z
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;)Landroid/support/v7/c/p;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Landroid/support/v7/c/p;->b(I)Landroid/support/v7/c/j;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/support/v7/c/j;->b()V

    sget-boolean v1, Landroid/support/v7/c/n;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route selected, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static d(Landroid/os/Messenger;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 459
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Client connection "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Landroid/support/v7/c/n;Landroid/os/Messenger;II)Z
    .locals 3

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;)Landroid/support/v7/c/p;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0, p3}, Landroid/support/v7/c/p;->b(I)Landroid/support/v7/c/j;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/support/v7/c/j;->c()V

    sget-boolean v1, Landroid/support/v7/c/n;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "MediaRouteProviderSrv"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ": Route unselected, controllerId="

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {p1, p2}, Landroid/support/v7/c/n;->b(Landroid/os/Messenger;I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public abstract a()Landroid/support/v7/c/f;
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4

    .prologue
    .line 113
    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.media.MediaRouteProviderService"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    iget-object v0, p0, Landroid/support/v7/c/n;->g:Landroid/support/v7/c/f;

    if-nez v0, :cond_1

    .line 115
    invoke-virtual {p0}, Landroid/support/v7/c/n;->a()Landroid/support/v7/c/f;

    move-result-object v0

    .line 116
    if-eqz v0, :cond_1

    .line 117
    iget-object v1, v0, Landroid/support/v7/c/f;->b:Landroid/support/v7/c/i;

    iget-object v1, v1, Landroid/support/v7/c/i;->a:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 118
    invoke-virtual {p0}, Landroid/support/v7/c/n;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 119
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onCreateMediaRouteProvider() returned a provider whose package name does not match the package name of the service.  A media route provider service can only export its own media route providers.  Provider package name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".  Service package name: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v7/c/n;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 126
    :cond_0
    iput-object v0, p0, Landroid/support/v7/c/n;->g:Landroid/support/v7/c/f;

    .line 127
    iget-object v0, p0, Landroid/support/v7/c/n;->g:Landroid/support/v7/c/f;

    iget-object v1, p0, Landroid/support/v7/c/n;->f:Landroid/support/v7/c/r;

    invoke-static {}, Landroid/support/v7/c/v;->a()V

    iput-object v1, v0, Landroid/support/v7/c/f;->d:Landroid/support/v7/c/g;

    .line 130
    :cond_1
    iget-object v0, p0, Landroid/support/v7/c/n;->g:Landroid/support/v7/c/f;

    if-eqz v0, :cond_2

    .line 131
    iget-object v0, p0, Landroid/support/v7/c/n;->d:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 134
    :goto_0
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
