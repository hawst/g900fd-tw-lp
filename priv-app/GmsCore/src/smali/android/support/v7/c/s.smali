.class final Landroid/support/v7/c/s;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Landroid/support/v7/c/n;)V
    .locals 1

    .prologue
    .line 581
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 582
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Landroid/support/v7/c/s;->a:Ljava/lang/ref/WeakReference;

    .line 583
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 10

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x0

    .line 587
    iget-object v5, p1, Landroid/os/Message;->replyTo:Landroid/os/Messenger;

    .line 588
    invoke-static {v5}, Landroid/support/v7/c/m;->a(Landroid/os/Messenger;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 589
    iget v6, p1, Landroid/os/Message;->what:I

    .line 590
    iget v7, p1, Landroid/os/Message;->arg1:I

    .line 591
    iget v8, p1, Landroid/os/Message;->arg2:I

    .line 592
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    .line 593
    invoke-virtual {p1}, Landroid/os/Message;->peekData()Landroid/os/Bundle;

    move-result-object v9

    .line 594
    iget-object v0, p0, Landroid/support/v7/c/s;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/n;

    if-eqz v0, :cond_0

    packed-switch v6, :pswitch_data_0

    :cond_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_2

    .line 595
    invoke-static {}, Landroid/support/v7/c/n;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 596
    const-string v0, "MediaRouteProviderSrv"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v5}, Landroid/support/v7/c/n;->a(Landroid/os/Messenger;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ": Message failed, what="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", requestId="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", arg="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", obj="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", data="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    :cond_1
    invoke-static {v5, v7}, Landroid/support/v7/c/n;->a(Landroid/os/Messenger;I)V

    .line 607
    :cond_2
    :goto_1
    return-void

    .line 594
    :pswitch_0
    invoke-static {v0, v5, v7, v8}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/n;Landroid/os/Messenger;II)Z

    move-result v0

    goto :goto_0

    :pswitch_1
    invoke-static {v0, v5, v7}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/n;Landroid/os/Messenger;I)Z

    move-result v0

    goto :goto_0

    :pswitch_2
    const-string v3, "routeId"

    invoke-virtual {v9, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {v0, v5, v7, v8, v3}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/n;Landroid/os/Messenger;IILjava/lang/String;)Z

    move-result v0

    goto :goto_0

    :pswitch_3
    invoke-static {v0, v5, v7, v8}, Landroid/support/v7/c/n;->b(Landroid/support/v7/c/n;Landroid/os/Messenger;II)Z

    move-result v0

    goto :goto_0

    :pswitch_4
    invoke-static {v0, v5, v7, v8}, Landroid/support/v7/c/n;->c(Landroid/support/v7/c/n;Landroid/os/Messenger;II)Z

    move-result v0

    goto :goto_0

    :pswitch_5
    invoke-static {v0, v5, v7, v8}, Landroid/support/v7/c/n;->d(Landroid/support/v7/c/n;Landroid/os/Messenger;II)Z

    move-result v0

    goto :goto_0

    :pswitch_6
    const-string v3, "volume"

    const/4 v4, -0x1

    invoke-virtual {v9, v3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-ltz v3, :cond_0

    invoke-static {v0, v5, v7, v8, v3}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/n;Landroid/os/Messenger;III)Z

    move-result v0

    goto/16 :goto_0

    :pswitch_7
    const-string v3, "volume"

    invoke-virtual {v9, v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v0, v5, v7, v8, v3}, Landroid/support/v7/c/n;->b(Landroid/support/v7/c/n;Landroid/os/Messenger;III)Z

    move-result v0

    goto/16 :goto_0

    :pswitch_8
    instance-of v3, v2, Landroid/content/Intent;

    if-eqz v3, :cond_0

    move-object v1, v2

    check-cast v1, Landroid/content/Intent;

    invoke-static {v0, v5, v7, v8, v1}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/n;Landroid/os/Messenger;IILandroid/content/Intent;)Z

    move-result v0

    goto/16 :goto_0

    :pswitch_9
    if-eqz v2, :cond_3

    instance-of v4, v2, Landroid/os/Bundle;

    if-eqz v4, :cond_0

    :cond_3
    move-object v1, v2

    check-cast v1, Landroid/os/Bundle;

    if-eqz v1, :cond_4

    new-instance v4, Landroid/support/v7/c/e;

    invoke-direct {v4, v1}, Landroid/support/v7/c/e;-><init>(Landroid/os/Bundle;)V

    move-object v1, v4

    :goto_2
    if-eqz v1, :cond_5

    invoke-virtual {v1}, Landroid/support/v7/c/e;->c()Z

    move-result v4

    if-eqz v4, :cond_5

    :goto_3
    invoke-static {v0, v5, v7, v1}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/n;Landroid/os/Messenger;ILandroid/support/v7/c/e;)Z

    move-result v0

    goto/16 :goto_0

    :cond_4
    move-object v1, v3

    goto :goto_2

    :cond_5
    move-object v1, v3

    goto :goto_3

    .line 603
    :cond_6
    invoke-static {}, Landroid/support/v7/c/n;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 604
    const-string v0, "MediaRouteProviderSrv"

    const-string v1, "Ignoring message without valid reply messenger."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 594
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method
