.class final Landroid/support/v7/c/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field public final a:Landroid/os/Messenger;

.field public final b:I

.field public c:Landroid/support/v7/c/e;

.field final synthetic d:Landroid/support/v7/c/n;

.field private final e:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>(Landroid/support/v7/c/n;Landroid/os/Messenger;I)V
    .locals 1

    .prologue
    .line 489
    iput-object p1, p0, Landroid/support/v7/c/p;->d:Landroid/support/v7/c/n;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 486
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/p;->e:Landroid/util/SparseArray;

    .line 490
    iput-object p2, p0, Landroid/support/v7/c/p;->a:Landroid/os/Messenger;

    .line 491
    iput p3, p0, Landroid/support/v7/c/p;->b:I

    .line 492
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 496
    :try_start_0
    iget-object v1, p0, Landroid/support/v7/c/p;->a:Landroid/os/Messenger;

    invoke-virtual {v1}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 497
    const/4 v0, 0x1

    .line 501
    :goto_0
    return v0

    .line 499
    :catch_0
    move-exception v1

    invoke-virtual {p0}, Landroid/support/v7/c/p;->binderDied()V

    goto :goto_0
.end method

.method public final a(I)Z
    .locals 2

    .prologue
    .line 533
    iget-object v0, p0, Landroid/support/v7/c/p;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/j;

    .line 534
    if-eqz v0, :cond_0

    .line 535
    iget-object v1, p0, Landroid/support/v7/c/p;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 536
    invoke-virtual {v0}, Landroid/support/v7/c/j;->a()V

    .line 537
    const/4 v0, 0x1

    .line 539
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Messenger;)Z
    .locals 2

    .prologue
    .line 517
    iget-object v0, p0, Landroid/support/v7/c/p;->a:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/support/v7/c/e;)Z
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Landroid/support/v7/c/p;->c:Landroid/support/v7/c/e;

    if-eq v0, p1, :cond_1

    iget-object v0, p0, Landroid/support/v7/c/p;->c:Landroid/support/v7/c/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v7/c/p;->c:Landroid/support/v7/c/e;

    invoke-virtual {v0, p1}, Landroid/support/v7/c/e;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 549
    :cond_0
    iput-object p1, p0, Landroid/support/v7/c/p;->c:Landroid/support/v7/c/e;

    .line 550
    iget-object v0, p0, Landroid/support/v7/c/p;->d:Landroid/support/v7/c/n;

    invoke-static {v0}, Landroid/support/v7/c/n;->b(Landroid/support/v7/c/n;)Z

    move-result v0

    .line 552
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;I)Z
    .locals 2

    .prologue
    .line 521
    iget-object v0, p0, Landroid/support/v7/c/p;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-gez v0, :cond_0

    .line 522
    iget-object v0, p0, Landroid/support/v7/c/p;->d:Landroid/support/v7/c/n;

    invoke-static {v0}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/n;)Landroid/support/v7/c/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v7/c/f;->a(Ljava/lang/String;)Landroid/support/v7/c/j;

    move-result-object v0

    .line 524
    if-eqz v0, :cond_0

    .line 525
    iget-object v1, p0, Landroid/support/v7/c/p;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 526
    const/4 v0, 0x1

    .line 529
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(I)Landroid/support/v7/c/j;
    .locals 1

    .prologue
    .line 543
    iget-object v0, p0, Landroid/support/v7/c/p;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/j;

    return-object v0
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 505
    iget-object v0, p0, Landroid/support/v7/c/p;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v1, v2

    .line 506
    :goto_0
    if-ge v1, v3, :cond_0

    .line 507
    iget-object v0, p0, Landroid/support/v7/c/p;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v7/c/j;

    invoke-virtual {v0}, Landroid/support/v7/c/j;->a()V

    .line 506
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 509
    :cond_0
    iget-object v0, p0, Landroid/support/v7/c/p;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 511
    iget-object v0, p0, Landroid/support/v7/c/p;->a:Landroid/os/Messenger;

    invoke-virtual {v0}, Landroid/os/Messenger;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v0, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 513
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v7/c/p;->a(Landroid/support/v7/c/e;)Z

    .line 514
    return-void
.end method

.method public final binderDied()V
    .locals 3

    .prologue
    .line 558
    iget-object v0, p0, Landroid/support/v7/c/p;->d:Landroid/support/v7/c/n;

    invoke-static {v0}, Landroid/support/v7/c/n;->c(Landroid/support/v7/c/n;)Landroid/support/v7/c/q;

    move-result-object v0

    const/4 v1, 0x1

    iget-object v2, p0, Landroid/support/v7/c/p;->a:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Landroid/support/v7/c/q;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 559
    return-void
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 563
    iget-object v0, p0, Landroid/support/v7/c/p;->a:Landroid/os/Messenger;

    invoke-static {v0}, Landroid/support/v7/c/n;->a(Landroid/os/Messenger;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
