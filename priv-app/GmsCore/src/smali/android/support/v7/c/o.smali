.class final Landroid/support/v7/c/o;
.super Landroid/support/v7/c/w;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/support/v7/c/p;

.field final synthetic b:I

.field final synthetic c:Landroid/content/Intent;

.field final synthetic d:Landroid/os/Messenger;

.field final synthetic e:I

.field final synthetic f:Landroid/support/v7/c/n;


# direct methods
.method constructor <init>(Landroid/support/v7/c/n;Landroid/support/v7/c/p;ILandroid/content/Intent;Landroid/os/Messenger;I)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Landroid/support/v7/c/o;->f:Landroid/support/v7/c/n;

    iput-object p2, p0, Landroid/support/v7/c/o;->a:Landroid/support/v7/c/p;

    iput p3, p0, Landroid/support/v7/c/o;->b:I

    iput-object p4, p0, Landroid/support/v7/c/o;->c:Landroid/content/Intent;

    iput-object p5, p0, Landroid/support/v7/c/o;->d:Landroid/os/Messenger;

    iput p6, p0, Landroid/support/v7/c/o;->e:I

    invoke-direct {p0}, Landroid/support/v7/c/w;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 305
    invoke-static {}, Landroid/support/v7/c/n;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 306
    const-string v0, "MediaRouteProviderSrv"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Landroid/support/v7/c/o;->a:Landroid/support/v7/c/p;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Route control request succeeded, controllerId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/support/v7/c/o;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", intent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/c/o;->c:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", data="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    :cond_0
    iget-object v0, p0, Landroid/support/v7/c/o;->f:Landroid/support/v7/c/n;

    iget-object v1, p0, Landroid/support/v7/c/o;->d:Landroid/os/Messenger;

    invoke-static {v0, v1}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/n;Landroid/os/Messenger;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 312
    iget-object v0, p0, Landroid/support/v7/c/o;->d:Landroid/os/Messenger;

    const/4 v1, 0x3

    iget v2, p0, Landroid/support/v7/c/o;->e:I

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, p1, v3}, Landroid/support/v7/c/n;->a(Landroid/os/Messenger;IILjava/lang/Object;Landroid/os/Bundle;)V

    .line 315
    :cond_1
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    .line 319
    invoke-static {}, Landroid/support/v7/c/n;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    const-string v0, "MediaRouteProviderSrv"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Landroid/support/v7/c/o;->a:Landroid/support/v7/c/p;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": Route control request failed, controllerId="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/support/v7/c/o;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", intent="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v7/c/o;->c:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", error="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", data="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :cond_0
    iget-object v0, p0, Landroid/support/v7/c/o;->f:Landroid/support/v7/c/n;

    iget-object v1, p0, Landroid/support/v7/c/o;->d:Landroid/os/Messenger;

    invoke-static {v0, v1}, Landroid/support/v7/c/n;->a(Landroid/support/v7/c/n;Landroid/os/Messenger;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 326
    if-eqz p1, :cond_2

    .line 327
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 328
    const-string v1, "error"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    iget-object v1, p0, Landroid/support/v7/c/o;->d:Landroid/os/Messenger;

    iget v2, p0, Landroid/support/v7/c/o;->e:I

    invoke-static {v1, v3, v2, p2, v0}, Landroid/support/v7/c/n;->a(Landroid/os/Messenger;IILjava/lang/Object;Landroid/os/Bundle;)V

    .line 336
    :cond_1
    :goto_0
    return-void

    .line 332
    :cond_2
    iget-object v0, p0, Landroid/support/v7/c/o;->d:Landroid/os/Messenger;

    iget v1, p0, Landroid/support/v7/c/o;->e:I

    const/4 v2, 0x0

    invoke-static {v0, v3, v1, p2, v2}, Landroid/support/v7/c/n;->a(Landroid/os/Messenger;IILjava/lang/Object;Landroid/os/Bundle;)V

    goto :goto_0
.end method
