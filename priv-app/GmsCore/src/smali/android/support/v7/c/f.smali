.class public abstract Landroid/support/v7/c/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field final b:Landroid/support/v7/c/i;

.field public final c:Landroid/support/v7/c/h;

.field d:Landroid/support/v7/c/g;

.field public e:Landroid/support/v7/c/e;

.field f:Z

.field public g:Landroid/support/v7/c/k;

.field public h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 79
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v7/c/f;-><init>(Landroid/content/Context;B)V

    .line 80
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;B)V
    .locals 3

    .prologue
    .line 82
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Landroid/support/v7/c/h;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v7/c/h;-><init>(Landroid/support/v7/c/f;B)V

    iput-object v0, p0, Landroid/support/v7/c/f;->c:Landroid/support/v7/c/h;

    .line 83
    if-nez p1, :cond_0

    .line 84
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "context must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_0
    iput-object p1, p0, Landroid/support/v7/c/f;->a:Landroid/content/Context;

    .line 88
    new-instance v0, Landroid/support/v7/c/i;

    new-instance v1, Landroid/content/ComponentName;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {v0, v1}, Landroid/support/v7/c/i;-><init>(Landroid/content/ComponentName;)V

    iput-object v0, p0, Landroid/support/v7/c/f;->b:Landroid/support/v7/c/i;

    .line 91
    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;)Landroid/support/v7/c/j;
    .locals 1

    .prologue
    .line 254
    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/support/v7/c/e;)V
    .locals 0

    .prologue
    .line 191
    return-void
.end method
