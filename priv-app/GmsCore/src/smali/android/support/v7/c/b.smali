.class public final Landroid/support/v7/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 322
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 323
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    .line 324
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Landroid/support/v7/c/b;->a(J)Landroid/support/v7/c/b;

    .line 325
    iget-object v0, p0, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v1, "playbackState"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 326
    return-void
.end method


# virtual methods
.method public final a()Landroid/support/v7/c/a;
    .locals 3

    .prologue
    .line 389
    new-instance v0, Landroid/support/v7/c/a;

    iget-object v1, p0, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/support/v7/c/a;-><init>(Landroid/os/Bundle;B)V

    return-object v0
.end method

.method public final a(J)Landroid/support/v7/c/b;
    .locals 3

    .prologue
    .line 345
    iget-object v0, p0, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v1, "timestamp"

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 346
    return-object p0
.end method

.method public final a(Landroid/os/Bundle;)Landroid/support/v7/c/b;
    .locals 2

    .prologue
    .line 381
    iget-object v0, p0, Landroid/support/v7/c/b;->a:Landroid/os/Bundle;

    const-string v1, "extras"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 382
    return-object p0
.end method
