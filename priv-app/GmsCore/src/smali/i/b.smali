.class public final Li/b;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1756
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1757
    const-string v0, ""

    iput-object v0, p0, Li/b;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/b;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/b;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/b;->d:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Li/b;->cachedSize:I

    .line 1758
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1839
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1840
    iget-object v1, p0, Li/b;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1841
    const/4 v1, 0x1

    iget-object v2, p0, Li/b;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1844
    :cond_0
    const/4 v1, 0x2

    iget-object v2, p0, Li/b;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1846
    const/4 v1, 0x3

    iget-object v2, p0, Li/b;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1848
    iget-object v1, p0, Li/b;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1849
    const/4 v1, 0x4

    iget-object v2, p0, Li/b;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1852
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1771
    if-ne p1, p0, :cond_1

    .line 1806
    :cond_0
    :goto_0
    return v0

    .line 1774
    :cond_1
    instance-of v2, p1, Li/b;

    if-nez v2, :cond_2

    move v0, v1

    .line 1775
    goto :goto_0

    .line 1777
    :cond_2
    check-cast p1, Li/b;

    .line 1778
    iget-object v2, p0, Li/b;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 1779
    iget-object v2, p1, Li/b;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1780
    goto :goto_0

    .line 1782
    :cond_3
    iget-object v2, p0, Li/b;->a:Ljava/lang/String;

    iget-object v3, p1, Li/b;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1783
    goto :goto_0

    .line 1785
    :cond_4
    iget-object v2, p0, Li/b;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1786
    iget-object v2, p1, Li/b;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 1787
    goto :goto_0

    .line 1789
    :cond_5
    iget-object v2, p0, Li/b;->b:Ljava/lang/String;

    iget-object v3, p1, Li/b;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1790
    goto :goto_0

    .line 1792
    :cond_6
    iget-object v2, p0, Li/b;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1793
    iget-object v2, p1, Li/b;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 1794
    goto :goto_0

    .line 1796
    :cond_7
    iget-object v2, p0, Li/b;->c:Ljava/lang/String;

    iget-object v3, p1, Li/b;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1797
    goto :goto_0

    .line 1799
    :cond_8
    iget-object v2, p0, Li/b;->d:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 1800
    iget-object v2, p1, Li/b;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1801
    goto :goto_0

    .line 1803
    :cond_9
    iget-object v2, p0, Li/b;->d:Ljava/lang/String;

    iget-object v3, p1, Li/b;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1804
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1811
    iget-object v0, p0, Li/b;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1814
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/b;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1816
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/b;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1818
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Li/b;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 1820
    return v0

    .line 1811
    :cond_0
    iget-object v0, p0, Li/b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1814
    :cond_1
    iget-object v0, p0, Li/b;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1816
    :cond_2
    iget-object v0, p0, Li/b;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1818
    :cond_3
    iget-object v1, p0, Li/b;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1727
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/b;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/b;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/b;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/b;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1826
    iget-object v0, p0, Li/b;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1827
    const/4 v0, 0x1

    iget-object v1, p0, Li/b;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1829
    :cond_0
    const/4 v0, 0x2

    iget-object v1, p0, Li/b;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1830
    const/4 v0, 0x3

    iget-object v1, p0, Li/b;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1831
    iget-object v0, p0, Li/b;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1832
    const/4 v0, 0x4

    iget-object v1, p0, Li/b;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1834
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1835
    return-void
.end method
