.class public final Li/p;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2551
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2552
    iput v1, p0, Li/p;->a:I

    const-string v0, ""

    iput-object v0, p0, Li/p;->b:Ljava/lang/String;

    iput v1, p0, Li/p;->c:I

    iput v1, p0, Li/p;->d:I

    iput v1, p0, Li/p;->e:I

    const/4 v0, -0x1

    iput v0, p0, Li/p;->cachedSize:I

    .line 2553
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2629
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2630
    const/4 v1, 0x1

    iget v2, p0, Li/p;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2632
    iget-object v1, p0, Li/p;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2633
    const/4 v1, 0x2

    iget-object v2, p0, Li/p;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2636
    :cond_0
    iget v1, p0, Li/p;->c:I

    if-eqz v1, :cond_1

    .line 2637
    const/4 v1, 0x3

    iget v2, p0, Li/p;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2640
    :cond_1
    iget v1, p0, Li/p;->d:I

    if-eqz v1, :cond_2

    .line 2641
    const/4 v1, 0x4

    iget v2, p0, Li/p;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2644
    :cond_2
    iget v1, p0, Li/p;->e:I

    if-eqz v1, :cond_3

    .line 2645
    const/4 v1, 0x5

    iget v2, p0, Li/p;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2648
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2567
    if-ne p1, p0, :cond_1

    .line 2593
    :cond_0
    :goto_0
    return v0

    .line 2570
    :cond_1
    instance-of v2, p1, Li/p;

    if-nez v2, :cond_2

    move v0, v1

    .line 2571
    goto :goto_0

    .line 2573
    :cond_2
    check-cast p1, Li/p;

    .line 2574
    iget v2, p0, Li/p;->a:I

    iget v3, p1, Li/p;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 2575
    goto :goto_0

    .line 2577
    :cond_3
    iget-object v2, p0, Li/p;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2578
    iget-object v2, p1, Li/p;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 2579
    goto :goto_0

    .line 2581
    :cond_4
    iget-object v2, p0, Li/p;->b:Ljava/lang/String;

    iget-object v3, p1, Li/p;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 2582
    goto :goto_0

    .line 2584
    :cond_5
    iget v2, p0, Li/p;->c:I

    iget v3, p1, Li/p;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 2585
    goto :goto_0

    .line 2587
    :cond_6
    iget v2, p0, Li/p;->d:I

    iget v3, p1, Li/p;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2588
    goto :goto_0

    .line 2590
    :cond_7
    iget v2, p0, Li/p;->e:I

    iget v3, p1, Li/p;->e:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2591
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2598
    iget v0, p0, Li/p;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 2600
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Li/p;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 2602
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Li/p;->c:I

    add-int/2addr v0, v1

    .line 2603
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Li/p;->d:I

    add-int/2addr v0, v1

    .line 2604
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Li/p;->e:I

    add-int/2addr v0, v1

    .line 2605
    return v0

    .line 2600
    :cond_0
    iget-object v0, p0, Li/p;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2519
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Li/p;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/p;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Li/p;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Li/p;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Li/p;->e:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2611
    const/4 v0, 0x1

    iget v1, p0, Li/p;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2612
    iget-object v0, p0, Li/p;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2613
    const/4 v0, 0x2

    iget-object v1, p0, Li/p;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2615
    :cond_0
    iget v0, p0, Li/p;->c:I

    if-eqz v0, :cond_1

    .line 2616
    const/4 v0, 0x3

    iget v1, p0, Li/p;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2618
    :cond_1
    iget v0, p0, Li/p;->d:I

    if-eqz v0, :cond_2

    .line 2619
    const/4 v0, 0x4

    iget v1, p0, Li/p;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2621
    :cond_2
    iget v0, p0, Li/p;->e:I

    if-eqz v0, :cond_3

    .line 2622
    const/4 v0, 0x5

    iget v1, p0, Li/p;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2624
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2625
    return-void
.end method
