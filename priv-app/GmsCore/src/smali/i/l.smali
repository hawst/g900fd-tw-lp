.class public final Li/l;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Li/l;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2981
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2982
    const-string v0, ""

    iput-object v0, p0, Li/l;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/l;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Li/l;->c:[B

    const/4 v0, -0x1

    iput v0, p0, Li/l;->cachedSize:I

    .line 2983
    return-void
.end method

.method public static a()[Li/l;
    .locals 2

    .prologue
    .line 2961
    sget-object v0, Li/l;->d:[Li/l;

    if-nez v0, :cond_1

    .line 2962
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2964
    :try_start_0
    sget-object v0, Li/l;->d:[Li/l;

    if-nez v0, :cond_0

    .line 2965
    const/4 v0, 0x0

    new-array v0, v0, [Li/l;

    sput-object v0, Li/l;->d:[Li/l;

    .line 2967
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2969
    :cond_1
    sget-object v0, Li/l;->d:[Li/l;

    return-object v0

    .line 2967
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3048
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3049
    const/4 v1, 0x1

    iget-object v2, p0, Li/l;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3051
    iget-object v1, p0, Li/l;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3052
    const/4 v1, 0x2

    iget-object v2, p0, Li/l;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3055
    :cond_0
    iget-object v1, p0, Li/l;->c:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3056
    const/4 v1, 0x3

    iget-object v2, p0, Li/l;->c:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 3059
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2995
    if-ne p1, p0, :cond_1

    .line 3019
    :cond_0
    :goto_0
    return v0

    .line 2998
    :cond_1
    instance-of v2, p1, Li/l;

    if-nez v2, :cond_2

    move v0, v1

    .line 2999
    goto :goto_0

    .line 3001
    :cond_2
    check-cast p1, Li/l;

    .line 3002
    iget-object v2, p0, Li/l;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 3003
    iget-object v2, p1, Li/l;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 3004
    goto :goto_0

    .line 3006
    :cond_3
    iget-object v2, p0, Li/l;->a:Ljava/lang/String;

    iget-object v3, p1, Li/l;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 3007
    goto :goto_0

    .line 3009
    :cond_4
    iget-object v2, p0, Li/l;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 3010
    iget-object v2, p1, Li/l;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 3011
    goto :goto_0

    .line 3013
    :cond_5
    iget-object v2, p0, Li/l;->b:Ljava/lang/String;

    iget-object v3, p1, Li/l;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 3014
    goto :goto_0

    .line 3016
    :cond_6
    iget-object v2, p0, Li/l;->c:[B

    iget-object v3, p1, Li/l;->c:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 3017
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3024
    iget-object v0, p0, Li/l;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3027
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Li/l;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 3029
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Li/l;->c:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    .line 3030
    return v0

    .line 3024
    :cond_0
    iget-object v0, p0, Li/l;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3027
    :cond_1
    iget-object v1, p0, Li/l;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2955
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/l;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/l;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Li/l;->c:[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3036
    const/4 v0, 0x1

    iget-object v1, p0, Li/l;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3037
    iget-object v0, p0, Li/l;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3038
    const/4 v0, 0x2

    iget-object v1, p0, Li/l;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3040
    :cond_0
    iget-object v0, p0, Li/l;->c:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3041
    const/4 v0, 0x3

    iget-object v1, p0, Li/l;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 3043
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3044
    return-void
.end method
