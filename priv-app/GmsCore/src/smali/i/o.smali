.class public final Li/o;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[Ljava/lang/String;

.field public e:[Ljava/lang/String;

.field public f:Li/p;

.field public g:I

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 638
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 639
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Li/o;->a:J

    const-string v0, ""

    iput-object v0, p0, Li/o;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/o;->c:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Li/o;->d:[Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Li/o;->e:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Li/o;->f:Li/p;

    const/4 v0, 0x0

    iput v0, p0, Li/o;->g:I

    const-string v0, ""

    iput-object v0, p0, Li/o;->h:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Li/o;->cachedSize:I

    .line 640
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 772
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 773
    const/4 v1, 0x1

    iget-wide v4, p0, Li/o;->a:J

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 775
    iget-object v1, p0, Li/o;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 776
    const/4 v1, 0x2

    iget-object v3, p0, Li/o;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 779
    :cond_0
    iget-object v1, p0, Li/o;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 780
    const/4 v1, 0x3

    iget-object v3, p0, Li/o;->c:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 783
    :cond_1
    iget-object v1, p0, Li/o;->d:[Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Li/o;->d:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_4

    move v1, v2

    move v3, v2

    move v4, v2

    .line 786
    :goto_0
    iget-object v5, p0, Li/o;->d:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_3

    .line 787
    iget-object v5, p0, Li/o;->d:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 788
    if-eqz v5, :cond_2

    .line 789
    add-int/lit8 v4, v4, 0x1

    .line 790
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 786
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 794
    :cond_3
    add-int/2addr v0, v3

    .line 795
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 797
    :cond_4
    iget-object v1, p0, Li/o;->e:[Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Li/o;->e:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_7

    move v1, v2

    move v3, v2

    .line 800
    :goto_1
    iget-object v4, p0, Li/o;->e:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_6

    .line 801
    iget-object v4, p0, Li/o;->e:[Ljava/lang/String;

    aget-object v4, v4, v2

    .line 802
    if-eqz v4, :cond_5

    .line 803
    add-int/lit8 v3, v3, 0x1

    .line 804
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 800
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 808
    :cond_6
    add-int/2addr v0, v1

    .line 809
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 811
    :cond_7
    iget-object v1, p0, Li/o;->f:Li/p;

    if-eqz v1, :cond_8

    .line 812
    const/4 v1, 0x6

    iget-object v2, p0, Li/o;->f:Li/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 815
    :cond_8
    iget v1, p0, Li/o;->g:I

    if-eqz v1, :cond_9

    .line 816
    const/4 v1, 0x7

    iget v2, p0, Li/o;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 819
    :cond_9
    iget-object v1, p0, Li/o;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 820
    const/16 v1, 0x8

    iget-object v2, p0, Li/o;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 823
    :cond_a
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 657
    if-ne p1, p0, :cond_1

    .line 708
    :cond_0
    :goto_0
    return v0

    .line 660
    :cond_1
    instance-of v2, p1, Li/o;

    if-nez v2, :cond_2

    move v0, v1

    .line 661
    goto :goto_0

    .line 663
    :cond_2
    check-cast p1, Li/o;

    .line 664
    iget-wide v2, p0, Li/o;->a:J

    iget-wide v4, p1, Li/o;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 665
    goto :goto_0

    .line 667
    :cond_3
    iget-object v2, p0, Li/o;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 668
    iget-object v2, p1, Li/o;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 669
    goto :goto_0

    .line 671
    :cond_4
    iget-object v2, p0, Li/o;->b:Ljava/lang/String;

    iget-object v3, p1, Li/o;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 672
    goto :goto_0

    .line 674
    :cond_5
    iget-object v2, p0, Li/o;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 675
    iget-object v2, p1, Li/o;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 676
    goto :goto_0

    .line 678
    :cond_6
    iget-object v2, p0, Li/o;->c:Ljava/lang/String;

    iget-object v3, p1, Li/o;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 679
    goto :goto_0

    .line 681
    :cond_7
    iget-object v2, p0, Li/o;->d:[Ljava/lang/String;

    iget-object v3, p1, Li/o;->d:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 683
    goto :goto_0

    .line 685
    :cond_8
    iget-object v2, p0, Li/o;->e:[Ljava/lang/String;

    iget-object v3, p1, Li/o;->e:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 687
    goto :goto_0

    .line 689
    :cond_9
    iget-object v2, p0, Li/o;->f:Li/p;

    if-nez v2, :cond_a

    .line 690
    iget-object v2, p1, Li/o;->f:Li/p;

    if-eqz v2, :cond_b

    move v0, v1

    .line 691
    goto :goto_0

    .line 694
    :cond_a
    iget-object v2, p0, Li/o;->f:Li/p;

    iget-object v3, p1, Li/o;->f:Li/p;

    invoke-virtual {v2, v3}, Li/p;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 695
    goto :goto_0

    .line 698
    :cond_b
    iget v2, p0, Li/o;->g:I

    iget v3, p1, Li/o;->g:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 699
    goto :goto_0

    .line 701
    :cond_c
    iget-object v2, p0, Li/o;->h:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 702
    iget-object v2, p1, Li/o;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 703
    goto :goto_0

    .line 705
    :cond_d
    iget-object v2, p0, Li/o;->h:Ljava/lang/String;

    iget-object v3, p1, Li/o;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 706
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 713
    iget-wide v2, p0, Li/o;->a:J

    iget-wide v4, p0, Li/o;->a:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit16 v0, v0, 0x20f

    .line 716
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/o;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 718
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/o;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 720
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Li/o;->d:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 722
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Li/o;->e:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 724
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/o;->f:Li/p;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 726
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Li/o;->g:I

    add-int/2addr v0, v2

    .line 727
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Li/o;->h:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 729
    return v0

    .line 716
    :cond_0
    iget-object v0, p0, Li/o;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 718
    :cond_1
    iget-object v0, p0, Li/o;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 724
    :cond_2
    iget-object v0, p0, Li/o;->f:Li/p;

    invoke-virtual {v0}, Li/p;->hashCode()I

    move-result v0

    goto :goto_2

    .line 727
    :cond_3
    iget-object v1, p0, Li/o;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 597
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v2

    iput-wide v2, p0, Li/o;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/o;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/o;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Li/o;->d:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Li/o;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Li/o;->d:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Li/o;->d:[Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Li/o;->e:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Li/o;->e:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Li/o;->e:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Li/o;->e:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Li/o;->f:Li/p;

    if-nez v0, :cond_7

    new-instance v0, Li/p;

    invoke-direct {v0}, Li/p;-><init>()V

    iput-object v0, p0, Li/o;->f:Li/p;

    :cond_7
    iget-object v0, p0, Li/o;->f:Li/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Li/o;->g:I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/o;->h:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 735
    const/4 v0, 0x1

    iget-wide v2, p0, Li/o;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 736
    iget-object v0, p0, Li/o;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 737
    const/4 v0, 0x2

    iget-object v2, p0, Li/o;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 739
    :cond_0
    iget-object v0, p0, Li/o;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 740
    const/4 v0, 0x3

    iget-object v2, p0, Li/o;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 742
    :cond_1
    iget-object v0, p0, Li/o;->d:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Li/o;->d:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 743
    :goto_0
    iget-object v2, p0, Li/o;->d:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 744
    iget-object v2, p0, Li/o;->d:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 745
    if-eqz v2, :cond_2

    .line 746
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 743
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 750
    :cond_3
    iget-object v0, p0, Li/o;->e:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Li/o;->e:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 751
    :goto_1
    iget-object v0, p0, Li/o;->e:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 752
    iget-object v0, p0, Li/o;->e:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 753
    if-eqz v0, :cond_4

    .line 754
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 751
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 758
    :cond_5
    iget-object v0, p0, Li/o;->f:Li/p;

    if-eqz v0, :cond_6

    .line 759
    const/4 v0, 0x6

    iget-object v1, p0, Li/o;->f:Li/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 761
    :cond_6
    iget v0, p0, Li/o;->g:I

    if-eqz v0, :cond_7

    .line 762
    const/4 v0, 0x7

    iget v1, p0, Li/o;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 764
    :cond_7
    iget-object v0, p0, Li/o;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 765
    const/16 v0, 0x8

    iget-object v1, p0, Li/o;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 767
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 768
    return-void
.end method
