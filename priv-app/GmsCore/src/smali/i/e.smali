.class public final Li/e;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[Li/l;

.field public e:[Li/m;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2734
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2735
    const-string v0, ""

    iput-object v0, p0, Li/e;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/e;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/e;->c:Ljava/lang/String;

    invoke-static {}, Li/l;->a()[Li/l;

    move-result-object v0

    iput-object v0, p0, Li/e;->d:[Li/l;

    invoke-static {}, Li/m;->a()[Li/m;

    move-result-object v0

    iput-object v0, p0, Li/e;->e:[Li/m;

    const/4 v0, -0x1

    iput v0, p0, Li/e;->cachedSize:I

    .line 2736
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2838
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2839
    iget-object v2, p0, Li/e;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2840
    const/4 v2, 0x2

    iget-object v3, p0, Li/e;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2843
    :cond_0
    iget-object v2, p0, Li/e;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2844
    const/4 v2, 0x3

    iget-object v3, p0, Li/e;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2847
    :cond_1
    iget-object v2, p0, Li/e;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 2848
    const/4 v2, 0x6

    iget-object v3, p0, Li/e;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2851
    :cond_2
    iget-object v2, p0, Li/e;->d:[Li/l;

    if-eqz v2, :cond_5

    iget-object v2, p0, Li/e;->d:[Li/l;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 2852
    :goto_0
    iget-object v3, p0, Li/e;->d:[Li/l;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 2853
    iget-object v3, p0, Li/e;->d:[Li/l;

    aget-object v3, v3, v0

    .line 2854
    if-eqz v3, :cond_3

    .line 2855
    const/16 v4, 0x9

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2852
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 2860
    :cond_5
    iget-object v2, p0, Li/e;->e:[Li/m;

    if-eqz v2, :cond_7

    iget-object v2, p0, Li/e;->e:[Li/m;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 2861
    :goto_1
    iget-object v2, p0, Li/e;->e:[Li/m;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 2862
    iget-object v2, p0, Li/e;->e:[Li/m;

    aget-object v2, v2, v1

    .line 2863
    if-eqz v2, :cond_6

    .line 2864
    const/16 v3, 0xb

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2861
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2869
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2750
    if-ne p1, p0, :cond_1

    .line 2786
    :cond_0
    :goto_0
    return v0

    .line 2753
    :cond_1
    instance-of v2, p1, Li/e;

    if-nez v2, :cond_2

    move v0, v1

    .line 2754
    goto :goto_0

    .line 2756
    :cond_2
    check-cast p1, Li/e;

    .line 2757
    iget-object v2, p0, Li/e;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 2758
    iget-object v2, p1, Li/e;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2759
    goto :goto_0

    .line 2761
    :cond_3
    iget-object v2, p0, Li/e;->a:Ljava/lang/String;

    iget-object v3, p1, Li/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2762
    goto :goto_0

    .line 2764
    :cond_4
    iget-object v2, p0, Li/e;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 2765
    iget-object v2, p1, Li/e;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 2766
    goto :goto_0

    .line 2768
    :cond_5
    iget-object v2, p0, Li/e;->b:Ljava/lang/String;

    iget-object v3, p1, Li/e;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 2769
    goto :goto_0

    .line 2771
    :cond_6
    iget-object v2, p0, Li/e;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2772
    iget-object v2, p1, Li/e;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 2773
    goto :goto_0

    .line 2775
    :cond_7
    iget-object v2, p0, Li/e;->c:Ljava/lang/String;

    iget-object v3, p1, Li/e;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 2776
    goto :goto_0

    .line 2778
    :cond_8
    iget-object v2, p0, Li/e;->d:[Li/l;

    iget-object v3, p1, Li/e;->d:[Li/l;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 2780
    goto :goto_0

    .line 2782
    :cond_9
    iget-object v2, p0, Li/e;->e:[Li/m;

    iget-object v3, p1, Li/e;->e:[Li/m;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2784
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2791
    iget-object v0, p0, Li/e;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2794
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/e;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2796
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Li/e;->c:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 2798
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Li/e;->d:[Li/l;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2800
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Li/e;->e:[Li/m;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2802
    return v0

    .line 2791
    :cond_0
    iget-object v0, p0, Li/e;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2794
    :cond_1
    iget-object v0, p0, Li/e;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2796
    :cond_2
    iget-object v1, p0, Li/e;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2702
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/e;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/e;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/e;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x4a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Li/e;->d:[Li/l;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Li/l;

    if-eqz v0, :cond_1

    iget-object v3, p0, Li/e;->d:[Li/l;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Li/l;

    invoke-direct {v3}, Li/l;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Li/e;->d:[Li/l;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Li/l;

    invoke-direct {v3}, Li/l;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Li/e;->d:[Li/l;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Li/e;->e:[Li/m;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Li/m;

    if-eqz v0, :cond_4

    iget-object v3, p0, Li/e;->e:[Li/m;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Li/m;

    invoke-direct {v3}, Li/m;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Li/e;->e:[Li/m;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Li/m;

    invoke-direct {v3}, Li/m;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Li/e;->e:[Li/m;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x32 -> :sswitch_3
        0x4a -> :sswitch_4
        0x5a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2808
    iget-object v0, p0, Li/e;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2809
    const/4 v0, 0x2

    iget-object v2, p0, Li/e;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2811
    :cond_0
    iget-object v0, p0, Li/e;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2812
    const/4 v0, 0x3

    iget-object v2, p0, Li/e;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2814
    :cond_1
    iget-object v0, p0, Li/e;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2815
    const/4 v0, 0x6

    iget-object v2, p0, Li/e;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2817
    :cond_2
    iget-object v0, p0, Li/e;->d:[Li/l;

    if-eqz v0, :cond_4

    iget-object v0, p0, Li/e;->d:[Li/l;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 2818
    :goto_0
    iget-object v2, p0, Li/e;->d:[Li/l;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 2819
    iget-object v2, p0, Li/e;->d:[Li/l;

    aget-object v2, v2, v0

    .line 2820
    if-eqz v2, :cond_3

    .line 2821
    const/16 v3, 0x9

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2818
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2825
    :cond_4
    iget-object v0, p0, Li/e;->e:[Li/m;

    if-eqz v0, :cond_6

    iget-object v0, p0, Li/e;->e:[Li/m;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 2826
    :goto_1
    iget-object v0, p0, Li/e;->e:[Li/m;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 2827
    iget-object v0, p0, Li/e;->e:[Li/m;

    aget-object v0, v0, v1

    .line 2828
    if-eqz v0, :cond_5

    .line 2829
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2826
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2833
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2834
    return-void
.end method
