.class public final Li/f;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1539
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1540
    const-string v0, ""

    iput-object v0, p0, Li/f;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/f;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/f;->c:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Li/f;->d:I

    const-string v0, ""

    iput-object v0, p0, Li/f;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/f;->f:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/f;->g:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Li/f;->cachedSize:I

    .line 1541
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1648
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1649
    const/4 v1, 0x1

    iget-object v2, p0, Li/f;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1651
    iget-object v1, p0, Li/f;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1652
    const/4 v1, 0x2

    iget-object v2, p0, Li/f;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1655
    :cond_0
    const/4 v1, 0x3

    iget-object v2, p0, Li/f;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1657
    const/4 v1, 0x4

    iget v2, p0, Li/f;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1659
    const/4 v1, 0x5

    iget-object v2, p0, Li/f;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1661
    const/4 v1, 0x6

    iget-object v2, p0, Li/f;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1663
    const/4 v1, 0x7

    iget-object v2, p0, Li/f;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1665
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1557
    if-ne p1, p0, :cond_1

    .line 1609
    :cond_0
    :goto_0
    return v0

    .line 1560
    :cond_1
    instance-of v2, p1, Li/f;

    if-nez v2, :cond_2

    move v0, v1

    .line 1561
    goto :goto_0

    .line 1563
    :cond_2
    check-cast p1, Li/f;

    .line 1564
    iget-object v2, p0, Li/f;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 1565
    iget-object v2, p1, Li/f;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1566
    goto :goto_0

    .line 1568
    :cond_3
    iget-object v2, p0, Li/f;->a:Ljava/lang/String;

    iget-object v3, p1, Li/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1569
    goto :goto_0

    .line 1571
    :cond_4
    iget-object v2, p0, Li/f;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1572
    iget-object v2, p1, Li/f;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 1573
    goto :goto_0

    .line 1575
    :cond_5
    iget-object v2, p0, Li/f;->b:Ljava/lang/String;

    iget-object v3, p1, Li/f;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1576
    goto :goto_0

    .line 1578
    :cond_6
    iget-object v2, p0, Li/f;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1579
    iget-object v2, p1, Li/f;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 1580
    goto :goto_0

    .line 1582
    :cond_7
    iget-object v2, p0, Li/f;->c:Ljava/lang/String;

    iget-object v3, p1, Li/f;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1583
    goto :goto_0

    .line 1585
    :cond_8
    iget v2, p0, Li/f;->d:I

    iget v3, p1, Li/f;->d:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1586
    goto :goto_0

    .line 1588
    :cond_9
    iget-object v2, p0, Li/f;->e:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 1589
    iget-object v2, p1, Li/f;->e:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 1590
    goto :goto_0

    .line 1592
    :cond_a
    iget-object v2, p0, Li/f;->e:Ljava/lang/String;

    iget-object v3, p1, Li/f;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1593
    goto :goto_0

    .line 1595
    :cond_b
    iget-object v2, p0, Li/f;->f:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 1596
    iget-object v2, p1, Li/f;->f:Ljava/lang/String;

    if-eqz v2, :cond_d

    move v0, v1

    .line 1597
    goto :goto_0

    .line 1599
    :cond_c
    iget-object v2, p0, Li/f;->f:Ljava/lang/String;

    iget-object v3, p1, Li/f;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 1600
    goto :goto_0

    .line 1602
    :cond_d
    iget-object v2, p0, Li/f;->g:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 1603
    iget-object v2, p1, Li/f;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1604
    goto/16 :goto_0

    .line 1606
    :cond_e
    iget-object v2, p0, Li/f;->g:Ljava/lang/String;

    iget-object v3, p1, Li/f;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1607
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1614
    iget-object v0, p0, Li/f;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1617
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/f;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1619
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/f;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1621
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Li/f;->d:I

    add-int/2addr v0, v2

    .line 1622
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/f;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 1624
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/f;->f:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 1626
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Li/f;->g:Ljava/lang/String;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 1628
    return v0

    .line 1614
    :cond_0
    iget-object v0, p0, Li/f;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1617
    :cond_1
    iget-object v0, p0, Li/f;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1619
    :cond_2
    iget-object v0, p0, Li/f;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1622
    :cond_3
    iget-object v0, p0, Li/f;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 1624
    :cond_4
    iget-object v0, p0, Li/f;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 1626
    :cond_5
    iget-object v1, p0, Li/f;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1501
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/f;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/f;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/f;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Li/f;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/f;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/f;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/f;->g:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1634
    const/4 v0, 0x1

    iget-object v1, p0, Li/f;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1635
    iget-object v0, p0, Li/f;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1636
    const/4 v0, 0x2

    iget-object v1, p0, Li/f;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1638
    :cond_0
    const/4 v0, 0x3

    iget-object v1, p0, Li/f;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1639
    const/4 v0, 0x4

    iget v1, p0, Li/f;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1640
    const/4 v0, 0x5

    iget-object v1, p0, Li/f;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1641
    const/4 v0, 0x6

    iget-object v1, p0, Li/f;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1642
    const/4 v0, 0x7

    iget-object v1, p0, Li/f;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1643
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1644
    return-void
.end method
