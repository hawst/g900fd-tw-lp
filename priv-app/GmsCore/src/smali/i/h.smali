.class public final Li/h;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Li/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3262
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3263
    const-string v0, ""

    iput-object v0, p0, Li/h;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/h;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Li/h;->c:Li/g;

    const/4 v0, -0x1

    iput v0, p0, Li/h;->cachedSize:I

    .line 3264
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3334
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3335
    const/4 v1, 0x1

    iget-object v2, p0, Li/h;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3337
    const/4 v1, 0x2

    iget-object v2, p0, Li/h;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3339
    iget-object v1, p0, Li/h;->c:Li/g;

    if-eqz v1, :cond_0

    .line 3340
    const/4 v1, 0x3

    iget-object v2, p0, Li/h;->c:Li/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3343
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3276
    if-ne p1, p0, :cond_1

    .line 3306
    :cond_0
    :goto_0
    return v0

    .line 3279
    :cond_1
    instance-of v2, p1, Li/h;

    if-nez v2, :cond_2

    move v0, v1

    .line 3280
    goto :goto_0

    .line 3282
    :cond_2
    check-cast p1, Li/h;

    .line 3283
    iget-object v2, p0, Li/h;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 3284
    iget-object v2, p1, Li/h;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 3285
    goto :goto_0

    .line 3287
    :cond_3
    iget-object v2, p0, Li/h;->a:Ljava/lang/String;

    iget-object v3, p1, Li/h;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 3288
    goto :goto_0

    .line 3290
    :cond_4
    iget-object v2, p0, Li/h;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 3291
    iget-object v2, p1, Li/h;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 3292
    goto :goto_0

    .line 3294
    :cond_5
    iget-object v2, p0, Li/h;->b:Ljava/lang/String;

    iget-object v3, p1, Li/h;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 3295
    goto :goto_0

    .line 3297
    :cond_6
    iget-object v2, p0, Li/h;->c:Li/g;

    if-nez v2, :cond_7

    .line 3298
    iget-object v2, p1, Li/h;->c:Li/g;

    if-eqz v2, :cond_0

    move v0, v1

    .line 3299
    goto :goto_0

    .line 3302
    :cond_7
    iget-object v2, p0, Li/h;->c:Li/g;

    iget-object v3, p1, Li/h;->c:Li/g;

    invoke-virtual {v2, v3}, Li/g;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 3303
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3311
    iget-object v0, p0, Li/h;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3314
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/h;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3316
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Li/h;->c:Li/g;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 3318
    return v0

    .line 3311
    :cond_0
    iget-object v0, p0, Li/h;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3314
    :cond_1
    iget-object v0, p0, Li/h;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3316
    :cond_2
    iget-object v1, p0, Li/h;->c:Li/g;

    invoke-virtual {v1}, Li/g;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3236
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/h;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/h;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Li/h;->c:Li/g;

    if-nez v0, :cond_1

    new-instance v0, Li/g;

    invoke-direct {v0}, Li/g;-><init>()V

    iput-object v0, p0, Li/h;->c:Li/g;

    :cond_1
    iget-object v0, p0, Li/h;->c:Li/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3324
    const/4 v0, 0x1

    iget-object v1, p0, Li/h;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3325
    const/4 v0, 0x2

    iget-object v1, p0, Li/h;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3326
    iget-object v0, p0, Li/h;->c:Li/g;

    if-eqz v0, :cond_0

    .line 3327
    const/4 v0, 0x3

    iget-object v1, p0, Li/h;->c:Li/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3329
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3330
    return-void
.end method
