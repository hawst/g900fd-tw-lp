.class public final Li/n;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 2141
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2142
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Li/n;->a:J

    const-string v0, ""

    iput-object v0, p0, Li/n;->b:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Li/n;->cachedSize:I

    .line 2143
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 2194
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2195
    const/4 v1, 0x1

    iget-wide v2, p0, Li/n;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2197
    const/4 v1, 0x2

    iget-object v2, p0, Li/n;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2199
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2154
    if-ne p1, p0, :cond_1

    .line 2171
    :cond_0
    :goto_0
    return v0

    .line 2157
    :cond_1
    instance-of v2, p1, Li/n;

    if-nez v2, :cond_2

    move v0, v1

    .line 2158
    goto :goto_0

    .line 2160
    :cond_2
    check-cast p1, Li/n;

    .line 2161
    iget-wide v2, p0, Li/n;->a:J

    iget-wide v4, p1, Li/n;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 2162
    goto :goto_0

    .line 2164
    :cond_3
    iget-object v2, p0, Li/n;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 2165
    iget-object v2, p1, Li/n;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 2166
    goto :goto_0

    .line 2168
    :cond_4
    iget-object v2, p0, Li/n;->b:Ljava/lang/String;

    iget-object v3, p1, Li/n;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2169
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    .line 2176
    iget-wide v0, p0, Li/n;->a:J

    iget-wide v2, p0, Li/n;->a:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 2179
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Li/n;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 2181
    return v0

    .line 2179
    :cond_0
    iget-object v0, p0, Li/n;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 2118
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Li/n;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/n;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 2187
    const/4 v0, 0x1

    iget-wide v2, p0, Li/n;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2188
    const/4 v0, 0x2

    iget-object v1, p0, Li/n;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2189
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2190
    return-void
.end method
