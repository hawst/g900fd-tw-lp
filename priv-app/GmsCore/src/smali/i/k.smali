.class public final Li/k;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 956
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 957
    const-string v0, ""

    iput-object v0, p0, Li/k;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/k;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/k;->c:Ljava/lang/String;

    iput v1, p0, Li/k;->d:I

    const-string v0, ""

    iput-object v0, p0, Li/k;->e:Ljava/lang/String;

    iput-boolean v1, p0, Li/k;->f:Z

    const-string v0, ""

    iput-object v0, p0, Li/k;->g:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Li/k;->cachedSize:I

    .line 958
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1066
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1067
    const/4 v1, 0x1

    iget-object v2, p0, Li/k;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1069
    const/4 v1, 0x2

    iget-object v2, p0, Li/k;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1071
    const/4 v1, 0x3

    iget-object v2, p0, Li/k;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1073
    iget v1, p0, Li/k;->d:I

    if-eqz v1, :cond_0

    .line 1074
    const/4 v1, 0x4

    iget v2, p0, Li/k;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1077
    :cond_0
    iget-object v1, p0, Li/k;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1078
    const/4 v1, 0x5

    iget-object v2, p0, Li/k;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1081
    :cond_1
    iget-boolean v1, p0, Li/k;->f:Z

    if-eqz v1, :cond_2

    .line 1082
    const/4 v1, 0x6

    iget-boolean v2, p0, Li/k;->f:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1085
    :cond_2
    iget-object v1, p0, Li/k;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1086
    const/4 v1, 0x7

    iget-object v2, p0, Li/k;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1089
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 974
    if-ne p1, p0, :cond_1

    .line 1022
    :cond_0
    :goto_0
    return v0

    .line 977
    :cond_1
    instance-of v2, p1, Li/k;

    if-nez v2, :cond_2

    move v0, v1

    .line 978
    goto :goto_0

    .line 980
    :cond_2
    check-cast p1, Li/k;

    .line 981
    iget-object v2, p0, Li/k;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 982
    iget-object v2, p1, Li/k;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 983
    goto :goto_0

    .line 985
    :cond_3
    iget-object v2, p0, Li/k;->a:Ljava/lang/String;

    iget-object v3, p1, Li/k;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 986
    goto :goto_0

    .line 988
    :cond_4
    iget-object v2, p0, Li/k;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 989
    iget-object v2, p1, Li/k;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 990
    goto :goto_0

    .line 992
    :cond_5
    iget-object v2, p0, Li/k;->b:Ljava/lang/String;

    iget-object v3, p1, Li/k;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 993
    goto :goto_0

    .line 995
    :cond_6
    iget-object v2, p0, Li/k;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 996
    iget-object v2, p1, Li/k;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 997
    goto :goto_0

    .line 999
    :cond_7
    iget-object v2, p0, Li/k;->c:Ljava/lang/String;

    iget-object v3, p1, Li/k;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1000
    goto :goto_0

    .line 1002
    :cond_8
    iget v2, p0, Li/k;->d:I

    iget v3, p1, Li/k;->d:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1003
    goto :goto_0

    .line 1005
    :cond_9
    iget-object v2, p0, Li/k;->e:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 1006
    iget-object v2, p1, Li/k;->e:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 1007
    goto :goto_0

    .line 1009
    :cond_a
    iget-object v2, p0, Li/k;->e:Ljava/lang/String;

    iget-object v3, p1, Li/k;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1010
    goto :goto_0

    .line 1012
    :cond_b
    iget-boolean v2, p0, Li/k;->f:Z

    iget-boolean v3, p1, Li/k;->f:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 1013
    goto :goto_0

    .line 1015
    :cond_c
    iget-object v2, p0, Li/k;->g:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 1016
    iget-object v2, p1, Li/k;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1017
    goto :goto_0

    .line 1019
    :cond_d
    iget-object v2, p0, Li/k;->g:Ljava/lang/String;

    iget-object v3, p1, Li/k;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1020
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1027
    iget-object v0, p0, Li/k;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1030
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/k;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1032
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/k;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1034
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Li/k;->d:I

    add-int/2addr v0, v2

    .line 1035
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/k;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 1037
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Li/k;->f:Z

    if-eqz v0, :cond_4

    const/16 v0, 0x4cf

    :goto_4
    add-int/2addr v0, v2

    .line 1038
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Li/k;->g:Ljava/lang/String;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 1040
    return v0

    .line 1027
    :cond_0
    iget-object v0, p0, Li/k;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1030
    :cond_1
    iget-object v0, p0, Li/k;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1032
    :cond_2
    iget-object v0, p0, Li/k;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1035
    :cond_3
    iget-object v0, p0, Li/k;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 1037
    :cond_4
    const/16 v0, 0x4d5

    goto :goto_4

    .line 1038
    :cond_5
    iget-object v1, p0, Li/k;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 918
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/k;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/k;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/k;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Li/k;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/k;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Li/k;->f:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/k;->g:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1046
    const/4 v0, 0x1

    iget-object v1, p0, Li/k;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1047
    const/4 v0, 0x2

    iget-object v1, p0, Li/k;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1048
    const/4 v0, 0x3

    iget-object v1, p0, Li/k;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1049
    iget v0, p0, Li/k;->d:I

    if-eqz v0, :cond_0

    .line 1050
    const/4 v0, 0x4

    iget v1, p0, Li/k;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1052
    :cond_0
    iget-object v0, p0, Li/k;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1053
    const/4 v0, 0x5

    iget-object v1, p0, Li/k;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1055
    :cond_1
    iget-boolean v0, p0, Li/k;->f:Z

    if-eqz v0, :cond_2

    .line 1056
    const/4 v0, 0x6

    iget-boolean v1, p0, Li/k;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1058
    :cond_2
    iget-object v0, p0, Li/k;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1059
    const/4 v0, 0x7

    iget-object v1, p0, Li/k;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1061
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1062
    return-void
.end method
