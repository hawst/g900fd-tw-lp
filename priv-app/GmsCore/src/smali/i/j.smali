.class public final Li/j;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Li/o;

.field public b:Li/k;

.field public c:Li/d;

.field public d:Li/f;

.field public e:Li/b;

.field public f:Li/c;

.field public g:Li/n;

.field public h:Li/q;

.field public i:I

.field public j:J

.field public k:Z

.field public l:Z


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, 0x0

    .line 254
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 255
    iput-object v0, p0, Li/j;->a:Li/o;

    iput-object v0, p0, Li/j;->b:Li/k;

    iput-object v0, p0, Li/j;->c:Li/d;

    iput-object v0, p0, Li/j;->d:Li/f;

    iput-object v0, p0, Li/j;->e:Li/b;

    iput-object v0, p0, Li/j;->f:Li/c;

    iput-object v0, p0, Li/j;->g:Li/n;

    iput-object v0, p0, Li/j;->h:Li/q;

    iput v2, p0, Li/j;->i:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Li/j;->j:J

    iput-boolean v2, p0, Li/j;->k:Z

    iput-boolean v2, p0, Li/j;->l:Z

    const/4 v0, -0x1

    iput v0, p0, Li/j;->cachedSize:I

    .line 256
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 442
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 443
    iget-object v1, p0, Li/j;->a:Li/o;

    if-eqz v1, :cond_0

    .line 444
    const/4 v1, 0x1

    iget-object v2, p0, Li/j;->a:Li/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 447
    :cond_0
    iget-object v1, p0, Li/j;->b:Li/k;

    if-eqz v1, :cond_1

    .line 448
    const/4 v1, 0x2

    iget-object v2, p0, Li/j;->b:Li/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 451
    :cond_1
    iget-object v1, p0, Li/j;->c:Li/d;

    if-eqz v1, :cond_2

    .line 452
    const/4 v1, 0x3

    iget-object v2, p0, Li/j;->c:Li/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 455
    :cond_2
    iget-object v1, p0, Li/j;->d:Li/f;

    if-eqz v1, :cond_3

    .line 456
    const/4 v1, 0x4

    iget-object v2, p0, Li/j;->d:Li/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 459
    :cond_3
    iget-object v1, p0, Li/j;->e:Li/b;

    if-eqz v1, :cond_4

    .line 460
    const/4 v1, 0x5

    iget-object v2, p0, Li/j;->e:Li/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 463
    :cond_4
    iget-object v1, p0, Li/j;->f:Li/c;

    if-eqz v1, :cond_5

    .line 464
    const/4 v1, 0x6

    iget-object v2, p0, Li/j;->f:Li/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 467
    :cond_5
    iget-object v1, p0, Li/j;->g:Li/n;

    if-eqz v1, :cond_6

    .line 468
    const/4 v1, 0x7

    iget-object v2, p0, Li/j;->g:Li/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 471
    :cond_6
    iget-object v1, p0, Li/j;->h:Li/q;

    if-eqz v1, :cond_7

    .line 472
    const/16 v1, 0x9

    iget-object v2, p0, Li/j;->h:Li/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 475
    :cond_7
    iget v1, p0, Li/j;->i:I

    if-eqz v1, :cond_8

    .line 476
    const/16 v1, 0xa

    iget v2, p0, Li/j;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 479
    :cond_8
    iget-wide v2, p0, Li/j;->j:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 480
    const/16 v1, 0xb

    iget-wide v2, p0, Li/j;->j:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 483
    :cond_9
    iget-boolean v1, p0, Li/j;->k:Z

    if-eqz v1, :cond_a

    .line 484
    const/16 v1, 0xc

    iget-boolean v2, p0, Li/j;->k:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 487
    :cond_a
    iget-boolean v1, p0, Li/j;->l:Z

    if-eqz v1, :cond_b

    .line 488
    const/16 v1, 0xd

    iget-boolean v2, p0, Li/j;->l:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 491
    :cond_b
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 277
    if-ne p1, p0, :cond_1

    .line 368
    :cond_0
    :goto_0
    return v0

    .line 280
    :cond_1
    instance-of v2, p1, Li/j;

    if-nez v2, :cond_2

    move v0, v1

    .line 281
    goto :goto_0

    .line 283
    :cond_2
    check-cast p1, Li/j;

    .line 284
    iget-object v2, p0, Li/j;->a:Li/o;

    if-nez v2, :cond_3

    .line 285
    iget-object v2, p1, Li/j;->a:Li/o;

    if-eqz v2, :cond_4

    move v0, v1

    .line 286
    goto :goto_0

    .line 289
    :cond_3
    iget-object v2, p0, Li/j;->a:Li/o;

    iget-object v3, p1, Li/j;->a:Li/o;

    invoke-virtual {v2, v3}, Li/o;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 290
    goto :goto_0

    .line 293
    :cond_4
    iget-object v2, p0, Li/j;->b:Li/k;

    if-nez v2, :cond_5

    .line 294
    iget-object v2, p1, Li/j;->b:Li/k;

    if-eqz v2, :cond_6

    move v0, v1

    .line 295
    goto :goto_0

    .line 298
    :cond_5
    iget-object v2, p0, Li/j;->b:Li/k;

    iget-object v3, p1, Li/j;->b:Li/k;

    invoke-virtual {v2, v3}, Li/k;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 299
    goto :goto_0

    .line 302
    :cond_6
    iget-object v2, p0, Li/j;->c:Li/d;

    if-nez v2, :cond_7

    .line 303
    iget-object v2, p1, Li/j;->c:Li/d;

    if-eqz v2, :cond_8

    move v0, v1

    .line 304
    goto :goto_0

    .line 307
    :cond_7
    iget-object v2, p0, Li/j;->c:Li/d;

    iget-object v3, p1, Li/j;->c:Li/d;

    invoke-virtual {v2, v3}, Li/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 308
    goto :goto_0

    .line 311
    :cond_8
    iget-object v2, p0, Li/j;->d:Li/f;

    if-nez v2, :cond_9

    .line 312
    iget-object v2, p1, Li/j;->d:Li/f;

    if-eqz v2, :cond_a

    move v0, v1

    .line 313
    goto :goto_0

    .line 316
    :cond_9
    iget-object v2, p0, Li/j;->d:Li/f;

    iget-object v3, p1, Li/j;->d:Li/f;

    invoke-virtual {v2, v3}, Li/f;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 317
    goto :goto_0

    .line 320
    :cond_a
    iget-object v2, p0, Li/j;->e:Li/b;

    if-nez v2, :cond_b

    .line 321
    iget-object v2, p1, Li/j;->e:Li/b;

    if-eqz v2, :cond_c

    move v0, v1

    .line 322
    goto :goto_0

    .line 325
    :cond_b
    iget-object v2, p0, Li/j;->e:Li/b;

    iget-object v3, p1, Li/j;->e:Li/b;

    invoke-virtual {v2, v3}, Li/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 326
    goto :goto_0

    .line 329
    :cond_c
    iget-object v2, p0, Li/j;->f:Li/c;

    if-nez v2, :cond_d

    .line 330
    iget-object v2, p1, Li/j;->f:Li/c;

    if-eqz v2, :cond_e

    move v0, v1

    .line 331
    goto :goto_0

    .line 334
    :cond_d
    iget-object v2, p0, Li/j;->f:Li/c;

    iget-object v3, p1, Li/j;->f:Li/c;

    invoke-virtual {v2, v3}, Li/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 335
    goto/16 :goto_0

    .line 338
    :cond_e
    iget-object v2, p0, Li/j;->g:Li/n;

    if-nez v2, :cond_f

    .line 339
    iget-object v2, p1, Li/j;->g:Li/n;

    if-eqz v2, :cond_10

    move v0, v1

    .line 340
    goto/16 :goto_0

    .line 343
    :cond_f
    iget-object v2, p0, Li/j;->g:Li/n;

    iget-object v3, p1, Li/j;->g:Li/n;

    invoke-virtual {v2, v3}, Li/n;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 344
    goto/16 :goto_0

    .line 347
    :cond_10
    iget-object v2, p0, Li/j;->h:Li/q;

    if-nez v2, :cond_11

    .line 348
    iget-object v2, p1, Li/j;->h:Li/q;

    if-eqz v2, :cond_12

    move v0, v1

    .line 349
    goto/16 :goto_0

    .line 352
    :cond_11
    iget-object v2, p0, Li/j;->h:Li/q;

    iget-object v3, p1, Li/j;->h:Li/q;

    invoke-virtual {v2, v3}, Li/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    .line 353
    goto/16 :goto_0

    .line 356
    :cond_12
    iget v2, p0, Li/j;->i:I

    iget v3, p1, Li/j;->i:I

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 357
    goto/16 :goto_0

    .line 359
    :cond_13
    iget-wide v2, p0, Li/j;->j:J

    iget-wide v4, p1, Li/j;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_14

    move v0, v1

    .line 360
    goto/16 :goto_0

    .line 362
    :cond_14
    iget-boolean v2, p0, Li/j;->k:Z

    iget-boolean v3, p1, Li/j;->k:Z

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 363
    goto/16 :goto_0

    .line 365
    :cond_15
    iget-boolean v2, p0, Li/j;->l:Z

    iget-boolean v3, p1, Li/j;->l:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 366
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 8

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 373
    iget-object v0, p0, Li/j;->a:Li/o;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 376
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Li/j;->b:Li/k;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 378
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Li/j;->c:Li/d;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 380
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Li/j;->d:Li/f;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    .line 382
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Li/j;->e:Li/b;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v4

    .line 384
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Li/j;->f:Li/c;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v4

    .line 386
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Li/j;->g:Li/n;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v4

    .line 388
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Li/j;->h:Li/q;

    if-nez v4, :cond_7

    :goto_7
    add-int/2addr v0, v1

    .line 390
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Li/j;->i:I

    add-int/2addr v0, v1

    .line 391
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Li/j;->j:J

    iget-wide v6, p0, Li/j;->j:J

    const/16 v1, 0x20

    ushr-long/2addr v6, v1

    xor-long/2addr v4, v6

    long-to-int v1, v4

    add-int/2addr v0, v1

    .line 393
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Li/j;->k:Z

    if-eqz v0, :cond_8

    move v0, v2

    :goto_8
    add-int/2addr v0, v1

    .line 394
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Li/j;->l:Z

    if-eqz v1, :cond_9

    :goto_9
    add-int/2addr v0, v2

    .line 395
    return v0

    .line 373
    :cond_0
    iget-object v0, p0, Li/j;->a:Li/o;

    invoke-virtual {v0}, Li/o;->hashCode()I

    move-result v0

    goto :goto_0

    .line 376
    :cond_1
    iget-object v0, p0, Li/j;->b:Li/k;

    invoke-virtual {v0}, Li/k;->hashCode()I

    move-result v0

    goto :goto_1

    .line 378
    :cond_2
    iget-object v0, p0, Li/j;->c:Li/d;

    invoke-virtual {v0}, Li/d;->hashCode()I

    move-result v0

    goto :goto_2

    .line 380
    :cond_3
    iget-object v0, p0, Li/j;->d:Li/f;

    invoke-virtual {v0}, Li/f;->hashCode()I

    move-result v0

    goto :goto_3

    .line 382
    :cond_4
    iget-object v0, p0, Li/j;->e:Li/b;

    invoke-virtual {v0}, Li/b;->hashCode()I

    move-result v0

    goto :goto_4

    .line 384
    :cond_5
    iget-object v0, p0, Li/j;->f:Li/c;

    invoke-virtual {v0}, Li/c;->hashCode()I

    move-result v0

    goto :goto_5

    .line 386
    :cond_6
    iget-object v0, p0, Li/j;->g:Li/n;

    invoke-virtual {v0}, Li/n;->hashCode()I

    move-result v0

    goto :goto_6

    .line 388
    :cond_7
    iget-object v1, p0, Li/j;->h:Li/q;

    invoke-virtual {v1}, Li/q;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_8
    move v0, v3

    .line 393
    goto :goto_8

    :cond_9
    move v2, v3

    .line 394
    goto :goto_9
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 201
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Li/j;->a:Li/o;

    if-nez v0, :cond_1

    new-instance v0, Li/o;

    invoke-direct {v0}, Li/o;-><init>()V

    iput-object v0, p0, Li/j;->a:Li/o;

    :cond_1
    iget-object v0, p0, Li/j;->a:Li/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Li/j;->b:Li/k;

    if-nez v0, :cond_2

    new-instance v0, Li/k;

    invoke-direct {v0}, Li/k;-><init>()V

    iput-object v0, p0, Li/j;->b:Li/k;

    :cond_2
    iget-object v0, p0, Li/j;->b:Li/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Li/j;->c:Li/d;

    if-nez v0, :cond_3

    new-instance v0, Li/d;

    invoke-direct {v0}, Li/d;-><init>()V

    iput-object v0, p0, Li/j;->c:Li/d;

    :cond_3
    iget-object v0, p0, Li/j;->c:Li/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Li/j;->d:Li/f;

    if-nez v0, :cond_4

    new-instance v0, Li/f;

    invoke-direct {v0}, Li/f;-><init>()V

    iput-object v0, p0, Li/j;->d:Li/f;

    :cond_4
    iget-object v0, p0, Li/j;->d:Li/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Li/j;->e:Li/b;

    if-nez v0, :cond_5

    new-instance v0, Li/b;

    invoke-direct {v0}, Li/b;-><init>()V

    iput-object v0, p0, Li/j;->e:Li/b;

    :cond_5
    iget-object v0, p0, Li/j;->e:Li/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Li/j;->f:Li/c;

    if-nez v0, :cond_6

    new-instance v0, Li/c;

    invoke-direct {v0}, Li/c;-><init>()V

    iput-object v0, p0, Li/j;->f:Li/c;

    :cond_6
    iget-object v0, p0, Li/j;->f:Li/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Li/j;->g:Li/n;

    if-nez v0, :cond_7

    new-instance v0, Li/n;

    invoke-direct {v0}, Li/n;-><init>()V

    iput-object v0, p0, Li/j;->g:Li/n;

    :cond_7
    iget-object v0, p0, Li/j;->g:Li/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Li/j;->h:Li/q;

    if-nez v0, :cond_8

    new-instance v0, Li/q;

    invoke-direct {v0}, Li/q;-><init>()V

    iput-object v0, p0, Li/j;->h:Li/q;

    :cond_8
    iget-object v0, p0, Li/j;->h:Li/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Li/j;->i:I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Li/j;->j:J

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Li/j;->k:Z

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Li/j;->l:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
        0x68 -> :sswitch_c
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 401
    iget-object v0, p0, Li/j;->a:Li/o;

    if-eqz v0, :cond_0

    .line 402
    const/4 v0, 0x1

    iget-object v1, p0, Li/j;->a:Li/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 404
    :cond_0
    iget-object v0, p0, Li/j;->b:Li/k;

    if-eqz v0, :cond_1

    .line 405
    const/4 v0, 0x2

    iget-object v1, p0, Li/j;->b:Li/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 407
    :cond_1
    iget-object v0, p0, Li/j;->c:Li/d;

    if-eqz v0, :cond_2

    .line 408
    const/4 v0, 0x3

    iget-object v1, p0, Li/j;->c:Li/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 410
    :cond_2
    iget-object v0, p0, Li/j;->d:Li/f;

    if-eqz v0, :cond_3

    .line 411
    const/4 v0, 0x4

    iget-object v1, p0, Li/j;->d:Li/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 413
    :cond_3
    iget-object v0, p0, Li/j;->e:Li/b;

    if-eqz v0, :cond_4

    .line 414
    const/4 v0, 0x5

    iget-object v1, p0, Li/j;->e:Li/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 416
    :cond_4
    iget-object v0, p0, Li/j;->f:Li/c;

    if-eqz v0, :cond_5

    .line 417
    const/4 v0, 0x6

    iget-object v1, p0, Li/j;->f:Li/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 419
    :cond_5
    iget-object v0, p0, Li/j;->g:Li/n;

    if-eqz v0, :cond_6

    .line 420
    const/4 v0, 0x7

    iget-object v1, p0, Li/j;->g:Li/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 422
    :cond_6
    iget-object v0, p0, Li/j;->h:Li/q;

    if-eqz v0, :cond_7

    .line 423
    const/16 v0, 0x9

    iget-object v1, p0, Li/j;->h:Li/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 425
    :cond_7
    iget v0, p0, Li/j;->i:I

    if-eqz v0, :cond_8

    .line 426
    const/16 v0, 0xa

    iget v1, p0, Li/j;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 428
    :cond_8
    iget-wide v0, p0, Li/j;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_9

    .line 429
    const/16 v0, 0xb

    iget-wide v2, p0, Li/j;->j:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 431
    :cond_9
    iget-boolean v0, p0, Li/j;->k:Z

    if-eqz v0, :cond_a

    .line 432
    const/16 v0, 0xc

    iget-boolean v1, p0, Li/j;->k:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 434
    :cond_a
    iget-boolean v0, p0, Li/j;->l:Z

    if-eqz v0, :cond_b

    .line 435
    const/16 v0, 0xd

    iget-boolean v1, p0, Li/j;->l:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 437
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 438
    return-void
.end method
