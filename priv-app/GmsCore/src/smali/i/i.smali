.class public final Li/i;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Li/e;

.field public b:Li/j;

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 42
    iput-object v1, p0, Li/i;->a:Li/e;

    iput-object v1, p0, Li/i;->b:Li/j;

    iput v0, p0, Li/i;->c:I

    iput v0, p0, Li/i;->d:I

    const/4 v0, -0x1

    iput v0, p0, Li/i;->cachedSize:I

    .line 43
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 122
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 123
    iget-object v1, p0, Li/i;->a:Li/e;

    if-eqz v1, :cond_0

    .line 124
    const/4 v1, 0x1

    iget-object v2, p0, Li/i;->a:Li/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    :cond_0
    iget-object v1, p0, Li/i;->b:Li/j;

    if-eqz v1, :cond_1

    .line 128
    const/4 v1, 0x2

    iget-object v2, p0, Li/i;->b:Li/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_1
    iget v1, p0, Li/i;->c:I

    if-eqz v1, :cond_2

    .line 132
    const/4 v1, 0x3

    iget v2, p0, Li/i;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_2
    iget v1, p0, Li/i;->d:I

    if-eqz v1, :cond_3

    .line 136
    const/4 v1, 0x4

    iget v2, p0, Li/i;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 56
    if-ne p1, p0, :cond_1

    .line 87
    :cond_0
    :goto_0
    return v0

    .line 59
    :cond_1
    instance-of v2, p1, Li/i;

    if-nez v2, :cond_2

    move v0, v1

    .line 60
    goto :goto_0

    .line 62
    :cond_2
    check-cast p1, Li/i;

    .line 63
    iget-object v2, p0, Li/i;->a:Li/e;

    if-nez v2, :cond_3

    .line 64
    iget-object v2, p1, Li/i;->a:Li/e;

    if-eqz v2, :cond_4

    move v0, v1

    .line 65
    goto :goto_0

    .line 68
    :cond_3
    iget-object v2, p0, Li/i;->a:Li/e;

    iget-object v3, p1, Li/i;->a:Li/e;

    invoke-virtual {v2, v3}, Li/e;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 69
    goto :goto_0

    .line 72
    :cond_4
    iget-object v2, p0, Li/i;->b:Li/j;

    if-nez v2, :cond_5

    .line 73
    iget-object v2, p1, Li/i;->b:Li/j;

    if-eqz v2, :cond_6

    move v0, v1

    .line 74
    goto :goto_0

    .line 77
    :cond_5
    iget-object v2, p0, Li/i;->b:Li/j;

    iget-object v3, p1, Li/i;->b:Li/j;

    invoke-virtual {v2, v3}, Li/j;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 78
    goto :goto_0

    .line 81
    :cond_6
    iget v2, p0, Li/i;->c:I

    iget v3, p1, Li/i;->c:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 82
    goto :goto_0

    .line 84
    :cond_7
    iget v2, p0, Li/i;->d:I

    iget v3, p1, Li/i;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 85
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 92
    iget-object v0, p0, Li/i;->a:Li/e;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 95
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Li/i;->b:Li/j;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 97
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Li/i;->c:I

    add-int/2addr v0, v1

    .line 98
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Li/i;->d:I

    add-int/2addr v0, v1

    .line 99
    return v0

    .line 92
    :cond_0
    iget-object v0, p0, Li/i;->a:Li/e;

    invoke-virtual {v0}, Li/e;->hashCode()I

    move-result v0

    goto :goto_0

    .line 95
    :cond_1
    iget-object v1, p0, Li/i;->b:Li/j;

    invoke-virtual {v1}, Li/j;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Li/i;->a:Li/e;

    if-nez v0, :cond_1

    new-instance v0, Li/e;

    invoke-direct {v0}, Li/e;-><init>()V

    iput-object v0, p0, Li/i;->a:Li/e;

    :cond_1
    iget-object v0, p0, Li/i;->a:Li/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Li/i;->b:Li/j;

    if-nez v0, :cond_2

    new-instance v0, Li/j;

    invoke-direct {v0}, Li/j;-><init>()V

    iput-object v0, p0, Li/i;->b:Li/j;

    :cond_2
    iget-object v0, p0, Li/i;->b:Li/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Li/i;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Li/i;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 105
    iget-object v0, p0, Li/i;->a:Li/e;

    if-eqz v0, :cond_0

    .line 106
    const/4 v0, 0x1

    iget-object v1, p0, Li/i;->a:Li/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 108
    :cond_0
    iget-object v0, p0, Li/i;->b:Li/j;

    if-eqz v0, :cond_1

    .line 109
    const/4 v0, 0x2

    iget-object v1, p0, Li/i;->b:Li/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 111
    :cond_1
    iget v0, p0, Li/i;->c:I

    if-eqz v0, :cond_2

    .line 112
    const/4 v0, 0x3

    iget v1, p0, Li/i;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 114
    :cond_2
    iget v0, p0, Li/i;->d:I

    if-eqz v0, :cond_3

    .line 115
    const/4 v0, 0x4

    iget v1, p0, Li/i;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 117
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 118
    return-void
.end method
