.class public final Li/c;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:J

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:F

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1943
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1944
    iput v2, p0, Li/c;->a:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Li/c;->b:J

    const-string v0, ""

    iput-object v0, p0, Li/c;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Li/c;->d:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Li/c;->e:F

    iput v2, p0, Li/c;->f:I

    const/4 v0, -0x1

    iput v0, p0, Li/c;->cachedSize:I

    .line 1945
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 2034
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2035
    const/4 v1, 0x1

    iget v2, p0, Li/c;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2037
    const/4 v1, 0x2

    iget-wide v2, p0, Li/c;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2039
    const/4 v1, 0x3

    iget-object v2, p0, Li/c;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2041
    const/4 v1, 0x4

    iget-object v2, p0, Li/c;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2043
    iget v1, p0, Li/c;->e:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    if-eq v1, v2, :cond_0

    .line 2045
    const/4 v1, 0x5

    iget v2, p0, Li/c;->e:F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 2048
    :cond_0
    iget v1, p0, Li/c;->f:I

    if-eqz v1, :cond_1

    .line 2049
    const/4 v1, 0x6

    iget v2, p0, Li/c;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2052
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1960
    if-ne p1, p0, :cond_1

    .line 1996
    :cond_0
    :goto_0
    return v0

    .line 1963
    :cond_1
    instance-of v2, p1, Li/c;

    if-nez v2, :cond_2

    move v0, v1

    .line 1964
    goto :goto_0

    .line 1966
    :cond_2
    check-cast p1, Li/c;

    .line 1967
    iget v2, p0, Li/c;->a:I

    iget v3, p1, Li/c;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1968
    goto :goto_0

    .line 1970
    :cond_3
    iget-wide v2, p0, Li/c;->b:J

    iget-wide v4, p1, Li/c;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 1971
    goto :goto_0

    .line 1973
    :cond_4
    iget-object v2, p0, Li/c;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1974
    iget-object v2, p1, Li/c;->c:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 1975
    goto :goto_0

    .line 1977
    :cond_5
    iget-object v2, p0, Li/c;->c:Ljava/lang/String;

    iget-object v3, p1, Li/c;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1978
    goto :goto_0

    .line 1980
    :cond_6
    iget-object v2, p0, Li/c;->d:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1981
    iget-object v2, p1, Li/c;->d:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 1982
    goto :goto_0

    .line 1984
    :cond_7
    iget-object v2, p0, Li/c;->d:Ljava/lang/String;

    iget-object v3, p1, Li/c;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1985
    goto :goto_0

    .line 1988
    :cond_8
    iget v2, p0, Li/c;->e:F

    invoke-static {v2}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v2

    .line 1989
    iget v3, p1, Li/c;->e:F

    invoke-static {v3}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v3

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1990
    goto :goto_0

    .line 1993
    :cond_9
    iget v2, p0, Li/c;->f:I

    iget v3, p1, Li/c;->f:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1994
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2001
    iget v0, p0, Li/c;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 2003
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Li/c;->b:J

    iget-wide v4, p0, Li/c;->b:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 2005
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Li/c;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 2007
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Li/c;->d:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 2009
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Li/c;->e:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    add-int/2addr v0, v1

    .line 2011
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Li/c;->f:I

    add-int/2addr v0, v1

    .line 2012
    return v0

    .line 2005
    :cond_0
    iget-object v0, p0, Li/c;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2007
    :cond_1
    iget-object v1, p0, Li/c;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 1902
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Li/c;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Li/c;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/c;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Li/c;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    iput v0, p0, Li/c;->e:F

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Li/c;->f:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2d -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 2018
    const/4 v0, 0x1

    iget v1, p0, Li/c;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2019
    const/4 v0, 0x2

    iget-wide v2, p0, Li/c;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2020
    const/4 v0, 0x3

    iget-object v1, p0, Li/c;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2021
    const/4 v0, 0x4

    iget-object v1, p0, Li/c;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2022
    iget v0, p0, Li/c;->e:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 2024
    const/4 v0, 0x5

    iget v1, p0, Li/c;->e:F

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 2026
    :cond_0
    iget v0, p0, Li/c;->f:I

    if-eqz v0, :cond_1

    .line 2027
    const/4 v0, 0x6

    iget v1, p0, Li/c;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2029
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2030
    return-void
.end method
