.class public final Lg/a/a;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lg/a/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1352
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1353
    const/4 v0, 0x0

    iput-object v0, p0, Lg/a/a;->a:Lg/a/b;

    const/4 v0, -0x1

    iput v0, p0, Lg/a/a;->cachedSize:I

    .line 1354
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1402
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1403
    iget-object v1, p0, Lg/a/a;->a:Lg/a/b;

    if-eqz v1, :cond_0

    .line 1404
    const/4 v1, 0x2

    iget-object v2, p0, Lg/a/a;->a:Lg/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1407
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1364
    if-ne p1, p0, :cond_1

    .line 1380
    :cond_0
    :goto_0
    return v0

    .line 1367
    :cond_1
    instance-of v2, p1, Lg/a/a;

    if-nez v2, :cond_2

    move v0, v1

    .line 1368
    goto :goto_0

    .line 1370
    :cond_2
    check-cast p1, Lg/a/a;

    .line 1371
    iget-object v2, p0, Lg/a/a;->a:Lg/a/b;

    if-nez v2, :cond_3

    .line 1372
    iget-object v2, p1, Lg/a/a;->a:Lg/a/b;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1373
    goto :goto_0

    .line 1376
    :cond_3
    iget-object v2, p0, Lg/a/a;->a:Lg/a/b;

    iget-object v3, p1, Lg/a/a;->a:Lg/a/b;

    invoke-virtual {v2, v3}, Lg/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1377
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 1385
    iget-object v0, p0, Lg/a/a;->a:Lg/a/b;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1388
    return v0

    .line 1385
    :cond_0
    iget-object v0, p0, Lg/a/a;->a:Lg/a/b;

    invoke-virtual {v0}, Lg/a/b;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lg/a/a;->a:Lg/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lg/a/b;

    invoke-direct {v0}, Lg/a/b;-><init>()V

    iput-object v0, p0, Lg/a/a;->a:Lg/a/b;

    :cond_1
    iget-object v0, p0, Lg/a/a;->a:Lg/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1394
    iget-object v0, p0, Lg/a/a;->a:Lg/a/b;

    if-eqz v0, :cond_0

    .line 1395
    const/4 v0, 0x2

    iget-object v1, p0, Lg/a/a;->a:Lg/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1397
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1398
    return-void
.end method
