.class public final Lg/a/c;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:J

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 70
    const/16 v0, 0x64

    iput v0, p0, Lg/a/c;->a:I

    const-string v0, ""

    iput-object v0, p0, Lg/a/c;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lg/a/c;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lg/a/c;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lg/a/c;->e:Ljava/lang/String;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lg/a/c;->f:J

    const-string v0, ""

    iput-object v0, p0, Lg/a/c;->g:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lg/a/c;->h:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lg/a/c;->i:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lg/a/c;->j:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lg/a/c;->k:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lg/a/c;->l:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lg/a/c;->cachedSize:I

    .line 71
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 251
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 252
    iget v1, p0, Lg/a/c;->a:I

    const/16 v2, 0x64

    if-eq v1, v2, :cond_0

    .line 253
    const/4 v1, 0x1

    iget v2, p0, Lg/a/c;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 256
    :cond_0
    iget-object v1, p0, Lg/a/c;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 257
    const/4 v1, 0x2

    iget-object v2, p0, Lg/a/c;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    :cond_1
    iget-object v1, p0, Lg/a/c;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 261
    const/4 v1, 0x3

    iget-object v2, p0, Lg/a/c;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    :cond_2
    iget-object v1, p0, Lg/a/c;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 265
    const/4 v1, 0x5

    iget-object v2, p0, Lg/a/c;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 268
    :cond_3
    iget-object v1, p0, Lg/a/c;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 269
    const/4 v1, 0x6

    iget-object v2, p0, Lg/a/c;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 272
    :cond_4
    iget-wide v2, p0, Lg/a/c;->f:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 273
    const/4 v1, 0x7

    iget-wide v2, p0, Lg/a/c;->f:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 276
    :cond_5
    iget-object v1, p0, Lg/a/c;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 277
    const/16 v1, 0x9

    iget-object v2, p0, Lg/a/c;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 280
    :cond_6
    iget-object v1, p0, Lg/a/c;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 281
    const/16 v1, 0xa

    iget-object v2, p0, Lg/a/c;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 284
    :cond_7
    iget-object v1, p0, Lg/a/c;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 285
    const/16 v1, 0xb

    iget-object v2, p0, Lg/a/c;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 288
    :cond_8
    iget-object v1, p0, Lg/a/c;->j:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 289
    const/16 v1, 0xc

    iget-object v2, p0, Lg/a/c;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 292
    :cond_9
    iget-object v1, p0, Lg/a/c;->k:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 293
    const/16 v1, 0xd

    iget-object v2, p0, Lg/a/c;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 296
    :cond_a
    iget-object v1, p0, Lg/a/c;->l:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 297
    const/16 v1, 0xf

    iget-object v2, p0, Lg/a/c;->l:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 300
    :cond_b
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 92
    if-ne p1, p0, :cond_1

    .line 175
    :cond_0
    :goto_0
    return v0

    .line 95
    :cond_1
    instance-of v2, p1, Lg/a/c;

    if-nez v2, :cond_2

    move v0, v1

    .line 96
    goto :goto_0

    .line 98
    :cond_2
    check-cast p1, Lg/a/c;

    .line 99
    iget v2, p0, Lg/a/c;->a:I

    iget v3, p1, Lg/a/c;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 100
    goto :goto_0

    .line 102
    :cond_3
    iget-object v2, p0, Lg/a/c;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 103
    iget-object v2, p1, Lg/a/c;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 104
    goto :goto_0

    .line 106
    :cond_4
    iget-object v2, p0, Lg/a/c;->b:Ljava/lang/String;

    iget-object v3, p1, Lg/a/c;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 107
    goto :goto_0

    .line 109
    :cond_5
    iget-object v2, p0, Lg/a/c;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 110
    iget-object v2, p1, Lg/a/c;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 111
    goto :goto_0

    .line 113
    :cond_6
    iget-object v2, p0, Lg/a/c;->c:Ljava/lang/String;

    iget-object v3, p1, Lg/a/c;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 114
    goto :goto_0

    .line 116
    :cond_7
    iget-object v2, p0, Lg/a/c;->d:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 117
    iget-object v2, p1, Lg/a/c;->d:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 118
    goto :goto_0

    .line 120
    :cond_8
    iget-object v2, p0, Lg/a/c;->d:Ljava/lang/String;

    iget-object v3, p1, Lg/a/c;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 121
    goto :goto_0

    .line 123
    :cond_9
    iget-object v2, p0, Lg/a/c;->e:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 124
    iget-object v2, p1, Lg/a/c;->e:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 125
    goto :goto_0

    .line 127
    :cond_a
    iget-object v2, p0, Lg/a/c;->e:Ljava/lang/String;

    iget-object v3, p1, Lg/a/c;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 128
    goto :goto_0

    .line 130
    :cond_b
    iget-wide v2, p0, Lg/a/c;->f:J

    iget-wide v4, p1, Lg/a/c;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    move v0, v1

    .line 131
    goto :goto_0

    .line 133
    :cond_c
    iget-object v2, p0, Lg/a/c;->g:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 134
    iget-object v2, p1, Lg/a/c;->g:Ljava/lang/String;

    if-eqz v2, :cond_e

    move v0, v1

    .line 135
    goto :goto_0

    .line 137
    :cond_d
    iget-object v2, p0, Lg/a/c;->g:Ljava/lang/String;

    iget-object v3, p1, Lg/a/c;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 138
    goto/16 :goto_0

    .line 140
    :cond_e
    iget-object v2, p0, Lg/a/c;->h:Ljava/lang/String;

    if-nez v2, :cond_f

    .line 141
    iget-object v2, p1, Lg/a/c;->h:Ljava/lang/String;

    if-eqz v2, :cond_10

    move v0, v1

    .line 142
    goto/16 :goto_0

    .line 144
    :cond_f
    iget-object v2, p0, Lg/a/c;->h:Ljava/lang/String;

    iget-object v3, p1, Lg/a/c;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 145
    goto/16 :goto_0

    .line 147
    :cond_10
    iget-object v2, p0, Lg/a/c;->i:Ljava/lang/String;

    if-nez v2, :cond_11

    .line 148
    iget-object v2, p1, Lg/a/c;->i:Ljava/lang/String;

    if-eqz v2, :cond_12

    move v0, v1

    .line 149
    goto/16 :goto_0

    .line 151
    :cond_11
    iget-object v2, p0, Lg/a/c;->i:Ljava/lang/String;

    iget-object v3, p1, Lg/a/c;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    .line 152
    goto/16 :goto_0

    .line 154
    :cond_12
    iget-object v2, p0, Lg/a/c;->j:Ljava/lang/String;

    if-nez v2, :cond_13

    .line 155
    iget-object v2, p1, Lg/a/c;->j:Ljava/lang/String;

    if-eqz v2, :cond_14

    move v0, v1

    .line 156
    goto/16 :goto_0

    .line 158
    :cond_13
    iget-object v2, p0, Lg/a/c;->j:Ljava/lang/String;

    iget-object v3, p1, Lg/a/c;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    move v0, v1

    .line 159
    goto/16 :goto_0

    .line 161
    :cond_14
    iget-object v2, p0, Lg/a/c;->k:Ljava/lang/String;

    if-nez v2, :cond_15

    .line 162
    iget-object v2, p1, Lg/a/c;->k:Ljava/lang/String;

    if-eqz v2, :cond_16

    move v0, v1

    .line 163
    goto/16 :goto_0

    .line 165
    :cond_15
    iget-object v2, p0, Lg/a/c;->k:Ljava/lang/String;

    iget-object v3, p1, Lg/a/c;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move v0, v1

    .line 166
    goto/16 :goto_0

    .line 168
    :cond_16
    iget-object v2, p0, Lg/a/c;->l:Ljava/lang/String;

    if-nez v2, :cond_17

    .line 169
    iget-object v2, p1, Lg/a/c;->l:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 170
    goto/16 :goto_0

    .line 172
    :cond_17
    iget-object v2, p0, Lg/a/c;->l:Ljava/lang/String;

    iget-object v3, p1, Lg/a/c;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 173
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 180
    iget v0, p0, Lg/a/c;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 182
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lg/a/c;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 184
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lg/a/c;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 186
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lg/a/c;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 188
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lg/a/c;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 190
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lg/a/c;->f:J

    iget-wide v4, p0, Lg/a/c;->f:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 192
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lg/a/c;->g:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 194
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lg/a/c;->h:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 196
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lg/a/c;->i:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 198
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lg/a/c;->j:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 200
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lg/a/c;->k:Ljava/lang/String;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 202
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lg/a/c;->l:Ljava/lang/String;

    if-nez v2, :cond_9

    :goto_9
    add-int/2addr v0, v1

    .line 204
    return v0

    .line 182
    :cond_0
    iget-object v0, p0, Lg/a/c;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 184
    :cond_1
    iget-object v0, p0, Lg/a/c;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 186
    :cond_2
    iget-object v0, p0, Lg/a/c;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 188
    :cond_3
    iget-object v0, p0, Lg/a/c;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 192
    :cond_4
    iget-object v0, p0, Lg/a/c;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 194
    :cond_5
    iget-object v0, p0, Lg/a/c;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 196
    :cond_6
    iget-object v0, p0, Lg/a/c;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 198
    :cond_7
    iget-object v0, p0, Lg/a/c;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_7

    .line 200
    :cond_8
    iget-object v0, p0, Lg/a/c;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_8

    .line 202
    :cond_9
    iget-object v1, p0, Lg/a/c;->l:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_9
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 12
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    iput v0, p0, Lg/a/c;->a:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/c;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/c;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/c;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/c;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lg/a/c;->f:J

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/c;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/c;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/c;->i:Ljava/lang/String;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/c;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/c;->k:Ljava/lang/String;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/c;->l:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x7a -> :sswitch_d
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0x64 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 210
    iget v0, p0, Lg/a/c;->a:I

    const/16 v1, 0x64

    if-eq v0, v1, :cond_0

    .line 211
    const/4 v0, 0x1

    iget v1, p0, Lg/a/c;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 213
    :cond_0
    iget-object v0, p0, Lg/a/c;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 214
    const/4 v0, 0x2

    iget-object v1, p0, Lg/a/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 216
    :cond_1
    iget-object v0, p0, Lg/a/c;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 217
    const/4 v0, 0x3

    iget-object v1, p0, Lg/a/c;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 219
    :cond_2
    iget-object v0, p0, Lg/a/c;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 220
    const/4 v0, 0x5

    iget-object v1, p0, Lg/a/c;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 222
    :cond_3
    iget-object v0, p0, Lg/a/c;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 223
    const/4 v0, 0x6

    iget-object v1, p0, Lg/a/c;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 225
    :cond_4
    iget-wide v0, p0, Lg/a/c;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_5

    .line 226
    const/4 v0, 0x7

    iget-wide v2, p0, Lg/a/c;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 228
    :cond_5
    iget-object v0, p0, Lg/a/c;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 229
    const/16 v0, 0x9

    iget-object v1, p0, Lg/a/c;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 231
    :cond_6
    iget-object v0, p0, Lg/a/c;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 232
    const/16 v0, 0xa

    iget-object v1, p0, Lg/a/c;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 234
    :cond_7
    iget-object v0, p0, Lg/a/c;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 235
    const/16 v0, 0xb

    iget-object v1, p0, Lg/a/c;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 237
    :cond_8
    iget-object v0, p0, Lg/a/c;->j:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 238
    const/16 v0, 0xc

    iget-object v1, p0, Lg/a/c;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 240
    :cond_9
    iget-object v0, p0, Lg/a/c;->k:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 241
    const/16 v0, 0xd

    iget-object v1, p0, Lg/a/c;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 243
    :cond_a
    iget-object v0, p0, Lg/a/c;->l:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 244
    const/16 v0, 0xf

    iget-object v1, p0, Lg/a/c;->l:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 246
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 247
    return-void
.end method
