.class public final Lg/a/d;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lg/a/f;

.field public b:[Ljava/lang/String;

.field public c:I

.field public d:J

.field public e:Lg/a/e;

.field public f:Z

.field public g:Z

.field public h:Ljava/lang/String;

.field public i:[Ljava/lang/String;

.field public j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, -0x1

    .line 854
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 855
    invoke-static {}, Lg/a/f;->a()[Lg/a/f;

    move-result-object v0

    iput-object v0, p0, Lg/a/d;->a:[Lg/a/f;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lg/a/d;->b:[Ljava/lang/String;

    iput v2, p0, Lg/a/d;->c:I

    const-wide/32 v0, -0x5265c00

    iput-wide v0, p0, Lg/a/d;->d:J

    const/4 v0, 0x0

    iput-object v0, p0, Lg/a/d;->e:Lg/a/e;

    iput-boolean v3, p0, Lg/a/d;->f:Z

    iput-boolean v3, p0, Lg/a/d;->g:Z

    const-string v0, ""

    iput-object v0, p0, Lg/a/d;->h:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lg/a/d;->i:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lg/a/d;->j:Ljava/lang/String;

    iput v2, p0, Lg/a/d;->cachedSize:I

    .line 856
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1008
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1009
    iget-object v2, p0, Lg/a/d;->a:[Lg/a/f;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lg/a/d;->a:[Lg/a/f;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 1010
    :goto_0
    iget-object v3, p0, Lg/a/d;->a:[Lg/a/f;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 1011
    iget-object v3, p0, Lg/a/d;->a:[Lg/a/f;

    aget-object v3, v3, v0

    .line 1012
    if-eqz v3, :cond_0

    .line 1013
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1010
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 1018
    :cond_2
    iget-object v2, p0, Lg/a/d;->b:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lg/a/d;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v1

    move v3, v1

    move v4, v1

    .line 1021
    :goto_1
    iget-object v5, p0, Lg/a/d;->b:[Ljava/lang/String;

    array-length v5, v5

    if-ge v2, v5, :cond_4

    .line 1022
    iget-object v5, p0, Lg/a/d;->b:[Ljava/lang/String;

    aget-object v5, v5, v2

    .line 1023
    if-eqz v5, :cond_3

    .line 1024
    add-int/lit8 v4, v4, 0x1

    .line 1025
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1021
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1029
    :cond_4
    add-int/2addr v0, v3

    .line 1030
    mul-int/lit8 v2, v4, 0x1

    add-int/2addr v0, v2

    .line 1032
    :cond_5
    iget v2, p0, Lg/a/d;->c:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_6

    .line 1033
    const/4 v2, 0x3

    iget v3, p0, Lg/a/d;->c:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1036
    :cond_6
    iget-wide v2, p0, Lg/a/d;->d:J

    const-wide/32 v4, -0x5265c00

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    .line 1037
    const/4 v2, 0x4

    iget-wide v4, p0, Lg/a/d;->d:J

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1040
    :cond_7
    iget-object v2, p0, Lg/a/d;->e:Lg/a/e;

    if-eqz v2, :cond_8

    .line 1041
    const/4 v2, 0x6

    iget-object v3, p0, Lg/a/d;->e:Lg/a/e;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1044
    :cond_8
    iget-boolean v2, p0, Lg/a/d;->f:Z

    if-eqz v2, :cond_9

    .line 1045
    const/4 v2, 0x7

    iget-boolean v3, p0, Lg/a/d;->f:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1048
    :cond_9
    iget-boolean v2, p0, Lg/a/d;->g:Z

    if-eqz v2, :cond_a

    .line 1049
    const/16 v2, 0x8

    iget-boolean v3, p0, Lg/a/d;->g:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1052
    :cond_a
    iget-object v2, p0, Lg/a/d;->h:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    .line 1053
    const/16 v2, 0x9

    iget-object v3, p0, Lg/a/d;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1056
    :cond_b
    iget-object v2, p0, Lg/a/d;->i:[Ljava/lang/String;

    if-eqz v2, :cond_e

    iget-object v2, p0, Lg/a/d;->i:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_e

    move v2, v1

    move v3, v1

    .line 1059
    :goto_2
    iget-object v4, p0, Lg/a/d;->i:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_d

    .line 1060
    iget-object v4, p0, Lg/a/d;->i:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 1061
    if-eqz v4, :cond_c

    .line 1062
    add-int/lit8 v3, v3, 0x1

    .line 1063
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1059
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1067
    :cond_d
    add-int/2addr v0, v2

    .line 1068
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 1070
    :cond_e
    iget-object v1, p0, Lg/a/d;->j:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    .line 1071
    const/16 v1, 0xb

    iget-object v2, p0, Lg/a/d;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1074
    :cond_f
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 875
    if-ne p1, p0, :cond_1

    .line 929
    :cond_0
    :goto_0
    return v0

    .line 878
    :cond_1
    instance-of v2, p1, Lg/a/d;

    if-nez v2, :cond_2

    move v0, v1

    .line 879
    goto :goto_0

    .line 881
    :cond_2
    check-cast p1, Lg/a/d;

    .line 882
    iget-object v2, p0, Lg/a/d;->a:[Lg/a/f;

    iget-object v3, p1, Lg/a/d;->a:[Lg/a/f;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 884
    goto :goto_0

    .line 886
    :cond_3
    iget-object v2, p0, Lg/a/d;->b:[Ljava/lang/String;

    iget-object v3, p1, Lg/a/d;->b:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 888
    goto :goto_0

    .line 890
    :cond_4
    iget v2, p0, Lg/a/d;->c:I

    iget v3, p1, Lg/a/d;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 891
    goto :goto_0

    .line 893
    :cond_5
    iget-wide v2, p0, Lg/a/d;->d:J

    iget-wide v4, p1, Lg/a/d;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 894
    goto :goto_0

    .line 896
    :cond_6
    iget-object v2, p0, Lg/a/d;->e:Lg/a/e;

    if-nez v2, :cond_7

    .line 897
    iget-object v2, p1, Lg/a/d;->e:Lg/a/e;

    if-eqz v2, :cond_8

    move v0, v1

    .line 898
    goto :goto_0

    .line 901
    :cond_7
    iget-object v2, p0, Lg/a/d;->e:Lg/a/e;

    iget-object v3, p1, Lg/a/d;->e:Lg/a/e;

    invoke-virtual {v2, v3}, Lg/a/e;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 902
    goto :goto_0

    .line 905
    :cond_8
    iget-boolean v2, p0, Lg/a/d;->f:Z

    iget-boolean v3, p1, Lg/a/d;->f:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 906
    goto :goto_0

    .line 908
    :cond_9
    iget-boolean v2, p0, Lg/a/d;->g:Z

    iget-boolean v3, p1, Lg/a/d;->g:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 909
    goto :goto_0

    .line 911
    :cond_a
    iget-object v2, p0, Lg/a/d;->h:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 912
    iget-object v2, p1, Lg/a/d;->h:Ljava/lang/String;

    if-eqz v2, :cond_c

    move v0, v1

    .line 913
    goto :goto_0

    .line 915
    :cond_b
    iget-object v2, p0, Lg/a/d;->h:Ljava/lang/String;

    iget-object v3, p1, Lg/a/d;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 916
    goto :goto_0

    .line 918
    :cond_c
    iget-object v2, p0, Lg/a/d;->i:[Ljava/lang/String;

    iget-object v3, p1, Lg/a/d;->i:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 920
    goto :goto_0

    .line 922
    :cond_d
    iget-object v2, p0, Lg/a/d;->j:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 923
    iget-object v2, p1, Lg/a/d;->j:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 924
    goto/16 :goto_0

    .line 926
    :cond_e
    iget-object v2, p0, Lg/a/d;->j:Ljava/lang/String;

    iget-object v3, p1, Lg/a/d;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 927
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 934
    iget-object v0, p0, Lg/a/d;->a:[Lg/a/f;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 937
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lg/a/d;->b:[Ljava/lang/String;

    invoke-static {v4}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 939
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lg/a/d;->c:I

    add-int/2addr v0, v4

    .line 940
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lg/a/d;->d:J

    iget-wide v6, p0, Lg/a/d;->d:J

    const/16 v8, 0x20

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 942
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lg/a/d;->e:Lg/a/e;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    .line 944
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lg/a/d;->f:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v4

    .line 945
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lg/a/d;->g:Z

    if-eqz v4, :cond_2

    :goto_2
    add-int/2addr v0, v2

    .line 946
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lg/a/d;->h:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 948
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lg/a/d;->i:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 950
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lg/a/d;->j:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 952
    return v0

    .line 942
    :cond_0
    iget-object v0, p0, Lg/a/d;->e:Lg/a/e;

    invoke-virtual {v0}, Lg/a/e;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v3

    .line 944
    goto :goto_1

    :cond_2
    move v2, v3

    .line 945
    goto :goto_2

    .line 946
    :cond_3
    iget-object v0, p0, Lg/a/d;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 950
    :cond_4
    iget-object v1, p0, Lg/a/d;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 388
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lg/a/d;->a:[Lg/a/f;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lg/a/f;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lg/a/d;->a:[Lg/a/f;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lg/a/f;

    invoke-direct {v3}, Lg/a/f;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lg/a/d;->a:[Lg/a/f;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lg/a/f;

    invoke-direct {v3}, Lg/a/f;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lg/a/d;->a:[Lg/a/f;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lg/a/d;->b:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lg/a/d;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lg/a/d;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lg/a/d;->b:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lg/a/d;->c:I

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lg/a/d;->d:J

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lg/a/d;->e:Lg/a/e;

    if-nez v0, :cond_7

    new-instance v0, Lg/a/e;

    invoke-direct {v0}, Lg/a/e;-><init>()V

    iput-object v0, p0, Lg/a/d;->e:Lg/a/e;

    :cond_7
    iget-object v0, p0, Lg/a/d;->e:Lg/a/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lg/a/d;->f:Z

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lg/a/d;->g:Z

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/d;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    const/16 v0, 0x52

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lg/a/d;->i:[Ljava/lang/String;

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v3, p0, Lg/a/d;->i:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_9
    iget-object v0, p0, Lg/a/d;->i:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_5

    :cond_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lg/a/d;->i:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/d;->j:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x32 -> :sswitch_5
        0x38 -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x52 -> :sswitch_9
        0x5a -> :sswitch_a
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 958
    iget-object v0, p0, Lg/a/d;->a:[Lg/a/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lg/a/d;->a:[Lg/a/f;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 959
    :goto_0
    iget-object v2, p0, Lg/a/d;->a:[Lg/a/f;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 960
    iget-object v2, p0, Lg/a/d;->a:[Lg/a/f;

    aget-object v2, v2, v0

    .line 961
    if-eqz v2, :cond_0

    .line 962
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 959
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 966
    :cond_1
    iget-object v0, p0, Lg/a/d;->b:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lg/a/d;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 967
    :goto_1
    iget-object v2, p0, Lg/a/d;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 968
    iget-object v2, p0, Lg/a/d;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 969
    if-eqz v2, :cond_2

    .line 970
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 967
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 974
    :cond_3
    iget v0, p0, Lg/a/d;->c:I

    const/4 v2, -0x1

    if-eq v0, v2, :cond_4

    .line 975
    const/4 v0, 0x3

    iget v2, p0, Lg/a/d;->c:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 977
    :cond_4
    iget-wide v2, p0, Lg/a/d;->d:J

    const-wide/32 v4, -0x5265c00

    cmp-long v0, v2, v4

    if-eqz v0, :cond_5

    .line 978
    const/4 v0, 0x4

    iget-wide v2, p0, Lg/a/d;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 980
    :cond_5
    iget-object v0, p0, Lg/a/d;->e:Lg/a/e;

    if-eqz v0, :cond_6

    .line 981
    const/4 v0, 0x6

    iget-object v2, p0, Lg/a/d;->e:Lg/a/e;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 983
    :cond_6
    iget-boolean v0, p0, Lg/a/d;->f:Z

    if-eqz v0, :cond_7

    .line 984
    const/4 v0, 0x7

    iget-boolean v2, p0, Lg/a/d;->f:Z

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 986
    :cond_7
    iget-boolean v0, p0, Lg/a/d;->g:Z

    if-eqz v0, :cond_8

    .line 987
    const/16 v0, 0x8

    iget-boolean v2, p0, Lg/a/d;->g:Z

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 989
    :cond_8
    iget-object v0, p0, Lg/a/d;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 990
    const/16 v0, 0x9

    iget-object v2, p0, Lg/a/d;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 992
    :cond_9
    iget-object v0, p0, Lg/a/d;->i:[Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lg/a/d;->i:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_b

    .line 993
    :goto_2
    iget-object v0, p0, Lg/a/d;->i:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_b

    .line 994
    iget-object v0, p0, Lg/a/d;->i:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 995
    if-eqz v0, :cond_a

    .line 996
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 993
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1000
    :cond_b
    iget-object v0, p0, Lg/a/d;->j:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 1001
    const/16 v0, 0xb

    iget-object v1, p0, Lg/a/d;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1003
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1004
    return-void
.end method
