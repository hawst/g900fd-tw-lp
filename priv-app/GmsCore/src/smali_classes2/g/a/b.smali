.class public final Lg/a/b;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lg/a/c;

.field public b:Lg/a/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1213
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1214
    iput-object v0, p0, Lg/a/b;->a:Lg/a/c;

    iput-object v0, p0, Lg/a/b;->b:Lg/a/d;

    const/4 v0, -0x1

    iput v0, p0, Lg/a/b;->cachedSize:I

    .line 1215
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1278
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1279
    iget-object v1, p0, Lg/a/b;->a:Lg/a/c;

    if-eqz v1, :cond_0

    .line 1280
    const/4 v1, 0x1

    iget-object v2, p0, Lg/a/b;->a:Lg/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1283
    :cond_0
    iget-object v1, p0, Lg/a/b;->b:Lg/a/d;

    if-eqz v1, :cond_1

    .line 1284
    const/4 v1, 0x2

    iget-object v2, p0, Lg/a/b;->b:Lg/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1287
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1226
    if-ne p1, p0, :cond_1

    .line 1251
    :cond_0
    :goto_0
    return v0

    .line 1229
    :cond_1
    instance-of v2, p1, Lg/a/b;

    if-nez v2, :cond_2

    move v0, v1

    .line 1230
    goto :goto_0

    .line 1232
    :cond_2
    check-cast p1, Lg/a/b;

    .line 1233
    iget-object v2, p0, Lg/a/b;->a:Lg/a/c;

    if-nez v2, :cond_3

    .line 1234
    iget-object v2, p1, Lg/a/b;->a:Lg/a/c;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1235
    goto :goto_0

    .line 1238
    :cond_3
    iget-object v2, p0, Lg/a/b;->a:Lg/a/c;

    iget-object v3, p1, Lg/a/b;->a:Lg/a/c;

    invoke-virtual {v2, v3}, Lg/a/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1239
    goto :goto_0

    .line 1242
    :cond_4
    iget-object v2, p0, Lg/a/b;->b:Lg/a/d;

    if-nez v2, :cond_5

    .line 1243
    iget-object v2, p1, Lg/a/b;->b:Lg/a/d;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1244
    goto :goto_0

    .line 1247
    :cond_5
    iget-object v2, p0, Lg/a/b;->b:Lg/a/d;

    iget-object v3, p1, Lg/a/b;->b:Lg/a/d;

    invoke-virtual {v2, v3}, Lg/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1248
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1256
    iget-object v0, p0, Lg/a/b;->a:Lg/a/c;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1259
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lg/a/b;->b:Lg/a/d;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1261
    return v0

    .line 1256
    :cond_0
    iget-object v0, p0, Lg/a/b;->a:Lg/a/c;

    invoke-virtual {v0}, Lg/a/c;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1259
    :cond_1
    iget-object v1, p0, Lg/a/b;->b:Lg/a/d;

    invoke-virtual {v1}, Lg/a/d;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 9
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lg/a/b;->a:Lg/a/c;

    if-nez v0, :cond_1

    new-instance v0, Lg/a/c;

    invoke-direct {v0}, Lg/a/c;-><init>()V

    iput-object v0, p0, Lg/a/b;->a:Lg/a/c;

    :cond_1
    iget-object v0, p0, Lg/a/b;->a:Lg/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lg/a/b;->b:Lg/a/d;

    if-nez v0, :cond_2

    new-instance v0, Lg/a/d;

    invoke-direct {v0}, Lg/a/d;-><init>()V

    iput-object v0, p0, Lg/a/b;->b:Lg/a/d;

    :cond_2
    iget-object v0, p0, Lg/a/b;->b:Lg/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1267
    iget-object v0, p0, Lg/a/b;->a:Lg/a/c;

    if-eqz v0, :cond_0

    .line 1268
    const/4 v0, 0x1

    iget-object v1, p0, Lg/a/b;->a:Lg/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1270
    :cond_0
    iget-object v0, p0, Lg/a/b;->b:Lg/a/d;

    if-eqz v0, :cond_1

    .line 1271
    const/4 v0, 0x2

    iget-object v1, p0, Lg/a/b;->b:Lg/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1273
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1274
    return-void
.end method
