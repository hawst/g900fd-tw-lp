.class public final Lg/a/f;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile f:[Lg/a/f;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:J

.field public d:J

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, -0x1

    .line 423
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 424
    const-string v0, ""

    iput-object v0, p0, Lg/a/f;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lg/a/f;->b:Ljava/lang/String;

    iput-wide v2, p0, Lg/a/f;->c:J

    iput-wide v2, p0, Lg/a/f;->d:J

    const-string v0, ""

    iput-object v0, p0, Lg/a/f;->e:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lg/a/f;->cachedSize:I

    .line 425
    return-void
.end method

.method public static a()[Lg/a/f;
    .locals 2

    .prologue
    .line 397
    sget-object v0, Lg/a/f;->f:[Lg/a/f;

    if-nez v0, :cond_1

    .line 398
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 400
    :try_start_0
    sget-object v0, Lg/a/f;->f:[Lg/a/f;

    if-nez v0, :cond_0

    .line 401
    const/4 v0, 0x0

    new-array v0, v0, [Lg/a/f;

    sput-object v0, Lg/a/f;->f:[Lg/a/f;

    .line 403
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 405
    :cond_1
    sget-object v0, Lg/a/f;->f:[Lg/a/f;

    return-object v0

    .line 403
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 515
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 516
    iget-object v1, p0, Lg/a/f;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 517
    const/4 v1, 0x1

    iget-object v2, p0, Lg/a/f;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 520
    :cond_0
    iget-object v1, p0, Lg/a/f;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 521
    const/4 v1, 0x2

    iget-object v2, p0, Lg/a/f;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 524
    :cond_1
    iget-wide v2, p0, Lg/a/f;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 525
    const/4 v1, 0x3

    iget-wide v2, p0, Lg/a/f;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 528
    :cond_2
    iget-wide v2, p0, Lg/a/f;->d:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    .line 529
    const/4 v1, 0x4

    iget-wide v2, p0, Lg/a/f;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 532
    :cond_3
    iget-object v1, p0, Lg/a/f;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 533
    const/4 v1, 0x5

    iget-object v2, p0, Lg/a/f;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 536
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 439
    if-ne p1, p0, :cond_1

    .line 473
    :cond_0
    :goto_0
    return v0

    .line 442
    :cond_1
    instance-of v2, p1, Lg/a/f;

    if-nez v2, :cond_2

    move v0, v1

    .line 443
    goto :goto_0

    .line 445
    :cond_2
    check-cast p1, Lg/a/f;

    .line 446
    iget-object v2, p0, Lg/a/f;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 447
    iget-object v2, p1, Lg/a/f;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 448
    goto :goto_0

    .line 450
    :cond_3
    iget-object v2, p0, Lg/a/f;->a:Ljava/lang/String;

    iget-object v3, p1, Lg/a/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 451
    goto :goto_0

    .line 453
    :cond_4
    iget-object v2, p0, Lg/a/f;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 454
    iget-object v2, p1, Lg/a/f;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 455
    goto :goto_0

    .line 457
    :cond_5
    iget-object v2, p0, Lg/a/f;->b:Ljava/lang/String;

    iget-object v3, p1, Lg/a/f;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 458
    goto :goto_0

    .line 460
    :cond_6
    iget-wide v2, p0, Lg/a/f;->c:J

    iget-wide v4, p1, Lg/a/f;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    .line 461
    goto :goto_0

    .line 463
    :cond_7
    iget-wide v2, p0, Lg/a/f;->d:J

    iget-wide v4, p1, Lg/a/f;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    move v0, v1

    .line 464
    goto :goto_0

    .line 466
    :cond_8
    iget-object v2, p0, Lg/a/f;->e:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 467
    iget-object v2, p1, Lg/a/f;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 468
    goto :goto_0

    .line 470
    :cond_9
    iget-object v2, p0, Lg/a/f;->e:Ljava/lang/String;

    iget-object v3, p1, Lg/a/f;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 471
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 478
    iget-object v0, p0, Lg/a/f;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 481
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lg/a/f;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 483
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lg/a/f;->c:J

    iget-wide v4, p0, Lg/a/f;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 485
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lg/a/f;->d:J

    iget-wide v4, p0, Lg/a/f;->d:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 487
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lg/a/f;->e:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 489
    return v0

    .line 478
    :cond_0
    iget-object v0, p0, Lg/a/f;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 481
    :cond_1
    iget-object v0, p0, Lg/a/f;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 487
    :cond_2
    iget-object v1, p0, Lg/a/f;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 391
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/f;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/f;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lg/a/f;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lg/a/f;->d:J

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lg/a/f;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 495
    iget-object v0, p0, Lg/a/f;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 496
    const/4 v0, 0x1

    iget-object v1, p0, Lg/a/f;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 498
    :cond_0
    iget-object v0, p0, Lg/a/f;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 499
    const/4 v0, 0x2

    iget-object v1, p0, Lg/a/f;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 501
    :cond_1
    iget-wide v0, p0, Lg/a/f;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 502
    const/4 v0, 0x3

    iget-wide v2, p0, Lg/a/f;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 504
    :cond_2
    iget-wide v0, p0, Lg/a/f;->d:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 505
    const/4 v0, 0x4

    iget-wide v2, p0, Lg/a/f;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 507
    :cond_3
    iget-object v0, p0, Lg/a/f;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 508
    const/4 v0, 0x5

    iget-object v1, p0, Lg/a/f;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 510
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 511
    return-void
.end method
