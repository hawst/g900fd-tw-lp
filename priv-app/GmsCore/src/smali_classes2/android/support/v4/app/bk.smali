.class public final Landroid/support/v4/app/bk;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field A:Landroid/app/Notification;

.field public B:Landroid/app/Notification;

.field public C:Ljava/util/ArrayList;

.field a:Landroid/content/Context;

.field b:Ljava/lang/CharSequence;

.field c:Ljava/lang/CharSequence;

.field public d:Landroid/app/PendingIntent;

.field public e:Landroid/app/PendingIntent;

.field f:Landroid/widget/RemoteViews;

.field public g:Landroid/graphics/Bitmap;

.field h:Ljava/lang/CharSequence;

.field i:I

.field public j:I

.field k:Z

.field public l:Z

.field m:Landroid/support/v4/app/bv;

.field n:Ljava/lang/CharSequence;

.field o:I

.field p:I

.field q:Z

.field r:Ljava/lang/String;

.field s:Z

.field t:Ljava/lang/String;

.field u:Ljava/util/ArrayList;

.field v:Z

.field public w:Ljava/lang/String;

.field x:Landroid/os/Bundle;

.field public y:I

.field public z:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 877
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 845
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v4/app/bk;->k:Z

    .line 855
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bk;->u:Ljava/util/ArrayList;

    .line 856
    iput-boolean v4, p0, Landroid/support/v4/app/bk;->v:Z

    .line 859
    iput v4, p0, Landroid/support/v4/app/bk;->y:I

    .line 860
    iput v4, p0, Landroid/support/v4/app/bk;->z:I

    .line 863
    new-instance v0, Landroid/app/Notification;

    invoke-direct {v0}, Landroid/app/Notification;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bk;->B:Landroid/app/Notification;

    .line 878
    iput-object p1, p0, Landroid/support/v4/app/bk;->a:Landroid/content/Context;

    .line 881
    iget-object v0, p0, Landroid/support/v4/app/bk;->B:Landroid/app/Notification;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Landroid/app/Notification;->when:J

    .line 882
    iget-object v0, p0, Landroid/support/v4/app/bk;->B:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 883
    iput v4, p0, Landroid/support/v4/app/bk;->j:I

    .line 884
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v4/app/bk;->C:Ljava/util/ArrayList;

    .line 885
    return-void
.end method

.method protected static f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;
    .locals 2

    .prologue
    const/16 v1, 0x1400

    .line 1507
    if-nez p0, :cond_1

    .line 1511
    :cond_0
    :goto_0
    return-object p0

    .line 1508
    :cond_1
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-le v0, v1, :cond_0

    .line 1509
    const/4 v0, 0x0

    invoke-interface {p0, v0, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object p0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/support/v4/app/bk;
    .locals 2

    .prologue
    .line 1187
    const/16 v0, 0x10

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/support/v4/app/bk;->a(IZ)V

    .line 1188
    return-object p0
.end method

.method public final a(I)Landroid/support/v4/app/bk;
    .locals 1

    .prologue
    .line 929
    iget-object v0, p0, Landroid/support/v4/app/bk;->B:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->icon:I

    .line 930
    return-object p0
.end method

.method public final a(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;
    .locals 2

    .prologue
    .line 1403
    iget-object v0, p0, Landroid/support/v4/app/bk;->u:Ljava/util/ArrayList;

    new-instance v1, Landroid/support/v4/app/bg;

    invoke-direct {v1, p1, p2, p3}, Landroid/support/v4/app/bg;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1404
    return-object p0
.end method

.method public final a(J)Landroid/support/v4/app/bk;
    .locals 1

    .prologue
    .line 892
    iget-object v0, p0, Landroid/support/v4/app/bk;->B:Landroid/app/Notification;

    iput-wide p1, v0, Landroid/app/Notification;->when:J

    .line 893
    return-object p0
.end method

.method public final a(Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;
    .locals 1

    .prologue
    .line 1037
    iget-object v0, p0, Landroid/support/v4/app/bk;->B:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->deleteIntent:Landroid/app/PendingIntent;

    .line 1038
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)Landroid/support/v4/app/bk;
    .locals 2

    .prologue
    .line 1102
    iget-object v0, p0, Landroid/support/v4/app/bk;->B:Landroid/app/Notification;

    iput-object p1, v0, Landroid/app/Notification;->sound:Landroid/net/Uri;

    .line 1103
    iget-object v0, p0, Landroid/support/v4/app/bk;->B:Landroid/app/Notification;

    const/4 v1, -0x1

    iput v1, v0, Landroid/app/Notification;->audioStreamType:I

    .line 1104
    return-object p0
.end method

.method public final a(Landroid/support/v4/app/bv;)Landroid/support/v4/app/bk;
    .locals 2

    .prologue
    .line 1435
    iget-object v0, p0, Landroid/support/v4/app/bk;->m:Landroid/support/v4/app/bv;

    if-eq v0, p1, :cond_0

    .line 1436
    iput-object p1, p0, Landroid/support/v4/app/bk;->m:Landroid/support/v4/app/bv;

    .line 1437
    iget-object v0, p0, Landroid/support/v4/app/bk;->m:Landroid/support/v4/app/bv;

    if-eqz v0, :cond_0

    .line 1438
    iget-object v0, p0, Landroid/support/v4/app/bk;->m:Landroid/support/v4/app/bv;

    iget-object v1, v0, Landroid/support/v4/app/bv;->d:Landroid/support/v4/app/bk;

    if-eq v1, p0, :cond_0

    iput-object p0, v0, Landroid/support/v4/app/bv;->d:Landroid/support/v4/app/bk;

    iget-object v1, v0, Landroid/support/v4/app/bv;->d:Landroid/support/v4/app/bk;

    if-eqz v1, :cond_0

    iget-object v1, v0, Landroid/support/v4/app/bv;->d:Landroid/support/v4/app/bk;

    invoke-virtual {v1, v0}, Landroid/support/v4/app/bk;->a(Landroid/support/v4/app/bv;)Landroid/support/v4/app/bk;

    .line 1441
    :cond_0
    return-object p0
.end method

.method public final a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;
    .locals 1

    .prologue
    .line 953
    invoke-static {p1}, Landroid/support/v4/app/bk;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/bk;->b:Ljava/lang/CharSequence;

    .line 954
    return-object p0
.end method

.method public final a(IZ)V
    .locals 3

    .prologue
    .line 1233
    if-eqz p2, :cond_0

    .line 1234
    iget-object v0, p0, Landroid/support/v4/app/bk;->B:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/2addr v1, p1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 1238
    :goto_0
    return-void

    .line 1236
    :cond_0
    iget-object v0, p0, Landroid/support/v4/app/bk;->B:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    xor-int/lit8 v2, p1, -0x1

    and-int/2addr v1, v2

    iput v1, v0, Landroid/app/Notification;->flags:I

    goto :goto_0
.end method

.method public final b()Landroid/app/Notification;
    .locals 1

    .prologue
    .line 1503
    invoke-static {}, Landroid/support/v4/app/bf;->a()Landroid/support/v4/app/bm;

    move-result-object v0

    invoke-interface {v0, p0}, Landroid/support/v4/app/bm;->a(Landroid/support/v4/app/bk;)Landroid/app/Notification;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Landroid/support/v4/app/bk;
    .locals 2

    .prologue
    .line 1225
    iget-object v0, p0, Landroid/support/v4/app/bk;->B:Landroid/app/Notification;

    iput p1, v0, Landroid/app/Notification;->defaults:I

    .line 1226
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_0

    .line 1227
    iget-object v0, p0, Landroid/support/v4/app/bk;->B:Landroid/app/Notification;

    iget v1, v0, Landroid/app/Notification;->flags:I

    or-int/lit8 v1, v1, 0x1

    iput v1, v0, Landroid/app/Notification;->flags:I

    .line 1229
    :cond_0
    return-object p0
.end method

.method public final b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;
    .locals 1

    .prologue
    .line 961
    invoke-static {p1}, Landroid/support/v4/app/bk;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/bk;->c:Ljava/lang/CharSequence;

    .line 962
    return-object p0
.end method

.method public final c(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;
    .locals 1

    .prologue
    .line 975
    invoke-static {p1}, Landroid/support/v4/app/bk;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/bk;->n:Ljava/lang/CharSequence;

    .line 976
    return-object p0
.end method

.method public final d(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;
    .locals 1

    .prologue
    .line 993
    invoke-static {p1}, Landroid/support/v4/app/bk;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/bk;->h:Ljava/lang/CharSequence;

    .line 994
    return-object p0
.end method

.method public final e(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;
    .locals 2

    .prologue
    .line 1070
    iget-object v0, p0, Landroid/support/v4/app/bk;->B:Landroid/app/Notification;

    invoke-static {p1}, Landroid/support/v4/app/bk;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, v0, Landroid/app/Notification;->tickerText:Ljava/lang/CharSequence;

    .line 1071
    return-object p0
.end method
