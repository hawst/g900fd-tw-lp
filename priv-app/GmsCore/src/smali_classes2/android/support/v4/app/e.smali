.class final Landroid/support/v4/app/e;
.super Landroid/app/SharedElementCallback;
.source "SourceFile"


# instance fields
.field private a:Landroid/support/v4/app/d;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/d;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Landroid/app/SharedElementCallback;-><init>()V

    .line 84
    iput-object p1, p0, Landroid/support/v4/app/e;->a:Landroid/support/v4/app/d;

    .line 85
    return-void
.end method


# virtual methods
.method public final onCaptureSharedElementSnapshot(Landroid/view/View;Landroid/graphics/Matrix;Landroid/graphics/RectF;)Landroid/os/Parcelable;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Landroid/support/v4/app/e;->a:Landroid/support/v4/app/d;

    invoke-virtual {v0, p1, p2, p3}, Landroid/support/v4/app/d;->a(Landroid/view/View;Landroid/graphics/Matrix;Landroid/graphics/RectF;)Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public final onCreateSnapshotView(Landroid/content/Context;Landroid/os/Parcelable;)Landroid/view/View;
    .locals 1

    .prologue
    .line 121
    iget-object v0, p0, Landroid/support/v4/app/e;->a:Landroid/support/v4/app/d;

    invoke-virtual {v0, p1, p2}, Landroid/support/v4/app/d;->a(Landroid/content/Context;Landroid/os/Parcelable;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final onMapSharedElements(Ljava/util/List;Ljava/util/Map;)V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Landroid/support/v4/app/e;->a:Landroid/support/v4/app/d;

    invoke-virtual {v0}, Landroid/support/v4/app/d;->d()V

    .line 109
    return-void
.end method

.method public final onRejectSharedElements(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Landroid/support/v4/app/e;->a:Landroid/support/v4/app/d;

    invoke-virtual {v0}, Landroid/support/v4/app/d;->c()V

    .line 104
    return-void
.end method

.method public final onSharedElementEnd(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Landroid/support/v4/app/e;->a:Landroid/support/v4/app/d;

    invoke-virtual {v0}, Landroid/support/v4/app/d;->b()V

    .line 99
    return-void
.end method

.method public final onSharedElementStart(Ljava/util/List;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Landroid/support/v4/app/e;->a:Landroid/support/v4/app/d;

    invoke-virtual {v0}, Landroid/support/v4/app/d;->a()V

    .line 92
    return-void
.end method
