.class public final Landroid/support/v4/app/bg;
.super Landroid/support/v4/app/cb;
.source "SourceFile"


# static fields
.field public static final d:Landroid/support/v4/app/cc;


# instance fields
.field public a:I

.field public b:Ljava/lang/CharSequence;

.field public c:Landroid/app/PendingIntent;

.field private final e:Landroid/os/Bundle;

.field private final f:[Landroid/support/v4/app/ci;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 2104
    new-instance v0, Landroid/support/v4/app/bh;

    invoke-direct {v0}, Landroid/support/v4/app/bh;-><init>()V

    sput-object v0, Landroid/support/v4/app/bg;->d:Landroid/support/v4/app/cc;

    return-void
.end method

.method public constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;)V
    .locals 1

    .prologue
    .line 1762
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p0, p1, p2, p3, v0}, Landroid/support/v4/app/bg;-><init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)V

    .line 1763
    return-void
.end method

.method private constructor <init>(ILjava/lang/CharSequence;Landroid/app/PendingIntent;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1766
    invoke-direct {p0}, Landroid/support/v4/app/cb;-><init>()V

    .line 1767
    iput p1, p0, Landroid/support/v4/app/bg;->a:I

    .line 1768
    invoke-static {p2}, Landroid/support/v4/app/bk;->f(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v4/app/bg;->b:Ljava/lang/CharSequence;

    .line 1769
    iput-object p3, p0, Landroid/support/v4/app/bg;->c:Landroid/app/PendingIntent;

    .line 1770
    if-eqz p4, :cond_0

    :goto_0
    iput-object p4, p0, Landroid/support/v4/app/bg;->e:Landroid/os/Bundle;

    .line 1771
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v4/app/bg;->f:[Landroid/support/v4/app/ci;

    .line 1772
    return-void

    .line 1770
    :cond_0
    new-instance p4, Landroid/os/Bundle;

    invoke-direct {p4}, Landroid/os/Bundle;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method protected final a()I
    .locals 1

    .prologue
    .line 1776
    iget v0, p0, Landroid/support/v4/app/bg;->a:I

    return v0
.end method

.method protected final b()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 1781
    iget-object v0, p0, Landroid/support/v4/app/bg;->b:Ljava/lang/CharSequence;

    return-object v0
.end method

.method protected final c()Landroid/app/PendingIntent;
    .locals 1

    .prologue
    .line 1786
    iget-object v0, p0, Landroid/support/v4/app/bg;->c:Landroid/app/PendingIntent;

    return-object v0
.end method

.method public final d()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 1793
    iget-object v0, p0, Landroid/support/v4/app/bg;->e:Landroid/os/Bundle;

    return-object v0
.end method

.method public final bridge synthetic e()[Landroid/support/v4/app/cq;
    .locals 1

    .prologue
    .line 1743
    iget-object v0, p0, Landroid/support/v4/app/bg;->f:[Landroid/support/v4/app/ci;

    return-object v0
.end method
