.class public final Le/a/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/nio/channels/WritableByteChannel;


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private b:Ljava/nio/ByteBuffer;

.field private c:I

.field private d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Le/a/a/a;->a:Ljava/util/ArrayList;

    return-void
.end method


# virtual methods
.method public final a()[B
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 96
    iget v1, p0, Le/a/a/a;->c:I

    new-array v3, v1, [B

    .line 97
    iget-object v1, p0, Le/a/a/a;->b:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_1

    .line 98
    iget-object v0, p0, Le/a/a/a;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 99
    iget-object v0, p0, Le/a/a/a;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->get([B)Ljava/nio/ByteBuffer;

    .line 111
    :cond_0
    return-object v3

    .line 101
    :cond_1
    iget-object v1, p0, Le/a/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v0

    move v2, v0

    .line 103
    :goto_0
    if-ge v1, v4, :cond_0

    .line 104
    iget-object v0, p0, Le/a/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/nio/ByteBuffer;

    .line 105
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v5

    .line 106
    invoke-virtual {v0, v3, v2, v5}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 107
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 108
    add-int/2addr v2, v5

    .line 103
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 116
    const/4 v0, 0x1

    iput-boolean v0, p0, Le/a/a/a;->d:Z

    .line 117
    return-void
.end method

.method public final isOpen()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Le/a/a/a;->d:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final write(Ljava/nio/ByteBuffer;)I
    .locals 3

    .prologue
    .line 41
    iget-boolean v0, p0, Le/a/a/a;->d:Z

    if-eqz v0, :cond_0

    .line 42
    new-instance v0, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v0}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v0

    .line 45
    :cond_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    .line 46
    iget v1, p0, Le/a/a/a;->c:I

    add-int/2addr v1, v0

    iput v1, p0, Le/a/a/a;->c:I

    .line 48
    iget-object v1, p0, Le/a/a/a;->b:Ljava/nio/ByteBuffer;

    if-eqz v1, :cond_2

    .line 49
    iget-object v1, p0, Le/a/a/a;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    if-gt v0, v1, :cond_1

    .line 50
    iget-object v1, p0, Le/a/a/a;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 66
    :goto_0
    return v0

    .line 56
    :cond_1
    iget-object v1, p0, Le/a/a/a;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 57
    iget-object v1, p0, Le/a/a/a;->a:Ljava/util/ArrayList;

    iget-object v2, p0, Le/a/a/a;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 58
    const/4 v1, 0x0

    iput-object v1, p0, Le/a/a/a;->b:Ljava/nio/ByteBuffer;

    .line 63
    :cond_2
    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 64
    invoke-virtual {v1, p1}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    .line 65
    iget-object v2, p0, Le/a/a/a;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method
