.class public Le/a/a/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lorg/json/JSONObject;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    iput-object v0, p0, Le/a/a/i;->a:Lorg/json/JSONObject;

    .line 21
    const-string v0, "ENABLE_LEGACY_MODE"

    invoke-direct {p0, v0, v1}, Le/a/a/i;->a(Ljava/lang/String;Z)Le/a/a/i;

    .line 22
    const-string v0, "ENABLE_QUIC"

    invoke-direct {p0, v0, v1}, Le/a/a/i;->a(Ljava/lang/String;Z)Le/a/a/i;

    .line 23
    const-string v0, "ENABLE_SPDY"

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Le/a/a/i;->a(Ljava/lang/String;Z)Le/a/a/i;

    .line 24
    sget-object v0, Le/a/a/k;->b:Le/a/a/k;

    const-wide/32 v2, 0x19000

    invoke-virtual {p0, v0, v2, v3}, Le/a/a/i;->a(Le/a/a/k;J)Le/a/a/i;

    .line 25
    return-void
.end method

.method private a(Ljava/lang/String;J)Le/a/a/i;
    .locals 2

    .prologue
    .line 159
    :try_start_0
    iget-object v0, p0, Le/a/a/i;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 163
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Le/a/a/i;
    .locals 1

    .prologue
    .line 172
    :try_start_0
    iget-object v0, p0, Le/a/a/i;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Z)Le/a/a/i;
    .locals 1

    .prologue
    .line 146
    :try_start_0
    iget-object v0, p0, Le/a/a/i;->a:Lorg/json/JSONObject;

    invoke-virtual {v0, p1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 150
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Le/a/a/k;J)Le/a/a/i;
    .locals 2

    .prologue
    .line 79
    sget-object v0, Le/a/a/j;->a:[I

    invoke-virtual {p1}, Le/a/a/k;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 92
    :goto_0
    return-object p0

    .line 81
    :pswitch_0
    const-string v0, "HTTP_CACHE"

    const-string v1, "HTTP_CACHE_DISABLED"

    invoke-direct {p0, v0, v1}, Le/a/a/i;->a(Ljava/lang/String;Ljava/lang/String;)Le/a/a/i;

    move-result-object p0

    goto :goto_0

    .line 84
    :pswitch_1
    const-string v0, "HTTP_CACHE_MAX_SIZE"

    invoke-direct {p0, v0, p2, p3}, Le/a/a/i;->a(Ljava/lang/String;J)Le/a/a/i;

    .line 85
    const-string v0, "HTTP_CACHE"

    const-string v1, "HTTP_CACHE_DISK"

    invoke-direct {p0, v0, v1}, Le/a/a/i;->a(Ljava/lang/String;Ljava/lang/String;)Le/a/a/i;

    move-result-object p0

    goto :goto_0

    .line 88
    :pswitch_2
    const-string v0, "HTTP_CACHE_MAX_SIZE"

    invoke-direct {p0, v0, p2, p3}, Le/a/a/i;->a(Ljava/lang/String;J)Le/a/a/i;

    .line 89
    const-string v0, "HTTP_CACHE"

    const-string v1, "HTTP_CACHE_MEMORY"

    invoke-direct {p0, v0, v1}, Le/a/a/i;->a(Ljava/lang/String;Ljava/lang/String;)Le/a/a/i;

    move-result-object p0

    goto :goto_0

    .line 79
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method final a()Z
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Le/a/a/i;->a:Lorg/json/JSONObject;

    const-string v1, "ENABLE_LEGACY_MODE"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b()Le/a/a/i;
    .locals 2

    .prologue
    .line 57
    const-string v0, "ENABLE_QUIC"

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Le/a/a/i;->a(Ljava/lang/String;Z)Le/a/a/i;

    move-result-object v0

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 137
    iget-object v0, p0, Le/a/a/i;->a:Lorg/json/JSONObject;

    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
