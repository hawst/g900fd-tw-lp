.class public abstract Le/a/a/h;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Le/a/a/i;)Le/a/a/h;
    .locals 4

    .prologue
    .line 26
    const/4 v0, 0x0

    .line 27
    invoke-virtual {p1}, Le/a/a/i;->a()Z

    move-result v1

    if-nez v1, :cond_0

    .line 28
    invoke-static {p0, p1}, Le/a/a/h;->b(Landroid/content/Context;Le/a/a/i;)Le/a/a/h;

    move-result-object v0

    .line 30
    :cond_0
    if-nez v0, :cond_1

    .line 32
    new-instance v0, Le/a/a/f;

    invoke-direct {v0, p0}, Le/a/a/f;-><init>(Landroid/content/Context;)V

    .line 34
    :cond_1
    const-string v1, "HttpUrlRequestFactory"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Using network stack: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Le/a/a/h;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    return-object v0
.end method

.method private static b(Landroid/content/Context;Le/a/a/i;)Le/a/a/h;
    .locals 5

    .prologue
    .line 64
    const/4 v1, 0x0

    .line 66
    :try_start_0
    const-class v0, Le/a/a/h;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    const-string v2, "org.chromium.net.ChromiumUrlRequestFactory"

    invoke-virtual {v0, v2}, Ljava/lang/ClassLoader;->loadClass(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    const-class v2, Le/a/a/h;

    invoke-virtual {v0, v2}, Ljava/lang/Class;->asSubclass(Ljava/lang/Class;)Ljava/lang/Class;

    move-result-object v0

    .line 70
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Le/a/a/i;

    aput-object v4, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v0

    .line 73
    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v2}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Le/a/a/h;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 86
    :goto_0
    return-object v0

    .line 85
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 80
    :catch_1
    move-exception v0

    .line 81
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Cannot instantiate: org.chromium.net.ChromiumUrlRequestFactory"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public abstract a(Ljava/lang/String;ILjava/util/Map;Le/a/a/l;)Le/a/a/g;
.end method

.method public abstract a(Ljava/lang/String;Ljava/util/Map;Ljava/nio/channels/WritableByteChannel;Le/a/a/l;)Le/a/a/g;
.end method

.method public abstract a()Ljava/lang/String;
.end method
