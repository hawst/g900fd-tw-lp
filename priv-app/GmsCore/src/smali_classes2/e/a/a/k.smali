.class public final enum Le/a/a/k;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Le/a/a/k;

.field public static final enum b:Le/a/a/k;

.field public static final enum c:Le/a/a/k;

.field private static final synthetic d:[Le/a/a/k;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 76
    new-instance v0, Le/a/a/k;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v2}, Le/a/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Le/a/a/k;->a:Le/a/a/k;

    new-instance v0, Le/a/a/k;

    const-string v1, "IN_MEMORY"

    invoke-direct {v0, v1, v3}, Le/a/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Le/a/a/k;->b:Le/a/a/k;

    new-instance v0, Le/a/a/k;

    const-string v1, "DISK"

    invoke-direct {v0, v1, v4}, Le/a/a/k;-><init>(Ljava/lang/String;I)V

    sput-object v0, Le/a/a/k;->c:Le/a/a/k;

    const/4 v0, 0x3

    new-array v0, v0, [Le/a/a/k;

    sget-object v1, Le/a/a/k;->a:Le/a/a/k;

    aput-object v1, v0, v2

    sget-object v1, Le/a/a/k;->b:Le/a/a/k;

    aput-object v1, v0, v3

    sget-object v1, Le/a/a/k;->c:Le/a/a/k;

    aput-object v1, v0, v4

    sput-object v0, Le/a/a/k;->d:[Le/a/a/k;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 76
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Le/a/a/k;
    .locals 1

    .prologue
    .line 76
    const-class v0, Le/a/a/k;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Le/a/a/k;

    return-object v0
.end method

.method public static values()[Le/a/a/k;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Le/a/a/k;->d:[Le/a/a/k;

    invoke-virtual {v0}, [Le/a/a/k;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Le/a/a/k;

    return-object v0
.end method
