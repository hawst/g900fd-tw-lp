.class final Le/a/a/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Le/a/a/g;


# static fields
.field private static final A:Ljava/lang/Object;

.field private static z:Ljava/util/concurrent/ExecutorService;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/Map;

.field private final d:Ljava/nio/channels/WritableByteChannel;

.field private final e:Le/a/a/l;

.field private f:Ljava/io/IOException;

.field private g:Ljava/net/HttpURLConnection;

.field private h:J

.field private i:I

.field private j:I

.field private k:J

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:J

.field private p:Ljava/lang/String;

.field private q:[B

.field private r:Ljava/nio/channels/ReadableByteChannel;

.field private s:Ljava/lang/String;

.field private t:I

.field private u:Z

.field private v:Z

.field private w:Ljava/lang/String;

.field private x:Ljava/io/InputStream;

.field private final y:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Le/a/a/b;->A:Ljava/lang/Object;

    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Le/a/a/l;)V
    .locals 6

    .prologue
    .line 99
    new-instance v4, Le/a/a/a;

    invoke-direct {v4}, Le/a/a/a;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Le/a/a/b;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Ljava/nio/channels/WritableByteChannel;Le/a/a/l;)V

    .line 101
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;Ljava/nio/channels/WritableByteChannel;Le/a/a/l;)V
    .locals 2

    .prologue
    .line 105
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 106
    if-nez p1, :cond_0

    .line 107
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "Context is required"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 109
    :cond_0
    if-nez p2, :cond_1

    .line 110
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "URL is required"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 112
    :cond_1
    iput-object p1, p0, Le/a/a/b;->a:Landroid/content/Context;

    .line 113
    iput-object p2, p0, Le/a/a/b;->b:Ljava/lang/String;

    .line 114
    iput-object p3, p0, Le/a/a/b;->c:Ljava/util/Map;

    .line 115
    iput-object p4, p0, Le/a/a/b;->d:Ljava/nio/channels/WritableByteChannel;

    .line 116
    iput-object p5, p0, Le/a/a/b;->e:Le/a/a/l;

    .line 117
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Le/a/a/b;->y:Ljava/lang/Object;

    .line 118
    return-void
.end method

.method static synthetic a(Le/a/a/b;)V
    .locals 11

    .prologue
    const/4 v3, 0x0

    const/4 v10, -0x1

    const-wide/16 v8, 0x0

    const/4 v2, 0x1

    .line 34
    :try_start_0
    iget-object v1, p0, Le/a/a/b;->y:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    iget-boolean v0, p0, Le/a/a/b;->v:Z

    if-eqz v0, :cond_2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Le/a/a/b;->r:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v0, :cond_0

    :try_start_2
    iget-object v0, p0, Le/a/a/b;->r:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_8

    :cond_0
    :goto_0
    iget-object v0, p0, Le/a/a/b;->e:Le/a/a/l;

    invoke-interface {v0, p0}, Le/a/a/l;->b(Le/a/a/g;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    :try_start_3
    monitor-exit v1

    new-instance v0, Ljava/net/URL;

    iget-object v1, p0, Le/a/a/b;->b:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;

    iput-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    iget-object v0, p0, Le/a/a/b;->w:Ljava/lang/String;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v0, :cond_3

    :try_start_4
    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    iget-object v1, p0, Le/a/a/b;->w:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V
    :try_end_4
    .catch Ljava/net/ProtocolException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    :cond_3
    :try_start_5
    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    const/16 v1, 0xbb8

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    const v1, 0x15f90

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    iget-object v0, p0, Le/a/a/b;->c:Ljava/util/Map;

    if-eqz v0, :cond_7

    iget-object v0, p0, Le/a/a/b;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    iget-object v5, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v1, v0}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    goto :goto_2

    :catch_0
    move-exception v0

    :goto_3
    :try_start_6
    iput-object v0, p0, Le/a/a/b;->f:Ljava/io/IOException;
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    iget-object v0, p0, Le/a/a/b;->r:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v0, :cond_4

    :try_start_7
    iget-object v0, p0, Le/a/a/b;->r:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_3

    :cond_4
    :goto_4
    if-nez v3, :cond_1

    iget-object v0, p0, Le/a/a/b;->e:Le/a/a/l;

    invoke-interface {v0, p0}, Le/a/a/l;->b(Le/a/a/g;)V

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_8
    monitor-exit v1

    throw v0
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    :catchall_1
    move-exception v0

    :goto_5
    iget-object v1, p0, Le/a/a/b;->r:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v1, :cond_5

    :try_start_9
    iget-object v1, p0, Le/a/a/b;->r:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v1}, Ljava/nio/channels/ReadableByteChannel;->close()V
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_2

    :cond_5
    :goto_6
    if-nez v3, :cond_6

    iget-object v1, p0, Le/a/a/b;->e:Le/a/a/l;

    invoke-interface {v1, p0}, Le/a/a/l;->b(Le/a/a/g;)V

    :cond_6
    throw v0

    :catch_1
    move-exception v0

    :try_start_a
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_7
    iget-wide v0, p0, Le/a/a/b;->h:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_8

    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    const-string v1, "Range"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "bytes="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, p0, Le/a/a/b;->h:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_8
    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    const-string v1, "User-Agent"

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->getRequestProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_9

    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    const-string v1, "User-Agent"

    iget-object v4, p0, Le/a/a/b;->a:Landroid/content/Context;

    invoke-static {v4}, Le/a/a/n;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    :cond_9
    iget-object v0, p0, Le/a/a/b;->q:[B

    if-nez v0, :cond_a

    iget-object v0, p0, Le/a/a/b;->r:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v0, :cond_b

    :cond_a
    invoke-direct {p0}, Le/a/a/b;->l()V
    :try_end_a
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :cond_b
    const/4 v0, 0x0

    :try_start_b
    iget-object v1, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_7
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    move-result-object v0

    :goto_7
    :try_start_c
    iget-object v1, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    iput v1, p0, Le/a/a/b;->t:I

    iget-object v1, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Le/a/a/b;->s:Ljava/lang/String;

    iget-object v1, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentLength()I

    move-result v1

    iput v1, p0, Le/a/a/b;->i:I

    iget-wide v4, p0, Le/a/a/b;->k:J

    cmp-long v1, v4, v8

    if-lez v1, :cond_d

    iget v1, p0, Le/a/a/b;->i:I

    int-to-long v4, v1

    iget-wide v6, p0, Le/a/a/b;->k:J

    cmp-long v1, v4, v6

    if-lez v1, :cond_d

    iget-boolean v1, p0, Le/a/a/b;->l:Z

    if-eqz v1, :cond_d

    invoke-direct {p0}, Le/a/a/b;->m()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_0
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    iget-object v0, p0, Le/a/a/b;->r:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v0, :cond_c

    :try_start_d
    iget-object v0, p0, Le/a/a/b;->r:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/ReadableByteChannel;->close()V
    :try_end_d
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_6

    :cond_c
    :goto_8
    iget-object v0, p0, Le/a/a/b;->e:Le/a/a/l;

    invoke-interface {v0, p0}, Le/a/a/l;->b(Le/a/a/g;)V

    goto/16 :goto_1

    :cond_d
    :try_start_e
    iget-object v1, p0, Le/a/a/b;->e:Le/a/a/l;

    invoke-interface {v1, p0}, Le/a/a/l;->a(Le/a/a/g;)V

    iget v1, p0, Le/a/a/b;->t:I

    div-int/lit8 v1, v1, 0x64

    const/4 v4, 0x2

    if-eq v1, v4, :cond_13

    move v1, v2

    :goto_9
    if-eqz v1, :cond_e

    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    :cond_e
    iput-object v0, p0, Le/a/a/b;->x:Ljava/io/InputStream;

    iget-object v0, p0, Le/a/a/b;->x:Ljava/io/InputStream;

    if-eqz v0, :cond_f

    const-string v0, "gzip"

    iget-object v1, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getContentEncoding()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_f

    new-instance v0, Ljava/util/zip/GZIPInputStream;

    iget-object v1, p0, Le/a/a/b;->x:Ljava/io/InputStream;

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Le/a/a/b;->x:Ljava/io/InputStream;

    const/4 v0, -0x1

    iput v0, p0, Le/a/a/b;->i:I

    :cond_f
    iget-wide v0, p0, Le/a/a/b;->h:J

    cmp-long v0, v0, v8

    if-eqz v0, :cond_11

    iget v0, p0, Le/a/a/b;->t:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_14

    iget v0, p0, Le/a/a/b;->i:I

    if-eq v0, v10, :cond_10

    iget v0, p0, Le/a/a/b;->i:I

    int-to-long v0, v0

    iget-wide v4, p0, Le/a/a/b;->h:J

    sub-long/2addr v0, v4

    long-to-int v0, v0

    iput v0, p0, Le/a/a/b;->i:I

    :cond_10
    const/4 v0, 0x1

    iput-boolean v0, p0, Le/a/a/b;->n:Z

    :cond_11
    :goto_a
    iget-object v0, p0, Le/a/a/b;->x:Ljava/io/InputStream;
    :try_end_e
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_0
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    if-eqz v0, :cond_15

    :try_start_f
    invoke-static {}, Le/a/a/b;->k()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Le/a/a/e;

    invoke-direct {v1, p0}, Le/a/a/e;-><init>(Le/a/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_f
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_4
    .catchall {:try_start_f .. :try_end_f} :catchall_2

    move v0, v2

    :goto_b
    iget-object v1, p0, Le/a/a/b;->r:Ljava/nio/channels/ReadableByteChannel;

    if-eqz v1, :cond_12

    :try_start_10
    iget-object v1, p0, Le/a/a/b;->r:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v1}, Ljava/nio/channels/ReadableByteChannel;->close()V
    :try_end_10
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_5

    :cond_12
    :goto_c
    if-nez v0, :cond_1

    iget-object v0, p0, Le/a/a/b;->e:Le/a/a/l;

    invoke-interface {v0, p0}, Le/a/a/l;->b(Le/a/a/g;)V

    goto/16 :goto_1

    :cond_13
    move v1, v3

    goto :goto_9

    :cond_14
    :try_start_11
    iget-wide v0, p0, Le/a/a/b;->h:J

    iput-wide v0, p0, Le/a/a/b;->o:J
    :try_end_11
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_0
    .catchall {:try_start_11 .. :try_end_11} :catchall_1

    goto :goto_a

    :catch_2
    move-exception v1

    goto/16 :goto_6

    :catchall_2
    move-exception v0

    move v3, v2

    goto/16 :goto_5

    :catch_3
    move-exception v0

    goto/16 :goto_4

    :catch_4
    move-exception v0

    move v3, v2

    goto/16 :goto_3

    :catch_5
    move-exception v1

    goto :goto_c

    :catch_6
    move-exception v0

    goto/16 :goto_8

    :catch_7
    move-exception v1

    goto/16 :goto_7

    :catch_8
    move-exception v0

    goto/16 :goto_0

    :cond_15
    move v0, v3

    goto :goto_b
.end method

.method static synthetic b(Le/a/a/b;)V
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 34
    :try_start_0
    iget-object v0, p0, Le/a/a/b;->x:Ljava/io/InputStream;

    if-eqz v0, :cond_2

    const/16 v0, 0x2000

    new-array v3, v0, [B

    :cond_0
    :goto_0
    invoke-virtual {p0}, Le/a/a/b;->d()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Le/a/a/b;->x:Ljava/io/InputStream;

    invoke-virtual {v0, v3}, Ljava/io/InputStream;->read([B)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    iget-wide v4, p0, Le/a/a/b;->o:J

    int-to-long v6, v0

    add-long/2addr v4, v6

    iput-wide v4, p0, Le/a/a/b;->o:J

    iget-boolean v1, p0, Le/a/a/b;->n:Z

    if-eqz v1, :cond_6

    iget-wide v4, p0, Le/a/a/b;->o:J

    iget-wide v6, p0, Le/a/a/b;->h:J

    cmp-long v1, v4, v6

    if-lez v1, :cond_0

    const/4 v1, 0x0

    iput-boolean v1, p0, Le/a/a/b;->n:Z

    iget-wide v4, p0, Le/a/a/b;->h:J

    iget-wide v6, p0, Le/a/a/b;->o:J

    int-to-long v8, v0

    sub-long/2addr v6, v8

    sub-long/2addr v4, v6

    long-to-int v1, v4

    sub-int/2addr v0, v1

    :goto_1
    iget-wide v4, p0, Le/a/a/b;->k:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-eqz v4, :cond_4

    iget-wide v4, p0, Le/a/a/b;->o:J

    iget-wide v6, p0, Le/a/a/b;->k:J

    cmp-long v4, v4, v6

    if-lez v4, :cond_4

    iget-wide v4, p0, Le/a/a/b;->o:J

    iget-wide v6, p0, Le/a/a/b;->k:J

    sub-long/2addr v4, v6

    long-to-int v2, v4

    sub-int/2addr v0, v2

    if-lez v0, :cond_1

    iget-object v2, p0, Le/a/a/b;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-static {v3, v1, v0}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I

    :cond_1
    invoke-direct {p0}, Le/a/a/b;->m()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    :try_start_1
    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_1
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_6

    :goto_2
    :try_start_2
    iget-object v0, p0, Le/a/a/b;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_3
    :goto_3
    iget-object v0, p0, Le/a/a/b;->e:Le/a/a/l;

    invoke-interface {v0, p0}, Le/a/a/l;->b(Le/a/a/g;)V

    return-void

    :cond_4
    :try_start_3
    iget-object v4, p0, Le/a/a/b;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-static {v3, v1, v0}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/nio/channels/WritableByteChannel;->write(Ljava/nio/ByteBuffer;)I
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    iput-object v0, p0, Le/a/a/b;->f:Ljava/io/IOException;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_5
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_5 .. :try_end_5} :catch_5

    :goto_4
    :try_start_6
    iget-object v0, p0, Le/a/a/b;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v0}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    goto :goto_3

    :catch_1
    move-exception v0

    iget-object v1, p0, Le/a/a/b;->f:Ljava/io/IOException;

    if-nez v1, :cond_3

    iput-object v0, p0, Le/a/a/b;->f:Ljava/io/IOException;

    goto :goto_3

    :catch_2
    move-exception v0

    iget-object v1, p0, Le/a/a/b;->f:Ljava/io/IOException;

    if-nez v1, :cond_3

    iput-object v0, p0, Le/a/a/b;->f:Ljava/io/IOException;

    goto :goto_3

    :catchall_0
    move-exception v0

    :try_start_7
    iget-object v1, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_7
    .catch Ljava/lang/ArrayIndexOutOfBoundsException; {:try_start_7 .. :try_end_7} :catch_4

    :goto_5
    :try_start_8
    iget-object v1, p0, Le/a/a/b;->d:Ljava/nio/channels/WritableByteChannel;

    invoke-interface {v1}, Ljava/nio/channels/WritableByteChannel;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_3

    :cond_5
    :goto_6
    throw v0

    :catch_3
    move-exception v1

    iget-object v2, p0, Le/a/a/b;->f:Ljava/io/IOException;

    if-nez v2, :cond_5

    iput-object v1, p0, Le/a/a/b;->f:Ljava/io/IOException;

    goto :goto_6

    :catch_4
    move-exception v1

    goto :goto_5

    :catch_5
    move-exception v0

    goto :goto_4

    :catch_6
    move-exception v0

    goto :goto_2

    :cond_6
    move v1, v2

    goto :goto_1
.end method

.method private static k()Ljava/util/concurrent/ExecutorService;
    .locals 2

    .prologue
    .line 121
    sget-object v1, Le/a/a/b;->A:Ljava/lang/Object;

    monitor-enter v1

    .line 122
    :try_start_0
    sget-object v0, Le/a/a/b;->z:Ljava/util/concurrent/ExecutorService;

    if-nez v0, :cond_0

    .line 123
    new-instance v0, Le/a/a/c;

    invoke-direct {v0}, Le/a/a/c;-><init>()V

    .line 137
    invoke-static {v0}, Ljava/util/concurrent/Executors;->newCachedThreadPool(Ljava/util/concurrent/ThreadFactory;)Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Le/a/a/b;->z:Ljava/util/concurrent/ExecutorService;

    .line 139
    :cond_0
    sget-object v0, Le/a/a/b;->z:Ljava/util/concurrent/ExecutorService;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 140
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private l()V
    .locals 5

    .prologue
    .line 309
    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 310
    iget-object v0, p0, Le/a/a/b;->p:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 311
    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    const-string v1, "Content-Type"

    iget-object v2, p0, Le/a/a/b;->p:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 314
    :cond_0
    const/4 v1, 0x0

    .line 316
    :try_start_0
    iget-object v0, p0, Le/a/a/b;->q:[B

    if-eqz v0, :cond_3

    .line 317
    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    iget-object v2, p0, Le/a/a/b;->q:[B

    array-length v2, v2

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 318
    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 319
    iget-object v0, p0, Le/a/a/b;->q:[B

    invoke-virtual {v1, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 332
    :cond_1
    if-eqz v1, :cond_2

    .line 333
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 336
    :cond_2
    return-void

    .line 321
    :cond_3
    :try_start_1
    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    iget v2, p0, Le/a/a/b;->j:I

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 322
    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    .line 323
    const/16 v0, 0x2000

    new-array v0, v0, [B

    .line 324
    invoke-static {v0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    .line 325
    :goto_0
    iget-object v3, p0, Le/a/a/b;->r:Ljava/nio/channels/ReadableByteChannel;

    invoke-interface {v3, v2}, Ljava/nio/channels/ReadableByteChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v3

    if-lez v3, :cond_1

    .line 326
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 327
    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->limit()I

    move-result v4

    invoke-virtual {v1, v0, v3, v4}, Ljava/io/OutputStream;->write([BII)V

    .line 328
    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 332
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_4

    .line 333
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    :cond_4
    throw v0
.end method

.method private m()V
    .locals 1

    .prologue
    .line 449
    const/4 v0, 0x1

    iput-boolean v0, p0, Le/a/a/b;->m:Z

    .line 450
    invoke-virtual {p0}, Le/a/a/b;->c()V

    .line 451
    return-void
.end method

.method private n()V
    .locals 2

    .prologue
    .line 504
    iget-boolean v0, p0, Le/a/a/b;->u:Z

    if-eqz v0, :cond_0

    .line 505
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Request already started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 507
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    iget-object v0, p0, Le/a/a/b;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 184
    invoke-direct {p0}, Le/a/a/b;->n()V

    .line 185
    iput-object p1, p0, Le/a/a/b;->w:Ljava/lang/String;

    .line 186
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/nio/channels/ReadableByteChannel;J)V
    .locals 3

    .prologue
    .line 170
    invoke-direct {p0}, Le/a/a/b;->n()V

    .line 171
    const-wide/32 v0, 0x7fffffff

    cmp-long v0, p3, v0

    if-lez v0, :cond_0

    .line 172
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Upload contentLength is too big."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_0
    long-to-int v0, p3

    iput v0, p0, Le/a/a/b;->j:I

    .line 176
    iput-object p1, p0, Le/a/a/b;->p:Ljava/lang/String;

    .line 177
    iput-object p2, p0, Le/a/a/b;->r:Ljava/nio/channels/ReadableByteChannel;

    .line 178
    const/4 v0, 0x0

    iput-object v0, p0, Le/a/a/b;->q:[B

    .line 179
    return-void
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 1

    .prologue
    .line 161
    invoke-direct {p0}, Le/a/a/b;->n()V

    .line 162
    iput-object p1, p0, Le/a/a/b;->p:Ljava/lang/String;

    .line 163
    iput-object p2, p0, Le/a/a/b;->q:[B

    .line 164
    const/4 v0, 0x0

    iput-object v0, p0, Le/a/a/b;->r:Ljava/nio/channels/ReadableByteChannel;

    .line 165
    return-void
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 482
    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    if-nez v0, :cond_0

    .line 483
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Response headers not available"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 485
    :cond_0
    iget-object v0, p0, Le/a/a/b;->g:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    .line 486
    if-eqz v0, :cond_1

    .line 487
    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 488
    if-eqz v0, :cond_1

    .line 489
    const-string v1, ", "

    invoke-static {v1, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    .line 492
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 190
    invoke-static {}, Le/a/a/b;->k()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    new-instance v1, Le/a/a/d;

    invoke-direct {v1, p0}, Le/a/a/d;-><init>(Le/a/a/b;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    .line 196
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 404
    iget-object v1, p0, Le/a/a/b;->y:Ljava/lang/Object;

    monitor-enter v1

    .line 405
    :try_start_0
    iget-boolean v0, p0, Le/a/a/b;->v:Z

    if-eqz v0, :cond_0

    .line 406
    monitor-exit v1

    .line 410
    :goto_0
    return-void

    .line 409
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Le/a/a/b;->v:Z

    .line 410
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 415
    iget-object v1, p0, Le/a/a/b;->y:Ljava/lang/Object;

    monitor-enter v1

    .line 416
    :try_start_0
    iget-boolean v0, p0, Le/a/a/b;->v:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 417
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 422
    const-string v0, ""

    return-object v0
.end method

.method public final f()I
    .locals 2

    .prologue
    .line 427
    iget v0, p0, Le/a/a/b;->t:I

    .line 434
    const/16 v1, 0xce

    if-ne v0, v1, :cond_0

    .line 435
    const/16 v0, 0xc8

    .line 437
    :cond_0
    return v0
.end method

.method public final g()Ljava/io/IOException;
    .locals 1

    .prologue
    .line 442
    iget-object v0, p0, Le/a/a/b;->f:Ljava/io/IOException;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Le/a/a/b;->m:Z

    if-eqz v0, :cond_0

    .line 443
    new-instance v0, Le/a/a/m;

    invoke-direct {v0}, Le/a/a/m;-><init>()V

    iput-object v0, p0, Le/a/a/b;->f:Ljava/io/IOException;

    .line 445
    :cond_0
    iget-object v0, p0, Le/a/a/b;->f:Ljava/io/IOException;

    return-object v0
.end method

.method public final h()[B
    .locals 1

    .prologue
    .line 467
    iget-object v0, p0, Le/a/a/b;->d:Ljava/nio/channels/WritableByteChannel;

    check-cast v0, Le/a/a/a;

    invoke-virtual {v0}, Le/a/a/a;->a()[B

    move-result-object v0

    return-object v0
.end method

.method public final i()J
    .locals 2

    .prologue
    .line 472
    iget v0, p0, Le/a/a/b;->i:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 477
    iget-object v0, p0, Le/a/a/b;->s:Ljava/lang/String;

    return-object v0
.end method
