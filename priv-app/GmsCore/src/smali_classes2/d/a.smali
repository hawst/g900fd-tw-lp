.class public final Ld/a;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/protobuf/nano/e;

.field private static final e:[Ld/a;


# instance fields
.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    const-class v0, Ld/a;

    const v1, 0x1ea08972

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/e;->a(Ljava/lang/Class;I)Lcom/google/protobuf/nano/e;

    move-result-object v0

    sput-object v0, Ld/a;->a:Lcom/google/protobuf/nano/e;

    .line 19
    const/4 v0, 0x0

    new-array v0, v0, [Ld/a;

    sput-object v0, Ld/a;->e:[Ld/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 34
    iput-object v0, p0, Ld/a;->b:Ljava/lang/String;

    iput-object v0, p0, Ld/a;->c:Ljava/lang/String;

    iput-object v0, p0, Ld/a;->d:Ljava/lang/String;

    iput-object v0, p0, Ld/a;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Ld/a;->cachedSize:I

    .line 35
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 109
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 110
    iget-object v1, p0, Ld/a;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 111
    const/4 v1, 0x1

    iget-object v2, p0, Ld/a;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 114
    :cond_0
    iget-object v1, p0, Ld/a;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 115
    const/4 v1, 0x2

    iget-object v2, p0, Ld/a;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    :cond_1
    iget-object v1, p0, Ld/a;->d:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 119
    const/4 v1, 0x3

    iget-object v2, p0, Ld/a;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 122
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 48
    if-ne p1, p0, :cond_1

    .line 49
    const/4 v0, 0x1

    .line 76
    :cond_0
    :goto_0
    return v0

    .line 51
    :cond_1
    instance-of v1, p1, Ld/a;

    if-eqz v1, :cond_0

    .line 54
    check-cast p1, Ld/a;

    .line 55
    iget-object v1, p0, Ld/a;->b:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 56
    iget-object v1, p1, Ld/a;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 62
    :cond_2
    iget-object v1, p0, Ld/a;->c:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 63
    iget-object v1, p1, Ld/a;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 69
    :cond_3
    iget-object v1, p0, Ld/a;->d:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 70
    iget-object v1, p1, Ld/a;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 76
    :cond_4
    invoke-virtual {p0, p1}, Ld/a;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 59
    :cond_5
    iget-object v1, p0, Ld/a;->b:Ljava/lang/String;

    iget-object v2, p1, Ld/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 66
    :cond_6
    iget-object v1, p0, Ld/a;->c:Ljava/lang/String;

    iget-object v2, p1, Ld/a;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 73
    :cond_7
    iget-object v1, p0, Ld/a;->d:Ljava/lang/String;

    iget-object v2, p1, Ld/a;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 81
    iget-object v0, p0, Ld/a;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 84
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Ld/a;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 86
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Ld/a;->d:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 88
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Ld/a;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    return v0

    .line 81
    :cond_0
    iget-object v0, p0, Ld/a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 84
    :cond_1
    iget-object v0, p0, Ld/a;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 86
    :cond_2
    iget-object v1, p0, Ld/a;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Ld/a;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ld/a;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ld/a;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Ld/a;->d:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Ld/a;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 96
    const/4 v0, 0x1

    iget-object v1, p0, Ld/a;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 98
    :cond_0
    iget-object v0, p0, Ld/a;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 99
    const/4 v0, 0x2

    iget-object v1, p0, Ld/a;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 101
    :cond_1
    iget-object v0, p0, Ld/a;->d:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 102
    const/4 v0, 0x3

    iget-object v1, p0, Ld/a;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 104
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 105
    return-void
.end method
