.class public final Lcom/google/protobuf/nano/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:I

.field protected final b:Ljava/lang/Class;

.field public final c:I

.field protected final d:Z


# direct methods
.method private constructor <init>(ILjava/lang/Class;I)V
    .locals 1

    .prologue
    .line 167
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 168
    const/16 v0, 0xb

    iput v0, p0, Lcom/google/protobuf/nano/e;->a:I

    .line 169
    iput-object p2, p0, Lcom/google/protobuf/nano/e;->b:Ljava/lang/Class;

    .line 170
    iput p3, p0, Lcom/google/protobuf/nano/e;->c:I

    .line 171
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/protobuf/nano/e;->d:Z

    .line 172
    return-void
.end method

.method public static a(Ljava/lang/Class;I)Lcom/google/protobuf/nano/e;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 87
    new-instance v0, Lcom/google/protobuf/nano/e;

    const/16 v1, 0xb

    invoke-direct {v0, v1, p0, p1}, Lcom/google/protobuf/nano/e;-><init>(ILjava/lang/Class;I)V

    return-object v0
.end method

.method private b(Ljava/lang/Object;)I
    .locals 3

    .prologue
    .line 319
    iget v0, p0, Lcom/google/protobuf/nano/e;->c:I

    invoke-static {v0}, Lcom/google/protobuf/nano/m;->b(I)I

    move-result v0

    .line 320
    iget v1, p0, Lcom/google/protobuf/nano/e;->a:I

    packed-switch v1, :pswitch_data_0

    .line 328
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/protobuf/nano/e;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 322
    :pswitch_0
    check-cast p1, Lcom/google/protobuf/nano/j;

    .line 323
    invoke-static {v0}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    invoke-virtual {p1}, Lcom/google/protobuf/nano/j;->getSerializedSize()I

    move-result v1

    add-int/2addr v0, v1

    .line 326
    :goto_0
    return v0

    .line 325
    :pswitch_1
    check-cast p1, Lcom/google/protobuf/nano/j;

    .line 326
    invoke-static {v0, p1}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    goto :goto_0

    .line 320
    nop

    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private b(Ljava/lang/Object;Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 263
    :try_start_0
    iget v0, p0, Lcom/google/protobuf/nano/e;->c:I

    invoke-virtual {p2, v0}, Lcom/google/protobuf/nano/b;->d(I)V

    .line 264
    iget v0, p0, Lcom/google/protobuf/nano/e;->a:I

    packed-switch v0, :pswitch_data_0

    .line 277
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/protobuf/nano/e;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 282
    :catch_0
    move-exception v0

    .line 281
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 266
    :pswitch_0
    :try_start_1
    check-cast p1, Lcom/google/protobuf/nano/j;

    .line 267
    iget v0, p0, Lcom/google/protobuf/nano/e;->c:I

    invoke-static {v0}, Lcom/google/protobuf/nano/m;->b(I)I

    move-result v0

    .line 268
    invoke-virtual {p1, p2}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 270
    const/4 v1, 0x4

    invoke-virtual {p2, v0, v1}, Lcom/google/protobuf/nano/b;->i(II)V

    .line 275
    :goto_0
    return-void

    .line 273
    :pswitch_1
    check-cast p1, Lcom/google/protobuf/nano/j;

    .line 274
    invoke-virtual {p2, p1}, Lcom/google/protobuf/nano/b;->a(Lcom/google/protobuf/nano/j;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 264
    :pswitch_data_0
    .packed-switch 0xa
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method final a(Ljava/lang/Object;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 297
    iget-boolean v1, p0, Lcom/google/protobuf/nano/e;->d:Z

    if-eqz v1, :cond_1

    .line 298
    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_2

    invoke-static {p1, v1}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {p1, v1}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/protobuf/nano/e;->b(Ljava/lang/Object;)I

    move-result v3

    add-int/2addr v0, v3

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 300
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/protobuf/nano/e;->b(Ljava/lang/Object;)I

    move-result v0

    :cond_2
    return v0
.end method

.method final a(Ljava/lang/Object;Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/google/protobuf/nano/e;->d:Z

    if-eqz v0, :cond_1

    .line 254
    invoke-static {p1}, Ljava/lang/reflect/Array;->getLength(Ljava/lang/Object;)I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_2

    invoke-static {p1, v0}, Ljava/lang/reflect/Array;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-direct {p0, v2, p2}, Lcom/google/protobuf/nano/e;->b(Ljava/lang/Object;Lcom/google/protobuf/nano/b;)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 256
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/protobuf/nano/e;->b(Ljava/lang/Object;Lcom/google/protobuf/nano/b;)V

    .line 258
    :cond_2
    return-void
.end method
