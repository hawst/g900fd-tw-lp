.class public final Lcom/google/a/b/d;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Integer;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Integer;

.field public l:Ljava/lang/Integer;

.field public m:Ljava/lang/Integer;

.field public n:Ljava/lang/Integer;

.field public o:Ljava/lang/Integer;

.field public p:Ljava/lang/Integer;

.field public q:Ljava/lang/Integer;

.field public r:Lcom/google/a/b/b;

.field public s:Ljava/lang/Integer;

.field public t:Ljava/lang/Integer;

.field public u:Lcom/google/a/b/c;

.field public v:Lcom/google/a/b/c;

.field public w:Lcom/google/a/b/c;

.field public x:Lcom/google/a/b/c;

.field public y:Ljava/lang/Integer;

.field public z:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 103
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 104
    iput-object v0, p0, Lcom/google/a/b/d;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->e:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->f:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->g:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->h:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->i:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->j:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->k:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->l:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->m:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->n:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->o:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->p:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->q:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->r:Lcom/google/a/b/b;

    iput-object v0, p0, Lcom/google/a/b/d;->s:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->t:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->u:Lcom/google/a/b/c;

    iput-object v0, p0, Lcom/google/a/b/d;->v:Lcom/google/a/b/c;

    iput-object v0, p0, Lcom/google/a/b/d;->w:Lcom/google/a/b/c;

    iput-object v0, p0, Lcom/google/a/b/d;->x:Lcom/google/a/b/c;

    iput-object v0, p0, Lcom/google/a/b/d;->y:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/a/b/d;->z:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/a/b/d;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/a/b/d;->cachedSize:I

    .line 105
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 225
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 226
    iget-object v1, p0, Lcom/google/a/b/d;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 227
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/a/b/d;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 230
    :cond_0
    iget-object v1, p0, Lcom/google/a/b/d;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 231
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/a/b/d;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 234
    :cond_1
    iget-object v1, p0, Lcom/google/a/b/d;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 235
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/a/b/d;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    :cond_2
    iget-object v1, p0, Lcom/google/a/b/d;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 239
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/a/b/d;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 242
    :cond_3
    iget-object v1, p0, Lcom/google/a/b/d;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 243
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/a/b/d;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 246
    :cond_4
    iget-object v1, p0, Lcom/google/a/b/d;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 247
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/a/b/d;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 250
    :cond_5
    iget-object v1, p0, Lcom/google/a/b/d;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 251
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/a/b/d;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 254
    :cond_6
    iget-object v1, p0, Lcom/google/a/b/d;->h:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 255
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/a/b/d;->h:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    :cond_7
    iget-object v1, p0, Lcom/google/a/b/d;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 259
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/a/b/d;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    :cond_8
    iget-object v1, p0, Lcom/google/a/b/d;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 263
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/a/b/d;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 266
    :cond_9
    iget-object v1, p0, Lcom/google/a/b/d;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 267
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/a/b/d;->k:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 270
    :cond_a
    iget-object v1, p0, Lcom/google/a/b/d;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 271
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/a/b/d;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 274
    :cond_b
    iget-object v1, p0, Lcom/google/a/b/d;->m:Ljava/lang/Integer;

    if-eqz v1, :cond_c

    .line 275
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/a/b/d;->m:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 278
    :cond_c
    iget-object v1, p0, Lcom/google/a/b/d;->n:Ljava/lang/Integer;

    if-eqz v1, :cond_d

    .line 279
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/a/b/d;->n:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 282
    :cond_d
    iget-object v1, p0, Lcom/google/a/b/d;->o:Ljava/lang/Integer;

    if-eqz v1, :cond_e

    .line 283
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/a/b/d;->o:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 286
    :cond_e
    iget-object v1, p0, Lcom/google/a/b/d;->p:Ljava/lang/Integer;

    if-eqz v1, :cond_f

    .line 287
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/a/b/d;->p:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 290
    :cond_f
    iget-object v1, p0, Lcom/google/a/b/d;->q:Ljava/lang/Integer;

    if-eqz v1, :cond_10

    .line 291
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/a/b/d;->q:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 294
    :cond_10
    iget-object v1, p0, Lcom/google/a/b/d;->r:Lcom/google/a/b/b;

    if-eqz v1, :cond_11

    .line 295
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/a/b/d;->r:Lcom/google/a/b/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 298
    :cond_11
    iget-object v1, p0, Lcom/google/a/b/d;->s:Ljava/lang/Integer;

    if-eqz v1, :cond_12

    .line 299
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/a/b/d;->s:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 302
    :cond_12
    iget-object v1, p0, Lcom/google/a/b/d;->t:Ljava/lang/Integer;

    if-eqz v1, :cond_13

    .line 303
    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/a/b/d;->t:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 306
    :cond_13
    iget-object v1, p0, Lcom/google/a/b/d;->u:Lcom/google/a/b/c;

    if-eqz v1, :cond_14

    .line 307
    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/a/b/d;->u:Lcom/google/a/b/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 310
    :cond_14
    iget-object v1, p0, Lcom/google/a/b/d;->v:Lcom/google/a/b/c;

    if-eqz v1, :cond_15

    .line 311
    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/a/b/d;->v:Lcom/google/a/b/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 314
    :cond_15
    iget-object v1, p0, Lcom/google/a/b/d;->w:Lcom/google/a/b/c;

    if-eqz v1, :cond_16

    .line 315
    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/a/b/d;->w:Lcom/google/a/b/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 318
    :cond_16
    iget-object v1, p0, Lcom/google/a/b/d;->x:Lcom/google/a/b/c;

    if-eqz v1, :cond_17

    .line 319
    const/16 v1, 0x18

    iget-object v2, p0, Lcom/google/a/b/d;->x:Lcom/google/a/b/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 322
    :cond_17
    iget-object v1, p0, Lcom/google/a/b/d;->y:Ljava/lang/Integer;

    if-eqz v1, :cond_18

    .line 323
    const/16 v1, 0x19

    iget-object v2, p0, Lcom/google/a/b/d;->y:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 326
    :cond_18
    iget-object v1, p0, Lcom/google/a/b/d;->z:Ljava/lang/String;

    if-eqz v1, :cond_19

    .line 327
    const/16 v1, 0x1a

    iget-object v2, p0, Lcom/google/a/b/d;->z:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 330
    :cond_19
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/a/b/d;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->h:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->i:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->j:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->m:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->n:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->o:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->p:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->q:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Lcom/google/a/b/d;->r:Lcom/google/a/b/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/a/b/b;

    invoke-direct {v0}, Lcom/google/a/b/b;-><init>()V

    iput-object v0, p0, Lcom/google/a/b/d;->r:Lcom/google/a/b/b;

    :cond_1
    iget-object v0, p0, Lcom/google/a/b/d;->r:Lcom/google/a/b/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_13
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->s:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->t:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_15
    iget-object v0, p0, Lcom/google/a/b/d;->u:Lcom/google/a/b/c;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/a/b/c;

    invoke-direct {v0}, Lcom/google/a/b/c;-><init>()V

    iput-object v0, p0, Lcom/google/a/b/d;->u:Lcom/google/a/b/c;

    :cond_2
    iget-object v0, p0, Lcom/google/a/b/d;->u:Lcom/google/a/b/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_16
    iget-object v0, p0, Lcom/google/a/b/d;->v:Lcom/google/a/b/c;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/a/b/c;

    invoke-direct {v0}, Lcom/google/a/b/c;-><init>()V

    iput-object v0, p0, Lcom/google/a/b/d;->v:Lcom/google/a/b/c;

    :cond_3
    iget-object v0, p0, Lcom/google/a/b/d;->v:Lcom/google/a/b/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_17
    iget-object v0, p0, Lcom/google/a/b/d;->w:Lcom/google/a/b/c;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/a/b/c;

    invoke-direct {v0}, Lcom/google/a/b/c;-><init>()V

    iput-object v0, p0, Lcom/google/a/b/d;->w:Lcom/google/a/b/c;

    :cond_4
    iget-object v0, p0, Lcom/google/a/b/d;->w:Lcom/google/a/b/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_18
    iget-object v0, p0, Lcom/google/a/b/d;->x:Lcom/google/a/b/c;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/a/b/c;

    invoke-direct {v0}, Lcom/google/a/b/c;-><init>()V

    iput-object v0, p0, Lcom/google/a/b/d;->x:Lcom/google/a/b/c;

    :cond_5
    iget-object v0, p0, Lcom/google/a/b/d;->x:Lcom/google/a/b/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_19
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->y:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_1a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/a/b/d;->z:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
        0x70 -> :sswitch_e
        0x78 -> :sswitch_f
        0x80 -> :sswitch_10
        0x88 -> :sswitch_11
        0x92 -> :sswitch_12
        0x98 -> :sswitch_13
        0xa0 -> :sswitch_14
        0xaa -> :sswitch_15
        0xb2 -> :sswitch_16
        0xba -> :sswitch_17
        0xc2 -> :sswitch_18
        0xc8 -> :sswitch_19
        0xd2 -> :sswitch_1a
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/a/b/d;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 143
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/a/b/d;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/a/b/d;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 146
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/a/b/d;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/google/a/b/d;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 149
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/a/b/d;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 151
    :cond_2
    iget-object v0, p0, Lcom/google/a/b/d;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 152
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/a/b/d;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 154
    :cond_3
    iget-object v0, p0, Lcom/google/a/b/d;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 155
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/a/b/d;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 157
    :cond_4
    iget-object v0, p0, Lcom/google/a/b/d;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 158
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/a/b/d;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 160
    :cond_5
    iget-object v0, p0, Lcom/google/a/b/d;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 161
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/a/b/d;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 163
    :cond_6
    iget-object v0, p0, Lcom/google/a/b/d;->h:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 164
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/a/b/d;->h:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 166
    :cond_7
    iget-object v0, p0, Lcom/google/a/b/d;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 167
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/a/b/d;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 169
    :cond_8
    iget-object v0, p0, Lcom/google/a/b/d;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 170
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/a/b/d;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 172
    :cond_9
    iget-object v0, p0, Lcom/google/a/b/d;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 173
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/a/b/d;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 175
    :cond_a
    iget-object v0, p0, Lcom/google/a/b/d;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 176
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/a/b/d;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 178
    :cond_b
    iget-object v0, p0, Lcom/google/a/b/d;->m:Ljava/lang/Integer;

    if-eqz v0, :cond_c

    .line 179
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/a/b/d;->m:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 181
    :cond_c
    iget-object v0, p0, Lcom/google/a/b/d;->n:Ljava/lang/Integer;

    if-eqz v0, :cond_d

    .line 182
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/a/b/d;->n:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 184
    :cond_d
    iget-object v0, p0, Lcom/google/a/b/d;->o:Ljava/lang/Integer;

    if-eqz v0, :cond_e

    .line 185
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/a/b/d;->o:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 187
    :cond_e
    iget-object v0, p0, Lcom/google/a/b/d;->p:Ljava/lang/Integer;

    if-eqz v0, :cond_f

    .line 188
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/a/b/d;->p:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 190
    :cond_f
    iget-object v0, p0, Lcom/google/a/b/d;->q:Ljava/lang/Integer;

    if-eqz v0, :cond_10

    .line 191
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/a/b/d;->q:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 193
    :cond_10
    iget-object v0, p0, Lcom/google/a/b/d;->r:Lcom/google/a/b/b;

    if-eqz v0, :cond_11

    .line 194
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/a/b/d;->r:Lcom/google/a/b/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 196
    :cond_11
    iget-object v0, p0, Lcom/google/a/b/d;->s:Ljava/lang/Integer;

    if-eqz v0, :cond_12

    .line 197
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/a/b/d;->s:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 199
    :cond_12
    iget-object v0, p0, Lcom/google/a/b/d;->t:Ljava/lang/Integer;

    if-eqz v0, :cond_13

    .line 200
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/a/b/d;->t:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 202
    :cond_13
    iget-object v0, p0, Lcom/google/a/b/d;->u:Lcom/google/a/b/c;

    if-eqz v0, :cond_14

    .line 203
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/a/b/d;->u:Lcom/google/a/b/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 205
    :cond_14
    iget-object v0, p0, Lcom/google/a/b/d;->v:Lcom/google/a/b/c;

    if-eqz v0, :cond_15

    .line 206
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/a/b/d;->v:Lcom/google/a/b/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 208
    :cond_15
    iget-object v0, p0, Lcom/google/a/b/d;->w:Lcom/google/a/b/c;

    if-eqz v0, :cond_16

    .line 209
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/a/b/d;->w:Lcom/google/a/b/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 211
    :cond_16
    iget-object v0, p0, Lcom/google/a/b/d;->x:Lcom/google/a/b/c;

    if-eqz v0, :cond_17

    .line 212
    const/16 v0, 0x18

    iget-object v1, p0, Lcom/google/a/b/d;->x:Lcom/google/a/b/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 214
    :cond_17
    iget-object v0, p0, Lcom/google/a/b/d;->y:Ljava/lang/Integer;

    if-eqz v0, :cond_18

    .line 215
    const/16 v0, 0x19

    iget-object v1, p0, Lcom/google/a/b/d;->y:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 217
    :cond_18
    iget-object v0, p0, Lcom/google/a/b/d;->z:Ljava/lang/String;

    if-eqz v0, :cond_19

    .line 218
    const/16 v0, 0x1a

    iget-object v1, p0, Lcom/google/a/b/d;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 220
    :cond_19
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 221
    return-void
.end method
