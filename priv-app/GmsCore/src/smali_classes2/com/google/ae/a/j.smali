.class public final Lcom/google/ae/a/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/ae/a/a;


# instance fields
.field private final a:I

.field private final b:I

.field private final c:Ljava/io/InputStream;

.field private d:J

.field private e:J

.field private f:Z

.field private g:J


# direct methods
.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 2

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/ae/a/j;->g:J

    .line 27
    invoke-virtual {p1}, Ljava/io/InputStream;->markSupported()Z

    move-result v0

    if-nez v0, :cond_0

    .line 28
    new-instance v0, Ljava/io/BufferedInputStream;

    invoke-direct {v0, p1}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    iput-object v0, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    .line 32
    :goto_0
    const/high16 v0, 0x100000

    iput v0, p0, Lcom/google/ae/a/j;->a:I

    .line 33
    const v0, 0x100001

    iput v0, p0, Lcom/google/ae/a/j;->b:I

    .line 35
    iget-object v0, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    iget v1, p0, Lcom/google/ae/a/j;->b:I

    invoke-virtual {v0, v1}, Ljava/io/InputStream;->mark(I)V

    .line 36
    return-void

    .line 30
    :cond_0
    iput-object p1, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a([BII)I
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 40
    monitor-enter p0

    :try_start_0
    array-length v1, p1

    sub-int/2addr v1, p2

    if-lt v1, p3, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "Cannot read into a buffer smaller than given length"

    invoke-static {v1, v2}, Lcom/google/k/a/ah;->a(ZLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    if-nez p3, :cond_1

    .line 64
    :goto_1
    monitor-exit p0

    return v0

    :cond_0
    move v1, v0

    .line 40
    goto :goto_0

    .line 49
    :cond_1
    :try_start_1
    iget-boolean v1, p0, Lcom/google/ae/a/j;->f:Z

    if-eqz v1, :cond_2

    .line 50
    iget-object v1, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->reset()V

    .line 51
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/ae/a/j;->f:Z

    .line 55
    :cond_2
    int-to-long v2, p3

    iget v1, p0, Lcom/google/ae/a/j;->a:I

    int-to-long v4, v1

    iget-wide v6, p0, Lcom/google/ae/a/j;->e:J

    iget-wide v8, p0, Lcom/google/ae/a/j;->d:J

    sub-long/2addr v6, v8

    sub-long/2addr v4, v6

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    long-to-int v1, v2

    .line 57
    iget-object v2, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    invoke-virtual {v2, p1, p2, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 58
    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    .line 59
    iget-wide v2, p0, Lcom/google/ae/a/j;->e:J

    iput-wide v2, p0, Lcom/google/ae/a/j;->g:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 63
    :cond_3
    :try_start_2
    iget-wide v2, p0, Lcom/google/ae/a/j;->e:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/ae/a/j;->e:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move v0, v1

    .line 64
    goto :goto_1
.end method

.method public final declared-synchronized a(J)J
    .locals 9

    .prologue
    const-wide/16 v0, 0x0

    .line 101
    monitor-enter p0

    :try_start_0
    iget v2, p0, Lcom/google/ae/a/j;->a:I

    int-to-long v2, v2

    iget-wide v4, p0, Lcom/google/ae/a/j;->e:J

    iget-wide v6, p0, Lcom/google/ae/a/j;->d:J

    sub-long/2addr v4, v6

    sub-long/2addr v2, v4

    invoke-static {p1, p2, v2, v3}, Ljava/lang/Math;->min(JJ)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    .line 103
    cmp-long v2, v4, v0

    if-nez v2, :cond_0

    .line 121
    :goto_0
    monitor-exit p0

    return-wide v0

    .line 108
    :cond_0
    :try_start_1
    iget-boolean v2, p0, Lcom/google/ae/a/j;->f:Z

    if-eqz v2, :cond_1

    .line 109
    iget-object v2, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->reset()V

    .line 110
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/ae/a/j;->f:Z

    .line 113
    :cond_1
    iget-object v2, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    invoke-virtual {v2, v4, v5}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v2

    .line 115
    cmp-long v6, v2, v0

    if-nez v6, :cond_3

    invoke-virtual {p0}, Lcom/google/ae/a/j;->h()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 117
    :cond_2
    iget-object v2, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    invoke-virtual {v2, v4, v5}, Ljava/io/InputStream;->skip(J)J

    move-result-wide v2

    .line 118
    cmp-long v6, v2, v0

    if-eqz v6, :cond_2

    :cond_3
    move-wide v0, v2

    .line 120
    iget-wide v2, p0, Lcom/google/ae/a/j;->e:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/ae/a/j;->e:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 101
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 4

    .prologue
    .line 71
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/ae/a/j;->d:J

    iget-wide v2, p0, Lcom/google/ae/a/j;->e:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 72
    iget-object v0, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    iget v1, p0, Lcom/google/ae/a/j;->b:I

    invoke-virtual {v0, v1}, Ljava/io/InputStream;->mark(I)V

    .line 74
    :cond_0
    iget-wide v0, p0, Lcom/google/ae/a/j;->e:J

    iput-wide v0, p0, Lcom/google/ae/a/j;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 75
    monitor-exit p0

    return-void

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized e()J
    .locals 2

    .prologue
    .line 84
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/ae/a/j;->e:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized f()J
    .locals 2

    .prologue
    .line 89
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/ae/a/j;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    int-to-long v0, v0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized g()J
    .locals 2

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    iget-wide v0, p0, Lcom/google/ae/a/j;->g:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized h()Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 132
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/ae/a/j;->f:Z

    if-eqz v2, :cond_0

    .line 133
    iget-object v2, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->reset()V

    .line 134
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/ae/a/j;->f:Z

    .line 138
    :cond_0
    iget-object v2, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->available()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-lez v2, :cond_2

    .line 155
    :cond_1
    :goto_0
    monitor-exit p0

    return v0

    .line 143
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_3

    .line 144
    iget-wide v2, p0, Lcom/google/ae/a/j;->e:J

    iput-wide v2, p0, Lcom/google/ae/a/j;->g:J

    move v0, v1

    .line 145
    goto :goto_0

    .line 149
    :cond_3
    iget-object v1, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    invoke-virtual {v1}, Ljava/io/InputStream;->reset()V

    .line 150
    iget-wide v2, p0, Lcom/google/ae/a/j;->e:J

    iget-wide v4, p0, Lcom/google/ae/a/j;->d:J

    sub-long/2addr v2, v4

    .line 151
    :goto_1
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_1

    .line 153
    iget-object v1, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    invoke-virtual {v1, v2, v3}, Ljava/io/InputStream;->skip(J)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    sub-long/2addr v2, v4

    goto :goto_1

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized i()V
    .locals 1

    .prologue
    .line 160
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/ae/a/j;->c:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 161
    monitor-exit p0

    return-void

    .line 160
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
