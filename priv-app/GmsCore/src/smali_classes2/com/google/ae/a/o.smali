.class final enum Lcom/google/ae/a/o;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/ae/a/o;

.field public static final enum b:Lcom/google/ae/a/o;

.field public static final enum c:Lcom/google/ae/a/o;

.field private static final synthetic d:[Lcom/google/ae/a/o;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 62
    new-instance v0, Lcom/google/ae/a/o;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v2}, Lcom/google/ae/a/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ae/a/o;->a:Lcom/google/ae/a/o;

    .line 63
    new-instance v0, Lcom/google/ae/a/o;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, Lcom/google/ae/a/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ae/a/o;->b:Lcom/google/ae/a/o;

    .line 64
    new-instance v0, Lcom/google/ae/a/o;

    const-string v1, "CANCELED"

    invoke-direct {v0, v1, v4}, Lcom/google/ae/a/o;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ae/a/o;->c:Lcom/google/ae/a/o;

    .line 61
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/ae/a/o;

    sget-object v1, Lcom/google/ae/a/o;->a:Lcom/google/ae/a/o;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ae/a/o;->b:Lcom/google/ae/a/o;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/ae/a/o;->c:Lcom/google/ae/a/o;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/ae/a/o;->d:[Lcom/google/ae/a/o;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/ae/a/o;
    .locals 1

    .prologue
    .line 61
    const-class v0, Lcom/google/ae/a/o;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/ae/a/o;

    return-object v0
.end method

.method public static values()[Lcom/google/ae/a/o;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/google/ae/a/o;->d:[Lcom/google/ae/a/o;

    invoke-virtual {v0}, [Lcom/google/ae/a/o;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ae/a/o;

    return-object v0
.end method
