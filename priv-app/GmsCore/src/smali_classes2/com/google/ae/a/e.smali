.class public Lcom/google/ae/a/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/ae/a/b;


# instance fields
.field private a:Lcom/google/ae/a/f;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 372
    new-instance v0, Lcom/google/ae/a/f;

    invoke-direct {v0}, Lcom/google/ae/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/ae/a/e;->a:Lcom/google/ae/a/f;

    .line 373
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ae/a/c;Lcom/google/ae/a/a;)Lcom/google/ae/a/s;
    .locals 3

    .prologue
    .line 385
    :try_start_0
    iget-object v0, p0, Lcom/google/ae/a/e;->a:Lcom/google/ae/a/f;

    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 394
    new-instance v1, Lcom/google/ae/a/g;

    invoke-direct {v1, v0, p2, p3, p4}, Lcom/google/ae/a/g;-><init>(Ljava/net/HttpURLConnection;Ljava/lang/String;Lcom/google/ae/a/c;Lcom/google/ae/a/a;)V

    return-object v1

    .line 388
    :catch_0
    move-exception v0

    .line 389
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Url is malformed."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 390
    :catch_1
    move-exception v0

    .line 391
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Http connection could not be created."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
