.class final enum Lcom/google/ae/a/i;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/ae/a/i;

.field public static final enum b:Lcom/google/ae/a/i;

.field public static final enum c:Lcom/google/ae/a/i;

.field private static final synthetic d:[Lcom/google/ae/a/i;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 74
    new-instance v0, Lcom/google/ae/a/i;

    const-string v1, "IN_PROGRESS"

    invoke-direct {v0, v1, v2}, Lcom/google/ae/a/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ae/a/i;->a:Lcom/google/ae/a/i;

    .line 75
    new-instance v0, Lcom/google/ae/a/i;

    const-string v1, "PAUSED"

    invoke-direct {v0, v1, v3}, Lcom/google/ae/a/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ae/a/i;->b:Lcom/google/ae/a/i;

    .line 76
    new-instance v0, Lcom/google/ae/a/i;

    const-string v1, "CANCELED"

    invoke-direct {v0, v1, v4}, Lcom/google/ae/a/i;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ae/a/i;->c:Lcom/google/ae/a/i;

    .line 73
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/ae/a/i;

    sget-object v1, Lcom/google/ae/a/i;->a:Lcom/google/ae/a/i;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ae/a/i;->b:Lcom/google/ae/a/i;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/ae/a/i;->c:Lcom/google/ae/a/i;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/ae/a/i;->d:[Lcom/google/ae/a/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/ae/a/i;
    .locals 1

    .prologue
    .line 73
    const-class v0, Lcom/google/ae/a/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/ae/a/i;

    return-object v0
.end method

.method public static values()[Lcom/google/ae/a/i;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Lcom/google/ae/a/i;->d:[Lcom/google/ae/a/i;

    invoke-virtual {v0}, [Lcom/google/ae/a/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ae/a/i;

    return-object v0
.end method
