.class final Lcom/google/ae/a/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/ae/a/s;


# static fields
.field static final synthetic a:Z


# instance fields
.field private final b:Ljava/net/HttpURLConnection;

.field private final c:Lcom/google/ae/a/a;

.field private d:J

.field private e:Lcom/google/ae/a/i;

.field private f:Lcom/google/ae/a/w;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-class v0, Lcom/google/ae/a/e;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/ae/a/g;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/net/HttpURLConnection;Ljava/lang/String;Lcom/google/ae/a/c;Lcom/google/ae/a/a;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const v2, 0x493e0

    const/4 v1, 0x1

    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ae/a/g;->g:I

    .line 99
    iput-object p1, p0, Lcom/google/ae/a/g;->b:Ljava/net/HttpURLConnection;

    .line 101
    :try_start_0
    invoke-virtual {p1, p2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_0

    .line 105
    invoke-virtual {p1, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 106
    invoke-virtual {p1, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 107
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    .line 110
    iput-object p4, p0, Lcom/google/ae/a/g;->c:Lcom/google/ae/a/a;

    .line 111
    if-eqz p4, :cond_0

    .line 112
    invoke-virtual {p1, v1}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 113
    invoke-interface {p4}, Lcom/google/ae/a/a;->g()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-ltz v0, :cond_3

    .line 114
    invoke-interface {p4}, Lcom/google/ae/a/a;->g()J

    move-result-wide v0

    invoke-interface {p4}, Lcom/google/ae/a/a;->e()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 115
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    .line 118
    long-to-int v0, v0

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(I)V

    .line 128
    :cond_0
    :goto_0
    invoke-virtual {p3}, Lcom/google/ae/a/c;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 129
    invoke-virtual {p3, v0}, Lcom/google/ae/a/c;->b(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 130
    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 102
    :catch_0
    move-exception v0

    .line 103
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid http method."

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 120
    :cond_2
    invoke-virtual {p1, v0, v1}, Ljava/net/HttpURLConnection;->setFixedLengthStreamingMode(J)V

    goto :goto_0

    .line 123
    :cond_3
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->setChunkedStreamingMode(I)V

    goto :goto_0

    .line 134
    :cond_4
    sget-object v0, Lcom/google/ae/a/i;->a:Lcom/google/ae/a/i;

    iput-object v0, p0, Lcom/google/ae/a/g;->e:Lcom/google/ae/a/i;

    .line 135
    return-void
.end method

.method static synthetic a(Lcom/google/ae/a/g;)Lcom/google/ae/a/d;
    .locals 1

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/ae/a/g;->c()Lcom/google/ae/a/d;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/google/ae/a/g;)Lcom/google/ae/a/w;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/ae/a/g;->f:Lcom/google/ae/a/w;

    return-object v0
.end method

.method private c()Lcom/google/ae/a/d;
    .locals 11

    .prologue
    const/high16 v10, 0x10000

    const/4 v1, 0x0

    .line 175
    monitor-enter p0

    .line 176
    :try_start_0
    iget-object v0, p0, Lcom/google/ae/a/g;->f:Lcom/google/ae/a/w;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/ae/a/g;->f:Lcom/google/ae/a/w;

    invoke-interface {v0}, Lcom/google/ae/a/w;->a()V

    .line 179
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 181
    invoke-direct {p0}, Lcom/google/ae/a/g;->f()V

    .line 184
    :try_start_1
    iget-object v0, p0, Lcom/google/ae/a/g;->b:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 197
    iget-object v0, p0, Lcom/google/ae/a/g;->c:Lcom/google/ae/a/a;

    if-nez v0, :cond_1

    .line 198
    invoke-direct {p0}, Lcom/google/ae/a/g;->d()Lcom/google/ae/a/d;

    move-result-object v0

    .line 247
    :goto_0
    return-object v0

    .line 179
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 185
    :catch_0
    move-exception v0

    .line 187
    new-instance v1, Lcom/google/ae/a/t;

    sget-object v2, Lcom/google/ae/a/u;->a:Lcom/google/ae/a/u;

    invoke-direct {v1, v2, v0}, Lcom/google/ae/a/t;-><init>(Lcom/google/ae/a/u;Ljava/lang/Throwable;)V

    throw v1

    .line 188
    :catch_1
    move-exception v0

    .line 190
    :try_start_2
    invoke-direct {p0}, Lcom/google/ae/a/g;->d()Lcom/google/ae/a/d;
    :try_end_2
    .catch Lcom/google/ae/a/t; {:try_start_2 .. :try_end_2} :catch_2

    move-result-object v0

    goto :goto_0

    .line 192
    :catch_2
    move-exception v1

    new-instance v1, Lcom/google/ae/a/t;

    sget-object v2, Lcom/google/ae/a/u;->d:Lcom/google/ae/a/u;

    invoke-direct {v1, v2, v0}, Lcom/google/ae/a/t;-><init>(Lcom/google/ae/a/u;Ljava/lang/Throwable;)V

    throw v1

    .line 201
    :cond_1
    :try_start_3
    iget-object v0, p0, Lcom/google/ae/a/g;->b:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4

    move-result-object v3

    move v0, v1

    .line 213
    :cond_2
    :goto_1
    invoke-direct {p0}, Lcom/google/ae/a/g;->e()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 214
    invoke-direct {p0}, Lcom/google/ae/a/g;->f()V

    .line 216
    new-array v4, v10, [B

    move v2, v1

    .line 217
    :goto_2
    if-ge v2, v10, :cond_3

    invoke-direct {p0}, Lcom/google/ae/a/g;->e()Z

    move-result v5

    if-eqz v5, :cond_3

    .line 218
    :try_start_4
    iget-object v5, p0, Lcom/google/ae/a/g;->c:Lcom/google/ae/a/a;

    sub-int v6, v10, v2

    invoke-interface {v5, v4, v2, v6}, Lcom/google/ae/a/a;->a([BII)I

    move-result v5

    .line 221
    iget-wide v6, p0, Lcom/google/ae/a/g;->d:J

    int-to-long v8, v5

    add-long/2addr v6, v8

    iput-wide v6, p0, Lcom/google/ae/a/g;->d:J
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_6

    .line 222
    add-int/2addr v2, v5

    .line 227
    sub-int v6, v2, v5

    :try_start_5
    invoke-virtual {v3, v4, v6, v5}, Ljava/io/OutputStream;->write([BII)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_2

    .line 228
    :catch_3
    move-exception v0

    .line 230
    :try_start_6
    invoke-direct {p0}, Lcom/google/ae/a/g;->d()Lcom/google/ae/a/d;
    :try_end_6
    .catch Lcom/google/ae/a/t; {:try_start_6 .. :try_end_6} :catch_7

    move-result-object v0

    goto :goto_0

    .line 204
    :catch_4
    move-exception v0

    .line 206
    :try_start_7
    invoke-direct {p0}, Lcom/google/ae/a/g;->d()Lcom/google/ae/a/d;
    :try_end_7
    .catch Lcom/google/ae/a/t; {:try_start_7 .. :try_end_7} :catch_5

    move-result-object v0

    goto :goto_0

    .line 208
    :catch_5
    move-exception v1

    new-instance v1, Lcom/google/ae/a/t;

    sget-object v2, Lcom/google/ae/a/u;->d:Lcom/google/ae/a/u;

    invoke-direct {v1, v2, v0}, Lcom/google/ae/a/t;-><init>(Lcom/google/ae/a/u;Ljava/lang/Throwable;)V

    throw v1

    .line 223
    :catch_6
    move-exception v0

    .line 224
    new-instance v1, Lcom/google/ae/a/t;

    sget-object v2, Lcom/google/ae/a/u;->c:Lcom/google/ae/a/u;

    invoke-direct {v1, v2, v0}, Lcom/google/ae/a/t;-><init>(Lcom/google/ae/a/u;Ljava/lang/Throwable;)V

    throw v1

    .line 232
    :catch_7
    move-exception v1

    new-instance v1, Lcom/google/ae/a/t;

    sget-object v2, Lcom/google/ae/a/u;->c:Lcom/google/ae/a/u;

    invoke-direct {v1, v2, v0}, Lcom/google/ae/a/t;-><init>(Lcom/google/ae/a/u;Ljava/lang/Throwable;)V

    throw v1

    .line 235
    :cond_3
    add-int/2addr v0, v2

    .line 238
    iget v2, p0, Lcom/google/ae/a/g;->g:I

    if-le v0, v2, :cond_2

    .line 239
    monitor-enter p0

    .line 240
    :try_start_8
    iget-object v0, p0, Lcom/google/ae/a/g;->f:Lcom/google/ae/a/w;

    if-eqz v0, :cond_4

    .line 241
    iget-object v0, p0, Lcom/google/ae/a/g;->f:Lcom/google/ae/a/w;

    invoke-interface {v0, p0}, Lcom/google/ae/a/w;->a(Lcom/google/ae/a/s;)V

    .line 243
    :cond_4
    monitor-exit p0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_1

    move v0, v1

    .line 244
    goto :goto_1

    .line 243
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 247
    :cond_5
    invoke-direct {p0}, Lcom/google/ae/a/g;->d()Lcom/google/ae/a/d;

    move-result-object v0

    goto/16 :goto_0
.end method

.method private d()Lcom/google/ae/a/d;
    .locals 8

    .prologue
    .line 255
    invoke-direct {p0}, Lcom/google/ae/a/g;->f()V

    .line 257
    :try_start_0
    iget-object v0, p0, Lcom/google/ae/a/g;->b:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v4

    .line 265
    :try_start_1
    iget-object v0, p0, Lcom/google/ae/a/g;->b:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    move-object v2, v0

    .line 272
    :goto_0
    const/4 v0, 0x0

    .line 273
    iget-object v1, p0, Lcom/google/ae/a/g;->b:Ljava/net/HttpURLConnection;

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v5

    .line 274
    if-eqz v5, :cond_3

    .line 275
    new-instance v3, Lcom/google/ae/a/c;

    invoke-direct {v3}, Lcom/google/ae/a/c;-><init>()V

    .line 277
    invoke-interface {v5}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_0
    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 278
    if-eqz v0, :cond_0

    .line 280
    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Collection;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/ae/a/c;->a(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_1

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v1, v3, Lcom/google/ae/a/c;->a:Ljava/util/Map;

    invoke-interface {v1, v0, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 260
    :catch_0
    move-exception v0

    .line 261
    new-instance v1, Lcom/google/ae/a/t;

    sget-object v2, Lcom/google/ae/a/u;->d:Lcom/google/ae/a/u;

    const-string v3, "Error while reading response code."

    invoke-direct {v1, v2, v3, v0}, Lcom/google/ae/a/t;-><init>(Lcom/google/ae/a/u;Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 269
    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/google/ae/a/g;->b:Ljava/net/HttpURLConnection;

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    move-object v2, v0

    goto :goto_0

    .line 280
    :cond_1
    iget-object v7, v3, Lcom/google/ae/a/c;->a:Ljava/util/Map;

    invoke-interface {v7, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_1

    :cond_2
    move-object v0, v3

    .line 285
    :cond_3
    new-instance v1, Lcom/google/ae/a/d;

    invoke-direct {v1, v4, v0, v2}, Lcom/google/ae/a/d;-><init>(ILcom/google/ae/a/c;Ljava/io/InputStream;)V

    return-object v1
.end method

.method private e()Z
    .locals 3

    .prologue
    .line 290
    :try_start_0
    iget-object v0, p0, Lcom/google/ae/a/g;->c:Lcom/google/ae/a/a;

    invoke-interface {v0}, Lcom/google/ae/a/a;->h()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 291
    :catch_0
    move-exception v0

    .line 292
    new-instance v1, Lcom/google/ae/a/t;

    sget-object v2, Lcom/google/ae/a/u;->c:Lcom/google/ae/a/u;

    invoke-direct {v1, v2, v0}, Lcom/google/ae/a/t;-><init>(Lcom/google/ae/a/u;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private declared-synchronized f()V
    .locals 3

    .prologue
    .line 297
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/ae/a/g;->e:Lcom/google/ae/a/i;

    sget-object v1, Lcom/google/ae/a/i;->b:Lcom/google/ae/a/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    .line 300
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 303
    :catch_0
    move-exception v0

    goto :goto_0

    .line 306
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/ae/a/g;->e:Lcom/google/ae/a/i;

    sget-object v1, Lcom/google/ae/a/i;->c:Lcom/google/ae/a/i;

    if-ne v0, v1, :cond_1

    .line 308
    new-instance v0, Lcom/google/ae/a/t;

    sget-object v1, Lcom/google/ae/a/u;->b:Lcom/google/ae/a/u;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/ae/a/t;-><init>(Lcom/google/ae/a/u;Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 297
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 311
    :cond_1
    :try_start_3
    sget-boolean v0, Lcom/google/ae/a/g;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/ae/a/g;->e:Lcom/google/ae/a/i;

    sget-object v1, Lcom/google/ae/a/i;->a:Lcom/google/ae/a/i;

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 312
    :cond_2
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/concurrent/Future;
    .locals 2

    .prologue
    .line 140
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Lcom/google/ae/a/h;

    invoke-direct {v1, p0}, Lcom/google/ae/a/h;-><init>(Lcom/google/ae/a/g;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 165
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    .line 166
    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 167
    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 168
    return-object v0
.end method

.method public final declared-synchronized a(Lcom/google/ae/a/w;I)V
    .locals 1

    .prologue
    .line 359
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/ae/a/g;->f:Lcom/google/ae/a/w;

    .line 360
    if-lez p2, :cond_0

    .line 361
    iput p2, p0, Lcom/google/ae/a/g;->g:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 363
    :cond_0
    monitor-exit p0

    return-void

    .line 359
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 354
    iget-wide v0, p0, Lcom/google/ae/a/g;->d:J

    return-wide v0
.end method
