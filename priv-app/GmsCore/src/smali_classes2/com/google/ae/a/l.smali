.class final enum Lcom/google/ae/a/l;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/ae/a/l;

.field public static final enum b:Lcom/google/ae/a/l;

.field public static final enum c:Lcom/google/ae/a/l;

.field public static final enum d:Lcom/google/ae/a/l;

.field private static final synthetic e:[Lcom/google/ae/a/l;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 22
    new-instance v0, Lcom/google/ae/a/l;

    const-string v1, "PREBODY"

    invoke-direct {v0, v1, v2}, Lcom/google/ae/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ae/a/l;->a:Lcom/google/ae/a/l;

    .line 23
    new-instance v0, Lcom/google/ae/a/l;

    const-string v1, "BODY"

    invoke-direct {v0, v1, v3}, Lcom/google/ae/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ae/a/l;->b:Lcom/google/ae/a/l;

    .line 24
    new-instance v0, Lcom/google/ae/a/l;

    const-string v1, "POSTBODY"

    invoke-direct {v0, v1, v4}, Lcom/google/ae/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ae/a/l;->c:Lcom/google/ae/a/l;

    .line 25
    new-instance v0, Lcom/google/ae/a/l;

    const-string v1, "DONE"

    invoke-direct {v0, v1, v5}, Lcom/google/ae/a/l;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/google/ae/a/l;->d:Lcom/google/ae/a/l;

    .line 21
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/ae/a/l;

    sget-object v1, Lcom/google/ae/a/l;->a:Lcom/google/ae/a/l;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/ae/a/l;->b:Lcom/google/ae/a/l;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/ae/a/l;->c:Lcom/google/ae/a/l;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/ae/a/l;->d:Lcom/google/ae/a/l;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/ae/a/l;->e:[Lcom/google/ae/a/l;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/ae/a/l;
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/google/ae/a/l;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/ae/a/l;

    return-object v0
.end method

.method public static values()[Lcom/google/ae/a/l;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/ae/a/l;->e:[Lcom/google/ae/a/l;

    invoke-virtual {v0}, [Lcom/google/ae/a/l;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/ae/a/l;

    return-object v0
.end method
