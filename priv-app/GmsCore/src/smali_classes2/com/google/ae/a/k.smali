.class final Lcom/google/ae/a/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/ae/a/a;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x8
.end annotation


# instance fields
.field a:Ljava/security/MessageDigest;

.field b:Z

.field private c:Lcom/google/ae/a/l;

.field private d:Lcom/google/ae/a/l;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/google/ae/a/a;

.field private g:[B

.field private h:[B

.field private i:Ljava/lang/String;

.field private j:J

.field private k:J

.field private l:Z

.field private m:I

.field private n:J


# direct methods
.method public constructor <init>(Lcom/google/ae/a/c;Ljava/lang/String;Ljava/lang/String;Lcom/google/ae/a/a;Ljava/security/MessageDigest;Z)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p3, p0, Lcom/google/ae/a/k;->e:Ljava/lang/String;

    .line 63
    iput-object p4, p0, Lcom/google/ae/a/k;->f:Lcom/google/ae/a/a;

    .line 64
    iput-object p5, p0, Lcom/google/ae/a/k;->a:Ljava/security/MessageDigest;

    .line 65
    iput-boolean p6, p0, Lcom/google/ae/a/k;->b:Z

    .line 66
    sget-object v0, Lcom/google/ae/a/l;->a:Lcom/google/ae/a/l;

    iput-object v0, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    .line 69
    invoke-direct {p0, p2, p1}, Lcom/google/ae/a/k;->a(Ljava/lang/String;Lcom/google/ae/a/c;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/ae/a/k;->g:[B

    .line 70
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "--"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/ae/a/k;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-boolean v1, p0, Lcom/google/ae/a/k;->b:Z

    if-nez v1, :cond_1

    const-string v1, "--"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/ae/a/k;->i:Ljava/lang/String;

    .line 73
    invoke-interface {p4}, Lcom/google/ae/a/a;->g()J

    move-result-wide v0

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    if-eqz p6, :cond_4

    .line 74
    :cond_0
    iput-wide v4, p0, Lcom/google/ae/a/k;->n:J

    .line 78
    :goto_1
    return-void

    .line 70
    :cond_1
    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "X-Goog-Hash:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/ae/a/k;->a:Ljava/security/MessageDigest;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/ae/a/k;->a:Ljava/security/MessageDigest;

    invoke-virtual {v1}, Ljava/security/MessageDigest;->getAlgorithm()Ljava/lang/String;

    move-result-object v1

    const-string v2, "md5"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "md5=%s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    :goto_2
    const-string v1, "\r\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "--"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v1, p0, Lcom/google/ae/a/k;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "--"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/google/ae/a/k;->a:Ljava/security/MessageDigest;

    invoke-virtual {v1}, Ljava/security/MessageDigest;->getAlgorithm()Ljava/lang/String;

    move-result-object v1

    const-string v2, "sha-1"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "sha1=%s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 76
    :cond_4
    iget-object v0, p0, Lcom/google/ae/a/k;->g:[B

    array-length v0, v0

    int-to-long v0, v0

    invoke-interface {p4}, Lcom/google/ae/a/a;->g()J

    move-result-wide v2

    add-long/2addr v0, v2

    iget-object v2, p0, Lcom/google/ae/a/k;->i:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    int-to-long v2, v2

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/ae/a/k;->n:J

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Lcom/google/ae/a/c;)[B
    .locals 6

    .prologue
    .line 247
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "--"

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 248
    iget-object v0, p0, Lcom/google/ae/a/k;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 249
    const-string v0, "\r\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    const-string v0, "Content-Type: text/plain"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    const-string v0, "\r\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    const-string v0, "\r\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 253
    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 254
    const-string v0, "\r\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 257
    const-string v0, "--"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 258
    iget-object v0, p0, Lcom/google/ae/a/k;->e:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 259
    const-string v0, "\r\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 260
    invoke-virtual {p2}, Lcom/google/ae/a/c;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 261
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 262
    const-string v3, ": "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 263
    invoke-virtual {p2, v0}, Lcom/google/ae/a/c;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 264
    const-string v0, "\r\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 266
    :cond_0
    iget-object v0, p0, Lcom/google/ae/a/k;->f:Lcom/google/ae/a/a;

    invoke-interface {v0}, Lcom/google/ae/a/a;->g()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-lez v0, :cond_1

    .line 267
    const-string v0, "Content-Length: "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 268
    iget-object v0, p0, Lcom/google/ae/a/k;->f:Lcom/google/ae/a/a;

    invoke-interface {v0}, Lcom/google/ae/a/a;->g()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 269
    const-string v0, "\r\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 271
    :cond_1
    const-string v0, "\r\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 272
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    return-object v0
.end method

.method private b([BII)I
    .locals 6

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/ae/a/k;->g:[B

    array-length v0, v0

    int-to-long v0, v0

    iget-wide v2, p0, Lcom/google/ae/a/k;->j:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 213
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    .line 214
    add-int/lit8 v1, p2, 0x1

    iget-object v3, p0, Lcom/google/ae/a/k;->g:[B

    iget-wide v4, p0, Lcom/google/ae/a/k;->j:J

    long-to-int v4, v4

    add-int/2addr v4, v0

    aget-byte v3, v3, v4

    aput-byte v3, p1, p2

    .line 213
    add-int/lit8 v0, v0, 0x1

    move p2, v1

    goto :goto_0

    .line 217
    :cond_0
    return v2
.end method

.method private c([BII)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 221
    iget-object v1, p0, Lcom/google/ae/a/k;->h:[B

    if-nez v1, :cond_0

    .line 225
    iget-boolean v1, p0, Lcom/google/ae/a/k;->b:Z

    if-nez v1, :cond_1

    .line 226
    iget-object v1, p0, Lcom/google/ae/a/k;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/ae/a/k;->h:[B

    .line 233
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/ae/a/k;->h:[B

    array-length v1, v1

    iget v2, p0, Lcom/google/ae/a/k;->m:I

    sub-int/2addr v1, v2

    invoke-static {p3, v1}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 234
    :goto_1
    if-ge v0, v2, :cond_2

    .line 235
    add-int/lit8 v1, p2, 0x1

    iget-object v3, p0, Lcom/google/ae/a/k;->h:[B

    iget v4, p0, Lcom/google/ae/a/k;->m:I

    add-int/2addr v4, v0

    aget-byte v3, v3, v4

    aput-byte v3, p1, p2

    .line 234
    add-int/lit8 v0, v0, 0x1

    move p2, v1

    goto :goto_1

    .line 228
    :cond_1
    iget-object v1, p0, Lcom/google/ae/a/k;->i:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p0, Lcom/google/ae/a/k;->a:Ljava/security/MessageDigest;

    invoke-virtual {v3}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v3

    const/4 v4, 0x2

    invoke-static {v3, v4}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->getBytes()[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/ae/a/k;->h:[B

    goto :goto_0

    .line 238
    :cond_2
    iget v0, p0, Lcom/google/ae/a/k;->m:I

    add-int/2addr v0, v2

    iput v0, p0, Lcom/google/ae/a/k;->m:I

    .line 239
    return v2
.end method


# virtual methods
.method public final a([BII)I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 82
    array-length v0, p1

    sub-int/2addr v0, p2

    if-lt v0, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "Buffer length must be greater than desired number of bytes."

    invoke-static {v0, v2}, Lcom/google/k/a/ah;->a(ZLjava/lang/Object;)V

    .line 87
    iget-boolean v0, p0, Lcom/google/ae/a/k;->l:Z

    if-eqz v0, :cond_1

    .line 88
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Trying to read from un-rewindable stream."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    move v0, v1

    .line 82
    goto :goto_0

    .line 91
    :cond_1
    if-nez p3, :cond_2

    .line 119
    :goto_1
    return v1

    .line 96
    :cond_2
    iget-wide v2, p0, Lcom/google/ae/a/k;->j:J

    .line 97
    iget-object v0, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    sget-object v4, Lcom/google/ae/a/l;->a:Lcom/google/ae/a/l;

    if-ne v0, v4, :cond_4

    .line 98
    iget-wide v0, p0, Lcom/google/ae/a/k;->j:J

    invoke-direct {p0, p1, p2, p3}, Lcom/google/ae/a/k;->b([BII)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/ae/a/k;->j:J

    .line 99
    iget-wide v0, p0, Lcom/google/ae/a/k;->j:J

    iget-object v4, p0, Lcom/google/ae/a/k;->g:[B

    array-length v4, v4

    int-to-long v4, v4

    cmp-long v0, v0, v4

    if-nez v0, :cond_3

    .line 100
    sget-object v0, Lcom/google/ae/a/l;->b:Lcom/google/ae/a/l;

    iput-object v0, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    .line 119
    :cond_3
    :goto_2
    iget-wide v0, p0, Lcom/google/ae/a/k;->j:J

    sub-long/2addr v0, v2

    long-to-int v1, v0

    goto :goto_1

    .line 102
    :cond_4
    iget-object v0, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    sget-object v4, Lcom/google/ae/a/l;->b:Lcom/google/ae/a/l;

    if-ne v0, v4, :cond_6

    .line 103
    iget-wide v0, p0, Lcom/google/ae/a/k;->j:J

    iget-object v4, p0, Lcom/google/ae/a/k;->f:Lcom/google/ae/a/a;

    invoke-interface {v4, p1, p2, p3}, Lcom/google/ae/a/a;->a([BII)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/ae/a/k;->j:J

    .line 104
    iget-object v0, p0, Lcom/google/ae/a/k;->f:Lcom/google/ae/a/a;

    invoke-interface {v0}, Lcom/google/ae/a/a;->f()J

    move-result-wide v0

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v0, v0, v4

    if-gez v0, :cond_5

    .line 105
    iget-object v0, p0, Lcom/google/ae/a/k;->f:Lcom/google/ae/a/a;

    invoke-interface {v0}, Lcom/google/ae/a/a;->d()V

    .line 107
    :cond_5
    iget-object v0, p0, Lcom/google/ae/a/k;->f:Lcom/google/ae/a/a;

    invoke-interface {v0}, Lcom/google/ae/a/a;->h()Z

    move-result v0

    if-nez v0, :cond_3

    .line 108
    sget-object v0, Lcom/google/ae/a/l;->c:Lcom/google/ae/a/l;

    iput-object v0, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    goto :goto_2

    .line 110
    :cond_6
    iget-object v0, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    sget-object v4, Lcom/google/ae/a/l;->c:Lcom/google/ae/a/l;

    if-ne v0, v4, :cond_7

    .line 111
    iget-wide v0, p0, Lcom/google/ae/a/k;->j:J

    invoke-direct {p0, p1, p2, p3}, Lcom/google/ae/a/k;->c([BII)I

    move-result v4

    int-to-long v4, v4

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/google/ae/a/k;->j:J

    .line 112
    iget v0, p0, Lcom/google/ae/a/k;->m:I

    iget-object v1, p0, Lcom/google/ae/a/k;->h:[B

    array-length v1, v1

    if-ne v0, v1, :cond_3

    .line 113
    sget-object v0, Lcom/google/ae/a/l;->d:Lcom/google/ae/a/l;

    iput-object v0, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    goto :goto_2

    .line 115
    :cond_7
    iget-object v0, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    sget-object v4, Lcom/google/ae/a/l;->d:Lcom/google/ae/a/l;

    if-ne v0, v4, :cond_3

    goto :goto_1
.end method

.method public final a(J)J
    .locals 7

    .prologue
    const/4 v4, 0x0

    const-wide/16 v0, 0x0

    .line 150
    cmp-long v2, p1, v0

    if-nez v2, :cond_0

    .line 192
    :goto_0
    return-wide v0

    .line 155
    :cond_0
    iget-object v2, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    sget-object v3, Lcom/google/ae/a/l;->a:Lcom/google/ae/a/l;

    if-ne v2, v3, :cond_2

    .line 156
    iget-object v0, p0, Lcom/google/ae/a/k;->g:[B

    array-length v0, v0

    int-to-long v0, v0

    iget-wide v2, p0, Lcom/google/ae/a/k;->j:J

    sub-long/2addr v0, v2

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 157
    iget-object v2, p0, Lcom/google/ae/a/k;->g:[B

    array-length v2, v2

    int-to-long v2, v2

    iget-wide v4, p0, Lcom/google/ae/a/k;->j:J

    sub-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-nez v2, :cond_1

    .line 158
    sget-object v2, Lcom/google/ae/a/l;->b:Lcom/google/ae/a/l;

    iput-object v2, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    .line 190
    :cond_1
    :goto_1
    iget-wide v2, p0, Lcom/google/ae/a/k;->j:J

    add-long/2addr v2, v0

    iput-wide v2, p0, Lcom/google/ae/a/k;->j:J

    goto :goto_0

    .line 161
    :cond_2
    iget-object v2, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    sget-object v3, Lcom/google/ae/a/l;->b:Lcom/google/ae/a/l;

    if-ne v2, v3, :cond_4

    .line 162
    long-to-int v0, p1

    .line 163
    new-array v1, v0, [B

    .line 164
    iget-object v2, p0, Lcom/google/ae/a/k;->f:Lcom/google/ae/a/a;

    invoke-interface {v2, v1, v4, v0}, Lcom/google/ae/a/a;->a([BII)I

    move-result v0

    int-to-long v0, v0

    .line 165
    iget-object v2, p0, Lcom/google/ae/a/k;->f:Lcom/google/ae/a/a;

    invoke-interface {v2}, Lcom/google/ae/a/a;->f()J

    move-result-wide v2

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v2, v2, v4

    if-gez v2, :cond_3

    .line 166
    iget-object v2, p0, Lcom/google/ae/a/k;->f:Lcom/google/ae/a/a;

    invoke-interface {v2}, Lcom/google/ae/a/a;->d()V

    .line 168
    :cond_3
    iget-object v2, p0, Lcom/google/ae/a/k;->f:Lcom/google/ae/a/a;

    invoke-interface {v2}, Lcom/google/ae/a/a;->h()Z

    move-result v2

    if-nez v2, :cond_1

    .line 169
    sget-object v2, Lcom/google/ae/a/l;->c:Lcom/google/ae/a/l;

    iput-object v2, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    goto :goto_1

    .line 172
    :cond_4
    iget-object v2, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    sget-object v3, Lcom/google/ae/a/l;->c:Lcom/google/ae/a/l;

    if-ne v2, v3, :cond_6

    .line 173
    iget-object v0, p0, Lcom/google/ae/a/k;->h:[B

    if-nez v0, :cond_5

    .line 177
    iget-object v0, p0, Lcom/google/ae/a/k;->i:Ljava/lang/String;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/ae/a/k;->a:Ljava/security/MessageDigest;

    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    const/4 v3, 0x2

    invoke-static {v2, v3}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/ae/a/k;->h:[B

    .line 181
    :cond_5
    iget-object v0, p0, Lcom/google/ae/a/k;->h:[B

    array-length v0, v0

    iget v1, p0, Lcom/google/ae/a/k;->m:I

    sub-int/2addr v0, v1

    int-to-long v0, v0

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 182
    iget v2, p0, Lcom/google/ae/a/k;->m:I

    int-to-long v2, v2

    add-long/2addr v2, v0

    long-to-int v2, v2

    iput v2, p0, Lcom/google/ae/a/k;->m:I

    .line 183
    iget v2, p0, Lcom/google/ae/a/k;->m:I

    iget-object v3, p0, Lcom/google/ae/a/k;->h:[B

    array-length v3, v3

    if-ne v2, v3, :cond_1

    .line 184
    sget-object v2, Lcom/google/ae/a/l;->d:Lcom/google/ae/a/l;

    iput-object v2, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    goto :goto_1

    .line 187
    :cond_6
    iget-object v2, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    sget-object v3, Lcom/google/ae/a/l;->d:Lcom/google/ae/a/l;

    if-ne v2, v3, :cond_1

    goto/16 :goto_0
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 124
    iget-wide v0, p0, Lcom/google/ae/a/k;->j:J

    iput-wide v0, p0, Lcom/google/ae/a/k;->k:J

    .line 125
    iget-object v0, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    iput-object v0, p0, Lcom/google/ae/a/k;->d:Lcom/google/ae/a/l;

    .line 126
    return-void
.end method

.method public final e()J
    .locals 2

    .prologue
    .line 135
    iget-wide v0, p0, Lcom/google/ae/a/k;->j:J

    return-wide v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 140
    const-wide v0, 0x7fffffffffffffffL

    return-wide v0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 197
    iget-wide v0, p0, Lcom/google/ae/a/k;->n:J

    return-wide v0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/ae/a/k;->c:Lcom/google/ae/a/l;

    sget-object v1, Lcom/google/ae/a/l;->d:Lcom/google/ae/a/l;

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/ae/a/k;->f:Lcom/google/ae/a/a;

    invoke-interface {v0}, Lcom/google/ae/a/a;->i()V

    .line 208
    return-void
.end method
