.class Lcom/google/ae/a/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/ae/a/s;


# static fields
.field static final synthetic a:Z

.field private static final b:[C


# instance fields
.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Lcom/google/ae/a/c;

.field private f:Ljava/lang/String;

.field private final g:Lcom/google/ae/a/a;

.field private final h:Lcom/google/ae/a/b;

.field private final i:Ljava/security/MessageDigest;

.field private final j:Z

.field private k:Lcom/google/ae/a/o;

.field private l:Lcom/google/ae/a/s;

.field private m:Lcom/google/ae/a/w;

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/ae/a/m;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/google/ae/a/m;->a:Z

    .line 27
    const-string v0, "0123456789abcdefghijklmnopqrstuvwxyz"

    invoke-virtual {v0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    sput-object v0, Lcom/google/ae/a/m;->b:[C

    return-void

    .line 24
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/ae/a/c;Lcom/google/ae/a/a;Ljava/lang/String;Lcom/google/ae/a/b;Lcom/google/ae/a/x;)V
    .locals 1

    .prologue
    .line 153
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 154
    iput-object p1, p0, Lcom/google/ae/a/m;->c:Ljava/lang/String;

    .line 155
    iput-object p2, p0, Lcom/google/ae/a/m;->d:Ljava/lang/String;

    .line 156
    if-nez p3, :cond_0

    new-instance p3, Lcom/google/ae/a/c;

    invoke-direct {p3}, Lcom/google/ae/a/c;-><init>()V

    :cond_0
    iput-object p3, p0, Lcom/google/ae/a/m;->e:Lcom/google/ae/a/c;

    .line 157
    if-nez p5, :cond_1

    const-string p5, ""

    :cond_1
    iput-object p5, p0, Lcom/google/ae/a/m;->f:Ljava/lang/String;

    .line 158
    iput-object p6, p0, Lcom/google/ae/a/m;->h:Lcom/google/ae/a/b;

    .line 159
    iput-object p4, p0, Lcom/google/ae/a/m;->g:Lcom/google/ae/a/a;

    .line 160
    sget-object v0, Lcom/google/ae/a/o;->a:Lcom/google/ae/a/o;

    iput-object v0, p0, Lcom/google/ae/a/m;->k:Lcom/google/ae/a/o;

    .line 161
    if-nez p7, :cond_2

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/ae/a/m;->i:Ljava/security/MessageDigest;

    .line 162
    if-nez p7, :cond_3

    const/4 v0, 0x0

    :goto_1
    iput-boolean v0, p0, Lcom/google/ae/a/m;->j:Z

    .line 163
    return-void

    .line 161
    :cond_2
    iget-object v0, p7, Lcom/google/ae/a/x;->a:Ljava/security/MessageDigest;

    goto :goto_0

    .line 162
    :cond_3
    iget-boolean v0, p7, Lcom/google/ae/a/x;->b:Z

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/ae/a/m;)Lcom/google/ae/a/d;
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/ae/a/m;->c()Lcom/google/ae/a/d;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ae/a/c;Lcom/google/ae/a/a;Ljava/lang/String;Lcom/google/ae/a/b;Lcom/google/ae/a/x;)Lcom/google/ae/a/m;
    .locals 8

    .prologue
    .line 133
    new-instance v0, Lcom/google/ae/a/m;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/ae/a/m;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/ae/a/c;Lcom/google/ae/a/a;Ljava/lang/String;Lcom/google/ae/a/b;Lcom/google/ae/a/x;)V

    return-object v0
.end method

.method static synthetic b(Lcom/google/ae/a/m;)Lcom/google/ae/a/w;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/ae/a/m;->m:Lcom/google/ae/a/w;

    return-object v0
.end method

.method private c()Lcom/google/ae/a/d;
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 221
    monitor-enter p0

    .line 222
    :try_start_0
    iget-object v0, p0, Lcom/google/ae/a/m;->m:Lcom/google/ae/a/w;

    if-eqz v0, :cond_0

    .line 223
    iget-object v0, p0, Lcom/google/ae/a/m;->m:Lcom/google/ae/a/w;

    invoke-interface {v0}, Lcom/google/ae/a/w;->a()V

    .line 225
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    invoke-direct {p0}, Lcom/google/ae/a/m;->d()V

    .line 230
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move v0, v7

    :goto_0
    const/16 v3, 0x46

    if-ge v0, v3, :cond_1

    sget-object v3, Lcom/google/ae/a/m;->b:[C

    sget-object v4, Lcom/google/ae/a/m;->b:[C

    array-length v4, v4

    invoke-virtual {v1, v4}, Ljava/util/Random;->nextInt(I)I

    move-result v4

    aget-char v3, v3, v4

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 225
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 230
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 231
    new-instance v1, Lcom/google/ae/a/c;

    invoke-direct {v1}, Lcom/google/ae/a/c;-><init>()V

    .line 232
    new-instance v8, Lcom/google/ae/a/c;

    invoke-direct {v8}, Lcom/google/ae/a/c;-><init>()V

    .line 234
    iget-object v0, p0, Lcom/google/ae/a/m;->e:Lcom/google/ae/a/c;

    invoke-virtual {v0}, Lcom/google/ae/a/c;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 235
    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string v5, "content-"

    invoke-virtual {v4, v5}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 236
    iget-object v4, p0, Lcom/google/ae/a/m;->e:Lcom/google/ae/a/c;

    invoke-virtual {v4, v0}, Lcom/google/ae/a/c;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v0, v4}, Lcom/google/ae/a/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/ae/a/c;

    goto :goto_1

    .line 238
    :cond_2
    iget-object v4, p0, Lcom/google/ae/a/m;->e:Lcom/google/ae/a/c;

    invoke-virtual {v4, v0}, Lcom/google/ae/a/c;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v0, v4}, Lcom/google/ae/a/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/ae/a/c;

    goto :goto_1

    .line 241
    :cond_3
    new-instance v0, Lcom/google/ae/a/k;

    iget-object v2, p0, Lcom/google/ae/a/m;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/ae/a/m;->g:Lcom/google/ae/a/a;

    iget-object v5, p0, Lcom/google/ae/a/m;->i:Ljava/security/MessageDigest;

    iget-boolean v6, p0, Lcom/google/ae/a/m;->j:Z

    invoke-direct/range {v0 .. v6}, Lcom/google/ae/a/k;-><init>(Lcom/google/ae/a/c;Ljava/lang/String;Ljava/lang/String;Lcom/google/ae/a/a;Ljava/security/MessageDigest;Z)V

    .line 246
    const-string v1, "X-Goog-Upload-Protocol"

    const-string v2, "multipart"

    invoke-virtual {v8, v1, v2}, Lcom/google/ae/a/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/ae/a/c;

    .line 247
    const-string v1, "Content-Type"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "multipart/related; boundary="

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lcom/google/ae/a/c;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/ae/a/c;

    .line 248
    iget-object v1, p0, Lcom/google/ae/a/m;->h:Lcom/google/ae/a/b;

    iget-object v2, p0, Lcom/google/ae/a/m;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/ae/a/m;->d:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v8, v0}, Lcom/google/ae/a/b;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/ae/a/c;Lcom/google/ae/a/a;)Lcom/google/ae/a/s;

    move-result-object v0

    .line 250
    monitor-enter p0

    .line 251
    :try_start_1
    new-instance v1, Lcom/google/ae/a/p;

    iget-object v2, p0, Lcom/google/ae/a/m;->m:Lcom/google/ae/a/w;

    invoke-direct {v1, p0, p0, v2}, Lcom/google/ae/a/p;-><init>(Lcom/google/ae/a/m;Lcom/google/ae/a/s;Lcom/google/ae/a/w;)V

    iget v2, p0, Lcom/google/ae/a/m;->n:I

    invoke-interface {v0, v1, v2}, Lcom/google/ae/a/s;->a(Lcom/google/ae/a/w;I)V

    .line 253
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 256
    monitor-enter p0

    .line 259
    :try_start_2
    iput-object v0, p0, Lcom/google/ae/a/m;->l:Lcom/google/ae/a/s;

    .line 260
    invoke-interface {v0}, Lcom/google/ae/a/s;->a()Ljava/util/concurrent/Future;

    move-result-object v0

    .line 261
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 263
    :try_start_3
    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ae/a/v;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_1

    .line 272
    iget-object v1, v0, Lcom/google/ae/a/v;->a:Lcom/google/ae/a/t;

    if-eqz v1, :cond_4

    const/4 v1, 0x1

    :goto_2
    if-eqz v1, :cond_6

    .line 274
    iget-object v1, v0, Lcom/google/ae/a/v;->a:Lcom/google/ae/a/t;

    invoke-virtual {v1}, Lcom/google/ae/a/t;->a()Lcom/google/ae/a/u;

    move-result-object v1

    sget-object v2, Lcom/google/ae/a/u;->b:Lcom/google/ae/a/u;

    if-eq v1, v2, :cond_5

    .line 275
    iget-object v0, v0, Lcom/google/ae/a/v;->a:Lcom/google/ae/a/t;

    throw v0

    .line 253
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 261
    :catchall_2
    move-exception v0

    monitor-exit p0

    throw v0

    .line 264
    :catch_0
    move-exception v0

    .line 266
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected error occurred: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 267
    :catch_1
    move-exception v0

    .line 269
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unexpected error occurred: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/concurrent/ExecutionException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_4
    move v1, v7

    .line 272
    goto :goto_2

    .line 279
    :cond_5
    invoke-direct {p0}, Lcom/google/ae/a/m;->d()V

    .line 281
    :cond_6
    iget-object v0, v0, Lcom/google/ae/a/v;->b:Lcom/google/ae/a/d;

    return-object v0
.end method

.method private declared-synchronized d()V
    .locals 3

    .prologue
    .line 340
    monitor-enter p0

    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/ae/a/m;->k:Lcom/google/ae/a/o;

    sget-object v1, Lcom/google/ae/a/o;->b:Lcom/google/ae/a/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne v0, v1, :cond_0

    .line 343
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 346
    :catch_0
    move-exception v0

    goto :goto_0

    .line 349
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/ae/a/m;->k:Lcom/google/ae/a/o;

    sget-object v1, Lcom/google/ae/a/o;->c:Lcom/google/ae/a/o;

    if-ne v0, v1, :cond_1

    .line 351
    new-instance v0, Lcom/google/ae/a/t;

    sget-object v1, Lcom/google/ae/a/u;->b:Lcom/google/ae/a/u;

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Lcom/google/ae/a/t;-><init>(Lcom/google/ae/a/u;Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 340
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 354
    :cond_1
    :try_start_3
    sget-boolean v0, Lcom/google/ae/a/m;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/ae/a/m;->k:Lcom/google/ae/a/o;

    sget-object v1, Lcom/google/ae/a/o;->a:Lcom/google/ae/a/o;

    if-eq v0, v1, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 355
    :cond_2
    monitor-exit p0

    return-void
.end method


# virtual methods
.method public final a()Ljava/util/concurrent/Future;
    .locals 2

    .prologue
    .line 179
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Lcom/google/ae/a/n;

    invoke-direct {v1, p0}, Lcom/google/ae/a/n;-><init>(Lcom/google/ae/a/m;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 210
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v1

    .line 211
    invoke-interface {v1, v0}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    .line 212
    invoke-interface {v1}, Ljava/util/concurrent/ExecutorService;->shutdown()V

    .line 213
    return-object v0
.end method

.method public final declared-synchronized a(Lcom/google/ae/a/w;I)V
    .locals 2

    .prologue
    .line 167
    monitor-enter p0

    if-lez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    :try_start_0
    const-string v1, "Progress threshold must be greater than 0"

    invoke-static {v0, v1}, Lcom/google/k/a/ah;->a(ZLjava/lang/Object;)V

    .line 168
    iput-object p1, p0, Lcom/google/ae/a/m;->m:Lcom/google/ae/a/w;

    .line 169
    iput p2, p0, Lcom/google/ae/a/m;->n:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    monitor-exit p0

    return-void

    .line 167
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/ae/a/m;->g:Lcom/google/ae/a/a;

    invoke-interface {v0}, Lcom/google/ae/a/a;->e()J

    move-result-wide v0

    return-wide v0
.end method
