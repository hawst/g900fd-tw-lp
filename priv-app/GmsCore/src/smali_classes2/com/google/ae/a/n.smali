.class final Lcom/google/ae/a/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic a:Lcom/google/ae/a/m;


# direct methods
.method constructor <init>(Lcom/google/ae/a/m;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/google/ae/a/n;->a:Lcom/google/ae/a/m;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a()Lcom/google/ae/a/v;
    .locals 4

    .prologue
    .line 186
    :try_start_0
    iget-object v0, p0, Lcom/google/ae/a/n;->a:Lcom/google/ae/a/m;

    invoke-static {v0}, Lcom/google/ae/a/m;->a(Lcom/google/ae/a/m;)Lcom/google/ae/a/d;

    move-result-object v1

    .line 187
    new-instance v0, Lcom/google/ae/a/v;

    invoke-direct {v0, v1}, Lcom/google/ae/a/v;-><init>(Lcom/google/ae/a/d;)V
    :try_end_0
    .catch Lcom/google/ae/a/t; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 194
    :goto_0
    iget-object v2, p0, Lcom/google/ae/a/n;->a:Lcom/google/ae/a/m;

    monitor-enter v2

    .line 195
    :try_start_1
    iget-object v1, p0, Lcom/google/ae/a/n;->a:Lcom/google/ae/a/m;

    invoke-static {v1}, Lcom/google/ae/a/m;->b(Lcom/google/ae/a/m;)Lcom/google/ae/a/w;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 196
    iget-object v1, v0, Lcom/google/ae/a/v;->b:Lcom/google/ae/a/d;

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    .line 197
    iget-object v1, p0, Lcom/google/ae/a/n;->a:Lcom/google/ae/a/m;

    invoke-static {v1}, Lcom/google/ae/a/m;->b(Lcom/google/ae/a/m;)Lcom/google/ae/a/w;

    move-result-object v1

    iget-object v3, p0, Lcom/google/ae/a/n;->a:Lcom/google/ae/a/m;

    iget-object v3, v0, Lcom/google/ae/a/v;->b:Lcom/google/ae/a/d;

    invoke-interface {v1, v3}, Lcom/google/ae/a/w;->a(Lcom/google/ae/a/d;)V

    .line 204
    :cond_0
    :goto_2
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 205
    return-object v0

    .line 188
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 189
    new-instance v0, Lcom/google/ae/a/v;

    invoke-direct {v0, v1}, Lcom/google/ae/a/v;-><init>(Lcom/google/ae/a/t;)V

    goto :goto_0

    .line 190
    :catch_1
    move-exception v0

    .line 191
    new-instance v1, Lcom/google/ae/a/t;

    sget-object v2, Lcom/google/ae/a/u;->f:Lcom/google/ae/a/u;

    invoke-direct {v1, v2, v0}, Lcom/google/ae/a/t;-><init>(Lcom/google/ae/a/u;Ljava/lang/Throwable;)V

    .line 192
    new-instance v0, Lcom/google/ae/a/v;

    invoke-direct {v0, v1}, Lcom/google/ae/a/v;-><init>(Lcom/google/ae/a/t;)V

    goto :goto_0

    .line 196
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 200
    :cond_2
    :try_start_2
    iget-object v1, p0, Lcom/google/ae/a/n;->a:Lcom/google/ae/a/m;

    invoke-static {v1}, Lcom/google/ae/a/m;->b(Lcom/google/ae/a/m;)Lcom/google/ae/a/w;

    move-result-object v1

    iget-object v3, p0, Lcom/google/ae/a/n;->a:Lcom/google/ae/a/m;

    iget-object v3, v0, Lcom/google/ae/a/v;->a:Lcom/google/ae/a/t;

    invoke-interface {v1, v3}, Lcom/google/ae/a/w;->a(Lcom/google/ae/a/t;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 181
    invoke-direct {p0}, Lcom/google/ae/a/n;->a()Lcom/google/ae/a/v;

    move-result-object v0

    return-object v0
.end method
