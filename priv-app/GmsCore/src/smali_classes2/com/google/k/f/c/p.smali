.class public final Lcom/google/k/f/c/p;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/google/k/f/c/k;

.field public e:Lcom/google/k/f/c/q;

.field public f:Lcom/google/k/f/c/o;

.field public g:Lcom/google/k/f/c/m;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 372
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 373
    iput-object v0, p0, Lcom/google/k/f/c/p;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/k/f/c/p;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/k/f/c/p;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/k/f/c/p;->d:Lcom/google/k/f/c/k;

    iput-object v0, p0, Lcom/google/k/f/c/p;->e:Lcom/google/k/f/c/q;

    iput-object v0, p0, Lcom/google/k/f/c/p;->f:Lcom/google/k/f/c/o;

    iput-object v0, p0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    iput-object v0, p0, Lcom/google/k/f/c/p;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/c/p;->cachedSize:I

    .line 374
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 418
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 419
    iget-object v1, p0, Lcom/google/k/f/c/p;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 420
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/k/f/c/p;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 423
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/c/p;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 424
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/c/p;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 427
    :cond_1
    iget-object v1, p0, Lcom/google/k/f/c/p;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 428
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/k/f/c/p;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 431
    :cond_2
    iget-object v1, p0, Lcom/google/k/f/c/p;->d:Lcom/google/k/f/c/k;

    if-eqz v1, :cond_3

    .line 432
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/k/f/c/p;->d:Lcom/google/k/f/c/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 435
    :cond_3
    iget-object v1, p0, Lcom/google/k/f/c/p;->e:Lcom/google/k/f/c/q;

    if-eqz v1, :cond_4

    .line 436
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/k/f/c/p;->e:Lcom/google/k/f/c/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 439
    :cond_4
    iget-object v1, p0, Lcom/google/k/f/c/p;->f:Lcom/google/k/f/c/o;

    if-eqz v1, :cond_5

    .line 440
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/k/f/c/p;->f:Lcom/google/k/f/c/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 443
    :cond_5
    iget-object v1, p0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    if-eqz v1, :cond_6

    .line 444
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 447
    :cond_6
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 327
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/c/p;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/c/p;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/c/p;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/c/p;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/k/f/c/p;->d:Lcom/google/k/f/c/k;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/k/f/c/k;

    invoke-direct {v0}, Lcom/google/k/f/c/k;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/c/p;->d:Lcom/google/k/f/c/k;

    :cond_1
    iget-object v0, p0, Lcom/google/k/f/c/p;->d:Lcom/google/k/f/c/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/k/f/c/p;->e:Lcom/google/k/f/c/q;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/k/f/c/q;

    invoke-direct {v0}, Lcom/google/k/f/c/q;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/c/p;->e:Lcom/google/k/f/c/q;

    :cond_2
    iget-object v0, p0, Lcom/google/k/f/c/p;->e:Lcom/google/k/f/c/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/k/f/c/p;->f:Lcom/google/k/f/c/o;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/k/f/c/o;

    invoke-direct {v0}, Lcom/google/k/f/c/o;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/c/p;->f:Lcom/google/k/f/c/o;

    :cond_3
    iget-object v0, p0, Lcom/google/k/f/c/p;->f:Lcom/google/k/f/c/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/k/f/c/m;

    invoke-direct {v0}, Lcom/google/k/f/c/m;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    :cond_4
    iget-object v0, p0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/k/f/c/p;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 393
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/k/f/c/p;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/c/p;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 396
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/c/p;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 398
    :cond_1
    iget-object v0, p0, Lcom/google/k/f/c/p;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 399
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/k/f/c/p;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 401
    :cond_2
    iget-object v0, p0, Lcom/google/k/f/c/p;->d:Lcom/google/k/f/c/k;

    if-eqz v0, :cond_3

    .line 402
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/k/f/c/p;->d:Lcom/google/k/f/c/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 404
    :cond_3
    iget-object v0, p0, Lcom/google/k/f/c/p;->e:Lcom/google/k/f/c/q;

    if-eqz v0, :cond_4

    .line 405
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/k/f/c/p;->e:Lcom/google/k/f/c/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 407
    :cond_4
    iget-object v0, p0, Lcom/google/k/f/c/p;->f:Lcom/google/k/f/c/o;

    if-eqz v0, :cond_5

    .line 408
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/k/f/c/p;->f:Lcom/google/k/f/c/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 410
    :cond_5
    iget-object v0, p0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    if-eqz v0, :cond_6

    .line 411
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/k/f/c/p;->g:Lcom/google/k/f/c/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 413
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 414
    return-void
.end method
