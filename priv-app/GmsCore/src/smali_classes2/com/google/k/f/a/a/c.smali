.class public final Lcom/google/k/f/a/a/c;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:Z

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 3254
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3255
    iput v0, p0, Lcom/google/k/f/a/a/c;->a:I

    iput v1, p0, Lcom/google/k/f/a/a/c;->b:I

    iput-boolean v0, p0, Lcom/google/k/f/a/a/c;->c:Z

    iput-boolean v0, p0, Lcom/google/k/f/a/a/c;->d:Z

    iput v1, p0, Lcom/google/k/f/a/a/c;->cachedSize:I

    .line 3256
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3321
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3322
    iget v1, p0, Lcom/google/k/f/a/a/c;->a:I

    if-eqz v1, :cond_0

    .line 3323
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/k/f/a/a/c;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3326
    :cond_0
    iget v1, p0, Lcom/google/k/f/a/a/c;->b:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 3327
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/k/f/a/a/c;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3330
    :cond_1
    iget-boolean v1, p0, Lcom/google/k/f/a/a/c;->c:Z

    if-eqz v1, :cond_2

    .line 3331
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/k/f/a/a/c;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3334
    :cond_2
    iget-boolean v1, p0, Lcom/google/k/f/a/a/c;->d:Z

    if-eqz v1, :cond_3

    .line 3335
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/k/f/a/a/c;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3338
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3269
    if-ne p1, p0, :cond_1

    .line 3288
    :cond_0
    :goto_0
    return v0

    .line 3272
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/a/a/c;

    if-nez v2, :cond_2

    move v0, v1

    .line 3273
    goto :goto_0

    .line 3275
    :cond_2
    check-cast p1, Lcom/google/k/f/a/a/c;

    .line 3276
    iget v2, p0, Lcom/google/k/f/a/a/c;->a:I

    iget v3, p1, Lcom/google/k/f/a/a/c;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 3277
    goto :goto_0

    .line 3279
    :cond_3
    iget v2, p0, Lcom/google/k/f/a/a/c;->b:I

    iget v3, p1, Lcom/google/k/f/a/a/c;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 3280
    goto :goto_0

    .line 3282
    :cond_4
    iget-boolean v2, p0, Lcom/google/k/f/a/a/c;->c:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/c;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 3283
    goto :goto_0

    .line 3285
    :cond_5
    iget-boolean v2, p0, Lcom/google/k/f/a/a/c;->d:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/c;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 3286
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 3293
    iget v0, p0, Lcom/google/k/f/a/a/c;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 3295
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/c;->b:I

    add-int/2addr v0, v3

    .line 3296
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/c;->c:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 3297
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/k/f/a/a/c;->d:Z

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 3298
    return v0

    :cond_0
    move v0, v2

    .line 3296
    goto :goto_0

    :cond_1
    move v1, v2

    .line 3297
    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3225
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/a/a/c;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/a/a/c;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/c;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/c;->d:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3304
    iget v0, p0, Lcom/google/k/f/a/a/c;->a:I

    if-eqz v0, :cond_0

    .line 3305
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/k/f/a/a/c;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3307
    :cond_0
    iget v0, p0, Lcom/google/k/f/a/a/c;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 3308
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/k/f/a/a/c;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3310
    :cond_1
    iget-boolean v0, p0, Lcom/google/k/f/a/a/c;->c:Z

    if-eqz v0, :cond_2

    .line 3311
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/k/f/a/a/c;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3313
    :cond_2
    iget-boolean v0, p0, Lcom/google/k/f/a/a/c;->d:Z

    if-eqz v0, :cond_3

    .line 3314
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/k/f/a/a/c;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3316
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3317
    return-void
.end method
