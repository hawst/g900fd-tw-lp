.class public final Lcom/google/k/f/ad;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 614
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 615
    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/ad;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/ad;->b:Ljava/lang/String;

    iput v1, p0, Lcom/google/k/f/ad;->c:I

    iput v1, p0, Lcom/google/k/f/ad;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/ad;->cachedSize:I

    .line 616
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 691
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 692
    iget-object v1, p0, Lcom/google/k/f/ad;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 693
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/k/f/ad;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 696
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/ad;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 697
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/ad;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 700
    :cond_1
    iget v1, p0, Lcom/google/k/f/ad;->c:I

    if-eqz v1, :cond_2

    .line 701
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/k/f/ad;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 704
    :cond_2
    iget v1, p0, Lcom/google/k/f/ad;->d:I

    if-eqz v1, :cond_3

    .line 705
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/k/f/ad;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 708
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 629
    if-ne p1, p0, :cond_1

    .line 656
    :cond_0
    :goto_0
    return v0

    .line 632
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/ad;

    if-nez v2, :cond_2

    move v0, v1

    .line 633
    goto :goto_0

    .line 635
    :cond_2
    check-cast p1, Lcom/google/k/f/ad;

    .line 636
    iget-object v2, p0, Lcom/google/k/f/ad;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 637
    iget-object v2, p1, Lcom/google/k/f/ad;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 638
    goto :goto_0

    .line 640
    :cond_3
    iget-object v2, p0, Lcom/google/k/f/ad;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/ad;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 641
    goto :goto_0

    .line 643
    :cond_4
    iget-object v2, p0, Lcom/google/k/f/ad;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 644
    iget-object v2, p1, Lcom/google/k/f/ad;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 645
    goto :goto_0

    .line 647
    :cond_5
    iget-object v2, p0, Lcom/google/k/f/ad;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/ad;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 648
    goto :goto_0

    .line 650
    :cond_6
    iget v2, p0, Lcom/google/k/f/ad;->c:I

    iget v3, p1, Lcom/google/k/f/ad;->c:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 651
    goto :goto_0

    .line 653
    :cond_7
    iget v2, p0, Lcom/google/k/f/ad;->d:I

    iget v3, p1, Lcom/google/k/f/ad;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 654
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 661
    iget-object v0, p0, Lcom/google/k/f/ad;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 664
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/k/f/ad;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 666
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ad;->c:I

    add-int/2addr v0, v1

    .line 667
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ad;->d:I

    add-int/2addr v0, v1

    .line 668
    return v0

    .line 661
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/ad;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 664
    :cond_1
    iget-object v1, p0, Lcom/google/k/f/ad;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 585
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/ad;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/ad;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ad;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ad;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 674
    iget-object v0, p0, Lcom/google/k/f/ad;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 675
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/k/f/ad;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 677
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/ad;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 678
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/ad;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 680
    :cond_1
    iget v0, p0, Lcom/google/k/f/ad;->c:I

    if-eqz v0, :cond_2

    .line 681
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/k/f/ad;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 683
    :cond_2
    iget v0, p0, Lcom/google/k/f/ad;->d:I

    if-eqz v0, :cond_3

    .line 684
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/k/f/ad;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 686
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 687
    return-void
.end method
