.class public final Lcom/google/k/f/ae;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 787
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 788
    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/ae;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/ae;->b:Ljava/lang/String;

    iput v1, p0, Lcom/google/k/f/ae;->c:I

    iput v1, p0, Lcom/google/k/f/ae;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/ae;->cachedSize:I

    .line 789
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 864
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 865
    iget-object v1, p0, Lcom/google/k/f/ae;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 866
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/k/f/ae;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 869
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/ae;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 870
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/ae;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 873
    :cond_1
    iget v1, p0, Lcom/google/k/f/ae;->c:I

    if-eqz v1, :cond_2

    .line 874
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/k/f/ae;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 877
    :cond_2
    iget v1, p0, Lcom/google/k/f/ae;->d:I

    if-eqz v1, :cond_3

    .line 878
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/k/f/ae;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 881
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 802
    if-ne p1, p0, :cond_1

    .line 829
    :cond_0
    :goto_0
    return v0

    .line 805
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/ae;

    if-nez v2, :cond_2

    move v0, v1

    .line 806
    goto :goto_0

    .line 808
    :cond_2
    check-cast p1, Lcom/google/k/f/ae;

    .line 809
    iget-object v2, p0, Lcom/google/k/f/ae;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 810
    iget-object v2, p1, Lcom/google/k/f/ae;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 811
    goto :goto_0

    .line 813
    :cond_3
    iget-object v2, p0, Lcom/google/k/f/ae;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/ae;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 814
    goto :goto_0

    .line 816
    :cond_4
    iget-object v2, p0, Lcom/google/k/f/ae;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 817
    iget-object v2, p1, Lcom/google/k/f/ae;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 818
    goto :goto_0

    .line 820
    :cond_5
    iget-object v2, p0, Lcom/google/k/f/ae;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/ae;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 821
    goto :goto_0

    .line 823
    :cond_6
    iget v2, p0, Lcom/google/k/f/ae;->c:I

    iget v3, p1, Lcom/google/k/f/ae;->c:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 824
    goto :goto_0

    .line 826
    :cond_7
    iget v2, p0, Lcom/google/k/f/ae;->d:I

    iget v3, p1, Lcom/google/k/f/ae;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 827
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 834
    iget-object v0, p0, Lcom/google/k/f/ae;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 837
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/k/f/ae;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 839
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ae;->c:I

    add-int/2addr v0, v1

    .line 840
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ae;->d:I

    add-int/2addr v0, v1

    .line 841
    return v0

    .line 834
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/ae;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 837
    :cond_1
    iget-object v1, p0, Lcom/google/k/f/ae;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 758
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/ae;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/ae;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ae;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ae;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 847
    iget-object v0, p0, Lcom/google/k/f/ae;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 848
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/k/f/ae;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 850
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/ae;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 851
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/ae;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 853
    :cond_1
    iget v0, p0, Lcom/google/k/f/ae;->c:I

    if-eqz v0, :cond_2

    .line 854
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/k/f/ae;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 856
    :cond_2
    iget v0, p0, Lcom/google/k/f/ae;->d:I

    if-eqz v0, :cond_3

    .line 857
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/k/f/ae;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 859
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 860
    return-void
.end method
