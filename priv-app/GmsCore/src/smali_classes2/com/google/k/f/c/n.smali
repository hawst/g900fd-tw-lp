.class public final Lcom/google/k/f/c/n;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/k/f/c/b;

.field public c:Lcom/google/k/f/c/g;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Float;

.field public f:[Lcom/google/k/f/c/s;

.field public g:Lcom/google/k/f/c/p;

.field public h:[Lcom/google/k/f/c/j;

.field public i:Lcom/google/k/f/c/j;

.field public j:Lcom/google/k/f/c/l;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 72
    iput-object v1, p0, Lcom/google/k/f/c/n;->a:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/k/f/c/n;->b:Lcom/google/k/f/c/b;

    iput-object v1, p0, Lcom/google/k/f/c/n;->c:Lcom/google/k/f/c/g;

    iput-object v1, p0, Lcom/google/k/f/c/n;->d:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/k/f/c/n;->e:Ljava/lang/Float;

    invoke-static {}, Lcom/google/k/f/c/s;->a()[Lcom/google/k/f/c/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    iput-object v1, p0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    invoke-static {}, Lcom/google/k/f/c/j;->a()[Lcom/google/k/f/c/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/c/n;->h:[Lcom/google/k/f/c/j;

    iput-object v1, p0, Lcom/google/k/f/c/n;->i:Lcom/google/k/f/c/j;

    iput-object v1, p0, Lcom/google/k/f/c/n;->j:Lcom/google/k/f/c/l;

    iput-object v1, p0, Lcom/google/k/f/c/n;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/c/n;->cachedSize:I

    .line 73
    return-void
.end method

.method public static a([B)Lcom/google/k/f/c/n;
    .locals 1

    .prologue
    .line 317
    new-instance v0, Lcom/google/k/f/c/n;

    invoke-direct {v0}, Lcom/google/k/f/c/n;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/k/f/c/n;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 139
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 140
    iget-object v2, p0, Lcom/google/k/f/c/n;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 141
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/k/f/c/n;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 144
    :cond_0
    iget-object v2, p0, Lcom/google/k/f/c/n;->b:Lcom/google/k/f/c/b;

    if-eqz v2, :cond_1

    .line 145
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/k/f/c/n;->b:Lcom/google/k/f/c/b;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 148
    :cond_1
    iget-object v2, p0, Lcom/google/k/f/c/n;->c:Lcom/google/k/f/c/g;

    if-eqz v2, :cond_2

    .line 149
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/k/f/c/n;->c:Lcom/google/k/f/c/g;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 152
    :cond_2
    iget-object v2, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 153
    :goto_0
    iget-object v3, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 154
    iget-object v3, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    aget-object v3, v3, v0

    .line 155
    if-eqz v3, :cond_3

    .line 156
    const/4 v4, 0x4

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 153
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 161
    :cond_5
    iget-object v2, p0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    if-eqz v2, :cond_6

    .line 162
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 165
    :cond_6
    iget-object v2, p0, Lcom/google/k/f/c/n;->h:[Lcom/google/k/f/c/j;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/k/f/c/n;->h:[Lcom/google/k/f/c/j;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 166
    :goto_1
    iget-object v2, p0, Lcom/google/k/f/c/n;->h:[Lcom/google/k/f/c/j;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 167
    iget-object v2, p0, Lcom/google/k/f/c/n;->h:[Lcom/google/k/f/c/j;

    aget-object v2, v2, v1

    .line 168
    if-eqz v2, :cond_7

    .line 169
    const/4 v3, 0x6

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 166
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 174
    :cond_8
    iget-object v1, p0, Lcom/google/k/f/c/n;->i:Lcom/google/k/f/c/j;

    if-eqz v1, :cond_9

    .line 175
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/k/f/c/n;->i:Lcom/google/k/f/c/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    :cond_9
    iget-object v1, p0, Lcom/google/k/f/c/n;->j:Lcom/google/k/f/c/l;

    if-eqz v1, :cond_a

    .line 179
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/k/f/c/n;->j:Lcom/google/k/f/c/l;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 182
    :cond_a
    iget-object v1, p0, Lcom/google/k/f/c/n;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 183
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/k/f/c/n;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 186
    :cond_b
    iget-object v1, p0, Lcom/google/k/f/c/n;->e:Ljava/lang/Float;

    if-eqz v1, :cond_c

    .line 187
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/k/f/c/n;->e:Ljava/lang/Float;

    invoke-virtual {v2}, Ljava/lang/Float;->floatValue()F

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 190
    :cond_c
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/c/n;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/c/n;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/k/f/c/n;->b:Lcom/google/k/f/c/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/k/f/c/b;

    invoke-direct {v0}, Lcom/google/k/f/c/b;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/c/n;->b:Lcom/google/k/f/c/b;

    :cond_1
    iget-object v0, p0, Lcom/google/k/f/c/n;->b:Lcom/google/k/f/c/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/k/f/c/n;->c:Lcom/google/k/f/c/g;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/k/f/c/g;

    invoke-direct {v0}, Lcom/google/k/f/c/g;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/c/n;->c:Lcom/google/k/f/c/g;

    :cond_2
    iget-object v0, p0, Lcom/google/k/f/c/n;->c:Lcom/google/k/f/c/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/k/f/c/s;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Lcom/google/k/f/c/s;

    invoke-direct {v3}, Lcom/google/k/f/c/s;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Lcom/google/k/f/c/s;

    invoke-direct {v3}, Lcom/google/k/f/c/s;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/k/f/c/p;

    invoke-direct {v0}, Lcom/google/k/f/c/p;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    :cond_6
    iget-object v0, p0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/k/f/c/n;->h:[Lcom/google/k/f/c/j;

    if-nez v0, :cond_8

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/k/f/c/j;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/k/f/c/n;->h:[Lcom/google/k/f/c/j;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/k/f/c/j;

    invoke-direct {v3}, Lcom/google/k/f/c/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    iget-object v0, p0, Lcom/google/k/f/c/n;->h:[Lcom/google/k/f/c/j;

    array-length v0, v0

    goto :goto_3

    :cond_9
    new-instance v3, Lcom/google/k/f/c/j;

    invoke-direct {v3}, Lcom/google/k/f/c/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/k/f/c/n;->h:[Lcom/google/k/f/c/j;

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/k/f/c/n;->i:Lcom/google/k/f/c/j;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/k/f/c/j;

    invoke-direct {v0}, Lcom/google/k/f/c/j;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/c/n;->i:Lcom/google/k/f/c/j;

    :cond_a
    iget-object v0, p0, Lcom/google/k/f/c/n;->i:Lcom/google/k/f/c/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/k/f/c/n;->j:Lcom/google/k/f/c/l;

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/k/f/c/l;

    invoke-direct {v0}, Lcom/google/k/f/c/l;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/c/n;->j:Lcom/google/k/f/c/l;

    :cond_b
    iget-object v0, p0, Lcom/google/k/f/c/n;->j:Lcom/google/k/f/c/l;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto/16 :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/c/n;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->intBitsToFloat(I)F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/c/n;->e:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x55 -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 94
    iget-object v0, p0, Lcom/google/k/f/c/n;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/k/f/c/n;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 97
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/c/n;->b:Lcom/google/k/f/c/b;

    if-eqz v0, :cond_1

    .line 98
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/k/f/c/n;->b:Lcom/google/k/f/c/b;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 100
    :cond_1
    iget-object v0, p0, Lcom/google/k/f/c/n;->c:Lcom/google/k/f/c/g;

    if-eqz v0, :cond_2

    .line 101
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/k/f/c/n;->c:Lcom/google/k/f/c/g;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 103
    :cond_2
    iget-object v0, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 104
    :goto_0
    iget-object v2, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 105
    iget-object v2, p0, Lcom/google/k/f/c/n;->f:[Lcom/google/k/f/c/s;

    aget-object v2, v2, v0

    .line 106
    if-eqz v2, :cond_3

    .line 107
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 104
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 111
    :cond_4
    iget-object v0, p0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    if-eqz v0, :cond_5

    .line 112
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/k/f/c/n;->g:Lcom/google/k/f/c/p;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 114
    :cond_5
    iget-object v0, p0, Lcom/google/k/f/c/n;->h:[Lcom/google/k/f/c/j;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/k/f/c/n;->h:[Lcom/google/k/f/c/j;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 115
    :goto_1
    iget-object v0, p0, Lcom/google/k/f/c/n;->h:[Lcom/google/k/f/c/j;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 116
    iget-object v0, p0, Lcom/google/k/f/c/n;->h:[Lcom/google/k/f/c/j;

    aget-object v0, v0, v1

    .line 117
    if-eqz v0, :cond_6

    .line 118
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 115
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 122
    :cond_7
    iget-object v0, p0, Lcom/google/k/f/c/n;->i:Lcom/google/k/f/c/j;

    if-eqz v0, :cond_8

    .line 123
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/k/f/c/n;->i:Lcom/google/k/f/c/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 125
    :cond_8
    iget-object v0, p0, Lcom/google/k/f/c/n;->j:Lcom/google/k/f/c/l;

    if-eqz v0, :cond_9

    .line 126
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/k/f/c/n;->j:Lcom/google/k/f/c/l;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 128
    :cond_9
    iget-object v0, p0, Lcom/google/k/f/c/n;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 129
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/k/f/c/n;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 131
    :cond_a
    iget-object v0, p0, Lcom/google/k/f/c/n;->e:Ljava/lang/Float;

    if-eqz v0, :cond_b

    .line 132
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/k/f/c/n;->e:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IF)V

    .line 134
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 135
    return-void
.end method
