.class public final Lcom/google/k/f/a/a/o;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:I

.field public c:I

.field public d:Z

.field public e:Z

.field public f:Z

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 1974
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1975
    iput-boolean v0, p0, Lcom/google/k/f/a/a/o;->a:Z

    iput v0, p0, Lcom/google/k/f/a/a/o;->b:I

    iput v1, p0, Lcom/google/k/f/a/a/o;->c:I

    iput-boolean v0, p0, Lcom/google/k/f/a/a/o;->d:Z

    iput-boolean v0, p0, Lcom/google/k/f/a/a/o;->e:Z

    iput-boolean v0, p0, Lcom/google/k/f/a/a/o;->f:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/a/a/o;->g:Ljava/lang/String;

    iput v1, p0, Lcom/google/k/f/a/a/o;->cachedSize:I

    .line 1976
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2070
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2071
    iget-boolean v1, p0, Lcom/google/k/f/a/a/o;->a:Z

    if-eqz v1, :cond_0

    .line 2072
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/k/f/a/a/o;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2075
    :cond_0
    iget v1, p0, Lcom/google/k/f/a/a/o;->b:I

    if-eqz v1, :cond_1

    .line 2076
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/k/f/a/a/o;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2079
    :cond_1
    iget v1, p0, Lcom/google/k/f/a/a/o;->c:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 2080
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/k/f/a/a/o;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2083
    :cond_2
    iget-boolean v1, p0, Lcom/google/k/f/a/a/o;->d:Z

    if-eqz v1, :cond_3

    .line 2084
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/k/f/a/a/o;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2087
    :cond_3
    iget-boolean v1, p0, Lcom/google/k/f/a/a/o;->e:Z

    if-eqz v1, :cond_4

    .line 2088
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/k/f/a/a/o;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2091
    :cond_4
    iget-boolean v1, p0, Lcom/google/k/f/a/a/o;->f:Z

    if-eqz v1, :cond_5

    .line 2092
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/k/f/a/a/o;->f:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2095
    :cond_5
    iget-object v1, p0, Lcom/google/k/f/a/a/o;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 2096
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/k/f/a/a/o;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2099
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1992
    if-ne p1, p0, :cond_1

    .line 2024
    :cond_0
    :goto_0
    return v0

    .line 1995
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/a/a/o;

    if-nez v2, :cond_2

    move v0, v1

    .line 1996
    goto :goto_0

    .line 1998
    :cond_2
    check-cast p1, Lcom/google/k/f/a/a/o;

    .line 1999
    iget-boolean v2, p0, Lcom/google/k/f/a/a/o;->a:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/o;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 2000
    goto :goto_0

    .line 2002
    :cond_3
    iget v2, p0, Lcom/google/k/f/a/a/o;->b:I

    iget v3, p1, Lcom/google/k/f/a/a/o;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2003
    goto :goto_0

    .line 2005
    :cond_4
    iget v2, p0, Lcom/google/k/f/a/a/o;->c:I

    iget v3, p1, Lcom/google/k/f/a/a/o;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 2006
    goto :goto_0

    .line 2008
    :cond_5
    iget-boolean v2, p0, Lcom/google/k/f/a/a/o;->d:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/o;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 2009
    goto :goto_0

    .line 2011
    :cond_6
    iget-boolean v2, p0, Lcom/google/k/f/a/a/o;->e:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/o;->e:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2012
    goto :goto_0

    .line 2014
    :cond_7
    iget-boolean v2, p0, Lcom/google/k/f/a/a/o;->f:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/o;->f:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 2015
    goto :goto_0

    .line 2017
    :cond_8
    iget-object v2, p0, Lcom/google/k/f/a/a/o;->g:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 2018
    iget-object v2, p1, Lcom/google/k/f/a/a/o;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 2019
    goto :goto_0

    .line 2021
    :cond_9
    iget-object v2, p0, Lcom/google/k/f/a/a/o;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/a/a/o;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2022
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 2029
    iget-boolean v0, p0, Lcom/google/k/f/a/a/o;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2031
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/o;->b:I

    add-int/2addr v0, v3

    .line 2032
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/o;->c:I

    add-int/2addr v0, v3

    .line 2033
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/o;->d:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 2034
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/o;->e:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    .line 2035
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/k/f/a/a/o;->f:Z

    if-eqz v3, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 2036
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/a/a/o;->g:Ljava/lang/String;

    if-nez v0, :cond_4

    const/4 v0, 0x0

    :goto_4
    add-int/2addr v0, v1

    .line 2038
    return v0

    :cond_0
    move v0, v2

    .line 2029
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2033
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2034
    goto :goto_2

    :cond_3
    move v1, v2

    .line 2035
    goto :goto_3

    .line 2036
    :cond_4
    iget-object v0, p0, Lcom/google/k/f/a/a/o;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1936
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/o;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/a/a/o;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/a/a/o;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/o;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/o;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/o;->f:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/a/a/o;->g:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2044
    iget-boolean v0, p0, Lcom/google/k/f/a/a/o;->a:Z

    if-eqz v0, :cond_0

    .line 2045
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/k/f/a/a/o;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2047
    :cond_0
    iget v0, p0, Lcom/google/k/f/a/a/o;->b:I

    if-eqz v0, :cond_1

    .line 2048
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/k/f/a/a/o;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2050
    :cond_1
    iget v0, p0, Lcom/google/k/f/a/a/o;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 2051
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/k/f/a/a/o;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2053
    :cond_2
    iget-boolean v0, p0, Lcom/google/k/f/a/a/o;->d:Z

    if-eqz v0, :cond_3

    .line 2054
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/k/f/a/a/o;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2056
    :cond_3
    iget-boolean v0, p0, Lcom/google/k/f/a/a/o;->e:Z

    if-eqz v0, :cond_4

    .line 2057
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/k/f/a/a/o;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2059
    :cond_4
    iget-boolean v0, p0, Lcom/google/k/f/a/a/o;->f:Z

    if-eqz v0, :cond_5

    .line 2060
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/k/f/a/a/o;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2062
    :cond_5
    iget-object v0, p0, Lcom/google/k/f/a/a/o;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2063
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/k/f/a/a/o;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2065
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2066
    return-void
.end method
