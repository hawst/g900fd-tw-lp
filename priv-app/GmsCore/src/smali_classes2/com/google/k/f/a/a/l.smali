.class public final Lcom/google/k/f/a/a/l;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3435
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3436
    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/a/a/l;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/a/a/l;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/k/f/a/a/l;->c:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/a/a/l;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/a/a/l;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/a/a/l;->f:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/a/a/l;->g:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/a/a/l;->cachedSize:I

    .line 3437
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3556
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3557
    iget-object v1, p0, Lcom/google/k/f/a/a/l;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3558
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/k/f/a/a/l;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3561
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/a/a/l;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3562
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/a/a/l;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3565
    :cond_1
    iget v1, p0, Lcom/google/k/f/a/a/l;->c:I

    if-eqz v1, :cond_2

    .line 3566
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/k/f/a/a/l;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3569
    :cond_2
    iget-object v1, p0, Lcom/google/k/f/a/a/l;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3570
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/k/f/a/a/l;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3573
    :cond_3
    iget-object v1, p0, Lcom/google/k/f/a/a/l;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 3574
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/k/f/a/a/l;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3577
    :cond_4
    iget-object v1, p0, Lcom/google/k/f/a/a/l;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 3578
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/k/f/a/a/l;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3581
    :cond_5
    iget-object v1, p0, Lcom/google/k/f/a/a/l;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 3582
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/k/f/a/a/l;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3585
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3453
    if-ne p1, p0, :cond_1

    .line 3505
    :cond_0
    :goto_0
    return v0

    .line 3456
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/a/a/l;

    if-nez v2, :cond_2

    move v0, v1

    .line 3457
    goto :goto_0

    .line 3459
    :cond_2
    check-cast p1, Lcom/google/k/f/a/a/l;

    .line 3460
    iget-object v2, p0, Lcom/google/k/f/a/a/l;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 3461
    iget-object v2, p1, Lcom/google/k/f/a/a/l;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 3462
    goto :goto_0

    .line 3464
    :cond_3
    iget-object v2, p0, Lcom/google/k/f/a/a/l;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/a/a/l;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 3465
    goto :goto_0

    .line 3467
    :cond_4
    iget-object v2, p0, Lcom/google/k/f/a/a/l;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 3468
    iget-object v2, p1, Lcom/google/k/f/a/a/l;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 3469
    goto :goto_0

    .line 3471
    :cond_5
    iget-object v2, p0, Lcom/google/k/f/a/a/l;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/a/a/l;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 3472
    goto :goto_0

    .line 3474
    :cond_6
    iget v2, p0, Lcom/google/k/f/a/a/l;->c:I

    iget v3, p1, Lcom/google/k/f/a/a/l;->c:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 3475
    goto :goto_0

    .line 3477
    :cond_7
    iget-object v2, p0, Lcom/google/k/f/a/a/l;->d:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 3478
    iget-object v2, p1, Lcom/google/k/f/a/a/l;->d:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 3479
    goto :goto_0

    .line 3481
    :cond_8
    iget-object v2, p0, Lcom/google/k/f/a/a/l;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/a/a/l;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 3482
    goto :goto_0

    .line 3484
    :cond_9
    iget-object v2, p0, Lcom/google/k/f/a/a/l;->e:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 3485
    iget-object v2, p1, Lcom/google/k/f/a/a/l;->e:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 3486
    goto :goto_0

    .line 3488
    :cond_a
    iget-object v2, p0, Lcom/google/k/f/a/a/l;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/a/a/l;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 3489
    goto :goto_0

    .line 3491
    :cond_b
    iget-object v2, p0, Lcom/google/k/f/a/a/l;->f:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 3492
    iget-object v2, p1, Lcom/google/k/f/a/a/l;->f:Ljava/lang/String;

    if-eqz v2, :cond_d

    move v0, v1

    .line 3493
    goto :goto_0

    .line 3495
    :cond_c
    iget-object v2, p0, Lcom/google/k/f/a/a/l;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/a/a/l;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 3496
    goto :goto_0

    .line 3498
    :cond_d
    iget-object v2, p0, Lcom/google/k/f/a/a/l;->g:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 3499
    iget-object v2, p1, Lcom/google/k/f/a/a/l;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 3500
    goto/16 :goto_0

    .line 3502
    :cond_e
    iget-object v2, p0, Lcom/google/k/f/a/a/l;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/a/a/l;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 3503
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3510
    iget-object v0, p0, Lcom/google/k/f/a/a/l;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3513
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/a/a/l;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3515
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/k/f/a/a/l;->c:I

    add-int/2addr v0, v2

    .line 3516
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/a/a/l;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3518
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/a/a/l;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 3520
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/a/a/l;->f:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 3522
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/k/f/a/a/l;->g:Ljava/lang/String;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 3524
    return v0

    .line 3510
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/a/a/l;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3513
    :cond_1
    iget-object v0, p0, Lcom/google/k/f/a/a/l;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3516
    :cond_2
    iget-object v0, p0, Lcom/google/k/f/a/a/l;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 3518
    :cond_3
    iget-object v0, p0, Lcom/google/k/f/a/a/l;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 3520
    :cond_4
    iget-object v0, p0, Lcom/google/k/f/a/a/l;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 3522
    :cond_5
    iget-object v1, p0, Lcom/google/k/f/a/a/l;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3397
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/a/a/l;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/a/a/l;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/a/a/l;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/a/a/l;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/a/a/l;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/a/a/l;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/a/a/l;->g:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3530
    iget-object v0, p0, Lcom/google/k/f/a/a/l;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3531
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/k/f/a/a/l;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3533
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/a/a/l;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3534
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/a/a/l;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3536
    :cond_1
    iget v0, p0, Lcom/google/k/f/a/a/l;->c:I

    if-eqz v0, :cond_2

    .line 3537
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/k/f/a/a/l;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3539
    :cond_2
    iget-object v0, p0, Lcom/google/k/f/a/a/l;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3540
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/k/f/a/a/l;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3542
    :cond_3
    iget-object v0, p0, Lcom/google/k/f/a/a/l;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 3543
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/k/f/a/a/l;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3545
    :cond_4
    iget-object v0, p0, Lcom/google/k/f/a/a/l;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 3546
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/k/f/a/a/l;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3548
    :cond_5
    iget-object v0, p0, Lcom/google/k/f/a/a/l;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 3549
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/k/f/a/a/l;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3551
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3552
    return-void
.end method
