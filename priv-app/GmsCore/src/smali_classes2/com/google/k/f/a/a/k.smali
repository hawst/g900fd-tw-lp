.class public final Lcom/google/k/f/a/a/k;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:I

.field public c:I

.field public d:Z

.field public e:Z

.field public f:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 2916
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2917
    iput-boolean v0, p0, Lcom/google/k/f/a/a/k;->a:Z

    iput v0, p0, Lcom/google/k/f/a/a/k;->b:I

    iput v1, p0, Lcom/google/k/f/a/a/k;->c:I

    iput-boolean v0, p0, Lcom/google/k/f/a/a/k;->d:Z

    iput-boolean v0, p0, Lcom/google/k/f/a/a/k;->e:Z

    iput-boolean v0, p0, Lcom/google/k/f/a/a/k;->f:Z

    iput v1, p0, Lcom/google/k/f/a/a/k;->cachedSize:I

    .line 2918
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2999
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3000
    iget-boolean v1, p0, Lcom/google/k/f/a/a/k;->a:Z

    if-eqz v1, :cond_0

    .line 3001
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/k/f/a/a/k;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3004
    :cond_0
    iget v1, p0, Lcom/google/k/f/a/a/k;->b:I

    if-eqz v1, :cond_1

    .line 3005
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/k/f/a/a/k;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3008
    :cond_1
    iget v1, p0, Lcom/google/k/f/a/a/k;->c:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 3009
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/k/f/a/a/k;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3012
    :cond_2
    iget-boolean v1, p0, Lcom/google/k/f/a/a/k;->d:Z

    if-eqz v1, :cond_3

    .line 3013
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/k/f/a/a/k;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3016
    :cond_3
    iget-boolean v1, p0, Lcom/google/k/f/a/a/k;->e:Z

    if-eqz v1, :cond_4

    .line 3017
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/k/f/a/a/k;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3020
    :cond_4
    iget-boolean v1, p0, Lcom/google/k/f/a/a/k;->f:Z

    if-eqz v1, :cond_5

    .line 3021
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/k/f/a/a/k;->f:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3024
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2933
    if-ne p1, p0, :cond_1

    .line 2958
    :cond_0
    :goto_0
    return v0

    .line 2936
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/a/a/k;

    if-nez v2, :cond_2

    move v0, v1

    .line 2937
    goto :goto_0

    .line 2939
    :cond_2
    check-cast p1, Lcom/google/k/f/a/a/k;

    .line 2940
    iget-boolean v2, p0, Lcom/google/k/f/a/a/k;->a:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/k;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 2941
    goto :goto_0

    .line 2943
    :cond_3
    iget v2, p0, Lcom/google/k/f/a/a/k;->b:I

    iget v3, p1, Lcom/google/k/f/a/a/k;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2944
    goto :goto_0

    .line 2946
    :cond_4
    iget v2, p0, Lcom/google/k/f/a/a/k;->c:I

    iget v3, p1, Lcom/google/k/f/a/a/k;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 2947
    goto :goto_0

    .line 2949
    :cond_5
    iget-boolean v2, p0, Lcom/google/k/f/a/a/k;->d:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/k;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 2950
    goto :goto_0

    .line 2952
    :cond_6
    iget-boolean v2, p0, Lcom/google/k/f/a/a/k;->e:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/k;->e:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2953
    goto :goto_0

    .line 2955
    :cond_7
    iget-boolean v2, p0, Lcom/google/k/f/a/a/k;->f:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/k;->f:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2956
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 2963
    iget-boolean v0, p0, Lcom/google/k/f/a/a/k;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2965
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/k;->b:I

    add-int/2addr v0, v3

    .line 2966
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/k;->c:I

    add-int/2addr v0, v3

    .line 2967
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/k;->d:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 2968
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/k;->e:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    .line 2969
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/k/f/a/a/k;->f:Z

    if-eqz v3, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 2970
    return v0

    :cond_0
    move v0, v2

    .line 2963
    goto :goto_0

    :cond_1
    move v0, v2

    .line 2967
    goto :goto_1

    :cond_2
    move v0, v2

    .line 2968
    goto :goto_2

    :cond_3
    move v1, v2

    .line 2969
    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2881
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/k;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/a/a/k;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/a/a/k;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/k;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/k;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/k;->f:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2976
    iget-boolean v0, p0, Lcom/google/k/f/a/a/k;->a:Z

    if-eqz v0, :cond_0

    .line 2977
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/k/f/a/a/k;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2979
    :cond_0
    iget v0, p0, Lcom/google/k/f/a/a/k;->b:I

    if-eqz v0, :cond_1

    .line 2980
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/k/f/a/a/k;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2982
    :cond_1
    iget v0, p0, Lcom/google/k/f/a/a/k;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 2983
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/k/f/a/a/k;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2985
    :cond_2
    iget-boolean v0, p0, Lcom/google/k/f/a/a/k;->d:Z

    if-eqz v0, :cond_3

    .line 2986
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/k/f/a/a/k;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2988
    :cond_3
    iget-boolean v0, p0, Lcom/google/k/f/a/a/k;->e:Z

    if-eqz v0, :cond_4

    .line 2989
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/k/f/a/a/k;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2991
    :cond_4
    iget-boolean v0, p0, Lcom/google/k/f/a/a/k;->f:Z

    if-eqz v0, :cond_5

    .line 2992
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/k/f/a/a/k;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2994
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2995
    return-void
.end method
