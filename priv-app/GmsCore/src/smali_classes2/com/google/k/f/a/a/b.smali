.class public final Lcom/google/k/f/a/a/b;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Z

.field public e:Z

.field public f:I

.field public g:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 456
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 457
    iput v0, p0, Lcom/google/k/f/a/a/b;->a:I

    iput v0, p0, Lcom/google/k/f/a/a/b;->b:I

    iput v0, p0, Lcom/google/k/f/a/a/b;->c:I

    iput-boolean v0, p0, Lcom/google/k/f/a/a/b;->d:Z

    iput-boolean v0, p0, Lcom/google/k/f/a/a/b;->e:Z

    iput v0, p0, Lcom/google/k/f/a/a/b;->f:I

    iput-boolean v0, p0, Lcom/google/k/f/a/a/b;->g:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/a/a/b;->cachedSize:I

    .line 458
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 547
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 548
    iget v1, p0, Lcom/google/k/f/a/a/b;->a:I

    if-eqz v1, :cond_0

    .line 549
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/k/f/a/a/b;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 552
    :cond_0
    iget v1, p0, Lcom/google/k/f/a/a/b;->b:I

    if-eqz v1, :cond_1

    .line 553
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/k/f/a/a/b;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 556
    :cond_1
    iget v1, p0, Lcom/google/k/f/a/a/b;->c:I

    if-eqz v1, :cond_2

    .line 557
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/k/f/a/a/b;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 560
    :cond_2
    iget-boolean v1, p0, Lcom/google/k/f/a/a/b;->d:Z

    if-eqz v1, :cond_3

    .line 561
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/k/f/a/a/b;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 564
    :cond_3
    iget-boolean v1, p0, Lcom/google/k/f/a/a/b;->e:Z

    if-eqz v1, :cond_4

    .line 565
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/k/f/a/a/b;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 568
    :cond_4
    iget v1, p0, Lcom/google/k/f/a/a/b;->f:I

    if-eqz v1, :cond_5

    .line 569
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/k/f/a/a/b;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 572
    :cond_5
    iget-boolean v1, p0, Lcom/google/k/f/a/a/b;->g:Z

    if-eqz v1, :cond_6

    .line 573
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/k/f/a/a/b;->g:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 576
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 474
    if-ne p1, p0, :cond_1

    .line 502
    :cond_0
    :goto_0
    return v0

    .line 477
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/a/a/b;

    if-nez v2, :cond_2

    move v0, v1

    .line 478
    goto :goto_0

    .line 480
    :cond_2
    check-cast p1, Lcom/google/k/f/a/a/b;

    .line 481
    iget v2, p0, Lcom/google/k/f/a/a/b;->a:I

    iget v3, p1, Lcom/google/k/f/a/a/b;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 482
    goto :goto_0

    .line 484
    :cond_3
    iget v2, p0, Lcom/google/k/f/a/a/b;->b:I

    iget v3, p1, Lcom/google/k/f/a/a/b;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 485
    goto :goto_0

    .line 487
    :cond_4
    iget v2, p0, Lcom/google/k/f/a/a/b;->c:I

    iget v3, p1, Lcom/google/k/f/a/a/b;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 488
    goto :goto_0

    .line 490
    :cond_5
    iget-boolean v2, p0, Lcom/google/k/f/a/a/b;->d:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/b;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 491
    goto :goto_0

    .line 493
    :cond_6
    iget-boolean v2, p0, Lcom/google/k/f/a/a/b;->e:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/b;->e:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 494
    goto :goto_0

    .line 496
    :cond_7
    iget v2, p0, Lcom/google/k/f/a/a/b;->f:I

    iget v3, p1, Lcom/google/k/f/a/a/b;->f:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 497
    goto :goto_0

    .line 499
    :cond_8
    iget-boolean v2, p0, Lcom/google/k/f/a/a/b;->g:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/b;->g:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 500
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 507
    iget v0, p0, Lcom/google/k/f/a/a/b;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 509
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/b;->b:I

    add-int/2addr v0, v3

    .line 510
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/b;->c:I

    add-int/2addr v0, v3

    .line 511
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/b;->d:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 512
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/b;->e:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 513
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/b;->f:I

    add-int/2addr v0, v3

    .line 514
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/k/f/a/a/b;->g:Z

    if-eqz v3, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 515
    return v0

    :cond_0
    move v0, v2

    .line 511
    goto :goto_0

    :cond_1
    move v0, v2

    .line 512
    goto :goto_1

    :cond_2
    move v1, v2

    .line 514
    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 404
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/a/a/b;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/k/f/a/a/b;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    iput v0, p0, Lcom/google/k/f/a/a/b;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/b;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/b;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    :pswitch_3
    iput v0, p0, Lcom/google/k/f/a/a/b;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/b;->g:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 521
    iget v0, p0, Lcom/google/k/f/a/a/b;->a:I

    if-eqz v0, :cond_0

    .line 522
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/k/f/a/a/b;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 524
    :cond_0
    iget v0, p0, Lcom/google/k/f/a/a/b;->b:I

    if-eqz v0, :cond_1

    .line 525
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/k/f/a/a/b;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 527
    :cond_1
    iget v0, p0, Lcom/google/k/f/a/a/b;->c:I

    if-eqz v0, :cond_2

    .line 528
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/k/f/a/a/b;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 530
    :cond_2
    iget-boolean v0, p0, Lcom/google/k/f/a/a/b;->d:Z

    if-eqz v0, :cond_3

    .line 531
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/k/f/a/a/b;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 533
    :cond_3
    iget-boolean v0, p0, Lcom/google/k/f/a/a/b;->e:Z

    if-eqz v0, :cond_4

    .line 534
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/k/f/a/a/b;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 536
    :cond_4
    iget v0, p0, Lcom/google/k/f/a/a/b;->f:I

    if-eqz v0, :cond_5

    .line 537
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/k/f/a/a/b;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 539
    :cond_5
    iget-boolean v0, p0, Lcom/google/k/f/a/a/b;->g:Z

    if-eqz v0, :cond_6

    .line 540
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/k/f/a/a/b;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 542
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 543
    return-void
.end method
