.class public final Lcom/google/k/f/af;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:I

.field public e:Z

.field public f:Z

.field public g:Lcom/google/k/f/ag;

.field public h:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1123
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1124
    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/af;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/af;->b:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/k/f/af;->c:Z

    iput v1, p0, Lcom/google/k/f/af;->d:I

    iput-boolean v1, p0, Lcom/google/k/f/af;->e:Z

    iput-boolean v1, p0, Lcom/google/k/f/af;->f:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    iput v1, p0, Lcom/google/k/f/af;->h:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/af;->cachedSize:I

    .line 1125
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1239
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1240
    iget-object v1, p0, Lcom/google/k/f/af;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1241
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/k/f/af;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1244
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/af;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1245
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/af;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1248
    :cond_1
    iget-boolean v1, p0, Lcom/google/k/f/af;->c:Z

    if-eqz v1, :cond_2

    .line 1249
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/k/f/af;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1252
    :cond_2
    iget v1, p0, Lcom/google/k/f/af;->d:I

    if-eqz v1, :cond_3

    .line 1253
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/k/f/af;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1256
    :cond_3
    iget-boolean v1, p0, Lcom/google/k/f/af;->e:Z

    if-eqz v1, :cond_4

    .line 1257
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/k/f/af;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1260
    :cond_4
    iget-boolean v1, p0, Lcom/google/k/f/af;->f:Z

    if-eqz v1, :cond_5

    .line 1261
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/k/f/af;->f:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1264
    :cond_5
    iget-object v1, p0, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    if-eqz v1, :cond_6

    .line 1265
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1268
    :cond_6
    iget v1, p0, Lcom/google/k/f/af;->h:I

    if-eqz v1, :cond_7

    .line 1269
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/k/f/af;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1272
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1142
    if-ne p1, p0, :cond_1

    .line 1187
    :cond_0
    :goto_0
    return v0

    .line 1145
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/af;

    if-nez v2, :cond_2

    move v0, v1

    .line 1146
    goto :goto_0

    .line 1148
    :cond_2
    check-cast p1, Lcom/google/k/f/af;

    .line 1149
    iget-object v2, p0, Lcom/google/k/f/af;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 1150
    iget-object v2, p1, Lcom/google/k/f/af;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1151
    goto :goto_0

    .line 1153
    :cond_3
    iget-object v2, p0, Lcom/google/k/f/af;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/af;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1154
    goto :goto_0

    .line 1156
    :cond_4
    iget-object v2, p0, Lcom/google/k/f/af;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1157
    iget-object v2, p1, Lcom/google/k/f/af;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 1158
    goto :goto_0

    .line 1160
    :cond_5
    iget-object v2, p0, Lcom/google/k/f/af;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/af;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1161
    goto :goto_0

    .line 1163
    :cond_6
    iget-boolean v2, p0, Lcom/google/k/f/af;->c:Z

    iget-boolean v3, p1, Lcom/google/k/f/af;->c:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1164
    goto :goto_0

    .line 1166
    :cond_7
    iget v2, p0, Lcom/google/k/f/af;->d:I

    iget v3, p1, Lcom/google/k/f/af;->d:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1167
    goto :goto_0

    .line 1169
    :cond_8
    iget-boolean v2, p0, Lcom/google/k/f/af;->e:Z

    iget-boolean v3, p1, Lcom/google/k/f/af;->e:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1170
    goto :goto_0

    .line 1172
    :cond_9
    iget-boolean v2, p0, Lcom/google/k/f/af;->f:Z

    iget-boolean v3, p1, Lcom/google/k/f/af;->f:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 1173
    goto :goto_0

    .line 1175
    :cond_a
    iget-object v2, p0, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    if-nez v2, :cond_b

    .line 1176
    iget-object v2, p1, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    if-eqz v2, :cond_c

    move v0, v1

    .line 1177
    goto :goto_0

    .line 1180
    :cond_b
    iget-object v2, p0, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    iget-object v3, p1, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    invoke-virtual {v2, v3}, Lcom/google/k/f/ag;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 1181
    goto :goto_0

    .line 1184
    :cond_c
    iget v2, p0, Lcom/google/k/f/af;->h:I

    iget v3, p1, Lcom/google/k/f/af;->h:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1185
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 1192
    iget-object v0, p0, Lcom/google/k/f/af;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1195
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/af;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 1197
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/af;->c:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    add-int/2addr v0, v4

    .line 1198
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/k/f/af;->d:I

    add-int/2addr v0, v4

    .line 1199
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/af;->e:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    add-int/2addr v0, v4

    .line 1200
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/k/f/af;->f:Z

    if-eqz v4, :cond_4

    :goto_4
    add-int/2addr v0, v2

    .line 1201
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 1203
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/af;->h:I

    add-int/2addr v0, v1

    .line 1204
    return v0

    .line 1192
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/af;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1195
    :cond_1
    iget-object v0, p0, Lcom/google/k/f/af;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    move v0, v3

    .line 1197
    goto :goto_2

    :cond_3
    move v0, v3

    .line 1199
    goto :goto_3

    :cond_4
    move v2, v3

    .line 1200
    goto :goto_4

    .line 1201
    :cond_5
    iget-object v1, p0, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    invoke-virtual {v1}, Lcom/google/k/f/ag;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1072
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/af;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/af;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/af;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/af;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/af;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/af;->f:Z

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/k/f/ag;

    invoke-direct {v0}, Lcom/google/k/f/ag;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    :cond_1
    iget-object v0, p0, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/af;->h:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1210
    iget-object v0, p0, Lcom/google/k/f/af;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1211
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/k/f/af;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1213
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/af;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1214
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/af;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1216
    :cond_1
    iget-boolean v0, p0, Lcom/google/k/f/af;->c:Z

    if-eqz v0, :cond_2

    .line 1217
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/k/f/af;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1219
    :cond_2
    iget v0, p0, Lcom/google/k/f/af;->d:I

    if-eqz v0, :cond_3

    .line 1220
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/k/f/af;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1222
    :cond_3
    iget-boolean v0, p0, Lcom/google/k/f/af;->e:Z

    if-eqz v0, :cond_4

    .line 1223
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/k/f/af;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1225
    :cond_4
    iget-boolean v0, p0, Lcom/google/k/f/af;->f:Z

    if-eqz v0, :cond_5

    .line 1226
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/k/f/af;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1228
    :cond_5
    iget-object v0, p0, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    if-eqz v0, :cond_6

    .line 1229
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1231
    :cond_6
    iget v0, p0, Lcom/google/k/f/af;->h:I

    if-eqz v0, :cond_7

    .line 1232
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/k/f/af;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1234
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1235
    return-void
.end method
