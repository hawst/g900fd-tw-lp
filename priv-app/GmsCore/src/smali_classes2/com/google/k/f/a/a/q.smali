.class public final Lcom/google/k/f/a/a/q;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:I

.field public g:J

.field public h:J

.field public i:D

.field public j:D


# direct methods
.method public constructor <init>()V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const/4 v4, -0x1

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    .line 2223
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2224
    iput v1, p0, Lcom/google/k/f/a/a/q;->a:I

    iput v4, p0, Lcom/google/k/f/a/a/q;->b:I

    iput v1, p0, Lcom/google/k/f/a/a/q;->c:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/a/a/q;->d:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/k/f/a/a/q;->e:Z

    iput v1, p0, Lcom/google/k/f/a/a/q;->f:I

    iput-wide v6, p0, Lcom/google/k/f/a/a/q;->g:J

    iput-wide v6, p0, Lcom/google/k/f/a/a/q;->h:J

    iput-wide v2, p0, Lcom/google/k/f/a/a/q;->i:D

    iput-wide v2, p0, Lcom/google/k/f/a/a/q;->j:D

    iput v4, p0, Lcom/google/k/f/a/a/q;->cachedSize:I

    .line 2225
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 8

    .prologue
    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    .line 2359
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2360
    iget v1, p0, Lcom/google/k/f/a/a/q;->a:I

    if-eqz v1, :cond_0

    .line 2361
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/k/f/a/a/q;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2364
    :cond_0
    iget v1, p0, Lcom/google/k/f/a/a/q;->b:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 2365
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/k/f/a/a/q;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2368
    :cond_1
    iget v1, p0, Lcom/google/k/f/a/a/q;->c:I

    if-eqz v1, :cond_2

    .line 2369
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/k/f/a/a/q;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2372
    :cond_2
    iget-object v1, p0, Lcom/google/k/f/a/a/q;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 2373
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/k/f/a/a/q;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2376
    :cond_3
    iget-boolean v1, p0, Lcom/google/k/f/a/a/q;->e:Z

    if-eqz v1, :cond_4

    .line 2377
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/k/f/a/a/q;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2380
    :cond_4
    iget v1, p0, Lcom/google/k/f/a/a/q;->f:I

    if-eqz v1, :cond_5

    .line 2381
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/k/f/a/a/q;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2384
    :cond_5
    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->g:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_6

    .line 2385
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->g:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2388
    :cond_6
    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->h:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_7

    .line 2389
    const/16 v1, 0x8

    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->h:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2392
    :cond_7
    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->i:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_8

    .line 2394
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->i:D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 2397
    :cond_8
    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->j:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    invoke-static {v6, v7}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 2399
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->j:D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 2402
    :cond_9
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2244
    if-ne p1, p0, :cond_1

    .line 2291
    :cond_0
    :goto_0
    return v0

    .line 2247
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/a/a/q;

    if-nez v2, :cond_2

    move v0, v1

    .line 2248
    goto :goto_0

    .line 2250
    :cond_2
    check-cast p1, Lcom/google/k/f/a/a/q;

    .line 2251
    iget v2, p0, Lcom/google/k/f/a/a/q;->a:I

    iget v3, p1, Lcom/google/k/f/a/a/q;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 2252
    goto :goto_0

    .line 2254
    :cond_3
    iget v2, p0, Lcom/google/k/f/a/a/q;->b:I

    iget v3, p1, Lcom/google/k/f/a/a/q;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 2255
    goto :goto_0

    .line 2257
    :cond_4
    iget v2, p0, Lcom/google/k/f/a/a/q;->c:I

    iget v3, p1, Lcom/google/k/f/a/a/q;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 2258
    goto :goto_0

    .line 2260
    :cond_5
    iget-object v2, p0, Lcom/google/k/f/a/a/q;->d:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 2261
    iget-object v2, p1, Lcom/google/k/f/a/a/q;->d:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 2262
    goto :goto_0

    .line 2264
    :cond_6
    iget-object v2, p0, Lcom/google/k/f/a/a/q;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/a/a/q;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 2265
    goto :goto_0

    .line 2267
    :cond_7
    iget-boolean v2, p0, Lcom/google/k/f/a/a/q;->e:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/q;->e:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 2268
    goto :goto_0

    .line 2270
    :cond_8
    iget v2, p0, Lcom/google/k/f/a/a/q;->f:I

    iget v3, p1, Lcom/google/k/f/a/a/q;->f:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 2271
    goto :goto_0

    .line 2273
    :cond_9
    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->g:J

    iget-wide v4, p1, Lcom/google/k/f/a/a/q;->g:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_a

    move v0, v1

    .line 2274
    goto :goto_0

    .line 2276
    :cond_a
    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->h:J

    iget-wide v4, p1, Lcom/google/k/f/a/a/q;->h:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_b

    move v0, v1

    .line 2277
    goto :goto_0

    .line 2280
    :cond_b
    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->i:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 2281
    iget-wide v4, p1, Lcom/google/k/f/a/a/q;->i:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    move v0, v1

    .line 2282
    goto :goto_0

    .line 2286
    :cond_c
    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->j:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 2287
    iget-wide v4, p1, Lcom/google/k/f/a/a/q;->j:D

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v4

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 2288
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 2296
    iget v0, p0, Lcom/google/k/f/a/a/q;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 2298
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/a/a/q;->b:I

    add-int/2addr v0, v1

    .line 2299
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/a/a/q;->c:I

    add-int/2addr v0, v1

    .line 2300
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/a/a/q;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 2302
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/q;->e:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v1

    .line 2303
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/a/a/q;->f:I

    add-int/2addr v0, v1

    .line 2304
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->g:J

    iget-wide v4, p0, Lcom/google/k/f/a/a/q;->g:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 2306
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->h:J

    iget-wide v4, p0, Lcom/google/k/f/a/a/q;->h:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 2309
    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->i:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 2310
    mul-int/lit8 v0, v0, 0x1f

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 2313
    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->j:D

    invoke-static {v2, v3}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    .line 2314
    mul-int/lit8 v0, v0, 0x1f

    ushr-long v4, v2, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 2316
    return v0

    .line 2300
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/a/a/q;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2302
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 2170
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/a/a/q;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/a/a/q;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/k/f/a/a/q;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/a/a/q;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/q;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/a/a/q;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/k/f/a/a/q;->g:J

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/k/f/a/a/q;->h:J

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/k/f/a/a/q;->i:D

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/k/f/a/a/q;->j:D

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x49 -> :sswitch_9
        0x51 -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x0

    const-wide/16 v4, 0x0

    .line 2322
    iget v0, p0, Lcom/google/k/f/a/a/q;->a:I

    if-eqz v0, :cond_0

    .line 2323
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/k/f/a/a/q;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2325
    :cond_0
    iget v0, p0, Lcom/google/k/f/a/a/q;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 2326
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/k/f/a/a/q;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2328
    :cond_1
    iget v0, p0, Lcom/google/k/f/a/a/q;->c:I

    if-eqz v0, :cond_2

    .line 2329
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/k/f/a/a/q;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2331
    :cond_2
    iget-object v0, p0, Lcom/google/k/f/a/a/q;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2332
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/k/f/a/a/q;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2334
    :cond_3
    iget-boolean v0, p0, Lcom/google/k/f/a/a/q;->e:Z

    if-eqz v0, :cond_4

    .line 2335
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/k/f/a/a/q;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2337
    :cond_4
    iget v0, p0, Lcom/google/k/f/a/a/q;->f:I

    if-eqz v0, :cond_5

    .line 2338
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/k/f/a/a/q;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2340
    :cond_5
    iget-wide v0, p0, Lcom/google/k/f/a/a/q;->g:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_6

    .line 2341
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->g:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2343
    :cond_6
    iget-wide v0, p0, Lcom/google/k/f/a/a/q;->h:J

    cmp-long v0, v0, v6

    if-eqz v0, :cond_7

    .line 2344
    const/16 v0, 0x8

    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->h:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2346
    :cond_7
    iget-wide v0, p0, Lcom/google/k/f/a/a/q;->i:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_8

    .line 2348
    const/16 v0, 0x9

    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->i:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 2350
    :cond_8
    iget-wide v0, p0, Lcom/google/k/f/a/a/q;->j:D

    invoke-static {v0, v1}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v0

    invoke-static {v4, v5}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_9

    .line 2352
    const/16 v0, 0xa

    iget-wide v2, p0, Lcom/google/k/f/a/a/q;->j:D

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 2354
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2355
    return-void
.end method
