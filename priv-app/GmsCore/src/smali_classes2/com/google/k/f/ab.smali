.class public final Lcom/google/k/f/ab;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/k/f/aa;

.field public b:Lcom/google/k/f/ac;

.field public c:Lcom/google/k/f/ad;

.field public d:Lcom/google/k/f/ae;

.field public e:I

.field public f:Z

.field public g:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 46
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 47
    iput-object v0, p0, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    iput-object v0, p0, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    iput-object v0, p0, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    iput-object v0, p0, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    iput v1, p0, Lcom/google/k/f/ab;->e:I

    iput-boolean v1, p0, Lcom/google/k/f/ab;->f:Z

    iput-boolean v1, p0, Lcom/google/k/f/ab;->g:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/ab;->cachedSize:I

    .line 48
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 165
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 166
    iget-object v1, p0, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    if-eqz v1, :cond_0

    .line 167
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 170
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    if-eqz v1, :cond_1

    .line 171
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 174
    :cond_1
    iget-object v1, p0, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    if-eqz v1, :cond_2

    .line 175
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 178
    :cond_2
    iget-object v1, p0, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    if-eqz v1, :cond_3

    .line 179
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 182
    :cond_3
    iget v1, p0, Lcom/google/k/f/ab;->e:I

    if-eqz v1, :cond_4

    .line 183
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/k/f/ab;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 186
    :cond_4
    iget-boolean v1, p0, Lcom/google/k/f/ab;->f:Z

    if-eqz v1, :cond_5

    .line 187
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/k/f/ab;->f:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 190
    :cond_5
    iget-boolean v1, p0, Lcom/google/k/f/ab;->g:Z

    if-eqz v1, :cond_6

    .line 191
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/k/f/ab;->g:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 194
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64
    if-ne p1, p0, :cond_1

    .line 116
    :cond_0
    :goto_0
    return v0

    .line 67
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/ab;

    if-nez v2, :cond_2

    move v0, v1

    .line 68
    goto :goto_0

    .line 70
    :cond_2
    check-cast p1, Lcom/google/k/f/ab;

    .line 71
    iget-object v2, p0, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    if-nez v2, :cond_3

    .line 72
    iget-object v2, p1, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    if-eqz v2, :cond_4

    move v0, v1

    .line 73
    goto :goto_0

    .line 76
    :cond_3
    iget-object v2, p0, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    iget-object v3, p1, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    invoke-virtual {v2, v3}, Lcom/google/k/f/aa;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 77
    goto :goto_0

    .line 80
    :cond_4
    iget-object v2, p0, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    if-nez v2, :cond_5

    .line 81
    iget-object v2, p1, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    if-eqz v2, :cond_6

    move v0, v1

    .line 82
    goto :goto_0

    .line 85
    :cond_5
    iget-object v2, p0, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    iget-object v3, p1, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    invoke-virtual {v2, v3}, Lcom/google/k/f/ac;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 86
    goto :goto_0

    .line 89
    :cond_6
    iget-object v2, p0, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    if-nez v2, :cond_7

    .line 90
    iget-object v2, p1, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    if-eqz v2, :cond_8

    move v0, v1

    .line 91
    goto :goto_0

    .line 94
    :cond_7
    iget-object v2, p0, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    iget-object v3, p1, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    invoke-virtual {v2, v3}, Lcom/google/k/f/ad;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 95
    goto :goto_0

    .line 98
    :cond_8
    iget-object v2, p0, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    if-nez v2, :cond_9

    .line 99
    iget-object v2, p1, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    if-eqz v2, :cond_a

    move v0, v1

    .line 100
    goto :goto_0

    .line 103
    :cond_9
    iget-object v2, p0, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    iget-object v3, p1, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    invoke-virtual {v2, v3}, Lcom/google/k/f/ae;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 104
    goto :goto_0

    .line 107
    :cond_a
    iget v2, p0, Lcom/google/k/f/ab;->e:I

    iget v3, p1, Lcom/google/k/f/ab;->e:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 108
    goto :goto_0

    .line 110
    :cond_b
    iget-boolean v2, p0, Lcom/google/k/f/ab;->f:Z

    iget-boolean v3, p1, Lcom/google/k/f/ab;->f:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 111
    goto :goto_0

    .line 113
    :cond_c
    iget-boolean v2, p0, Lcom/google/k/f/ab;->g:Z

    iget-boolean v3, p1, Lcom/google/k/f/ab;->g:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 114
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 121
    iget-object v0, p0, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 124
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 126
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 128
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    if-nez v4, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 130
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ab;->e:I

    add-int/2addr v0, v1

    .line 131
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/ab;->f:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v1

    .line 132
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/k/f/ab;->g:Z

    if-eqz v1, :cond_5

    :goto_5
    add-int/2addr v0, v2

    .line 133
    return v0

    .line 121
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    invoke-virtual {v0}, Lcom/google/k/f/aa;->hashCode()I

    move-result v0

    goto :goto_0

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    invoke-virtual {v0}, Lcom/google/k/f/ac;->hashCode()I

    move-result v0

    goto :goto_1

    .line 126
    :cond_2
    iget-object v0, p0, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    invoke-virtual {v0}, Lcom/google/k/f/ad;->hashCode()I

    move-result v0

    goto :goto_2

    .line 128
    :cond_3
    iget-object v1, p0, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    invoke-virtual {v1}, Lcom/google/k/f/ae;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    move v0, v3

    .line 131
    goto :goto_4

    :cond_5
    move v2, v3

    .line 132
    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/k/f/aa;

    invoke-direct {v0}, Lcom/google/k/f/aa;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    :cond_1
    iget-object v0, p0, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/k/f/ac;

    invoke-direct {v0}, Lcom/google/k/f/ac;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    :cond_2
    iget-object v0, p0, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/k/f/ad;

    invoke-direct {v0}, Lcom/google/k/f/ad;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    :cond_3
    iget-object v0, p0, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/k/f/ae;

    invoke-direct {v0}, Lcom/google/k/f/ae;-><init>()V

    iput-object v0, p0, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    :cond_4
    iget-object v0, p0, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ab;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/ab;->f:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/ab;->g:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    if-eqz v0, :cond_0

    .line 140
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    if-eqz v0, :cond_1

    .line 143
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    if-eqz v0, :cond_2

    .line 146
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 148
    :cond_2
    iget-object v0, p0, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    if-eqz v0, :cond_3

    .line 149
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 151
    :cond_3
    iget v0, p0, Lcom/google/k/f/ab;->e:I

    if-eqz v0, :cond_4

    .line 152
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/k/f/ab;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 154
    :cond_4
    iget-boolean v0, p0, Lcom/google/k/f/ab;->f:Z

    if-eqz v0, :cond_5

    .line 155
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/k/f/ab;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 157
    :cond_5
    iget-boolean v0, p0, Lcom/google/k/f/ab;->g:Z

    if-eqz v0, :cond_6

    .line 158
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/k/f/ab;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 160
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 161
    return-void
.end method
