.class public final Lcom/google/k/f/c/j;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/k/f/c/j;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1119
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1120
    iput-object v0, p0, Lcom/google/k/f/c/j;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/k/f/c/j;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/k/f/c/j;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/k/f/c/j;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/c/j;->cachedSize:I

    .line 1121
    return-void
.end method

.method public static a()[Lcom/google/k/f/c/j;
    .locals 2

    .prologue
    .line 1099
    sget-object v0, Lcom/google/k/f/c/j;->d:[Lcom/google/k/f/c/j;

    if-nez v0, :cond_1

    .line 1100
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1102
    :try_start_0
    sget-object v0, Lcom/google/k/f/c/j;->d:[Lcom/google/k/f/c/j;

    if-nez v0, :cond_0

    .line 1103
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/k/f/c/j;

    sput-object v0, Lcom/google/k/f/c/j;->d:[Lcom/google/k/f/c/j;

    .line 1105
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1107
    :cond_1
    sget-object v0, Lcom/google/k/f/c/j;->d:[Lcom/google/k/f/c/j;

    return-object v0

    .line 1105
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1149
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1150
    iget-object v1, p0, Lcom/google/k/f/c/j;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1151
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/k/f/c/j;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1154
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/c/j;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1155
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/c/j;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1158
    :cond_1
    iget-object v1, p0, Lcom/google/k/f/c/j;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 1159
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/k/f/c/j;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1162
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1083
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/k/f/c/j;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/c/j;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/c/j;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/c/j;->c:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_4
        0x1 -> :sswitch_4
        0x2 -> :sswitch_4
        0x21 -> :sswitch_4
        0x22 -> :sswitch_4
        0x23 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1135
    iget-object v0, p0, Lcom/google/k/f/c/j;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1136
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/k/f/c/j;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1138
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/c/j;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1139
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/c/j;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1141
    :cond_1
    iget-object v0, p0, Lcom/google/k/f/c/j;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 1142
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/k/f/c/j;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1144
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1145
    return-void
.end method
