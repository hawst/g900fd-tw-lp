.class public final Lcom/google/k/f/aa;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 294
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 295
    iput v0, p0, Lcom/google/k/f/aa;->a:I

    iput v0, p0, Lcom/google/k/f/aa;->b:I

    iput v0, p0, Lcom/google/k/f/aa;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/aa;->cachedSize:I

    .line 296
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 353
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 354
    iget v1, p0, Lcom/google/k/f/aa;->a:I

    if-eqz v1, :cond_0

    .line 355
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/k/f/aa;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 358
    :cond_0
    iget v1, p0, Lcom/google/k/f/aa;->b:I

    if-eqz v1, :cond_1

    .line 359
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/k/f/aa;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 362
    :cond_1
    iget v1, p0, Lcom/google/k/f/aa;->c:I

    if-eqz v1, :cond_2

    .line 363
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/k/f/aa;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 366
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 308
    if-ne p1, p0, :cond_1

    .line 324
    :cond_0
    :goto_0
    return v0

    .line 311
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/aa;

    if-nez v2, :cond_2

    move v0, v1

    .line 312
    goto :goto_0

    .line 314
    :cond_2
    check-cast p1, Lcom/google/k/f/aa;

    .line 315
    iget v2, p0, Lcom/google/k/f/aa;->a:I

    iget v3, p1, Lcom/google/k/f/aa;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 316
    goto :goto_0

    .line 318
    :cond_3
    iget v2, p0, Lcom/google/k/f/aa;->b:I

    iget v3, p1, Lcom/google/k/f/aa;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 319
    goto :goto_0

    .line 321
    :cond_4
    iget v2, p0, Lcom/google/k/f/aa;->c:I

    iget v3, p1, Lcom/google/k/f/aa;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 322
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 329
    iget v0, p0, Lcom/google/k/f/aa;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 331
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/aa;->b:I

    add-int/2addr v0, v1

    .line 332
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/aa;->c:I

    add-int/2addr v0, v1

    .line 333
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 268
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/aa;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/aa;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/aa;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 339
    iget v0, p0, Lcom/google/k/f/aa;->a:I

    if-eqz v0, :cond_0

    .line 340
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/k/f/aa;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 342
    :cond_0
    iget v0, p0, Lcom/google/k/f/aa;->b:I

    if-eqz v0, :cond_1

    .line 343
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/k/f/aa;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 345
    :cond_1
    iget v0, p0, Lcom/google/k/f/aa;->c:I

    if-eqz v0, :cond_2

    .line 346
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/k/f/aa;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 348
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 349
    return-void
.end method
