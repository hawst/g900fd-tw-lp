.class public final Lcom/google/k/f/a/a/s;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Z

.field public c:I

.field public d:I

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 1724
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1725
    iput-boolean v0, p0, Lcom/google/k/f/a/a/s;->a:Z

    iput-boolean v0, p0, Lcom/google/k/f/a/a/s;->b:Z

    iput v0, p0, Lcom/google/k/f/a/a/s;->c:I

    iput v1, p0, Lcom/google/k/f/a/a/s;->d:I

    iput-boolean v0, p0, Lcom/google/k/f/a/a/s;->e:Z

    iput-boolean v0, p0, Lcom/google/k/f/a/a/s;->f:Z

    iput-boolean v0, p0, Lcom/google/k/f/a/a/s;->g:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/a/a/s;->h:Ljava/lang/String;

    iput v1, p0, Lcom/google/k/f/a/a/s;->cachedSize:I

    .line 1726
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1828
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1829
    iget-boolean v1, p0, Lcom/google/k/f/a/a/s;->a:Z

    if-eqz v1, :cond_0

    .line 1830
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/k/f/a/a/s;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1833
    :cond_0
    iget-boolean v1, p0, Lcom/google/k/f/a/a/s;->b:Z

    if-eqz v1, :cond_1

    .line 1834
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/k/f/a/a/s;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1837
    :cond_1
    iget v1, p0, Lcom/google/k/f/a/a/s;->c:I

    if-eqz v1, :cond_2

    .line 1838
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/k/f/a/a/s;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1841
    :cond_2
    iget v1, p0, Lcom/google/k/f/a/a/s;->d:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_3

    .line 1842
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/k/f/a/a/s;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1845
    :cond_3
    iget-boolean v1, p0, Lcom/google/k/f/a/a/s;->e:Z

    if-eqz v1, :cond_4

    .line 1846
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/k/f/a/a/s;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1849
    :cond_4
    iget-boolean v1, p0, Lcom/google/k/f/a/a/s;->f:Z

    if-eqz v1, :cond_5

    .line 1850
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/k/f/a/a/s;->f:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1853
    :cond_5
    iget-boolean v1, p0, Lcom/google/k/f/a/a/s;->g:Z

    if-eqz v1, :cond_6

    .line 1854
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/k/f/a/a/s;->g:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1857
    :cond_6
    iget-object v1, p0, Lcom/google/k/f/a/a/s;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1858
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/k/f/a/a/s;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1861
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1743
    if-ne p1, p0, :cond_1

    .line 1778
    :cond_0
    :goto_0
    return v0

    .line 1746
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/a/a/s;

    if-nez v2, :cond_2

    move v0, v1

    .line 1747
    goto :goto_0

    .line 1749
    :cond_2
    check-cast p1, Lcom/google/k/f/a/a/s;

    .line 1750
    iget-boolean v2, p0, Lcom/google/k/f/a/a/s;->a:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/s;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1751
    goto :goto_0

    .line 1753
    :cond_3
    iget-boolean v2, p0, Lcom/google/k/f/a/a/s;->b:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/s;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1754
    goto :goto_0

    .line 1756
    :cond_4
    iget v2, p0, Lcom/google/k/f/a/a/s;->c:I

    iget v3, p1, Lcom/google/k/f/a/a/s;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1757
    goto :goto_0

    .line 1759
    :cond_5
    iget v2, p0, Lcom/google/k/f/a/a/s;->d:I

    iget v3, p1, Lcom/google/k/f/a/a/s;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 1760
    goto :goto_0

    .line 1762
    :cond_6
    iget-boolean v2, p0, Lcom/google/k/f/a/a/s;->e:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/s;->e:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1763
    goto :goto_0

    .line 1765
    :cond_7
    iget-boolean v2, p0, Lcom/google/k/f/a/a/s;->f:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/s;->f:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1766
    goto :goto_0

    .line 1768
    :cond_8
    iget-boolean v2, p0, Lcom/google/k/f/a/a/s;->g:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/s;->g:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 1769
    goto :goto_0

    .line 1771
    :cond_9
    iget-object v2, p0, Lcom/google/k/f/a/a/s;->h:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 1772
    iget-object v2, p1, Lcom/google/k/f/a/a/s;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1773
    goto :goto_0

    .line 1775
    :cond_a
    iget-object v2, p0, Lcom/google/k/f/a/a/s;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/a/a/s;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1776
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 1783
    iget-boolean v0, p0, Lcom/google/k/f/a/a/s;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1785
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/s;->b:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 1786
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/s;->c:I

    add-int/2addr v0, v3

    .line 1787
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/s;->d:I

    add-int/2addr v0, v3

    .line 1788
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/s;->e:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    .line 1789
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/s;->f:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v3

    .line 1790
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/k/f/a/a/s;->g:Z

    if-eqz v3, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 1791
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/k/f/a/a/s;->h:Ljava/lang/String;

    if-nez v0, :cond_5

    const/4 v0, 0x0

    :goto_5
    add-int/2addr v0, v1

    .line 1793
    return v0

    :cond_0
    move v0, v2

    .line 1783
    goto :goto_0

    :cond_1
    move v0, v2

    .line 1785
    goto :goto_1

    :cond_2
    move v0, v2

    .line 1788
    goto :goto_2

    :cond_3
    move v0, v2

    .line 1789
    goto :goto_3

    :cond_4
    move v1, v2

    .line 1790
    goto :goto_4

    .line 1791
    :cond_5
    iget-object v0, p0, Lcom/google/k/f/a/a/s;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1683
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/s;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/s;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/a/a/s;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/a/a/s;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/s;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/s;->f:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/s;->g:Z

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/a/a/s;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1799
    iget-boolean v0, p0, Lcom/google/k/f/a/a/s;->a:Z

    if-eqz v0, :cond_0

    .line 1800
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/k/f/a/a/s;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1802
    :cond_0
    iget-boolean v0, p0, Lcom/google/k/f/a/a/s;->b:Z

    if-eqz v0, :cond_1

    .line 1803
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/k/f/a/a/s;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1805
    :cond_1
    iget v0, p0, Lcom/google/k/f/a/a/s;->c:I

    if-eqz v0, :cond_2

    .line 1806
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/k/f/a/a/s;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1808
    :cond_2
    iget v0, p0, Lcom/google/k/f/a/a/s;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_3

    .line 1809
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/k/f/a/a/s;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1811
    :cond_3
    iget-boolean v0, p0, Lcom/google/k/f/a/a/s;->e:Z

    if-eqz v0, :cond_4

    .line 1812
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/k/f/a/a/s;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1814
    :cond_4
    iget-boolean v0, p0, Lcom/google/k/f/a/a/s;->f:Z

    if-eqz v0, :cond_5

    .line 1815
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/k/f/a/a/s;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1817
    :cond_5
    iget-boolean v0, p0, Lcom/google/k/f/a/a/s;->g:Z

    if-eqz v0, :cond_6

    .line 1818
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/k/f/a/a/s;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1820
    :cond_6
    iget-object v0, p0, Lcom/google/k/f/a/a/s;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1821
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/k/f/a/a/s;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1823
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1824
    return-void
.end method
