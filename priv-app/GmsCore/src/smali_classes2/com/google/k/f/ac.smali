.class public final Lcom/google/k/f/ac;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 441
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 442
    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/ac;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/k/f/ac;->b:Ljava/lang/String;

    iput v1, p0, Lcom/google/k/f/ac;->c:I

    iput v1, p0, Lcom/google/k/f/ac;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/k/f/ac;->cachedSize:I

    .line 443
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 518
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 519
    iget-object v1, p0, Lcom/google/k/f/ac;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 520
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/k/f/ac;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 523
    :cond_0
    iget-object v1, p0, Lcom/google/k/f/ac;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 524
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/k/f/ac;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 527
    :cond_1
    iget v1, p0, Lcom/google/k/f/ac;->c:I

    if-eqz v1, :cond_2

    .line 528
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/k/f/ac;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 531
    :cond_2
    iget v1, p0, Lcom/google/k/f/ac;->d:I

    if-eqz v1, :cond_3

    .line 532
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/k/f/ac;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 535
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 456
    if-ne p1, p0, :cond_1

    .line 483
    :cond_0
    :goto_0
    return v0

    .line 459
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/ac;

    if-nez v2, :cond_2

    move v0, v1

    .line 460
    goto :goto_0

    .line 462
    :cond_2
    check-cast p1, Lcom/google/k/f/ac;

    .line 463
    iget-object v2, p0, Lcom/google/k/f/ac;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 464
    iget-object v2, p1, Lcom/google/k/f/ac;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 465
    goto :goto_0

    .line 467
    :cond_3
    iget-object v2, p0, Lcom/google/k/f/ac;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/ac;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 468
    goto :goto_0

    .line 470
    :cond_4
    iget-object v2, p0, Lcom/google/k/f/ac;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 471
    iget-object v2, p1, Lcom/google/k/f/ac;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 472
    goto :goto_0

    .line 474
    :cond_5
    iget-object v2, p0, Lcom/google/k/f/ac;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/k/f/ac;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 475
    goto :goto_0

    .line 477
    :cond_6
    iget v2, p0, Lcom/google/k/f/ac;->c:I

    iget v3, p1, Lcom/google/k/f/ac;->c:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 478
    goto :goto_0

    .line 480
    :cond_7
    iget v2, p0, Lcom/google/k/f/ac;->d:I

    iget v3, p1, Lcom/google/k/f/ac;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 481
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 488
    iget-object v0, p0, Lcom/google/k/f/ac;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 491
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/k/f/ac;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 493
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ac;->c:I

    add-int/2addr v0, v1

    .line 494
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/ac;->d:I

    add-int/2addr v0, v1

    .line 495
    return v0

    .line 488
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/ac;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 491
    :cond_1
    iget-object v1, p0, Lcom/google/k/f/ac;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 412
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/ac;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/k/f/ac;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ac;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/ac;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/k/f/ac;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 502
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/k/f/ac;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 504
    :cond_0
    iget-object v0, p0, Lcom/google/k/f/ac;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 505
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/k/f/ac;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 507
    :cond_1
    iget v0, p0, Lcom/google/k/f/ac;->c:I

    if-eqz v0, :cond_2

    .line 508
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/k/f/ac;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 510
    :cond_2
    iget v0, p0, Lcom/google/k/f/ac;->d:I

    if-eqz v0, :cond_3

    .line 511
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/k/f/ac;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 513
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 514
    return-void
.end method
