.class public final Lcom/google/k/f/a/a/e;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:I

.field public c:Z

.field public d:Z

.field public e:Z

.field public f:I

.field public g:Z

.field public h:Z

.field public i:I

.field public j:I

.field public k:I

.field public l:I

.field public m:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 84
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 85
    iput-boolean v0, p0, Lcom/google/k/f/a/a/e;->a:Z

    iput v0, p0, Lcom/google/k/f/a/a/e;->b:I

    iput-boolean v0, p0, Lcom/google/k/f/a/a/e;->c:Z

    iput-boolean v0, p0, Lcom/google/k/f/a/a/e;->d:Z

    iput-boolean v0, p0, Lcom/google/k/f/a/a/e;->e:Z

    iput v0, p0, Lcom/google/k/f/a/a/e;->f:I

    iput-boolean v0, p0, Lcom/google/k/f/a/a/e;->g:Z

    iput-boolean v0, p0, Lcom/google/k/f/a/a/e;->h:Z

    iput v1, p0, Lcom/google/k/f/a/a/e;->i:I

    iput v0, p0, Lcom/google/k/f/a/a/e;->j:I

    iput v0, p0, Lcom/google/k/f/a/a/e;->k:I

    iput v0, p0, Lcom/google/k/f/a/a/e;->l:I

    iput-boolean v0, p0, Lcom/google/k/f/a/a/e;->m:Z

    iput v1, p0, Lcom/google/k/f/a/a/e;->cachedSize:I

    .line 86
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 223
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 224
    iget-boolean v1, p0, Lcom/google/k/f/a/a/e;->a:Z

    if-eqz v1, :cond_0

    .line 225
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/k/f/a/a/e;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 228
    :cond_0
    iget v1, p0, Lcom/google/k/f/a/a/e;->b:I

    if-eqz v1, :cond_1

    .line 229
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/k/f/a/a/e;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 232
    :cond_1
    iget-boolean v1, p0, Lcom/google/k/f/a/a/e;->c:Z

    if-eqz v1, :cond_2

    .line 233
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/k/f/a/a/e;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 236
    :cond_2
    iget-boolean v1, p0, Lcom/google/k/f/a/a/e;->d:Z

    if-eqz v1, :cond_3

    .line 237
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/k/f/a/a/e;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 240
    :cond_3
    iget-boolean v1, p0, Lcom/google/k/f/a/a/e;->e:Z

    if-eqz v1, :cond_4

    .line 241
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/k/f/a/a/e;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 244
    :cond_4
    iget v1, p0, Lcom/google/k/f/a/a/e;->f:I

    if-eqz v1, :cond_5

    .line 245
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/k/f/a/a/e;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 248
    :cond_5
    iget-boolean v1, p0, Lcom/google/k/f/a/a/e;->g:Z

    if-eqz v1, :cond_6

    .line 249
    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/k/f/a/a/e;->g:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 252
    :cond_6
    iget-boolean v1, p0, Lcom/google/k/f/a/a/e;->h:Z

    if-eqz v1, :cond_7

    .line 253
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/k/f/a/a/e;->h:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 256
    :cond_7
    iget v1, p0, Lcom/google/k/f/a/a/e;->i:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_8

    .line 257
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/k/f/a/a/e;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    :cond_8
    iget v1, p0, Lcom/google/k/f/a/a/e;->j:I

    if-eqz v1, :cond_9

    .line 261
    const/16 v1, 0xa

    iget v2, p0, Lcom/google/k/f/a/a/e;->j:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 264
    :cond_9
    iget v1, p0, Lcom/google/k/f/a/a/e;->k:I

    if-eqz v1, :cond_a

    .line 265
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/k/f/a/a/e;->k:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 268
    :cond_a
    iget v1, p0, Lcom/google/k/f/a/a/e;->l:I

    if-eqz v1, :cond_b

    .line 269
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/k/f/a/a/e;->l:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 272
    :cond_b
    iget-boolean v1, p0, Lcom/google/k/f/a/a/e;->m:Z

    if-eqz v1, :cond_c

    .line 273
    const/16 v1, 0xd

    iget-boolean v2, p0, Lcom/google/k/f/a/a/e;->m:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 276
    :cond_c
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 108
    if-ne p1, p0, :cond_1

    .line 154
    :cond_0
    :goto_0
    return v0

    .line 111
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/a/a/e;

    if-nez v2, :cond_2

    move v0, v1

    .line 112
    goto :goto_0

    .line 114
    :cond_2
    check-cast p1, Lcom/google/k/f/a/a/e;

    .line 115
    iget-boolean v2, p0, Lcom/google/k/f/a/a/e;->a:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/e;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 116
    goto :goto_0

    .line 118
    :cond_3
    iget v2, p0, Lcom/google/k/f/a/a/e;->b:I

    iget v3, p1, Lcom/google/k/f/a/a/e;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 119
    goto :goto_0

    .line 121
    :cond_4
    iget-boolean v2, p0, Lcom/google/k/f/a/a/e;->c:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/e;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 122
    goto :goto_0

    .line 124
    :cond_5
    iget-boolean v2, p0, Lcom/google/k/f/a/a/e;->d:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/e;->d:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 125
    goto :goto_0

    .line 127
    :cond_6
    iget-boolean v2, p0, Lcom/google/k/f/a/a/e;->e:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/e;->e:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 128
    goto :goto_0

    .line 130
    :cond_7
    iget v2, p0, Lcom/google/k/f/a/a/e;->f:I

    iget v3, p1, Lcom/google/k/f/a/a/e;->f:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 131
    goto :goto_0

    .line 133
    :cond_8
    iget-boolean v2, p0, Lcom/google/k/f/a/a/e;->g:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/e;->g:Z

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 134
    goto :goto_0

    .line 136
    :cond_9
    iget-boolean v2, p0, Lcom/google/k/f/a/a/e;->h:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/e;->h:Z

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 137
    goto :goto_0

    .line 139
    :cond_a
    iget v2, p0, Lcom/google/k/f/a/a/e;->i:I

    iget v3, p1, Lcom/google/k/f/a/a/e;->i:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 140
    goto :goto_0

    .line 142
    :cond_b
    iget v2, p0, Lcom/google/k/f/a/a/e;->j:I

    iget v3, p1, Lcom/google/k/f/a/a/e;->j:I

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 143
    goto :goto_0

    .line 145
    :cond_c
    iget v2, p0, Lcom/google/k/f/a/a/e;->k:I

    iget v3, p1, Lcom/google/k/f/a/a/e;->k:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 146
    goto :goto_0

    .line 148
    :cond_d
    iget v2, p0, Lcom/google/k/f/a/a/e;->l:I

    iget v3, p1, Lcom/google/k/f/a/a/e;->l:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 149
    goto :goto_0

    .line 151
    :cond_e
    iget-boolean v2, p0, Lcom/google/k/f/a/a/e;->m:Z

    iget-boolean v3, p1, Lcom/google/k/f/a/a/e;->m:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 152
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 159
    iget-boolean v0, p0, Lcom/google/k/f/a/a/e;->a:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 161
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/e;->b:I

    add-int/2addr v0, v3

    .line 162
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/e;->c:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    .line 163
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/e;->d:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v3

    .line 164
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/e;->e:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v3

    .line 165
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/e;->f:I

    add-int/2addr v0, v3

    .line 166
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/e;->g:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v3

    .line 167
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/k/f/a/a/e;->h:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v3

    .line 168
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/e;->i:I

    add-int/2addr v0, v3

    .line 169
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/e;->j:I

    add-int/2addr v0, v3

    .line 170
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/e;->k:I

    add-int/2addr v0, v3

    .line 171
    mul-int/lit8 v0, v0, 0x1f

    iget v3, p0, Lcom/google/k/f/a/a/e;->l:I

    add-int/2addr v0, v3

    .line 172
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/k/f/a/a/e;->m:Z

    if-eqz v3, :cond_6

    :goto_6
    add-int/2addr v0, v1

    .line 173
    return v0

    :cond_0
    move v0, v2

    .line 159
    goto :goto_0

    :cond_1
    move v0, v2

    .line 162
    goto :goto_1

    :cond_2
    move v0, v2

    .line 163
    goto :goto_2

    :cond_3
    move v0, v2

    .line 164
    goto :goto_3

    :cond_4
    move v0, v2

    .line 166
    goto :goto_4

    :cond_5
    move v0, v2

    .line 167
    goto :goto_5

    :cond_6
    move v1, v2

    .line 172
    goto :goto_6
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 23
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/e;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/a/a/e;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/e;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/e;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/e;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/k/f/a/a/e;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/e;->g:Z

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/e;->h:Z

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/a/a/e;->i:I

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto :goto_0

    :pswitch_2
    iput v0, p0, Lcom/google/k/f/a/a/e;->j:I

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    goto :goto_0

    :pswitch_3
    iput v0, p0, Lcom/google/k/f/a/a/e;->k:I

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_4

    goto :goto_0

    :pswitch_4
    iput v0, p0, Lcom/google/k/f/a/a/e;->l:I

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/k/f/a/a/e;->m:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
        0x68 -> :sswitch_d
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 179
    iget-boolean v0, p0, Lcom/google/k/f/a/a/e;->a:Z

    if-eqz v0, :cond_0

    .line 180
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/k/f/a/a/e;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 182
    :cond_0
    iget v0, p0, Lcom/google/k/f/a/a/e;->b:I

    if-eqz v0, :cond_1

    .line 183
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/k/f/a/a/e;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 185
    :cond_1
    iget-boolean v0, p0, Lcom/google/k/f/a/a/e;->c:Z

    if-eqz v0, :cond_2

    .line 186
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/k/f/a/a/e;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 188
    :cond_2
    iget-boolean v0, p0, Lcom/google/k/f/a/a/e;->d:Z

    if-eqz v0, :cond_3

    .line 189
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/k/f/a/a/e;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 191
    :cond_3
    iget-boolean v0, p0, Lcom/google/k/f/a/a/e;->e:Z

    if-eqz v0, :cond_4

    .line 192
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/k/f/a/a/e;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 194
    :cond_4
    iget v0, p0, Lcom/google/k/f/a/a/e;->f:I

    if-eqz v0, :cond_5

    .line 195
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/k/f/a/a/e;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 197
    :cond_5
    iget-boolean v0, p0, Lcom/google/k/f/a/a/e;->g:Z

    if-eqz v0, :cond_6

    .line 198
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/k/f/a/a/e;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 200
    :cond_6
    iget-boolean v0, p0, Lcom/google/k/f/a/a/e;->h:Z

    if-eqz v0, :cond_7

    .line 201
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/k/f/a/a/e;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 203
    :cond_7
    iget v0, p0, Lcom/google/k/f/a/a/e;->i:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_8

    .line 204
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/k/f/a/a/e;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 206
    :cond_8
    iget v0, p0, Lcom/google/k/f/a/a/e;->j:I

    if-eqz v0, :cond_9

    .line 207
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/k/f/a/a/e;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 209
    :cond_9
    iget v0, p0, Lcom/google/k/f/a/a/e;->k:I

    if-eqz v0, :cond_a

    .line 210
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/k/f/a/a/e;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 212
    :cond_a
    iget v0, p0, Lcom/google/k/f/a/a/e;->l:I

    if-eqz v0, :cond_b

    .line 213
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/k/f/a/a/e;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 215
    :cond_b
    iget-boolean v0, p0, Lcom/google/k/f/a/a/e;->m:Z

    if-eqz v0, :cond_c

    .line 216
    const/16 v0, 0xd

    iget-boolean v1, p0, Lcom/google/k/f/a/a/e;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 218
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 219
    return-void
.end method
