.class public final Lcom/google/k/f/a/a/f;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 2770
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2771
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/k/f/a/a/f;->a:I

    iput v1, p0, Lcom/google/k/f/a/a/f;->b:I

    iput v1, p0, Lcom/google/k/f/a/a/f;->cachedSize:I

    .line 2772
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2821
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2822
    iget v1, p0, Lcom/google/k/f/a/a/f;->a:I

    if-eqz v1, :cond_0

    .line 2823
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/k/f/a/a/f;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2826
    :cond_0
    iget v1, p0, Lcom/google/k/f/a/a/f;->b:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_1

    .line 2827
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/k/f/a/a/f;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2830
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2783
    if-ne p1, p0, :cond_1

    .line 2796
    :cond_0
    :goto_0
    return v0

    .line 2786
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/a/a/f;

    if-nez v2, :cond_2

    move v0, v1

    .line 2787
    goto :goto_0

    .line 2789
    :cond_2
    check-cast p1, Lcom/google/k/f/a/a/f;

    .line 2790
    iget v2, p0, Lcom/google/k/f/a/a/f;->a:I

    iget v3, p1, Lcom/google/k/f/a/a/f;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 2791
    goto :goto_0

    .line 2793
    :cond_3
    iget v2, p0, Lcom/google/k/f/a/a/f;->b:I

    iget v3, p1, Lcom/google/k/f/a/a/f;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2794
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2801
    iget v0, p0, Lcom/google/k/f/a/a/f;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 2803
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/a/a/f;->b:I

    add-int/2addr v0, v1

    .line 2804
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2747
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/a/a/f;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/a/a/f;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2810
    iget v0, p0, Lcom/google/k/f/a/a/f;->a:I

    if-eqz v0, :cond_0

    .line 2811
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/k/f/a/a/f;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2813
    :cond_0
    iget v0, p0, Lcom/google/k/f/a/a/f;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 2814
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/k/f/a/a/f;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2816
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2817
    return-void
.end method
