.class public final Lcom/google/k/f/a/a/r;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, -0x1

    .line 1423
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1424
    iput v1, p0, Lcom/google/k/f/a/a/r;->a:I

    iput v1, p0, Lcom/google/k/f/a/a/r;->b:I

    iput v0, p0, Lcom/google/k/f/a/a/r;->c:I

    iput v0, p0, Lcom/google/k/f/a/a/r;->cachedSize:I

    .line 1425
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1482
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1483
    iget v1, p0, Lcom/google/k/f/a/a/r;->a:I

    if-eqz v1, :cond_0

    .line 1484
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/k/f/a/a/r;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1487
    :cond_0
    iget v1, p0, Lcom/google/k/f/a/a/r;->b:I

    if-eqz v1, :cond_1

    .line 1488
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/k/f/a/a/r;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1491
    :cond_1
    iget v1, p0, Lcom/google/k/f/a/a/r;->c:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 1492
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/k/f/a/a/r;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1495
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1437
    if-ne p1, p0, :cond_1

    .line 1453
    :cond_0
    :goto_0
    return v0

    .line 1440
    :cond_1
    instance-of v2, p1, Lcom/google/k/f/a/a/r;

    if-nez v2, :cond_2

    move v0, v1

    .line 1441
    goto :goto_0

    .line 1443
    :cond_2
    check-cast p1, Lcom/google/k/f/a/a/r;

    .line 1444
    iget v2, p0, Lcom/google/k/f/a/a/r;->a:I

    iget v3, p1, Lcom/google/k/f/a/a/r;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1445
    goto :goto_0

    .line 1447
    :cond_3
    iget v2, p0, Lcom/google/k/f/a/a/r;->b:I

    iget v3, p1, Lcom/google/k/f/a/a/r;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 1448
    goto :goto_0

    .line 1450
    :cond_4
    iget v2, p0, Lcom/google/k/f/a/a/r;->c:I

    iget v3, p1, Lcom/google/k/f/a/a/r;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1451
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1458
    iget v0, p0, Lcom/google/k/f/a/a/r;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 1460
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/a/a/r;->b:I

    add-int/2addr v0, v1

    .line 1461
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/k/f/a/a/r;->c:I

    add-int/2addr v0, v1

    .line 1462
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1392
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/k/f/a/a/r;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/k/f/a/a/r;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/k/f/a/a/r;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1468
    iget v0, p0, Lcom/google/k/f/a/a/r;->a:I

    if-eqz v0, :cond_0

    .line 1469
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/k/f/a/a/r;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1471
    :cond_0
    iget v0, p0, Lcom/google/k/f/a/a/r;->b:I

    if-eqz v0, :cond_1

    .line 1472
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/k/f/a/a/r;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1474
    :cond_1
    iget v0, p0, Lcom/google/k/f/a/a/r;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 1475
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/k/f/a/a/r;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1477
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1478
    return-void
.end method
