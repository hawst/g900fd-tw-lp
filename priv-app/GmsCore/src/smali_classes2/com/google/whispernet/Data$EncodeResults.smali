.class public final Lcom/google/whispernet/Data$EncodeResults;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public data:[B

.field public startTransition:[B


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 467
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 468
    invoke-virtual {p0}, Lcom/google/whispernet/Data$EncodeResults;->clear()Lcom/google/whispernet/Data$EncodeResults;

    .line 469
    return-void
.end method


# virtual methods
.method public final clear()Lcom/google/whispernet/Data$EncodeResults;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 472
    iput-object v0, p0, Lcom/google/whispernet/Data$EncodeResults;->startTransition:[B

    .line 473
    iput-object v0, p0, Lcom/google/whispernet/Data$EncodeResults;->data:[B

    .line 474
    iput-object v0, p0, Lcom/google/whispernet/Data$EncodeResults;->unknownFieldData:Lcom/google/protobuf/nano/f;

    .line 475
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/whispernet/Data$EncodeResults;->cachedSize:I

    .line 476
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 520
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 521
    iget-object v1, p0, Lcom/google/whispernet/Data$EncodeResults;->startTransition:[B

    if-eqz v1, :cond_0

    .line 522
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/whispernet/Data$EncodeResults;->startTransition:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 525
    :cond_0
    iget-object v1, p0, Lcom/google/whispernet/Data$EncodeResults;->data:[B

    if-eqz v1, :cond_1

    .line 526
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/whispernet/Data$EncodeResults;->data:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 529
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 481
    if-ne p1, p0, :cond_1

    .line 482
    const/4 v0, 0x1

    .line 494
    :cond_0
    :goto_0
    return v0

    .line 484
    :cond_1
    instance-of v1, p1, Lcom/google/whispernet/Data$EncodeResults;

    if-eqz v1, :cond_0

    .line 487
    check-cast p1, Lcom/google/whispernet/Data$EncodeResults;

    .line 488
    iget-object v1, p0, Lcom/google/whispernet/Data$EncodeResults;->startTransition:[B

    iget-object v2, p1, Lcom/google/whispernet/Data$EncodeResults;->startTransition:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 491
    iget-object v1, p0, Lcom/google/whispernet/Data$EncodeResults;->data:[B

    iget-object v2, p1, Lcom/google/whispernet/Data$EncodeResults;->data:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 494
    invoke-virtual {p0, p1}, Lcom/google/whispernet/Data$EncodeResults;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 499
    iget-object v0, p0, Lcom/google/whispernet/Data$EncodeResults;->startTransition:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 501
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/whispernet/Data$EncodeResults;->data:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    .line 502
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/whispernet/Data$EncodeResults;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 503
    return v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 444
    invoke-virtual {p0, p1}, Lcom/google/whispernet/Data$EncodeResults;->mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/whispernet/Data$EncodeResults;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/whispernet/Data$EncodeResults;
    .locals 1

    .prologue
    .line 537
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    .line 538
    sparse-switch v0, :sswitch_data_0

    .line 542
    invoke-virtual {p0, p1, v0}, Lcom/google/whispernet/Data$EncodeResults;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 543
    :sswitch_0
    return-object p0

    .line 548
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/Data$EncodeResults;->startTransition:[B

    goto :goto_0

    .line 552
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/Data$EncodeResults;->data:[B

    goto :goto_0

    .line 538
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 509
    iget-object v0, p0, Lcom/google/whispernet/Data$EncodeResults;->startTransition:[B

    if-eqz v0, :cond_0

    .line 510
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/whispernet/Data$EncodeResults;->startTransition:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 512
    :cond_0
    iget-object v0, p0, Lcom/google/whispernet/Data$EncodeResults;->data:[B

    if-eqz v0, :cond_1

    .line 513
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/whispernet/Data$EncodeResults;->data:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 515
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 516
    return-void
.end method
