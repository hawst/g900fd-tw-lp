.class public final Lcom/google/whispernet/Data$DecodedToken;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile _emptyArray:[Lcom/google/whispernet/Data$DecodedToken;


# instance fields
.field public bestDecodingGroupSize:Ljava/lang/Integer;

.field public confidence:Ljava/lang/Double;

.field public dateMillis:Ljava/lang/Long;

.field public metadataFile:Ljava/lang/String;

.field public timeToDecodeNanos:Ljava/lang/Long;

.field public token:[B

.field public tokenLengthBytes:Ljava/lang/Integer;

.field public waveFile:Ljava/lang/String;

.field public waveFileLengthMillis:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 53
    invoke-virtual {p0}, Lcom/google/whispernet/Data$DecodedToken;->clear()Lcom/google/whispernet/Data$DecodedToken;

    .line 54
    return-void
.end method

.method public static emptyArray()[Lcom/google/whispernet/Data$DecodedToken;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/whispernet/Data$DecodedToken;->_emptyArray:[Lcom/google/whispernet/Data$DecodedToken;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/whispernet/Data$DecodedToken;->_emptyArray:[Lcom/google/whispernet/Data$DecodedToken;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/whispernet/Data$DecodedToken;

    sput-object v0, Lcom/google/whispernet/Data$DecodedToken;->_emptyArray:[Lcom/google/whispernet/Data$DecodedToken;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/whispernet/Data$DecodedToken;->_emptyArray:[Lcom/google/whispernet/Data$DecodedToken;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final clear()Lcom/google/whispernet/Data$DecodedToken;
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 57
    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->token:[B

    .line 58
    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->tokenLengthBytes:Ljava/lang/Integer;

    .line 59
    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->confidence:Ljava/lang/Double;

    .line 60
    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFile:Ljava/lang/String;

    .line 61
    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->dateMillis:Ljava/lang/Long;

    .line 62
    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFileLengthMillis:Ljava/lang/Long;

    .line 63
    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->timeToDecodeNanos:Ljava/lang/Long;

    .line 64
    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->metadataFile:Ljava/lang/String;

    .line 65
    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->bestDecodingGroupSize:Ljava/lang/Integer;

    .line 66
    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->unknownFieldData:Lcom/google/protobuf/nano/f;

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/whispernet/Data$DecodedToken;->cachedSize:I

    .line 68
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 201
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 202
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->token:[B

    if-eqz v1, :cond_0

    .line 203
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/whispernet/Data$DecodedToken;->token:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 206
    :cond_0
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->tokenLengthBytes:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 207
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/whispernet/Data$DecodedToken;->tokenLengthBytes:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 210
    :cond_1
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->confidence:Ljava/lang/Double;

    if-eqz v1, :cond_2

    .line 211
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/whispernet/Data$DecodedToken;->confidence:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 214
    :cond_2
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFile:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 215
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFile:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 218
    :cond_3
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->dateMillis:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 219
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/whispernet/Data$DecodedToken;->dateMillis:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 222
    :cond_4
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFileLengthMillis:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 223
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFileLengthMillis:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 226
    :cond_5
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->timeToDecodeNanos:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 227
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/whispernet/Data$DecodedToken;->timeToDecodeNanos:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 230
    :cond_6
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->metadataFile:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 231
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/whispernet/Data$DecodedToken;->metadataFile:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 234
    :cond_7
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->bestDecodingGroupSize:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 235
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/whispernet/Data$DecodedToken;->bestDecodingGroupSize:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 73
    if-ne p1, p0, :cond_1

    .line 74
    const/4 v0, 0x1

    .line 139
    :cond_0
    :goto_0
    return v0

    .line 76
    :cond_1
    instance-of v1, p1, Lcom/google/whispernet/Data$DecodedToken;

    if-eqz v1, :cond_0

    .line 79
    check-cast p1, Lcom/google/whispernet/Data$DecodedToken;

    .line 80
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->token:[B

    iget-object v2, p1, Lcom/google/whispernet/Data$DecodedToken;->token:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->tokenLengthBytes:Ljava/lang/Integer;

    if-nez v1, :cond_a

    .line 84
    iget-object v1, p1, Lcom/google/whispernet/Data$DecodedToken;->tokenLengthBytes:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 90
    :cond_2
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->confidence:Ljava/lang/Double;

    if-nez v1, :cond_b

    .line 91
    iget-object v1, p1, Lcom/google/whispernet/Data$DecodedToken;->confidence:Ljava/lang/Double;

    if-nez v1, :cond_0

    .line 97
    :cond_3
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFile:Ljava/lang/String;

    if-nez v1, :cond_c

    .line 98
    iget-object v1, p1, Lcom/google/whispernet/Data$DecodedToken;->waveFile:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 104
    :cond_4
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->dateMillis:Ljava/lang/Long;

    if-nez v1, :cond_d

    .line 105
    iget-object v1, p1, Lcom/google/whispernet/Data$DecodedToken;->dateMillis:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 111
    :cond_5
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFileLengthMillis:Ljava/lang/Long;

    if-nez v1, :cond_e

    .line 112
    iget-object v1, p1, Lcom/google/whispernet/Data$DecodedToken;->waveFileLengthMillis:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 118
    :cond_6
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->timeToDecodeNanos:Ljava/lang/Long;

    if-nez v1, :cond_f

    .line 119
    iget-object v1, p1, Lcom/google/whispernet/Data$DecodedToken;->timeToDecodeNanos:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 125
    :cond_7
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->metadataFile:Ljava/lang/String;

    if-nez v1, :cond_10

    .line 126
    iget-object v1, p1, Lcom/google/whispernet/Data$DecodedToken;->metadataFile:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 132
    :cond_8
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->bestDecodingGroupSize:Ljava/lang/Integer;

    if-nez v1, :cond_11

    .line 133
    iget-object v1, p1, Lcom/google/whispernet/Data$DecodedToken;->bestDecodingGroupSize:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 139
    :cond_9
    invoke-virtual {p0, p1}, Lcom/google/whispernet/Data$DecodedToken;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 87
    :cond_a
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->tokenLengthBytes:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/whispernet/Data$DecodedToken;->tokenLengthBytes:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 94
    :cond_b
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->confidence:Ljava/lang/Double;

    iget-object v2, p1, Lcom/google/whispernet/Data$DecodedToken;->confidence:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 101
    :cond_c
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFile:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/whispernet/Data$DecodedToken;->waveFile:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 108
    :cond_d
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->dateMillis:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/whispernet/Data$DecodedToken;->dateMillis:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 115
    :cond_e
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFileLengthMillis:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/whispernet/Data$DecodedToken;->waveFileLengthMillis:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 122
    :cond_f
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->timeToDecodeNanos:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/whispernet/Data$DecodedToken;->timeToDecodeNanos:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 129
    :cond_10
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->metadataFile:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/whispernet/Data$DecodedToken;->metadataFile:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 136
    :cond_11
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->bestDecodingGroupSize:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/whispernet/Data$DecodedToken;->bestDecodingGroupSize:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 144
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->token:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 146
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->tokenLengthBytes:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 148
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->confidence:Ljava/lang/Double;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 150
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFile:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 152
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->dateMillis:Ljava/lang/Long;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 154
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFileLengthMillis:Ljava/lang/Long;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 156
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->timeToDecodeNanos:Ljava/lang/Long;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 158
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->metadataFile:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 160
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/whispernet/Data$DecodedToken;->bestDecodingGroupSize:Ljava/lang/Integer;

    if-nez v2, :cond_7

    :goto_7
    add-int/2addr v0, v1

    .line 162
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/whispernet/Data$DecodedToken;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    return v0

    .line 146
    :cond_0
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->tokenLengthBytes:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_0

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->confidence:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto :goto_1

    .line 150
    :cond_2
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFile:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 152
    :cond_3
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->dateMillis:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_3

    .line 154
    :cond_4
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFileLengthMillis:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_4

    .line 156
    :cond_5
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->timeToDecodeNanos:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_5

    .line 158
    :cond_6
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->metadataFile:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 160
    :cond_7
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->bestDecodingGroupSize:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_7
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    invoke-virtual {p0, p1}, Lcom/google/whispernet/Data$DecodedToken;->mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/whispernet/Data$DecodedToken;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/whispernet/Data$DecodedToken;
    .locals 2

    .prologue
    .line 246
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    .line 247
    sparse-switch v0, :sswitch_data_0

    .line 251
    invoke-virtual {p0, p1, v0}, Lcom/google/whispernet/Data$DecodedToken;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 252
    :sswitch_0
    return-object p0

    .line 257
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->token:[B

    goto :goto_0

    .line 261
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->tokenLengthBytes:Ljava/lang/Integer;

    goto :goto_0

    .line 265
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->confidence:Ljava/lang/Double;

    goto :goto_0

    .line 269
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFile:Ljava/lang/String;

    goto :goto_0

    .line 273
    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->dateMillis:Ljava/lang/Long;

    goto :goto_0

    .line 277
    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFileLengthMillis:Ljava/lang/Long;

    goto :goto_0

    .line 281
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->timeToDecodeNanos:Ljava/lang/Long;

    goto :goto_0

    .line 285
    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->metadataFile:Ljava/lang/String;

    goto :goto_0

    .line 289
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->bestDecodingGroupSize:Ljava/lang/Integer;

    goto :goto_0

    .line 247
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x19 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->token:[B

    if-eqz v0, :cond_0

    .line 170
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->token:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 172
    :cond_0
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->tokenLengthBytes:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 173
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->tokenLengthBytes:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->confidence:Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 176
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->confidence:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 178
    :cond_2
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFile:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 179
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFile:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 181
    :cond_3
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->dateMillis:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 182
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->dateMillis:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 184
    :cond_4
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFileLengthMillis:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 185
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->waveFileLengthMillis:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 187
    :cond_5
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->timeToDecodeNanos:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 188
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->timeToDecodeNanos:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 190
    :cond_6
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->metadataFile:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 191
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->metadataFile:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 193
    :cond_7
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedToken;->bestDecodingGroupSize:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 194
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedToken;->bestDecodingGroupSize:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 196
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 197
    return-void
.end method
