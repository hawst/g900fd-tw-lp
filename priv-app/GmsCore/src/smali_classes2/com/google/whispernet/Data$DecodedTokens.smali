.class public final Lcom/google/whispernet/Data$DecodedTokens;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public token:[Lcom/google/whispernet/Data$DecodedToken;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 328
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 329
    invoke-virtual {p0}, Lcom/google/whispernet/Data$DecodedTokens;->clear()Lcom/google/whispernet/Data$DecodedTokens;

    .line 330
    return-void
.end method


# virtual methods
.method public final clear()Lcom/google/whispernet/Data$DecodedTokens;
    .locals 1

    .prologue
    .line 333
    invoke-static {}, Lcom/google/whispernet/Data$DecodedToken;->emptyArray()[Lcom/google/whispernet/Data$DecodedToken;

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    .line 334
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/whispernet/Data$DecodedTokens;->unknownFieldData:Lcom/google/protobuf/nano/f;

    .line 335
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/whispernet/Data$DecodedTokens;->cachedSize:I

    .line 336
    return-object p0
.end method

.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 380
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 381
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 382
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 383
    iget-object v2, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    aget-object v2, v2, v0

    .line 384
    if-eqz v2, :cond_0

    .line 385
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 382
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 390
    :cond_1
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 341
    if-ne p1, p0, :cond_1

    .line 342
    const/4 v0, 0x1

    .line 352
    :cond_0
    :goto_0
    return v0

    .line 344
    :cond_1
    instance-of v1, p1, Lcom/google/whispernet/Data$DecodedTokens;

    if-eqz v1, :cond_0

    .line 347
    check-cast p1, Lcom/google/whispernet/Data$DecodedTokens;

    .line 348
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    iget-object v2, p1, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 352
    invoke-virtual {p0, p1}, Lcom/google/whispernet/Data$DecodedTokens;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 360
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/whispernet/Data$DecodedTokens;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 361
    return v0
.end method

.method public final bridge synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 308
    invoke-virtual {p0, p1}, Lcom/google/whispernet/Data$DecodedTokens;->mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/whispernet/Data$DecodedTokens;

    move-result-object v0

    return-object v0
.end method

.method public final mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/whispernet/Data$DecodedTokens;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 398
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    .line 399
    sparse-switch v0, :sswitch_data_0

    .line 403
    invoke-virtual {p0, p1, v0}, Lcom/google/whispernet/Data$DecodedTokens;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 404
    :sswitch_0
    return-object p0

    .line 409
    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    .line 411
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    if-nez v0, :cond_2

    move v0, v1

    .line 412
    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/whispernet/Data$DecodedToken;

    .line 414
    if-eqz v0, :cond_1

    .line 415
    iget-object v3, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 417
    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    .line 418
    new-instance v3, Lcom/google/whispernet/Data$DecodedToken;

    invoke-direct {v3}, Lcom/google/whispernet/Data$DecodedToken;-><init>()V

    aput-object v3, v2, v0

    .line 419
    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    .line 420
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    .line 417
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 411
    :cond_2
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    array-length v0, v0

    goto :goto_1

    .line 423
    :cond_3
    new-instance v3, Lcom/google/whispernet/Data$DecodedToken;

    invoke-direct {v3}, Lcom/google/whispernet/Data$DecodedToken;-><init>()V

    aput-object v3, v2, v0

    .line 424
    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    .line 425
    iput-object v2, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    goto :goto_0

    .line 399
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 368
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 369
    iget-object v1, p0, Lcom/google/whispernet/Data$DecodedTokens;->token:[Lcom/google/whispernet/Data$DecodedToken;

    aget-object v1, v1, v0

    .line 370
    if-eqz v1, :cond_0

    .line 371
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 368
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 375
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 376
    return-void
.end method
