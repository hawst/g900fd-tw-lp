.class public final Lcom/google/whispernet/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 41
    iput-object v0, p0, Lcom/google/whispernet/b;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/whispernet/b;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/whispernet/b;->c:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/whispernet/b;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/whispernet/b;->e:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/whispernet/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/whispernet/b;->cachedSize:I

    .line 42
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 142
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 143
    iget-object v1, p0, Lcom/google/whispernet/b;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 144
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/whispernet/b;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 147
    :cond_0
    iget-object v1, p0, Lcom/google/whispernet/b;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 148
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/whispernet/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 151
    :cond_1
    iget-object v1, p0, Lcom/google/whispernet/b;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 152
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/whispernet/b;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 155
    :cond_2
    iget-object v1, p0, Lcom/google/whispernet/b;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 156
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/whispernet/b;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 159
    :cond_3
    iget-object v1, p0, Lcom/google/whispernet/b;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 160
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/whispernet/b;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 57
    if-ne p1, p0, :cond_1

    .line 58
    const/4 v0, 0x1

    .line 99
    :cond_0
    :goto_0
    return v0

    .line 60
    :cond_1
    instance-of v1, p1, Lcom/google/whispernet/b;

    if-eqz v1, :cond_0

    .line 63
    check-cast p1, Lcom/google/whispernet/b;

    .line 64
    iget-object v1, p0, Lcom/google/whispernet/b;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_7

    .line 65
    iget-object v1, p1, Lcom/google/whispernet/b;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 71
    :cond_2
    iget-object v1, p0, Lcom/google/whispernet/b;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_8

    .line 72
    iget-object v1, p1, Lcom/google/whispernet/b;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 78
    :cond_3
    iget-object v1, p0, Lcom/google/whispernet/b;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_9

    .line 79
    iget-object v1, p1, Lcom/google/whispernet/b;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 85
    :cond_4
    iget-object v1, p0, Lcom/google/whispernet/b;->d:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 86
    iget-object v1, p1, Lcom/google/whispernet/b;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 92
    :cond_5
    iget-object v1, p0, Lcom/google/whispernet/b;->e:Ljava/lang/Integer;

    if-nez v1, :cond_b

    .line 93
    iget-object v1, p1, Lcom/google/whispernet/b;->e:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 99
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/whispernet/b;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 68
    :cond_7
    iget-object v1, p0, Lcom/google/whispernet/b;->a:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/whispernet/b;->a:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 75
    :cond_8
    iget-object v1, p0, Lcom/google/whispernet/b;->b:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/whispernet/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 82
    :cond_9
    iget-object v1, p0, Lcom/google/whispernet/b;->c:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/whispernet/b;->c:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 89
    :cond_a
    iget-object v1, p0, Lcom/google/whispernet/b;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/whispernet/b;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 96
    :cond_b
    iget-object v1, p0, Lcom/google/whispernet/b;->e:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/whispernet/b;->e:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 104
    iget-object v0, p0, Lcom/google/whispernet/b;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 107
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/whispernet/b;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 109
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/whispernet/b;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 111
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/whispernet/b;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 113
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/whispernet/b;->e:Ljava/lang/Integer;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 115
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/whispernet/b;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    return v0

    .line 104
    :cond_0
    iget-object v0, p0, Lcom/google/whispernet/b;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/google/whispernet/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_1

    .line 109
    :cond_2
    iget-object v0, p0, Lcom/google/whispernet/b;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_2

    .line 111
    :cond_3
    iget-object v0, p0, Lcom/google/whispernet/b;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 113
    :cond_4
    iget-object v1, p0, Lcom/google/whispernet/b;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/whispernet/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/b;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/b;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/b;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/b;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/whispernet/b;->e:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/whispernet/b;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 123
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/whispernet/b;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 125
    :cond_0
    iget-object v0, p0, Lcom/google/whispernet/b;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 126
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/whispernet/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 128
    :cond_1
    iget-object v0, p0, Lcom/google/whispernet/b;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 129
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/whispernet/b;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 131
    :cond_2
    iget-object v0, p0, Lcom/google/whispernet/b;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 132
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/whispernet/b;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 134
    :cond_3
    iget-object v0, p0, Lcom/google/whispernet/b;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 135
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/whispernet/b;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 137
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 138
    return-void
.end method
