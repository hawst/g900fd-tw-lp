.class public final Lcom/google/ab/b/a/a/s;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Lcom/google/ab/b/a/c/b;

.field private c:Z

.field private d:I

.field private e:Z

.field private f:J

.field private g:Z

.field private h:J

.field private i:Ljava/util/List;

.field private j:Z

.field private k:Lcom/google/protobuf/a/a;

.field private l:Z

.field private m:Ljava/lang/String;

.field private n:Z

.field private o:I

.field private p:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, -0x1

    .line 5898
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 5903
    iput v1, p0, Lcom/google/ab/b/a/a/s;->d:I

    .line 5920
    iput-wide v2, p0, Lcom/google/ab/b/a/a/s;->f:J

    .line 5937
    iput-wide v2, p0, Lcom/google/ab/b/a/a/s;->h:J

    .line 5953
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ab/b/a/a/s;->i:Ljava/util/List;

    .line 5987
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ab/b/a/a/s;->b:Lcom/google/ab/b/a/c/b;

    .line 6007
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/ab/b/a/a/s;->k:Lcom/google/protobuf/a/a;

    .line 6024
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ab/b/a/a/s;->m:Ljava/lang/String;

    .line 6041
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/ab/b/a/a/s;->o:I

    .line 6101
    iput v1, p0, Lcom/google/ab/b/a/a/s;->p:I

    .line 5898
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 6104
    iget v0, p0, Lcom/google/ab/b/a/a/s;->p:I

    if-gez v0, :cond_0

    .line 6106
    invoke-virtual {p0}, Lcom/google/ab/b/a/a/s;->b()I

    .line 6108
    :cond_0
    iget v0, p0, Lcom/google/ab/b/a/a/s;->p:I

    return v0
.end method

.method public final a(I)Lcom/google/ab/b/a/a/s;
    .locals 1

    .prologue
    .line 5907
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/a/s;->c:Z

    .line 5908
    iput p1, p0, Lcom/google/ab/b/a/a/s;->d:I

    .line 5909
    return-object p0
.end method

.method public final a(J)Lcom/google/ab/b/a/a/s;
    .locals 1

    .prologue
    .line 5924
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/a/s;->e:Z

    .line 5925
    iput-wide p1, p0, Lcom/google/ab/b/a/a/s;->f:J

    .line 5926
    return-object p0
.end method

.method public final a(Lcom/google/ab/b/a/a/u;)Lcom/google/ab/b/a/a/s;
    .locals 1

    .prologue
    .line 5970
    if-nez p1, :cond_0

    .line 5971
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5973
    :cond_0
    iget-object v0, p0, Lcom/google/ab/b/a/a/s;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 5974
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/ab/b/a/a/s;->i:Ljava/util/List;

    .line 5976
    :cond_1
    iget-object v0, p0, Lcom/google/ab/b/a/a/s;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 5977
    return-object p0
.end method

.method public final a(Lcom/google/ab/b/a/c/b;)Lcom/google/ab/b/a/a/s;
    .locals 1

    .prologue
    .line 5991
    if-nez p1, :cond_0

    .line 5992
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 5994
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/a/s;->a:Z

    .line 5995
    iput-object p1, p0, Lcom/google/ab/b/a/a/s;->b:Lcom/google/ab/b/a/c/b;

    .line 5996
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5895
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/a/s;->a(I)Lcom/google/ab/b/a/a/s;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->k()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/ab/b/a/a/s;->a(J)Lcom/google/ab/b/a/a/s;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->k()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/ab/b/a/a/s;->b(J)Lcom/google/ab/b/a/a/s;

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/ab/b/a/a/u;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/u;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/a/s;->a(Lcom/google/ab/b/a/a/u;)Lcom/google/ab/b/a/a/s;

    goto :goto_0

    :sswitch_5
    new-instance v0, Lcom/google/ab/b/a/c/b;

    invoke-direct {v0}, Lcom/google/ab/b/a/c/b;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/a/s;->a(Lcom/google/ab/b/a/c/b;)Lcom/google/ab/b/a/a/s;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/s;->j:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/s;->k:Lcom/google/protobuf/a/a;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/s;->l:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/s;->m:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/s;->n:Z

    iput v0, p0, Lcom/google/ab/b/a/a/s;->o:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x11 -> :sswitch_2
        0x19 -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
        0x1388a -> :sswitch_6
        0x13892 -> :sswitch_7
        0x13898 -> :sswitch_8
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 6075
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/s;->c:Z

    if-eqz v0, :cond_0

    .line 6076
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/ab/b/a/a/s;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 6078
    :cond_0
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/s;->e:Z

    if-eqz v0, :cond_1

    .line 6079
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/ab/b/a/a/s;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->c(IJ)V

    .line 6081
    :cond_1
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/s;->g:Z

    if-eqz v0, :cond_2

    .line 6082
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/ab/b/a/a/s;->h:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->c(IJ)V

    .line 6084
    :cond_2
    iget-object v0, p0, Lcom/google/ab/b/a/a/s;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ab/b/a/a/u;

    .line 6085
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    goto :goto_0

    .line 6087
    :cond_3
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/s;->a:Z

    if-eqz v0, :cond_4

    .line 6088
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ab/b/a/a/s;->b:Lcom/google/ab/b/a/c/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 6090
    :cond_4
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/s;->j:Z

    if-eqz v0, :cond_5

    .line 6091
    const/16 v0, 0x2711

    iget-object v1, p0, Lcom/google/ab/b/a/a/s;->k:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 6093
    :cond_5
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/s;->l:Z

    if-eqz v0, :cond_6

    .line 6094
    const/16 v0, 0x2712

    iget-object v1, p0, Lcom/google/ab/b/a/a/s;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 6096
    :cond_6
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/s;->n:Z

    if-eqz v0, :cond_7

    .line 6097
    const/16 v0, 0x2713

    iget v1, p0, Lcom/google/ab/b/a/a/s;->o:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 6099
    :cond_7
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 6113
    const/4 v0, 0x0

    .line 6114
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/s;->c:Z

    if-eqz v1, :cond_0

    .line 6115
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/ab/b/a/a/s;->d:I

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6118
    :cond_0
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/s;->e:Z

    if-eqz v1, :cond_1

    .line 6119
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/ab/b/a/a/s;->f:J

    invoke-static {v1}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 6122
    :cond_1
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/s;->g:Z

    if-eqz v1, :cond_2

    .line 6123
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/ab/b/a/a/s;->h:J

    invoke-static {v1}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 6126
    :cond_2
    iget-object v1, p0, Lcom/google/ab/b/a/a/s;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/ab/b/a/a/u;

    .line 6127
    const/4 v3, 0x4

    invoke-static {v3, v0}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 6129
    goto :goto_0

    .line 6130
    :cond_3
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/s;->a:Z

    if-eqz v0, :cond_4

    .line 6131
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/ab/b/a/a/s;->b:Lcom/google/ab/b/a/c/b;

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v1, v0

    .line 6134
    :cond_4
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/s;->j:Z

    if-eqz v0, :cond_5

    .line 6135
    const/16 v0, 0x2711

    iget-object v2, p0, Lcom/google/ab/b/a/a/s;->k:Lcom/google/protobuf/a/a;

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v0

    add-int/2addr v1, v0

    .line 6138
    :cond_5
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/s;->l:Z

    if-eqz v0, :cond_6

    .line 6139
    const/16 v0, 0x2712

    iget-object v2, p0, Lcom/google/ab/b/a/a/s;->m:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 6142
    :cond_6
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/s;->n:Z

    if-eqz v0, :cond_7

    .line 6143
    const/16 v0, 0x2713

    iget v2, p0, Lcom/google/ab/b/a/a/s;->o:I

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 6146
    :cond_7
    iput v1, p0, Lcom/google/ab/b/a/a/s;->p:I

    .line 6147
    return v1
.end method

.method public final b(J)Lcom/google/ab/b/a/a/s;
    .locals 1

    .prologue
    .line 5941
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/a/s;->g:Z

    .line 5942
    iput-wide p1, p0, Lcom/google/ab/b/a/a/s;->h:J

    .line 5943
    return-object p0
.end method
