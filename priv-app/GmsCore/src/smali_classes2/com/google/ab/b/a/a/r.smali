.class public final Lcom/google/ab/b/a/a/r;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/protobuf/a/a;

.field public b:J

.field public c:Z

.field public d:Lcom/google/ab/b/a/a/q;

.field public e:Ljava/lang/String;

.field public f:Lcom/google/ab/b/a/a/f;

.field public g:Z

.field public h:Lcom/google/ab/b/a/a/i;

.field public i:Z

.field public j:Lcom/google/ab/b/a/a/c;

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;

.field private o:Z

.field private p:Z

.field private q:Lcom/google/ab/b/a/a/g;

.field private r:Z

.field private s:Z

.field private t:I

.field private u:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 4013
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 4018
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/ab/b/a/a/r;->a:Lcom/google/protobuf/a/a;

    .line 4035
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/ab/b/a/a/r;->b:J

    .line 4052
    iput-object v2, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    .line 4072
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ab/b/a/a/r;->n:Ljava/lang/String;

    .line 4089
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ab/b/a/a/r;->e:Ljava/lang/String;

    .line 4106
    iput-object v2, p0, Lcom/google/ab/b/a/a/r;->q:Lcom/google/ab/b/a/a/g;

    .line 4126
    iput-object v2, p0, Lcom/google/ab/b/a/a/r;->f:Lcom/google/ab/b/a/a/f;

    .line 4146
    iput-object v2, p0, Lcom/google/ab/b/a/a/r;->h:Lcom/google/ab/b/a/a/i;

    .line 4166
    iput-object v2, p0, Lcom/google/ab/b/a/a/r;->j:Lcom/google/ab/b/a/a/c;

    .line 4186
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/ab/b/a/a/r;->t:I

    .line 4254
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ab/b/a/a/r;->u:I

    .line 4013
    return-void
.end method

.method public static a([B)Lcom/google/ab/b/a/a/r;
    .locals 2

    .prologue
    .line 4382
    new-instance v0, Lcom/google/ab/b/a/a/r;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/r;-><init>()V

    array-length v1, p0

    invoke-super {v0, p0, v1}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v0

    check-cast v0, Lcom/google/ab/b/a/a/r;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 4257
    iget v0, p0, Lcom/google/ab/b/a/a/r;->u:I

    if-gez v0, :cond_0

    .line 4259
    invoke-virtual {p0}, Lcom/google/ab/b/a/a/r;->b()I

    .line 4261
    :cond_0
    iget v0, p0, Lcom/google/ab/b/a/a/r;->u:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 4010
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/r;->k:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/r;->a:Lcom/google/protobuf/a/a;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/ab/b/a/a/q;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/q;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/r;->c:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/ab/b/a/a/f;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/f;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/r;->r:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/r;->f:Lcom/google/ab/b/a/a/f;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/r;->m:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/r;->n:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/r;->o:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/r;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/ab/b/a/a/i;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/i;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/r;->g:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/r;->h:Lcom/google/ab/b/a/a/i;

    goto :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/ab/b/a/a/c;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/c;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/r;->i:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/r;->j:Lcom/google/ab/b/a/a/c;

    goto :goto_0

    :sswitch_8
    new-instance v0, Lcom/google/ab/b/a/a/g;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/g;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/r;->p:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/r;->q:Lcom/google/ab/b/a/a/g;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/r;->l:Z

    iput-wide v0, p0, Lcom/google/ab/b/a/a/r;->b:J

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/r;->s:Z

    iput v0, p0, Lcom/google/ab/b/a/a/r;->t:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
        0x318 -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 4222
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/r;->k:Z

    if-eqz v0, :cond_0

    .line 4223
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->a:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 4225
    :cond_0
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/r;->c:Z

    if-eqz v0, :cond_1

    .line 4226
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 4228
    :cond_1
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/r;->r:Z

    if-eqz v0, :cond_2

    .line 4229
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->f:Lcom/google/ab/b/a/a/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 4231
    :cond_2
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/r;->m:Z

    if-eqz v0, :cond_3

    .line 4232
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 4234
    :cond_3
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/r;->o:Z

    if-eqz v0, :cond_4

    .line 4235
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 4237
    :cond_4
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/r;->g:Z

    if-eqz v0, :cond_5

    .line 4238
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->h:Lcom/google/ab/b/a/a/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 4240
    :cond_5
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/r;->i:Z

    if-eqz v0, :cond_6

    .line 4241
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->j:Lcom/google/ab/b/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 4243
    :cond_6
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/r;->p:Z

    if-eqz v0, :cond_7

    .line 4244
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->q:Lcom/google/ab/b/a/a/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 4246
    :cond_7
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/r;->l:Z

    if-eqz v0, :cond_8

    .line 4247
    const/16 v0, 0xa

    iget-wide v2, p0, Lcom/google/ab/b/a/a/r;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 4249
    :cond_8
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/r;->s:Z

    if-eqz v0, :cond_9

    .line 4250
    const/16 v0, 0x63

    iget v1, p0, Lcom/google/ab/b/a/a/r;->t:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 4252
    :cond_9
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 4266
    const/4 v0, 0x0

    .line 4267
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/r;->k:Z

    if-eqz v1, :cond_0

    .line 4268
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ab/b/a/a/r;->a:Lcom/google/protobuf/a/a;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 4271
    :cond_0
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/r;->c:Z

    if-eqz v1, :cond_1

    .line 4272
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ab/b/a/a/r;->d:Lcom/google/ab/b/a/a/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4275
    :cond_1
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/r;->r:Z

    if-eqz v1, :cond_2

    .line 4276
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ab/b/a/a/r;->f:Lcom/google/ab/b/a/a/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4279
    :cond_2
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/r;->m:Z

    if-eqz v1, :cond_3

    .line 4280
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ab/b/a/a/r;->n:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4283
    :cond_3
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/r;->o:Z

    if-eqz v1, :cond_4

    .line 4284
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ab/b/a/a/r;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4287
    :cond_4
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/r;->g:Z

    if-eqz v1, :cond_5

    .line 4288
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ab/b/a/a/r;->h:Lcom/google/ab/b/a/a/i;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4291
    :cond_5
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/r;->i:Z

    if-eqz v1, :cond_6

    .line 4292
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ab/b/a/a/r;->j:Lcom/google/ab/b/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4295
    :cond_6
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/r;->p:Z

    if-eqz v1, :cond_7

    .line 4296
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/ab/b/a/a/r;->q:Lcom/google/ab/b/a/a/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4299
    :cond_7
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/r;->l:Z

    if-eqz v1, :cond_8

    .line 4300
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/ab/b/a/a/r;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4303
    :cond_8
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/r;->s:Z

    if-eqz v1, :cond_9

    .line 4304
    const/16 v1, 0x63

    iget v2, p0, Lcom/google/ab/b/a/a/r;->t:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4307
    :cond_9
    iput v0, p0, Lcom/google/ab/b/a/a/r;->u:I

    .line 4308
    return v0
.end method
