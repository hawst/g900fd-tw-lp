.class public final Lcom/google/ab/b/a/c/b;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Z

.field private c:Z

.field private d:Z

.field private e:J

.field private f:Z

.field private g:J

.field private h:Z

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 429
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 434
    iput-boolean v0, p0, Lcom/google/ab/b/a/c/b;->a:Z

    .line 451
    iput-wide v2, p0, Lcom/google/ab/b/a/c/b;->e:J

    .line 468
    iput-wide v2, p0, Lcom/google/ab/b/a/c/b;->g:J

    .line 485
    iput-boolean v0, p0, Lcom/google/ab/b/a/c/b;->h:Z

    .line 529
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ab/b/a/c/b;->i:I

    .line 429
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 532
    iget v0, p0, Lcom/google/ab/b/a/c/b;->i:I

    if-gez v0, :cond_0

    .line 534
    invoke-virtual {p0}, Lcom/google/ab/b/a/c/b;->b()I

    .line 536
    :cond_0
    iget v0, p0, Lcom/google/ab/b/a/c/b;->i:I

    return v0
.end method

.method public final a(J)Lcom/google/ab/b/a/c/b;
    .locals 1

    .prologue
    .line 455
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/c/b;->d:Z

    .line 456
    iput-wide p1, p0, Lcom/google/ab/b/a/c/b;->e:J

    .line 457
    return-object p0
.end method

.method public final a(Z)Lcom/google/ab/b/a/c/b;
    .locals 1

    .prologue
    .line 438
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/c/b;->c:Z

    .line 439
    iput-boolean p1, p0, Lcom/google/ab/b/a/c/b;->a:Z

    .line 440
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 426
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/c/b;->a(Z)Lcom/google/ab/b/a/c/b;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/ab/b/a/c/b;->a(J)Lcom/google/ab/b/a/c/b;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/ab/b/a/c/b;->b(J)Lcom/google/ab/b/a/c/b;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/c/b;->b(Z)Lcom/google/ab/b/a/c/b;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x18 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 515
    iget-boolean v0, p0, Lcom/google/ab/b/a/c/b;->c:Z

    if-eqz v0, :cond_0

    .line 516
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/ab/b/a/c/b;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 518
    :cond_0
    iget-boolean v0, p0, Lcom/google/ab/b/a/c/b;->d:Z

    if-eqz v0, :cond_1

    .line 519
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/ab/b/a/c/b;->e:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 521
    :cond_1
    iget-boolean v0, p0, Lcom/google/ab/b/a/c/b;->f:Z

    if-eqz v0, :cond_2

    .line 522
    const/4 v0, 0x4

    iget-wide v2, p0, Lcom/google/ab/b/a/c/b;->g:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 524
    :cond_2
    iget-boolean v0, p0, Lcom/google/ab/b/a/c/b;->b:Z

    if-eqz v0, :cond_3

    .line 525
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/ab/b/a/c/b;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 527
    :cond_3
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 541
    const/4 v0, 0x0

    .line 542
    iget-boolean v1, p0, Lcom/google/ab/b/a/c/b;->c:Z

    if-eqz v1, :cond_0

    .line 543
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/ab/b/a/c/b;->a:Z

    invoke-static {v0}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/lit8 v0, v0, 0x0

    .line 546
    :cond_0
    iget-boolean v1, p0, Lcom/google/ab/b/a/c/b;->d:Z

    if-eqz v1, :cond_1

    .line 547
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/ab/b/a/c/b;->e:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 550
    :cond_1
    iget-boolean v1, p0, Lcom/google/ab/b/a/c/b;->f:Z

    if-eqz v1, :cond_2

    .line 551
    const/4 v1, 0x4

    iget-wide v2, p0, Lcom/google/ab/b/a/c/b;->g:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 554
    :cond_2
    iget-boolean v1, p0, Lcom/google/ab/b/a/c/b;->b:Z

    if-eqz v1, :cond_3

    .line 555
    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/ab/b/a/c/b;->h:Z

    invoke-static {v1}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 558
    :cond_3
    iput v0, p0, Lcom/google/ab/b/a/c/b;->i:I

    .line 559
    return v0
.end method

.method public final b(J)Lcom/google/ab/b/a/c/b;
    .locals 1

    .prologue
    .line 472
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/c/b;->f:Z

    .line 473
    iput-wide p1, p0, Lcom/google/ab/b/a/c/b;->g:J

    .line 474
    return-object p0
.end method

.method public final b(Z)Lcom/google/ab/b/a/c/b;
    .locals 1

    .prologue
    .line 489
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/c/b;->b:Z

    .line 490
    iput-boolean p1, p0, Lcom/google/ab/b/a/c/b;->h:Z

    .line 491
    return-object p0
.end method
