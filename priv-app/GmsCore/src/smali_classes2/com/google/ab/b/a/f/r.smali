.class public final Lcom/google/ab/b/a/f/r;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field a:Z

.field b:Lcom/google/protobuf/a/a;

.field c:I

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 815
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 820
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/ab/b/a/f/r;->b:Lcom/google/protobuf/a/a;

    .line 837
    const v0, 0x10001

    iput v0, p0, Lcom/google/ab/b/a/f/r;->c:I

    .line 874
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ab/b/a/f/r;->e:I

    .line 815
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 877
    iget v0, p0, Lcom/google/ab/b/a/f/r;->e:I

    if-gez v0, :cond_0

    .line 879
    invoke-virtual {p0}, Lcom/google/ab/b/a/f/r;->b()I

    .line 881
    :cond_0
    iget v0, p0, Lcom/google/ab/b/a/f/r;->e:I

    return v0
.end method

.method public final a(I)Lcom/google/ab/b/a/f/r;
    .locals 1

    .prologue
    .line 841
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/f/r;->d:Z

    .line 842
    iput p1, p0, Lcom/google/ab/b/a/f/r;->c:I

    .line 843
    return-object p0
.end method

.method public final a(Lcom/google/protobuf/a/a;)Lcom/google/ab/b/a/f/r;
    .locals 1

    .prologue
    .line 824
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/f/r;->a:Z

    .line 825
    iput-object p1, p0, Lcom/google/ab/b/a/f/r;->b:Lcom/google/protobuf/a/a;

    .line 826
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 1

    .prologue
    .line 812
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/f/r;->a(Lcom/google/protobuf/a/a;)Lcom/google/ab/b/a/f/r;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/f/r;->a(I)Lcom/google/ab/b/a/f/r;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 866
    iget-boolean v0, p0, Lcom/google/ab/b/a/f/r;->a:Z

    if-eqz v0, :cond_0

    .line 867
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ab/b/a/f/r;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 869
    :cond_0
    iget-boolean v0, p0, Lcom/google/ab/b/a/f/r;->d:Z

    if-eqz v0, :cond_1

    .line 870
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/ab/b/a/f/r;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 872
    :cond_1
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 886
    const/4 v0, 0x0

    .line 887
    iget-boolean v1, p0, Lcom/google/ab/b/a/f/r;->a:Z

    if-eqz v1, :cond_0

    .line 888
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ab/b/a/f/r;->b:Lcom/google/protobuf/a/a;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 891
    :cond_0
    iget-boolean v1, p0, Lcom/google/ab/b/a/f/r;->d:Z

    if-eqz v1, :cond_1

    .line 892
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/ab/b/a/f/r;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 895
    :cond_1
    iput v0, p0, Lcom/google/ab/b/a/f/r;->e:I

    .line 896
    return v0
.end method
