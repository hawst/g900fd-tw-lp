.class public final Lcom/google/ab/b/a/a/u;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:J

.field private c:Z

.field private d:Z

.field private e:Z

.field private f:I

.field private g:Z

.field private h:Lcom/google/ab/b/a/a/k;

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 5695
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 5714
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/ab/b/a/a/u;->b:J

    .line 5731
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ab/b/a/a/u;->d:Z

    .line 5748
    iput v2, p0, Lcom/google/ab/b/a/a/u;->f:I

    .line 5765
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ab/b/a/a/u;->h:Lcom/google/ab/b/a/a/k;

    .line 5812
    iput v2, p0, Lcom/google/ab/b/a/a/u;->i:I

    .line 5695
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 5815
    iget v0, p0, Lcom/google/ab/b/a/a/u;->i:I

    if-gez v0, :cond_0

    .line 5817
    invoke-virtual {p0}, Lcom/google/ab/b/a/a/u;->b()I

    .line 5819
    :cond_0
    iget v0, p0, Lcom/google/ab/b/a/a/u;->i:I

    return v0
.end method

.method public final a(I)Lcom/google/ab/b/a/a/u;
    .locals 1

    .prologue
    .line 5752
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/a/u;->e:Z

    .line 5753
    iput p1, p0, Lcom/google/ab/b/a/a/u;->f:I

    .line 5754
    return-object p0
.end method

.method public final a(J)Lcom/google/ab/b/a/a/u;
    .locals 1

    .prologue
    .line 5718
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/a/u;->a:Z

    .line 5719
    iput-wide p1, p0, Lcom/google/ab/b/a/a/u;->b:J

    .line 5720
    return-object p0
.end method

.method public final a(Z)Lcom/google/ab/b/a/a/u;
    .locals 1

    .prologue
    .line 5735
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/a/u;->c:Z

    .line 5736
    iput-boolean p1, p0, Lcom/google/ab/b/a/a/u;->d:Z

    .line 5737
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 5692
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/ab/b/a/a/u;->a(J)Lcom/google/ab/b/a/a/u;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/a/u;->a(Z)Lcom/google/ab/b/a/a/u;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/a/u;->a(I)Lcom/google/ab/b/a/a/u;

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/ab/b/a/a/k;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/k;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/ab/b/a/a/u;->g:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/u;->h:Lcom/google/ab/b/a/a/k;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 5798
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/u;->a:Z

    if-eqz v0, :cond_0

    .line 5799
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/ab/b/a/a/u;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 5801
    :cond_0
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/u;->c:Z

    if-eqz v0, :cond_1

    .line 5802
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/ab/b/a/a/u;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 5804
    :cond_1
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/u;->e:Z

    if-eqz v0, :cond_2

    .line 5805
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/ab/b/a/a/u;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 5807
    :cond_2
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/u;->g:Z

    if-eqz v0, :cond_3

    .line 5808
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ab/b/a/a/u;->h:Lcom/google/ab/b/a/a/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 5810
    :cond_3
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 5824
    const/4 v0, 0x0

    .line 5825
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/u;->a:Z

    if-eqz v1, :cond_0

    .line 5826
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/ab/b/a/a/u;->b:J

    invoke-static {v0, v2, v3}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 5829
    :cond_0
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/u;->c:Z

    if-eqz v1, :cond_1

    .line 5830
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/ab/b/a/a/u;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5833
    :cond_1
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/u;->e:Z

    if-eqz v1, :cond_2

    .line 5834
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/ab/b/a/a/u;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5837
    :cond_2
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/u;->g:Z

    if-eqz v1, :cond_3

    .line 5838
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ab/b/a/a/u;->h:Lcom/google/ab/b/a/a/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5841
    :cond_3
    iput v0, p0, Lcom/google/ab/b/a/a/u;->i:I

    .line 5842
    return v0
.end method
