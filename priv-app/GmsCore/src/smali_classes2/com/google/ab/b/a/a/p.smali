.class public final Lcom/google/ab/b/a/a/p;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ab/b/a/a/s;

.field private b:Z

.field private c:Lcom/google/ab/b/a/a/r;

.field private d:Z

.field private e:Z

.field private f:Lcom/google/protobuf/a/a;

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6221
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 6226
    iput-object v0, p0, Lcom/google/ab/b/a/a/p;->c:Lcom/google/ab/b/a/a/r;

    .line 6246
    iput-object v0, p0, Lcom/google/ab/b/a/a/p;->a:Lcom/google/ab/b/a/a/s;

    .line 6266
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/ab/b/a/a/p;->f:Lcom/google/protobuf/a/a;

    .line 6306
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ab/b/a/a/p;->g:I

    .line 6221
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 6309
    iget v0, p0, Lcom/google/ab/b/a/a/p;->g:I

    if-gez v0, :cond_0

    .line 6311
    invoke-virtual {p0}, Lcom/google/ab/b/a/a/p;->b()I

    .line 6313
    :cond_0
    iget v0, p0, Lcom/google/ab/b/a/a/p;->g:I

    return v0
.end method

.method public final a(Lcom/google/ab/b/a/a/r;)Lcom/google/ab/b/a/a/p;
    .locals 1

    .prologue
    .line 6230
    if-nez p1, :cond_0

    .line 6231
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6233
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/a/p;->b:Z

    .line 6234
    iput-object p1, p0, Lcom/google/ab/b/a/a/p;->c:Lcom/google/ab/b/a/a/r;

    .line 6235
    return-object p0
.end method

.method public final a(Lcom/google/ab/b/a/a/s;)Lcom/google/ab/b/a/a/p;
    .locals 1

    .prologue
    .line 6250
    if-nez p1, :cond_0

    .line 6251
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6253
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/a/p;->d:Z

    .line 6254
    iput-object p1, p0, Lcom/google/ab/b/a/a/p;->a:Lcom/google/ab/b/a/a/s;

    .line 6255
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 6218
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/ab/b/a/a/r;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/r;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/a/p;->a(Lcom/google/ab/b/a/a/r;)Lcom/google/ab/b/a/a/p;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/ab/b/a/a/s;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/s;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/a/p;->a(Lcom/google/ab/b/a/a/s;)Lcom/google/ab/b/a/a/p;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/ab/b/a/a/p;->e:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/p;->f:Lcom/google/protobuf/a/a;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 6295
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/p;->b:Z

    if-eqz v0, :cond_0

    .line 6296
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ab/b/a/a/p;->c:Lcom/google/ab/b/a/a/r;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 6298
    :cond_0
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/p;->d:Z

    if-eqz v0, :cond_1

    .line 6299
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ab/b/a/a/p;->a:Lcom/google/ab/b/a/a/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 6301
    :cond_1
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/p;->e:Z

    if-eqz v0, :cond_2

    .line 6302
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ab/b/a/a/p;->f:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 6304
    :cond_2
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 6318
    const/4 v0, 0x0

    .line 6319
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/p;->b:Z

    if-eqz v1, :cond_0

    .line 6320
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ab/b/a/a/p;->c:Lcom/google/ab/b/a/a/r;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6323
    :cond_0
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/p;->d:Z

    if-eqz v1, :cond_1

    .line 6324
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ab/b/a/a/p;->a:Lcom/google/ab/b/a/a/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6327
    :cond_1
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/p;->e:Z

    if-eqz v1, :cond_2

    .line 6328
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ab/b/a/a/p;->f:Lcom/google/protobuf/a/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6331
    :cond_2
    iput v0, p0, Lcom/google/ab/b/a/a/p;->g:I

    .line 6332
    return v0
.end method
