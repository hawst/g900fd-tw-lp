.class public final Lcom/google/ab/b/a/a/j;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:J

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 3213
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 3218
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/ab/b/a/a/j;->b:J

    .line 3235
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ab/b/a/a/j;->d:Ljava/lang/String;

    .line 3271
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ab/b/a/a/j;->e:I

    .line 3213
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 3274
    iget v0, p0, Lcom/google/ab/b/a/a/j;->e:I

    if-gez v0, :cond_0

    .line 3276
    invoke-virtual {p0}, Lcom/google/ab/b/a/a/j;->b()I

    .line 3278
    :cond_0
    iget v0, p0, Lcom/google/ab/b/a/a/j;->e:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3210
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/j;->a:Z

    iput-wide v0, p0, Lcom/google/ab/b/a/a/j;->b:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/j;->c:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/j;->d:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 3263
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/j;->a:Z

    if-eqz v0, :cond_0

    .line 3264
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/ab/b/a/a/j;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 3266
    :cond_0
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/j;->c:Z

    if-eqz v0, :cond_1

    .line 3267
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ab/b/a/a/j;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 3269
    :cond_1
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 3283
    const/4 v0, 0x0

    .line 3284
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/j;->a:Z

    if-eqz v1, :cond_0

    .line 3285
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/ab/b/a/a/j;->b:J

    invoke-static {v0, v2, v3}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3288
    :cond_0
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/j;->c:Z

    if-eqz v1, :cond_1

    .line 3289
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ab/b/a/a/j;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3292
    :cond_1
    iput v0, p0, Lcom/google/ab/b/a/a/j;->e:I

    .line 3293
    return v0
.end method
