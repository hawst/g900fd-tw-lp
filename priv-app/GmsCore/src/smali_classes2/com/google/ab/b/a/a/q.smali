.class public final Lcom/google/ab/b/a/a/q;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:I

.field public c:I

.field public d:Z

.field public e:J

.field public f:Z

.field public g:Lcom/google/ab/b/a/a/e;

.field public h:Z

.field public i:Lcom/google/ab/b/a/a/h;

.field public j:Z

.field public k:Lcom/google/ab/b/a/a/b;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:Z

.field private s:Z

.field private t:I

.field private u:Ljava/util/List;

.field private v:Z

.field private w:Lcom/google/ab/b/a/a/j;

.field private x:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v3, 0x0

    const/4 v2, -0x1

    const/4 v1, 0x0

    .line 3436
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 3441
    iput-wide v4, p0, Lcom/google/ab/b/a/a/q;->a:J

    .line 3458
    iput v3, p0, Lcom/google/ab/b/a/a/q;->b:I

    .line 3475
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ab/b/a/a/q;->o:Ljava/lang/String;

    .line 3492
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ab/b/a/a/q;->q:Ljava/lang/String;

    .line 3509
    iput v3, p0, Lcom/google/ab/b/a/a/q;->c:I

    .line 3526
    iput v2, p0, Lcom/google/ab/b/a/a/q;->t:I

    .line 3543
    iput-wide v4, p0, Lcom/google/ab/b/a/a/q;->e:J

    .line 3559
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ab/b/a/a/q;->u:Ljava/util/List;

    .line 3587
    iput-object v1, p0, Lcom/google/ab/b/a/a/q;->w:Lcom/google/ab/b/a/a/j;

    .line 3607
    iput-object v1, p0, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    .line 3627
    iput-object v1, p0, Lcom/google/ab/b/a/a/q;->i:Lcom/google/ab/b/a/a/h;

    .line 3647
    iput-object v1, p0, Lcom/google/ab/b/a/a/q;->k:Lcom/google/ab/b/a/a/b;

    .line 3726
    iput v2, p0, Lcom/google/ab/b/a/a/q;->x:I

    .line 3436
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 3729
    iget v0, p0, Lcom/google/ab/b/a/a/q;->x:I

    if-gez v0, :cond_0

    .line 3731
    invoke-virtual {p0}, Lcom/google/ab/b/a/a/q;->b()I

    .line 3733
    :cond_0
    iget v0, p0, Lcom/google/ab/b/a/a/q;->x:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 3433
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->k()J

    move-result-wide v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/q;->l:Z

    iput-wide v0, p0, Lcom/google/ab/b/a/a/q;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/q;->m:Z

    iput v0, p0, Lcom/google/ab/b/a/a/q;->b:I

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/ab/b/a/a/e;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/e;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/q;->f:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/q;->n:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/q;->o:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/q;->p:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/q;->q:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    new-instance v0, Lcom/google/ab/b/a/a/h;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/h;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/q;->h:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/q;->i:Lcom/google/ab/b/a/a/h;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/q;->r:Z

    iput v0, p0, Lcom/google/ab/b/a/a/q;->c:I

    goto :goto_0

    :sswitch_8
    new-instance v0, Lcom/google/ab/b/a/a/b;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/b;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/q;->j:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/q;->k:Lcom/google/ab/b/a/a/b;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/q;->s:Z

    iput v0, p0, Lcom/google/ab/b/a/a/q;->t:I

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/q;->d:Z

    iput-wide v0, p0, Lcom/google/ab/b/a/a/q;->e:J

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/ab/b/a/a/q;->u:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->u:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :sswitch_c
    new-instance v0, Lcom/google/ab/b/a/a/j;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/j;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/q;->v:Z

    iput-object v0, p0, Lcom/google/ab/b/a/a/q;->w:Lcom/google/ab/b/a/a/j;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
        0x6a -> :sswitch_c
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 3688
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/q;->l:Z

    if-eqz v0, :cond_0

    .line 3689
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/ab/b/a/a/q;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->c(IJ)V

    .line 3691
    :cond_0
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/q;->m:Z

    if-eqz v0, :cond_1

    .line 3692
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/ab/b/a/a/q;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 3694
    :cond_1
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/q;->f:Z

    if-eqz v0, :cond_2

    .line 3695
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 3697
    :cond_2
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/q;->n:Z

    if-eqz v0, :cond_3

    .line 3698
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 3700
    :cond_3
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/q;->p:Z

    if-eqz v0, :cond_4

    .line 3701
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 3703
    :cond_4
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/q;->h:Z

    if-eqz v0, :cond_5

    .line 3704
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->i:Lcom/google/ab/b/a/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 3706
    :cond_5
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/q;->r:Z

    if-eqz v0, :cond_6

    .line 3707
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/ab/b/a/a/q;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 3709
    :cond_6
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/q;->j:Z

    if-eqz v0, :cond_7

    .line 3710
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->k:Lcom/google/ab/b/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 3712
    :cond_7
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/q;->s:Z

    if-eqz v0, :cond_8

    .line 3713
    const/16 v0, 0xa

    iget v1, p0, Lcom/google/ab/b/a/a/q;->t:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 3715
    :cond_8
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/q;->d:Z

    if-eqz v0, :cond_9

    .line 3716
    const/16 v0, 0xb

    iget-wide v2, p0, Lcom/google/ab/b/a/a/q;->e:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 3718
    :cond_9
    iget-object v0, p0, Lcom/google/ab/b/a/a/q;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 3719
    const/16 v2, 0xc

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->a(II)V

    goto :goto_0

    .line 3721
    :cond_a
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/q;->v:Z

    if-eqz v0, :cond_b

    .line 3722
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->w:Lcom/google/ab/b/a/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 3724
    :cond_b
    return-void
.end method

.method public final b()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 3738
    .line 3739
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/q;->l:Z

    if-eqz v0, :cond_b

    .line 3740
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/ab/b/a/a/q;->a:J

    invoke-static {v0}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x8

    add-int/lit8 v0, v0, 0x0

    .line 3743
    :goto_0
    iget-boolean v2, p0, Lcom/google/ab/b/a/a/q;->m:Z

    if-eqz v2, :cond_0

    .line 3744
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/ab/b/a/a/q;->b:I

    invoke-static {v2, v3}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3747
    :cond_0
    iget-boolean v2, p0, Lcom/google/ab/b/a/a/q;->f:Z

    if-eqz v2, :cond_1

    .line 3748
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/ab/b/a/a/q;->g:Lcom/google/ab/b/a/a/e;

    invoke-static {v2, v3}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3751
    :cond_1
    iget-boolean v2, p0, Lcom/google/ab/b/a/a/q;->n:Z

    if-eqz v2, :cond_2

    .line 3752
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/ab/b/a/a/q;->o:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3755
    :cond_2
    iget-boolean v2, p0, Lcom/google/ab/b/a/a/q;->p:Z

    if-eqz v2, :cond_3

    .line 3756
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/ab/b/a/a/q;->q:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3759
    :cond_3
    iget-boolean v2, p0, Lcom/google/ab/b/a/a/q;->h:Z

    if-eqz v2, :cond_4

    .line 3760
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/ab/b/a/a/q;->i:Lcom/google/ab/b/a/a/h;

    invoke-static {v2, v3}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3763
    :cond_4
    iget-boolean v2, p0, Lcom/google/ab/b/a/a/q;->r:Z

    if-eqz v2, :cond_5

    .line 3764
    const/4 v2, 0x7

    iget v3, p0, Lcom/google/ab/b/a/a/q;->c:I

    invoke-static {v2, v3}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3767
    :cond_5
    iget-boolean v2, p0, Lcom/google/ab/b/a/a/q;->j:Z

    if-eqz v2, :cond_6

    .line 3768
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/ab/b/a/a/q;->k:Lcom/google/ab/b/a/a/b;

    invoke-static {v2, v3}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3771
    :cond_6
    iget-boolean v2, p0, Lcom/google/ab/b/a/a/q;->s:Z

    if-eqz v2, :cond_7

    .line 3772
    const/16 v2, 0xa

    iget v3, p0, Lcom/google/ab/b/a/a/q;->t:I

    invoke-static {v2, v3}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3775
    :cond_7
    iget-boolean v2, p0, Lcom/google/ab/b/a/a/q;->d:Z

    if-eqz v2, :cond_a

    .line 3776
    const/16 v2, 0xb

    iget-wide v4, p0, Lcom/google/ab/b/a/a/q;->e:J

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    move v2, v0

    .line 3781
    :goto_1
    iget-object v0, p0, Lcom/google/ab/b/a/a/q;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 3782
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/protobuf/a/c;->a(I)I

    move-result v0

    add-int/2addr v1, v0

    .line 3784
    goto :goto_2

    .line 3785
    :cond_8
    add-int v0, v2, v1

    .line 3786
    iget-object v1, p0, Lcom/google/ab/b/a/a/q;->u:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3788
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/q;->v:Z

    if-eqz v1, :cond_9

    .line 3789
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/ab/b/a/a/q;->w:Lcom/google/ab/b/a/a/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3792
    :cond_9
    iput v0, p0, Lcom/google/ab/b/a/a/q;->x:I

    .line 3793
    return v0

    :cond_a
    move v2, v0

    goto :goto_1

    :cond_b
    move v0, v1

    goto/16 :goto_0
.end method
