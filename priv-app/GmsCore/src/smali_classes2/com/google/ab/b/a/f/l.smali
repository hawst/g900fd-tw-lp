.class public final Lcom/google/ab/b/a/f/l;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field a:Z

.field b:Lcom/google/protobuf/a/a;

.field c:Z

.field d:Lcom/google/protobuf/a/a;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 687
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 692
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/ab/b/a/f/l;->b:Lcom/google/protobuf/a/a;

    .line 709
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/ab/b/a/f/l;->d:Lcom/google/protobuf/a/a;

    .line 747
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ab/b/a/f/l;->e:I

    .line 687
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 750
    iget v0, p0, Lcom/google/ab/b/a/f/l;->e:I

    if-gez v0, :cond_0

    .line 752
    invoke-virtual {p0}, Lcom/google/ab/b/a/f/l;->b()I

    .line 754
    :cond_0
    iget v0, p0, Lcom/google/ab/b/a/f/l;->e:I

    return v0
.end method

.method public final a(Lcom/google/protobuf/a/a;)Lcom/google/ab/b/a/f/l;
    .locals 1

    .prologue
    .line 696
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/f/l;->a:Z

    .line 697
    iput-object p1, p0, Lcom/google/ab/b/a/f/l;->b:Lcom/google/protobuf/a/a;

    .line 698
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 1

    .prologue
    .line 684
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/f/l;->a(Lcom/google/protobuf/a/a;)Lcom/google/ab/b/a/f/l;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/f/l;->b(Lcom/google/protobuf/a/a;)Lcom/google/ab/b/a/f/l;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 739
    iget-boolean v0, p0, Lcom/google/ab/b/a/f/l;->a:Z

    if-eqz v0, :cond_0

    .line 740
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ab/b/a/f/l;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 742
    :cond_0
    iget-boolean v0, p0, Lcom/google/ab/b/a/f/l;->c:Z

    if-eqz v0, :cond_1

    .line 743
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ab/b/a/f/l;->d:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 745
    :cond_1
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 759
    const/4 v0, 0x0

    .line 760
    iget-boolean v1, p0, Lcom/google/ab/b/a/f/l;->a:Z

    if-eqz v1, :cond_0

    .line 761
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ab/b/a/f/l;->b:Lcom/google/protobuf/a/a;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 764
    :cond_0
    iget-boolean v1, p0, Lcom/google/ab/b/a/f/l;->c:Z

    if-eqz v1, :cond_1

    .line 765
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ab/b/a/f/l;->d:Lcom/google/protobuf/a/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v1

    add-int/2addr v0, v1

    .line 768
    :cond_1
    iput v0, p0, Lcom/google/ab/b/a/f/l;->e:I

    .line 769
    return v0
.end method

.method public final b(Lcom/google/protobuf/a/a;)Lcom/google/ab/b/a/f/l;
    .locals 1

    .prologue
    .line 713
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/f/l;->c:Z

    .line 714
    iput-object p1, p0, Lcom/google/ab/b/a/f/l;->d:Lcom/google/protobuf/a/a;

    .line 715
    return-object p0
.end method
