.class public final Lcom/google/ab/b/a/f/i;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/ab/b/a/f/q;)Lcom/google/ab/b/a/f/n;
    .locals 2

    .prologue
    .line 42
    iget-boolean v0, p0, Lcom/google/ab/b/a/f/q;->a:Z

    if-nez v0, :cond_0

    .line 43
    new-instance v0, Lcom/google/protobuf/a/e;

    const-string v1, "Missing header and body"

    invoke-direct {v0, v1}, Lcom/google/protobuf/a/e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    iget-object v0, p0, Lcom/google/ab/b/a/f/q;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/ab/b/a/f/o;->a([B)Lcom/google/ab/b/a/f/o;

    move-result-object v0

    iget-object v0, v0, Lcom/google/ab/b/a/f/o;->b:Lcom/google/ab/b/a/f/n;

    .line 46
    if-nez v0, :cond_1

    .line 47
    new-instance v0, Lcom/google/protobuf/a/e;

    const-string v1, "Missing header"

    invoke-direct {v0, v1}, Lcom/google/protobuf/a/e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_1
    iget-boolean v1, v0, Lcom/google/ab/b/a/f/n;->a:Z

    if-nez v1, :cond_2

    .line 51
    new-instance v0, Lcom/google/protobuf/a/e;

    const-string v1, "Missing header field(s)"

    invoke-direct {v0, v1}, Lcom/google/protobuf/a/e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_2
    :try_start_0
    iget v1, v0, Lcom/google/ab/b/a/f/n;->b:I

    invoke-static {v1}, Lcom/google/ab/b/a/f/c;->a(I)Lcom/google/ab/b/a/f/c;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    iget-boolean v1, v0, Lcom/google/ab/b/a/f/n;->c:Z

    if-eqz v1, :cond_3

    .line 62
    :try_start_1
    iget v1, v0, Lcom/google/ab/b/a/f/n;->d:I

    invoke-static {v1}, Lcom/google/ab/b/a/f/b;->a(I)Lcom/google/ab/b/a/f/b;
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 67
    :cond_3
    return-object v0

    .line 57
    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/protobuf/a/e;

    const-string v1, "Corrupt/unsupported SignatureScheme"

    invoke-direct {v0, v1}, Lcom/google/protobuf/a/e;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :catch_1
    move-exception v0

    new-instance v0, Lcom/google/protobuf/a/e;

    const-string v1, "Corrupt/unsupported EncryptionScheme"

    invoke-direct {v0, v1}, Lcom/google/protobuf/a/e;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static a(Lcom/google/ab/b/a/f/q;Ljava/security/Key;Lcom/google/ab/b/a/f/c;Lcom/google/ab/b/a/f/b;[BZ)Lcom/google/ab/b/a/f/o;
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 221
    iget-boolean v0, p0, Lcom/google/ab/b/a/f/q;->a:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/ab/b/a/f/q;->c:Z

    if-nez v0, :cond_1

    .line 222
    :cond_0
    new-instance v0, Ljava/security/SignatureException;

    const-string v1, "Signature failed verification"

    invoke-direct {v0, v1}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 224
    :cond_1
    iget-object v0, p0, Lcom/google/ab/b/a/f/q;->d:Lcom/google/protobuf/a/a;

    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v3

    .line 225
    iget-object v0, p0, Lcom/google/ab/b/a/f/q;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v0

    .line 226
    if-eqz p5, :cond_3

    .line 229
    :goto_0
    invoke-static {p1, p2, v3, v0}, Lcom/google/ab/b/a/f/a;->a(Ljava/security/Key;Lcom/google/ab/b/a/f/c;[B[B)Z

    move-result v4

    .line 230
    const/4 v0, 0x0

    .line 232
    :try_start_0
    iget-object v3, p0, Lcom/google/ab/b/a/f/q;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {v3}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/ab/b/a/f/o;->a([B)Lcom/google/ab/b/a/f/o;
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v3

    .line 234
    :try_start_1
    iget-boolean v0, v3, Lcom/google/ab/b/a/f/o;->a:Z

    if-eqz v0, :cond_2

    iget-boolean v0, v3, Lcom/google/ab/b/a/f/o;->c:Z

    if-nez v0, :cond_4

    .line 235
    :cond_2
    new-instance v0, Ljava/security/SignatureException;

    const-string v1, "Signature failed verification"

    invoke-direct {v0, v1}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/google/protobuf/a/e; {:try_start_1 .. :try_end_1} :catch_0

    .line 248
    :catch_0
    move-exception v0

    move-object v0, v3

    :goto_1
    move-object v3, v0

    .line 251
    :goto_2
    if-eqz v2, :cond_d

    .line 252
    return-object v3

    .line 226
    :cond_3
    invoke-static {v0, p4}, Lcom/google/ab/b/a/f/a;->b([B[B)[B

    move-result-object v0

    goto :goto_0

    .line 237
    :cond_4
    :try_start_2
    iget-object v0, v3, Lcom/google/ab/b/a/f/o;->b:Lcom/google/ab/b/a/f/n;

    iget v0, v0, Lcom/google/ab/b/a/f/n;->b:I

    invoke-virtual {p2}, Lcom/google/ab/b/a/f/c;->a()I

    move-result v5

    if-ne v0, v5, :cond_7

    move v0, v1

    :goto_3
    and-int/2addr v4, v0

    .line 238
    iget-object v0, v3, Lcom/google/ab/b/a/f/o;->b:Lcom/google/ab/b/a/f/n;

    iget v0, v0, Lcom/google/ab/b/a/f/n;->d:I

    invoke-virtual {p3}, Lcom/google/ab/b/a/f/b;->a()I

    move-result v5

    if-ne v0, v5, :cond_8

    move v0, v1

    :goto_4
    and-int/2addr v4, v0

    .line 240
    sget-object v0, Lcom/google/ab/b/a/f/b;->a:Lcom/google/ab/b/a/f/b;

    if-ne p3, v0, :cond_5

    iget-object v0, v3, Lcom/google/ab/b/a/f/o;->b:Lcom/google/ab/b/a/f/n;

    iget-boolean v0, v0, Lcom/google/ab/b/a/f/n;->g:Z

    if-nez v0, :cond_9

    :cond_5
    move v0, v1

    :goto_5
    and-int/2addr v4, v0

    .line 243
    sget-object v0, Lcom/google/ab/b/a/f/b;->a:Lcom/google/ab/b/a/f/b;

    if-eq p3, v0, :cond_6

    invoke-virtual {p2}, Lcom/google/ab/b/a/f/c;->c()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-object v0, v3, Lcom/google/ab/b/a/f/o;->b:Lcom/google/ab/b/a/f/n;

    iget-boolean v0, v0, Lcom/google/ab/b/a/f/n;->e:Z

    if-eqz v0, :cond_a

    :cond_6
    move v0, v1

    :goto_6
    and-int/2addr v4, v0

    .line 245
    if-nez p4, :cond_b

    move v0, v2

    .line 246
    :goto_7
    iget-object v5, v3, Lcom/google/ab/b/a/f/o;->b:Lcom/google/ab/b/a/f/n;

    iget v5, v5, Lcom/google/ab/b/a/f/n;->m:I

    if-ne v5, v0, :cond_c

    move v0, v1

    :goto_8
    and-int v2, v4, v0

    goto :goto_2

    :cond_7
    move v0, v2

    .line 237
    goto :goto_3

    :cond_8
    move v0, v2

    .line 238
    goto :goto_4

    :cond_9
    move v0, v2

    .line 240
    goto :goto_5

    :cond_a
    move v0, v2

    .line 243
    goto :goto_6

    .line 245
    :cond_b
    array-length v0, p4
    :try_end_2
    .catch Lcom/google/protobuf/a/e; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_7

    :cond_c
    move v0, v2

    .line 246
    goto :goto_8

    .line 254
    :cond_d
    new-instance v0, Ljava/security/SignatureException;

    const-string v1, "Signature failed verification"

    invoke-direct {v0, v1}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 248
    :catch_1
    move-exception v1

    goto :goto_1
.end method

.method public static a(Lcom/google/ab/b/a/f/q;Ljava/security/Key;Lcom/google/ab/b/a/f/c;Ljava/security/Key;Lcom/google/ab/b/a/f/b;)Lcom/google/ab/b/a/f/o;
    .locals 6

    .prologue
    .line 121
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/ab/b/a/f/i;->a(Lcom/google/ab/b/a/f/q;Ljava/security/Key;Lcom/google/ab/b/a/f/c;Ljava/security/Key;Lcom/google/ab/b/a/f/b;[B)Lcom/google/ab/b/a/f/o;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/ab/b/a/f/q;Ljava/security/Key;Lcom/google/ab/b/a/f/c;Ljava/security/Key;Lcom/google/ab/b/a/f/b;[B)Lcom/google/ab/b/a/f/o;
    .locals 7

    .prologue
    const/16 v6, 0x14

    .line 141
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 146
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 148
    :cond_1
    sget-object v0, Lcom/google/ab/b/a/f/b;->a:Lcom/google/ab/b/a/f/b;

    if-ne p4, v0, :cond_2

    .line 149
    new-instance v0, Ljava/security/SignatureException;

    const-string v1, "Not a signcrypted message"

    invoke-direct {v0, v1}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_2
    invoke-static {p1, p2, p3}, Lcom/google/ab/b/a/f/h;->a(Ljava/security/Key;Lcom/google/ab/b/a/f/c;Ljava/security/Key;)Z

    move-result v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    .line 155
    invoke-static/range {v0 .. v5}, Lcom/google/ab/b/a/f/i;->a(Lcom/google/ab/b/a/f/q;Ljava/security/Key;Lcom/google/ab/b/a/f/c;Lcom/google/ab/b/a/f/b;[BZ)Lcom/google/ab/b/a/f/o;

    move-result-object v1

    .line 164
    iget-object v0, v1, Lcom/google/ab/b/a/f/o;->b:Lcom/google/ab/b/a/f/n;

    .line 165
    iget-boolean v2, v0, Lcom/google/ab/b/a/f/n;->i:Z

    if-nez v2, :cond_3

    .line 166
    new-instance v0, Ljava/security/SignatureException;

    invoke-direct {v0}, Ljava/security/SignatureException;-><init>()V

    throw v0

    .line 169
    :cond_3
    :try_start_0
    iget-object v0, v0, Lcom/google/ab/b/a/f/n;->j:Lcom/google/protobuf/a/a;

    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v0

    iget-object v2, v1, Lcom/google/ab/b/a/f/o;->d:Lcom/google/protobuf/a/a;

    invoke-virtual {v2}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v2

    invoke-static {p3, p4, v0, v2}, Lcom/google/ab/b/a/f/a;->a(Ljava/security/Key;Lcom/google/ab/b/a/f/b;[B[B)[B
    :try_end_0
    .catch Ljava/security/InvalidAlgorithmParameterException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljavax/crypto/IllegalBlockSizeException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljavax/crypto/BadPaddingException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    .line 180
    if-nez v5, :cond_4

    .line 182
    invoke-static {v2}, Lcom/google/protobuf/a/a;->a([B)Lcom/google/protobuf/a/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/ab/b/a/f/o;->a(Lcom/google/protobuf/a/a;)Lcom/google/ab/b/a/f/o;

    move-result-object v0

    .line 208
    :goto_0
    return-object v0

    .line 173
    :catch_0
    move-exception v0

    new-instance v0, Ljava/security/SignatureException;

    invoke-direct {v0}, Ljava/security/SignatureException;-><init>()V

    throw v0

    .line 175
    :catch_1
    move-exception v0

    new-instance v0, Ljava/security/SignatureException;

    invoke-direct {v0}, Ljava/security/SignatureException;-><init>()V

    throw v0

    .line 177
    :catch_2
    move-exception v0

    new-instance v0, Ljava/security/SignatureException;

    invoke-direct {v0}, Ljava/security/SignatureException;-><init>()V

    throw v0

    .line 189
    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/google/ab/b/a/f/q;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v0

    new-instance v3, Lcom/google/ab/b/a/f/p;

    invoke-direct {v3}, Lcom/google/ab/b/a/f/p;-><init>()V

    array-length v4, v0

    invoke-virtual {v3, v0, v4}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v0

    check-cast v0, Lcom/google/ab/b/a/f/p;

    iget-object v0, v0, Lcom/google/ab/b/a/f/p;->a:Lcom/google/protobuf/a/a;

    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->b()[B
    :try_end_1
    .catch Lcom/google/protobuf/a/e; {:try_start_1 .. :try_end_1} :catch_3

    move-result-object v3

    .line 195
    const/4 v0, 0x0

    .line 196
    invoke-static {v3, p5}, Lcom/google/ab/b/a/f/a;->b([B[B)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/ab/b/a/f/a;->a([B)[B

    move-result-object v3

    .line 197
    array-length v4, v2

    if-lt v4, v6, :cond_5

    .line 198
    invoke-static {v2}, Lcom/google/ab/b/a/f/a;->b([B)[B

    move-result-object v4

    .line 199
    invoke-static {v4, v3}, Lcom/google/ab/b/a/f/a;->a([B[B)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 200
    const/4 v0, 0x1

    .line 203
    :cond_5
    if-nez v0, :cond_6

    .line 204
    new-instance v0, Ljava/security/SignatureException;

    invoke-direct {v0}, Ljava/security/SignatureException;-><init>()V

    throw v0

    .line 191
    :catch_3
    move-exception v0

    .line 193
    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 207
    :cond_6
    array-length v0, v2

    add-int/lit8 v0, v0, -0x14

    .line 208
    invoke-static {v2, v6, v0}, Lcom/google/protobuf/a/a;->a([BII)Lcom/google/protobuf/a/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/ab/b/a/f/o;->a(Lcom/google/protobuf/a/a;)Lcom/google/ab/b/a/f/o;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/ab/b/a/f/q;Ljava/security/Key;Lcom/google/ab/b/a/f/c;[B)Lcom/google/ab/b/a/f/o;
    .locals 6

    .prologue
    .line 94
    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 95
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 97
    :cond_1
    sget-object v3, Lcom/google/ab/b/a/f/b;->a:Lcom/google/ab/b/a/f/b;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Lcom/google/ab/b/a/f/i;->a(Lcom/google/ab/b/a/f/q;Ljava/security/Key;Lcom/google/ab/b/a/f/c;Lcom/google/ab/b/a/f/b;[BZ)Lcom/google/ab/b/a/f/o;

    move-result-object v0

    return-object v0
.end method
