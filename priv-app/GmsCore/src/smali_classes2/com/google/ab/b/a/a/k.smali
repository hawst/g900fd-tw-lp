.class public final Lcom/google/ab/b/a/a/k;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:I

.field private c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3339
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 3344
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/ab/b/a/a/k;->b:I

    .line 3376
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ab/b/a/a/k;->c:I

    .line 3339
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 3379
    iget v0, p0, Lcom/google/ab/b/a/a/k;->c:I

    if-gez v0, :cond_0

    .line 3381
    invoke-virtual {p0}, Lcom/google/ab/b/a/a/k;->b()I

    .line 3383
    :cond_0
    iget v0, p0, Lcom/google/ab/b/a/a/k;->c:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 3336
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->g()I

    move-result v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/ab/b/a/a/k;->a:Z

    iput v0, p0, Lcom/google/ab/b/a/a/k;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 3371
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/k;->a:Z

    if-eqz v0, :cond_0

    .line 3372
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/ab/b/a/a/k;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->c(II)V

    .line 3374
    :cond_0
    return-void
.end method

.method public final b()I
    .locals 2

    .prologue
    .line 3388
    const/4 v0, 0x0

    .line 3389
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/k;->a:Z

    if-eqz v1, :cond_0

    .line 3390
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/ab/b/a/a/k;->b:I

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->f(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 3393
    :cond_0
    iput v0, p0, Lcom/google/ab/b/a/a/k;->c:I

    .line 3394
    return v0
.end method
