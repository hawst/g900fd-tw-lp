.class public final Lcom/google/ab/b/a/e/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljavax/crypto/SecretKey;[B)Lcom/google/ab/b/a/e/c;
    .locals 3

    .prologue
    .line 180
    :try_start_0
    invoke-static {p1, p0}, Lcom/google/ab/b/a/e/a;->a([BLjavax/crypto/SecretKey;)Lcom/google/ab/b/a/e/t;

    move-result-object v0

    .line 181
    sget-object v1, Lcom/google/ab/b/a/e/u;->k:Lcom/google/ab/b/a/e/u;

    iget-object v2, v0, Lcom/google/ab/b/a/e/t;->a:Lcom/google/ab/b/a/e/u;

    invoke-virtual {v1, v2}, Lcom/google/ab/b/a/e/u;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 183
    new-instance v0, Ljava/security/SignatureException;

    const-string v1, "wrong message type in responder hello"

    invoke-direct {v0, v1}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_2

    .line 186
    :catch_0
    move-exception v0

    .line 187
    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 185
    :cond_0
    :try_start_1
    iget-object v0, v0, Lcom/google/ab/b/a/e/t;->b:[B

    invoke-static {v0}, Lcom/google/ab/b/a/e/c;->a([B)Lcom/google/ab/b/a/e/c;
    :try_end_1
    .catch Lcom/google/protobuf/a/e; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    return-object v0

    .line 188
    :catch_1
    move-exception v0

    .line 189
    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 190
    :catch_2
    move-exception v0

    .line 191
    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static a([BLjavax/crypto/SecretKey;)Lcom/google/ab/b/a/e/t;
    .locals 4

    .prologue
    .line 124
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 125
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 128
    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/google/ab/b/a/f/q;->a([B)Lcom/google/ab/b/a/f/q;

    move-result-object v0

    .line 129
    sget-object v1, Lcom/google/ab/b/a/f/c;->a:Lcom/google/ab/b/a/f/c;

    sget-object v2, Lcom/google/ab/b/a/f/b;->b:Lcom/google/ab/b/a/f/b;

    invoke-static {v0, p1, v1, p1, v2}, Lcom/google/ab/b/a/f/i;->a(Lcom/google/ab/b/a/f/q;Ljava/security/Key;Lcom/google/ab/b/a/f/c;Ljava/security/Key;Lcom/google/ab/b/a/f/b;)Lcom/google/ab/b/a/f/o;

    move-result-object v0

    .line 135
    iget-object v1, v0, Lcom/google/ab/b/a/f/o;->b:Lcom/google/ab/b/a/f/n;

    iget-boolean v1, v1, Lcom/google/ab/b/a/f/n;->k:Z

    if-nez v1, :cond_2

    .line 136
    new-instance v0, Ljava/security/SignatureException;

    const-string v1, "missing metadata"

    invoke-direct {v0, v1}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 144
    :catch_0
    move-exception v0

    .line 145
    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 138
    :cond_2
    :try_start_1
    iget-object v1, v0, Lcom/google/ab/b/a/f/o;->b:Lcom/google/ab/b/a/f/n;

    iget-object v1, v1, Lcom/google/ab/b/a/f/n;->l:Lcom/google/protobuf/a/a;

    invoke-virtual {v1}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/ab/b/a/e/o;->a([B)Lcom/google/ab/b/a/e/o;

    move-result-object v1

    .line 140
    iget v2, v1, Lcom/google/ab/b/a/e/o;->b:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_3

    .line 141
    new-instance v0, Ljava/security/SignatureException;

    const-string v1, "Unsupported protocol version"

    invoke-direct {v0, v1}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/google/protobuf/a/e; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 146
    :catch_1
    move-exception v0

    .line 147
    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 143
    :cond_3
    :try_start_2
    new-instance v2, Lcom/google/ab/b/a/e/t;

    iget v1, v1, Lcom/google/ab/b/a/e/o;->a:I

    invoke-static {v1}, Lcom/google/ab/b/a/e/u;->a(I)Lcom/google/ab/b/a/e/u;

    move-result-object v1

    iget-object v0, v0, Lcom/google/ab/b/a/f/o;->d:Lcom/google/protobuf/a/a;

    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v0

    invoke-direct {v2, v1, v0}, Lcom/google/ab/b/a/e/t;-><init>(Lcom/google/ab/b/a/e/u;[B)V
    :try_end_2
    .catch Lcom/google/protobuf/a/e; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    return-object v2
.end method

.method public static a(Ljava/security/PrivateKey;[B)Ljavax/crypto/SecretKey;
    .locals 3

    .prologue
    .line 159
    if-nez p1, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_2

    .line 161
    :catch_0
    move-exception v0

    .line 162
    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 159
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/google/ab/b/a/f/q;->a([B)Lcom/google/ab/b/a/f/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/ab/b/a/f/i;->a(Lcom/google/ab/b/a/f/q;)Lcom/google/ab/b/a/f/n;

    move-result-object v0

    iget-boolean v1, v0, Lcom/google/ab/b/a/f/n;->g:Z

    if-nez v1, :cond_1

    new-instance v0, Lcom/google/protobuf/a/e;

    const-string v1, "Missing decryption key id"

    invoke-direct {v0, v1}, Lcom/google/protobuf/a/e;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/google/protobuf/a/e; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_2

    .line 163
    :catch_1
    move-exception v0

    .line 164
    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 159
    :cond_1
    :try_start_2
    iget-object v0, v0, Lcom/google/ab/b/a/f/n;->h:Lcom/google/protobuf/a/a;

    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v0

    new-instance v1, Lcom/google/ab/b/a/e/e;

    invoke-direct {v1}, Lcom/google/ab/b/a/e/e;-><init>()V

    array-length v2, v0

    invoke-virtual {v1, v0, v2}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v0

    check-cast v0, Lcom/google/ab/b/a/e/e;

    iget-boolean v1, v0, Lcom/google/ab/b/a/e/e;->a:Z

    if-nez v1, :cond_2

    new-instance v0, Lcom/google/protobuf/a/e;

    const-string v1, "Missing public key in responder hello"

    invoke-direct {v0, v1}, Lcom/google/protobuf/a/e;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/google/protobuf/a/e; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_2 .. :try_end_2} :catch_2

    .line 165
    :catch_2
    move-exception v0

    .line 166
    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 159
    :cond_2
    :try_start_3
    iget-object v0, v0, Lcom/google/ab/b/a/e/e;->b:Lcom/google/ab/b/a/f/m;

    invoke-static {v0}, Lcom/google/ab/b/a/f/d;->a(Lcom/google/ab/b/a/f/m;)Ljava/security/PublicKey;

    move-result-object v0

    .line 160
    invoke-static {p0, v0}, Lcom/google/ab/b/a/e/i;->a(Ljava/security/PrivateKey;Ljava/security/PublicKey;)Ljavax/crypto/SecretKey;
    :try_end_3
    .catch Lcom/google/protobuf/a/e; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/security/InvalidKeyException; {:try_start_3 .. :try_end_3} :catch_2

    move-result-object v0

    return-object v0
.end method

.method static a(Lcom/google/ab/b/a/e/t;Ljavax/crypto/SecretKey;[B)[B
    .locals 6

    .prologue
    .line 71
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 72
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 75
    :cond_1
    new-instance v0, Lcom/google/ab/b/a/f/h;

    invoke-direct {v0}, Lcom/google/ab/b/a/f/h;-><init>()V

    new-instance v1, Lcom/google/ab/b/a/e/o;

    invoke-direct {v1}, Lcom/google/ab/b/a/e/o;-><init>()V

    iget-object v2, p0, Lcom/google/ab/b/a/e/t;->a:Lcom/google/ab/b/a/e/u;

    invoke-virtual {v2}, Lcom/google/ab/b/a/e/u;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/ab/b/a/e/o;->a(I)Lcom/google/ab/b/a/e/o;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/ab/b/a/e/o;->b(I)Lcom/google/ab/b/a/e/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/ab/b/a/e/o;->g()[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/ab/b/a/f/h;->a([B)Lcom/google/ab/b/a/f/h;

    move-result-object v0

    .line 81
    if-eqz p2, :cond_2

    .line 82
    invoke-static {p2}, Lcom/google/protobuf/a/a;->a([B)Lcom/google/protobuf/a/a;

    move-result-object v1

    iput-object v1, v0, Lcom/google/ab/b/a/f/h;->a:Lcom/google/protobuf/a/a;

    .line 85
    :cond_2
    sget-object v2, Lcom/google/ab/b/a/f/c;->a:Lcom/google/ab/b/a/f/c;

    sget-object v4, Lcom/google/ab/b/a/f/b;->b:Lcom/google/ab/b/a/f/b;

    iget-object v5, p0, Lcom/google/ab/b/a/e/t;->b:[B

    move-object v1, p1

    move-object v3, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/ab/b/a/f/h;->a(Ljava/security/Key;Lcom/google/ab/b/a/f/c;Ljava/security/Key;Lcom/google/ab/b/a/f/b;[B)Lcom/google/ab/b/a/f/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ab/b/a/f/q;->g()[B

    move-result-object v0

    return-object v0
.end method

.method static b(Ljava/security/PrivateKey;[B)Ljavax/crypto/SecretKey;
    .locals 2

    .prologue
    .line 203
    :try_start_0
    new-instance v0, Lcom/google/ab/b/a/e/d;

    invoke-direct {v0}, Lcom/google/ab/b/a/e/d;-><init>()V

    array-length v1, p1

    invoke-virtual {v0, p1, v1}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v0

    check-cast v0, Lcom/google/ab/b/a/e/d;

    .line 204
    iget-boolean v1, v0, Lcom/google/ab/b/a/e/d;->a:Z

    if-nez v1, :cond_0

    .line 205
    new-instance v0, Ljava/security/SignatureException;

    const-string v1, "Missing public key in initiator hello"

    invoke-direct {v0, v1}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_2

    .line 210
    :catch_0
    move-exception v0

    .line 211
    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 207
    :cond_0
    :try_start_1
    iget-object v0, v0, Lcom/google/ab/b/a/e/d;->b:Lcom/google/ab/b/a/f/m;

    invoke-static {v0}, Lcom/google/ab/b/a/f/d;->a(Lcom/google/ab/b/a/f/m;)Ljava/security/PublicKey;

    move-result-object v0

    .line 209
    invoke-static {p0, v0}, Lcom/google/ab/b/a/e/i;->a(Ljava/security/PrivateKey;Ljava/security/PublicKey;)Ljavax/crypto/SecretKey;
    :try_end_1
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/spec/InvalidKeySpecException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/protobuf/a/e; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    return-object v0

    .line 212
    :catch_1
    move-exception v0

    .line 213
    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 214
    :catch_2
    move-exception v0

    .line 215
    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
