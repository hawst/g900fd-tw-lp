.class public final Lcom/google/ab/b/a/f/m;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field a:Z

.field b:I

.field c:Z

.field d:Lcom/google/ab/b/a/f/l;

.field e:Z

.field f:Lcom/google/ab/b/a/f/r;

.field g:Z

.field h:Lcom/google/ab/b/a/f/k;

.field private i:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1040
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 1045
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/ab/b/a/f/m;->b:I

    .line 1062
    iput-object v1, p0, Lcom/google/ab/b/a/f/m;->d:Lcom/google/ab/b/a/f/l;

    .line 1082
    iput-object v1, p0, Lcom/google/ab/b/a/f/m;->f:Lcom/google/ab/b/a/f/r;

    .line 1102
    iput-object v1, p0, Lcom/google/ab/b/a/f/m;->h:Lcom/google/ab/b/a/f/k;

    .line 1159
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ab/b/a/f/m;->i:I

    .line 1040
    return-void
.end method

.method public static a([B)Lcom/google/ab/b/a/f/m;
    .locals 2

    .prologue
    .line 1235
    new-instance v0, Lcom/google/ab/b/a/f/m;

    invoke-direct {v0}, Lcom/google/ab/b/a/f/m;-><init>()V

    array-length v1, p0

    invoke-super {v0, p0, v1}, Lcom/google/protobuf/a/f;->a([BI)Lcom/google/protobuf/a/f;

    move-result-object v0

    check-cast v0, Lcom/google/ab/b/a/f/m;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1162
    iget v0, p0, Lcom/google/ab/b/a/f/m;->i:I

    if-gez v0, :cond_0

    .line 1164
    invoke-virtual {p0}, Lcom/google/ab/b/a/f/m;->b()I

    .line 1166
    :cond_0
    iget v0, p0, Lcom/google/ab/b/a/f/m;->i:I

    return v0
.end method

.method public final a(I)Lcom/google/ab/b/a/f/m;
    .locals 1

    .prologue
    .line 1049
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/f/m;->a:Z

    .line 1050
    iput p1, p0, Lcom/google/ab/b/a/f/m;->b:I

    .line 1051
    return-object p0
.end method

.method public final a(Lcom/google/ab/b/a/f/k;)Lcom/google/ab/b/a/f/m;
    .locals 1

    .prologue
    .line 1106
    if-nez p1, :cond_0

    .line 1107
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1109
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/f/m;->g:Z

    .line 1110
    iput-object p1, p0, Lcom/google/ab/b/a/f/m;->h:Lcom/google/ab/b/a/f/k;

    .line 1111
    return-object p0
.end method

.method public final a(Lcom/google/ab/b/a/f/l;)Lcom/google/ab/b/a/f/m;
    .locals 1

    .prologue
    .line 1066
    if-nez p1, :cond_0

    .line 1067
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1069
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/f/m;->c:Z

    .line 1070
    iput-object p1, p0, Lcom/google/ab/b/a/f/m;->d:Lcom/google/ab/b/a/f/l;

    .line 1071
    return-object p0
.end method

.method public final a(Lcom/google/ab/b/a/f/r;)Lcom/google/ab/b/a/f/m;
    .locals 1

    .prologue
    .line 1086
    if-nez p1, :cond_0

    .line 1087
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1089
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/f/m;->e:Z

    .line 1090
    iput-object p1, p0, Lcom/google/ab/b/a/f/m;->f:Lcom/google/ab/b/a/f/r;

    .line 1091
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 1

    .prologue
    .line 1037
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/f/m;->a(I)Lcom/google/ab/b/a/f/m;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/ab/b/a/f/l;

    invoke-direct {v0}, Lcom/google/ab/b/a/f/l;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/f/m;->a(Lcom/google/ab/b/a/f/l;)Lcom/google/ab/b/a/f/m;

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/ab/b/a/f/r;

    invoke-direct {v0}, Lcom/google/ab/b/a/f/r;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/f/m;->a(Lcom/google/ab/b/a/f/r;)Lcom/google/ab/b/a/f/m;

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/ab/b/a/f/k;

    invoke-direct {v0}, Lcom/google/ab/b/a/f/k;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/f/m;->a(Lcom/google/ab/b/a/f/k;)Lcom/google/ab/b/a/f/m;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 1145
    iget-boolean v0, p0, Lcom/google/ab/b/a/f/m;->a:Z

    if-eqz v0, :cond_0

    .line 1146
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/ab/b/a/f/m;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 1148
    :cond_0
    iget-boolean v0, p0, Lcom/google/ab/b/a/f/m;->c:Z

    if-eqz v0, :cond_1

    .line 1149
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ab/b/a/f/m;->d:Lcom/google/ab/b/a/f/l;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 1151
    :cond_1
    iget-boolean v0, p0, Lcom/google/ab/b/a/f/m;->e:Z

    if-eqz v0, :cond_2

    .line 1152
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ab/b/a/f/m;->f:Lcom/google/ab/b/a/f/r;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 1154
    :cond_2
    iget-boolean v0, p0, Lcom/google/ab/b/a/f/m;->g:Z

    if-eqz v0, :cond_3

    .line 1155
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ab/b/a/f/m;->h:Lcom/google/ab/b/a/f/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 1157
    :cond_3
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 1171
    const/4 v0, 0x0

    .line 1172
    iget-boolean v1, p0, Lcom/google/ab/b/a/f/m;->a:Z

    if-eqz v1, :cond_0

    .line 1173
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/ab/b/a/f/m;->b:I

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1176
    :cond_0
    iget-boolean v1, p0, Lcom/google/ab/b/a/f/m;->c:Z

    if-eqz v1, :cond_1

    .line 1177
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ab/b/a/f/m;->d:Lcom/google/ab/b/a/f/l;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1180
    :cond_1
    iget-boolean v1, p0, Lcom/google/ab/b/a/f/m;->e:Z

    if-eqz v1, :cond_2

    .line 1181
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ab/b/a/f/m;->f:Lcom/google/ab/b/a/f/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1184
    :cond_2
    iget-boolean v1, p0, Lcom/google/ab/b/a/f/m;->g:Z

    if-eqz v1, :cond_3

    .line 1185
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ab/b/a/f/m;->h:Lcom/google/ab/b/a/f/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1188
    :cond_3
    iput v0, p0, Lcom/google/ab/b/a/f/m;->i:I

    .line 1189
    return v0
.end method
