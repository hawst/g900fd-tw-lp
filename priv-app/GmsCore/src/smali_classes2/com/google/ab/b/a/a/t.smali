.class public final Lcom/google/ab/b/a/a/t;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Lcom/google/protobuf/a/a;

.field private c:Z

.field private d:J

.field private e:Z

.field private f:Lcom/google/ab/b/a/a/d;

.field private g:Z

.field private h:Z

.field private i:Z

.field private j:I

.field private k:Z

.field private l:I

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 6725
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 6736
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/ab/b/a/a/t;->b:Lcom/google/protobuf/a/a;

    .line 6753
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/ab/b/a/a/t;->d:J

    .line 6770
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ab/b/a/a/t;->f:Lcom/google/ab/b/a/a/d;

    .line 6790
    iput-boolean v2, p0, Lcom/google/ab/b/a/a/t;->h:Z

    .line 6807
    const/16 v0, 0x28

    iput v0, p0, Lcom/google/ab/b/a/a/t;->j:I

    .line 6824
    iput v2, p0, Lcom/google/ab/b/a/a/t;->l:I

    .line 6876
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ab/b/a/a/t;->m:I

    .line 6725
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 6879
    iget v0, p0, Lcom/google/ab/b/a/a/t;->m:I

    if-gez v0, :cond_0

    .line 6881
    invoke-virtual {p0}, Lcom/google/ab/b/a/a/t;->b()I

    .line 6883
    :cond_0
    iget v0, p0, Lcom/google/ab/b/a/a/t;->m:I

    return v0
.end method

.method public final a(J)Lcom/google/ab/b/a/a/t;
    .locals 1

    .prologue
    .line 6757
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/a/t;->c:Z

    .line 6758
    iput-wide p1, p0, Lcom/google/ab/b/a/a/t;->d:J

    .line 6759
    return-object p0
.end method

.method public final a(Lcom/google/ab/b/a/a/d;)Lcom/google/ab/b/a/a/t;
    .locals 1

    .prologue
    .line 6774
    if-nez p1, :cond_0

    .line 6775
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 6777
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/a/t;->e:Z

    .line 6778
    iput-object p1, p0, Lcom/google/ab/b/a/a/t;->f:Lcom/google/ab/b/a/a/d;

    .line 6779
    return-object p0
.end method

.method public final a(Lcom/google/protobuf/a/a;)Lcom/google/ab/b/a/a/t;
    .locals 1

    .prologue
    .line 6740
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/ab/b/a/a/t;->a:Z

    .line 6741
    iput-object p1, p0, Lcom/google/ab/b/a/a/t;->b:Lcom/google/protobuf/a/a;

    .line 6742
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 6722
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/a/t;->a(Lcom/google/protobuf/a/a;)Lcom/google/ab/b/a/a/t;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/ab/b/a/a/t;->a(J)Lcom/google/ab/b/a/a/t;

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/ab/b/a/a/d;

    invoke-direct {v0}, Lcom/google/ab/b/a/a/d;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/ab/b/a/a/t;->a(Lcom/google/ab/b/a/a/d;)Lcom/google/ab/b/a/a/t;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/t;->g:Z

    iput-boolean v0, p0, Lcom/google/ab/b/a/a/t;->h:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/t;->i:Z

    iput v0, p0, Lcom/google/ab/b/a/a/t;->j:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/ab/b/a/a/t;->k:Z

    iput v0, p0, Lcom/google/ab/b/a/a/t;->l:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 6856
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/t;->a:Z

    if-eqz v0, :cond_0

    .line 6857
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ab/b/a/a/t;->b:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 6859
    :cond_0
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/t;->c:Z

    if-eqz v0, :cond_1

    .line 6860
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/ab/b/a/a/t;->d:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 6862
    :cond_1
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/t;->e:Z

    if-eqz v0, :cond_2

    .line 6863
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ab/b/a/a/t;->f:Lcom/google/ab/b/a/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 6865
    :cond_2
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/t;->g:Z

    if-eqz v0, :cond_3

    .line 6866
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/ab/b/a/a/t;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 6868
    :cond_3
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/t;->i:Z

    if-eqz v0, :cond_4

    .line 6869
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/ab/b/a/a/t;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 6871
    :cond_4
    iget-boolean v0, p0, Lcom/google/ab/b/a/a/t;->k:Z

    if-eqz v0, :cond_5

    .line 6872
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/ab/b/a/a/t;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 6874
    :cond_5
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 6888
    const/4 v0, 0x0

    .line 6889
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/t;->a:Z

    if-eqz v1, :cond_0

    .line 6890
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ab/b/a/a/t;->b:Lcom/google/protobuf/a/a;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 6893
    :cond_0
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/t;->c:Z

    if-eqz v1, :cond_1

    .line 6894
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/ab/b/a/a/t;->d:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6897
    :cond_1
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/t;->e:Z

    if-eqz v1, :cond_2

    .line 6898
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ab/b/a/a/t;->f:Lcom/google/ab/b/a/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6901
    :cond_2
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/t;->g:Z

    if-eqz v1, :cond_3

    .line 6902
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/ab/b/a/a/t;->h:Z

    invoke-static {v1}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 6905
    :cond_3
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/t;->i:Z

    if-eqz v1, :cond_4

    .line 6906
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/ab/b/a/a/t;->j:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6909
    :cond_4
    iget-boolean v1, p0, Lcom/google/ab/b/a/a/t;->k:Z

    if-eqz v1, :cond_5

    .line 6910
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/ab/b/a/a/t;->l:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6913
    :cond_5
    iput v0, p0, Lcom/google/ab/b/a/a/t;->m:I

    .line 6914
    return v0
.end method
