.class public final Lcom/google/ab/b/a/e/s;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a([BLjavax/crypto/SecretKey;)Lcom/google/ab/b/a/e/t;
    .locals 4

    .prologue
    .line 142
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 143
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 146
    :cond_1
    :try_start_0
    invoke-static {p0}, Lcom/google/ab/b/a/f/q;->a([B)Lcom/google/ab/b/a/f/q;

    move-result-object v0

    .line 147
    sget-object v1, Lcom/google/ab/b/a/f/c;->a:Lcom/google/ab/b/a/f/c;

    sget-object v2, Lcom/google/ab/b/a/f/b;->b:Lcom/google/ab/b/a/f/b;

    invoke-static {v0, p1, v1, p1, v2}, Lcom/google/ab/b/a/f/i;->a(Lcom/google/ab/b/a/f/q;Ljava/security/Key;Lcom/google/ab/b/a/f/c;Ljava/security/Key;Lcom/google/ab/b/a/f/b;)Lcom/google/ab/b/a/f/o;

    move-result-object v0

    .line 153
    iget-object v1, v0, Lcom/google/ab/b/a/f/o;->b:Lcom/google/ab/b/a/f/n;

    iget-object v1, v1, Lcom/google/ab/b/a/f/n;->l:Lcom/google/protobuf/a/a;

    invoke-virtual {v1}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/ab/b/a/e/o;->a([B)Lcom/google/ab/b/a/e/o;

    move-result-object v1

    .line 155
    iget v2, v1, Lcom/google/ab/b/a/e/o;->b:I

    const/4 v3, 0x1

    if-le v2, v3, :cond_2

    .line 156
    new-instance v0, Ljava/security/SignatureException;

    const-string v1, "Unsupported protocol version"

    invoke-direct {v0, v1}, Ljava/security/SignatureException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/protobuf/a/e; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 159
    :catch_0
    move-exception v0

    .line 160
    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 158
    :cond_2
    :try_start_1
    new-instance v2, Lcom/google/ab/b/a/e/t;

    iget v1, v1, Lcom/google/ab/b/a/e/o;->a:I

    invoke-static {v1}, Lcom/google/ab/b/a/e/u;->a(I)Lcom/google/ab/b/a/e/u;

    move-result-object v1

    iget-object v0, v0, Lcom/google/ab/b/a/f/o;->d:Lcom/google/protobuf/a/a;

    invoke-virtual {v0}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v0

    invoke-direct {v2, v1, v0}, Lcom/google/ab/b/a/e/t;-><init>(Lcom/google/ab/b/a/e/u;[B)V
    :try_end_1
    .catch Lcom/google/protobuf/a/e; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    return-object v2

    .line 161
    :catch_1
    move-exception v0

    .line 162
    new-instance v1, Ljava/security/SignatureException;

    invoke-direct {v1, v0}, Ljava/security/SignatureException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static a(Lcom/google/ab/b/a/e/t;Ljava/security/KeyPair;Ljavax/crypto/SecretKey;)[B
    .locals 6

    .prologue
    .line 176
    if-eqz p0, :cond_0

    if-nez p2, :cond_1

    .line 177
    :cond_0
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 180
    :cond_1
    invoke-virtual {p1}, Ljava/security/KeyPair;->getPublic()Ljava/security/PublicKey;

    move-result-object v2

    .line 181
    invoke-virtual {p1}, Ljava/security/KeyPair;->getPrivate()Ljava/security/PrivateKey;

    move-result-object v1

    .line 183
    new-instance v0, Lcom/google/ab/b/a/f/h;

    invoke-direct {v0}, Lcom/google/ab/b/a/f/h;-><init>()V

    invoke-static {v2}, Lcom/google/ab/b/a/e/j;->a(Ljava/security/PublicKey;)[B

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/ab/b/a/f/h;->b([B)Lcom/google/ab/b/a/f/h;

    move-result-object v0

    new-instance v3, Lcom/google/ab/b/a/e/o;

    invoke-direct {v3}, Lcom/google/ab/b/a/e/o;-><init>()V

    iget-object v4, p0, Lcom/google/ab/b/a/e/t;->a:Lcom/google/ab/b/a/e/u;

    invoke-virtual {v4}, Lcom/google/ab/b/a/e/u;->a()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/google/ab/b/a/e/o;->a(I)Lcom/google/ab/b/a/e/o;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Lcom/google/ab/b/a/e/o;->b(I)Lcom/google/ab/b/a/e/o;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/ab/b/a/e/o;->g()[B

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/ab/b/a/f/h;->a([B)Lcom/google/ab/b/a/f/h;

    move-result-object v0

    instance-of v3, v2, Ljava/security/interfaces/ECPublicKey;

    if-eqz v3, :cond_2

    sget-object v2, Lcom/google/ab/b/a/f/c;->b:Lcom/google/ab/b/a/f/c;

    :goto_0
    sget-object v4, Lcom/google/ab/b/a/f/b;->b:Lcom/google/ab/b/a/f/b;

    iget-object v5, p0, Lcom/google/ab/b/a/e/t;->b:[B

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/ab/b/a/f/h;->a(Ljava/security/Key;Lcom/google/ab/b/a/f/c;Ljava/security/Key;Lcom/google/ab/b/a/f/b;[B)Lcom/google/ab/b/a/f/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/ab/b/a/f/q;->g()[B

    move-result-object v0

    return-object v0

    :cond_2
    instance-of v2, v2, Ljava/security/interfaces/RSAPublicKey;

    if-eqz v2, :cond_3

    sget-object v2, Lcom/google/ab/b/a/f/c;->c:Lcom/google/ab/b/a/f/c;

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/security/InvalidKeyException;

    const-string v1, "Unsupported key type"

    invoke-direct {v0, v1}, Ljava/security/InvalidKeyException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
