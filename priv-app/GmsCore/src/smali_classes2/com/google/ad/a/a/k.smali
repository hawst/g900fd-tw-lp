.class public final Lcom/google/ad/a/a/k;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile i:[Lcom/google/ad/a/a/k;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Z

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:[J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2081
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2082
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/ad/a/a/k;->c:Z

    iput-boolean v1, p0, Lcom/google/ad/a/a/k;->d:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/k;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/k;->f:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/ad/a/a/k;->g:Z

    sget-object v0, Lcom/google/protobuf/nano/m;->b:[J

    iput-object v0, p0, Lcom/google/ad/a/a/k;->h:[J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ad/a/a/k;->cachedSize:I

    .line 2083
    return-void
.end method

.method public static a()[Lcom/google/ad/a/a/k;
    .locals 2

    .prologue
    .line 2046
    sget-object v0, Lcom/google/ad/a/a/k;->i:[Lcom/google/ad/a/a/k;

    if-nez v0, :cond_1

    .line 2047
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2049
    :try_start_0
    sget-object v0, Lcom/google/ad/a/a/k;->i:[Lcom/google/ad/a/a/k;

    if-nez v0, :cond_0

    .line 2050
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ad/a/a/k;

    sput-object v0, Lcom/google/ad/a/a/k;->i:[Lcom/google/ad/a/a/k;

    .line 2052
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2054
    :cond_1
    sget-object v0, Lcom/google/ad/a/a/k;->i:[Lcom/google/ad/a/a/k;

    return-object v0

    .line 2052
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2204
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2205
    iget-object v2, p0, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2206
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2209
    :cond_0
    iget-object v2, p0, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2210
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2213
    :cond_1
    iget-boolean v2, p0, Lcom/google/ad/a/a/k;->c:Z

    if-eqz v2, :cond_2

    .line 2214
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/ad/a/a/k;->c:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2217
    :cond_2
    iget-boolean v2, p0, Lcom/google/ad/a/a/k;->d:Z

    if-eqz v2, :cond_3

    .line 2218
    const/4 v2, 0x4

    iget-boolean v3, p0, Lcom/google/ad/a/a/k;->d:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2221
    :cond_3
    iget-object v2, p0, Lcom/google/ad/a/a/k;->e:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 2222
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/ad/a/a/k;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2225
    :cond_4
    iget-object v2, p0, Lcom/google/ad/a/a/k;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 2226
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/ad/a/a/k;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2229
    :cond_5
    iget-boolean v2, p0, Lcom/google/ad/a/a/k;->g:Z

    if-eqz v2, :cond_6

    .line 2230
    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/google/ad/a/a/k;->g:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 2233
    :cond_6
    iget-object v2, p0, Lcom/google/ad/a/a/k;->h:[J

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/ad/a/a/k;->h:[J

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v1

    .line 2235
    :goto_0
    iget-object v3, p0, Lcom/google/ad/a/a/k;->h:[J

    array-length v3, v3

    if-ge v1, v3, :cond_7

    .line 2236
    iget-object v3, p0, Lcom/google/ad/a/a/k;->h:[J

    aget-wide v4, v3, v1

    .line 2237
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/b;->a(J)I

    move-result v3

    add-int/2addr v2, v3

    .line 2235
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2240
    :cond_7
    add-int/2addr v0, v2

    .line 2241
    iget-object v1, p0, Lcom/google/ad/a/a/k;->h:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2243
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2100
    if-ne p1, p0, :cond_1

    .line 2148
    :cond_0
    :goto_0
    return v0

    .line 2103
    :cond_1
    instance-of v2, p1, Lcom/google/ad/a/a/k;

    if-nez v2, :cond_2

    move v0, v1

    .line 2104
    goto :goto_0

    .line 2106
    :cond_2
    check-cast p1, Lcom/google/ad/a/a/k;

    .line 2107
    iget-object v2, p0, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 2108
    iget-object v2, p1, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2109
    goto :goto_0

    .line 2111
    :cond_3
    iget-object v2, p0, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2112
    goto :goto_0

    .line 2114
    :cond_4
    iget-object v2, p0, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 2115
    iget-object v2, p1, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 2116
    goto :goto_0

    .line 2118
    :cond_5
    iget-object v2, p0, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 2119
    goto :goto_0

    .line 2121
    :cond_6
    iget-boolean v2, p0, Lcom/google/ad/a/a/k;->c:Z

    iget-boolean v3, p1, Lcom/google/ad/a/a/k;->c:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 2122
    goto :goto_0

    .line 2124
    :cond_7
    iget-boolean v2, p0, Lcom/google/ad/a/a/k;->d:Z

    iget-boolean v3, p1, Lcom/google/ad/a/a/k;->d:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 2125
    goto :goto_0

    .line 2127
    :cond_8
    iget-object v2, p0, Lcom/google/ad/a/a/k;->e:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 2128
    iget-object v2, p1, Lcom/google/ad/a/a/k;->e:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 2129
    goto :goto_0

    .line 2131
    :cond_9
    iget-object v2, p0, Lcom/google/ad/a/a/k;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/k;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 2132
    goto :goto_0

    .line 2134
    :cond_a
    iget-object v2, p0, Lcom/google/ad/a/a/k;->f:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 2135
    iget-object v2, p1, Lcom/google/ad/a/a/k;->f:Ljava/lang/String;

    if-eqz v2, :cond_c

    move v0, v1

    .line 2136
    goto :goto_0

    .line 2138
    :cond_b
    iget-object v2, p0, Lcom/google/ad/a/a/k;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/k;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 2139
    goto :goto_0

    .line 2141
    :cond_c
    iget-boolean v2, p0, Lcom/google/ad/a/a/k;->g:Z

    iget-boolean v3, p1, Lcom/google/ad/a/a/k;->g:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 2142
    goto :goto_0

    .line 2144
    :cond_d
    iget-object v2, p0, Lcom/google/ad/a/a/k;->h:[J

    iget-object v3, p1, Lcom/google/ad/a/a/k;->h:[J

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([J[J)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2146
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 2153
    iget-object v0, p0, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2156
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 2158
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/ad/a/a/k;->c:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    add-int/2addr v0, v4

    .line 2159
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/ad/a/a/k;->d:Z

    if-eqz v0, :cond_3

    move v0, v2

    :goto_3
    add-int/2addr v0, v4

    .line 2160
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/k;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v4

    .line 2162
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/ad/a/a/k;->f:Ljava/lang/String;

    if-nez v4, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 2164
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/ad/a/a/k;->g:Z

    if-eqz v1, :cond_6

    :goto_6
    add-int/2addr v0, v2

    .line 2165
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ad/a/a/k;->h:[J

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([J)I

    move-result v1

    add-int/2addr v0, v1

    .line 2167
    return v0

    .line 2153
    :cond_0
    iget-object v0, p0, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2156
    :cond_1
    iget-object v0, p0, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    move v0, v3

    .line 2158
    goto :goto_2

    :cond_3
    move v0, v3

    .line 2159
    goto :goto_3

    .line 2160
    :cond_4
    iget-object v0, p0, Lcom/google/ad/a/a/k;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 2162
    :cond_5
    iget-object v1, p0, Lcom/google/ad/a/a/k;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    :cond_6
    move v2, v3

    .line 2164
    goto :goto_6
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2040
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/ad/a/a/k;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/ad/a/a/k;->d:Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/k;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/k;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/ad/a/a/k;->g:Z

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x48

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ad/a/a/k;->h:[J

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [J

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ad/a/a/k;->h:[J

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ad/a/a/k;->h:[J

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lcom/google/ad/a/a/k;->h:[J

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/ad/a/a/k;->h:[J

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [J

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/ad/a/a/k;->h:[J

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v4

    aput-wide v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/google/ad/a/a/k;->h:[J

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lcom/google/ad/a/a/k;->h:[J

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 2173
    iget-object v0, p0, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2174
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ad/a/a/k;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2176
    :cond_0
    iget-object v0, p0, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2177
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ad/a/a/k;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2179
    :cond_1
    iget-boolean v0, p0, Lcom/google/ad/a/a/k;->c:Z

    if-eqz v0, :cond_2

    .line 2180
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/ad/a/a/k;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2182
    :cond_2
    iget-boolean v0, p0, Lcom/google/ad/a/a/k;->d:Z

    if-eqz v0, :cond_3

    .line 2183
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/ad/a/a/k;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2185
    :cond_3
    iget-object v0, p0, Lcom/google/ad/a/a/k;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2186
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ad/a/a/k;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2188
    :cond_4
    iget-object v0, p0, Lcom/google/ad/a/a/k;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 2189
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ad/a/a/k;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2191
    :cond_5
    iget-boolean v0, p0, Lcom/google/ad/a/a/k;->g:Z

    if-eqz v0, :cond_6

    .line 2192
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/ad/a/a/k;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2194
    :cond_6
    iget-object v0, p0, Lcom/google/ad/a/a/k;->h:[J

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/ad/a/a/k;->h:[J

    array-length v0, v0

    if-lez v0, :cond_7

    .line 2195
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ad/a/a/k;->h:[J

    array-length v1, v1

    if-ge v0, v1, :cond_7

    .line 2196
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/ad/a/a/k;->h:[J

    aget-wide v2, v2, v0

    invoke-virtual {p1, v1, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2195
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2199
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2200
    return-void
.end method
