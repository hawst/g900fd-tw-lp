.class public final Lcom/google/ad/a/a/r;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:[Ljava/lang/String;

.field public e:[Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 993
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 994
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ad/a/a/r;->a:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/r;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/r;->c:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/r;->f:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/r;->g:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/r;->h:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/r;->i:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ad/a/a/r;->cachedSize:I

    .line 995
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1144
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1145
    iget-boolean v1, p0, Lcom/google/ad/a/a/r;->a:Z

    if-eqz v1, :cond_0

    .line 1146
    const/4 v1, 0x1

    iget-boolean v3, p0, Lcom/google/ad/a/a/r;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1149
    :cond_0
    iget-object v1, p0, Lcom/google/ad/a/a/r;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1150
    const/4 v1, 0x2

    iget-object v3, p0, Lcom/google/ad/a/a/r;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1153
    :cond_1
    iget-object v1, p0, Lcom/google/ad/a/a/r;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1154
    const/4 v1, 0x3

    iget-object v3, p0, Lcom/google/ad/a/a/r;->c:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1157
    :cond_2
    iget-object v1, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_5

    move v1, v2

    move v3, v2

    move v4, v2

    .line 1160
    :goto_0
    iget-object v5, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_4

    .line 1161
    iget-object v5, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 1162
    if-eqz v5, :cond_3

    .line 1163
    add-int/lit8 v4, v4, 0x1

    .line 1164
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1160
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1168
    :cond_4
    add-int/2addr v0, v3

    .line 1169
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 1171
    :cond_5
    iget-object v1, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_8

    move v1, v2

    move v3, v2

    .line 1174
    :goto_1
    iget-object v4, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_7

    .line 1175
    iget-object v4, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    aget-object v4, v4, v2

    .line 1176
    if-eqz v4, :cond_6

    .line 1177
    add-int/lit8 v3, v3, 0x1

    .line 1178
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 1174
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1182
    :cond_7
    add-int/2addr v0, v1

    .line 1183
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 1185
    :cond_8
    iget-object v1, p0, Lcom/google/ad/a/a/r;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 1186
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ad/a/a/r;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1189
    :cond_9
    iget-object v1, p0, Lcom/google/ad/a/a/r;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 1190
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ad/a/a/r;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1193
    :cond_a
    iget-object v1, p0, Lcom/google/ad/a/a/r;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1194
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/ad/a/a/r;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1197
    :cond_b
    iget-object v1, p0, Lcom/google/ad/a/a/r;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 1198
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/ad/a/a/r;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1201
    :cond_c
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1013
    if-ne p1, p0, :cond_1

    .line 1073
    :cond_0
    :goto_0
    return v0

    .line 1016
    :cond_1
    instance-of v2, p1, Lcom/google/ad/a/a/r;

    if-nez v2, :cond_2

    move v0, v1

    .line 1017
    goto :goto_0

    .line 1019
    :cond_2
    check-cast p1, Lcom/google/ad/a/a/r;

    .line 1020
    iget-boolean v2, p0, Lcom/google/ad/a/a/r;->a:Z

    iget-boolean v3, p1, Lcom/google/ad/a/a/r;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1021
    goto :goto_0

    .line 1023
    :cond_3
    iget-object v2, p0, Lcom/google/ad/a/a/r;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1024
    iget-object v2, p1, Lcom/google/ad/a/a/r;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 1025
    goto :goto_0

    .line 1027
    :cond_4
    iget-object v2, p0, Lcom/google/ad/a/a/r;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/r;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1028
    goto :goto_0

    .line 1030
    :cond_5
    iget-object v2, p0, Lcom/google/ad/a/a/r;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 1031
    iget-object v2, p1, Lcom/google/ad/a/a/r;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 1032
    goto :goto_0

    .line 1034
    :cond_6
    iget-object v2, p0, Lcom/google/ad/a/a/r;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/r;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 1035
    goto :goto_0

    .line 1037
    :cond_7
    iget-object v2, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1039
    goto :goto_0

    .line 1041
    :cond_8
    iget-object v2, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 1043
    goto :goto_0

    .line 1045
    :cond_9
    iget-object v2, p0, Lcom/google/ad/a/a/r;->f:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 1046
    iget-object v2, p1, Lcom/google/ad/a/a/r;->f:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 1047
    goto :goto_0

    .line 1049
    :cond_a
    iget-object v2, p0, Lcom/google/ad/a/a/r;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/r;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1050
    goto :goto_0

    .line 1052
    :cond_b
    iget-object v2, p0, Lcom/google/ad/a/a/r;->g:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 1053
    iget-object v2, p1, Lcom/google/ad/a/a/r;->g:Ljava/lang/String;

    if-eqz v2, :cond_d

    move v0, v1

    .line 1054
    goto :goto_0

    .line 1056
    :cond_c
    iget-object v2, p0, Lcom/google/ad/a/a/r;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/r;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 1057
    goto :goto_0

    .line 1059
    :cond_d
    iget-object v2, p0, Lcom/google/ad/a/a/r;->h:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 1060
    iget-object v2, p1, Lcom/google/ad/a/a/r;->h:Ljava/lang/String;

    if-eqz v2, :cond_f

    move v0, v1

    .line 1061
    goto/16 :goto_0

    .line 1063
    :cond_e
    iget-object v2, p0, Lcom/google/ad/a/a/r;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/r;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 1064
    goto/16 :goto_0

    .line 1066
    :cond_f
    iget-object v2, p0, Lcom/google/ad/a/a/r;->i:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 1067
    iget-object v2, p1, Lcom/google/ad/a/a/r;->i:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1068
    goto/16 :goto_0

    .line 1070
    :cond_10
    iget-object v2, p0, Lcom/google/ad/a/a/r;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/r;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1071
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1078
    iget-boolean v0, p0, Lcom/google/ad/a/a/r;->a:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1080
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/r;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1082
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/r;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1084
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1086
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1088
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/r;->f:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 1090
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/r;->g:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 1092
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/r;->h:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 1094
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ad/a/a/r;->i:Ljava/lang/String;

    if-nez v2, :cond_6

    :goto_6
    add-int/2addr v0, v1

    .line 1096
    return v0

    .line 1078
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 1080
    :cond_1
    iget-object v0, p0, Lcom/google/ad/a/a/r;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1082
    :cond_2
    iget-object v0, p0, Lcom/google/ad/a/a/r;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1088
    :cond_3
    iget-object v0, p0, Lcom/google/ad/a/a/r;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 1090
    :cond_4
    iget-object v0, p0, Lcom/google/ad/a/a/r;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 1092
    :cond_5
    iget-object v0, p0, Lcom/google/ad/a/a/r;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 1094
    :cond_6
    iget-object v1, p0, Lcom/google/ad/a/a/r;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_6
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 949
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/ad/a/a/r;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/r;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/r;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/r;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/r;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/r;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/r;->i:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1102
    iget-boolean v0, p0, Lcom/google/ad/a/a/r;->a:Z

    if-eqz v0, :cond_0

    .line 1103
    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/google/ad/a/a/r;->a:Z

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1105
    :cond_0
    iget-object v0, p0, Lcom/google/ad/a/a/r;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1106
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/ad/a/a/r;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1108
    :cond_1
    iget-object v0, p0, Lcom/google/ad/a/a/r;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1109
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/ad/a/a/r;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1111
    :cond_2
    iget-object v0, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 1112
    :goto_0
    iget-object v2, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 1113
    iget-object v2, p0, Lcom/google/ad/a/a/r;->d:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 1114
    if-eqz v2, :cond_3

    .line 1115
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1112
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1119
    :cond_4
    iget-object v0, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 1120
    :goto_1
    iget-object v0, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 1121
    iget-object v0, p0, Lcom/google/ad/a/a/r;->e:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 1122
    if-eqz v0, :cond_5

    .line 1123
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1120
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1127
    :cond_6
    iget-object v0, p0, Lcom/google/ad/a/a/r;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 1128
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ad/a/a/r;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1130
    :cond_7
    iget-object v0, p0, Lcom/google/ad/a/a/r;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 1131
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ad/a/a/r;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1133
    :cond_8
    iget-object v0, p0, Lcom/google/ad/a/a/r;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 1134
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/ad/a/a/r;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1136
    :cond_9
    iget-object v0, p0, Lcom/google/ad/a/a/r;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 1137
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/ad/a/a/r;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1139
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1140
    return-void
.end method
