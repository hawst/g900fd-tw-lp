.class public final Lcom/google/ad/a/a/x;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:[Ljava/lang/String;

.field public f:[Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 287
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 288
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ad/a/a/x;->a:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/x;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/x;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/x;->d:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/x;->g:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ad/a/a/x;->cachedSize:I

    .line 289
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 412
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 413
    iget-boolean v1, p0, Lcom/google/ad/a/a/x;->a:Z

    if-eqz v1, :cond_0

    .line 414
    const/4 v1, 0x1

    iget-boolean v3, p0, Lcom/google/ad/a/a/x;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 417
    :cond_0
    iget-object v1, p0, Lcom/google/ad/a/a/x;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 418
    const/4 v1, 0x2

    iget-object v3, p0, Lcom/google/ad/a/a/x;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 421
    :cond_1
    iget-object v1, p0, Lcom/google/ad/a/a/x;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 422
    const/4 v1, 0x3

    iget-object v3, p0, Lcom/google/ad/a/a/x;->c:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 425
    :cond_2
    iget-object v1, p0, Lcom/google/ad/a/a/x;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 426
    const/4 v1, 0x4

    iget-object v3, p0, Lcom/google/ad/a/a/x;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 429
    :cond_3
    iget-object v1, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_6

    move v1, v2

    move v3, v2

    move v4, v2

    .line 432
    :goto_0
    iget-object v5, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_5

    .line 433
    iget-object v5, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 434
    if-eqz v5, :cond_4

    .line 435
    add-int/lit8 v4, v4, 0x1

    .line 436
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 432
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 440
    :cond_5
    add-int/2addr v0, v3

    .line 441
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 443
    :cond_6
    iget-object v1, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_9

    move v1, v2

    move v3, v2

    .line 446
    :goto_1
    iget-object v4, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_8

    .line 447
    iget-object v4, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    aget-object v4, v4, v2

    .line 448
    if-eqz v4, :cond_7

    .line 449
    add-int/lit8 v3, v3, 0x1

    .line 450
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 446
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 454
    :cond_8
    add-int/2addr v0, v1

    .line 455
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 457
    :cond_9
    iget-object v1, p0, Lcom/google/ad/a/a/x;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 458
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ad/a/a/x;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 461
    :cond_a
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 305
    if-ne p1, p0, :cond_1

    .line 351
    :cond_0
    :goto_0
    return v0

    .line 308
    :cond_1
    instance-of v2, p1, Lcom/google/ad/a/a/x;

    if-nez v2, :cond_2

    move v0, v1

    .line 309
    goto :goto_0

    .line 311
    :cond_2
    check-cast p1, Lcom/google/ad/a/a/x;

    .line 312
    iget-boolean v2, p0, Lcom/google/ad/a/a/x;->a:Z

    iget-boolean v3, p1, Lcom/google/ad/a/a/x;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 313
    goto :goto_0

    .line 315
    :cond_3
    iget-object v2, p0, Lcom/google/ad/a/a/x;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 316
    iget-object v2, p1, Lcom/google/ad/a/a/x;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 317
    goto :goto_0

    .line 319
    :cond_4
    iget-object v2, p0, Lcom/google/ad/a/a/x;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/x;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 320
    goto :goto_0

    .line 322
    :cond_5
    iget-object v2, p0, Lcom/google/ad/a/a/x;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 323
    iget-object v2, p1, Lcom/google/ad/a/a/x;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 324
    goto :goto_0

    .line 326
    :cond_6
    iget-object v2, p0, Lcom/google/ad/a/a/x;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/x;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 327
    goto :goto_0

    .line 329
    :cond_7
    iget-object v2, p0, Lcom/google/ad/a/a/x;->d:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 330
    iget-object v2, p1, Lcom/google/ad/a/a/x;->d:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 331
    goto :goto_0

    .line 333
    :cond_8
    iget-object v2, p0, Lcom/google/ad/a/a/x;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/x;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 334
    goto :goto_0

    .line 336
    :cond_9
    iget-object v2, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 338
    goto :goto_0

    .line 340
    :cond_a
    iget-object v2, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 342
    goto :goto_0

    .line 344
    :cond_b
    iget-object v2, p0, Lcom/google/ad/a/a/x;->g:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 345
    iget-object v2, p1, Lcom/google/ad/a/a/x;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 346
    goto :goto_0

    .line 348
    :cond_c
    iget-object v2, p0, Lcom/google/ad/a/a/x;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/x;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 349
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 356
    iget-boolean v0, p0, Lcom/google/ad/a/a/x;->a:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 358
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/x;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 360
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/x;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 362
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/x;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 364
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 366
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 368
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ad/a/a/x;->g:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 370
    return v0

    .line 356
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 358
    :cond_1
    iget-object v0, p0, Lcom/google/ad/a/a/x;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 360
    :cond_2
    iget-object v0, p0, Lcom/google/ad/a/a/x;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 362
    :cond_3
    iget-object v0, p0, Lcom/google/ad/a/a/x;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 368
    :cond_4
    iget-object v1, p0, Lcom/google/ad/a/a/x;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 249
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/ad/a/a/x;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/x;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/x;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/x;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/x;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 376
    iget-boolean v0, p0, Lcom/google/ad/a/a/x;->a:Z

    if-eqz v0, :cond_0

    .line 377
    const/4 v0, 0x1

    iget-boolean v2, p0, Lcom/google/ad/a/a/x;->a:Z

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 379
    :cond_0
    iget-object v0, p0, Lcom/google/ad/a/a/x;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 380
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/ad/a/a/x;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 382
    :cond_1
    iget-object v0, p0, Lcom/google/ad/a/a/x;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 383
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/ad/a/a/x;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 385
    :cond_2
    iget-object v0, p0, Lcom/google/ad/a/a/x;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 386
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/ad/a/a/x;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 388
    :cond_3
    iget-object v0, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 389
    :goto_0
    iget-object v2, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 390
    iget-object v2, p0, Lcom/google/ad/a/a/x;->e:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 391
    if-eqz v2, :cond_4

    .line 392
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 389
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 396
    :cond_5
    iget-object v0, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 397
    :goto_1
    iget-object v0, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 398
    iget-object v0, p0, Lcom/google/ad/a/a/x;->f:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 399
    if-eqz v0, :cond_6

    .line 400
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 397
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 404
    :cond_7
    iget-object v0, p0, Lcom/google/ad/a/a/x;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 405
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ad/a/a/x;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 407
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 408
    return-void
.end method
