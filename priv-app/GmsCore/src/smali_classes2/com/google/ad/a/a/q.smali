.class public final Lcom/google/ad/a/a/q;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 575
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 576
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ad/a/a/q;->a:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/q;->c:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ad/a/a/q;->cachedSize:I

    .line 577
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 644
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 645
    iget-boolean v1, p0, Lcom/google/ad/a/a/q;->a:Z

    if-eqz v1, :cond_0

    .line 646
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/ad/a/a/q;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 649
    :cond_0
    iget-object v1, p0, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 650
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 653
    :cond_1
    iget-object v1, p0, Lcom/google/ad/a/a/q;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 654
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ad/a/a/q;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 657
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 589
    if-ne p1, p0, :cond_1

    .line 613
    :cond_0
    :goto_0
    return v0

    .line 592
    :cond_1
    instance-of v2, p1, Lcom/google/ad/a/a/q;

    if-nez v2, :cond_2

    move v0, v1

    .line 593
    goto :goto_0

    .line 595
    :cond_2
    check-cast p1, Lcom/google/ad/a/a/q;

    .line 596
    iget-boolean v2, p0, Lcom/google/ad/a/a/q;->a:Z

    iget-boolean v3, p1, Lcom/google/ad/a/a/q;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 597
    goto :goto_0

    .line 599
    :cond_3
    iget-object v2, p0, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 600
    iget-object v2, p1, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 601
    goto :goto_0

    .line 603
    :cond_4
    iget-object v2, p0, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 604
    goto :goto_0

    .line 606
    :cond_5
    iget-object v2, p0, Lcom/google/ad/a/a/q;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 607
    iget-object v2, p1, Lcom/google/ad/a/a/q;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 608
    goto :goto_0

    .line 610
    :cond_6
    iget-object v2, p0, Lcom/google/ad/a/a/q;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/q;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 611
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 618
    iget-boolean v0, p0, Lcom/google/ad/a/a/q;->a:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 620
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 622
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ad/a/a/q;->c:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 624
    return v0

    .line 618
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 620
    :cond_1
    iget-object v0, p0, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 622
    :cond_2
    iget-object v1, p0, Lcom/google/ad/a/a/q;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 549
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/ad/a/a/q;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/q;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 630
    iget-boolean v0, p0, Lcom/google/ad/a/a/q;->a:Z

    if-eqz v0, :cond_0

    .line 631
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/ad/a/a/q;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 633
    :cond_0
    iget-object v0, p0, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 634
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ad/a/a/q;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 636
    :cond_1
    iget-object v0, p0, Lcom/google/ad/a/a/q;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 637
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ad/a/a/q;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 639
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 640
    return-void
.end method
