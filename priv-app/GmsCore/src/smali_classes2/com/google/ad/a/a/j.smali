.class public final Lcom/google/ad/a/a/j;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1912
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1913
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/j;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/j;->b:Ljava/lang/String;

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/ad/a/a/j;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ad/a/a/j;->cachedSize:I

    .line 1914
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1981
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1982
    iget-object v1, p0, Lcom/google/ad/a/a/j;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1983
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ad/a/a/j;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1986
    :cond_0
    iget-object v1, p0, Lcom/google/ad/a/a/j;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1987
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ad/a/a/j;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1990
    :cond_1
    iget v1, p0, Lcom/google/ad/a/a/j;->c:I

    const/16 v2, 0x3e8

    if-eq v1, v2, :cond_2

    .line 1991
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/ad/a/a/j;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1994
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1926
    if-ne p1, p0, :cond_1

    .line 1950
    :cond_0
    :goto_0
    return v0

    .line 1929
    :cond_1
    instance-of v2, p1, Lcom/google/ad/a/a/j;

    if-nez v2, :cond_2

    move v0, v1

    .line 1930
    goto :goto_0

    .line 1932
    :cond_2
    check-cast p1, Lcom/google/ad/a/a/j;

    .line 1933
    iget-object v2, p0, Lcom/google/ad/a/a/j;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 1934
    iget-object v2, p1, Lcom/google/ad/a/a/j;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1935
    goto :goto_0

    .line 1937
    :cond_3
    iget-object v2, p0, Lcom/google/ad/a/a/j;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/j;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1938
    goto :goto_0

    .line 1940
    :cond_4
    iget-object v2, p0, Lcom/google/ad/a/a/j;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1941
    iget-object v2, p1, Lcom/google/ad/a/a/j;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 1942
    goto :goto_0

    .line 1944
    :cond_5
    iget-object v2, p0, Lcom/google/ad/a/a/j;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/j;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1945
    goto :goto_0

    .line 1947
    :cond_6
    iget v2, p0, Lcom/google/ad/a/a/j;->c:I

    iget v3, p1, Lcom/google/ad/a/a/j;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1948
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1955
    iget-object v0, p0, Lcom/google/ad/a/a/j;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1958
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ad/a/a/j;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1960
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/ad/a/a/j;->c:I

    add-int/2addr v0, v1

    .line 1961
    return v0

    .line 1955
    :cond_0
    iget-object v0, p0, Lcom/google/ad/a/a/j;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1958
    :cond_1
    iget-object v1, p0, Lcom/google/ad/a/a/j;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1886
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/j;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/j;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/ad/a/a/j;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1967
    iget-object v0, p0, Lcom/google/ad/a/a/j;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1968
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ad/a/a/j;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1970
    :cond_0
    iget-object v0, p0, Lcom/google/ad/a/a/j;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1971
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ad/a/a/j;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1973
    :cond_1
    iget v0, p0, Lcom/google/ad/a/a/j;->c:I

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_2

    .line 1974
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/ad/a/a/j;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1976
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1977
    return-void
.end method
