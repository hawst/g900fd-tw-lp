.class public final Lcom/google/ad/a/a/w;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 726
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 727
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/w;->b:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ad/a/a/w;->cachedSize:I

    .line 728
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 787
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 788
    iget-object v1, p0, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 789
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 792
    :cond_0
    iget-object v1, p0, Lcom/google/ad/a/a/w;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 793
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ad/a/a/w;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 796
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 739
    if-ne p1, p0, :cond_1

    .line 760
    :cond_0
    :goto_0
    return v0

    .line 742
    :cond_1
    instance-of v2, p1, Lcom/google/ad/a/a/w;

    if-nez v2, :cond_2

    move v0, v1

    .line 743
    goto :goto_0

    .line 745
    :cond_2
    check-cast p1, Lcom/google/ad/a/a/w;

    .line 746
    iget-object v2, p0, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 747
    iget-object v2, p1, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 748
    goto :goto_0

    .line 750
    :cond_3
    iget-object v2, p0, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 751
    goto :goto_0

    .line 753
    :cond_4
    iget-object v2, p0, Lcom/google/ad/a/a/w;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 754
    iget-object v2, p1, Lcom/google/ad/a/a/w;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 755
    goto :goto_0

    .line 757
    :cond_5
    iget-object v2, p0, Lcom/google/ad/a/a/w;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/w;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 758
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 765
    iget-object v0, p0, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 768
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ad/a/a/w;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 770
    return v0

    .line 765
    :cond_0
    iget-object v0, p0, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 768
    :cond_1
    iget-object v1, p0, Lcom/google/ad/a/a/w;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 703
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/w;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 776
    iget-object v0, p0, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 777
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ad/a/a/w;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 779
    :cond_0
    iget-object v0, p0, Lcom/google/ad/a/a/w;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 780
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ad/a/a/w;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 782
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 783
    return-void
.end method
