.class public final Lcom/google/ad/a/a/n;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lcom/google/ad/a/a/m;

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 217
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 218
    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/n;->a:Ljava/lang/String;

    invoke-static {}, Lcom/google/ad/a/a/m;->a()[Lcom/google/ad/a/a/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ad/a/a/n;->c:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ad/a/a/n;->cachedSize:I

    .line 219
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 288
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 289
    iget-object v1, p0, Lcom/google/ad/a/a/n;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 290
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ad/a/a/n;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 293
    :cond_0
    iget-object v1, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 294
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 295
    iget-object v2, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    aget-object v2, v2, v0

    .line 296
    if-eqz v2, :cond_1

    .line 297
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 294
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 302
    :cond_3
    iget-boolean v1, p0, Lcom/google/ad/a/a/n;->c:Z

    if-eqz v1, :cond_4

    .line 303
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/ad/a/a/n;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 306
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 231
    if-ne p1, p0, :cond_1

    .line 252
    :cond_0
    :goto_0
    return v0

    .line 234
    :cond_1
    instance-of v2, p1, Lcom/google/ad/a/a/n;

    if-nez v2, :cond_2

    move v0, v1

    .line 235
    goto :goto_0

    .line 237
    :cond_2
    check-cast p1, Lcom/google/ad/a/a/n;

    .line 238
    iget-object v2, p0, Lcom/google/ad/a/a/n;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 239
    iget-object v2, p1, Lcom/google/ad/a/a/n;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 240
    goto :goto_0

    .line 242
    :cond_3
    iget-object v2, p0, Lcom/google/ad/a/a/n;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/n;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 243
    goto :goto_0

    .line 245
    :cond_4
    iget-object v2, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    iget-object v3, p1, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 247
    goto :goto_0

    .line 249
    :cond_5
    iget-boolean v2, p0, Lcom/google/ad/a/a/n;->c:Z

    iget-boolean v3, p1, Lcom/google/ad/a/a/n;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 250
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/ad/a/a/n;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 260
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 262
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/ad/a/a/n;->c:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v1

    .line 263
    return v0

    .line 257
    :cond_0
    iget-object v0, p0, Lcom/google/ad/a/a/n;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 262
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 191
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/n;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ad/a/a/m;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ad/a/a/m;

    invoke-direct {v3}, Lcom/google/ad/a/a/m;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ad/a/a/m;

    invoke-direct {v3}, Lcom/google/ad/a/a/m;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/ad/a/a/n;->c:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/ad/a/a/n;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 270
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ad/a/a/n;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 272
    :cond_0
    iget-object v0, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 273
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 274
    iget-object v1, p0, Lcom/google/ad/a/a/n;->b:[Lcom/google/ad/a/a/m;

    aget-object v1, v1, v0

    .line 275
    if-eqz v1, :cond_1

    .line 276
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 273
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 280
    :cond_2
    iget-boolean v0, p0, Lcom/google/ad/a/a/n;->c:Z

    if-eqz v0, :cond_3

    .line 281
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/ad/a/a/n;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 283
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 284
    return-void
.end method
