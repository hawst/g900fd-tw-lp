.class public final Lcom/google/ad/a/a/s;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Z

.field public b:Ljava/lang/String;

.field public c:Lcom/google/ad/a/a/g;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/ad/a/a/s;->a:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/s;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/s;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/s;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/s;->f:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ad/a/a/s;->cachedSize:I

    .line 55
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 163
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 164
    iget-boolean v1, p0, Lcom/google/ad/a/a/s;->a:Z

    if-eqz v1, :cond_0

    .line 165
    const/4 v1, 0x1

    iget-boolean v2, p0, Lcom/google/ad/a/a/s;->a:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 168
    :cond_0
    iget-object v1, p0, Lcom/google/ad/a/a/s;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 169
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ad/a/a/s;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_1
    iget-object v1, p0, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    if-eqz v1, :cond_2

    .line 173
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_2
    iget-object v1, p0, Lcom/google/ad/a/a/s;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 177
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ad/a/a/s;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 180
    :cond_3
    iget-object v1, p0, Lcom/google/ad/a/a/s;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 181
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ad/a/a/s;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 184
    :cond_4
    iget-object v1, p0, Lcom/google/ad/a/a/s;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 185
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ad/a/a/s;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 188
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 70
    if-ne p1, p0, :cond_1

    .line 117
    :cond_0
    :goto_0
    return v0

    .line 73
    :cond_1
    instance-of v2, p1, Lcom/google/ad/a/a/s;

    if-nez v2, :cond_2

    move v0, v1

    .line 74
    goto :goto_0

    .line 76
    :cond_2
    check-cast p1, Lcom/google/ad/a/a/s;

    .line 77
    iget-boolean v2, p0, Lcom/google/ad/a/a/s;->a:Z

    iget-boolean v3, p1, Lcom/google/ad/a/a/s;->a:Z

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 78
    goto :goto_0

    .line 80
    :cond_3
    iget-object v2, p0, Lcom/google/ad/a/a/s;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 81
    iget-object v2, p1, Lcom/google/ad/a/a/s;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 82
    goto :goto_0

    .line 84
    :cond_4
    iget-object v2, p0, Lcom/google/ad/a/a/s;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/s;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 85
    goto :goto_0

    .line 87
    :cond_5
    iget-object v2, p0, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    if-nez v2, :cond_6

    .line 88
    iget-object v2, p1, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    if-eqz v2, :cond_7

    move v0, v1

    .line 89
    goto :goto_0

    .line 92
    :cond_6
    iget-object v2, p0, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    iget-object v3, p1, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    invoke-virtual {v2, v3}, Lcom/google/ad/a/a/g;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 93
    goto :goto_0

    .line 96
    :cond_7
    iget-object v2, p0, Lcom/google/ad/a/a/s;->d:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 97
    iget-object v2, p1, Lcom/google/ad/a/a/s;->d:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 98
    goto :goto_0

    .line 100
    :cond_8
    iget-object v2, p0, Lcom/google/ad/a/a/s;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/s;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 101
    goto :goto_0

    .line 103
    :cond_9
    iget-object v2, p0, Lcom/google/ad/a/a/s;->e:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 104
    iget-object v2, p1, Lcom/google/ad/a/a/s;->e:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 105
    goto :goto_0

    .line 107
    :cond_a
    iget-object v2, p0, Lcom/google/ad/a/a/s;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/s;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 108
    goto :goto_0

    .line 110
    :cond_b
    iget-object v2, p0, Lcom/google/ad/a/a/s;->f:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 111
    iget-object v2, p1, Lcom/google/ad/a/a/s;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 112
    goto :goto_0

    .line 114
    :cond_c
    iget-object v2, p0, Lcom/google/ad/a/a/s;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/s;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 115
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 122
    iget-boolean v0, p0, Lcom/google/ad/a/a/s;->a:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 124
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/s;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 126
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 128
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/s;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 130
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/s;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 132
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ad/a/a/s;->f:Ljava/lang/String;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 134
    return v0

    .line 122
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 124
    :cond_1
    iget-object v0, p0, Lcom/google/ad/a/a/s;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 126
    :cond_2
    iget-object v0, p0, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    invoke-virtual {v0}, Lcom/google/ad/a/a/g;->hashCode()I

    move-result v0

    goto :goto_2

    .line 128
    :cond_3
    iget-object v0, p0, Lcom/google/ad/a/a/s;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 130
    :cond_4
    iget-object v0, p0, Lcom/google/ad/a/a/s;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 132
    :cond_5
    iget-object v1, p0, Lcom/google/ad/a/a/s;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 18
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/ad/a/a/s;->a:Z

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/s;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ad/a/a/g;

    invoke-direct {v0}, Lcom/google/ad/a/a/g;-><init>()V

    iput-object v0, p0, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    :cond_1
    iget-object v0, p0, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/s;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/s;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/s;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 140
    iget-boolean v0, p0, Lcom/google/ad/a/a/s;->a:Z

    if-eqz v0, :cond_0

    .line 141
    const/4 v0, 0x1

    iget-boolean v1, p0, Lcom/google/ad/a/a/s;->a:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/ad/a/a/s;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 144
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ad/a/a/s;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 146
    :cond_1
    iget-object v0, p0, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    if-eqz v0, :cond_2

    .line 147
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ad/a/a/s;->c:Lcom/google/ad/a/a/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/google/ad/a/a/s;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 150
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ad/a/a/s;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 152
    :cond_3
    iget-object v0, p0, Lcom/google/ad/a/a/s;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 153
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ad/a/a/s;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 155
    :cond_4
    iget-object v0, p0, Lcom/google/ad/a/a/s;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 156
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ad/a/a/s;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 158
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 159
    return-void
.end method
