.class public final Lcom/google/ad/a/a/p;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/ad/a/a/p;


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1544
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1545
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/ad/a/a/p;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/ad/a/a/p;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ad/a/a/p;->cachedSize:I

    .line 1546
    return-void
.end method

.method public static a()[Lcom/google/ad/a/a/p;
    .locals 2

    .prologue
    .line 1524
    sget-object v0, Lcom/google/ad/a/a/p;->d:[Lcom/google/ad/a/a/p;

    if-nez v0, :cond_1

    .line 1525
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1527
    :try_start_0
    sget-object v0, Lcom/google/ad/a/a/p;->d:[Lcom/google/ad/a/a/p;

    if-nez v0, :cond_0

    .line 1528
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ad/a/a/p;

    sput-object v0, Lcom/google/ad/a/a/p;->d:[Lcom/google/ad/a/a/p;

    .line 1530
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1532
    :cond_1
    sget-object v0, Lcom/google/ad/a/a/p;->d:[Lcom/google/ad/a/a/p;

    return-object v0

    .line 1530
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1615
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1616
    iget v2, p0, Lcom/google/ad/a/a/p;->a:I

    if-eqz v2, :cond_0

    .line 1617
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/ad/a/a/p;->a:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1620
    :cond_0
    iget-object v2, p0, Lcom/google/ad/a/a/p;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1621
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/ad/a/a/p;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1624
    :cond_1
    iget-object v2, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 1627
    :goto_0
    iget-object v4, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 1628
    iget-object v4, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 1629
    if-eqz v4, :cond_2

    .line 1630
    add-int/lit8 v3, v3, 0x1

    .line 1631
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1627
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1635
    :cond_3
    add-int/2addr v0, v2

    .line 1636
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 1638
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1558
    if-ne p1, p0, :cond_1

    .line 1579
    :cond_0
    :goto_0
    return v0

    .line 1561
    :cond_1
    instance-of v2, p1, Lcom/google/ad/a/a/p;

    if-nez v2, :cond_2

    move v0, v1

    .line 1562
    goto :goto_0

    .line 1564
    :cond_2
    check-cast p1, Lcom/google/ad/a/a/p;

    .line 1565
    iget v2, p0, Lcom/google/ad/a/a/p;->a:I

    iget v3, p1, Lcom/google/ad/a/a/p;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1566
    goto :goto_0

    .line 1568
    :cond_3
    iget-object v2, p0, Lcom/google/ad/a/a/p;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 1569
    iget-object v2, p1, Lcom/google/ad/a/a/p;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 1570
    goto :goto_0

    .line 1572
    :cond_4
    iget-object v2, p0, Lcom/google/ad/a/a/p;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/p;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1573
    goto :goto_0

    .line 1575
    :cond_5
    iget-object v2, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1577
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1584
    iget v0, p0, Lcom/google/ad/a/a/p;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 1586
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/ad/a/a/p;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 1588
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1590
    return v0

    .line 1586
    :cond_0
    iget-object v0, p0, Lcom/google/ad/a/a/p;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1518
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/ad/a/a/p;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ad/a/a/p;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1596
    iget v0, p0, Lcom/google/ad/a/a/p;->a:I

    if-eqz v0, :cond_0

    .line 1597
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/ad/a/a/p;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1599
    :cond_0
    iget-object v0, p0, Lcom/google/ad/a/a/p;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1600
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ad/a/a/p;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1602
    :cond_1
    iget-object v0, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 1603
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 1604
    iget-object v1, p0, Lcom/google/ad/a/a/p;->c:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 1605
    if-eqz v1, :cond_2

    .line 1606
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1603
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1610
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1611
    return-void
.end method
