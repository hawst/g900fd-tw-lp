.class public final Lcom/google/aa/b/a/a/a/a/w;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile m:[Lcom/google/aa/b/a/a/a/a/w;


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:[Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Lcom/google/checkout/inapp/proto/a/b;

.field public i:I

.field public j:Ljava/lang/String;

.field public k:Ljava/lang/String;

.field public l:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 113
    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    iput v1, p0, Lcom/google/aa/b/a/a/a/a/w;->b:I

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    iput v1, p0, Lcom/google/aa/b/a/a/a/a/w;->e:I

    iput v1, p0, Lcom/google/aa/b/a/a/a/a/w;->f:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->g:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    iput v1, p0, Lcom/google/aa/b/a/a/a/a/w;->i:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    iput v1, p0, Lcom/google/aa/b/a/a/a/a/w;->l:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/w;->cachedSize:I

    .line 114
    return-void
.end method

.method public static a()[Lcom/google/aa/b/a/a/a/a/w;
    .locals 2

    .prologue
    .line 65
    sget-object v0, Lcom/google/aa/b/a/a/a/a/w;->m:[Lcom/google/aa/b/a/a/a/a/w;

    if-nez v0, :cond_1

    .line 66
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 68
    :try_start_0
    sget-object v0, Lcom/google/aa/b/a/a/a/a/w;->m:[Lcom/google/aa/b/a/a/a/a/w;

    if-nez v0, :cond_0

    .line 69
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/aa/b/a/a/a/a/w;

    sput-object v0, Lcom/google/aa/b/a/a/a/a/w;->m:[Lcom/google/aa/b/a/a/a/a/w;

    .line 71
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 73
    :cond_1
    sget-object v0, Lcom/google/aa/b/a/a/a/a/w;->m:[Lcom/google/aa/b/a/a/a/a/w;

    return-object v0

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 282
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 283
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 284
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 287
    :cond_0
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->b:I

    if-eqz v2, :cond_1

    .line 288
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/aa/b/a/a/a/a/w;->b:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 291
    :cond_1
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 294
    :goto_0
    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 295
    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 296
    if-eqz v4, :cond_2

    .line 297
    add-int/lit8 v3, v3, 0x1

    .line 298
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 294
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 302
    :cond_3
    add-int/2addr v0, v2

    .line 303
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 305
    :cond_4
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 306
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 309
    :cond_5
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->e:I

    if-eqz v1, :cond_6

    .line 310
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 313
    :cond_6
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->f:I

    if-eqz v1, :cond_7

    .line 314
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 317
    :cond_7
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 318
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 321
    :cond_8
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->i:I

    if-eqz v1, :cond_9

    .line 322
    const/16 v1, 0xb

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 325
    :cond_9
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 326
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 329
    :cond_a
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 330
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 333
    :cond_b
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->l:I

    if-eqz v1, :cond_c

    .line 334
    const/16 v1, 0xe

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->l:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 337
    :cond_c
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_d

    .line 338
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 341
    :cond_d
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 135
    if-ne p1, p0, :cond_1

    .line 205
    :cond_0
    :goto_0
    return v0

    .line 138
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/w;

    if-nez v2, :cond_2

    move v0, v1

    .line 139
    goto :goto_0

    .line 141
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/w;

    .line 142
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 143
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 144
    goto :goto_0

    .line 146
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 147
    goto :goto_0

    .line 149
    :cond_4
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->b:I

    iget v3, p1, Lcom/google/aa/b/a/a/a/a/w;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 150
    goto :goto_0

    .line 152
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 154
    goto :goto_0

    .line 156
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 157
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 158
    goto :goto_0

    .line 160
    :cond_7
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 161
    goto :goto_0

    .line 163
    :cond_8
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->e:I

    iget v3, p1, Lcom/google/aa/b/a/a/a/a/w;->e:I

    if-eq v2, v3, :cond_9

    move v0, v1

    .line 164
    goto :goto_0

    .line 166
    :cond_9
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->f:I

    iget v3, p1, Lcom/google/aa/b/a/a/a/a/w;->f:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 167
    goto :goto_0

    .line 169
    :cond_a
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->g:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 170
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/w;->g:Ljava/lang/String;

    if-eqz v2, :cond_c

    move v0, v1

    .line 171
    goto :goto_0

    .line 173
    :cond_b
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/w;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 174
    goto :goto_0

    .line 176
    :cond_c
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v2, :cond_d

    .line 177
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_e

    move v0, v1

    .line 178
    goto :goto_0

    .line 181
    :cond_d
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 182
    goto/16 :goto_0

    .line 185
    :cond_e
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->i:I

    iget v3, p1, Lcom/google/aa/b/a/a/a/a/w;->i:I

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 186
    goto/16 :goto_0

    .line 188
    :cond_f
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 189
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    if-eqz v2, :cond_11

    move v0, v1

    .line 190
    goto/16 :goto_0

    .line 192
    :cond_10
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    move v0, v1

    .line 193
    goto/16 :goto_0

    .line 195
    :cond_11
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    if-nez v2, :cond_12

    .line 196
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    if-eqz v2, :cond_13

    move v0, v1

    .line 197
    goto/16 :goto_0

    .line 199
    :cond_12
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 200
    goto/16 :goto_0

    .line 202
    :cond_13
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->l:I

    iget v3, p1, Lcom/google/aa/b/a/a/a/a/w;->l:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 203
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 210
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 213
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->b:I

    add-int/2addr v0, v2

    .line 214
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 216
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 218
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->e:I

    add-int/2addr v0, v2

    .line 219
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->f:I

    add-int/2addr v0, v2

    .line 220
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->g:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 222
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 224
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/w;->i:I

    add-int/2addr v0, v2

    .line 225
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 227
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 229
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->l:I

    add-int/2addr v0, v1

    .line 230
    return v0

    .line 210
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 216
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 220
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 222
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/a/b;->hashCode()I

    move-result v0

    goto :goto_3

    .line 225
    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 227
    :cond_5
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 34
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/aa/b/a/a/a/a/w;->b:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/w;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/w;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/aa/b/a/a/a/a/w;->i:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_2

    goto/16 :goto_0

    :pswitch_2
    iput v0, p0, Lcom/google/aa/b/a/a/a/a/w;->l:I

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x58 -> :sswitch_8
        0x62 -> :sswitch_9
        0x6a -> :sswitch_a
        0x70 -> :sswitch_b
        0x8a -> :sswitch_c
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 237
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 239
    :cond_0
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/w;->b:I

    if-eqz v0, :cond_1

    .line 240
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 242
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 243
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 244
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->c:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 245
    if-eqz v1, :cond_2

    .line 246
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 243
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 250
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 251
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 253
    :cond_4
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/w;->e:I

    if-eqz v0, :cond_5

    .line 254
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 256
    :cond_5
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/w;->f:I

    if-eqz v0, :cond_6

    .line 257
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 259
    :cond_6
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 260
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 262
    :cond_7
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/w;->i:I

    if-eqz v0, :cond_8

    .line 263
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 265
    :cond_8
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 266
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 268
    :cond_9
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 269
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 271
    :cond_a
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/w;->l:I

    if-eqz v0, :cond_b

    .line 272
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/w;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 274
    :cond_b
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_c

    .line 275
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/w;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 277
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 278
    return-void
.end method
