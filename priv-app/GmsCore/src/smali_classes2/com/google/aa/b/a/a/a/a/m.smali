.class public final Lcom/google/aa/b/a/a/a/a/m;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/aa/b/a/a/a/a/u;

.field public b:Ljava/lang/String;

.field public c:[I

.field public d:Z

.field public e:Ljava/lang/String;

.field public f:Lcom/google/aa/b/a/a/a/a/t;

.field public g:Ljava/lang/String;

.field public h:Z

.field public i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 2166
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2167
    iput-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    iput-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/m;->d:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    iput-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->g:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/m;->h:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->i:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/m;->cachedSize:I

    .line 2168
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2311
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2312
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    if-eqz v2, :cond_0

    .line 2313
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2316
    :cond_0
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 2317
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/m;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2320
    :cond_1
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    .line 2322
    :goto_0
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 2323
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    aget v3, v3, v1

    .line 2324
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 2322
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2327
    :cond_2
    add-int/2addr v0, v2

    .line 2328
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2330
    :cond_3
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/m;->d:Z

    if-eqz v1, :cond_4

    .line 2331
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/m;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2334
    :cond_4
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2335
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2338
    :cond_5
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    if-eqz v1, :cond_6

    .line 2339
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2342
    :cond_6
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 2343
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2346
    :cond_7
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/m;->h:Z

    if-eqz v1, :cond_8

    .line 2347
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/m;->h:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2350
    :cond_8
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 2351
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2354
    :cond_9
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2186
    if-ne p1, p0, :cond_1

    .line 2249
    :cond_0
    :goto_0
    return v0

    .line 2189
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/m;

    if-nez v2, :cond_2

    move v0, v1

    .line 2190
    goto :goto_0

    .line 2192
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/m;

    .line 2193
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    if-nez v2, :cond_3

    .line 2194
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2195
    goto :goto_0

    .line 2198
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/a/a/a/u;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2199
    goto :goto_0

    .line 2202
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 2203
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/m;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 2204
    goto :goto_0

    .line 2206
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/m;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 2207
    goto :goto_0

    .line 2209
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 2211
    goto :goto_0

    .line 2213
    :cond_7
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/m;->d:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/m;->d:Z

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 2214
    goto :goto_0

    .line 2216
    :cond_8
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 2217
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 2218
    goto :goto_0

    .line 2220
    :cond_9
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 2221
    goto :goto_0

    .line 2223
    :cond_a
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    if-nez v2, :cond_b

    .line 2224
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    if-eqz v2, :cond_c

    move v0, v1

    .line 2225
    goto :goto_0

    .line 2228
    :cond_b
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/a/a/a/t;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 2229
    goto :goto_0

    .line 2232
    :cond_c
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->g:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 2233
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/m;->g:Ljava/lang/String;

    if-eqz v2, :cond_e

    move v0, v1

    .line 2234
    goto :goto_0

    .line 2236
    :cond_d
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/m;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 2237
    goto/16 :goto_0

    .line 2239
    :cond_e
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/m;->h:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/m;->h:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 2240
    goto/16 :goto_0

    .line 2242
    :cond_f
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->i:Ljava/lang/String;

    if-nez v2, :cond_10

    .line 2243
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/m;->i:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 2244
    goto/16 :goto_0

    .line 2246
    :cond_10
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/m;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2247
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 2254
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2257
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 2259
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    invoke-static {v4}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v4

    add-int/2addr v0, v4

    .line 2261
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/m;->d:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_2
    add-int/2addr v0, v4

    .line 2262
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    .line 2264
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v4

    .line 2266
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->g:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v4

    .line 2268
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/aa/b/a/a/a/a/m;->h:Z

    if-eqz v4, :cond_6

    :goto_6
    add-int/2addr v0, v2

    .line 2269
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->i:Ljava/lang/String;

    if-nez v2, :cond_7

    :goto_7
    add-int/2addr v0, v1

    .line 2271
    return v0

    .line 2254
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    invoke-virtual {v0}, Lcom/google/aa/b/a/a/a/a/u;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2257
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    :cond_2
    move v0, v3

    .line 2261
    goto :goto_2

    .line 2262
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 2264
    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    invoke-virtual {v0}, Lcom/google/aa/b/a/a/a/a/t;->hashCode()I

    move-result v0

    goto :goto_4

    .line 2266
    :cond_5
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_6
    move v2, v3

    .line 2268
    goto :goto_6

    .line 2269
    :cond_7
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2122
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/aa/b/a/a/a/a/u;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/u;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    if-nez v0, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_5

    array-length v3, v5

    if-ne v1, v3, :cond_5

    iput-object v5, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    array-length v0, v0

    goto :goto_3

    :cond_5
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_6

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    if-eqz v0, :cond_b

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    if-nez v1, :cond_9

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_a

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_9
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    array-length v1, v1

    goto :goto_5

    :cond_a
    iput-object v4, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    :cond_b
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/m;->d:Z

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    if-nez v0, :cond_c

    new-instance v0, Lcom/google/aa/b/a/a/a/a/t;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/t;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    :cond_c
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/m;->h:Z

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->i:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x20 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0x40 -> :sswitch_9
        0x4a -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 2277
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    if-eqz v0, :cond_0

    .line 2278
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->a:Lcom/google/aa/b/a/a/a/a/u;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2280
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2281
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2283
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    array-length v0, v0

    if-lez v0, :cond_2

    .line 2284
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 2285
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/m;->c:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2284
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2288
    :cond_2
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/m;->d:Z

    if-eqz v0, :cond_3

    .line 2289
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/m;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2291
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2292
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2294
    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    if-eqz v0, :cond_5

    .line 2295
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->f:Lcom/google/aa/b/a/a/a/a/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2297
    :cond_5
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 2298
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2300
    :cond_6
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/m;->h:Z

    if-eqz v0, :cond_7

    .line 2301
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/m;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2303
    :cond_7
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/m;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 2304
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/m;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2306
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2307
    return-void
.end method
