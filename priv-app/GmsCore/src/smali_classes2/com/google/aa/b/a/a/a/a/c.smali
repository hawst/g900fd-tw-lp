.class public final Lcom/google/aa/b/a/a/a/a/c;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/aa/b/a/a/a/a/t;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5913
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5914
    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/c;->cachedSize:I

    .line 5915
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5989
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 5990
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/c;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 5991
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/c;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5994
    :cond_0
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/c;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 5995
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/c;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5998
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    if-eqz v1, :cond_2

    .line 5999
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6002
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5927
    if-ne p1, p0, :cond_1

    .line 5957
    :cond_0
    :goto_0
    return v0

    .line 5930
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/c;

    if-nez v2, :cond_2

    move v0, v1

    .line 5931
    goto :goto_0

    .line 5933
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/c;

    .line 5934
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/c;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 5935
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/c;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 5936
    goto :goto_0

    .line 5938
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/c;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/c;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 5939
    goto :goto_0

    .line 5941
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/c;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 5942
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/c;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 5943
    goto :goto_0

    .line 5945
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/c;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/c;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 5946
    goto :goto_0

    .line 5948
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    if-nez v2, :cond_7

    .line 5949
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    if-eqz v2, :cond_0

    move v0, v1

    .line 5950
    goto :goto_0

    .line 5953
    :cond_7
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/a/a/a/t;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 5954
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5962
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 5965
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 5967
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 5969
    return v0

    .line 5962
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5965
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 5967
    :cond_2
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    invoke-virtual {v1}, Lcom/google/aa/b/a/a/a/a/t;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5887
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/aa/b/a/a/a/a/t;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/t;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5975
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5976
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/c;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5978
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5979
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5981
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    if-eqz v0, :cond_2

    .line 5982
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/c;->c:Lcom/google/aa/b/a/a/a/a/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5984
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5985
    return-void
.end method
