.class public final Lcom/google/aa/b/a/a/a/a/k;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/aa/b/a/a/a/a/u;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Lcom/google/checkout/a/a/a/d;

.field public g:Lcom/google/checkout/inapp/proto/a/b;

.field public h:[Ljava/lang/String;

.field public i:Z

.field public j:J

.field public k:[I

.field public l:Z

.field public m:Lcom/google/aa/b/a/a/a/a/t;

.field public n:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 2849
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2850
    iput-object v3, p0, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/k;->e:Z

    iput-object v3, p0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    iput-object v3, p0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/aa/b/a/a/a/a/k;->j:J

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    iput-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/k;->l:Z

    iput-object v3, p0, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    iput v2, p0, Lcom/google/aa/b/a/a/a/a/k;->n:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/k;->cachedSize:I

    .line 2851
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 3051
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3052
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    if-eqz v1, :cond_0

    .line 3053
    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3056
    :cond_0
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/k;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3057
    const/4 v1, 0x2

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/k;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3060
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/k;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3061
    const/4 v1, 0x3

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/k;->c:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3064
    :cond_2
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3065
    const/4 v1, 0x4

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3068
    :cond_3
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    if-eqz v1, :cond_4

    .line 3069
    const/4 v1, 0x5

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3072
    :cond_4
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_5

    .line 3073
    const/4 v1, 0x7

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3076
    :cond_5
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_8

    move v1, v2

    move v3, v2

    move v4, v2

    .line 3079
    :goto_0
    iget-object v5, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_7

    .line 3080
    iget-object v5, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 3081
    if-eqz v5, :cond_6

    .line 3082
    add-int/lit8 v4, v4, 0x1

    .line 3083
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 3079
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3087
    :cond_7
    add-int/2addr v0, v3

    .line 3088
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 3090
    :cond_8
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    if-eqz v1, :cond_9

    .line 3091
    const/16 v1, 0x9

    iget-boolean v3, p0, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3094
    :cond_9
    iget-wide v4, p0, Lcom/google/aa/b/a/a/a/a/k;->j:J

    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-eqz v1, :cond_a

    .line 3095
    const/16 v1, 0xa

    iget-wide v4, p0, Lcom/google/aa/b/a/a/a/a/k;->j:J

    invoke-static {v1, v4, v5}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3098
    :cond_a
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    array-length v1, v1

    if-lez v1, :cond_c

    move v1, v2

    .line 3100
    :goto_1
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    array-length v3, v3

    if-ge v2, v3, :cond_b

    .line 3101
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    aget v3, v3, v2

    .line 3102
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 3100
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 3105
    :cond_b
    add-int/2addr v0, v1

    .line 3106
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3108
    :cond_c
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/k;->l:Z

    if-eqz v1, :cond_d

    .line 3109
    const/16 v1, 0xc

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/k;->l:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3112
    :cond_d
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    if-eqz v1, :cond_e

    .line 3113
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3116
    :cond_e
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/k;->n:I

    if-eqz v1, :cond_f

    .line 3117
    const/16 v1, 0xf

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/k;->n:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3120
    :cond_f
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/k;->e:Z

    if-eqz v1, :cond_10

    .line 3121
    const/16 v1, 0x10

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/k;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3124
    :cond_10
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2874
    if-ne p1, p0, :cond_1

    .line 2961
    :cond_0
    :goto_0
    return v0

    .line 2877
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/k;

    if-nez v2, :cond_2

    move v0, v1

    .line 2878
    goto :goto_0

    .line 2880
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/k;

    .line 2881
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    if-nez v2, :cond_3

    .line 2882
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2883
    goto :goto_0

    .line 2886
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/a/a/a/u;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2887
    goto :goto_0

    .line 2890
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 2891
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/k;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 2892
    goto :goto_0

    .line 2894
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/k;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 2895
    goto :goto_0

    .line 2897
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2898
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/k;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 2899
    goto :goto_0

    .line 2901
    :cond_7
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/k;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 2902
    goto :goto_0

    .line 2904
    :cond_8
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 2905
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 2906
    goto :goto_0

    .line 2908
    :cond_9
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 2909
    goto :goto_0

    .line 2911
    :cond_a
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/k;->e:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/k;->e:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 2912
    goto :goto_0

    .line 2914
    :cond_b
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    if-nez v2, :cond_c

    .line 2915
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    if-eqz v2, :cond_d

    move v0, v1

    .line 2916
    goto :goto_0

    .line 2919
    :cond_c
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    invoke-virtual {v2, v3}, Lcom/google/checkout/a/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 2920
    goto :goto_0

    .line 2923
    :cond_d
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v2, :cond_e

    .line 2924
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_f

    move v0, v1

    .line 2925
    goto/16 :goto_0

    .line 2928
    :cond_e
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 2929
    goto/16 :goto_0

    .line 2932
    :cond_f
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 2934
    goto/16 :goto_0

    .line 2936
    :cond_10
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 2937
    goto/16 :goto_0

    .line 2939
    :cond_11
    iget-wide v2, p0, Lcom/google/aa/b/a/a/a/a/k;->j:J

    iget-wide v4, p1, Lcom/google/aa/b/a/a/a/a/k;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_12

    move v0, v1

    .line 2940
    goto/16 :goto_0

    .line 2942
    :cond_12
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 2944
    goto/16 :goto_0

    .line 2946
    :cond_13
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/k;->l:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/k;->l:Z

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 2947
    goto/16 :goto_0

    .line 2949
    :cond_14
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    if-nez v2, :cond_15

    .line 2950
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    if-eqz v2, :cond_16

    move v0, v1

    .line 2951
    goto/16 :goto_0

    .line 2954
    :cond_15
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/a/a/a/t;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move v0, v1

    .line 2955
    goto/16 :goto_0

    .line 2958
    :cond_16
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/k;->n:I

    iget v3, p1, Lcom/google/aa/b/a/a/a/a/k;->n:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2959
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 9

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 2966
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2969
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 2971
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 2973
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    .line 2975
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/k;->e:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v4

    .line 2976
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v4

    .line 2978
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v4

    .line 2980
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    invoke-static {v4}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 2982
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_7
    add-int/2addr v0, v4

    .line 2983
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v4, p0, Lcom/google/aa/b/a/a/a/a/k;->j:J

    iget-wide v6, p0, Lcom/google/aa/b/a/a/a/a/k;->j:J

    const/16 v8, 0x20

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int/2addr v0, v4

    .line 2985
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    invoke-static {v4}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v4

    add-int/2addr v0, v4

    .line 2987
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/aa/b/a/a/a/a/k;->l:Z

    if-eqz v4, :cond_8

    :goto_8
    add-int/2addr v0, v2

    .line 2988
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    if-nez v2, :cond_9

    :goto_9
    add-int/2addr v0, v1

    .line 2990
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/k;->n:I

    add-int/2addr v0, v1

    .line 2991
    return v0

    .line 2966
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    invoke-virtual {v0}, Lcom/google/aa/b/a/a/a/a/u;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2969
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2971
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2973
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    move v0, v3

    .line 2975
    goto :goto_4

    .line 2976
    :cond_5
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    invoke-virtual {v0}, Lcom/google/checkout/a/a/a/d;->hashCode()I

    move-result v0

    goto :goto_5

    .line 2978
    :cond_6
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/a/b;->hashCode()I

    move-result v0

    goto :goto_6

    :cond_7
    move v0, v3

    .line 2982
    goto :goto_7

    :cond_8
    move v2, v3

    .line 2987
    goto :goto_8

    .line 2988
    :cond_9
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    invoke-virtual {v1}, Lcom/google/aa/b/a/a/a/a/t;->hashCode()I

    move-result v1

    goto :goto_9
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 2785
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/aa/b/a/a/a/a/u;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/u;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/checkout/a/a/a/d;

    invoke-direct {v0}, Lcom/google/checkout/a/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/aa/b/a/a/a/a/k;->j:J

    goto/16 :goto_0

    :sswitch_a
    const/16 v0, 0x58

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v1

    move v2, v1

    :goto_3
    if-ge v3, v4, :cond_8

    if-eqz v3, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v2

    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_3

    :pswitch_0
    add-int/lit8 v0, v2, 0x1

    aput v6, v5, v2

    goto :goto_4

    :cond_8
    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    if-nez v0, :cond_9

    move v0, v1

    :goto_5
    if-nez v0, :cond_a

    array-length v3, v5

    if-ne v2, v3, :cond_a

    iput-object v5, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    array-length v0, v0

    goto :goto_5

    :cond_a
    add-int v3, v0, v2

    new-array v3, v3, [I

    if-eqz v0, :cond_b

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    invoke-static {v5, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_c

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_6

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_c
    if-eqz v0, :cond_10

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    if-nez v2, :cond_e

    move v2, v1

    :goto_7
    add-int/2addr v0, v2

    new-array v4, v0, [I

    if-eqz v2, :cond_d

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    invoke-static {v0, v1, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_d
    :goto_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_f

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_8

    :pswitch_2
    add-int/lit8 v0, v2, 0x1

    aput v5, v4, v2

    move v2, v0

    goto :goto_8

    :cond_e
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    array-length v2, v2

    goto :goto_7

    :cond_f
    iput-object v4, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    :cond_10
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/k;->l:Z

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    if-nez v0, :cond_11

    new-instance v0, Lcom/google/aa/b/a/a/a/a/t;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/t;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    :cond_11
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_0

    :pswitch_3
    iput v0, p0, Lcom/google/aa/b/a/a/a/a/k;->n:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/k;->e:Z

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x5a -> :sswitch_b
        0x60 -> :sswitch_c
        0x6a -> :sswitch_d
        0x78 -> :sswitch_e
        0x80 -> :sswitch_f
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 2997
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    if-eqz v0, :cond_0

    .line 2998
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->a:Lcom/google/aa/b/a/a/a/a/u;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3000
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3001
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3003
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3004
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3006
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3007
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3009
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    if-eqz v0, :cond_4

    .line 3010
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->f:Lcom/google/checkout/a/a/a/d;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3012
    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_5

    .line 3013
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3015
    :cond_5
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_7

    move v0, v1

    .line 3016
    :goto_0
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_7

    .line 3017
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->h:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 3018
    if-eqz v2, :cond_6

    .line 3019
    const/16 v3, 0x8

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3016
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3023
    :cond_7
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    if-eqz v0, :cond_8

    .line 3024
    const/16 v0, 0x9

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/k;->i:Z

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3026
    :cond_8
    iget-wide v2, p0, Lcom/google/aa/b/a/a/a/a/k;->j:J

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-eqz v0, :cond_9

    .line 3027
    const/16 v0, 0xa

    iget-wide v2, p0, Lcom/google/aa/b/a/a/a/a/k;->j:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3029
    :cond_9
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    array-length v0, v0

    if-lez v0, :cond_a

    .line 3030
    :goto_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    array-length v0, v0

    if-ge v1, v0, :cond_a

    .line 3031
    const/16 v0, 0xb

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/k;->k:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3030
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3034
    :cond_a
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/k;->l:Z

    if-eqz v0, :cond_b

    .line 3035
    const/16 v0, 0xc

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/k;->l:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3037
    :cond_b
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    if-eqz v0, :cond_c

    .line 3038
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/k;->m:Lcom/google/aa/b/a/a/a/a/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3040
    :cond_c
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/k;->n:I

    if-eqz v0, :cond_d

    .line 3041
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/k;->n:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3043
    :cond_d
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/k;->e:Z

    if-eqz v0, :cond_e

    .line 3044
    const/16 v0, 0x10

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/k;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3046
    :cond_e
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3047
    return-void
.end method
