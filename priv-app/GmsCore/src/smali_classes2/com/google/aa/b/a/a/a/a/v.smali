.class public final Lcom/google/aa/b/a/a/a/a/v;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Ljava/lang/String;

.field public c:[Lcom/google/aa/b/a/g;

.field public d:Lcom/google/checkout/inapp/proto/a/b;

.field public e:Lcom/google/aa/b/a/a/a/a/w;

.field public f:[Lcom/google/aa/b/a/h;

.field public g:[Lcom/google/aa/b/a/i;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1801
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1802
    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    invoke-static {}, Lcom/google/aa/b/a/g;->a()[Lcom/google/aa/b/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    invoke-static {}, Lcom/google/aa/b/a/h;->a()[Lcom/google/aa/b/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    invoke-static {}, Lcom/google/aa/b/a/i;->a()[Lcom/google/aa/b/a/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/v;->cachedSize:I

    .line 1803
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1939
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1940
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1941
    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1944
    :cond_0
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v2

    move v3, v2

    move v4, v2

    .line 1947
    :goto_0
    iget-object v5, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_2

    .line 1948
    iget-object v5, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 1949
    if-eqz v5, :cond_1

    .line 1950
    add-int/lit8 v4, v4, 0x1

    .line 1951
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1947
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1955
    :cond_2
    add-int/2addr v0, v3

    .line 1956
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 1958
    :cond_3
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_4

    .line 1959
    const/4 v1, 0x4

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1962
    :cond_4
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    if-eqz v1, :cond_5

    .line 1963
    const/4 v1, 0x5

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1966
    :cond_5
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    array-length v1, v1

    if-lez v1, :cond_8

    move v1, v0

    move v0, v2

    .line 1967
    :goto_1
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    array-length v3, v3

    if-ge v0, v3, :cond_7

    .line 1968
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    aget-object v3, v3, v0

    .line 1969
    if-eqz v3, :cond_6

    .line 1970
    const/4 v4, 0x6

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v1, v3

    .line 1967
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_7
    move v0, v1

    .line 1975
    :cond_8
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    array-length v1, v1

    if-lez v1, :cond_b

    move v1, v0

    move v0, v2

    .line 1976
    :goto_2
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    array-length v3, v3

    if-ge v0, v3, :cond_a

    .line 1977
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    aget-object v3, v3, v0

    .line 1978
    if-eqz v3, :cond_9

    .line 1979
    const/4 v4, 0x7

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v1, v3

    .line 1976
    :cond_9
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_a
    move v0, v1

    .line 1984
    :cond_b
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    if-eqz v1, :cond_d

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    array-length v1, v1

    if-lez v1, :cond_d

    .line 1985
    :goto_3
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    array-length v1, v1

    if-ge v2, v1, :cond_d

    .line 1986
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    aget-object v1, v1, v2

    .line 1987
    if-eqz v1, :cond_c

    .line 1988
    const/16 v3, 0x8

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1985
    :cond_c
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1993
    :cond_d
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1819
    if-ne p1, p0, :cond_1

    .line 1867
    :cond_0
    :goto_0
    return v0

    .line 1822
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/v;

    if-nez v2, :cond_2

    move v0, v1

    .line 1823
    goto :goto_0

    .line 1825
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/v;

    .line 1826
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 1827
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1828
    goto :goto_0

    .line 1830
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1831
    goto :goto_0

    .line 1833
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1835
    goto :goto_0

    .line 1837
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1839
    goto :goto_0

    .line 1841
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v2, :cond_7

    .line 1842
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_8

    move v0, v1

    .line 1843
    goto :goto_0

    .line 1846
    :cond_7
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1847
    goto :goto_0

    .line 1850
    :cond_8
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    if-nez v2, :cond_9

    .line 1851
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    if-eqz v2, :cond_a

    move v0, v1

    .line 1852
    goto :goto_0

    .line 1855
    :cond_9
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/a/a/a/w;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 1856
    goto :goto_0

    .line 1859
    :cond_a
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1861
    goto :goto_0

    .line 1863
    :cond_b
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1865
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1872
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1875
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1877
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1879
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1881
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 1883
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1885
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1887
    return v0

    .line 1872
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1879
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/a/b;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1881
    :cond_2
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    invoke-virtual {v1}, Lcom/google/aa/b/a/a/a/a/w;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1763
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/aa/b/a/a/a/a/w;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/w;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    :cond_5
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/aa/b/a/h;

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Lcom/google/aa/b/a/h;

    invoke-direct {v3}, Lcom/google/aa/b/a/h;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Lcom/google/aa/b/a/h;

    invoke-direct {v3}, Lcom/google/aa/b/a/h;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    if-nez v0, :cond_a

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/aa/b/a/i;

    if-eqz v0, :cond_9

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_b

    new-instance v3, Lcom/google/aa/b/a/i;

    invoke-direct {v3}, Lcom/google/aa/b/a/i;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_a
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    array-length v0, v0

    goto :goto_5

    :cond_b
    new-instance v3, Lcom/google/aa/b/a/i;

    invoke-direct {v3}, Lcom/google/aa/b/a/i;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    if-nez v0, :cond_d

    move v0, v1

    :goto_7
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/aa/b/a/g;

    if-eqz v0, :cond_c

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    :goto_8
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_e

    new-instance v3, Lcom/google/aa/b/a/g;

    invoke-direct {v3}, Lcom/google/aa/b/a/g;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_d
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    array-length v0, v0

    goto :goto_7

    :cond_e
    new-instance v3, Lcom/google/aa/b/a/g;

    invoke-direct {v3}, Lcom/google/aa/b/a/g;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1893
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1894
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1896
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 1897
    :goto_0
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1898
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 1899
    if-eqz v2, :cond_1

    .line 1900
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1897
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1904
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_3

    .line 1905
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->d:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1907
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    if-eqz v0, :cond_4

    .line 1908
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->e:Lcom/google/aa/b/a/a/a/a/w;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1910
    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    .line 1911
    :goto_1
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 1912
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->f:[Lcom/google/aa/b/a/h;

    aget-object v2, v2, v0

    .line 1913
    if-eqz v2, :cond_5

    .line 1914
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1911
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1918
    :cond_6
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    .line 1919
    :goto_2
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    array-length v2, v2

    if-ge v0, v2, :cond_8

    .line 1920
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/v;->g:[Lcom/google/aa/b/a/i;

    aget-object v2, v2, v0

    .line 1921
    if-eqz v2, :cond_7

    .line 1922
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1919
    :cond_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1926
    :cond_8
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    array-length v0, v0

    if-lez v0, :cond_a

    .line 1927
    :goto_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    array-length v0, v0

    if-ge v1, v0, :cond_a

    .line 1928
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/v;->c:[Lcom/google/aa/b/a/g;

    aget-object v0, v0, v1

    .line 1929
    if-eqz v0, :cond_9

    .line 1930
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1927
    :cond_9
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1934
    :cond_a
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1935
    return-void
.end method
