.class public final Lcom/google/aa/b/a/a/a/a/b;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:[Ljava/lang/String;

.field public d:I

.field public e:I

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, -0x1

    .line 994
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 995
    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/b;->a:Ljava/lang/String;

    iput v2, p0, Lcom/google/aa/b/a/a/a/a/b;->b:I

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    iput v1, p0, Lcom/google/aa/b/a/a/a/a/b;->d:I

    iput v2, p0, Lcom/google/aa/b/a/a/a/a/b;->e:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/b;->f:Ljava/lang/String;

    iput v1, p0, Lcom/google/aa/b/a/a/a/a/b;->cachedSize:I

    .line 996
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1094
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1095
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/b;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1096
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/b;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1099
    :cond_0
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/b;->b:I

    if-eqz v2, :cond_1

    .line 1100
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/aa/b/a/a/a/a/b;->b:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1103
    :cond_1
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 1106
    :goto_0
    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 1107
    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 1108
    if-eqz v4, :cond_2

    .line 1109
    add-int/lit8 v3, v3, 0x1

    .line 1110
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 1106
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1114
    :cond_3
    add-int/2addr v0, v2

    .line 1115
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 1117
    :cond_4
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/b;->d:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_5

    .line 1118
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/b;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1121
    :cond_5
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/b;->e:I

    if-eqz v1, :cond_6

    .line 1122
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/b;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1125
    :cond_6
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/b;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1126
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/b;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1129
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1011
    if-ne p1, p0, :cond_1

    .line 1045
    :cond_0
    :goto_0
    return v0

    .line 1014
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/b;

    if-nez v2, :cond_2

    move v0, v1

    .line 1015
    goto :goto_0

    .line 1017
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/b;

    .line 1018
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/b;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 1019
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/b;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1020
    goto :goto_0

    .line 1022
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/b;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/b;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1023
    goto :goto_0

    .line 1025
    :cond_4
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/b;->b:I

    iget v3, p1, Lcom/google/aa/b/a/a/a/a/b;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1026
    goto :goto_0

    .line 1028
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1030
    goto :goto_0

    .line 1032
    :cond_6
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/b;->d:I

    iget v3, p1, Lcom/google/aa/b/a/a/a/a/b;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 1033
    goto :goto_0

    .line 1035
    :cond_7
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/b;->e:I

    iget v3, p1, Lcom/google/aa/b/a/a/a/a/b;->e:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 1036
    goto :goto_0

    .line 1038
    :cond_8
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/b;->f:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 1039
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/b;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1040
    goto :goto_0

    .line 1042
    :cond_9
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/b;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/b;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1043
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1050
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/b;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1053
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/b;->b:I

    add-int/2addr v0, v2

    .line 1054
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1056
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/b;->d:I

    add-int/2addr v0, v2

    .line 1057
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/b;->e:I

    add-int/2addr v0, v2

    .line 1058
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/b;->f:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1060
    return v0

    .line 1050
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/b;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1058
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/b;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 955
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/b;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/aa/b/a/a/a/a/b;->b:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/b;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/aa/b/a/a/a/a/b;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/b;->f:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1066
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/b;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1067
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/b;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1069
    :cond_0
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/b;->b:I

    if-eqz v0, :cond_1

    .line 1070
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/b;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1072
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 1073
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 1074
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/b;->c:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 1075
    if-eqz v1, :cond_2

    .line 1076
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1073
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1080
    :cond_3
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/b;->d:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_4

    .line 1081
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/b;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1083
    :cond_4
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/b;->e:I

    if-eqz v0, :cond_5

    .line 1084
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/b;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1086
    :cond_5
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/b;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1087
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/b;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1089
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1090
    return-void
.end method
