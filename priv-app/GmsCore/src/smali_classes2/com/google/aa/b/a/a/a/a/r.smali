.class public final Lcom/google/aa/b/a/a/a/a/r;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/aa/b/a/a/a/a/r;


# instance fields
.field public a:I

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 515
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 516
    const/16 v0, 0xfa

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/r;->a:I

    const/16 v0, 0x3ca

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/r;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/r;->cachedSize:I

    .line 517
    return-void
.end method

.method public static a()[Lcom/google/aa/b/a/a/a/a/r;
    .locals 2

    .prologue
    .line 498
    sget-object v0, Lcom/google/aa/b/a/a/a/a/r;->c:[Lcom/google/aa/b/a/a/a/a/r;

    if-nez v0, :cond_1

    .line 499
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 501
    :try_start_0
    sget-object v0, Lcom/google/aa/b/a/a/a/a/r;->c:[Lcom/google/aa/b/a/a/a/a/r;

    if-nez v0, :cond_0

    .line 502
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/aa/b/a/a/a/a/r;

    sput-object v0, Lcom/google/aa/b/a/a/a/a/r;->c:[Lcom/google/aa/b/a/a/a/a/r;

    .line 504
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 506
    :cond_1
    sget-object v0, Lcom/google/aa/b/a/a/a/a/r;->c:[Lcom/google/aa/b/a/a/a/a/r;

    return-object v0

    .line 504
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 566
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 567
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/r;->a:I

    const/16 v2, 0xfa

    if-eq v1, v2, :cond_0

    .line 568
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/r;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 571
    :cond_0
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/r;->b:I

    const/16 v2, 0x3ca

    if-eq v1, v2, :cond_1

    .line 572
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/r;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 528
    if-ne p1, p0, :cond_1

    .line 541
    :cond_0
    :goto_0
    return v0

    .line 531
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/r;

    if-nez v2, :cond_2

    move v0, v1

    .line 532
    goto :goto_0

    .line 534
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/r;

    .line 535
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/r;->a:I

    iget v3, p1, Lcom/google/aa/b/a/a/a/a/r;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 536
    goto :goto_0

    .line 538
    :cond_3
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/r;->b:I

    iget v3, p1, Lcom/google/aa/b/a/a/a/a/r;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 539
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 546
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/r;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 548
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/r;->b:I

    add-int/2addr v0, v1

    .line 549
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 476
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/aa/b/a/a/a/a/r;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    iput v0, p0, Lcom/google/aa/b/a/a/a/a/r;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0xfa
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x3ca
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 555
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/r;->a:I

    const/16 v1, 0xfa

    if-eq v0, v1, :cond_0

    .line 556
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/r;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 558
    :cond_0
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/r;->b:I

    const/16 v1, 0x3ca

    if-eq v0, v1, :cond_1

    .line 559
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/r;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 561
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 562
    return-void
.end method
