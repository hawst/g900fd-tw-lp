.class public final Lcom/google/aa/b/a/h;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile j:[Lcom/google/aa/b/a/h;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 515
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 516
    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/h;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/h;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/h;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/h;->f:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/h;->g:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/h;->h:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/h;->cachedSize:I

    .line 517
    return-void
.end method

.method public static a()[Lcom/google/aa/b/a/h;
    .locals 2

    .prologue
    .line 477
    sget-object v0, Lcom/google/aa/b/a/h;->j:[Lcom/google/aa/b/a/h;

    if-nez v0, :cond_1

    .line 478
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 480
    :try_start_0
    sget-object v0, Lcom/google/aa/b/a/h;->j:[Lcom/google/aa/b/a/h;

    if-nez v0, :cond_0

    .line 481
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/aa/b/a/h;

    sput-object v0, Lcom/google/aa/b/a/h;->j:[Lcom/google/aa/b/a/h;

    .line 483
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 485
    :cond_1
    sget-object v0, Lcom/google/aa/b/a/h;->j:[Lcom/google/aa/b/a/h;

    return-object v0

    .line 483
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 667
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 668
    iget-object v1, p0, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 669
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 672
    :cond_0
    iget-object v1, p0, Lcom/google/aa/b/a/h;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 673
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/aa/b/a/h;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 676
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/h;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 677
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/aa/b/a/h;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 680
    :cond_2
    iget-object v1, p0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 681
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 684
    :cond_3
    iget-object v1, p0, Lcom/google/aa/b/a/h;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 685
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/aa/b/a/h;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 688
    :cond_4
    iget-object v1, p0, Lcom/google/aa/b/a/h;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 689
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/aa/b/a/h;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 692
    :cond_5
    iget-object v1, p0, Lcom/google/aa/b/a/h;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 693
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/aa/b/a/h;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 696
    :cond_6
    iget-object v1, p0, Lcom/google/aa/b/a/h;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 697
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/aa/b/a/h;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 700
    :cond_7
    iget-object v1, p0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    .line 701
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 704
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 535
    if-ne p1, p0, :cond_1

    .line 605
    :cond_0
    :goto_0
    return v0

    .line 538
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/h;

    if-nez v2, :cond_2

    move v0, v1

    .line 539
    goto :goto_0

    .line 541
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/h;

    .line 542
    iget-object v2, p0, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 543
    iget-object v2, p1, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 544
    goto :goto_0

    .line 546
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 547
    goto :goto_0

    .line 549
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/h;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 550
    iget-object v2, p1, Lcom/google/aa/b/a/h;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 551
    goto :goto_0

    .line 553
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/h;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/h;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 554
    goto :goto_0

    .line 556
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/h;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 557
    iget-object v2, p1, Lcom/google/aa/b/a/h;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 558
    goto :goto_0

    .line 560
    :cond_7
    iget-object v2, p0, Lcom/google/aa/b/a/h;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/h;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 561
    goto :goto_0

    .line 563
    :cond_8
    iget-object v2, p0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 564
    iget-object v2, p1, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 565
    goto :goto_0

    .line 567
    :cond_9
    iget-object v2, p0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 568
    goto :goto_0

    .line 570
    :cond_a
    iget-object v2, p0, Lcom/google/aa/b/a/h;->e:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 571
    iget-object v2, p1, Lcom/google/aa/b/a/h;->e:Ljava/lang/String;

    if-eqz v2, :cond_c

    move v0, v1

    .line 572
    goto :goto_0

    .line 574
    :cond_b
    iget-object v2, p0, Lcom/google/aa/b/a/h;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/h;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 575
    goto :goto_0

    .line 577
    :cond_c
    iget-object v2, p0, Lcom/google/aa/b/a/h;->f:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 578
    iget-object v2, p1, Lcom/google/aa/b/a/h;->f:Ljava/lang/String;

    if-eqz v2, :cond_e

    move v0, v1

    .line 579
    goto :goto_0

    .line 581
    :cond_d
    iget-object v2, p0, Lcom/google/aa/b/a/h;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/h;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 582
    goto/16 :goto_0

    .line 584
    :cond_e
    iget-object v2, p0, Lcom/google/aa/b/a/h;->g:Ljava/lang/String;

    if-nez v2, :cond_f

    .line 585
    iget-object v2, p1, Lcom/google/aa/b/a/h;->g:Ljava/lang/String;

    if-eqz v2, :cond_10

    move v0, v1

    .line 586
    goto/16 :goto_0

    .line 588
    :cond_f
    iget-object v2, p0, Lcom/google/aa/b/a/h;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/h;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    move v0, v1

    .line 589
    goto/16 :goto_0

    .line 591
    :cond_10
    iget-object v2, p0, Lcom/google/aa/b/a/h;->h:Ljava/lang/String;

    if-nez v2, :cond_11

    .line 592
    iget-object v2, p1, Lcom/google/aa/b/a/h;->h:Ljava/lang/String;

    if-eqz v2, :cond_12

    move v0, v1

    .line 593
    goto/16 :goto_0

    .line 595
    :cond_11
    iget-object v2, p0, Lcom/google/aa/b/a/h;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/h;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    .line 596
    goto/16 :goto_0

    .line 598
    :cond_12
    iget-object v2, p0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    if-nez v2, :cond_13

    .line 599
    iget-object v2, p1, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 600
    goto/16 :goto_0

    .line 602
    :cond_13
    iget-object v2, p0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 603
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 610
    iget-object v0, p0, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 613
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/h;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 615
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/h;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 617
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 619
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/h;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 621
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/h;->f:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 623
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/h;->g:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 625
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/h;->h:Ljava/lang/String;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 627
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    if-nez v2, :cond_8

    :goto_8
    add-int/2addr v0, v1

    .line 629
    return v0

    .line 610
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 613
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/h;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 615
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/h;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 617
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 619
    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/h;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 621
    :cond_5
    iget-object v0, p0, Lcom/google/aa/b/a/h;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 623
    :cond_6
    iget-object v0, p0, Lcom/google/aa/b/a/h;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 625
    :cond_7
    iget-object v0, p0, Lcom/google/aa/b/a/h;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_7

    .line 627
    :cond_8
    iget-object v1, p0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_8
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 471
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/h;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/h;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/h;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/h;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/h;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/h;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 635
    iget-object v0, p0, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 636
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/aa/b/a/h;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 638
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/h;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 639
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/aa/b/a/h;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 641
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/h;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 642
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/aa/b/a/h;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 644
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 645
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/aa/b/a/h;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 647
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/h;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 648
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/aa/b/a/h;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 650
    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/h;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 651
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/aa/b/a/h;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 653
    :cond_5
    iget-object v0, p0, Lcom/google/aa/b/a/h;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 654
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/aa/b/a/h;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 656
    :cond_6
    iget-object v0, p0, Lcom/google/aa/b/a/h;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 657
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/aa/b/a/h;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 659
    :cond_7
    iget-object v0, p0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 660
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/aa/b/a/h;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 662
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 663
    return-void
.end method
