.class public final Lcom/google/aa/b/a/a/a/a/e;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/aa/b/a/a/a/a/q;

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6197
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6198
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/e;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/e;->cachedSize:I

    .line 6199
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 6255
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 6256
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    if-eqz v1, :cond_0

    .line 6257
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6260
    :cond_0
    iget v1, p0, Lcom/google/aa/b/a/a/a/a/e;->b:I

    if-eqz v1, :cond_1

    .line 6261
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/e;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6264
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6210
    if-ne p1, p0, :cond_1

    .line 6229
    :cond_0
    :goto_0
    return v0

    .line 6213
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/e;

    if-nez v2, :cond_2

    move v0, v1

    .line 6214
    goto :goto_0

    .line 6216
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/e;

    .line 6217
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    if-nez v2, :cond_3

    .line 6218
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    if-eqz v2, :cond_4

    move v0, v1

    .line 6219
    goto :goto_0

    .line 6222
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/a/a/a/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 6223
    goto :goto_0

    .line 6226
    :cond_4
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/e;->b:I

    iget v3, p1, Lcom/google/aa/b/a/a/a/a/e;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 6227
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 6234
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 6237
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/e;->b:I

    add-int/2addr v0, v1

    .line 6238
    return v0

    .line 6234
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    invoke-virtual {v0}, Lcom/google/aa/b/a/a/a/a/q;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 6169
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/aa/b/a/a/a/a/q;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/q;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/aa/b/a/a/a/a/e;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x18 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 6244
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    if-eqz v0, :cond_0

    .line 6245
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/e;->a:Lcom/google/aa/b/a/a/a/a/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 6247
    :cond_0
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/e;->b:I

    if-eqz v0, :cond_1

    .line 6248
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/aa/b/a/a/a/a/e;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 6250
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 6251
    return-void
.end method
