.class public final Lcom/google/aa/b/a/a/a/a/p;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[I

.field public b:Ljava/lang/String;

.field public c:[Lcom/google/aa/b/a/a/a/a/w;

.field public d:Ljava/lang/String;

.field public e:[Lcom/google/checkout/inapp/proto/a/b;

.field public f:Ljava/lang/String;

.field public g:[Lcom/google/aa/b/a/a/a/a/s;

.field public h:Ljava/lang/String;

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:[Lcom/google/aa/b/a/h;

.field public m:[Lcom/google/aa/b/a/i;

.field public n:Lcom/google/checkout/inapp/proto/a/d;

.field public o:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4027
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4028
    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->b:Ljava/lang/String;

    invoke-static {}, Lcom/google/aa/b/a/a/a/a/w;->a()[Lcom/google/aa/b/a/a/a/a/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->d:Ljava/lang/String;

    invoke-static {}, Lcom/google/checkout/inapp/proto/a/b;->a()[Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->f:Ljava/lang/String;

    invoke-static {}, Lcom/google/aa/b/a/a/a/a/s;->a()[Lcom/google/aa/b/a/a/a/a/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/p;->i:Z

    iput-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/p;->j:Z

    iput-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/p;->k:Z

    invoke-static {}, Lcom/google/aa/b/a/h;->a()[Lcom/google/aa/b/a/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    invoke-static {}, Lcom/google/aa/b/a/i;->a()[Lcom/google/aa/b/a/i;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    iput-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/p;->o:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/p;->cachedSize:I

    .line 4029
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 4248
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v3

    .line 4249
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    if-eqz v0, :cond_18

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    array-length v0, v0

    if-lez v0, :cond_18

    move v0, v1

    move v2, v1

    .line 4251
    :goto_0
    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 4252
    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    aget v4, v4, v0

    .line 4253
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v4

    add-int/2addr v2, v4

    .line 4251
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4256
    :cond_0
    add-int v0, v3, v2

    .line 4257
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4259
    :goto_1
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 4260
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4263
    :cond_1
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    .line 4264
    :goto_2
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 4265
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    aget-object v3, v3, v0

    .line 4266
    if-eqz v3, :cond_2

    .line 4267
    const/4 v4, 0x3

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4264
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    move v0, v2

    .line 4272
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 4273
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4276
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v2, v2

    if-lez v2, :cond_8

    move v2, v0

    move v0, v1

    .line 4277
    :goto_3
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v3, v3

    if-ge v0, v3, :cond_7

    .line 4278
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v3, v3, v0

    .line 4279
    if-eqz v3, :cond_6

    .line 4280
    const/4 v4, 0x5

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4277
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    move v0, v2

    .line 4285
    :cond_8
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 4286
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4289
    :cond_9
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    array-length v2, v2

    if-lez v2, :cond_c

    move v2, v0

    move v0, v1

    .line 4290
    :goto_4
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    array-length v3, v3

    if-ge v0, v3, :cond_b

    .line 4291
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    aget-object v3, v3, v0

    .line 4292
    if-eqz v3, :cond_a

    .line 4293
    const/4 v4, 0x7

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4290
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_b
    move v0, v2

    .line 4298
    :cond_c
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    .line 4299
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4302
    :cond_d
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/p;->i:Z

    if-eqz v2, :cond_e

    .line 4303
    const/16 v2, 0xb

    iget-boolean v3, p0, Lcom/google/aa/b/a/a/a/a/p;->i:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4306
    :cond_e
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/p;->j:Z

    if-eqz v2, :cond_f

    .line 4307
    const/16 v2, 0xc

    iget-boolean v3, p0, Lcom/google/aa/b/a/a/a/a/p;->j:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4310
    :cond_f
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/p;->k:Z

    if-eqz v2, :cond_10

    .line 4311
    const/16 v2, 0xd

    iget-boolean v3, p0, Lcom/google/aa/b/a/a/a/a/p;->k:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 4314
    :cond_10
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    array-length v2, v2

    if-lez v2, :cond_13

    move v2, v0

    move v0, v1

    .line 4315
    :goto_5
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    array-length v3, v3

    if-ge v0, v3, :cond_12

    .line 4316
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    aget-object v3, v3, v0

    .line 4317
    if-eqz v3, :cond_11

    .line 4318
    const/16 v4, 0xe

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4315
    :cond_11
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_12
    move v0, v2

    .line 4323
    :cond_13
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    array-length v2, v2

    if-lez v2, :cond_15

    .line 4324
    :goto_6
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    array-length v2, v2

    if-ge v1, v2, :cond_15

    .line 4325
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    aget-object v2, v2, v1

    .line 4326
    if-eqz v2, :cond_14

    .line 4327
    const/16 v3, 0xf

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4324
    :cond_14
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 4332
    :cond_15
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v1, :cond_16

    .line 4333
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4336
    :cond_16
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/p;->o:Z

    if-eqz v1, :cond_17

    .line 4337
    const/16 v1, 0x11

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/p;->o:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4340
    :cond_17
    return v0

    :cond_18
    move v0, v3

    goto/16 :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4053
    if-ne p1, p0, :cond_1

    .line 4133
    :cond_0
    :goto_0
    return v0

    .line 4056
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/p;

    if-nez v2, :cond_2

    move v0, v1

    .line 4057
    goto :goto_0

    .line 4059
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/p;

    .line 4060
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 4062
    goto :goto_0

    .line 4064
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 4065
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/p;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 4066
    goto :goto_0

    .line 4068
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/p;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 4069
    goto :goto_0

    .line 4071
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 4073
    goto :goto_0

    .line 4075
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->d:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 4076
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/p;->d:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 4077
    goto :goto_0

    .line 4079
    :cond_7
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/p;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 4080
    goto :goto_0

    .line 4082
    :cond_8
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 4084
    goto :goto_0

    .line 4086
    :cond_9
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->f:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 4087
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/p;->f:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 4088
    goto :goto_0

    .line 4090
    :cond_a
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/p;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 4091
    goto :goto_0

    .line 4093
    :cond_b
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 4095
    goto :goto_0

    .line 4097
    :cond_c
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 4098
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    if-eqz v2, :cond_e

    move v0, v1

    .line 4099
    goto/16 :goto_0

    .line 4101
    :cond_d
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 4102
    goto/16 :goto_0

    .line 4104
    :cond_e
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/p;->i:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/p;->i:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 4105
    goto/16 :goto_0

    .line 4107
    :cond_f
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/p;->j:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/p;->j:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 4108
    goto/16 :goto_0

    .line 4110
    :cond_10
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/p;->k:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/p;->k:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 4111
    goto/16 :goto_0

    .line 4113
    :cond_11
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_12

    move v0, v1

    .line 4115
    goto/16 :goto_0

    .line 4117
    :cond_12
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    move v0, v1

    .line 4119
    goto/16 :goto_0

    .line 4121
    :cond_13
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v2, :cond_14

    .line 4122
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v2, :cond_15

    move v0, v1

    .line 4123
    goto/16 :goto_0

    .line 4126
    :cond_14
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_15

    move v0, v1

    .line 4127
    goto/16 :goto_0

    .line 4130
    :cond_15
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/p;->o:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/p;->o:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 4131
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 4138
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 4141
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v4

    .line 4143
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    invoke-static {v4}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4145
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 4147
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v4}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4149
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->f:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 4151
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    invoke-static {v4}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4153
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    .line 4155
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/p;->i:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v4

    .line 4156
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/p;->j:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    add-int/2addr v0, v4

    .line 4157
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/p;->k:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_6
    add-int/2addr v0, v4

    .line 4158
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    invoke-static {v4}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4160
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    invoke-static {v4}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v4

    add-int/2addr v0, v4

    .line 4162
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v4, :cond_7

    :goto_7
    add-int/2addr v0, v1

    .line 4164
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/p;->o:Z

    if-eqz v1, :cond_8

    :goto_8
    add-int/2addr v0, v2

    .line 4165
    return v0

    .line 4141
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 4145
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 4149
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 4153
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    move v0, v3

    .line 4155
    goto :goto_4

    :cond_5
    move v0, v3

    .line 4156
    goto :goto_5

    :cond_6
    move v0, v3

    .line 4157
    goto :goto_6

    .line 4162
    :cond_7
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v1}, Lcom/google/checkout/inapp/proto/a/d;->hashCode()I

    move-result v1

    goto :goto_7

    :cond_8
    move v2, v3

    .line 4164
    goto :goto_8
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 3965
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    :pswitch_2
    goto :goto_4

    :pswitch_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    :pswitch_4
    goto :goto_6

    :pswitch_5
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    :cond_a
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->b:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    if-nez v0, :cond_c

    move v0, v2

    :goto_7
    add-int/2addr v1, v0

    new-array v1, v1, [Lcom/google/aa/b/a/a/a/a/w;

    if-eqz v0, :cond_b

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_8
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    new-instance v3, Lcom/google/aa/b/a/a/a/a/w;

    invoke-direct {v3}, Lcom/google/aa/b/a/a/a/a/w;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_c
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    array-length v0, v0

    goto :goto_7

    :cond_d
    new-instance v3, Lcom/google/aa/b/a/a/a/a/w;

    invoke-direct {v3}, Lcom/google/aa/b/a/a/a/a/w;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->d:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_f

    move v0, v2

    :goto_9
    add-int/2addr v1, v0

    new-array v1, v1, [Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_e

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_e
    :goto_a
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_10

    new-instance v3, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_f
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v0, v0

    goto :goto_9

    :cond_10
    new-instance v3, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->f:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    if-nez v0, :cond_12

    move v0, v2

    :goto_b
    add-int/2addr v1, v0

    new-array v1, v1, [Lcom/google/aa/b/a/a/a/a/s;

    if-eqz v0, :cond_11

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_11
    :goto_c
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_13

    new-instance v3, Lcom/google/aa/b/a/a/a/a/s;

    invoke-direct {v3}, Lcom/google/aa/b/a/a/a/a/s;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    :cond_12
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    array-length v0, v0

    goto :goto_b

    :cond_13
    new-instance v3, Lcom/google/aa/b/a/a/a/a/s;

    invoke-direct {v3}, Lcom/google/aa/b/a/a/a/a/s;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/p;->i:Z

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/p;->j:Z

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/p;->k:Z

    goto/16 :goto_0

    :sswitch_d
    const/16 v0, 0x72

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    if-nez v0, :cond_15

    move v0, v2

    :goto_d
    add-int/2addr v1, v0

    new-array v1, v1, [Lcom/google/aa/b/a/h;

    if-eqz v0, :cond_14

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_14
    :goto_e
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_16

    new-instance v3, Lcom/google/aa/b/a/h;

    invoke-direct {v3}, Lcom/google/aa/b/a/h;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    :cond_15
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    array-length v0, v0

    goto :goto_d

    :cond_16
    new-instance v3, Lcom/google/aa/b/a/h;

    invoke-direct {v3}, Lcom/google/aa/b/a/h;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    goto/16 :goto_0

    :sswitch_e
    const/16 v0, 0x7a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    if-nez v0, :cond_18

    move v0, v2

    :goto_f
    add-int/2addr v1, v0

    new-array v1, v1, [Lcom/google/aa/b/a/i;

    if-eqz v0, :cond_17

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_17
    :goto_10
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_19

    new-instance v3, Lcom/google/aa/b/a/i;

    invoke-direct {v3}, Lcom/google/aa/b/a/i;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    :cond_18
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    array-length v0, v0

    goto :goto_f

    :cond_19
    new-instance v3, Lcom/google/aa/b/a/i;

    invoke-direct {v3}, Lcom/google/aa/b/a/i;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_1a

    new-instance v0, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    :cond_1a
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/p;->o:Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
        0x2a -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0x4a -> :sswitch_9
        0x58 -> :sswitch_a
        0x60 -> :sswitch_b
        0x68 -> :sswitch_c
        0x72 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x88 -> :sswitch_10
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4171
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    move v0, v1

    .line 4172
    :goto_0
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 4173
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/p;->a:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4172
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4176
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4177
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4179
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 4180
    :goto_1
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 4181
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->c:[Lcom/google/aa/b/a/a/a/a/w;

    aget-object v2, v2, v0

    .line 4182
    if-eqz v2, :cond_2

    .line 4183
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4180
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 4187
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 4188
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4190
    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    .line 4191
    :goto_2
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v2, v2

    if-ge v0, v2, :cond_6

    .line 4192
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->e:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v2, v2, v0

    .line 4193
    if-eqz v2, :cond_5

    .line 4194
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4191
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 4198
    :cond_6
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 4199
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4201
    :cond_7
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    array-length v0, v0

    if-lez v0, :cond_9

    move v0, v1

    .line 4202
    :goto_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    array-length v2, v2

    if-ge v0, v2, :cond_9

    .line 4203
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->g:[Lcom/google/aa/b/a/a/a/a/s;

    aget-object v2, v2, v0

    .line 4204
    if-eqz v2, :cond_8

    .line 4205
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4202
    :cond_8
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 4209
    :cond_9
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_a

    .line 4210
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4212
    :cond_a
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/p;->i:Z

    if-eqz v0, :cond_b

    .line 4213
    const/16 v0, 0xb

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/p;->i:Z

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 4215
    :cond_b
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/p;->j:Z

    if-eqz v0, :cond_c

    .line 4216
    const/16 v0, 0xc

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/p;->j:Z

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 4218
    :cond_c
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/p;->k:Z

    if-eqz v0, :cond_d

    .line 4219
    const/16 v0, 0xd

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/p;->k:Z

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 4221
    :cond_d
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    array-length v0, v0

    if-lez v0, :cond_f

    move v0, v1

    .line 4222
    :goto_4
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    array-length v2, v2

    if-ge v0, v2, :cond_f

    .line 4223
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/p;->l:[Lcom/google/aa/b/a/h;

    aget-object v2, v2, v0

    .line 4224
    if-eqz v2, :cond_e

    .line 4225
    const/16 v3, 0xe

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4222
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 4229
    :cond_f
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    array-length v0, v0

    if-lez v0, :cond_11

    .line 4230
    :goto_5
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    array-length v0, v0

    if-ge v1, v0, :cond_11

    .line 4231
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->m:[Lcom/google/aa/b/a/i;

    aget-object v0, v0, v1

    .line 4232
    if-eqz v0, :cond_10

    .line 4233
    const/16 v2, 0xf

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4230
    :cond_10
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 4237
    :cond_11
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_12

    .line 4238
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/p;->n:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4240
    :cond_12
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/p;->o:Z

    if-eqz v0, :cond_13

    .line 4241
    const/16 v0, 0x11

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/p;->o:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 4243
    :cond_13
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4244
    return-void
.end method
