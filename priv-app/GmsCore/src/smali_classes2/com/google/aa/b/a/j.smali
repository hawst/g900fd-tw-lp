.class public final Lcom/google/aa/b/a/j;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Lcom/google/aa/b/a/k;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 320
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 321
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/aa/b/a/j;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/j;->b:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/j;->cachedSize:I

    .line 322
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 391
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 392
    iget v1, p0, Lcom/google/aa/b/a/j;->a:I

    if-eqz v1, :cond_0

    .line 393
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/aa/b/a/j;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 396
    :cond_0
    iget-object v1, p0, Lcom/google/aa/b/a/j;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 397
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/aa/b/a/j;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 400
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    if-eqz v1, :cond_2

    .line 401
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 404
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 334
    if-ne p1, p0, :cond_1

    .line 360
    :cond_0
    :goto_0
    return v0

    .line 337
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/j;

    if-nez v2, :cond_2

    move v0, v1

    .line 338
    goto :goto_0

    .line 340
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/j;

    .line 341
    iget v2, p0, Lcom/google/aa/b/a/j;->a:I

    iget v3, p1, Lcom/google/aa/b/a/j;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 342
    goto :goto_0

    .line 344
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/j;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 345
    iget-object v2, p1, Lcom/google/aa/b/a/j;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 346
    goto :goto_0

    .line 348
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/j;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/j;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 349
    goto :goto_0

    .line 351
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    if-nez v2, :cond_6

    .line 352
    iget-object v2, p1, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    if-eqz v2, :cond_0

    move v0, v1

    .line 353
    goto :goto_0

    .line 356
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    iget-object v3, p1, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/k;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 357
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 365
    iget v0, p0, Lcom/google/aa/b/a/j;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 367
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/j;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 369
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 371
    return v0

    .line 367
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/j;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 369
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    invoke-virtual {v1}, Lcom/google/aa/b/a/k;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 143
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    iput v0, p0, Lcom/google/aa/b/a/j;->a:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/j;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/aa/b/a/k;

    invoke-direct {v0}, Lcom/google/aa/b/a/k;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0xb -> :sswitch_2
        0xc -> :sswitch_2
        0x15 -> :sswitch_2
        0x16 -> :sswitch_2
        0x1f -> :sswitch_2
        0x20 -> :sswitch_2
        0x29 -> :sswitch_2
        0x2a -> :sswitch_2
        0x2b -> :sswitch_2
        0x2c -> :sswitch_2
        0x33 -> :sswitch_2
        0x3d -> :sswitch_2
        0x3e -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 377
    iget v0, p0, Lcom/google/aa/b/a/j;->a:I

    if-eqz v0, :cond_0

    .line 378
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/aa/b/a/j;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 380
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/j;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 381
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/aa/b/a/j;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 383
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    if-eqz v0, :cond_2

    .line 384
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/aa/b/a/j;->c:Lcom/google/aa/b/a/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 386
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 387
    return-void
.end method
