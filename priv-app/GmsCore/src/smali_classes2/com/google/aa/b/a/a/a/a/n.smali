.class public final Lcom/google/aa/b/a/a/a/a/n;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[I

.field public b:Lcom/google/aa/b/a/a/a/a/v;

.field public c:Lcom/google/aa/b/a/a/a/a/p;

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2528
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2529
    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/n;->d:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/n;->cachedSize:I

    .line 2530
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 2613
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v2

    .line 2614
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    array-length v1, v1

    if-lez v1, :cond_4

    move v1, v0

    .line 2616
    :goto_0
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 2617
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    aget v3, v3, v0

    .line 2618
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 2616
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2621
    :cond_0
    add-int v0, v2, v1

    .line 2622
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2624
    :goto_1
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    if-eqz v1, :cond_1

    .line 2625
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2628
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v1, :cond_2

    .line 2629
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2632
    :cond_2
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/n;->d:Z

    if-eqz v1, :cond_3

    .line 2633
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/n;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2636
    :cond_3
    return v0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2543
    if-ne p1, p0, :cond_1

    .line 2575
    :cond_0
    :goto_0
    return v0

    .line 2546
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/n;

    if-nez v2, :cond_2

    move v0, v1

    .line 2547
    goto :goto_0

    .line 2549
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/n;

    .line 2550
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 2552
    goto :goto_0

    .line 2554
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    if-nez v2, :cond_4

    .line 2555
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    if-eqz v2, :cond_5

    move v0, v1

    .line 2556
    goto :goto_0

    .line 2559
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/a/a/a/v;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 2560
    goto :goto_0

    .line 2563
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    if-nez v2, :cond_6

    .line 2564
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v2, :cond_7

    move v0, v1

    .line 2565
    goto :goto_0

    .line 2568
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/a/a/a/p;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 2569
    goto :goto_0

    .line 2572
    :cond_7
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/n;->d:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/n;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 2573
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2580
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 2583
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 2585
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 2587
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/n;->d:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x4cf

    :goto_2
    add-int/2addr v0, v1

    .line 2588
    return v0

    .line 2583
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    invoke-virtual {v0}, Lcom/google/aa/b/a/a/a/a/v;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2585
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    invoke-virtual {v1}, Lcom/google/aa/b/a/a/a/a/p;->hashCode()I

    move-result v1

    goto :goto_1

    .line 2587
    :cond_2
    const/16 v0, 0x4d5

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2499
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    :pswitch_2
    goto :goto_4

    :pswitch_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    :pswitch_4
    goto :goto_6

    :pswitch_5
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    :cond_a
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/aa/b/a/a/a/a/v;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/v;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    :cond_b
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    if-nez v0, :cond_c

    new-instance v0, Lcom/google/aa/b/a/a/a/a/p;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/p;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    :cond_c
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/n;->d:Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x32 -> :sswitch_4
        0x40 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 2594
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 2595
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 2596
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/n;->a:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2595
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2599
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    if-eqz v0, :cond_1

    .line 2600
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/n;->b:Lcom/google/aa/b/a/a/a/a/v;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2602
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v0, :cond_2

    .line 2603
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/n;->c:Lcom/google/aa/b/a/a/a/a/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2605
    :cond_2
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/n;->d:Z

    if-eqz v0, :cond_3

    .line 2606
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/n;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2608
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2609
    return-void
.end method
