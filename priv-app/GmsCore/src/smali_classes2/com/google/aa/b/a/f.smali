.class public final Lcom/google/aa/b/a/f;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile g:[Lcom/google/aa/b/a/f;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:I

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 958
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 959
    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/f;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/f;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/f;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/f;->d:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/aa/b/a/f;->e:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/f;->f:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/f;->cachedSize:I

    .line 960
    return-void
.end method

.method public static a()[Lcom/google/aa/b/a/f;
    .locals 2

    .prologue
    .line 929
    sget-object v0, Lcom/google/aa/b/a/f;->g:[Lcom/google/aa/b/a/f;

    if-nez v0, :cond_1

    .line 930
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 932
    :try_start_0
    sget-object v0, Lcom/google/aa/b/a/f;->g:[Lcom/google/aa/b/a/f;

    if-nez v0, :cond_0

    .line 933
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/aa/b/a/f;

    sput-object v0, Lcom/google/aa/b/a/f;->g:[Lcom/google/aa/b/a/f;

    .line 935
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 937
    :cond_1
    sget-object v0, Lcom/google/aa/b/a/f;->g:[Lcom/google/aa/b/a/f;

    return-object v0

    .line 935
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1066
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1067
    iget-object v1, p0, Lcom/google/aa/b/a/f;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1068
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/aa/b/a/f;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1071
    :cond_0
    iget-object v1, p0, Lcom/google/aa/b/a/f;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1072
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/aa/b/a/f;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1075
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/f;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1076
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/aa/b/a/f;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1079
    :cond_2
    iget-object v1, p0, Lcom/google/aa/b/a/f;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1080
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/aa/b/a/f;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1083
    :cond_3
    iget v1, p0, Lcom/google/aa/b/a/f;->e:I

    if-eqz v1, :cond_4

    .line 1084
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/aa/b/a/f;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1087
    :cond_4
    iget-object v1, p0, Lcom/google/aa/b/a/f;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1088
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/aa/b/a/f;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1091
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 975
    if-ne p1, p0, :cond_1

    .line 1020
    :cond_0
    :goto_0
    return v0

    .line 978
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/f;

    if-nez v2, :cond_2

    move v0, v1

    .line 979
    goto :goto_0

    .line 981
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/f;

    .line 982
    iget-object v2, p0, Lcom/google/aa/b/a/f;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 983
    iget-object v2, p1, Lcom/google/aa/b/a/f;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 984
    goto :goto_0

    .line 986
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/f;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 987
    goto :goto_0

    .line 989
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/f;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 990
    iget-object v2, p1, Lcom/google/aa/b/a/f;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 991
    goto :goto_0

    .line 993
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/f;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/f;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 994
    goto :goto_0

    .line 996
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/f;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 997
    iget-object v2, p1, Lcom/google/aa/b/a/f;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 998
    goto :goto_0

    .line 1000
    :cond_7
    iget-object v2, p0, Lcom/google/aa/b/a/f;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/f;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1001
    goto :goto_0

    .line 1003
    :cond_8
    iget-object v2, p0, Lcom/google/aa/b/a/f;->d:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 1004
    iget-object v2, p1, Lcom/google/aa/b/a/f;->d:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 1005
    goto :goto_0

    .line 1007
    :cond_9
    iget-object v2, p0, Lcom/google/aa/b/a/f;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/f;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 1008
    goto :goto_0

    .line 1010
    :cond_a
    iget v2, p0, Lcom/google/aa/b/a/f;->e:I

    iget v3, p1, Lcom/google/aa/b/a/f;->e:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 1011
    goto :goto_0

    .line 1013
    :cond_b
    iget-object v2, p0, Lcom/google/aa/b/a/f;->f:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 1014
    iget-object v2, p1, Lcom/google/aa/b/a/f;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1015
    goto :goto_0

    .line 1017
    :cond_c
    iget-object v2, p0, Lcom/google/aa/b/a/f;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/f;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1018
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1025
    iget-object v0, p0, Lcom/google/aa/b/a/f;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1028
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/f;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1030
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/f;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1032
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/f;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 1034
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/aa/b/a/f;->e:I

    add-int/2addr v0, v2

    .line 1035
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/f;->f:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 1037
    return v0

    .line 1025
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/f;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1028
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/f;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1030
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/f;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1032
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/f;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 1035
    :cond_4
    iget-object v1, p0, Lcom/google/aa/b/a/f;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 918
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/f;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/f;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/f;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/f;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/aa/b/a/f;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/f;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1043
    iget-object v0, p0, Lcom/google/aa/b/a/f;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1044
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/aa/b/a/f;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1046
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/f;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1047
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/aa/b/a/f;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1049
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/f;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1050
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/aa/b/a/f;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1052
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/f;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1053
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/aa/b/a/f;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1055
    :cond_3
    iget v0, p0, Lcom/google/aa/b/a/f;->e:I

    if-eqz v0, :cond_4

    .line 1056
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/aa/b/a/f;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1058
    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/f;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1059
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/aa/b/a/f;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1061
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1062
    return-void
.end method
