.class public final Lcom/google/aa/b/a/a/a/a/g;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/aa/b/a/a/a/a/b;

.field public b:Z

.field public c:[Lcom/google/j/a/a/a/j;

.field public d:[Lcom/google/j/a/a/a/p;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6599
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6600
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/g;->b:Z

    invoke-static {}, Lcom/google/j/a/a/a/j;->a()[Lcom/google/j/a/a/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    invoke-static {}, Lcom/google/j/a/a/a/p;->a()[Lcom/google/j/a/a/a/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/g;->cachedSize:I

    .line 6601
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 6687
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 6688
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    if-eqz v2, :cond_0

    .line 6689
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6692
    :cond_0
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/g;->b:Z

    if-eqz v2, :cond_1

    .line 6693
    const/4 v2, 0x3

    iget-boolean v3, p0, Lcom/google/aa/b/a/a/a/a/g;->b:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 6696
    :cond_1
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    .line 6697
    :goto_0
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 6698
    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    aget-object v3, v3, v0

    .line 6699
    if-eqz v3, :cond_2

    .line 6700
    const/4 v4, 0x4

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 6697
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 6705
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 6706
    :goto_1
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 6707
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    aget-object v2, v2, v1

    .line 6708
    if-eqz v2, :cond_5

    .line 6709
    const/4 v3, 0x5

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6706
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6714
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6614
    if-ne p1, p0, :cond_1

    .line 6641
    :cond_0
    :goto_0
    return v0

    .line 6617
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/g;

    if-nez v2, :cond_2

    move v0, v1

    .line 6618
    goto :goto_0

    .line 6620
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/g;

    .line 6621
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    if-nez v2, :cond_3

    .line 6622
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    if-eqz v2, :cond_4

    move v0, v1

    .line 6623
    goto :goto_0

    .line 6626
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/a/a/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 6627
    goto :goto_0

    .line 6630
    :cond_4
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/g;->b:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/g;->b:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 6631
    goto :goto_0

    .line 6633
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 6635
    goto :goto_0

    .line 6637
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 6639
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 6646
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 6649
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/g;->b:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v1

    .line 6650
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6652
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6654
    return v0

    .line 6646
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    invoke-virtual {v0}, Lcom/google/aa/b/a/a/a/a/b;->hashCode()I

    move-result v0

    goto :goto_0

    .line 6649
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6570
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/aa/b/a/a/a/a/b;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/g;->b:Z

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/j/a/a/a/j;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/j/a/a/a/j;

    invoke-direct {v3}, Lcom/google/j/a/a/a/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/j/a/a/a/j;

    invoke-direct {v3}, Lcom/google/j/a/a/a/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/j/a/a/a/p;

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Lcom/google/j/a/a/a/p;

    invoke-direct {v3}, Lcom/google/j/a/a/a/p;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    array-length v0, v0

    goto :goto_3

    :cond_7
    new-instance v3, Lcom/google/j/a/a/a/p;

    invoke-direct {v3}, Lcom/google/j/a/a/a/p;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6660
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    if-eqz v0, :cond_0

    .line 6661
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->a:Lcom/google/aa/b/a/a/a/a/b;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 6663
    :cond_0
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/g;->b:Z

    if-eqz v0, :cond_1

    .line 6664
    const/4 v0, 0x3

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/g;->b:Z

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 6666
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 6667
    :goto_0
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 6668
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/g;->c:[Lcom/google/j/a/a/a/j;

    aget-object v2, v2, v0

    .line 6669
    if-eqz v2, :cond_2

    .line 6670
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 6667
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6674
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 6675
    :goto_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 6676
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/g;->d:[Lcom/google/j/a/a/a/p;

    aget-object v0, v0, v1

    .line 6677
    if-eqz v0, :cond_4

    .line 6678
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 6675
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6682
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 6683
    return-void
.end method
