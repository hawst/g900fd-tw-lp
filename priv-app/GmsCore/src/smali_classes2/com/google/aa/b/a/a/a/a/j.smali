.class public final Lcom/google/aa/b/a/a/a/a/j;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Ljava/lang/String;

.field public b:[Lcom/google/aa/b/a/g;

.field public c:I

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lcom/google/checkout/inapp/proto/a/b;

.field public h:Lcom/google/checkout/inapp/proto/a/b;

.field public i:[I

.field public j:Lcom/google/aa/b/a/a/a/a/p;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 5456
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5457
    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    invoke-static {}, Lcom/google/aa/b/a/g;->a()[Lcom/google/aa/b/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    iput v2, p0, Lcom/google/aa/b/a/a/a/a/j;->c:I

    iput v2, p0, Lcom/google/aa/b/a/a/a/a/j;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/j;->cachedSize:I

    .line 5458
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5620
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v4

    .line 5621
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_d

    move v0, v1

    move v2, v1

    move v3, v1

    .line 5624
    :goto_0
    iget-object v5, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    array-length v5, v5

    if-ge v0, v5, :cond_1

    .line 5625
    iget-object v5, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    aget-object v5, v5, v0

    .line 5626
    if-eqz v5, :cond_0

    .line 5627
    add-int/lit8 v3, v3, 0x1

    .line 5628
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v2, v5

    .line 5624
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5632
    :cond_1
    add-int v0, v4, v2

    .line 5633
    mul-int/lit8 v2, v3, 0x1

    add-int/2addr v0, v2

    .line 5635
    :goto_1
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/j;->c:I

    if-eqz v2, :cond_2

    .line 5636
    const/4 v2, 0x2

    iget v3, p0, Lcom/google/aa/b/a/a/a/a/j;->c:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 5639
    :cond_2
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/j;->d:I

    if-eqz v2, :cond_3

    .line 5640
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/aa/b/a/a/a/a/j;->d:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 5643
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 5644
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5647
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 5648
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5651
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_6

    .line 5652
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5655
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_7

    .line 5656
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5659
    :cond_7
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    array-length v2, v2

    if-lez v2, :cond_9

    move v2, v1

    move v3, v1

    .line 5661
    :goto_2
    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    array-length v4, v4

    if-ge v2, v4, :cond_8

    .line 5662
    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    aget v4, v4, v2

    .line 5663
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 5661
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 5666
    :cond_8
    add-int/2addr v0, v3

    .line 5667
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 5669
    :cond_9
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v2, :cond_a

    .line 5670
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5673
    :cond_a
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    array-length v2, v2

    if-lez v2, :cond_c

    .line 5674
    :goto_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    array-length v2, v2

    if-ge v1, v2, :cond_c

    .line 5675
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    aget-object v2, v2, v1

    .line 5676
    if-eqz v2, :cond_b

    .line 5677
    const/16 v3, 0xa

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5674
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 5682
    :cond_c
    return v0

    :cond_d
    move v0, v4

    goto/16 :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5477
    if-ne p1, p0, :cond_1

    .line 5543
    :cond_0
    :goto_0
    return v0

    .line 5480
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/j;

    if-nez v2, :cond_2

    move v0, v1

    .line 5481
    goto :goto_0

    .line 5483
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/j;

    .line 5484
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 5486
    goto :goto_0

    .line 5488
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 5490
    goto :goto_0

    .line 5492
    :cond_4
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/j;->c:I

    iget v3, p1, Lcom/google/aa/b/a/a/a/a/j;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 5493
    goto :goto_0

    .line 5495
    :cond_5
    iget v2, p0, Lcom/google/aa/b/a/a/a/a/j;->d:I

    iget v3, p1, Lcom/google/aa/b/a/a/a/a/j;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 5496
    goto :goto_0

    .line 5498
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 5499
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 5500
    goto :goto_0

    .line 5502
    :cond_7
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 5503
    goto :goto_0

    .line 5505
    :cond_8
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 5506
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 5507
    goto :goto_0

    .line 5509
    :cond_9
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 5510
    goto :goto_0

    .line 5512
    :cond_a
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v2, :cond_b

    .line 5513
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_c

    move v0, v1

    .line 5514
    goto :goto_0

    .line 5517
    :cond_b
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 5518
    goto :goto_0

    .line 5521
    :cond_c
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v2, :cond_d

    .line 5522
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_e

    move v0, v1

    .line 5523
    goto :goto_0

    .line 5526
    :cond_d
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 5527
    goto/16 :goto_0

    .line 5530
    :cond_e
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 5532
    goto/16 :goto_0

    .line 5534
    :cond_f
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    if-nez v2, :cond_10

    .line 5535
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v2, :cond_0

    move v0, v1

    .line 5536
    goto/16 :goto_0

    .line 5539
    :cond_10
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/a/a/a/p;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 5540
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5548
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 5551
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5553
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/j;->c:I

    add-int/2addr v0, v2

    .line 5554
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/j;->d:I

    add-int/2addr v0, v2

    .line 5555
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 5557
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 5559
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 5561
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 5563
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v2

    add-int/2addr v0, v2

    .line 5565
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 5567
    return v0

    .line 5555
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5557
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 5559
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/a/b;->hashCode()I

    move-result v0

    goto :goto_2

    .line 5561
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/a/b;->hashCode()I

    move-result v0

    goto :goto_3

    .line 5565
    :cond_4
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    invoke-virtual {v1}, Lcom/google/aa/b/a/a/a/a/p;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 5409
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/j;->c:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/j;->d:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    :cond_5
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v1

    move v2, v1

    :goto_3
    if-ge v3, v4, :cond_7

    if-eqz v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v0, v2

    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v0

    goto :goto_3

    :pswitch_1
    add-int/lit8 v0, v2, 0x1

    aput v6, v5, v2

    goto :goto_4

    :cond_7
    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    if-nez v0, :cond_9

    array-length v3, v5

    if-ne v2, v3, :cond_9

    iput-object v5, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    goto/16 :goto_0

    :cond_8
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    array-length v0, v0

    goto :goto_5

    :cond_9
    add-int v3, v0, v2

    new-array v3, v3, [I

    if-eqz v0, :cond_a

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_a
    invoke-static {v5, v1, v3, v0, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_b

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    :pswitch_2
    goto :goto_6

    :pswitch_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_b
    if-eqz v0, :cond_f

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    if-nez v2, :cond_d

    move v2, v1

    :goto_7
    add-int/2addr v0, v2

    new-array v4, v0, [I

    if-eqz v2, :cond_c

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    invoke-static {v0, v1, v4, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    :goto_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_e

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    :pswitch_4
    goto :goto_8

    :pswitch_5
    add-int/lit8 v0, v2, 0x1

    aput v5, v4, v2

    move v2, v0

    goto :goto_8

    :cond_d
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    array-length v2, v2

    goto :goto_7

    :cond_e
    iput-object v4, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    :cond_f
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    if-nez v0, :cond_10

    new-instance v0, Lcom/google/aa/b/a/a/a/a/p;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/p;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    :cond_10
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x52

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    if-nez v0, :cond_12

    move v0, v1

    :goto_9
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/aa/b/a/g;

    if-eqz v0, :cond_11

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_11
    :goto_a
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_13

    new-instance v3, Lcom/google/aa/b/a/g;

    invoke-direct {v3}, Lcom/google/aa/b/a/g;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_12
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    array-length v0, v0

    goto :goto_9

    :cond_13
    new-instance v3, Lcom/google/aa/b/a/g;

    invoke-direct {v3}, Lcom/google/aa/b/a/g;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x42 -> :sswitch_9
        0x4a -> :sswitch_a
        0x52 -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5573
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 5574
    :goto_0
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 5575
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->a:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 5576
    if-eqz v2, :cond_0

    .line 5577
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5574
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5581
    :cond_1
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/j;->c:I

    if-eqz v0, :cond_2

    .line 5582
    const/4 v0, 0x2

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/j;->c:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5584
    :cond_2
    iget v0, p0, Lcom/google/aa/b/a/a/a/a/j;->d:I

    if-eqz v0, :cond_3

    .line 5585
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/aa/b/a/a/a/a/j;->d:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5587
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 5588
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5590
    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 5591
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5593
    :cond_5
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_6

    .line 5594
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->g:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5596
    :cond_6
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_7

    .line 5597
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->h:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5599
    :cond_7
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    .line 5600
    :goto_1
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    array-length v2, v2

    if-ge v0, v2, :cond_8

    .line 5601
    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/j;->i:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5600
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 5604
    :cond_8
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v0, :cond_9

    .line 5605
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/j;->j:Lcom/google/aa/b/a/a/a/a/p;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5607
    :cond_9
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    array-length v0, v0

    if-lez v0, :cond_b

    .line 5608
    :goto_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    array-length v0, v0

    if-ge v1, v0, :cond_b

    .line 5609
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/j;->b:[Lcom/google/aa/b/a/g;

    aget-object v0, v0, v1

    .line 5610
    if-eqz v0, :cond_a

    .line 5611
    const/16 v2, 0xa

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5608
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 5615
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5616
    return-void
.end method
