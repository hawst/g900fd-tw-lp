.class public final Lcom/google/aa/b/a/a/a/a/u;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/aa/b/a/a/a/a/b;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Z

.field public h:Z

.field public i:Z

.field public j:Z

.field public k:Z

.field public l:Z

.field public m:Z

.field public n:[Lcom/google/aa/b/a/a/a/a/x;

.field public o:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1385
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1386
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    iput-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    iput-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    iput-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    iput-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    iput-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    iput-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    invoke-static {}, Lcom/google/aa/b/a/a/a/a/x;->a()[Lcom/google/aa/b/a/a/a/a/x;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->o:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/u;->cachedSize:I

    .line 1387
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 1584
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1585
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    if-eqz v1, :cond_0

    .line 1586
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1589
    :cond_0
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1590
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1593
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 1594
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1597
    :cond_2
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 1598
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1601
    :cond_3
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1602
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1605
    :cond_4
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1606
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1609
    :cond_5
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    if-eqz v1, :cond_6

    .line 1610
    const/16 v1, 0x8

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1613
    :cond_6
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    if-eqz v1, :cond_7

    .line 1614
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1617
    :cond_7
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    if-eqz v1, :cond_8

    .line 1618
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1621
    :cond_8
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    if-eqz v1, :cond_9

    .line 1622
    const/16 v1, 0xb

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1625
    :cond_9
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    if-eqz v1, :cond_a

    .line 1626
    const/16 v1, 0xf

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1629
    :cond_a
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->o:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1630
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->o:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1633
    :cond_b
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    if-eqz v1, :cond_c

    .line 1634
    const/16 v1, 0x12

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1637
    :cond_c
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    if-eqz v1, :cond_d

    .line 1638
    const/16 v1, 0x13

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1641
    :cond_d
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    if-eqz v1, :cond_10

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    array-length v1, v1

    if-lez v1, :cond_10

    .line 1642
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    array-length v2, v2

    if-ge v0, v2, :cond_f

    .line 1643
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    aget-object v2, v2, v0

    .line 1644
    if-eqz v2, :cond_e

    .line 1645
    const/16 v3, 0x15

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1642
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_f
    move v0, v1

    .line 1650
    :cond_10
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1411
    if-ne p1, p0, :cond_1

    .line 1494
    :cond_0
    :goto_0
    return v0

    .line 1414
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/u;

    if-nez v2, :cond_2

    move v0, v1

    .line 1415
    goto :goto_0

    .line 1417
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/u;

    .line 1418
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    if-nez v2, :cond_3

    .line 1419
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1420
    goto :goto_0

    .line 1423
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/a/a/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1424
    goto :goto_0

    .line 1427
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1428
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 1429
    goto :goto_0

    .line 1431
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1432
    goto :goto_0

    .line 1434
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1435
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 1436
    goto :goto_0

    .line 1438
    :cond_7
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1439
    goto :goto_0

    .line 1441
    :cond_8
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 1442
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 1443
    goto :goto_0

    .line 1445
    :cond_9
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 1446
    goto :goto_0

    .line 1448
    :cond_a
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 1449
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    if-eqz v2, :cond_c

    move v0, v1

    .line 1450
    goto :goto_0

    .line 1452
    :cond_b
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 1453
    goto :goto_0

    .line 1455
    :cond_c
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    if-nez v2, :cond_d

    .line 1456
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    if-eqz v2, :cond_e

    move v0, v1

    .line 1457
    goto :goto_0

    .line 1459
    :cond_d
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    move v0, v1

    .line 1460
    goto/16 :goto_0

    .line 1462
    :cond_e
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    if-eq v2, v3, :cond_f

    move v0, v1

    .line 1463
    goto/16 :goto_0

    .line 1465
    :cond_f
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 1466
    goto/16 :goto_0

    .line 1468
    :cond_10
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 1469
    goto/16 :goto_0

    .line 1471
    :cond_11
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 1472
    goto/16 :goto_0

    .line 1474
    :cond_12
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    if-eq v2, v3, :cond_13

    move v0, v1

    .line 1475
    goto/16 :goto_0

    .line 1477
    :cond_13
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    if-eq v2, v3, :cond_14

    move v0, v1

    .line 1478
    goto/16 :goto_0

    .line 1480
    :cond_14
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    if-eq v2, v3, :cond_15

    move v0, v1

    .line 1481
    goto/16 :goto_0

    .line 1483
    :cond_15
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    move v0, v1

    .line 1485
    goto/16 :goto_0

    .line 1487
    :cond_16
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->o:Ljava/lang/String;

    if-nez v2, :cond_17

    .line 1488
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/u;->o:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1489
    goto/16 :goto_0

    .line 1491
    :cond_17
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->o:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/u;->o:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1492
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 1499
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1502
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 1504
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 1506
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    .line 1508
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v4

    .line 1510
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v4

    .line 1512
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_6
    add-int/2addr v0, v4

    .line 1513
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_7
    add-int/2addr v0, v4

    .line 1514
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    if-eqz v0, :cond_8

    move v0, v2

    :goto_8
    add-int/2addr v0, v4

    .line 1515
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    if-eqz v0, :cond_9

    move v0, v2

    :goto_9
    add-int/2addr v0, v4

    .line 1516
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    if-eqz v0, :cond_a

    move v0, v2

    :goto_a
    add-int/2addr v0, v4

    .line 1517
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    if-eqz v0, :cond_b

    move v0, v2

    :goto_b
    add-int/2addr v0, v4

    .line 1518
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    if-eqz v4, :cond_c

    :goto_c
    add-int/2addr v0, v2

    .line 1519
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1521
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->o:Ljava/lang/String;

    if-nez v2, :cond_d

    :goto_d
    add-int/2addr v0, v1

    .line 1523
    return v0

    .line 1499
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    invoke-virtual {v0}, Lcom/google/aa/b/a/a/a/a/b;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1502
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1504
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1506
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 1508
    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 1510
    :cond_5
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    :cond_6
    move v0, v3

    .line 1512
    goto :goto_6

    :cond_7
    move v0, v3

    .line 1513
    goto :goto_7

    :cond_8
    move v0, v3

    .line 1514
    goto :goto_8

    :cond_9
    move v0, v3

    .line 1515
    goto :goto_9

    :cond_a
    move v0, v3

    .line 1516
    goto :goto_a

    :cond_b
    move v0, v3

    .line 1517
    goto :goto_b

    :cond_c
    move v2, v3

    .line 1518
    goto :goto_c

    .line 1521
    :cond_d
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->o:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_d
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1323
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/aa/b/a/a/a/a/b;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    goto :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->o:Ljava/lang/String;

    goto :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    goto :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    goto :goto_0

    :sswitch_f
    const/16 v0, 0xaa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/aa/b/a/a/a/a/x;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/aa/b/a/a/a/a/x;

    invoke-direct {v3}, Lcom/google/aa/b/a/a/a/a/x;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/aa/b/a/a/a/a/x;

    invoke-direct {v3}, Lcom/google/aa/b/a/a/a/a/x;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x78 -> :sswitch_b
        0x8a -> :sswitch_c
        0x90 -> :sswitch_d
        0x98 -> :sswitch_e
        0xaa -> :sswitch_f
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1529
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    if-eqz v0, :cond_0

    .line 1530
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->a:Lcom/google/aa/b/a/a/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1532
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1533
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1535
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1536
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1538
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1539
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1541
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1542
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1544
    :cond_4
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1545
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1547
    :cond_5
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    if-eqz v0, :cond_6

    .line 1548
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1550
    :cond_6
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    if-eqz v0, :cond_7

    .line 1551
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1553
    :cond_7
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    if-eqz v0, :cond_8

    .line 1554
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->i:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1556
    :cond_8
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    if-eqz v0, :cond_9

    .line 1557
    const/16 v0, 0xb

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1559
    :cond_9
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    if-eqz v0, :cond_a

    .line 1560
    const/16 v0, 0xf

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->k:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1562
    :cond_a
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->o:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 1563
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1565
    :cond_b
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    if-eqz v0, :cond_c

    .line 1566
    const/16 v0, 0x12

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->l:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1568
    :cond_c
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    if-eqz v0, :cond_d

    .line 1569
    const/16 v0, 0x13

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/u;->m:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1571
    :cond_d
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    array-length v0, v0

    if-lez v0, :cond_f

    .line 1572
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    array-length v1, v1

    if-ge v0, v1, :cond_f

    .line 1573
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/u;->n:[Lcom/google/aa/b/a/a/a/a/x;

    aget-object v1, v1, v0

    .line 1574
    if-eqz v1, :cond_e

    .line 1575
    const/16 v2, 0x15

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1572
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1579
    :cond_f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1580
    return-void
.end method
