.class public final Lcom/google/aa/b/a/a/a/a/s;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/aa/b/a/a/a/a/s;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 801
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 802
    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->d:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/s;->cachedSize:I

    .line 803
    return-void
.end method

.method public static a()[Lcom/google/aa/b/a/a/a/a/s;
    .locals 2

    .prologue
    .line 778
    sget-object v0, Lcom/google/aa/b/a/a/a/a/s;->e:[Lcom/google/aa/b/a/a/a/a/s;

    if-nez v0, :cond_1

    .line 779
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 781
    :try_start_0
    sget-object v0, Lcom/google/aa/b/a/a/a/a/s;->e:[Lcom/google/aa/b/a/a/a/a/s;

    if-nez v0, :cond_0

    .line 782
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/aa/b/a/a/a/a/s;

    sput-object v0, Lcom/google/aa/b/a/a/a/a/s;->e:[Lcom/google/aa/b/a/a/a/a/s;

    .line 784
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 786
    :cond_1
    sget-object v0, Lcom/google/aa/b/a/a/a/a/s;->e:[Lcom/google/aa/b/a/a/a/a/s;

    return-object v0

    .line 784
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 888
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 889
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/s;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 890
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/s;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 893
    :cond_0
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/s;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 894
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/s;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 897
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/s;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 898
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/s;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 901
    :cond_2
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/s;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 902
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/s;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 905
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 816
    if-ne p1, p0, :cond_1

    .line 851
    :cond_0
    :goto_0
    return v0

    .line 819
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/s;

    if-nez v2, :cond_2

    move v0, v1

    .line 820
    goto :goto_0

    .line 822
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/s;

    .line 823
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/s;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 824
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/s;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 825
    goto :goto_0

    .line 827
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/s;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/s;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 828
    goto :goto_0

    .line 830
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/s;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 831
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/s;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 832
    goto :goto_0

    .line 834
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/s;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/s;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 835
    goto :goto_0

    .line 837
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/s;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 838
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/s;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 839
    goto :goto_0

    .line 841
    :cond_7
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/s;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/s;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 842
    goto :goto_0

    .line 844
    :cond_8
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/s;->d:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 845
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/s;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 846
    goto :goto_0

    .line 848
    :cond_9
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/s;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/s;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 849
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 856
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 859
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 861
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 863
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/s;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 865
    return v0

    .line 856
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 859
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 861
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 863
    :cond_3
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/s;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 772
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 871
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 872
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/s;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 874
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 875
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/s;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 877
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 878
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/s;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 880
    :cond_2
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/s;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 881
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/s;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 883
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 884
    return-void
.end method
