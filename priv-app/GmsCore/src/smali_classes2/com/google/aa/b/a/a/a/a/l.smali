.class public final Lcom/google/aa/b/a/a/a/a/l;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[I

.field public b:Lcom/google/aa/b/a/a/a/a/v;

.field public c:[Lcom/google/aa/b/a/a/a/a/r;

.field public d:Lcom/google/aa/b/a/a/a/a/p;

.field public e:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3347
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3348
    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    invoke-static {}, Lcom/google/aa/b/a/a/a/a/r;->a()[Lcom/google/aa/b/a/a/a/a/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/l;->e:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/aa/b/a/a/a/a/l;->cachedSize:I

    .line 3349
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3447
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v3

    .line 3448
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    array-length v0, v0

    if-lez v0, :cond_6

    move v0, v1

    move v2, v1

    .line 3450
    :goto_0
    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    array-length v4, v4

    if-ge v0, v4, :cond_0

    .line 3451
    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    aget v4, v4, v0

    .line 3452
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v4

    add-int/2addr v2, v4

    .line 3450
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3455
    :cond_0
    add-int v0, v3, v2

    .line 3456
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    array-length v2, v2

    mul-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 3458
    :goto_1
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    if-eqz v2, :cond_1

    .line 3459
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3462
    :cond_1
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    array-length v2, v2

    if-lez v2, :cond_3

    .line 3463
    :goto_2
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    array-length v2, v2

    if-ge v1, v2, :cond_3

    .line 3464
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    aget-object v2, v2, v1

    .line 3465
    if-eqz v2, :cond_2

    .line 3466
    const/4 v3, 0x5

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3463
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3471
    :cond_3
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v1, :cond_4

    .line 3472
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3475
    :cond_4
    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/l;->e:Z

    if-eqz v1, :cond_5

    .line 3476
    const/16 v1, 0xa

    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/l;->e:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3479
    :cond_5
    return v0

    :cond_6
    move v0, v3

    goto :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3363
    if-ne p1, p0, :cond_1

    .line 3399
    :cond_0
    :goto_0
    return v0

    .line 3366
    :cond_1
    instance-of v2, p1, Lcom/google/aa/b/a/a/a/a/l;

    if-nez v2, :cond_2

    move v0, v1

    .line 3367
    goto :goto_0

    .line 3369
    :cond_2
    check-cast p1, Lcom/google/aa/b/a/a/a/a/l;

    .line 3370
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 3372
    goto :goto_0

    .line 3374
    :cond_3
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    if-nez v2, :cond_4

    .line 3375
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    if-eqz v2, :cond_5

    move v0, v1

    .line 3376
    goto :goto_0

    .line 3379
    :cond_4
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/a/a/a/v;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 3380
    goto :goto_0

    .line 3383
    :cond_5
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 3385
    goto :goto_0

    .line 3387
    :cond_6
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    if-nez v2, :cond_7

    .line 3388
    iget-object v2, p1, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v2, :cond_8

    move v0, v1

    .line 3389
    goto :goto_0

    .line 3392
    :cond_7
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    iget-object v3, p1, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/a/a/a/p;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 3393
    goto :goto_0

    .line 3396
    :cond_8
    iget-boolean v2, p0, Lcom/google/aa/b/a/a/a/a/l;->e:Z

    iget-boolean v3, p1, Lcom/google/aa/b/a/a/a/a/l;->e:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 3397
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3404
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 3407
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 3409
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3411
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 3413
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/l;->e:Z

    if-eqz v0, :cond_2

    const/16 v0, 0x4cf

    :goto_2
    add-int/2addr v0, v1

    .line 3414
    return v0

    .line 3407
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    invoke-virtual {v0}, Lcom/google/aa/b/a/a/a/a/v;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3411
    :cond_1
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    invoke-virtual {v1}, Lcom/google/aa/b/a/a/a/a/p;->hashCode()I

    move-result v1

    goto :goto_1

    .line 3413
    :cond_2
    const/16 v0, 0x4d5

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 3315
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    :pswitch_2
    goto :goto_4

    :pswitch_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    :pswitch_4
    goto :goto_6

    :pswitch_5
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    :cond_a
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/aa/b/a/a/a/a/v;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/v;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    :cond_b
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v1

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    if-nez v0, :cond_d

    move v0, v2

    :goto_7
    add-int/2addr v1, v0

    new-array v1, v1, [Lcom/google/aa/b/a/a/a/a/r;

    if-eqz v0, :cond_c

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    invoke-static {v3, v2, v1, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_c
    :goto_8
    array-length v3, v1

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_e

    new-instance v3, Lcom/google/aa/b/a/a/a/a/r;

    invoke-direct {v3}, Lcom/google/aa/b/a/a/a/a/r;-><init>()V

    aput-object v3, v1, v0

    aget-object v3, v1, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_8

    :cond_d
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    array-length v0, v0

    goto :goto_7

    :cond_e
    new-instance v3, Lcom/google/aa/b/a/a/a/a/r;

    invoke-direct {v3}, Lcom/google/aa/b/a/a/a/a/r;-><init>()V

    aput-object v3, v1, v0

    aget-object v0, v1, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v1, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    goto/16 :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    if-nez v0, :cond_f

    new-instance v0, Lcom/google/aa/b/a/a/a/a/p;

    invoke-direct {v0}, Lcom/google/aa/b/a/a/a/a/p;-><init>()V

    iput-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    :cond_f
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/l;->e:Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x12 -> :sswitch_3
        0x2a -> :sswitch_4
        0x42 -> :sswitch_5
        0x50 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3420
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    move v0, v1

    .line 3421
    :goto_0
    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 3422
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/aa/b/a/a/a/a/l;->a:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3421
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3425
    :cond_0
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    if-eqz v0, :cond_1

    .line 3426
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/aa/b/a/a/a/a/l;->b:Lcom/google/aa/b/a/a/a/a/v;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3428
    :cond_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 3429
    :goto_1
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 3430
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->c:[Lcom/google/aa/b/a/a/a/a/r;

    aget-object v0, v0, v1

    .line 3431
    if-eqz v0, :cond_2

    .line 3432
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3429
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 3436
    :cond_3
    iget-object v0, p0, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    if-eqz v0, :cond_4

    .line 3437
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/aa/b/a/a/a/a/l;->d:Lcom/google/aa/b/a/a/a/a/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3439
    :cond_4
    iget-boolean v0, p0, Lcom/google/aa/b/a/a/a/a/l;->e:Z

    if-eqz v0, :cond_5

    .line 3440
    const/16 v0, 0xa

    iget-boolean v1, p0, Lcom/google/aa/b/a/a/a/a/l;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3442
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3443
    return-void
.end method
