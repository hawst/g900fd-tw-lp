.class public final Lcom/google/checkout/inapp/proto/ao;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/checkout/inapp/proto/q;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/t/a/b;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3588
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3589
    iput-object v1, p0, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->e:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/ao;->cachedSize:I

    .line 3590
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3692
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3693
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3694
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3697
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    if-eqz v1, :cond_1

    .line 3698
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3701
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3702
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3705
    :cond_2
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ao;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3706
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3709
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v1, :cond_4

    .line 3710
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3713
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3604
    if-ne p1, p0, :cond_1

    .line 3650
    :cond_0
    :goto_0
    return v0

    .line 3607
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/ao;

    if-nez v2, :cond_2

    move v0, v1

    .line 3608
    goto :goto_0

    .line 3610
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/ao;

    .line 3611
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v2, :cond_3

    .line 3612
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v2, :cond_4

    move v0, v1

    .line 3613
    goto :goto_0

    .line 3616
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 3617
    goto :goto_0

    .line 3620
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 3621
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 3622
    goto :goto_0

    .line 3624
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 3625
    goto :goto_0

    .line 3627
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    if-nez v2, :cond_7

    .line 3628
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    if-eqz v2, :cond_8

    move v0, v1

    .line 3629
    goto :goto_0

    .line 3632
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    invoke-virtual {v2, v3}, Lcom/google/t/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 3633
    goto :goto_0

    .line 3636
    :cond_8
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 3637
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 3638
    goto :goto_0

    .line 3640
    :cond_9
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 3641
    goto :goto_0

    .line 3643
    :cond_a
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->e:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 3644
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ao;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 3645
    goto :goto_0

    .line 3647
    :cond_b
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ao;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 3648
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3655
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3658
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3660
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3662
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 3664
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ao;->e:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 3666
    return v0

    .line 3655
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/q;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3658
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3660
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    invoke-virtual {v0}, Lcom/google/t/a/b;->hashCode()I

    move-result v0

    goto :goto_2

    .line 3662
    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 3664
    :cond_4
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ao;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3556
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/t/a/b;

    invoke-direct {v0}, Lcom/google/t/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/checkout/inapp/proto/q;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/q;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3672
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 3673
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ao;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3675
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    if-eqz v0, :cond_1

    .line 3676
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ao;->c:Lcom/google/t/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3678
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3679
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ao;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3681
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3682
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ao;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3684
    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v0, :cond_4

    .line 3685
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ao;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3687
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3688
    return-void
.end method
