.class public final Lcom/google/checkout/inapp/proto/aj;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/checkout/inapp/proto/aj;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 756
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 757
    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/aj;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/aj;->b:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/aj;->cachedSize:I

    .line 758
    return-void
.end method

.method public static a()[Lcom/google/checkout/inapp/proto/aj;
    .locals 2

    .prologue
    .line 736
    sget-object v0, Lcom/google/checkout/inapp/proto/aj;->d:[Lcom/google/checkout/inapp/proto/aj;

    if-nez v0, :cond_1

    .line 737
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 739
    :try_start_0
    sget-object v0, Lcom/google/checkout/inapp/proto/aj;->d:[Lcom/google/checkout/inapp/proto/aj;

    if-nez v0, :cond_0

    .line 740
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/checkout/inapp/proto/aj;

    sput-object v0, Lcom/google/checkout/inapp/proto/aj;->d:[Lcom/google/checkout/inapp/proto/aj;

    .line 742
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 744
    :cond_1
    sget-object v0, Lcom/google/checkout/inapp/proto/aj;->d:[Lcom/google/checkout/inapp/proto/aj;

    return-object v0

    .line 742
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 832
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 833
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aj;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 834
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/aj;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 837
    :cond_0
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aj;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 838
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/aj;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 841
    :cond_1
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 844
    :goto_0
    iget-object v4, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 845
    iget-object v4, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 846
    if-eqz v4, :cond_2

    .line 847
    add-int/lit8 v3, v3, 0x1

    .line 848
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 844
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 852
    :cond_3
    add-int/2addr v0, v2

    .line 853
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 855
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 770
    if-ne p1, p0, :cond_1

    .line 795
    :cond_0
    :goto_0
    return v0

    .line 773
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/aj;

    if-nez v2, :cond_2

    move v0, v1

    .line 774
    goto :goto_0

    .line 776
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/aj;

    .line 777
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aj;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 778
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/aj;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 779
    goto :goto_0

    .line 781
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aj;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/aj;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 782
    goto :goto_0

    .line 784
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aj;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 785
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/aj;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 786
    goto :goto_0

    .line 788
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aj;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/aj;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 789
    goto :goto_0

    .line 791
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 793
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 800
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aj;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 803
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aj;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 805
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 807
    return v0

    .line 800
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aj;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 803
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/aj;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 730
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/aj;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/aj;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x2a -> :sswitch_2
        0x32 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 813
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aj;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 814
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/aj;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 816
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aj;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 817
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/aj;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 819
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 820
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 821
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/aj;->c:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 822
    if-eqz v1, :cond_2

    .line 823
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 820
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 827
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 828
    return-void
.end method
