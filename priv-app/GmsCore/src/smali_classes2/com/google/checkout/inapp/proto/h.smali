.class public final Lcom/google/checkout/inapp/proto/h;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile g:[Lcom/google/checkout/inapp/proto/h;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 43
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 44
    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/h;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/h;->b:Ljava/lang/String;

    iput v1, p0, Lcom/google/checkout/inapp/proto/h;->c:I

    iput v1, p0, Lcom/google/checkout/inapp/proto/h;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/h;->e:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/h;->f:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/h;->cachedSize:I

    .line 45
    return-void
.end method

.method public static a()[Lcom/google/checkout/inapp/proto/h;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/checkout/inapp/proto/h;->g:[Lcom/google/checkout/inapp/proto/h;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/checkout/inapp/proto/h;->g:[Lcom/google/checkout/inapp/proto/h;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/checkout/inapp/proto/h;

    sput-object v0, Lcom/google/checkout/inapp/proto/h;->g:[Lcom/google/checkout/inapp/proto/h;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/checkout/inapp/proto/h;->g:[Lcom/google/checkout/inapp/proto/h;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 136
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 137
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/h;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/h;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 141
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/checkout/inapp/proto/h;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/checkout/inapp/proto/h;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/h;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 146
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/h;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 149
    :cond_0
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/h;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 151
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 60
    if-ne p1, p0, :cond_1

    .line 101
    :cond_0
    :goto_0
    return v0

    .line 63
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/h;

    if-nez v2, :cond_2

    move v0, v1

    .line 64
    goto :goto_0

    .line 66
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/h;

    .line 67
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/h;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 68
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/h;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 69
    goto :goto_0

    .line 71
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/h;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/h;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 72
    goto :goto_0

    .line 74
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/h;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 75
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/h;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 76
    goto :goto_0

    .line 78
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/h;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/h;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 79
    goto :goto_0

    .line 81
    :cond_6
    iget v2, p0, Lcom/google/checkout/inapp/proto/h;->c:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/h;->c:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 82
    goto :goto_0

    .line 84
    :cond_7
    iget v2, p0, Lcom/google/checkout/inapp/proto/h;->d:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/h;->d:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 85
    goto :goto_0

    .line 87
    :cond_8
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/h;->e:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 88
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/h;->e:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 89
    goto :goto_0

    .line 91
    :cond_9
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/h;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/h;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 92
    goto :goto_0

    .line 94
    :cond_a
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/h;->f:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 95
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/h;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 96
    goto :goto_0

    .line 98
    :cond_b
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/h;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/h;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 99
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 106
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/h;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 109
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/h;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 111
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/checkout/inapp/proto/h;->c:I

    add-int/2addr v0, v2

    .line 112
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/checkout/inapp/proto/h;->d:I

    add-int/2addr v0, v2

    .line 113
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/h;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 115
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/h;->f:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 117
    return v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/h;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/h;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 113
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/h;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 115
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/h;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/h;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/h;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/checkout/inapp/proto/h;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/checkout/inapp/proto/h;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/h;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/h;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 123
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/h;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 124
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/h;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 125
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/checkout/inapp/proto/h;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 126
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/checkout/inapp/proto/h;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 127
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/h;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 128
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/h;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 130
    :cond_0
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/h;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 131
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 132
    return-void
.end method
