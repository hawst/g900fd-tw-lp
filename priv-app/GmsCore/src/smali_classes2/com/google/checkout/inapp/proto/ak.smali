.class public final Lcom/google/checkout/inapp/proto/ak;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/checkout/inapp/proto/q;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 56
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ak;->b:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/ak;->cachedSize:I

    .line 57
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 118
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 119
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ak;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 120
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ak;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 123
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v1, :cond_1

    .line 124
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 68
    if-ne p1, p0, :cond_1

    .line 91
    :cond_0
    :goto_0
    return v0

    .line 71
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/ak;

    if-nez v2, :cond_2

    move v0, v1

    .line 72
    goto :goto_0

    .line 74
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/ak;

    .line 75
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v2, :cond_3

    .line 76
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v2, :cond_4

    move v0, v1

    .line 77
    goto :goto_0

    .line 80
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 81
    goto :goto_0

    .line 84
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ak;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 85
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ak;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 86
    goto :goto_0

    .line 88
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ak;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ak;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 89
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 96
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 99
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ak;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 101
    return v0

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/q;->hashCode()I

    move-result v0

    goto :goto_0

    .line 99
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ak;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 32
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ak;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/checkout/inapp/proto/q;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/q;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ak;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 108
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ak;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v0, :cond_1

    .line 111
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ak;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 113
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 114
    return-void
.end method
