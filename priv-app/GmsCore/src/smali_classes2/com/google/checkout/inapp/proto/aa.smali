.class public final Lcom/google/checkout/inapp/proto/aa;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/checkout/inapp/proto/q;

.field public b:Lcom/google/checkout/a/a/a/d;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1617
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1618
    iput-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->c:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/aa;->cachedSize:I

    .line 1619
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1695
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1696
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    if-eqz v1, :cond_0

    .line 1697
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1700
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/aa;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 1701
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aa;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1704
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v1, :cond_2

    .line 1705
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1708
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1631
    if-ne p1, p0, :cond_1

    .line 1663
    :cond_0
    :goto_0
    return v0

    .line 1634
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/aa;

    if-nez v2, :cond_2

    move v0, v1

    .line 1635
    goto :goto_0

    .line 1637
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/aa;

    .line 1638
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v2, :cond_3

    .line 1639
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1640
    goto :goto_0

    .line 1643
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1644
    goto :goto_0

    .line 1647
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    if-nez v2, :cond_5

    .line 1648
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    if-eqz v2, :cond_6

    move v0, v1

    .line 1649
    goto :goto_0

    .line 1652
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    invoke-virtual {v2, v3}, Lcom/google/checkout/a/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1653
    goto :goto_0

    .line 1656
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aa;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1657
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/aa;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1658
    goto :goto_0

    .line 1660
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aa;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/aa;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1661
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1668
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1671
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1673
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/aa;->c:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 1675
    return v0

    .line 1668
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/q;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1671
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    invoke-virtual {v0}, Lcom/google/checkout/a/a/a/d;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1673
    :cond_2
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/aa;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1591
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/checkout/a/a/a/d;

    invoke-direct {v0}, Lcom/google/checkout/a/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/checkout/inapp/proto/q;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/q;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x42 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1681
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    if-eqz v0, :cond_0

    .line 1682
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/aa;->b:Lcom/google/checkout/a/a/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1684
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1685
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/aa;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1687
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v0, :cond_2

    .line 1688
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/aa;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1690
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1691
    return-void
.end method
