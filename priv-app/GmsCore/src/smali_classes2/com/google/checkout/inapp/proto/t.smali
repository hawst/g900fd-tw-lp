.class public final Lcom/google/checkout/inapp/proto/t;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/checkout/inapp/proto/j;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/google/checkout/inapp/proto/a/d;

.field public e:Lcom/google/checkout/inapp/proto/a/d;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5489
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5490
    invoke-static {}, Lcom/google/checkout/inapp/proto/j;->a()[Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/t;->c:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/t;->cachedSize:I

    .line 5491
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 5595
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v1

    .line 5596
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 5597
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 5598
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    aget-object v2, v2, v0

    .line 5599
    if-eqz v2, :cond_0

    .line 5600
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 5597
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5605
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 5606
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 5609
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 5610
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->c:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 5613
    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_4

    .line 5614
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 5617
    :cond_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_5

    .line 5618
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 5621
    :cond_5
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5505
    if-ne p1, p0, :cond_1

    .line 5548
    :cond_0
    :goto_0
    return v0

    .line 5508
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/t;

    if-nez v2, :cond_2

    move v0, v1

    .line 5509
    goto :goto_0

    .line 5511
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/t;

    .line 5512
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 5514
    goto :goto_0

    .line 5516
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 5517
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 5518
    goto :goto_0

    .line 5520
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 5521
    goto :goto_0

    .line 5523
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 5524
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/t;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 5525
    goto :goto_0

    .line 5527
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/t;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 5528
    goto :goto_0

    .line 5530
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v2, :cond_8

    .line 5531
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v2, :cond_9

    move v0, v1

    .line 5532
    goto :goto_0

    .line 5535
    :cond_8
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 5536
    goto :goto_0

    .line 5539
    :cond_9
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v2, :cond_a

    .line 5540
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v2, :cond_0

    move v0, v1

    .line 5541
    goto :goto_0

    .line 5544
    :cond_a
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 5545
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5553
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 5556
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 5558
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 5560
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 5562
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 5564
    return v0

    .line 5556
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5558
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 5560
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/a/d;->hashCode()I

    move-result v0

    goto :goto_2

    .line 5562
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v1}, Lcom/google/checkout/inapp/proto/a/d;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5457
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/checkout/inapp/proto/j;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/checkout/inapp/proto/j;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/t;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    :cond_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    :cond_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 5570
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 5571
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 5572
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/t;->a:[Lcom/google/checkout/inapp/proto/j;

    aget-object v1, v1, v0

    .line 5573
    if-eqz v1, :cond_0

    .line 5574
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5571
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5578
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 5579
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/t;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5581
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 5582
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/t;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5584
    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_4

    .line 5585
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/t;->d:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5587
    :cond_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_5

    .line 5588
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/t;->e:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5590
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5591
    return-void
.end method
