.class public final Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/checkout/inapp/proto/al;

.field public b:[I

.field public c:[I

.field public d:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2757
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2758
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->cachedSize:I

    .line 2759
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 2843
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2844
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    if-eqz v1, :cond_0

    .line 2845
    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2848
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    array-length v1, v1

    if-lez v1, :cond_2

    move v1, v2

    move v3, v2

    .line 2850
    :goto_0
    iget-object v4, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    array-length v4, v4

    if-ge v1, v4, :cond_1

    .line 2851
    iget-object v4, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    aget v4, v4, v1

    .line 2852
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 2850
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2855
    :cond_1
    add-int/2addr v0, v3

    .line 2856
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2858
    :cond_2
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    array-length v1, v1

    if-lez v1, :cond_4

    move v1, v2

    move v3, v2

    .line 2860
    :goto_1
    iget-object v4, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 2861
    iget-object v4, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    aget v4, v4, v1

    .line 2862
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 2860
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2865
    :cond_3
    add-int/2addr v0, v3

    .line 2866
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2868
    :cond_4
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    array-length v1, v1

    if-lez v1, :cond_6

    move v1, v2

    .line 2870
    :goto_2
    iget-object v3, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    array-length v3, v3

    if-ge v2, v3, :cond_5

    .line 2871
    iget-object v3, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    aget v3, v3, v2

    .line 2872
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 2870
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 2875
    :cond_5
    add-int/2addr v0, v1

    .line 2876
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2878
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2772
    if-ne p1, p0, :cond_1

    .line 2800
    :cond_0
    :goto_0
    return v0

    .line 2775
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;

    if-nez v2, :cond_2

    move v0, v1

    .line 2776
    goto :goto_0

    .line 2778
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;

    .line 2779
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    if-nez v2, :cond_3

    .line 2780
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2781
    goto :goto_0

    .line 2784
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/al;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2785
    goto :goto_0

    .line 2788
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 2790
    goto :goto_0

    .line 2792
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 2794
    goto :goto_0

    .line 2796
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2798
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2805
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2808
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 2810
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 2812
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 2814
    return v0

    .line 2805
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/al;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 2728
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/checkout/inapp/proto/al;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/al;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    if-nez v0, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_5

    array-length v3, v5

    if-ne v1, v3, :cond_5

    iput-object v5, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    array-length v0, v0

    goto :goto_3

    :cond_5
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_6

    iget-object v4, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    :pswitch_2
    goto :goto_4

    :pswitch_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    if-eqz v0, :cond_b

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    if-nez v1, :cond_9

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_a

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    :pswitch_4
    goto :goto_6

    :pswitch_5
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_9
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    array-length v1, v1

    goto :goto_5

    :cond_a
    iput-object v4, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    :cond_b
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x20

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_7
    if-ge v3, v4, :cond_d

    if-eqz v3, :cond_c

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_3

    move v0, v1

    :goto_8
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_7

    :pswitch_6
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_8

    :cond_d
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    if-nez v0, :cond_e

    move v0, v2

    :goto_9
    if-nez v0, :cond_f

    array-length v3, v5

    if-ne v1, v3, :cond_f

    iput-object v5, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    goto/16 :goto_0

    :cond_e
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    array-length v0, v0

    goto :goto_9

    :cond_f
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_10

    iget-object v4, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_10
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_11

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_4

    goto :goto_a

    :pswitch_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_11
    if-eqz v0, :cond_15

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    if-nez v1, :cond_13

    move v1, v2

    :goto_b
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_12

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_12
    :goto_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_14

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_5

    goto :goto_c

    :pswitch_8
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_c

    :cond_13
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    array-length v1, v1

    goto :goto_b

    :cond_14
    iput-object v4, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    :cond_15
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_6
    const/16 v0, 0x28

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_d
    if-ge v3, v4, :cond_17

    if-eqz v3, :cond_16

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_16
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_6

    move v0, v1

    :goto_e
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_d

    :pswitch_9
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_e

    :cond_17
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    if-nez v0, :cond_18

    move v0, v2

    :goto_f
    if-nez v0, :cond_19

    array-length v3, v5

    if-ne v1, v3, :cond_19

    iput-object v5, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    goto/16 :goto_0

    :cond_18
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    array-length v0, v0

    goto :goto_f

    :cond_19
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_1a

    iget-object v4, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1a
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_1b

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_7

    goto :goto_10

    :pswitch_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    :cond_1b
    if-eqz v0, :cond_1f

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    if-nez v1, :cond_1d

    move v1, v2

    :goto_11
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_1c

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1c
    :goto_12
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_1e

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_8

    goto :goto_12

    :pswitch_b
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_12

    :cond_1d
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    array-length v1, v1

    goto :goto_11

    :cond_1e
    iput-object v4, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    :cond_1f
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x18 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x22 -> :sswitch_5
        0x28 -> :sswitch_6
        0x2a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_4
        :pswitch_5
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_6
        :pswitch_6
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_7
        :pswitch_7
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x0
        :pswitch_8
        :pswitch_8
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x0
        :pswitch_9
        :pswitch_9
    .end packed-switch

    :pswitch_data_7
    .packed-switch 0x0
        :pswitch_a
        :pswitch_a
    .end packed-switch

    :pswitch_data_8
    .packed-switch 0x0
        :pswitch_b
        :pswitch_b
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2820
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    if-eqz v0, :cond_0

    .line 2821
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->a:Lcom/google/checkout/inapp/proto/al;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2823
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 2824
    :goto_0
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2825
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->b:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2824
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2828
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 2829
    :goto_1
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 2830
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->c:[I

    aget v3, v3, v0

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2829
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2833
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    array-length v0, v0

    if-lez v0, :cond_3

    .line 2834
    :goto_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 2835
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$CreateProfilePostResponse;->d:[I

    aget v2, v2, v1

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2834
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2838
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2839
    return-void
.end method
