.class public final Lcom/google/checkout/inapp/proto/f;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile k:[Lcom/google/checkout/inapp/proto/f;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/checkout/inapp/proto/a/d;

.field public d:Lcom/google/checkout/inapp/proto/a/d;

.field public e:I

.field public f:Lcom/google/checkout/inapp/proto/p;

.field public g:Lcom/google/checkout/inapp/proto/a/d;

.field public h:I

.field public i:Z

.field public j:J


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 61
    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    iput v2, p0, Lcom/google/checkout/inapp/proto/f;->e:I

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    iput v2, p0, Lcom/google/checkout/inapp/proto/f;->h:I

    iput-boolean v2, p0, Lcom/google/checkout/inapp/proto/f;->i:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/checkout/inapp/proto/f;->j:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/f;->cachedSize:I

    .line 62
    return-void
.end method

.method public static a()[Lcom/google/checkout/inapp/proto/f;
    .locals 2

    .prologue
    .line 19
    sget-object v0, Lcom/google/checkout/inapp/proto/f;->k:[Lcom/google/checkout/inapp/proto/f;

    if-nez v0, :cond_1

    .line 20
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 22
    :try_start_0
    sget-object v0, Lcom/google/checkout/inapp/proto/f;->k:[Lcom/google/checkout/inapp/proto/f;

    if-nez v0, :cond_0

    .line 23
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/checkout/inapp/proto/f;

    sput-object v0, Lcom/google/checkout/inapp/proto/f;->k:[Lcom/google/checkout/inapp/proto/f;

    .line 25
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    :cond_1
    sget-object v0, Lcom/google/checkout/inapp/proto/f;->k:[Lcom/google/checkout/inapp/proto/f;

    return-object v0

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 214
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 215
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 216
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 220
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v1, :cond_2

    .line 224
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_2
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    if-eqz v1, :cond_3

    .line 228
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 231
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v1, :cond_4

    .line 232
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 235
    :cond_4
    iget v1, p0, Lcom/google/checkout/inapp/proto/f;->e:I

    if-eqz v1, :cond_5

    .line 236
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/checkout/inapp/proto/f;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 239
    :cond_5
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v1, :cond_6

    .line 240
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 243
    :cond_6
    iget v1, p0, Lcom/google/checkout/inapp/proto/f;->h:I

    if-eqz v1, :cond_7

    .line 244
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/checkout/inapp/proto/f;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 247
    :cond_7
    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/f;->i:Z

    if-eqz v1, :cond_8

    .line 248
    const/16 v1, 0x9

    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/f;->i:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 251
    :cond_8
    iget-wide v2, p0, Lcom/google/checkout/inapp/proto/f;->j:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_9

    .line 252
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/checkout/inapp/proto/f;->j:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 255
    :cond_9
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 81
    if-ne p1, p0, :cond_1

    .line 150
    :cond_0
    :goto_0
    return v0

    .line 84
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/f;

    if-nez v2, :cond_2

    move v0, v1

    .line 85
    goto :goto_0

    .line 87
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/f;

    .line 88
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 89
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 90
    goto :goto_0

    .line 92
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 93
    goto :goto_0

    .line 95
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 96
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 97
    goto :goto_0

    .line 99
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 100
    goto :goto_0

    .line 102
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v2, :cond_7

    .line 103
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v2, :cond_8

    move v0, v1

    .line 104
    goto :goto_0

    .line 107
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 108
    goto :goto_0

    .line 111
    :cond_8
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v2, :cond_9

    .line 112
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v2, :cond_a

    move v0, v1

    .line 113
    goto :goto_0

    .line 116
    :cond_9
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 117
    goto :goto_0

    .line 120
    :cond_a
    iget v2, p0, Lcom/google/checkout/inapp/proto/f;->e:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/f;->e:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 121
    goto :goto_0

    .line 123
    :cond_b
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    if-nez v2, :cond_c

    .line 124
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    if-eqz v2, :cond_d

    move v0, v1

    .line 125
    goto :goto_0

    .line 128
    :cond_c
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/p;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 129
    goto :goto_0

    .line 132
    :cond_d
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v2, :cond_e

    .line 133
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v2, :cond_f

    move v0, v1

    .line 134
    goto/16 :goto_0

    .line 137
    :cond_e
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 138
    goto/16 :goto_0

    .line 141
    :cond_f
    iget v2, p0, Lcom/google/checkout/inapp/proto/f;->h:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/f;->h:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 142
    goto/16 :goto_0

    .line 144
    :cond_10
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/f;->i:Z

    iget-boolean v3, p1, Lcom/google/checkout/inapp/proto/f;->i:Z

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 145
    goto/16 :goto_0

    .line 147
    :cond_11
    iget-wide v2, p0, Lcom/google/checkout/inapp/proto/f;->j:J

    iget-wide v4, p1, Lcom/google/checkout/inapp/proto/f;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 148
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 155
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 158
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 160
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 162
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 164
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/checkout/inapp/proto/f;->e:I

    add-int/2addr v0, v2

    .line 165
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 167
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 169
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/checkout/inapp/proto/f;->h:I

    add-int/2addr v0, v1

    .line 170
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/f;->i:Z

    if-eqz v0, :cond_6

    const/16 v0, 0x4cf

    :goto_6
    add-int/2addr v0, v1

    .line 171
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/checkout/inapp/proto/f;->j:J

    iget-wide v4, p0, Lcom/google/checkout/inapp/proto/f;->j:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 173
    return v0

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 158
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 160
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/a/d;->hashCode()I

    move-result v0

    goto :goto_2

    .line 162
    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/a/d;->hashCode()I

    move-result v0

    goto :goto_3

    .line 165
    :cond_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/p;->hashCode()I

    move-result v0

    goto :goto_4

    .line 167
    :cond_5
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v1}, Lcom/google/checkout/inapp/proto/a/d;->hashCode()I

    move-result v1

    goto :goto_5

    .line 170
    :cond_6
    const/16 v0, 0x4d5

    goto :goto_6
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/checkout/inapp/proto/p;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/p;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/checkout/inapp/proto/f;->e:I

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    :cond_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/checkout/inapp/proto/f;->h:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/checkout/inapp/proto/f;->i:Z

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/checkout/inapp/proto/f;->j:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/f;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 182
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 183
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/f;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 185
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_2

    .line 186
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/f;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 188
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    if-eqz v0, :cond_3

    .line 189
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/f;->f:Lcom/google/checkout/inapp/proto/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 191
    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_4

    .line 192
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/f;->d:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 194
    :cond_4
    iget v0, p0, Lcom/google/checkout/inapp/proto/f;->e:I

    if-eqz v0, :cond_5

    .line 195
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/checkout/inapp/proto/f;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 197
    :cond_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_6

    .line 198
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/f;->g:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 200
    :cond_6
    iget v0, p0, Lcom/google/checkout/inapp/proto/f;->h:I

    if-eqz v0, :cond_7

    .line 201
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/checkout/inapp/proto/f;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 203
    :cond_7
    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/f;->i:Z

    if-eqz v0, :cond_8

    .line 204
    const/16 v0, 0x9

    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/f;->i:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 206
    :cond_8
    iget-wide v0, p0, Lcom/google/checkout/inapp/proto/f;->j:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_9

    .line 207
    const/16 v0, 0xa

    iget-wide v2, p0, Lcom/google/checkout/inapp/proto/f;->j:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 209
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 210
    return-void
.end method
