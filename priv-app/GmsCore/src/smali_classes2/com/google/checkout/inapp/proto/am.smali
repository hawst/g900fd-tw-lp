.class public final Lcom/google/checkout/inapp/proto/am;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/checkout/inapp/proto/q;

.field public b:Ljava/lang/String;

.field public c:[Lcom/google/checkout/inapp/proto/h;

.field public d:Lcom/google/checkout/inapp/proto/r;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 946
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 947
    iput-object v1, p0, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/am;->b:Ljava/lang/String;

    invoke-static {}, Lcom/google/checkout/inapp/proto/h;->a()[Lcom/google/checkout/inapp/proto/h;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/am;->e:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/am;->cachedSize:I

    .line 948
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 1052
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1053
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/am;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 1054
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1057
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 1058
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1059
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    aget-object v2, v2, v0

    .line 1060
    if-eqz v2, :cond_1

    .line 1061
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1058
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 1066
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/am;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1067
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1070
    :cond_4
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v1, :cond_5

    .line 1071
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1074
    :cond_5
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    if-eqz v1, :cond_6

    .line 1075
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1078
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 962
    if-ne p1, p0, :cond_1

    .line 1005
    :cond_0
    :goto_0
    return v0

    .line 965
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/am;

    if-nez v2, :cond_2

    move v0, v1

    .line 966
    goto :goto_0

    .line 968
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/am;

    .line 969
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v2, :cond_3

    .line 970
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v2, :cond_4

    move v0, v1

    .line 971
    goto :goto_0

    .line 974
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 975
    goto :goto_0

    .line 978
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 979
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/am;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 980
    goto :goto_0

    .line 982
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/am;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 983
    goto :goto_0

    .line 985
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 987
    goto :goto_0

    .line 989
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    if-nez v2, :cond_8

    .line 990
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    if-eqz v2, :cond_9

    move v0, v1

    .line 991
    goto :goto_0

    .line 994
    :cond_8
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/r;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 995
    goto :goto_0

    .line 998
    :cond_9
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->e:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 999
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/am;->e:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1000
    goto :goto_0

    .line 1002
    :cond_a
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/am;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1003
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1010
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1013
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1015
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1017
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1019
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/am;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 1021
    return v0

    .line 1010
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/q;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1013
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1017
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/r;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1019
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/am;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 914
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/am;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/checkout/inapp/proto/h;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/checkout/inapp/proto/h;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/h;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/checkout/inapp/proto/h;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/h;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/am;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/checkout/inapp/proto/q;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/q;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    :cond_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/checkout/inapp/proto/r;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/r;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    :cond_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x3a -> :sswitch_3
        0x52 -> :sswitch_4
        0x5a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1027
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1028
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/am;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1030
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 1031
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 1032
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/am;->c:[Lcom/google/checkout/inapp/proto/h;

    aget-object v1, v1, v0

    .line 1033
    if-eqz v1, :cond_1

    .line 1034
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1031
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1038
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1039
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/am;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1041
    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v0, :cond_4

    .line 1042
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/am;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1044
    :cond_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    if-eqz v0, :cond_5

    .line 1045
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/am;->d:Lcom/google/checkout/inapp/proto/r;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1047
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1048
    return-void
.end method
