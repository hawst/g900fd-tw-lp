.class public final Lcom/google/checkout/inapp/proto/ap;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/checkout/inapp/proto/q;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/checkout/a/a/a/d;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2032
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2033
    iput-object v1, p0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->d:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/ap;->cachedSize:I

    .line 2034
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2123
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2124
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2125
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2128
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    if-eqz v1, :cond_1

    .line 2129
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2132
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ap;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 2133
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ap;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2136
    :cond_2
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v1, :cond_3

    .line 2137
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2140
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2047
    if-ne p1, p0, :cond_1

    .line 2086
    :cond_0
    :goto_0
    return v0

    .line 2050
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/ap;

    if-nez v2, :cond_2

    move v0, v1

    .line 2051
    goto :goto_0

    .line 2053
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/ap;

    .line 2054
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v2, :cond_3

    .line 2055
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2056
    goto :goto_0

    .line 2059
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2060
    goto :goto_0

    .line 2063
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 2064
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 2065
    goto :goto_0

    .line 2067
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 2068
    goto :goto_0

    .line 2070
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    if-nez v2, :cond_7

    .line 2071
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    if-eqz v2, :cond_8

    move v0, v1

    .line 2072
    goto :goto_0

    .line 2075
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    invoke-virtual {v2, v3}, Lcom/google/checkout/a/a/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 2076
    goto :goto_0

    .line 2079
    :cond_8
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ap;->d:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 2080
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ap;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 2081
    goto :goto_0

    .line 2083
    :cond_9
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ap;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ap;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2084
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2091
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2094
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2096
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2098
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ap;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 2100
    return v0

    .line 2091
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/q;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2094
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2096
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    invoke-virtual {v0}, Lcom/google/checkout/a/a/a/d;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2098
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ap;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2003
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/checkout/a/a/a/d;

    invoke-direct {v0}, Lcom/google/checkout/a/a/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/checkout/inapp/proto/q;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/q;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2106
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2107
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ap;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2109
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    if-eqz v0, :cond_1

    .line 2110
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ap;->c:Lcom/google/checkout/a/a/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2112
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 2113
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ap;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2115
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v0, :cond_3

    .line 2116
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ap;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2118
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2119
    return-void
.end method
