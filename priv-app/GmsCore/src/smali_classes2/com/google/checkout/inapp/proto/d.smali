.class public final Lcom/google/checkout/inapp/proto/d;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Lcom/google/checkout/inapp/proto/f;

.field public c:Lcom/google/checkout/inapp/proto/a/d;

.field public d:Ljava/lang/String;

.field public e:Lcom/google/checkout/inapp/proto/a/d;

.field public f:Lcom/google/checkout/inapp/proto/at;

.field public g:Lcom/google/checkout/inapp/proto/ar;

.field public h:Z

.field public i:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 59
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 60
    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    invoke-static {}, Lcom/google/checkout/inapp/proto/f;->a()[Lcom/google/checkout/inapp/proto/f;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/d;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    iput-boolean v2, p0, Lcom/google/checkout/inapp/proto/d;->h:Z

    iput v2, p0, Lcom/google/checkout/inapp/proto/d;->i:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/d;->cachedSize:I

    .line 61
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 211
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 212
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 213
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 217
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 218
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    aget-object v2, v2, v0

    .line 219
    if-eqz v2, :cond_1

    .line 220
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 217
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 225
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v1, :cond_4

    .line 226
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 229
    :cond_4
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    if-eqz v1, :cond_5

    .line 230
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    :cond_5
    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/d;->h:Z

    if-eqz v1, :cond_6

    .line 234
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/d;->h:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 237
    :cond_6
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    if-eqz v1, :cond_7

    .line 238
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 241
    :cond_7
    iget v1, p0, Lcom/google/checkout/inapp/proto/d;->i:I

    if-eqz v1, :cond_8

    .line 242
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/checkout/inapp/proto/d;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 245
    :cond_8
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v1, :cond_9

    .line 246
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 249
    :cond_9
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    .line 250
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    :cond_a
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 79
    if-ne p1, p0, :cond_1

    .line 146
    :cond_0
    :goto_0
    return v0

    .line 82
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/d;

    if-nez v2, :cond_2

    move v0, v1

    .line 83
    goto :goto_0

    .line 85
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/d;

    .line 86
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 87
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 88
    goto :goto_0

    .line 90
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 91
    goto :goto_0

    .line 93
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 95
    goto :goto_0

    .line 97
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v2, :cond_6

    .line 98
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v2, :cond_7

    move v0, v1

    .line 99
    goto :goto_0

    .line 102
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 103
    goto :goto_0

    .line 106
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->d:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 107
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/d;->d:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 108
    goto :goto_0

    .line 110
    :cond_8
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/d;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 111
    goto :goto_0

    .line 113
    :cond_9
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v2, :cond_a

    .line 114
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v2, :cond_b

    move v0, v1

    .line 115
    goto :goto_0

    .line 118
    :cond_a
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 119
    goto :goto_0

    .line 122
    :cond_b
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    if-nez v2, :cond_c

    .line 123
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    if-eqz v2, :cond_d

    move v0, v1

    .line 124
    goto :goto_0

    .line 127
    :cond_c
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/at;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 128
    goto/16 :goto_0

    .line 131
    :cond_d
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    if-nez v2, :cond_e

    .line 132
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    if-eqz v2, :cond_f

    move v0, v1

    .line 133
    goto/16 :goto_0

    .line 136
    :cond_e
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/ar;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 137
    goto/16 :goto_0

    .line 140
    :cond_f
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/d;->h:Z

    iget-boolean v3, p1, Lcom/google/checkout/inapp/proto/d;->h:Z

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 141
    goto/16 :goto_0

    .line 143
    :cond_10
    iget v2, p0, Lcom/google/checkout/inapp/proto/d;->i:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/d;->i:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 144
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 151
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 154
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 156
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 158
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 160
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 162
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 164
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 166
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/d;->h:Z

    if-eqz v0, :cond_6

    const/16 v0, 0x4cf

    :goto_6
    add-int/2addr v0, v1

    .line 167
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/checkout/inapp/proto/d;->i:I

    add-int/2addr v0, v1

    .line 168
    return v0

    .line 151
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/a/d;->hashCode()I

    move-result v0

    goto :goto_1

    .line 158
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 160
    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/a/d;->hashCode()I

    move-result v0

    goto :goto_3

    .line 162
    :cond_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/at;->hashCode()I

    move-result v0

    goto :goto_4

    .line 164
    :cond_5
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    invoke-virtual {v1}, Lcom/google/checkout/inapp/proto/ar;->hashCode()I

    move-result v1

    goto :goto_5

    .line 166
    :cond_6
    const/16 v0, 0x4d5

    goto :goto_6
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/checkout/inapp/proto/f;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/checkout/inapp/proto/f;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/f;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/checkout/inapp/proto/f;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/f;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    :cond_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/checkout/inapp/proto/at;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/at;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    :cond_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/checkout/inapp/proto/d;->h:Z

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/checkout/inapp/proto/ar;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/ar;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    :cond_6
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/checkout/inapp/proto/d;->i:I

    goto/16 :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    :cond_7
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/d;->d:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x30 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x52 -> :sswitch_8
        0x5a -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 175
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 178
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 179
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->b:[Lcom/google/checkout/inapp/proto/f;

    aget-object v1, v1, v0

    .line 180
    if-eqz v1, :cond_1

    .line 181
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 178
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 185
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_3

    .line 186
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->e:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 188
    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    if-eqz v0, :cond_4

    .line 189
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->f:Lcom/google/checkout/inapp/proto/at;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 191
    :cond_4
    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/d;->h:Z

    if-eqz v0, :cond_5

    .line 192
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/d;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 194
    :cond_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    if-eqz v0, :cond_6

    .line 195
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->g:Lcom/google/checkout/inapp/proto/ar;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 197
    :cond_6
    iget v0, p0, Lcom/google/checkout/inapp/proto/d;->i:I

    if-eqz v0, :cond_7

    .line 198
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/checkout/inapp/proto/d;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 200
    :cond_7
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_8

    .line 201
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 203
    :cond_8
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/d;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 204
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/d;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 206
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 207
    return-void
.end method
