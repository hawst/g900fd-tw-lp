.class public final Lcom/google/checkout/inapp/proto/n;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/checkout/inapp/proto/n;


# instance fields
.field public a:Lcom/google/checkout/inapp/proto/j;

.field public b:Lcom/google/checkout/inapp/proto/a/b;

.field public c:Lcom/google/checkout/inapp/proto/d;

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 38
    iput-object v0, p0, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/checkout/inapp/proto/n;->d:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/n;->cachedSize:I

    .line 39
    return-void
.end method

.method public static a()[Lcom/google/checkout/inapp/proto/n;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/checkout/inapp/proto/n;->e:[Lcom/google/checkout/inapp/proto/n;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/checkout/inapp/proto/n;->e:[Lcom/google/checkout/inapp/proto/n;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/checkout/inapp/proto/n;

    sput-object v0, Lcom/google/checkout/inapp/proto/n;->e:[Lcom/google/checkout/inapp/proto/n;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/checkout/inapp/proto/n;->e:[Lcom/google/checkout/inapp/proto/n;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 125
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 126
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    if-eqz v1, :cond_0

    .line 127
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 130
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    if-eqz v1, :cond_1

    .line 131
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 134
    :cond_1
    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/n;->d:Z

    if-eqz v1, :cond_2

    .line 135
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/n;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 138
    :cond_2
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_3

    .line 139
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 142
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 52
    if-ne p1, p0, :cond_1

    .line 89
    :cond_0
    :goto_0
    return v0

    .line 55
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/n;

    if-nez v2, :cond_2

    move v0, v1

    .line 56
    goto :goto_0

    .line 58
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/n;

    .line 59
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    if-nez v2, :cond_3

    .line 60
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    if-eqz v2, :cond_4

    move v0, v1

    .line 61
    goto :goto_0

    .line 64
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/j;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 65
    goto :goto_0

    .line 68
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v2, :cond_5

    .line 69
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_6

    move v0, v1

    .line 70
    goto :goto_0

    .line 73
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 74
    goto :goto_0

    .line 77
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    if-nez v2, :cond_7

    .line 78
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    if-eqz v2, :cond_8

    move v0, v1

    .line 79
    goto :goto_0

    .line 82
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 83
    goto :goto_0

    .line 86
    :cond_8
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/n;->d:Z

    iget-boolean v3, p1, Lcom/google/checkout/inapp/proto/n;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 87
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 94
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 97
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 99
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 101
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/n;->d:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x4cf

    :goto_3
    add-int/2addr v0, v1

    .line 102
    return v0

    .line 94
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/j;->hashCode()I

    move-result v0

    goto :goto_0

    .line 97
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/a/b;->hashCode()I

    move-result v0

    goto :goto_1

    .line 99
    :cond_2
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    invoke-virtual {v1}, Lcom/google/checkout/inapp/proto/d;->hashCode()I

    move-result v1

    goto :goto_2

    .line 101
    :cond_3
    const/16 v0, 0x4d5

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/checkout/inapp/proto/j;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/j;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/checkout/inapp/proto/d;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/d;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/checkout/inapp/proto/n;->d:Z

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x2a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_0

    .line 109
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/n;->a:Lcom/google/checkout/inapp/proto/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 111
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    if-eqz v0, :cond_1

    .line 112
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/n;->c:Lcom/google/checkout/inapp/proto/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 114
    :cond_1
    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/n;->d:Z

    if-eqz v0, :cond_2

    .line 115
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/n;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 117
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_3

    .line 118
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/n;->b:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 120
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 121
    return-void
.end method
