.class public final Lcom/google/checkout/inapp/proto/j;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile n:[Lcom/google/checkout/inapp/proto/j;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:I

.field public d:I

.field public e:Lcom/google/checkout/inapp/proto/a/b;

.field public f:Z

.field public g:[I

.field public h:I

.field public i:Ljava/lang/String;

.field public j:I

.field public k:I

.field public l:I

.field public m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 103
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 104
    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    iput v1, p0, Lcom/google/checkout/inapp/proto/j;->c:I

    const/16 v0, 0x64

    iput v0, p0, Lcom/google/checkout/inapp/proto/j;->d:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iput-boolean v1, p0, Lcom/google/checkout/inapp/proto/j;->f:Z

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    iput v1, p0, Lcom/google/checkout/inapp/proto/j;->h:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    iput v1, p0, Lcom/google/checkout/inapp/proto/j;->j:I

    iput v1, p0, Lcom/google/checkout/inapp/proto/j;->k:I

    iput v1, p0, Lcom/google/checkout/inapp/proto/j;->l:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/j;->cachedSize:I

    .line 105
    return-void
.end method

.method public static a()[Lcom/google/checkout/inapp/proto/j;
    .locals 2

    .prologue
    .line 53
    sget-object v0, Lcom/google/checkout/inapp/proto/j;->n:[Lcom/google/checkout/inapp/proto/j;

    if-nez v0, :cond_1

    .line 54
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 56
    :try_start_0
    sget-object v0, Lcom/google/checkout/inapp/proto/j;->n:[Lcom/google/checkout/inapp/proto/j;

    if-nez v0, :cond_0

    .line 57
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/checkout/inapp/proto/j;

    sput-object v0, Lcom/google/checkout/inapp/proto/j;->n:[Lcom/google/checkout/inapp/proto/j;

    .line 59
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    :cond_1
    sget-object v0, Lcom/google/checkout/inapp/proto/j;->n:[Lcom/google/checkout/inapp/proto/j;

    return-object v0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 273
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 274
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 275
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 278
    :cond_0
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 279
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 282
    :cond_1
    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->c:I

    if-eqz v2, :cond_2

    .line 283
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/checkout/inapp/proto/j;->c:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 286
    :cond_2
    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->d:I

    const/16 v3, 0x64

    if-eq v2, v3, :cond_3

    .line 287
    const/4 v2, 0x4

    iget v3, p0, Lcom/google/checkout/inapp/proto/j;->d:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 290
    :cond_3
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/j;->f:Z

    if-eqz v2, :cond_4

    .line 291
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/checkout/inapp/proto/j;->f:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 294
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v1

    .line 296
    :goto_0
    iget-object v3, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 297
    iget-object v3, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    aget v3, v3, v1

    .line 298
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 296
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 301
    :cond_5
    add-int/2addr v0, v2

    .line 302
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 304
    :cond_6
    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->h:I

    if-eqz v1, :cond_7

    .line 305
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 308
    :cond_7
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_8

    .line 309
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 312
    :cond_8
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 313
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 316
    :cond_9
    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->j:I

    if-eqz v1, :cond_a

    .line 317
    const/16 v1, 0xe

    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->j:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 320
    :cond_a
    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->k:I

    if-eqz v1, :cond_b

    .line 321
    const/16 v1, 0xf

    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->k:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 324
    :cond_b
    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->l:I

    if-eqz v1, :cond_c

    .line 325
    const/16 v1, 0x10

    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->l:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 328
    :cond_c
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 329
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 332
    :cond_d
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 127
    if-ne p1, p0, :cond_1

    .line 196
    :cond_0
    :goto_0
    return v0

    .line 130
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/j;

    if-nez v2, :cond_2

    move v0, v1

    .line 131
    goto :goto_0

    .line 133
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/j;

    .line 134
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 135
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 136
    goto :goto_0

    .line 138
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 139
    goto :goto_0

    .line 141
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 142
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 143
    goto :goto_0

    .line 145
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 146
    goto :goto_0

    .line 148
    :cond_6
    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->c:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/j;->c:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 149
    goto :goto_0

    .line 151
    :cond_7
    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->d:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/j;->d:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 152
    goto :goto_0

    .line 154
    :cond_8
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v2, :cond_9

    .line 155
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_a

    move v0, v1

    .line 156
    goto :goto_0

    .line 159
    :cond_9
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 160
    goto :goto_0

    .line 163
    :cond_a
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/j;->f:Z

    iget-boolean v3, p1, Lcom/google/checkout/inapp/proto/j;->f:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 164
    goto :goto_0

    .line 166
    :cond_b
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/j;->g:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 168
    goto :goto_0

    .line 170
    :cond_c
    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->h:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/j;->h:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 171
    goto :goto_0

    .line 173
    :cond_d
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 174
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    if-eqz v2, :cond_f

    move v0, v1

    .line 175
    goto :goto_0

    .line 177
    :cond_e
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 178
    goto/16 :goto_0

    .line 180
    :cond_f
    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->j:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/j;->j:I

    if-eq v2, v3, :cond_10

    move v0, v1

    .line 181
    goto/16 :goto_0

    .line 183
    :cond_10
    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->k:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/j;->k:I

    if-eq v2, v3, :cond_11

    move v0, v1

    .line 184
    goto/16 :goto_0

    .line 186
    :cond_11
    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->l:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/j;->l:I

    if-eq v2, v3, :cond_12

    move v0, v1

    .line 187
    goto/16 :goto_0

    .line 189
    :cond_12
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    if-nez v2, :cond_13

    .line 190
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 191
    goto/16 :goto_0

    .line 193
    :cond_13
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 194
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 201
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 204
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 206
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->c:I

    add-int/2addr v0, v2

    .line 207
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->d:I

    add-int/2addr v0, v2

    .line 208
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 210
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/j;->f:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x4cf

    :goto_3
    add-int/2addr v0, v2

    .line 211
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v2

    add-int/2addr v0, v2

    .line 213
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->h:I

    add-int/2addr v0, v2

    .line 214
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 216
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->j:I

    add-int/2addr v0, v2

    .line 217
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->k:I

    add-int/2addr v0, v2

    .line 218
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/checkout/inapp/proto/j;->l:I

    add-int/2addr v0, v2

    .line 219
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 221
    return v0

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 204
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 208
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/a/b;->hashCode()I

    move-result v0

    goto :goto_2

    .line 210
    :cond_3
    const/16 v0, 0x4d5

    goto :goto_3

    .line 214
    :cond_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 219
    :cond_5
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_4
    iput v0, p0, Lcom/google/checkout/inapp/proto/j;->c:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_2

    goto :goto_0

    :sswitch_6
    iput v0, p0, Lcom/google/checkout/inapp/proto/j;->d:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/checkout/inapp/proto/j;->f:Z

    goto :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    :cond_a
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_0

    :pswitch_3
    iput v0, p0, Lcom/google/checkout/inapp/proto/j;->h:I

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    :cond_b
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/checkout/inapp/proto/j;->j:I

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/checkout/inapp/proto/j;->k:I

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_4

    goto/16 :goto_0

    :pswitch_4
    iput v0, p0, Lcom/google/checkout/inapp/proto/j;->l:I

    goto/16 :goto_0

    :sswitch_10
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_5
        0x30 -> :sswitch_7
        0x40 -> :sswitch_8
        0x42 -> :sswitch_9
        0x48 -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x70 -> :sswitch_d
        0x78 -> :sswitch_e
        0x80 -> :sswitch_f
        0x8a -> :sswitch_10
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_4
        0x1 -> :sswitch_4
        0x2 -> :sswitch_4
        0x3 -> :sswitch_4
        0x4 -> :sswitch_4
        0x5 -> :sswitch_4
        0x6 -> :sswitch_4
        0x7 -> :sswitch_4
        0x1b -> :sswitch_4
        0x20 -> :sswitch_4
        0x21 -> :sswitch_4
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x0 -> :sswitch_6
        0x7 -> :sswitch_6
        0x64 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x0
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 228
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 231
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 233
    :cond_1
    iget v0, p0, Lcom/google/checkout/inapp/proto/j;->c:I

    if-eqz v0, :cond_2

    .line 234
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 236
    :cond_2
    iget v0, p0, Lcom/google/checkout/inapp/proto/j;->d:I

    const/16 v1, 0x64

    if-eq v0, v1, :cond_3

    .line 237
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 239
    :cond_3
    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/j;->f:Z

    if-eqz v0, :cond_4

    .line 240
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/j;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 242
    :cond_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v0, v0

    if-lez v0, :cond_5

    .line 243
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 244
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/j;->g:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 243
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247
    :cond_5
    iget v0, p0, Lcom/google/checkout/inapp/proto/j;->h:I

    if-eqz v0, :cond_6

    .line 248
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 250
    :cond_6
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_7

    .line 251
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->e:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 253
    :cond_7
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 254
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 256
    :cond_8
    iget v0, p0, Lcom/google/checkout/inapp/proto/j;->j:I

    if-eqz v0, :cond_9

    .line 257
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 259
    :cond_9
    iget v0, p0, Lcom/google/checkout/inapp/proto/j;->k:I

    if-eqz v0, :cond_a

    .line 260
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->k:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 262
    :cond_a
    iget v0, p0, Lcom/google/checkout/inapp/proto/j;->l:I

    if-eqz v0, :cond_b

    .line 263
    const/16 v0, 0x10

    iget v1, p0, Lcom/google/checkout/inapp/proto/j;->l:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 265
    :cond_b
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_c

    .line 266
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/j;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 268
    :cond_c
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 269
    return-void
.end method
