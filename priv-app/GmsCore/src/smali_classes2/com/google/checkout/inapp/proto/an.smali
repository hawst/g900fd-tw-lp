.class public final Lcom/google/checkout/inapp/proto/an;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:[I

.field public d:Lcom/google/checkout/inapp/proto/al;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1203
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1204
    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/an;->a:Ljava/lang/String;

    const/16 v0, 0x64

    iput v0, p0, Lcom/google/checkout/inapp/proto/an;->b:I

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/an;->cachedSize:I

    .line 1205
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1286
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1287
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/an;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1288
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/an;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1291
    :cond_0
    iget v2, p0, Lcom/google/checkout/inapp/proto/an;->b:I

    const/16 v3, 0x64

    if-eq v2, v3, :cond_1

    .line 1292
    const/4 v2, 0x5

    iget v3, p0, Lcom/google/checkout/inapp/proto/an;->b:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 1295
    :cond_1
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    .line 1297
    :goto_0
    iget-object v3, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 1298
    iget-object v3, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    aget v3, v3, v1

    .line 1299
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 1297
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1302
    :cond_2
    add-int/2addr v0, v2

    .line 1303
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1305
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    if-eqz v1, :cond_4

    .line 1306
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1309
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1218
    if-ne p1, p0, :cond_1

    .line 1248
    :cond_0
    :goto_0
    return v0

    .line 1221
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/an;

    if-nez v2, :cond_2

    move v0, v1

    .line 1222
    goto :goto_0

    .line 1224
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/an;

    .line 1225
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/an;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 1226
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/an;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 1227
    goto :goto_0

    .line 1229
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/an;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/an;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1230
    goto :goto_0

    .line 1232
    :cond_4
    iget v2, p0, Lcom/google/checkout/inapp/proto/an;->b:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/an;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 1233
    goto :goto_0

    .line 1235
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/an;->c:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1237
    goto :goto_0

    .line 1239
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    if-nez v2, :cond_7

    .line 1240
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1241
    goto :goto_0

    .line 1244
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/al;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1245
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1253
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/an;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1256
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/checkout/inapp/proto/an;->b:I

    add-int/2addr v0, v2

    .line 1257
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v2

    add-int/2addr v0, v2

    .line 1259
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1261
    return v0

    .line 1253
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/an;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1259
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    invoke-virtual {v1}, Lcom/google/checkout/inapp/proto/al;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1154
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/an;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_3
    iput v0, p0, Lcom/google/checkout/inapp/proto/an;->b:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x30

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    :pswitch_2
    goto :goto_4

    :pswitch_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    :pswitch_4
    goto :goto_6

    :pswitch_5
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    :cond_a
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/checkout/inapp/proto/al;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/al;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    :cond_b
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x28 -> :sswitch_2
        0x30 -> :sswitch_4
        0x32 -> :sswitch_5
        0x42 -> :sswitch_6
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_3
        0x1 -> :sswitch_3
        0x3 -> :sswitch_3
        0x4 -> :sswitch_3
        0x5 -> :sswitch_3
        0x6 -> :sswitch_3
        0x7 -> :sswitch_3
        0x8 -> :sswitch_3
        0x9 -> :sswitch_3
        0xa -> :sswitch_3
        0xb -> :sswitch_3
        0xc -> :sswitch_3
        0xd -> :sswitch_3
        0x64 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1267
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/an;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1268
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/an;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1270
    :cond_0
    iget v0, p0, Lcom/google/checkout/inapp/proto/an;->b:I

    const/16 v1, 0x64

    if-eq v0, v1, :cond_1

    .line 1271
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/checkout/inapp/proto/an;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1273
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    array-length v0, v0

    if-lez v0, :cond_2

    .line 1274
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 1275
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/an;->c:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1274
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1278
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    if-eqz v0, :cond_3

    .line 1279
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/an;->d:Lcom/google/checkout/inapp/proto/al;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1281
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1282
    return-void
.end method
