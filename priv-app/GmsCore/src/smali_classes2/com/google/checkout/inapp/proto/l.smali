.class public final Lcom/google/checkout/inapp/proto/l;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/checkout/inapp/proto/a/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 32
    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/l;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/l;->b:Lcom/google/checkout/inapp/proto/a/b;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/l;->cachedSize:I

    .line 33
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 94
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 95
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/l;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 96
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/l;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/l;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v1, :cond_1

    .line 100
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/l;->b:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 44
    if-ne p1, p0, :cond_1

    .line 67
    :cond_0
    :goto_0
    return v0

    .line 47
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/l;

    if-nez v2, :cond_2

    move v0, v1

    .line 48
    goto :goto_0

    .line 50
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/l;

    .line 51
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/l;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 52
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/l;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 53
    goto :goto_0

    .line 55
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/l;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/l;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 56
    goto :goto_0

    .line 58
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/l;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v2, :cond_5

    .line 59
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/l;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_0

    move v0, v1

    .line 60
    goto :goto_0

    .line 63
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/l;->b:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/l;->b:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 64
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 72
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/l;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 75
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/l;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 77
    return v0

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/l;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 75
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/l;->b:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v1}, Lcom/google/checkout/inapp/proto/a/b;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/l;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/l;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/l;->b:Lcom/google/checkout/inapp/proto/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/l;->b:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
        0x3a -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/l;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 84
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/l;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 86
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/l;->b:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_1

    .line 87
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/l;->b:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 89
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 90
    return-void
.end method
