.class public final Lcom/google/checkout/inapp/proto/ab;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/checkout/inapp/proto/q;

.field public b:Lcom/google/checkout/a/a/b/b;

.field public c:Ljava/lang/String;

.field public d:[Ljava/lang/String;

.field public e:[Lcom/google/checkout/inapp/proto/y;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2474
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 2475
    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->c:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    invoke-static {}, Lcom/google/checkout/inapp/proto/y;->a()[Lcom/google/checkout/inapp/proto/y;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->f:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/ab;->cachedSize:I

    .line 2476
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2595
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 2596
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    if-eqz v1, :cond_0

    .line 2597
    const/4 v1, 0x2

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2600
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ab;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 2601
    const/4 v1, 0x3

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/ab;->c:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2604
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_4

    move v1, v2

    move v3, v2

    move v4, v2

    .line 2607
    :goto_0
    iget-object v5, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_3

    .line 2608
    iget-object v5, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 2609
    if-eqz v5, :cond_2

    .line 2610
    add-int/lit8 v4, v4, 0x1

    .line 2611
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 2607
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2615
    :cond_3
    add-int/2addr v0, v3

    .line 2616
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 2618
    :cond_4
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ab;->f:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 2619
    const/16 v1, 0x9

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/ab;->f:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2622
    :cond_5
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v1, :cond_6

    .line 2623
    const/16 v1, 0xa

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2626
    :cond_6
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    array-length v1, v1

    if-lez v1, :cond_8

    .line 2627
    :goto_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    array-length v1, v1

    if-ge v2, v1, :cond_8

    .line 2628
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    aget-object v1, v1, v2

    .line 2629
    if-eqz v1, :cond_7

    .line 2630
    const/16 v3, 0xb

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2627
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 2635
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 2491
    if-ne p1, p0, :cond_1

    .line 2538
    :cond_0
    :goto_0
    return v0

    .line 2494
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/ab;

    if-nez v2, :cond_2

    move v0, v1

    .line 2495
    goto :goto_0

    .line 2497
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/ab;

    .line 2498
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v2, :cond_3

    .line 2499
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v2, :cond_4

    move v0, v1

    .line 2500
    goto :goto_0

    .line 2503
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 2504
    goto :goto_0

    .line 2507
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    if-nez v2, :cond_5

    .line 2508
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    if-eqz v2, :cond_6

    move v0, v1

    .line 2509
    goto :goto_0

    .line 2512
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    invoke-virtual {v2, v3}, Lcom/google/checkout/a/a/b/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 2513
    goto :goto_0

    .line 2516
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 2517
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ab;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 2518
    goto :goto_0

    .line 2520
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ab;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 2521
    goto :goto_0

    .line 2523
    :cond_8
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 2525
    goto :goto_0

    .line 2527
    :cond_9
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 2529
    goto :goto_0

    .line 2531
    :cond_a
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->f:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 2532
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ab;->f:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 2533
    goto :goto_0

    .line 2535
    :cond_b
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ab;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 2536
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2543
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2546
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2548
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2550
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2552
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2554
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->f:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 2556
    return v0

    .line 2543
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/q;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2546
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    invoke-virtual {v0}, Lcom/google/checkout/a/a/b/b;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2548
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2554
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ab;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2439
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/checkout/a/a/b/b;

    invoke-direct {v0}, Lcom/google/checkout/a/a/b/b;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/checkout/inapp/proto/q;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/q;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    :cond_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    const/16 v0, 0x5a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/checkout/inapp/proto/y;

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Lcom/google/checkout/inapp/proto/y;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/y;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Lcom/google/checkout/inapp/proto/y;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/y;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x3a -> :sswitch_3
        0x4a -> :sswitch_4
        0x52 -> :sswitch_5
        0x5a -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2562
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    if-eqz v0, :cond_0

    .line 2563
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->b:Lcom/google/checkout/a/a/b/b;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2565
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 2566
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2568
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 2569
    :goto_0
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 2570
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->d:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 2571
    if-eqz v2, :cond_2

    .line 2572
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2569
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2576
    :cond_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->f:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 2577
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2579
    :cond_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v0, :cond_5

    .line 2580
    const/16 v0, 0xa

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ab;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2582
    :cond_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    array-length v0, v0

    if-lez v0, :cond_7

    .line 2583
    :goto_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 2584
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ab;->e:[Lcom/google/checkout/inapp/proto/y;

    aget-object v0, v0, v1

    .line 2585
    if-eqz v0, :cond_6

    .line 2586
    const/16 v2, 0xb

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2583
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2590
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2591
    return-void
.end method
