.class public final Lcom/google/checkout/inapp/proto/z;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/checkout/inapp/proto/q;

.field public b:Lcom/google/t/a/b;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3155
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3156
    iput-object v0, p0, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/z;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/z;->d:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/z;->cachedSize:I

    .line 3157
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3246
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3247
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    if-eqz v1, :cond_0

    .line 3248
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3251
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/z;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 3252
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/z;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3255
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/z;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3256
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/z;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3259
    :cond_2
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v1, :cond_3

    .line 3260
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3263
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3170
    if-ne p1, p0, :cond_1

    .line 3209
    :cond_0
    :goto_0
    return v0

    .line 3173
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/z;

    if-nez v2, :cond_2

    move v0, v1

    .line 3174
    goto :goto_0

    .line 3176
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/z;

    .line 3177
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v2, :cond_3

    .line 3178
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v2, :cond_4

    move v0, v1

    .line 3179
    goto :goto_0

    .line 3182
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 3183
    goto :goto_0

    .line 3186
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    if-nez v2, :cond_5

    .line 3187
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    if-eqz v2, :cond_6

    move v0, v1

    .line 3188
    goto :goto_0

    .line 3191
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    invoke-virtual {v2, v3}, Lcom/google/t/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 3192
    goto :goto_0

    .line 3195
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/z;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 3196
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/z;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 3197
    goto :goto_0

    .line 3199
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/z;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/z;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 3200
    goto :goto_0

    .line 3202
    :cond_8
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/z;->d:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 3203
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/z;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 3204
    goto :goto_0

    .line 3206
    :cond_9
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/z;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/z;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 3207
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3214
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3217
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3219
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/z;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3221
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/z;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 3223
    return v0

    .line 3214
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/q;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3217
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    invoke-virtual {v0}, Lcom/google/t/a/b;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3219
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/z;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 3221
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/z;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3126
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/t/a/b;

    invoke-direct {v0}, Lcom/google/t/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/z;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/z;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/checkout/inapp/proto/q;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/q;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3229
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    if-eqz v0, :cond_0

    .line 3230
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/z;->b:Lcom/google/t/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3232
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/z;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 3233
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/z;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3235
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/z;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3236
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/z;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3238
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v0, :cond_3

    .line 3239
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/z;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3241
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3242
    return-void
.end method
