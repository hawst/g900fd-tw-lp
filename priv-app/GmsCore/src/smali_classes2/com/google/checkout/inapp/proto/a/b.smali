.class public final Lcom/google/checkout/inapp/proto/a/b;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile k:[Lcom/google/checkout/inapp/proto/a/b;


# instance fields
.field public a:Lcom/google/t/a/b;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Z

.field public f:Z

.field public g:Z

.field public h:Z

.field public i:[I

.field public j:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 60
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 61
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->e:Z

    iput-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    iput-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    iput-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    iput-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->j:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/a/b;->cachedSize:I

    .line 62
    return-void
.end method

.method public static a()[Lcom/google/checkout/inapp/proto/a/b;
    .locals 2

    .prologue
    .line 19
    sget-object v0, Lcom/google/checkout/inapp/proto/a/b;->k:[Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_1

    .line 20
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 22
    :try_start_0
    sget-object v0, Lcom/google/checkout/inapp/proto/a/b;->k:[Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_0

    .line 23
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/checkout/inapp/proto/a/b;

    sput-object v0, Lcom/google/checkout/inapp/proto/a/b;->k:[Lcom/google/checkout/inapp/proto/a/b;

    .line 25
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    :cond_1
    sget-object v0, Lcom/google/checkout/inapp/proto/a/b;->k:[Lcom/google/checkout/inapp/proto/a/b;

    return-object v0

    .line 25
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 201
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 202
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v2, :cond_0

    .line 203
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 206
    :cond_0
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 207
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 210
    :cond_1
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 211
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/a/b;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 214
    :cond_2
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 215
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 218
    :cond_3
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/a/b;->e:Z

    if-eqz v2, :cond_4

    .line 219
    const/4 v2, 0x5

    iget-boolean v3, p0, Lcom/google/checkout/inapp/proto/a/b;->e:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 222
    :cond_4
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    if-eqz v2, :cond_5

    .line 223
    const/4 v2, 0x6

    iget-boolean v3, p0, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 226
    :cond_5
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    if-eqz v2, :cond_6

    .line 227
    const/4 v2, 0x7

    iget-boolean v3, p0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 230
    :cond_6
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    if-eqz v2, :cond_7

    .line 231
    const/16 v2, 0x8

    iget-boolean v3, p0, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 234
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    array-length v2, v2

    if-lez v2, :cond_9

    move v2, v1

    .line 236
    :goto_0
    iget-object v3, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    array-length v3, v3

    if-ge v1, v3, :cond_8

    .line 237
    iget-object v3, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    aget v3, v3, v1

    .line 238
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 236
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 241
    :cond_8
    add-int/2addr v0, v2

    .line 242
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 244
    :cond_9
    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->j:Z

    if-eqz v1, :cond_a

    .line 245
    const/16 v1, 0x47e

    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/a/b;->j:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 248
    :cond_a
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 81
    if-ne p1, p0, :cond_1

    .line 137
    :cond_0
    :goto_0
    return v0

    .line 84
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/a/b;

    if-nez v2, :cond_2

    move v0, v1

    .line 85
    goto :goto_0

    .line 87
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/a/b;

    .line 88
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-nez v2, :cond_3

    .line 89
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v2, :cond_4

    move v0, v1

    .line 90
    goto :goto_0

    .line 93
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-virtual {v2, v3}, Lcom/google/t/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 94
    goto :goto_0

    .line 97
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 98
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 99
    goto :goto_0

    .line 101
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 102
    goto :goto_0

    .line 104
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->c:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 105
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/a/b;->c:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 106
    goto :goto_0

    .line 108
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/a/b;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 109
    goto :goto_0

    .line 111
    :cond_8
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 112
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 113
    goto :goto_0

    .line 115
    :cond_9
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 116
    goto :goto_0

    .line 118
    :cond_a
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/a/b;->e:Z

    iget-boolean v3, p1, Lcom/google/checkout/inapp/proto/a/b;->e:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 119
    goto :goto_0

    .line 121
    :cond_b
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    iget-boolean v3, p1, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    if-eq v2, v3, :cond_c

    move v0, v1

    .line 122
    goto :goto_0

    .line 124
    :cond_c
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    iget-boolean v3, p1, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 125
    goto :goto_0

    .line 127
    :cond_d
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    iget-boolean v3, p1, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 128
    goto :goto_0

    .line 130
    :cond_e
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 132
    goto/16 :goto_0

    .line 134
    :cond_f
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/a/b;->j:Z

    iget-boolean v3, p1, Lcom/google/checkout/inapp/proto/a/b;->j:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 135
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    .line 142
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 145
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v4

    .line 147
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 149
    mul-int/lit8 v0, v0, 0x1f

    iget-object v4, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    if-nez v4, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 151
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->e:Z

    if-eqz v0, :cond_4

    move v0, v2

    :goto_4
    add-int/2addr v0, v1

    .line 152
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    if-eqz v0, :cond_5

    move v0, v2

    :goto_5
    add-int/2addr v0, v1

    .line 153
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    if-eqz v0, :cond_6

    move v0, v2

    :goto_6
    add-int/2addr v0, v1

    .line 154
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    if-eqz v0, :cond_7

    move v0, v2

    :goto_7
    add-int/2addr v0, v1

    .line 155
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 157
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->j:Z

    if-eqz v1, :cond_8

    :goto_8
    add-int/2addr v0, v2

    .line 158
    return v0

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-virtual {v0}, Lcom/google/t/a/b;->hashCode()I

    move-result v0

    goto :goto_0

    .line 145
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 147
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 149
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    :cond_4
    move v0, v3

    .line 151
    goto :goto_4

    :cond_5
    move v0, v3

    .line 152
    goto :goto_5

    :cond_6
    move v0, v3

    .line 153
    goto :goto_6

    :cond_7
    move v0, v3

    .line 154
    goto :goto_7

    :cond_8
    move v2, v3

    .line 157
    goto :goto_8
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/t/a/b;

    invoke-direct {v0}, Lcom/google/t/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->e:Z

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    goto :goto_0

    :sswitch_9
    const/16 v0, 0x48

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    if-nez v0, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_5

    array-length v3, v5

    if-ne v1, v3, :cond_5

    iput-object v5, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    array-length v0, v0

    goto :goto_3

    :cond_5
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_6

    iget-object v4, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    if-eqz v0, :cond_b

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    if-nez v1, :cond_9

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_a

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_9
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    array-length v1, v1

    goto :goto_5

    :cond_a
    iput-object v4, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    :cond_b
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->j:Z

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x4a -> :sswitch_a
        0x23f0 -> :sswitch_b
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    if-eqz v0, :cond_0

    .line 165
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->a:Lcom/google/t/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 167
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 168
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 170
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 171
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 173
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 174
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 176
    :cond_3
    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->e:Z

    if-eqz v0, :cond_4

    .line 177
    const/4 v0, 0x5

    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->e:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 179
    :cond_4
    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    if-eqz v0, :cond_5

    .line 180
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 182
    :cond_5
    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    if-eqz v0, :cond_6

    .line 183
    const/4 v0, 0x7

    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->g:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 185
    :cond_6
    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    if-eqz v0, :cond_7

    .line 186
    const/16 v0, 0x8

    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->h:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 188
    :cond_7
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    array-length v0, v0

    if-lez v0, :cond_8

    .line 189
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    array-length v1, v1

    if-ge v0, v1, :cond_8

    .line 190
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/a/b;->i:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 189
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 193
    :cond_8
    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/a/b;->j:Z

    if-eqz v0, :cond_9

    .line 194
    const/16 v0, 0x47e

    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/a/b;->j:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 196
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 197
    return-void
.end method
