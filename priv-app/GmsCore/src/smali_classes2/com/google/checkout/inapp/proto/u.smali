.class public final Lcom/google/checkout/inapp/proto/u;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/checkout/inapp/proto/q;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/checkout/inapp/proto/a/d;

.field public d:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5726
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5727
    iput-object v1, p0, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/u;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/checkout/inapp/proto/u;->d:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/u;->cachedSize:I

    .line 5728
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5812
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 5813
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v1, :cond_0

    .line 5814
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5817
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/u;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 5818
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/u;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5821
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v1, :cond_2

    .line 5822
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5825
    :cond_2
    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/u;->d:Z

    if-eqz v1, :cond_3

    .line 5826
    const/4 v1, 0x4

    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/u;->d:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5829
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5741
    if-ne p1, p0, :cond_1

    .line 5776
    :cond_0
    :goto_0
    return v0

    .line 5744
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/u;

    if-nez v2, :cond_2

    move v0, v1

    .line 5745
    goto :goto_0

    .line 5747
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/u;

    .line 5748
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v2, :cond_3

    .line 5749
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v2, :cond_4

    move v0, v1

    .line 5750
    goto :goto_0

    .line 5753
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/q;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 5754
    goto :goto_0

    .line 5757
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/u;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 5758
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/u;->b:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 5759
    goto :goto_0

    .line 5761
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/u;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/u;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 5762
    goto :goto_0

    .line 5764
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v2, :cond_7

    .line 5765
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v2, :cond_8

    move v0, v1

    .line 5766
    goto :goto_0

    .line 5769
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 5770
    goto :goto_0

    .line 5773
    :cond_8
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/u;->d:Z

    iget-boolean v3, p1, Lcom/google/checkout/inapp/proto/u;->d:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 5774
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5781
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 5784
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/u;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 5786
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 5788
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/u;->d:Z

    if-eqz v0, :cond_3

    const/16 v0, 0x4cf

    :goto_3
    add-int/2addr v0, v1

    .line 5789
    return v0

    .line 5781
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/q;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5784
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/u;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 5786
    :cond_2
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v1}, Lcom/google/checkout/inapp/proto/a/d;->hashCode()I

    move-result v1

    goto :goto_2

    .line 5788
    :cond_3
    const/16 v0, 0x4d5

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5697
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/checkout/inapp/proto/q;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/q;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/u;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/checkout/inapp/proto/u;->d:Z

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5795
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    if-eqz v0, :cond_0

    .line 5796
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/u;->a:Lcom/google/checkout/inapp/proto/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5798
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/u;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 5799
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/u;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5801
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_2

    .line 5802
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/u;->c:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5804
    :cond_2
    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/u;->d:Z

    if-eqz v0, :cond_3

    .line 5805
    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/u;->d:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5807
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5808
    return-void
.end method
