.class public final Lcom/google/checkout/inapp/proto/at;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/checkout/inapp/proto/a/d;

.field public b:Z

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/checkout/inapp/proto/at;->b:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/at;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/at;->cachedSize:I

    .line 41
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 105
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 106
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v1, :cond_0

    .line 107
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    :cond_0
    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/at;->b:Z

    if-eqz v1, :cond_1

    .line 111
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/at;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 114
    :cond_1
    iget v1, p0, Lcom/google/checkout/inapp/proto/at;->c:I

    if-eq v1, v3, :cond_2

    .line 115
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/checkout/inapp/proto/at;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 53
    if-ne p1, p0, :cond_1

    .line 75
    :cond_0
    :goto_0
    return v0

    .line 56
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/at;

    if-nez v2, :cond_2

    move v0, v1

    .line 57
    goto :goto_0

    .line 59
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/at;

    .line 60
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v2, :cond_3

    .line 61
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v2, :cond_4

    move v0, v1

    .line 62
    goto :goto_0

    .line 65
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/d;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 66
    goto :goto_0

    .line 69
    :cond_4
    iget-boolean v2, p0, Lcom/google/checkout/inapp/proto/at;->b:Z

    iget-boolean v3, p1, Lcom/google/checkout/inapp/proto/at;->b:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 70
    goto :goto_0

    .line 72
    :cond_5
    iget v2, p0, Lcom/google/checkout/inapp/proto/at;->c:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/at;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 73
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 83
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/at;->b:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v1

    .line 84
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/checkout/inapp/proto/at;->c:I

    add-int/2addr v0, v1

    .line 85
    return v0

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/a/d;->hashCode()I

    move-result v0

    goto :goto_0

    .line 83
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/checkout/inapp/proto/a/d;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/checkout/inapp/proto/at;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/checkout/inapp/proto/at;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 91
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/at;->a:Lcom/google/checkout/inapp/proto/a/d;

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 94
    :cond_0
    iget-boolean v0, p0, Lcom/google/checkout/inapp/proto/at;->b:Z

    if-eqz v0, :cond_1

    .line 95
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/checkout/inapp/proto/at;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 97
    :cond_1
    iget v0, p0, Lcom/google/checkout/inapp/proto/at;->c:I

    if-eq v0, v2, :cond_2

    .line 98
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/checkout/inapp/proto/at;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 100
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 101
    return-void
.end method
