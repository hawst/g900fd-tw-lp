.class public final Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/checkout/inapp/proto/a/b;

.field public b:Lcom/google/checkout/inapp/proto/al;

.field public c:[I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3799
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3800
    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->cachedSize:I

    .line 3801
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3876
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3877
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_0

    .line 3878
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3881
    :cond_0
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    if-eqz v2, :cond_1

    .line 3882
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3885
    :cond_1
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    .line 3887
    :goto_0
    iget-object v3, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    array-length v3, v3

    if-ge v1, v3, :cond_2

    .line 3888
    iget-object v3, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    aget v3, v3, v1

    .line 3889
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 3887
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 3892
    :cond_2
    add-int/2addr v0, v2

    .line 3893
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3895
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3813
    if-ne p1, p0, :cond_1

    .line 3842
    :cond_0
    :goto_0
    return v0

    .line 3816
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;

    if-nez v2, :cond_2

    move v0, v1

    .line 3817
    goto :goto_0

    .line 3819
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;

    .line 3820
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v2, :cond_3

    .line 3821
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_4

    move v0, v1

    .line 3822
    goto :goto_0

    .line 3825
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 3826
    goto :goto_0

    .line 3829
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    if-nez v2, :cond_5

    .line 3830
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    if-eqz v2, :cond_6

    move v0, v1

    .line 3831
    goto :goto_0

    .line 3834
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    invoke-virtual {v2, v3}, Lcom/google/checkout/inapp/proto/al;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 3835
    goto :goto_0

    .line 3838
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 3840
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3847
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3850
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 3852
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 3854
    return v0

    .line 3847
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {v0}, Lcom/google/checkout/inapp/proto/a/b;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3850
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    invoke-virtual {v1}, Lcom/google/checkout/inapp/proto/al;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 3773
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/checkout/inapp/proto/al;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/al;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_4

    if-eqz v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_4
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    if-nez v0, :cond_5

    move v0, v2

    :goto_3
    if-nez v0, :cond_6

    array-length v3, v5

    if-ne v1, v3, :cond_6

    iput-object v5, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    array-length v0, v0

    goto :goto_3

    :cond_6
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_7

    iget-object v4, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_8

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_8
    if-eqz v0, :cond_c

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    if-nez v1, :cond_a

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_9

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_9
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_b

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_a
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    array-length v1, v1

    goto :goto_5

    :cond_b
    iput-object v4, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    :cond_c
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 3860
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_0

    .line 3861
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->a:Lcom/google/checkout/inapp/proto/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3863
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    if-eqz v0, :cond_1

    .line 3864
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->b:Lcom/google/checkout/inapp/proto/al;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3866
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    array-length v0, v0

    if-lez v0, :cond_2

    .line 3867
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 3868
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/Service$UpdateAddressPostResponse;->c:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3867
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3871
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3872
    return-void
.end method
