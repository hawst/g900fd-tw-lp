.class public final Lcom/google/checkout/inapp/proto/p;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:J

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 43
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 44
    iput v2, p0, Lcom/google/checkout/inapp/proto/p;->a:I

    iput v2, p0, Lcom/google/checkout/inapp/proto/p;->b:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/checkout/inapp/proto/p;->c:J

    iput v2, p0, Lcom/google/checkout/inapp/proto/p;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/p;->cachedSize:I

    .line 45
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 111
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 112
    iget v1, p0, Lcom/google/checkout/inapp/proto/p;->a:I

    if-eqz v1, :cond_0

    .line 113
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/checkout/inapp/proto/p;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 116
    :cond_0
    iget v1, p0, Lcom/google/checkout/inapp/proto/p;->b:I

    if-eqz v1, :cond_1

    .line 117
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/checkout/inapp/proto/p;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 120
    :cond_1
    iget-wide v2, p0, Lcom/google/checkout/inapp/proto/p;->c:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 121
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/checkout/inapp/proto/p;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    :cond_2
    iget v1, p0, Lcom/google/checkout/inapp/proto/p;->d:I

    if-eqz v1, :cond_3

    .line 125
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/checkout/inapp/proto/p;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 128
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 58
    if-ne p1, p0, :cond_1

    .line 77
    :cond_0
    :goto_0
    return v0

    .line 61
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/p;

    if-nez v2, :cond_2

    move v0, v1

    .line 62
    goto :goto_0

    .line 64
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/p;

    .line 65
    iget v2, p0, Lcom/google/checkout/inapp/proto/p;->a:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/p;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 66
    goto :goto_0

    .line 68
    :cond_3
    iget v2, p0, Lcom/google/checkout/inapp/proto/p;->b:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/p;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 69
    goto :goto_0

    .line 71
    :cond_4
    iget-wide v2, p0, Lcom/google/checkout/inapp/proto/p;->c:J

    iget-wide v4, p1, Lcom/google/checkout/inapp/proto/p;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 72
    goto :goto_0

    .line 74
    :cond_5
    iget v2, p0, Lcom/google/checkout/inapp/proto/p;->d:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/p;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 75
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 82
    iget v0, p0, Lcom/google/checkout/inapp/proto/p;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 84
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/checkout/inapp/proto/p;->b:I

    add-int/2addr v0, v1

    .line 85
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/checkout/inapp/proto/p;->c:J

    iget-wide v4, p0, Lcom/google/checkout/inapp/proto/p;->c:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 87
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/checkout/inapp/proto/p;->d:I

    add-int/2addr v0, v1

    .line 88
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/checkout/inapp/proto/p;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/checkout/inapp/proto/p;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/checkout/inapp/proto/p;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/checkout/inapp/proto/p;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x28 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 94
    iget v0, p0, Lcom/google/checkout/inapp/proto/p;->a:I

    if-eqz v0, :cond_0

    .line 95
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/checkout/inapp/proto/p;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 97
    :cond_0
    iget v0, p0, Lcom/google/checkout/inapp/proto/p;->b:I

    if-eqz v0, :cond_1

    .line 98
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/checkout/inapp/proto/p;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 100
    :cond_1
    iget-wide v0, p0, Lcom/google/checkout/inapp/proto/p;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_2

    .line 101
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/checkout/inapp/proto/p;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 103
    :cond_2
    iget v0, p0, Lcom/google/checkout/inapp/proto/p;->d:I

    if-eqz v0, :cond_3

    .line 104
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/checkout/inapp/proto/p;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 106
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 107
    return-void
.end method
