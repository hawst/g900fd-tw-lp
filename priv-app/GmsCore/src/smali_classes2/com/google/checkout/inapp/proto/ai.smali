.class public final Lcom/google/checkout/inapp/proto/ai;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:[Lcom/google/checkout/inapp/proto/j;

.field public c:[Lcom/google/checkout/inapp/proto/a/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4218
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4219
    const/16 v0, 0x64

    iput v0, p0, Lcom/google/checkout/inapp/proto/ai;->a:I

    invoke-static {}, Lcom/google/checkout/inapp/proto/j;->a()[Lcom/google/checkout/inapp/proto/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {}, Lcom/google/checkout/inapp/proto/a/b;->a()[Lcom/google/checkout/inapp/proto/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/ai;->cachedSize:I

    .line 4220
    return-void
.end method

.method public static a([B)Lcom/google/checkout/inapp/proto/ai;
    .locals 1

    .prologue
    .line 4394
    new-instance v0, Lcom/google/checkout/inapp/proto/ai;

    invoke-direct {v0}, Lcom/google/checkout/inapp/proto/ai;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/checkout/inapp/proto/ai;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 4291
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 4292
    iget v2, p0, Lcom/google/checkout/inapp/proto/ai;->a:I

    const/16 v3, 0x64

    if-eq v2, v3, :cond_0

    .line 4293
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/checkout/inapp/proto/ai;->a:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4296
    :cond_0
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 4297
    :goto_0
    iget-object v3, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 4298
    iget-object v3, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    aget-object v3, v3, v0

    .line 4299
    if-eqz v3, :cond_1

    .line 4300
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4297
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 4305
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 4306
    :goto_1
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 4307
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v2, v2, v1

    .line 4308
    if-eqz v2, :cond_4

    .line 4309
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4306
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4314
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4232
    if-ne p1, p0, :cond_1

    .line 4250
    :cond_0
    :goto_0
    return v0

    .line 4235
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/ai;

    if-nez v2, :cond_2

    move v0, v1

    .line 4236
    goto :goto_0

    .line 4238
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/ai;

    .line 4239
    iget v2, p0, Lcom/google/checkout/inapp/proto/ai;->a:I

    iget v3, p1, Lcom/google/checkout/inapp/proto/ai;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 4240
    goto :goto_0

    .line 4242
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 4244
    goto :goto_0

    .line 4246
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 4248
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 4255
    iget v0, p0, Lcom/google/checkout/inapp/proto/ai;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 4257
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4259
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4261
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4192
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    iput v0, p0, Lcom/google/checkout/inapp/proto/ai;->a:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/checkout/inapp/proto/j;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/checkout/inapp/proto/j;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/checkout/inapp/proto/a/b;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/a/b;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
        0x1a -> :sswitch_4
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_2
        0x1 -> :sswitch_2
        0x2 -> :sswitch_2
        0x3 -> :sswitch_2
        0x4 -> :sswitch_2
        0x5 -> :sswitch_2
        0x6 -> :sswitch_2
        0x64 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4267
    iget v0, p0, Lcom/google/checkout/inapp/proto/ai;->a:I

    const/16 v2, 0x64

    if-eq v0, v2, :cond_0

    .line 4268
    const/4 v0, 0x1

    iget v2, p0, Lcom/google/checkout/inapp/proto/ai;->a:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4270
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 4271
    :goto_0
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 4272
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ai;->b:[Lcom/google/checkout/inapp/proto/j;

    aget-object v2, v2, v0

    .line 4273
    if-eqz v2, :cond_1

    .line 4274
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4271
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4278
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 4279
    :goto_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 4280
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ai;->c:[Lcom/google/checkout/inapp/proto/a/b;

    aget-object v0, v0, v1

    .line 4281
    if-eqz v0, :cond_3

    .line 4282
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4279
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4286
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4287
    return-void
.end method
