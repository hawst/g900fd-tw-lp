.class public final Lcom/google/checkout/inapp/proto/ac;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:[Ljava/lang/String;

.field public c:[Lcom/google/checkout/inapp/proto/y;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4430
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4431
    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->a:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    invoke-static {}, Lcom/google/checkout/inapp/proto/y;->a()[Lcom/google/checkout/inapp/proto/y;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/inapp/proto/ac;->cachedSize:I

    .line 4432
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 4508
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 4509
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ac;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4510
    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/ac;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4513
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v2

    move v3, v2

    move v4, v2

    .line 4516
    :goto_0
    iget-object v5, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_2

    .line 4517
    iget-object v5, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 4518
    if-eqz v5, :cond_1

    .line 4519
    add-int/lit8 v4, v4, 0x1

    .line 4520
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 4516
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4524
    :cond_2
    add-int/2addr v0, v3

    .line 4525
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 4527
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    array-length v1, v1

    if-lez v1, :cond_5

    .line 4528
    :goto_1
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    array-length v1, v1

    if-ge v2, v1, :cond_5

    .line 4529
    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    aget-object v1, v1, v2

    .line 4530
    if-eqz v1, :cond_4

    .line 4531
    const/4 v3, 0x3

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4528
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 4536
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4444
    if-ne p1, p0, :cond_1

    .line 4466
    :cond_0
    :goto_0
    return v0

    .line 4447
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/inapp/proto/ac;

    if-nez v2, :cond_2

    move v0, v1

    .line 4448
    goto :goto_0

    .line 4450
    :cond_2
    check-cast p1, Lcom/google/checkout/inapp/proto/ac;

    .line 4451
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ac;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 4452
    iget-object v2, p1, Lcom/google/checkout/inapp/proto/ac;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 4453
    goto :goto_0

    .line 4455
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ac;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ac;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 4456
    goto :goto_0

    .line 4458
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 4460
    goto :goto_0

    .line 4462
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    iget-object v3, p1, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 4464
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 4471
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4474
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4476
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4478
    return v0

    .line 4471
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4404
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/checkout/inapp/proto/y;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/checkout/inapp/proto/y;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/y;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/checkout/inapp/proto/y;

    invoke-direct {v3}, Lcom/google/checkout/inapp/proto/y;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4484
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4485
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ac;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4487
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 4488
    :goto_0
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 4489
    iget-object v2, p0, Lcom/google/checkout/inapp/proto/ac;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 4490
    if-eqz v2, :cond_1

    .line 4491
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4488
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4495
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 4496
    :goto_1
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 4497
    iget-object v0, p0, Lcom/google/checkout/inapp/proto/ac;->c:[Lcom/google/checkout/inapp/proto/y;

    aget-object v0, v0, v1

    .line 4498
    if-eqz v0, :cond_3

    .line 4499
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4496
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4503
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4504
    return-void
.end method
