.class public final Lcom/google/checkout/a/a/a/b;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/checkout/a/a/a/c;

.field public b:I

.field public c:I

.field public d:Lcom/google/t/a/b;

.field public e:Ljava/lang/String;

.field public f:I

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 339
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 340
    iput-object v0, p0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    iput v1, p0, Lcom/google/checkout/a/a/a/b;->b:I

    iput v1, p0, Lcom/google/checkout/a/a/a/b;->c:I

    iput-object v0, p0, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/a/a/a/b;->e:Ljava/lang/String;

    iput v1, p0, Lcom/google/checkout/a/a/a/b;->f:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/a/a/a/b;->h:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/a/a/a/b;->cachedSize:I

    .line 341
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 467
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 468
    iget-object v1, p0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    if-eqz v1, :cond_0

    .line 469
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 472
    :cond_0
    iget v1, p0, Lcom/google/checkout/a/a/a/b;->b:I

    if-eqz v1, :cond_1

    .line 473
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/checkout/a/a/a/b;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 476
    :cond_1
    iget v1, p0, Lcom/google/checkout/a/a/a/b;->c:I

    if-eqz v1, :cond_2

    .line 477
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/checkout/a/a/a/b;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 480
    :cond_2
    iget-object v1, p0, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    if-eqz v1, :cond_3

    .line 481
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 484
    :cond_3
    iget-object v1, p0, Lcom/google/checkout/a/a/a/b;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 485
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 488
    :cond_4
    iget v1, p0, Lcom/google/checkout/a/a/a/b;->f:I

    if-eqz v1, :cond_5

    .line 489
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/checkout/a/a/a/b;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 492
    :cond_5
    iget-object v1, p0, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 493
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 496
    :cond_6
    iget-object v1, p0, Lcom/google/checkout/a/a/a/b;->h:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 497
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 500
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 358
    if-ne p1, p0, :cond_1

    .line 413
    :cond_0
    :goto_0
    return v0

    .line 361
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/a/a/a/b;

    if-nez v2, :cond_2

    move v0, v1

    .line 362
    goto :goto_0

    .line 364
    :cond_2
    check-cast p1, Lcom/google/checkout/a/a/a/b;

    .line 365
    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    if-nez v2, :cond_3

    .line 366
    iget-object v2, p1, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    if-eqz v2, :cond_4

    move v0, v1

    .line 367
    goto :goto_0

    .line 370
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    iget-object v3, p1, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    invoke-virtual {v2, v3}, Lcom/google/checkout/a/a/a/c;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 371
    goto :goto_0

    .line 374
    :cond_4
    iget v2, p0, Lcom/google/checkout/a/a/a/b;->b:I

    iget v3, p1, Lcom/google/checkout/a/a/a/b;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 375
    goto :goto_0

    .line 377
    :cond_5
    iget v2, p0, Lcom/google/checkout/a/a/a/b;->c:I

    iget v3, p1, Lcom/google/checkout/a/a/a/b;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 378
    goto :goto_0

    .line 380
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    if-nez v2, :cond_7

    .line 381
    iget-object v2, p1, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    if-eqz v2, :cond_8

    move v0, v1

    .line 382
    goto :goto_0

    .line 385
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    iget-object v3, p1, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    invoke-virtual {v2, v3}, Lcom/google/t/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 386
    goto :goto_0

    .line 389
    :cond_8
    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->e:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 390
    iget-object v2, p1, Lcom/google/checkout/a/a/a/b;->e:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 391
    goto :goto_0

    .line 393
    :cond_9
    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/a/a/a/b;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 394
    goto :goto_0

    .line 396
    :cond_a
    iget v2, p0, Lcom/google/checkout/a/a/a/b;->f:I

    iget v3, p1, Lcom/google/checkout/a/a/a/b;->f:I

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 397
    goto :goto_0

    .line 399
    :cond_b
    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 400
    iget-object v2, p1, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    if-eqz v2, :cond_d

    move v0, v1

    .line 401
    goto :goto_0

    .line 403
    :cond_c
    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    move v0, v1

    .line 404
    goto :goto_0

    .line 406
    :cond_d
    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->h:Ljava/lang/String;

    if-nez v2, :cond_e

    .line 407
    iget-object v2, p1, Lcom/google/checkout/a/a/a/b;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 408
    goto/16 :goto_0

    .line 410
    :cond_e
    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/a/a/a/b;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 411
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 418
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 421
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/checkout/a/a/a/b;->b:I

    add-int/2addr v0, v2

    .line 422
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/checkout/a/a/a/b;->c:I

    add-int/2addr v0, v2

    .line 423
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 425
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 427
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/checkout/a/a/a/b;->f:I

    add-int/2addr v0, v2

    .line 428
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 430
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/a/a/a/b;->h:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 432
    return v0

    .line 418
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    invoke-virtual {v0}, Lcom/google/checkout/a/a/a/c;->hashCode()I

    move-result v0

    goto :goto_0

    .line 423
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    invoke-virtual {v0}, Lcom/google/t/a/b;->hashCode()I

    move-result v0

    goto :goto_1

    .line 425
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 428
    :cond_3
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 430
    :cond_4
    iget-object v1, p0, Lcom/google/checkout/a/a/a/b;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 155
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/checkout/a/a/a/c;

    invoke-direct {v0}, Lcom/google/checkout/a/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/checkout/a/a/a/b;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/checkout/a/a/a/b;->c:I

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/t/a/b;

    invoke-direct {v0}, Lcom/google/t/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/a/a/a/b;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/checkout/a/a/a/b;->f:I

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/a/a/a/b;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 438
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    if-eqz v0, :cond_0

    .line 439
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/checkout/a/a/a/b;->a:Lcom/google/checkout/a/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 441
    :cond_0
    iget v0, p0, Lcom/google/checkout/a/a/a/b;->b:I

    if-eqz v0, :cond_1

    .line 442
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/checkout/a/a/a/b;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 444
    :cond_1
    iget v0, p0, Lcom/google/checkout/a/a/a/b;->c:I

    if-eqz v0, :cond_2

    .line 445
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/checkout/a/a/a/b;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 447
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    if-eqz v0, :cond_3

    .line 448
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/checkout/a/a/a/b;->d:Lcom/google/t/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 450
    :cond_3
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 451
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/checkout/a/a/a/b;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 453
    :cond_4
    iget v0, p0, Lcom/google/checkout/a/a/a/b;->f:I

    if-eqz v0, :cond_5

    .line 454
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/checkout/a/a/a/b;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 456
    :cond_5
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 457
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/checkout/a/a/a/b;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 459
    :cond_6
    iget-object v0, p0, Lcom/google/checkout/a/a/a/b;->h:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 460
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/checkout/a/a/a/b;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 462
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 463
    return-void
.end method
