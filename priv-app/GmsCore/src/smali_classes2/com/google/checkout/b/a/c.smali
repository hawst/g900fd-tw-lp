.class public final Lcom/google/checkout/b/a/c;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:Lcom/google/checkout/b/a/b;

.field public d:Lcom/google/aa/b/a/j;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 46
    const/16 v0, 0x65

    iput v0, p0, Lcom/google/checkout/b/a/c;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/checkout/b/a/c;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    iput-object v1, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/checkout/b/a/c;->cachedSize:I

    .line 47
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 131
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 132
    iget v1, p0, Lcom/google/checkout/b/a/c;->a:I

    const/16 v2, 0x65

    if-eq v1, v2, :cond_0

    .line 133
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/checkout/b/a/c;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 136
    :cond_0
    iget-object v1, p0, Lcom/google/checkout/b/a/c;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 137
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/checkout/b/a/c;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 140
    :cond_1
    iget-object v1, p0, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    if-eqz v1, :cond_2

    .line 141
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 144
    :cond_2
    iget-object v1, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    if-eqz v1, :cond_3

    .line 145
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 148
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 60
    if-ne p1, p0, :cond_1

    .line 95
    :cond_0
    :goto_0
    return v0

    .line 63
    :cond_1
    instance-of v2, p1, Lcom/google/checkout/b/a/c;

    if-nez v2, :cond_2

    move v0, v1

    .line 64
    goto :goto_0

    .line 66
    :cond_2
    check-cast p1, Lcom/google/checkout/b/a/c;

    .line 67
    iget v2, p0, Lcom/google/checkout/b/a/c;->a:I

    iget v3, p1, Lcom/google/checkout/b/a/c;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 68
    goto :goto_0

    .line 70
    :cond_3
    iget-object v2, p0, Lcom/google/checkout/b/a/c;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 71
    iget-object v2, p1, Lcom/google/checkout/b/a/c;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 72
    goto :goto_0

    .line 74
    :cond_4
    iget-object v2, p0, Lcom/google/checkout/b/a/c;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/checkout/b/a/c;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 75
    goto :goto_0

    .line 77
    :cond_5
    iget-object v2, p0, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    if-nez v2, :cond_6

    .line 78
    iget-object v2, p1, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    if-eqz v2, :cond_7

    move v0, v1

    .line 79
    goto :goto_0

    .line 82
    :cond_6
    iget-object v2, p0, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    iget-object v3, p1, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    invoke-virtual {v2, v3}, Lcom/google/checkout/b/a/b;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 83
    goto :goto_0

    .line 86
    :cond_7
    iget-object v2, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    if-nez v2, :cond_8

    .line 87
    iget-object v2, p1, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    if-eqz v2, :cond_0

    move v0, v1

    .line 88
    goto :goto_0

    .line 91
    :cond_8
    iget-object v2, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    iget-object v3, p1, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    invoke-virtual {v2, v3}, Lcom/google/aa/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 92
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 100
    iget v0, p0, Lcom/google/checkout/b/a/c;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 102
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/b/a/c;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 104
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 106
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 108
    return v0

    .line 102
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/b/a/c;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    invoke-virtual {v0}, Lcom/google/checkout/b/a/b;->hashCode()I

    move-result v0

    goto :goto_1

    .line 106
    :cond_2
    iget-object v1, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    invoke-virtual {v1}, Lcom/google/aa/b/a/j;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/checkout/b/a/c;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/checkout/b/a/c;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/checkout/b/a/b;

    invoke-direct {v0}, Lcom/google/checkout/b/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/aa/b/a/j;

    invoke-direct {v0}, Lcom/google/aa/b/a/j;-><init>()V

    iput-object v0, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    :cond_2
    iget-object v0, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 114
    iget v0, p0, Lcom/google/checkout/b/a/c;->a:I

    const/16 v1, 0x65

    if-eq v0, v1, :cond_0

    .line 115
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/checkout/b/a/c;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/checkout/b/a/c;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 118
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/checkout/b/a/c;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 120
    :cond_1
    iget-object v0, p0, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    if-eqz v0, :cond_2

    .line 121
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/checkout/b/a/c;->c:Lcom/google/checkout/b/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 123
    :cond_2
    iget-object v0, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    if-eqz v0, :cond_3

    .line 124
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/checkout/b/a/c;->d:Lcom/google/aa/b/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 126
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 127
    return-void
.end method
