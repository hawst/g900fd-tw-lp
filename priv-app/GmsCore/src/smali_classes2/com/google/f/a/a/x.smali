.class public final Lcom/google/f/a/a/x;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/f/a/a/y;

.field public b:Lcom/google/f/a/a/z;

.field public c:Lcom/google/f/a/a/r;

.field public d:Lcom/google/f/a/b;

.field public e:Lcom/google/f/a/a/q;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 38
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 39
    iput-object v0, p0, Lcom/google/f/a/a/x;->a:Lcom/google/f/a/a/y;

    iput-object v0, p0, Lcom/google/f/a/a/x;->b:Lcom/google/f/a/a/z;

    iput-object v0, p0, Lcom/google/f/a/a/x;->c:Lcom/google/f/a/a/r;

    iput-object v0, p0, Lcom/google/f/a/a/x;->d:Lcom/google/f/a/b;

    iput-object v0, p0, Lcom/google/f/a/a/x;->e:Lcom/google/f/a/a/q;

    iput-object v0, p0, Lcom/google/f/a/a/x;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/a/x;->cachedSize:I

    .line 40
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 150
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 151
    iget-object v1, p0, Lcom/google/f/a/a/x;->a:Lcom/google/f/a/a/y;

    if-eqz v1, :cond_0

    .line 152
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/a/x;->a:Lcom/google/f/a/a/y;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 155
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/a/x;->b:Lcom/google/f/a/a/z;

    if-eqz v1, :cond_1

    .line 156
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/f/a/a/x;->b:Lcom/google/f/a/a/z;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 159
    :cond_1
    iget-object v1, p0, Lcom/google/f/a/a/x;->c:Lcom/google/f/a/a/r;

    if-eqz v1, :cond_2

    .line 160
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/f/a/a/x;->c:Lcom/google/f/a/a/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 163
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/a/x;->d:Lcom/google/f/a/b;

    if-eqz v1, :cond_3

    .line 164
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/f/a/a/x;->d:Lcom/google/f/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/x;->e:Lcom/google/f/a/a/q;

    if-eqz v1, :cond_4

    .line 168
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/f/a/a/x;->e:Lcom/google/f/a/a/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 55
    if-ne p1, p0, :cond_1

    .line 56
    const/4 v0, 0x1

    .line 107
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/a/x;

    if-eqz v1, :cond_0

    .line 61
    check-cast p1, Lcom/google/f/a/a/x;

    .line 62
    iget-object v1, p0, Lcom/google/f/a/a/x;->a:Lcom/google/f/a/a/y;

    if-nez v1, :cond_7

    .line 63
    iget-object v1, p1, Lcom/google/f/a/a/x;->a:Lcom/google/f/a/a/y;

    if-nez v1, :cond_0

    .line 71
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/a/x;->b:Lcom/google/f/a/a/z;

    if-nez v1, :cond_8

    .line 72
    iget-object v1, p1, Lcom/google/f/a/a/x;->b:Lcom/google/f/a/a/z;

    if-nez v1, :cond_0

    .line 80
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/x;->c:Lcom/google/f/a/a/r;

    if-nez v1, :cond_9

    .line 81
    iget-object v1, p1, Lcom/google/f/a/a/x;->c:Lcom/google/f/a/a/r;

    if-nez v1, :cond_0

    .line 89
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/x;->d:Lcom/google/f/a/b;

    if-nez v1, :cond_a

    .line 90
    iget-object v1, p1, Lcom/google/f/a/a/x;->d:Lcom/google/f/a/b;

    if-nez v1, :cond_0

    .line 98
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/a/x;->e:Lcom/google/f/a/a/q;

    if-nez v1, :cond_b

    .line 99
    iget-object v1, p1, Lcom/google/f/a/a/x;->e:Lcom/google/f/a/a/q;

    if-nez v1, :cond_0

    .line 107
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/f/a/a/x;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 67
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/a/x;->a:Lcom/google/f/a/a/y;

    iget-object v2, p1, Lcom/google/f/a/a/x;->a:Lcom/google/f/a/a/y;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/y;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 76
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/a/x;->b:Lcom/google/f/a/a/z;

    iget-object v2, p1, Lcom/google/f/a/a/x;->b:Lcom/google/f/a/a/z;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/z;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 85
    :cond_9
    iget-object v1, p0, Lcom/google/f/a/a/x;->c:Lcom/google/f/a/a/r;

    iget-object v2, p1, Lcom/google/f/a/a/x;->c:Lcom/google/f/a/a/r;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/r;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 94
    :cond_a
    iget-object v1, p0, Lcom/google/f/a/a/x;->d:Lcom/google/f/a/b;

    iget-object v2, p1, Lcom/google/f/a/a/x;->d:Lcom/google/f/a/b;

    invoke-virtual {v1, v2}, Lcom/google/f/a/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 103
    :cond_b
    iget-object v1, p0, Lcom/google/f/a/a/x;->e:Lcom/google/f/a/a/q;

    iget-object v2, p1, Lcom/google/f/a/a/x;->e:Lcom/google/f/a/a/q;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 112
    iget-object v0, p0, Lcom/google/f/a/a/x;->a:Lcom/google/f/a/a/y;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 115
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/x;->b:Lcom/google/f/a/a/z;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 117
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/x;->c:Lcom/google/f/a/a/r;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 119
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/x;->d:Lcom/google/f/a/b;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 121
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/a/x;->e:Lcom/google/f/a/a/q;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 123
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/a/x;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 124
    return v0

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/x;->a:Lcom/google/f/a/a/y;

    invoke-virtual {v0}, Lcom/google/f/a/a/y;->hashCode()I

    move-result v0

    goto :goto_0

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/x;->b:Lcom/google/f/a/a/z;

    invoke-virtual {v0}, Lcom/google/f/a/a/z;->hashCode()I

    move-result v0

    goto :goto_1

    .line 117
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/x;->c:Lcom/google/f/a/a/r;

    invoke-virtual {v0}, Lcom/google/f/a/a/r;->hashCode()I

    move-result v0

    goto :goto_2

    .line 119
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/x;->d:Lcom/google/f/a/b;

    invoke-virtual {v0}, Lcom/google/f/a/b;->hashCode()I

    move-result v0

    goto :goto_3

    .line 121
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/x;->e:Lcom/google/f/a/a/q;

    invoke-virtual {v1}, Lcom/google/f/a/a/q;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/a/x;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/f/a/a/x;->a:Lcom/google/f/a/a/y;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/a/y;

    invoke-direct {v0}, Lcom/google/f/a/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/x;->a:Lcom/google/f/a/a/y;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/x;->a:Lcom/google/f/a/a/y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/f/a/a/x;->b:Lcom/google/f/a/a/z;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/f/a/a/z;

    invoke-direct {v0}, Lcom/google/f/a/a/z;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/x;->b:Lcom/google/f/a/a/z;

    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/x;->b:Lcom/google/f/a/a/z;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/f/a/a/x;->c:Lcom/google/f/a/a/r;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/f/a/a/r;

    invoke-direct {v0}, Lcom/google/f/a/a/r;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/x;->c:Lcom/google/f/a/a/r;

    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/x;->c:Lcom/google/f/a/a/r;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/f/a/a/x;->d:Lcom/google/f/a/b;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/f/a/b;

    invoke-direct {v0}, Lcom/google/f/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/x;->d:Lcom/google/f/a/b;

    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/x;->d:Lcom/google/f/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/f/a/a/x;->e:Lcom/google/f/a/a/q;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/f/a/a/q;

    invoke-direct {v0}, Lcom/google/f/a/a/q;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/x;->e:Lcom/google/f/a/a/q;

    :cond_5
    iget-object v0, p0, Lcom/google/f/a/a/x;->e:Lcom/google/f/a/a/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/f/a/a/x;->a:Lcom/google/f/a/a/y;

    if-eqz v0, :cond_0

    .line 131
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/a/x;->a:Lcom/google/f/a/a/y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/x;->b:Lcom/google/f/a/a/z;

    if-eqz v0, :cond_1

    .line 134
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/f/a/a/x;->b:Lcom/google/f/a/a/z;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/x;->c:Lcom/google/f/a/a/r;

    if-eqz v0, :cond_2

    .line 137
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/f/a/a/x;->c:Lcom/google/f/a/a/r;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 139
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/x;->d:Lcom/google/f/a/b;

    if-eqz v0, :cond_3

    .line 140
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/f/a/a/x;->d:Lcom/google/f/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 142
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/x;->e:Lcom/google/f/a/a/q;

    if-eqz v0, :cond_4

    .line 143
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/f/a/a/x;->e:Lcom/google/f/a/a/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 145
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 146
    return-void
.end method
