.class public final Lcom/google/f/a/u;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/f/a/m;

.field public b:Lcom/google/f/a/t;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 35
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 36
    iput-object v0, p0, Lcom/google/f/a/u;->a:Lcom/google/f/a/m;

    iput-object v0, p0, Lcom/google/f/a/u;->b:Lcom/google/f/a/t;

    iput-object v0, p0, Lcom/google/f/a/u;->c:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/f/a/u;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/f/a/u;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/u;->cachedSize:I

    .line 37
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 128
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 129
    iget-object v1, p0, Lcom/google/f/a/u;->a:Lcom/google/f/a/m;

    if-eqz v1, :cond_0

    .line 130
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/u;->a:Lcom/google/f/a/m;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 133
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/u;->b:Lcom/google/f/a/t;

    if-eqz v1, :cond_1

    .line 134
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/f/a/u;->b:Lcom/google/f/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 137
    :cond_1
    iget-object v1, p0, Lcom/google/f/a/u;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 138
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/f/a/u;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 141
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/u;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 142
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/f/a/u;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 145
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 51
    if-ne p1, p0, :cond_1

    .line 52
    const/4 v0, 0x1

    .line 90
    :cond_0
    :goto_0
    return v0

    .line 54
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/u;

    if-eqz v1, :cond_0

    .line 57
    check-cast p1, Lcom/google/f/a/u;

    .line 58
    iget-object v1, p0, Lcom/google/f/a/u;->a:Lcom/google/f/a/m;

    if-nez v1, :cond_6

    .line 59
    iget-object v1, p1, Lcom/google/f/a/u;->a:Lcom/google/f/a/m;

    if-nez v1, :cond_0

    .line 67
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/u;->b:Lcom/google/f/a/t;

    if-nez v1, :cond_7

    .line 68
    iget-object v1, p1, Lcom/google/f/a/u;->b:Lcom/google/f/a/t;

    if-nez v1, :cond_0

    .line 76
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/u;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_8

    .line 77
    iget-object v1, p1, Lcom/google/f/a/u;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 83
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/u;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_9

    .line 84
    iget-object v1, p1, Lcom/google/f/a/u;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 90
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/f/a/u;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 63
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/u;->a:Lcom/google/f/a/m;

    iget-object v2, p1, Lcom/google/f/a/u;->a:Lcom/google/f/a/m;

    invoke-virtual {v1, v2}, Lcom/google/f/a/m;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 72
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/u;->b:Lcom/google/f/a/t;

    iget-object v2, p1, Lcom/google/f/a/u;->b:Lcom/google/f/a/t;

    invoke-virtual {v1, v2}, Lcom/google/f/a/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 80
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/u;->c:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/u;->c:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 87
    :cond_9
    iget-object v1, p0, Lcom/google/f/a/u;->d:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/u;->d:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 95
    iget-object v0, p0, Lcom/google/f/a/u;->a:Lcom/google/f/a/m;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 98
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/u;->b:Lcom/google/f/a/t;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 100
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/u;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 102
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/u;->d:Ljava/lang/Boolean;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 104
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/u;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 105
    return v0

    .line 95
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/u;->a:Lcom/google/f/a/m;

    invoke-virtual {v0}, Lcom/google/f/a/m;->hashCode()I

    move-result v0

    goto :goto_0

    .line 98
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/u;->b:Lcom/google/f/a/t;

    invoke-virtual {v0}, Lcom/google/f/a/t;->hashCode()I

    move-result v0

    goto :goto_1

    .line 100
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/u;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_2

    .line 102
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/u;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/u;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/f/a/u;->a:Lcom/google/f/a/m;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/m;

    invoke-direct {v0}, Lcom/google/f/a/m;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/u;->a:Lcom/google/f/a/m;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/u;->a:Lcom/google/f/a/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/f/a/u;->b:Lcom/google/f/a/t;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/f/a/t;

    invoke-direct {v0}, Lcom/google/f/a/t;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/u;->b:Lcom/google/f/a/t;

    :cond_2
    iget-object v0, p0, Lcom/google/f/a/u;->b:Lcom/google/f/a/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/u;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/u;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/f/a/u;->a:Lcom/google/f/a/m;

    if-eqz v0, :cond_0

    .line 112
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/u;->a:Lcom/google/f/a/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/u;->b:Lcom/google/f/a/t;

    if-eqz v0, :cond_1

    .line 115
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/u;->b:Lcom/google/f/a/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 117
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/u;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 118
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/f/a/u;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 120
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/u;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 121
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/f/a/u;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 123
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 124
    return-void
.end method
