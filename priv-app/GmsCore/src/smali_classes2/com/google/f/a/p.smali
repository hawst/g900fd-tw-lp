.class public final Lcom/google/f/a/p;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/f/a/h;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Boolean;

.field public e:Lcom/google/f/a/h;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 212
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 213
    iput-object v0, p0, Lcom/google/f/a/p;->a:Lcom/google/f/a/h;

    iput-object v0, p0, Lcom/google/f/a/p;->b:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/f/a/p;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/f/a/p;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/f/a/p;->e:Lcom/google/f/a/h;

    iput-object v0, p0, Lcom/google/f/a/p;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/p;->cachedSize:I

    .line 214
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 318
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 319
    iget-object v1, p0, Lcom/google/f/a/p;->a:Lcom/google/f/a/h;

    if-eqz v1, :cond_0

    .line 320
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/p;->a:Lcom/google/f/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 323
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/p;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 324
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/f/a/p;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 327
    :cond_1
    iget-object v1, p0, Lcom/google/f/a/p;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 328
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/f/a/p;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 331
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/p;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 332
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/f/a/p;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 335
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/p;->e:Lcom/google/f/a/h;

    if-eqz v1, :cond_4

    .line 336
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/f/a/p;->e:Lcom/google/f/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 339
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 229
    if-ne p1, p0, :cond_1

    .line 230
    const/4 v0, 0x1

    .line 275
    :cond_0
    :goto_0
    return v0

    .line 232
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/p;

    if-eqz v1, :cond_0

    .line 235
    check-cast p1, Lcom/google/f/a/p;

    .line 236
    iget-object v1, p0, Lcom/google/f/a/p;->a:Lcom/google/f/a/h;

    if-nez v1, :cond_7

    .line 237
    iget-object v1, p1, Lcom/google/f/a/p;->a:Lcom/google/f/a/h;

    if-nez v1, :cond_0

    .line 245
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/p;->b:Ljava/lang/Long;

    if-nez v1, :cond_8

    .line 246
    iget-object v1, p1, Lcom/google/f/a/p;->b:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 252
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/p;->c:Ljava/lang/Integer;

    if-nez v1, :cond_9

    .line 253
    iget-object v1, p1, Lcom/google/f/a/p;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 259
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/p;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_a

    .line 260
    iget-object v1, p1, Lcom/google/f/a/p;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 266
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/p;->e:Lcom/google/f/a/h;

    if-nez v1, :cond_b

    .line 267
    iget-object v1, p1, Lcom/google/f/a/p;->e:Lcom/google/f/a/h;

    if-nez v1, :cond_0

    .line 275
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/f/a/p;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 241
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/p;->a:Lcom/google/f/a/h;

    iget-object v2, p1, Lcom/google/f/a/p;->a:Lcom/google/f/a/h;

    invoke-virtual {v1, v2}, Lcom/google/f/a/h;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 249
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/p;->b:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/f/a/p;->b:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 256
    :cond_9
    iget-object v1, p0, Lcom/google/f/a/p;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/p;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 263
    :cond_a
    iget-object v1, p0, Lcom/google/f/a/p;->d:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/p;->d:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 271
    :cond_b
    iget-object v1, p0, Lcom/google/f/a/p;->e:Lcom/google/f/a/h;

    iget-object v2, p1, Lcom/google/f/a/p;->e:Lcom/google/f/a/h;

    invoke-virtual {v1, v2}, Lcom/google/f/a/h;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 280
    iget-object v0, p0, Lcom/google/f/a/p;->a:Lcom/google/f/a/h;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 283
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/p;->b:Ljava/lang/Long;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 285
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/p;->c:Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 287
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/p;->d:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 289
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/p;->e:Lcom/google/f/a/h;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 291
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/p;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 292
    return v0

    .line 280
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/p;->a:Lcom/google/f/a/h;

    invoke-virtual {v0}, Lcom/google/f/a/h;->hashCode()I

    move-result v0

    goto :goto_0

    .line 283
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/p;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_1

    .line 285
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/p;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_2

    .line 287
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/p;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_3

    .line 289
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/p;->e:Lcom/google/f/a/h;

    invoke-virtual {v1}, Lcom/google/f/a/h;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 180
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/p;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/f/a/p;->a:Lcom/google/f/a/h;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/h;

    invoke-direct {v0}, Lcom/google/f/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/p;->a:Lcom/google/f/a/h;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/p;->a:Lcom/google/f/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/p;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/p;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/p;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/f/a/p;->e:Lcom/google/f/a/h;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/f/a/h;

    invoke-direct {v0}, Lcom/google/f/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/p;->e:Lcom/google/f/a/h;

    :cond_2
    iget-object v0, p0, Lcom/google/f/a/p;->e:Lcom/google/f/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/f/a/p;->a:Lcom/google/f/a/h;

    if-eqz v0, :cond_0

    .line 299
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/p;->a:Lcom/google/f/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 301
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/p;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 302
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/p;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 304
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/p;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 305
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/f/a/p;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 307
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/p;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 308
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/f/a/p;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 310
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/p;->e:Lcom/google/f/a/h;

    if-eqz v0, :cond_4

    .line 311
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/f/a/p;->e:Lcom/google/f/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 313
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 314
    return-void
.end method
