.class public final Lcom/google/f/a/a/t;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Lcom/google/f/a/x;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 42
    iput-object v0, p0, Lcom/google/f/a/a/t;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/f/a/a/t;->b:Lcom/google/f/a/x;

    iput-object v0, p0, Lcom/google/f/a/a/t;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/f/a/a/t;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/f/a/a/t;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/f/a/a/t;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/f/a/a/t;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/a/t;->cachedSize:I

    .line 43
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 158
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 159
    iget-object v1, p0, Lcom/google/f/a/a/t;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 160
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/f/a/a/t;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 163
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/a/t;->b:Lcom/google/f/a/x;

    if-eqz v1, :cond_1

    .line 164
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/f/a/a/t;->b:Lcom/google/f/a/x;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    :cond_1
    iget-object v1, p0, Lcom/google/f/a/a/t;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 168
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/f/a/a/t;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/a/t;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 172
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/f/a/a/t;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 175
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/t;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 176
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/f/a/a/t;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/t;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 180
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/f/a/a/t;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 183
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 59
    if-ne p1, p0, :cond_1

    .line 60
    const/4 v0, 0x1

    .line 110
    :cond_0
    :goto_0
    return v0

    .line 62
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/a/t;

    if-eqz v1, :cond_0

    .line 65
    check-cast p1, Lcom/google/f/a/a/t;

    .line 66
    iget-object v1, p0, Lcom/google/f/a/a/t;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_8

    .line 67
    iget-object v1, p1, Lcom/google/f/a/a/t;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 73
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/a/t;->b:Lcom/google/f/a/x;

    if-nez v1, :cond_9

    .line 74
    iget-object v1, p1, Lcom/google/f/a/a/t;->b:Lcom/google/f/a/x;

    if-nez v1, :cond_0

    .line 82
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/t;->c:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 83
    iget-object v1, p1, Lcom/google/f/a/a/t;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 89
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/t;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_b

    .line 90
    iget-object v1, p1, Lcom/google/f/a/a/t;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 96
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/a/t;->e:Ljava/lang/String;

    if-nez v1, :cond_c

    .line 97
    iget-object v1, p1, Lcom/google/f/a/a/t;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 103
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/a/t;->f:Ljava/lang/String;

    if-nez v1, :cond_d

    .line 104
    iget-object v1, p1, Lcom/google/f/a/a/t;->f:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 110
    :cond_7
    invoke-virtual {p0, p1}, Lcom/google/f/a/a/t;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 70
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/a/t;->a:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/a/t;->a:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 78
    :cond_9
    iget-object v1, p0, Lcom/google/f/a/a/t;->b:Lcom/google/f/a/x;

    iget-object v2, p1, Lcom/google/f/a/a/t;->b:Lcom/google/f/a/x;

    invoke-virtual {v1, v2}, Lcom/google/f/a/x;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 86
    :cond_a
    iget-object v1, p0, Lcom/google/f/a/a/t;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/f/a/a/t;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 93
    :cond_b
    iget-object v1, p0, Lcom/google/f/a/a/t;->d:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/a/t;->d:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 100
    :cond_c
    iget-object v1, p0, Lcom/google/f/a/a/t;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/f/a/a/t;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 107
    :cond_d
    iget-object v1, p0, Lcom/google/f/a/a/t;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/f/a/a/t;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 115
    iget-object v0, p0, Lcom/google/f/a/a/t;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 118
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/t;->b:Lcom/google/f/a/x;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 120
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/t;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 122
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/t;->d:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 124
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/t;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 126
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/a/t;->f:Ljava/lang/String;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 128
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/a/t;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    return v0

    .line 115
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/t;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0

    .line 118
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/t;->b:Lcom/google/f/a/x;

    invoke-virtual {v0}, Lcom/google/f/a/x;->hashCode()I

    move-result v0

    goto :goto_1

    .line 120
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/t;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 122
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/t;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_3

    .line 124
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/t;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 126
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/a/t;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/a/t;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/t;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/f/a/a/t;->b:Lcom/google/f/a/x;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/x;

    invoke-direct {v0}, Lcom/google/f/a/x;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/t;->b:Lcom/google/f/a/x;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/t;->b:Lcom/google/f/a/x;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/t;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/t;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/t;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/t;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x28 -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/f/a/a/t;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 136
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/a/t;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/t;->b:Lcom/google/f/a/x;

    if-eqz v0, :cond_1

    .line 139
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/f/a/a/t;->b:Lcom/google/f/a/x;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 141
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/t;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 142
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/f/a/a/t;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 144
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/t;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 145
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/f/a/a/t;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 147
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/t;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 148
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/f/a/a/t;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 150
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/t;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 151
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/f/a/a/t;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 153
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 154
    return-void
.end method
