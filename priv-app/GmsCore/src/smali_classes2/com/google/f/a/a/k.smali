.class public final Lcom/google/f/a/a/k;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/f/a/a/t;

.field public b:[Lcom/google/f/a/w;

.field public c:Lcom/google/f/a/a/o;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 33
    iput-object v1, p0, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    invoke-static {}, Lcom/google/f/a/w;->a()[Lcom/google/f/a/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    iput-object v1, p0, Lcom/google/f/a/a/k;->c:Lcom/google/f/a/a/o;

    iput-object v1, p0, Lcom/google/f/a/a/k;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/a/k;->cachedSize:I

    .line 34
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 114
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 115
    iget-object v1, p0, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    if-eqz v1, :cond_0

    .line 116
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 119
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 120
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 121
    iget-object v2, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    aget-object v2, v2, v0

    .line 122
    if-eqz v2, :cond_1

    .line 123
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 120
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 128
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/k;->c:Lcom/google/f/a/a/o;

    if-eqz v1, :cond_4

    .line 129
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/f/a/a/k;->c:Lcom/google/f/a/a/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 132
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 47
    if-ne p1, p0, :cond_1

    .line 48
    const/4 v0, 0x1

    .line 76
    :cond_0
    :goto_0
    return v0

    .line 50
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/a/k;

    if-eqz v1, :cond_0

    .line 53
    check-cast p1, Lcom/google/f/a/a/k;

    .line 54
    iget-object v1, p0, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    if-nez v1, :cond_4

    .line 55
    iget-object v1, p1, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    if-nez v1, :cond_0

    .line 63
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    iget-object v2, p1, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 67
    iget-object v1, p0, Lcom/google/f/a/a/k;->c:Lcom/google/f/a/a/o;

    if-nez v1, :cond_5

    .line 68
    iget-object v1, p1, Lcom/google/f/a/a/k;->c:Lcom/google/f/a/a/o;

    if-nez v1, :cond_0

    .line 76
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/f/a/a/k;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 59
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    iget-object v2, p1, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 72
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/a/k;->c:Lcom/google/f/a/a/o;

    iget-object v2, p1, Lcom/google/f/a/a/k;->c:Lcom/google/f/a/a/o;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/o;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 81
    iget-object v0, p0, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 84
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 86
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/a/k;->c:Lcom/google/f/a/a/o;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 88
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/a/k;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    return v0

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    invoke-virtual {v0}, Lcom/google/f/a/a/t;->hashCode()I

    move-result v0

    goto :goto_0

    .line 86
    :cond_1
    iget-object v1, p0, Lcom/google/f/a/a/k;->c:Lcom/google/f/a/a/o;

    invoke-virtual {v1}, Lcom/google/f/a/a/o;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/a/k;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/a/t;

    invoke-direct {v0}, Lcom/google/f/a/a/t;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/f/a/w;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/f/a/w;

    invoke-direct {v3}, Lcom/google/f/a/w;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/f/a/w;

    invoke-direct {v3}, Lcom/google/f/a/w;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/f/a/a/k;->c:Lcom/google/f/a/a/o;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/f/a/a/o;

    invoke-direct {v0}, Lcom/google/f/a/a/o;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/k;->c:Lcom/google/f/a/a/o;

    :cond_5
    iget-object v0, p0, Lcom/google/f/a/a/k;->c:Lcom/google/f/a/a/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    if-eqz v0, :cond_0

    .line 96
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/a/k;->a:Lcom/google/f/a/a/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 99
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 100
    iget-object v1, p0, Lcom/google/f/a/a/k;->b:[Lcom/google/f/a/w;

    aget-object v1, v1, v0

    .line 101
    if-eqz v1, :cond_1

    .line 102
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 99
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/k;->c:Lcom/google/f/a/a/o;

    if-eqz v0, :cond_3

    .line 107
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/f/a/a/k;->c:Lcom/google/f/a/a/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 109
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 110
    return-void
.end method
