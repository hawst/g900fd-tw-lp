.class public final Lcom/google/f/a/s;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/f/a/o;

.field public b:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1025
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1026
    iput-object v1, p0, Lcom/google/f/a/s;->a:Lcom/google/f/a/o;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/f/a/s;->b:[I

    iput-object v1, p0, Lcom/google/f/a/s;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/s;->cachedSize:I

    .line 1027
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1089
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1090
    iget-object v2, p0, Lcom/google/f/a/s;->a:Lcom/google/f/a/o;

    if-eqz v2, :cond_0

    .line 1091
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/f/a/s;->a:Lcom/google/f/a/o;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1094
    :cond_0
    iget-object v2, p0, Lcom/google/f/a/s;->b:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/f/a/s;->b:[I

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v1

    .line 1096
    :goto_0
    iget-object v3, p0, Lcom/google/f/a/s;->b:[I

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 1097
    iget-object v3, p0, Lcom/google/f/a/s;->b:[I

    aget v3, v3, v1

    .line 1098
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 1096
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1101
    :cond_1
    add-int/2addr v0, v2

    .line 1102
    iget-object v1, p0, Lcom/google/f/a/s;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1104
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1039
    if-ne p1, p0, :cond_1

    .line 1040
    const/4 v0, 0x1

    .line 1059
    :cond_0
    :goto_0
    return v0

    .line 1042
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/s;

    if-eqz v1, :cond_0

    .line 1045
    check-cast p1, Lcom/google/f/a/s;

    .line 1046
    iget-object v1, p0, Lcom/google/f/a/s;->a:Lcom/google/f/a/o;

    if-nez v1, :cond_3

    .line 1047
    iget-object v1, p1, Lcom/google/f/a/s;->a:Lcom/google/f/a/o;

    if-nez v1, :cond_0

    .line 1055
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/s;->b:[I

    iget-object v2, p1, Lcom/google/f/a/s;->b:[I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1059
    invoke-virtual {p0, p1}, Lcom/google/f/a/s;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1051
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/s;->a:Lcom/google/f/a/o;

    iget-object v2, p1, Lcom/google/f/a/s;->a:Lcom/google/f/a/o;

    invoke-virtual {v1, v2}, Lcom/google/f/a/o;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1064
    iget-object v0, p0, Lcom/google/f/a/s;->a:Lcom/google/f/a/o;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1067
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/f/a/s;->b:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 1069
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/s;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1070
    return v0

    .line 1064
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/s;->a:Lcom/google/f/a/o;

    invoke-virtual {v0}, Lcom/google/f/a/o;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1002
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/s;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/f/a/s;->a:Lcom/google/f/a/o;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/o;

    invoke-direct {v0}, Lcom/google/f/a/o;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/s;->a:Lcom/google/f/a/o;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/s;->a:Lcom/google/f/a/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_3
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/f/a/s;->b:[I

    if-nez v0, :cond_4

    move v0, v2

    :goto_3
    if-nez v0, :cond_5

    array-length v3, v5

    if-ne v1, v3, :cond_5

    iput-object v5, p0, Lcom/google/f/a/s;->b:[I

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/f/a/s;->b:[I

    array-length v0, v0

    goto :goto_3

    :cond_5
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_6

    iget-object v4, p0, Lcom/google/f/a/s;->b:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/f/a/s;->b:[I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    if-eqz v0, :cond_b

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/f/a/s;->b:[I

    if-nez v1, :cond_9

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_8

    iget-object v0, p0, Lcom/google/f/a/s;->b:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_a

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_9
    iget-object v1, p0, Lcom/google/f/a/s;->b:[I

    array-length v1, v1

    goto :goto_5

    :cond_a
    iput-object v4, p0, Lcom/google/f/a/s;->b:[I

    :cond_b
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1076
    iget-object v0, p0, Lcom/google/f/a/s;->a:Lcom/google/f/a/o;

    if-eqz v0, :cond_0

    .line 1077
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/s;->a:Lcom/google/f/a/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1079
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/s;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/f/a/s;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1080
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/f/a/s;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1081
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/f/a/s;->b:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1080
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1084
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1085
    return-void
.end method
