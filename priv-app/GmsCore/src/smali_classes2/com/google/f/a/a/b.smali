.class public final Lcom/google/f/a/a/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/f/a/a/t;

.field public b:Lcom/google/f/a/a/u;

.field public c:Lcom/google/f/a/w;

.field public d:Lcom/google/f/a/a/r;

.field public e:Lcom/google/f/a/w;

.field public f:Lcom/google/f/a/a/f;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 48
    iput-object v0, p0, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    iput-object v0, p0, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    iput-object v0, p0, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    iput-object v0, p0, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    iput-object v0, p0, Lcom/google/f/a/a/b;->e:Lcom/google/f/a/w;

    iput-object v0, p0, Lcom/google/f/a/a/b;->f:Lcom/google/f/a/a/f;

    iput-object v0, p0, Lcom/google/f/a/a/b;->g:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/f/a/a/b;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/f/a/a/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/a/b;->cachedSize:I

    .line 49
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 200
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 201
    iget-object v1, p0, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    if-eqz v1, :cond_0

    .line 202
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 205
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    if-eqz v1, :cond_1

    .line 206
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 209
    :cond_1
    iget-object v1, p0, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    if-eqz v1, :cond_2

    .line 210
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 213
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    if-eqz v1, :cond_3

    .line 214
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 217
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/b;->e:Lcom/google/f/a/w;

    if-eqz v1, :cond_4

    .line 218
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/f/a/a/b;->e:Lcom/google/f/a/w;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/b;->f:Lcom/google/f/a/a/f;

    if-eqz v1, :cond_5

    .line 222
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/f/a/a/b;->f:Lcom/google/f/a/a/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 225
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/a/b;->g:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 226
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/f/a/a/b;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 229
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/a/b;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 230
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/f/a/a/b;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 67
    if-ne p1, p0, :cond_1

    .line 68
    const/4 v0, 0x1

    .line 142
    :cond_0
    :goto_0
    return v0

    .line 70
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/a/b;

    if-eqz v1, :cond_0

    .line 73
    check-cast p1, Lcom/google/f/a/a/b;

    .line 74
    iget-object v1, p0, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    if-nez v1, :cond_a

    .line 75
    iget-object v1, p1, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    if-nez v1, :cond_0

    .line 83
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    if-nez v1, :cond_b

    .line 84
    iget-object v1, p1, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    if-nez v1, :cond_0

    .line 92
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    if-nez v1, :cond_c

    .line 93
    iget-object v1, p1, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    if-nez v1, :cond_0

    .line 101
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    if-nez v1, :cond_d

    .line 102
    iget-object v1, p1, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    if-nez v1, :cond_0

    .line 110
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/a/b;->e:Lcom/google/f/a/w;

    if-nez v1, :cond_e

    .line 111
    iget-object v1, p1, Lcom/google/f/a/a/b;->e:Lcom/google/f/a/w;

    if-nez v1, :cond_0

    .line 119
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/a/b;->f:Lcom/google/f/a/a/f;

    if-nez v1, :cond_f

    .line 120
    iget-object v1, p1, Lcom/google/f/a/a/b;->f:Lcom/google/f/a/a/f;

    if-nez v1, :cond_0

    .line 128
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/a/b;->g:Ljava/lang/Boolean;

    if-nez v1, :cond_10

    .line 129
    iget-object v1, p1, Lcom/google/f/a/a/b;->g:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 135
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/a/b;->h:Ljava/lang/String;

    if-nez v1, :cond_11

    .line 136
    iget-object v1, p1, Lcom/google/f/a/a/b;->h:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 142
    :cond_9
    invoke-virtual {p0, p1}, Lcom/google/f/a/a/b;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 79
    :cond_a
    iget-object v1, p0, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    iget-object v2, p1, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 88
    :cond_b
    iget-object v1, p0, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    iget-object v2, p1, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/u;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 97
    :cond_c
    iget-object v1, p0, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    iget-object v2, p1, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    invoke-virtual {v1, v2}, Lcom/google/f/a/w;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 106
    :cond_d
    iget-object v1, p0, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    iget-object v2, p1, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/r;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 115
    :cond_e
    iget-object v1, p0, Lcom/google/f/a/a/b;->e:Lcom/google/f/a/w;

    iget-object v2, p1, Lcom/google/f/a/a/b;->e:Lcom/google/f/a/w;

    invoke-virtual {v1, v2}, Lcom/google/f/a/w;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 124
    :cond_f
    iget-object v1, p0, Lcom/google/f/a/a/b;->f:Lcom/google/f/a/a/f;

    iget-object v2, p1, Lcom/google/f/a/a/b;->f:Lcom/google/f/a/a/f;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/f;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 132
    :cond_10
    iget-object v1, p0, Lcom/google/f/a/a/b;->g:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/a/b;->g:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 139
    :cond_11
    iget-object v1, p0, Lcom/google/f/a/a/b;->h:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/f/a/a/b;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 147
    iget-object v0, p0, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 150
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 152
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 154
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 156
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/b;->e:Lcom/google/f/a/w;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 158
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/b;->f:Lcom/google/f/a/a/f;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 160
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/b;->g:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 162
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/a/b;->h:Ljava/lang/String;

    if-nez v2, :cond_7

    :goto_7
    add-int/2addr v0, v1

    .line 164
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/a/b;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    return v0

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    invoke-virtual {v0}, Lcom/google/f/a/a/t;->hashCode()I

    move-result v0

    goto :goto_0

    .line 150
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    invoke-virtual {v0}, Lcom/google/f/a/a/u;->hashCode()I

    move-result v0

    goto :goto_1

    .line 152
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    invoke-virtual {v0}, Lcom/google/f/a/w;->hashCode()I

    move-result v0

    goto :goto_2

    .line 154
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    invoke-virtual {v0}, Lcom/google/f/a/a/r;->hashCode()I

    move-result v0

    goto :goto_3

    .line 156
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/b;->e:Lcom/google/f/a/w;

    invoke-virtual {v0}, Lcom/google/f/a/w;->hashCode()I

    move-result v0

    goto :goto_4

    .line 158
    :cond_5
    iget-object v0, p0, Lcom/google/f/a/a/b;->f:Lcom/google/f/a/a/f;

    invoke-virtual {v0}, Lcom/google/f/a/a/f;->hashCode()I

    move-result v0

    goto :goto_5

    .line 160
    :cond_6
    iget-object v0, p0, Lcom/google/f/a/a/b;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_6

    .line 162
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/a/b;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/a/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/a/t;

    invoke-direct {v0}, Lcom/google/f/a/a/t;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/f/a/a/u;

    invoke-direct {v0}, Lcom/google/f/a/a/u;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/f/a/w;

    invoke-direct {v0}, Lcom/google/f/a/w;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/f/a/a/r;

    invoke-direct {v0}, Lcom/google/f/a/a/r;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/f/a/a/b;->e:Lcom/google/f/a/w;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/f/a/w;

    invoke-direct {v0}, Lcom/google/f/a/w;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/b;->e:Lcom/google/f/a/w;

    :cond_5
    iget-object v0, p0, Lcom/google/f/a/a/b;->e:Lcom/google/f/a/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/f/a/a/b;->f:Lcom/google/f/a/a/f;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/f/a/a/f;

    invoke-direct {v0}, Lcom/google/f/a/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/b;->f:Lcom/google/f/a/a/f;

    :cond_6
    iget-object v0, p0, Lcom/google/f/a/a/b;->f:Lcom/google/f/a/a/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/b;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/b;->h:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    if-eqz v0, :cond_0

    .line 172
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/a/b;->a:Lcom/google/f/a/a/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    if-eqz v0, :cond_1

    .line 175
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/a/b;->b:Lcom/google/f/a/a/u;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 177
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    if-eqz v0, :cond_2

    .line 178
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/f/a/a/b;->c:Lcom/google/f/a/w;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 180
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    if-eqz v0, :cond_3

    .line 181
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/f/a/a/b;->d:Lcom/google/f/a/a/r;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 183
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/b;->e:Lcom/google/f/a/w;

    if-eqz v0, :cond_4

    .line 184
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/f/a/a/b;->e:Lcom/google/f/a/w;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 186
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/b;->f:Lcom/google/f/a/a/f;

    if-eqz v0, :cond_5

    .line 187
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/f/a/a/b;->f:Lcom/google/f/a/a/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 189
    :cond_5
    iget-object v0, p0, Lcom/google/f/a/a/b;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 190
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/f/a/a/b;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 192
    :cond_6
    iget-object v0, p0, Lcom/google/f/a/a/b;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 193
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/f/a/a/b;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 195
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 196
    return-void
.end method
