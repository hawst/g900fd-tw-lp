.class public final Lcom/google/f/a/h;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Lcom/google/f/a/i;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 205
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 206
    iput-object v0, p0, Lcom/google/f/a/h;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/f/a/h;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/f/a/h;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    iput-object v0, p0, Lcom/google/f/a/h;->e:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/f/a/h;->f:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/f/a/h;->g:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/f/a/h;->h:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/f/a/h;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/h;->cachedSize:I

    .line 207
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 344
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 345
    iget-object v1, p0, Lcom/google/f/a/h;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 346
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/h;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 349
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/h;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 350
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/f/a/h;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 353
    :cond_1
    iget-object v1, p0, Lcom/google/f/a/h;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 354
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/f/a/h;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 357
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    if-eqz v1, :cond_3

    .line 358
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 361
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/h;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 362
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/f/a/h;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 365
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/h;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 366
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/f/a/h;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 369
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/h;->g:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 370
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/f/a/h;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 373
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/h;->h:Ljava/lang/Boolean;

    if-eqz v1, :cond_7

    .line 374
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/f/a/h;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 377
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 225
    if-ne p1, p0, :cond_1

    .line 226
    const/4 v0, 0x1

    .line 288
    :cond_0
    :goto_0
    return v0

    .line 228
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/h;

    if-eqz v1, :cond_0

    .line 231
    check-cast p1, Lcom/google/f/a/h;

    .line 232
    iget-object v1, p0, Lcom/google/f/a/h;->a:Ljava/lang/Integer;

    if-nez v1, :cond_a

    .line 233
    iget-object v1, p1, Lcom/google/f/a/h;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 239
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/h;->b:Ljava/lang/Integer;

    if-nez v1, :cond_b

    .line 240
    iget-object v1, p1, Lcom/google/f/a/h;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 246
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/h;->c:Ljava/lang/Integer;

    if-nez v1, :cond_c

    .line 247
    iget-object v1, p1, Lcom/google/f/a/h;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 253
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    if-nez v1, :cond_d

    .line 254
    iget-object v1, p1, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    if-nez v1, :cond_0

    .line 262
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/h;->e:Ljava/lang/Integer;

    if-nez v1, :cond_e

    .line 263
    iget-object v1, p1, Lcom/google/f/a/h;->e:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 268
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/h;->f:Ljava/lang/Integer;

    if-nez v1, :cond_f

    .line 269
    iget-object v1, p1, Lcom/google/f/a/h;->f:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 274
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/h;->g:Ljava/lang/Long;

    if-nez v1, :cond_10

    .line 275
    iget-object v1, p1, Lcom/google/f/a/h;->g:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 281
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/h;->h:Ljava/lang/Boolean;

    if-nez v1, :cond_11

    .line 282
    iget-object v1, p1, Lcom/google/f/a/h;->h:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 288
    :cond_9
    invoke-virtual {p0, p1}, Lcom/google/f/a/h;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 236
    :cond_a
    iget-object v1, p0, Lcom/google/f/a/h;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/h;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 243
    :cond_b
    iget-object v1, p0, Lcom/google/f/a/h;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/h;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 250
    :cond_c
    iget-object v1, p0, Lcom/google/f/a/h;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/h;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 258
    :cond_d
    iget-object v1, p0, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    iget-object v2, p1, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    invoke-virtual {v1, v2}, Lcom/google/f/a/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 266
    :cond_e
    iget-object v1, p0, Lcom/google/f/a/h;->e:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/h;->e:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 272
    :cond_f
    iget-object v1, p0, Lcom/google/f/a/h;->f:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/h;->f:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 278
    :cond_10
    iget-object v1, p0, Lcom/google/f/a/h;->g:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/f/a/h;->g:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 285
    :cond_11
    iget-object v1, p0, Lcom/google/f/a/h;->h:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/h;->h:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 293
    iget-object v0, p0, Lcom/google/f/a/h;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 296
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/h;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 298
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/h;->c:Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 300
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 302
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/h;->e:Ljava/lang/Integer;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 303
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/h;->f:Ljava/lang/Integer;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 304
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/h;->g:Ljava/lang/Long;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 306
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/h;->h:Ljava/lang/Boolean;

    if-nez v2, :cond_7

    :goto_7
    add-int/2addr v0, v1

    .line 308
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/h;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 309
    return v0

    .line 293
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/h;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_0

    .line 296
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/h;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_1

    .line 298
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/h;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_2

    .line 300
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    invoke-virtual {v0}, Lcom/google/f/a/i;->hashCode()I

    move-result v0

    goto :goto_3

    .line 302
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/h;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_4

    .line 303
    :cond_5
    iget-object v0, p0, Lcom/google/f/a/h;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_5

    .line 304
    :cond_6
    iget-object v0, p0, Lcom/google/f/a/h;->g:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_6

    .line 306
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/h;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_7
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/h;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/h;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/h;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/h;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/i;

    invoke-direct {v0}, Lcom/google/f/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/h;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/h;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/h;->g:Ljava/lang/Long;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/h;->h:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x39 -> :sswitch_7
        0x40 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/f/a/h;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 316
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/h;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 318
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/h;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 319
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/h;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 321
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/h;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 322
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/f/a/h;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 324
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    if-eqz v0, :cond_3

    .line 325
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/f/a/h;->d:Lcom/google/f/a/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 327
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/h;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 328
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/f/a/h;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 330
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/h;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 331
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/f/a/h;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 333
    :cond_5
    iget-object v0, p0, Lcom/google/f/a/h;->g:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 334
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/f/a/h;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 336
    :cond_6
    iget-object v0, p0, Lcom/google/f/a/h;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_7

    .line 337
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/f/a/h;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 339
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 340
    return-void
.end method
