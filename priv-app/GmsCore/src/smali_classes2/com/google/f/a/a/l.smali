.class public final Lcom/google/f/a/a/l;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/f/a/a/r;

.field public b:Lcom/google/f/a/a/q;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 29
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 30
    invoke-static {}, Lcom/google/f/a/a/r;->a()[Lcom/google/f/a/a/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    iput-object v1, p0, Lcom/google/f/a/a/l;->b:Lcom/google/f/a/a/q;

    iput-object v1, p0, Lcom/google/f/a/a/l;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/a/l;->cachedSize:I

    .line 31
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 96
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 97
    iget-object v0, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 98
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 99
    iget-object v2, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    aget-object v2, v2, v0

    .line 100
    if-eqz v2, :cond_0

    .line 101
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 98
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/l;->b:Lcom/google/f/a/a/q;

    if-eqz v0, :cond_2

    .line 107
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/f/a/a/l;->b:Lcom/google/f/a/a/q;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 110
    :cond_2
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 43
    if-ne p1, p0, :cond_1

    .line 44
    const/4 v0, 0x1

    .line 63
    :cond_0
    :goto_0
    return v0

    .line 46
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/a/l;

    if-eqz v1, :cond_0

    .line 49
    check-cast p1, Lcom/google/f/a/a/l;

    .line 50
    iget-object v1, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    iget-object v2, p1, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 54
    iget-object v1, p0, Lcom/google/f/a/a/l;->b:Lcom/google/f/a/a/q;

    if-nez v1, :cond_3

    .line 55
    iget-object v1, p1, Lcom/google/f/a/a/l;->b:Lcom/google/f/a/a/q;

    if-nez v1, :cond_0

    .line 63
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/f/a/a/l;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 59
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/l;->b:Lcom/google/f/a/a/q;

    iget-object v2, p1, Lcom/google/f/a/a/l;->b:Lcom/google/f/a/a/q;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 71
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/l;->b:Lcom/google/f/a/a/q;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 73
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/a/l;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 74
    return v0

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/l;->b:Lcom/google/f/a/a/q;

    invoke-virtual {v0}, Lcom/google/f/a/a/q;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/a/l;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/f/a/a/r;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/f/a/a/r;

    invoke-direct {v3}, Lcom/google/f/a/a/r;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/f/a/a/r;

    invoke-direct {v3}, Lcom/google/f/a/a/r;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/f/a/a/l;->b:Lcom/google/f/a/a/q;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/f/a/a/q;

    invoke-direct {v0}, Lcom/google/f/a/a/q;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/l;->b:Lcom/google/f/a/a/q;

    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/l;->b:Lcom/google/f/a/a/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 81
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 82
    iget-object v1, p0, Lcom/google/f/a/a/l;->a:[Lcom/google/f/a/a/r;

    aget-object v1, v1, v0

    .line 83
    if-eqz v1, :cond_0

    .line 84
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 81
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 88
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/l;->b:Lcom/google/f/a/a/q;

    if-eqz v0, :cond_2

    .line 89
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/a/l;->b:Lcom/google/f/a/a/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 91
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 92
    return-void
.end method
