.class public final Lcom/google/f/a/a/r;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/protobuf/nano/e;

.field public static final b:Lcom/google/protobuf/nano/e;

.field private static final y:[Lcom/google/f/a/a/r;


# instance fields
.field public c:Lcom/google/f/a/w;

.field public d:Lcom/google/f/a/a/y;

.field public e:Lcom/google/f/a/w;

.field public f:Lcom/google/f/a/a/u;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/Long;

.field public i:Ljava/lang/Long;

.field public j:Ljava/lang/Boolean;

.field public k:Ljava/lang/Boolean;

.field public l:Ljava/lang/Boolean;

.field public m:Ljava/lang/Boolean;

.field public n:Ljava/lang/Long;

.field public o:Lcom/google/f/a/h;

.field public p:Lcom/google/f/a/h;

.field public q:Lcom/google/f/a/j;

.field public r:Lcom/google/f/a/l;

.field public s:Ljava/lang/Long;

.field public t:Lcom/google/f/a/v;

.field public u:Lcom/google/f/a/u;

.field public v:Lcom/google/f/a/b;

.field public w:Lcom/google/f/a/a/f;

.field public x:Lcom/google/f/a/f;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 13
    const-class v0, Lcom/google/f/a/a/s;

    const v1, 0x1ca5d29a

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/e;->a(Ljava/lang/Class;I)Lcom/google/protobuf/nano/e;

    move-result-object v0

    sput-object v0, Lcom/google/f/a/a/r;->a:Lcom/google/protobuf/nano/e;

    .line 23
    const-class v0, Lcom/google/f/a/a/r;

    const v1, 0x1651cf02

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/e;->a(Ljava/lang/Class;I)Lcom/google/protobuf/nano/e;

    move-result-object v0

    sput-object v0, Lcom/google/f/a/a/r;->b:Lcom/google/protobuf/nano/e;

    .line 106
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/f/a/a/r;

    sput-object v0, Lcom/google/f/a/a/r;->y:[Lcom/google/f/a/a/r;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 177
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 178
    iput-object v0, p0, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    iput-object v0, p0, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    iput-object v0, p0, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    iput-object v0, p0, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    iput-object v0, p0, Lcom/google/f/a/a/r;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/f/a/a/r;->h:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/f/a/a/r;->i:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/f/a/a/r;->j:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/f/a/a/r;->l:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/f/a/a/r;->m:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/f/a/a/r;->n:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    iput-object v0, p0, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    iput-object v0, p0, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    iput-object v0, p0, Lcom/google/f/a/a/r;->r:Lcom/google/f/a/l;

    iput-object v0, p0, Lcom/google/f/a/a/r;->s:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/f/a/a/r;->t:Lcom/google/f/a/v;

    iput-object v0, p0, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    iput-object v0, p0, Lcom/google/f/a/a/r;->v:Lcom/google/f/a/b;

    iput-object v0, p0, Lcom/google/f/a/a/r;->w:Lcom/google/f/a/a/f;

    iput-object v0, p0, Lcom/google/f/a/a/r;->x:Lcom/google/f/a/f;

    iput-object v0, p0, Lcom/google/f/a/a/r;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/a/r;->cachedSize:I

    .line 179
    return-void
.end method

.method public static a()[Lcom/google/f/a/a/r;
    .locals 1

    .prologue
    .line 108
    sget-object v0, Lcom/google/f/a/a/r;->y:[Lcom/google/f/a/a/r;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 526
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 527
    iget-object v1, p0, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    if-eqz v1, :cond_0

    .line 528
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 531
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    if-eqz v1, :cond_1

    .line 532
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 535
    :cond_1
    iget-object v1, p0, Lcom/google/f/a/a/r;->g:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 536
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/f/a/a/r;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 539
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    if-eqz v1, :cond_3

    .line 540
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 543
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    if-eqz v1, :cond_4

    .line 544
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 547
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    if-eqz v1, :cond_5

    .line 548
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 551
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/a/r;->j:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 552
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/f/a/a/r;->j:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 555
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    if-eqz v1, :cond_7

    .line 556
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 559
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    if-eqz v1, :cond_8

    .line 560
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 563
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/a/r;->i:Ljava/lang/Long;

    if-eqz v1, :cond_9

    .line 564
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/f/a/a/r;->i:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    :cond_9
    iget-object v1, p0, Lcom/google/f/a/a/r;->r:Lcom/google/f/a/l;

    if-eqz v1, :cond_a

    .line 568
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/f/a/a/r;->r:Lcom/google/f/a/l;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 571
    :cond_a
    iget-object v1, p0, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    if-eqz v1, :cond_b

    .line 572
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    :cond_b
    iget-object v1, p0, Lcom/google/f/a/a/r;->s:Ljava/lang/Long;

    if-eqz v1, :cond_c

    .line 576
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/f/a/a/r;->s:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 579
    :cond_c
    iget-object v1, p0, Lcom/google/f/a/a/r;->t:Lcom/google/f/a/v;

    if-eqz v1, :cond_d

    .line 580
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/f/a/a/r;->t:Lcom/google/f/a/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 583
    :cond_d
    iget-object v1, p0, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    if-eqz v1, :cond_e

    .line 584
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 587
    :cond_e
    iget-object v1, p0, Lcom/google/f/a/a/r;->v:Lcom/google/f/a/b;

    if-eqz v1, :cond_f

    .line 588
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/f/a/a/r;->v:Lcom/google/f/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 591
    :cond_f
    iget-object v1, p0, Lcom/google/f/a/a/r;->h:Ljava/lang/Long;

    if-eqz v1, :cond_10

    .line 592
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/f/a/a/r;->h:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 595
    :cond_10
    iget-object v1, p0, Lcom/google/f/a/a/r;->w:Lcom/google/f/a/a/f;

    if-eqz v1, :cond_11

    .line 596
    const/16 v1, 0x13

    iget-object v2, p0, Lcom/google/f/a/a/r;->w:Lcom/google/f/a/a/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 599
    :cond_11
    iget-object v1, p0, Lcom/google/f/a/a/r;->x:Lcom/google/f/a/f;

    if-eqz v1, :cond_12

    .line 600
    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/f/a/a/r;->x:Lcom/google/f/a/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 603
    :cond_12
    iget-object v1, p0, Lcom/google/f/a/a/r;->l:Ljava/lang/Boolean;

    if-eqz v1, :cond_13

    .line 604
    const/16 v1, 0x15

    iget-object v2, p0, Lcom/google/f/a/a/r;->l:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 607
    :cond_13
    iget-object v1, p0, Lcom/google/f/a/a/r;->m:Ljava/lang/Boolean;

    if-eqz v1, :cond_14

    .line 608
    const/16 v1, 0x16

    iget-object v2, p0, Lcom/google/f/a/a/r;->m:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 611
    :cond_14
    iget-object v1, p0, Lcom/google/f/a/a/r;->n:Ljava/lang/Long;

    if-eqz v1, :cond_15

    .line 612
    const/16 v1, 0x17

    iget-object v2, p0, Lcom/google/f/a/a/r;->n:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 615
    :cond_15
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 211
    if-ne p1, p0, :cond_1

    .line 212
    const/4 v0, 0x1

    .line 398
    :cond_0
    :goto_0
    return v0

    .line 214
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/a/r;

    if-eqz v1, :cond_0

    .line 217
    check-cast p1, Lcom/google/f/a/a/r;

    .line 218
    iget-object v1, p0, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    if-nez v1, :cond_18

    .line 219
    iget-object v1, p1, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    if-nez v1, :cond_0

    .line 227
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    if-nez v1, :cond_19

    .line 228
    iget-object v1, p1, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    if-nez v1, :cond_0

    .line 236
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    if-nez v1, :cond_1a

    .line 237
    iget-object v1, p1, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    if-nez v1, :cond_0

    .line 245
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    if-nez v1, :cond_1b

    .line 246
    iget-object v1, p1, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    if-nez v1, :cond_0

    .line 254
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/a/r;->g:Ljava/lang/String;

    if-nez v1, :cond_1c

    .line 255
    iget-object v1, p1, Lcom/google/f/a/a/r;->g:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 261
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/a/r;->h:Ljava/lang/Long;

    if-nez v1, :cond_1d

    .line 262
    iget-object v1, p1, Lcom/google/f/a/a/r;->h:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 268
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/a/r;->i:Ljava/lang/Long;

    if-nez v1, :cond_1e

    .line 269
    iget-object v1, p1, Lcom/google/f/a/a/r;->i:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 275
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/a/r;->j:Ljava/lang/Boolean;

    if-nez v1, :cond_1f

    .line 276
    iget-object v1, p1, Lcom/google/f/a/a/r;->j:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 282
    :cond_9
    iget-object v1, p0, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    if-nez v1, :cond_20

    .line 283
    iget-object v1, p1, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 289
    :cond_a
    iget-object v1, p0, Lcom/google/f/a/a/r;->l:Ljava/lang/Boolean;

    if-nez v1, :cond_21

    .line 290
    iget-object v1, p1, Lcom/google/f/a/a/r;->l:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 296
    :cond_b
    iget-object v1, p0, Lcom/google/f/a/a/r;->m:Ljava/lang/Boolean;

    if-nez v1, :cond_22

    .line 297
    iget-object v1, p1, Lcom/google/f/a/a/r;->m:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 303
    :cond_c
    iget-object v1, p0, Lcom/google/f/a/a/r;->n:Ljava/lang/Long;

    if-nez v1, :cond_23

    .line 304
    iget-object v1, p1, Lcom/google/f/a/a/r;->n:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 310
    :cond_d
    iget-object v1, p0, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    if-nez v1, :cond_24

    .line 311
    iget-object v1, p1, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    if-nez v1, :cond_0

    .line 319
    :cond_e
    iget-object v1, p0, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    if-nez v1, :cond_25

    .line 320
    iget-object v1, p1, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    if-nez v1, :cond_0

    .line 328
    :cond_f
    iget-object v1, p0, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    if-nez v1, :cond_26

    .line 329
    iget-object v1, p1, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    if-nez v1, :cond_0

    .line 337
    :cond_10
    iget-object v1, p0, Lcom/google/f/a/a/r;->r:Lcom/google/f/a/l;

    if-nez v1, :cond_27

    .line 338
    iget-object v1, p1, Lcom/google/f/a/a/r;->r:Lcom/google/f/a/l;

    if-nez v1, :cond_0

    .line 346
    :cond_11
    iget-object v1, p0, Lcom/google/f/a/a/r;->s:Ljava/lang/Long;

    if-nez v1, :cond_28

    .line 347
    iget-object v1, p1, Lcom/google/f/a/a/r;->s:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 353
    :cond_12
    iget-object v1, p0, Lcom/google/f/a/a/r;->t:Lcom/google/f/a/v;

    if-nez v1, :cond_29

    .line 354
    iget-object v1, p1, Lcom/google/f/a/a/r;->t:Lcom/google/f/a/v;

    if-nez v1, :cond_0

    .line 362
    :cond_13
    iget-object v1, p0, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    if-nez v1, :cond_2a

    .line 363
    iget-object v1, p1, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    if-nez v1, :cond_0

    .line 371
    :cond_14
    iget-object v1, p0, Lcom/google/f/a/a/r;->v:Lcom/google/f/a/b;

    if-nez v1, :cond_2b

    .line 372
    iget-object v1, p1, Lcom/google/f/a/a/r;->v:Lcom/google/f/a/b;

    if-nez v1, :cond_0

    .line 380
    :cond_15
    iget-object v1, p0, Lcom/google/f/a/a/r;->w:Lcom/google/f/a/a/f;

    if-nez v1, :cond_2c

    .line 381
    iget-object v1, p1, Lcom/google/f/a/a/r;->w:Lcom/google/f/a/a/f;

    if-nez v1, :cond_0

    .line 389
    :cond_16
    iget-object v1, p0, Lcom/google/f/a/a/r;->x:Lcom/google/f/a/f;

    if-nez v1, :cond_2d

    .line 390
    iget-object v1, p1, Lcom/google/f/a/a/r;->x:Lcom/google/f/a/f;

    if-nez v1, :cond_0

    .line 398
    :cond_17
    invoke-virtual {p0, p1}, Lcom/google/f/a/a/r;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto/16 :goto_0

    .line 223
    :cond_18
    iget-object v1, p0, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    iget-object v2, p1, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    invoke-virtual {v1, v2}, Lcom/google/f/a/w;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0

    .line 232
    :cond_19
    iget-object v1, p0, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    iget-object v2, p1, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/y;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 241
    :cond_1a
    iget-object v1, p0, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    iget-object v2, p1, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    invoke-virtual {v1, v2}, Lcom/google/f/a/w;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 250
    :cond_1b
    iget-object v1, p0, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    iget-object v2, p1, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/u;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 258
    :cond_1c
    iget-object v1, p0, Lcom/google/f/a/a/r;->g:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/f/a/a/r;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 265
    :cond_1d
    iget-object v1, p0, Lcom/google/f/a/a/r;->h:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/f/a/a/r;->h:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 272
    :cond_1e
    iget-object v1, p0, Lcom/google/f/a/a/r;->i:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/f/a/a/r;->i:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 279
    :cond_1f
    iget-object v1, p0, Lcom/google/f/a/a/r;->j:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/a/r;->j:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 286
    :cond_20
    iget-object v1, p0, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 293
    :cond_21
    iget-object v1, p0, Lcom/google/f/a/a/r;->l:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/a/r;->l:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    .line 300
    :cond_22
    iget-object v1, p0, Lcom/google/f/a/a/r;->m:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/a/r;->m:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    goto/16 :goto_0

    .line 307
    :cond_23
    iget-object v1, p0, Lcom/google/f/a/a/r;->n:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/f/a/a/r;->n:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    goto/16 :goto_0

    .line 315
    :cond_24
    iget-object v1, p0, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    iget-object v2, p1, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    invoke-virtual {v1, v2}, Lcom/google/f/a/h;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    goto/16 :goto_0

    .line 324
    :cond_25
    iget-object v1, p0, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    iget-object v2, p1, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    invoke-virtual {v1, v2}, Lcom/google/f/a/h;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    goto/16 :goto_0

    .line 333
    :cond_26
    iget-object v1, p0, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    iget-object v2, p1, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    invoke-virtual {v1, v2}, Lcom/google/f/a/j;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_10

    goto/16 :goto_0

    .line 342
    :cond_27
    iget-object v1, p0, Lcom/google/f/a/a/r;->r:Lcom/google/f/a/l;

    iget-object v2, p1, Lcom/google/f/a/a/r;->r:Lcom/google/f/a/l;

    invoke-virtual {v1, v2}, Lcom/google/f/a/l;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_11

    goto/16 :goto_0

    .line 350
    :cond_28
    iget-object v1, p0, Lcom/google/f/a/a/r;->s:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/f/a/a/r;->s:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_12

    goto/16 :goto_0

    .line 358
    :cond_29
    iget-object v1, p0, Lcom/google/f/a/a/r;->t:Lcom/google/f/a/v;

    iget-object v2, p1, Lcom/google/f/a/a/r;->t:Lcom/google/f/a/v;

    invoke-virtual {v1, v2}, Lcom/google/f/a/v;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_13

    goto/16 :goto_0

    .line 367
    :cond_2a
    iget-object v1, p0, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    iget-object v2, p1, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    invoke-virtual {v1, v2}, Lcom/google/f/a/u;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_14

    goto/16 :goto_0

    .line 376
    :cond_2b
    iget-object v1, p0, Lcom/google/f/a/a/r;->v:Lcom/google/f/a/b;

    iget-object v2, p1, Lcom/google/f/a/a/r;->v:Lcom/google/f/a/b;

    invoke-virtual {v1, v2}, Lcom/google/f/a/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_15

    goto/16 :goto_0

    .line 385
    :cond_2c
    iget-object v1, p0, Lcom/google/f/a/a/r;->w:Lcom/google/f/a/a/f;

    iget-object v2, p1, Lcom/google/f/a/a/r;->w:Lcom/google/f/a/a/f;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/f;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    goto/16 :goto_0

    .line 394
    :cond_2d
    iget-object v1, p0, Lcom/google/f/a/a/r;->x:Lcom/google/f/a/f;

    iget-object v2, p1, Lcom/google/f/a/a/r;->x:Lcom/google/f/a/f;

    invoke-virtual {v1, v2}, Lcom/google/f/a/f;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_17

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 403
    iget-object v0, p0, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 406
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 408
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 410
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 412
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->g:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 414
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->h:Ljava/lang/Long;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 416
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->i:Ljava/lang/Long;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 418
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->j:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 420
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 422
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->l:Ljava/lang/Boolean;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 424
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->m:Ljava/lang/Boolean;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 426
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->n:Ljava/lang/Long;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    .line 428
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    .line 430
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    if-nez v0, :cond_d

    move v0, v1

    :goto_d
    add-int/2addr v0, v2

    .line 432
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    if-nez v0, :cond_e

    move v0, v1

    :goto_e
    add-int/2addr v0, v2

    .line 434
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->r:Lcom/google/f/a/l;

    if-nez v0, :cond_f

    move v0, v1

    :goto_f
    add-int/2addr v0, v2

    .line 436
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->s:Ljava/lang/Long;

    if-nez v0, :cond_10

    move v0, v1

    :goto_10
    add-int/2addr v0, v2

    .line 438
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->t:Lcom/google/f/a/v;

    if-nez v0, :cond_11

    move v0, v1

    :goto_11
    add-int/2addr v0, v2

    .line 440
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    if-nez v0, :cond_12

    move v0, v1

    :goto_12
    add-int/2addr v0, v2

    .line 442
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->v:Lcom/google/f/a/b;

    if-nez v0, :cond_13

    move v0, v1

    :goto_13
    add-int/2addr v0, v2

    .line 444
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/r;->w:Lcom/google/f/a/a/f;

    if-nez v0, :cond_14

    move v0, v1

    :goto_14
    add-int/2addr v0, v2

    .line 446
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/a/r;->x:Lcom/google/f/a/f;

    if-nez v2, :cond_15

    :goto_15
    add-int/2addr v0, v1

    .line 448
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/a/r;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 449
    return v0

    .line 403
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    invoke-virtual {v0}, Lcom/google/f/a/w;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 406
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    invoke-virtual {v0}, Lcom/google/f/a/a/y;->hashCode()I

    move-result v0

    goto/16 :goto_1

    .line 408
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    invoke-virtual {v0}, Lcom/google/f/a/w;->hashCode()I

    move-result v0

    goto/16 :goto_2

    .line 410
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    invoke-virtual {v0}, Lcom/google/f/a/a/u;->hashCode()I

    move-result v0

    goto/16 :goto_3

    .line 412
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/r;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto/16 :goto_4

    .line 414
    :cond_5
    iget-object v0, p0, Lcom/google/f/a/a/r;->h:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_5

    .line 416
    :cond_6
    iget-object v0, p0, Lcom/google/f/a/a/r;->i:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_6

    .line 418
    :cond_7
    iget-object v0, p0, Lcom/google/f/a/a/r;->j:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_7

    .line 420
    :cond_8
    iget-object v0, p0, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_8

    .line 422
    :cond_9
    iget-object v0, p0, Lcom/google/f/a/a/r;->l:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_9

    .line 424
    :cond_a
    iget-object v0, p0, Lcom/google/f/a/a/r;->m:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto/16 :goto_a

    .line 426
    :cond_b
    iget-object v0, p0, Lcom/google/f/a/a/r;->n:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_b

    .line 428
    :cond_c
    iget-object v0, p0, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    invoke-virtual {v0}, Lcom/google/f/a/h;->hashCode()I

    move-result v0

    goto/16 :goto_c

    .line 430
    :cond_d
    iget-object v0, p0, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    invoke-virtual {v0}, Lcom/google/f/a/h;->hashCode()I

    move-result v0

    goto/16 :goto_d

    .line 432
    :cond_e
    iget-object v0, p0, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    invoke-virtual {v0}, Lcom/google/f/a/j;->hashCode()I

    move-result v0

    goto/16 :goto_e

    .line 434
    :cond_f
    iget-object v0, p0, Lcom/google/f/a/a/r;->r:Lcom/google/f/a/l;

    invoke-virtual {v0}, Lcom/google/f/a/l;->hashCode()I

    move-result v0

    goto/16 :goto_f

    .line 436
    :cond_10
    iget-object v0, p0, Lcom/google/f/a/a/r;->s:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto/16 :goto_10

    .line 438
    :cond_11
    iget-object v0, p0, Lcom/google/f/a/a/r;->t:Lcom/google/f/a/v;

    invoke-virtual {v0}, Lcom/google/f/a/v;->hashCode()I

    move-result v0

    goto/16 :goto_11

    .line 440
    :cond_12
    iget-object v0, p0, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    invoke-virtual {v0}, Lcom/google/f/a/u;->hashCode()I

    move-result v0

    goto/16 :goto_12

    .line 442
    :cond_13
    iget-object v0, p0, Lcom/google/f/a/a/r;->v:Lcom/google/f/a/b;

    invoke-virtual {v0}, Lcom/google/f/a/b;->hashCode()I

    move-result v0

    goto/16 :goto_13

    .line 444
    :cond_14
    iget-object v0, p0, Lcom/google/f/a/a/r;->w:Lcom/google/f/a/a/f;

    invoke-virtual {v0}, Lcom/google/f/a/a/f;->hashCode()I

    move-result v0

    goto/16 :goto_14

    .line 446
    :cond_15
    iget-object v1, p0, Lcom/google/f/a/a/r;->x:Lcom/google/f/a/f;

    invoke-virtual {v1}, Lcom/google/f/a/f;->hashCode()I

    move-result v1

    goto/16 :goto_15
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/a/r;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/w;

    invoke-direct {v0}, Lcom/google/f/a/w;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/f/a/a/u;

    invoke-direct {v0}, Lcom/google/f/a/a/u;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/r;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/f/a/h;

    invoke-direct {v0}, Lcom/google/f/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/f/a/j;

    invoke-direct {v0}, Lcom/google/f/a/j;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/f/a/h;

    invoke-direct {v0}, Lcom/google/f/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    :cond_5
    iget-object v0, p0, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/r;->j:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/f/a/w;

    invoke-direct {v0}, Lcom/google/f/a/w;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    :cond_6
    iget-object v0, p0, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/r;->i:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/google/f/a/a/r;->r:Lcom/google/f/a/l;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/f/a/l;

    invoke-direct {v0}, Lcom/google/f/a/l;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/r;->r:Lcom/google/f/a/l;

    :cond_7
    iget-object v0, p0, Lcom/google/f/a/a/r;->r:Lcom/google/f/a/l;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    if-nez v0, :cond_8

    new-instance v0, Lcom/google/f/a/a/y;

    invoke-direct {v0}, Lcom/google/f/a/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    :cond_8
    iget-object v0, p0, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/r;->s:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_e
    iget-object v0, p0, Lcom/google/f/a/a/r;->t:Lcom/google/f/a/v;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/f/a/v;

    invoke-direct {v0}, Lcom/google/f/a/v;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/r;->t:Lcom/google/f/a/v;

    :cond_9
    iget-object v0, p0, Lcom/google/f/a/a/r;->t:Lcom/google/f/a/v;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_f
    iget-object v0, p0, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/f/a/u;

    invoke-direct {v0}, Lcom/google/f/a/u;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    :cond_a
    iget-object v0, p0, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Lcom/google/f/a/a/r;->v:Lcom/google/f/a/b;

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/f/a/b;

    invoke-direct {v0}, Lcom/google/f/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/r;->v:Lcom/google/f/a/b;

    :cond_b
    iget-object v0, p0, Lcom/google/f/a/a/r;->v:Lcom/google/f/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/r;->h:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_12
    iget-object v0, p0, Lcom/google/f/a/a/r;->w:Lcom/google/f/a/a/f;

    if-nez v0, :cond_c

    new-instance v0, Lcom/google/f/a/a/f;

    invoke-direct {v0}, Lcom/google/f/a/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/r;->w:Lcom/google/f/a/a/f;

    :cond_c
    iget-object v0, p0, Lcom/google/f/a/a/r;->w:Lcom/google/f/a/a/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_13
    iget-object v0, p0, Lcom/google/f/a/a/r;->x:Lcom/google/f/a/f;

    if-nez v0, :cond_d

    new-instance v0, Lcom/google/f/a/f;

    invoke-direct {v0}, Lcom/google/f/a/f;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/r;->x:Lcom/google/f/a/f;

    :cond_d
    iget-object v0, p0, Lcom/google/f/a/a/r;->x:Lcom/google/f/a/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_14
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/r;->l:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/r;->m:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_16
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/r;->n:Ljava/lang/Long;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x40 -> :sswitch_7
        0x4a -> :sswitch_8
        0x50 -> :sswitch_9
        0x58 -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x70 -> :sswitch_d
        0x7a -> :sswitch_e
        0x82 -> :sswitch_f
        0x8a -> :sswitch_10
        0x90 -> :sswitch_11
        0x9a -> :sswitch_12
        0xa2 -> :sswitch_13
        0xa8 -> :sswitch_14
        0xb0 -> :sswitch_15
        0xb9 -> :sswitch_16
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    if-eqz v0, :cond_0

    .line 456
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/a/r;->c:Lcom/google/f/a/w;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 458
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    if-eqz v0, :cond_1

    .line 459
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/a/r;->f:Lcom/google/f/a/a/u;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 461
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/r;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 462
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/f/a/a/r;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 464
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    if-eqz v0, :cond_3

    .line 465
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/f/a/a/r;->o:Lcom/google/f/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 467
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    if-eqz v0, :cond_4

    .line 468
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/f/a/a/r;->q:Lcom/google/f/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 470
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    if-eqz v0, :cond_5

    .line 471
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/f/a/a/r;->p:Lcom/google/f/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 473
    :cond_5
    iget-object v0, p0, Lcom/google/f/a/a/r;->j:Ljava/lang/Boolean;

    if-eqz v0, :cond_6

    .line 474
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/f/a/a/r;->j:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 476
    :cond_6
    iget-object v0, p0, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    if-eqz v0, :cond_7

    .line 477
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/f/a/a/r;->e:Lcom/google/f/a/w;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 479
    :cond_7
    iget-object v0, p0, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 480
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/f/a/a/r;->k:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 482
    :cond_8
    iget-object v0, p0, Lcom/google/f/a/a/r;->i:Ljava/lang/Long;

    if-eqz v0, :cond_9

    .line 483
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/f/a/a/r;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 485
    :cond_9
    iget-object v0, p0, Lcom/google/f/a/a/r;->r:Lcom/google/f/a/l;

    if-eqz v0, :cond_a

    .line 486
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/f/a/a/r;->r:Lcom/google/f/a/l;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 488
    :cond_a
    iget-object v0, p0, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    if-eqz v0, :cond_b

    .line 489
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/f/a/a/r;->d:Lcom/google/f/a/a/y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 491
    :cond_b
    iget-object v0, p0, Lcom/google/f/a/a/r;->s:Ljava/lang/Long;

    if-eqz v0, :cond_c

    .line 492
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/f/a/a/r;->s:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 494
    :cond_c
    iget-object v0, p0, Lcom/google/f/a/a/r;->t:Lcom/google/f/a/v;

    if-eqz v0, :cond_d

    .line 495
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/f/a/a/r;->t:Lcom/google/f/a/v;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 497
    :cond_d
    iget-object v0, p0, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    if-eqz v0, :cond_e

    .line 498
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/f/a/a/r;->u:Lcom/google/f/a/u;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 500
    :cond_e
    iget-object v0, p0, Lcom/google/f/a/a/r;->v:Lcom/google/f/a/b;

    if-eqz v0, :cond_f

    .line 501
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/f/a/a/r;->v:Lcom/google/f/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 503
    :cond_f
    iget-object v0, p0, Lcom/google/f/a/a/r;->h:Ljava/lang/Long;

    if-eqz v0, :cond_10

    .line 504
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/f/a/a/r;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 506
    :cond_10
    iget-object v0, p0, Lcom/google/f/a/a/r;->w:Lcom/google/f/a/a/f;

    if-eqz v0, :cond_11

    .line 507
    const/16 v0, 0x13

    iget-object v1, p0, Lcom/google/f/a/a/r;->w:Lcom/google/f/a/a/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 509
    :cond_11
    iget-object v0, p0, Lcom/google/f/a/a/r;->x:Lcom/google/f/a/f;

    if-eqz v0, :cond_12

    .line 510
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/f/a/a/r;->x:Lcom/google/f/a/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 512
    :cond_12
    iget-object v0, p0, Lcom/google/f/a/a/r;->l:Ljava/lang/Boolean;

    if-eqz v0, :cond_13

    .line 513
    const/16 v0, 0x15

    iget-object v1, p0, Lcom/google/f/a/a/r;->l:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 515
    :cond_13
    iget-object v0, p0, Lcom/google/f/a/a/r;->m:Ljava/lang/Boolean;

    if-eqz v0, :cond_14

    .line 516
    const/16 v0, 0x16

    iget-object v1, p0, Lcom/google/f/a/a/r;->m:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 518
    :cond_14
    iget-object v0, p0, Lcom/google/f/a/a/r;->n:Ljava/lang/Long;

    if-eqz v0, :cond_15

    .line 519
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/f/a/a/r;->n:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 521
    :cond_15
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 522
    return-void
.end method
