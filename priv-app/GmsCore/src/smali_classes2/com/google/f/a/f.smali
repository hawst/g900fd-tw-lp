.class public final Lcom/google/f/a/f;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/f/a/g;

.field public b:Lcom/google/f/a/h;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 142
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 143
    iput-object v0, p0, Lcom/google/f/a/f;->a:Lcom/google/f/a/g;

    iput-object v0, p0, Lcom/google/f/a/f;->b:Lcom/google/f/a/h;

    iput-object v0, p0, Lcom/google/f/a/f;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/f;->cachedSize:I

    .line 144
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 209
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 210
    iget-object v1, p0, Lcom/google/f/a/f;->a:Lcom/google/f/a/g;

    if-eqz v1, :cond_0

    .line 211
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/f;->a:Lcom/google/f/a/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 214
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/f;->b:Lcom/google/f/a/h;

    if-eqz v1, :cond_1

    .line 215
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/f/a/f;->b:Lcom/google/f/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 218
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 156
    if-ne p1, p0, :cond_1

    .line 157
    const/4 v0, 0x1

    .line 181
    :cond_0
    :goto_0
    return v0

    .line 159
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/f;

    if-eqz v1, :cond_0

    .line 162
    check-cast p1, Lcom/google/f/a/f;

    .line 163
    iget-object v1, p0, Lcom/google/f/a/f;->a:Lcom/google/f/a/g;

    if-nez v1, :cond_4

    .line 164
    iget-object v1, p1, Lcom/google/f/a/f;->a:Lcom/google/f/a/g;

    if-nez v1, :cond_0

    .line 172
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/f;->b:Lcom/google/f/a/h;

    if-nez v1, :cond_5

    .line 173
    iget-object v1, p1, Lcom/google/f/a/f;->b:Lcom/google/f/a/h;

    if-nez v1, :cond_0

    .line 181
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/f/a/f;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 168
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/f;->a:Lcom/google/f/a/g;

    iget-object v2, p1, Lcom/google/f/a/f;->a:Lcom/google/f/a/g;

    invoke-virtual {v1, v2}, Lcom/google/f/a/g;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 177
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/f;->b:Lcom/google/f/a/h;

    iget-object v2, p1, Lcom/google/f/a/f;->b:Lcom/google/f/a/h;

    invoke-virtual {v1, v2}, Lcom/google/f/a/h;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 186
    iget-object v0, p0, Lcom/google/f/a/f;->a:Lcom/google/f/a/g;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 189
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/f;->b:Lcom/google/f/a/h;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 191
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/f;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 192
    return v0

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/f;->a:Lcom/google/f/a/g;

    invoke-virtual {v0}, Lcom/google/f/a/g;->hashCode()I

    move-result v0

    goto :goto_0

    .line 189
    :cond_1
    iget-object v1, p0, Lcom/google/f/a/f;->b:Lcom/google/f/a/h;

    invoke-virtual {v1}, Lcom/google/f/a/h;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/f;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/f/a/f;->a:Lcom/google/f/a/g;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/g;

    invoke-direct {v0}, Lcom/google/f/a/g;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/f;->a:Lcom/google/f/a/g;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/f;->a:Lcom/google/f/a/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/f/a/f;->b:Lcom/google/f/a/h;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/f/a/h;

    invoke-direct {v0}, Lcom/google/f/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/f;->b:Lcom/google/f/a/h;

    :cond_2
    iget-object v0, p0, Lcom/google/f/a/f;->b:Lcom/google/f/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/f/a/f;->a:Lcom/google/f/a/g;

    if-eqz v0, :cond_0

    .line 199
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/f;->a:Lcom/google/f/a/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/f;->b:Lcom/google/f/a/h;

    if-eqz v0, :cond_1

    .line 202
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/f;->b:Lcom/google/f/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 204
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 205
    return-void
.end method
