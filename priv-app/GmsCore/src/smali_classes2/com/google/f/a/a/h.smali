.class public final Lcom/google/f/a/a/h;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/f/a/a/t;

.field public b:[Lcom/google/f/a/a/u;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 45
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 46
    iput-object v1, p0, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    invoke-static {}, Lcom/google/f/a/a/u;->a()[Lcom/google/f/a/a/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    iput-object v1, p0, Lcom/google/f/a/a/h;->c:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/f/a/a/h;->d:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/f/a/a/h;->e:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/f/a/a/h;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/a/h;->cachedSize:I

    .line 47
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 149
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 150
    iget-object v1, p0, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    if-eqz v1, :cond_0

    .line 151
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 154
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 155
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 156
    iget-object v2, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    aget-object v2, v2, v0

    .line 157
    if-eqz v2, :cond_1

    .line 158
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 155
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 163
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/h;->c:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 164
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/f/a/a/h;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 167
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/h;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 168
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/f/a/a/h;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/a/h;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 172
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/f/a/a/h;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 62
    if-ne p1, p0, :cond_1

    .line 63
    const/4 v0, 0x1

    .line 102
    :cond_0
    :goto_0
    return v0

    .line 65
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/a/h;

    if-eqz v1, :cond_0

    .line 68
    check-cast p1, Lcom/google/f/a/a/h;

    .line 69
    iget-object v1, p0, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    if-nez v1, :cond_6

    .line 70
    iget-object v1, p1, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    if-nez v1, :cond_0

    .line 78
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    iget-object v2, p1, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 82
    iget-object v1, p0, Lcom/google/f/a/a/h;->c:Ljava/lang/Long;

    if-nez v1, :cond_7

    .line 83
    iget-object v1, p1, Lcom/google/f/a/a/h;->c:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 89
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/h;->d:Ljava/lang/Integer;

    if-nez v1, :cond_8

    .line 90
    iget-object v1, p1, Lcom/google/f/a/a/h;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 96
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/h;->e:Ljava/lang/Integer;

    if-nez v1, :cond_9

    .line 97
    iget-object v1, p1, Lcom/google/f/a/a/h;->e:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 102
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/f/a/a/h;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 74
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    iget-object v2, p1, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 86
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/a/h;->c:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/f/a/a/h;->c:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 93
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/a/h;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/a/h;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 100
    :cond_9
    iget-object v1, p0, Lcom/google/f/a/a/h;->e:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/a/h;->e:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 107
    iget-object v0, p0, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 110
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 112
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/h;->c:Ljava/lang/Long;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 114
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/h;->d:Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 116
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/a/h;->e:Ljava/lang/Integer;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 117
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/a/h;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 118
    return v0

    .line 107
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    invoke-virtual {v0}, Lcom/google/f/a/a/t;->hashCode()I

    move-result v0

    goto :goto_0

    .line 112
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/h;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_1

    .line 114
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/h;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_2

    .line 116
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/h;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/a/h;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/a/t;

    invoke-direct {v0}, Lcom/google/f/a/a/t;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/f/a/a/u;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/f/a/a/u;

    invoke-direct {v3}, Lcom/google/f/a/a/u;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/f/a/a/u;

    invoke-direct {v3}, Lcom/google/f/a/a/u;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/h;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/h;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/h;->e:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    if-eqz v0, :cond_0

    .line 125
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/a/h;->a:Lcom/google/f/a/a/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 127
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 128
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 129
    iget-object v1, p0, Lcom/google/f/a/a/h;->b:[Lcom/google/f/a/a/u;

    aget-object v1, v1, v0

    .line 130
    if-eqz v1, :cond_1

    .line 131
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 128
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 135
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/h;->c:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 136
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/f/a/a/h;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 138
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/h;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 139
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/f/a/a/h;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 141
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/h;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 142
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/f/a/a/h;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 144
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 145
    return-void
.end method
