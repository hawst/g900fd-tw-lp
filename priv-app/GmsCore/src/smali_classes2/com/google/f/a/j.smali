.class public final Lcom/google/f/a/j;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Double;

.field public b:Ljava/lang/Double;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Lcom/google/o/a/a/a;

.field public g:Ljava/lang/String;

.field public h:Lcom/google/f/a/a;

.field public i:Lcom/google/f/a/k;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 56
    iput-object v0, p0, Lcom/google/f/a/j;->a:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/f/a/j;->b:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/f/a/j;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/f/a/j;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/f/a/j;->e:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/f/a/j;->f:Lcom/google/o/a/a/a;

    iput-object v0, p0, Lcom/google/f/a/j;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/f/a/j;->h:Lcom/google/f/a/a;

    iput-object v0, p0, Lcom/google/f/a/j;->i:Lcom/google/f/a/k;

    iput-object v0, p0, Lcom/google/f/a/j;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/j;->cachedSize:I

    .line 57
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 213
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 214
    iget-object v1, p0, Lcom/google/f/a/j;->a:Ljava/lang/Double;

    if-eqz v1, :cond_0

    .line 215
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/j;->a:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 218
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/j;->b:Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 219
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/f/a/j;->b:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 222
    :cond_1
    iget-object v1, p0, Lcom/google/f/a/j;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 223
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/f/a/j;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 226
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/j;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 227
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/f/a/j;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 230
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/j;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 231
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/f/a/j;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 234
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/j;->f:Lcom/google/o/a/a/a;

    if-eqz v1, :cond_5

    .line 235
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/f/a/j;->f:Lcom/google/o/a/a/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 238
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/j;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 239
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/f/a/j;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 242
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/j;->i:Lcom/google/f/a/k;

    if-eqz v1, :cond_7

    .line 243
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/f/a/j;->i:Lcom/google/f/a/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 246
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/j;->h:Lcom/google/f/a/a;

    if-eqz v1, :cond_8

    .line 247
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/f/a/j;->h:Lcom/google/f/a/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 250
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 76
    if-ne p1, p0, :cond_1

    .line 77
    const/4 v0, 0x1

    .line 151
    :cond_0
    :goto_0
    return v0

    .line 79
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/j;

    if-eqz v1, :cond_0

    .line 82
    check-cast p1, Lcom/google/f/a/j;

    .line 83
    iget-object v1, p0, Lcom/google/f/a/j;->a:Ljava/lang/Double;

    if-nez v1, :cond_b

    .line 84
    iget-object v1, p1, Lcom/google/f/a/j;->a:Ljava/lang/Double;

    if-nez v1, :cond_0

    .line 90
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/j;->b:Ljava/lang/Double;

    if-nez v1, :cond_c

    .line 91
    iget-object v1, p1, Lcom/google/f/a/j;->b:Ljava/lang/Double;

    if-nez v1, :cond_0

    .line 97
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/j;->c:Ljava/lang/String;

    if-nez v1, :cond_d

    .line 98
    iget-object v1, p1, Lcom/google/f/a/j;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 104
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/j;->d:Ljava/lang/Integer;

    if-nez v1, :cond_e

    .line 105
    iget-object v1, p1, Lcom/google/f/a/j;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 111
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/j;->e:Ljava/lang/Integer;

    if-nez v1, :cond_f

    .line 112
    iget-object v1, p1, Lcom/google/f/a/j;->e:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 117
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/j;->f:Lcom/google/o/a/a/a;

    if-nez v1, :cond_10

    .line 118
    iget-object v1, p1, Lcom/google/f/a/j;->f:Lcom/google/o/a/a/a;

    if-nez v1, :cond_0

    .line 126
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/j;->g:Ljava/lang/String;

    if-nez v1, :cond_11

    .line 127
    iget-object v1, p1, Lcom/google/f/a/j;->g:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 133
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/j;->h:Lcom/google/f/a/a;

    if-nez v1, :cond_12

    .line 134
    iget-object v1, p1, Lcom/google/f/a/j;->h:Lcom/google/f/a/a;

    if-nez v1, :cond_0

    .line 142
    :cond_9
    iget-object v1, p0, Lcom/google/f/a/j;->i:Lcom/google/f/a/k;

    if-nez v1, :cond_13

    .line 143
    iget-object v1, p1, Lcom/google/f/a/j;->i:Lcom/google/f/a/k;

    if-nez v1, :cond_0

    .line 151
    :cond_a
    invoke-virtual {p0, p1}, Lcom/google/f/a/j;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 87
    :cond_b
    iget-object v1, p0, Lcom/google/f/a/j;->a:Ljava/lang/Double;

    iget-object v2, p1, Lcom/google/f/a/j;->a:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 94
    :cond_c
    iget-object v1, p0, Lcom/google/f/a/j;->b:Ljava/lang/Double;

    iget-object v2, p1, Lcom/google/f/a/j;->b:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 101
    :cond_d
    iget-object v1, p0, Lcom/google/f/a/j;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/f/a/j;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 108
    :cond_e
    iget-object v1, p0, Lcom/google/f/a/j;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/j;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 115
    :cond_f
    iget-object v1, p0, Lcom/google/f/a/j;->e:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/j;->e:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 122
    :cond_10
    iget-object v1, p0, Lcom/google/f/a/j;->f:Lcom/google/o/a/a/a;

    iget-object v2, p1, Lcom/google/f/a/j;->f:Lcom/google/o/a/a/a;

    invoke-virtual {v1, v2}, Lcom/google/o/a/a/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 130
    :cond_11
    iget-object v1, p0, Lcom/google/f/a/j;->g:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/f/a/j;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 138
    :cond_12
    iget-object v1, p0, Lcom/google/f/a/j;->h:Lcom/google/f/a/a;

    iget-object v2, p1, Lcom/google/f/a/j;->h:Lcom/google/f/a/a;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 147
    :cond_13
    iget-object v1, p0, Lcom/google/f/a/j;->i:Lcom/google/f/a/k;

    iget-object v2, p1, Lcom/google/f/a/j;->i:Lcom/google/f/a/k;

    invoke-virtual {v1, v2}, Lcom/google/f/a/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 156
    iget-object v0, p0, Lcom/google/f/a/j;->a:Ljava/lang/Double;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 159
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/j;->b:Ljava/lang/Double;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 161
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/j;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 163
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/j;->d:Ljava/lang/Integer;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 165
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/j;->e:Ljava/lang/Integer;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 166
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/j;->f:Lcom/google/o/a/a/a;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 168
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/j;->g:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 170
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/j;->h:Lcom/google/f/a/a;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 172
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/j;->i:Lcom/google/f/a/k;

    if-nez v2, :cond_8

    :goto_8
    add-int/2addr v0, v1

    .line 174
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/j;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    return v0

    .line 156
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/j;->a:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto :goto_0

    .line 159
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/j;->b:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto :goto_1

    .line 161
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/j;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 163
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/j;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_3

    .line 165
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/j;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_4

    .line 166
    :cond_5
    iget-object v0, p0, Lcom/google/f/a/j;->f:Lcom/google/o/a/a/a;

    invoke-virtual {v0}, Lcom/google/o/a/a/a;->hashCode()I

    move-result v0

    goto :goto_5

    .line 168
    :cond_6
    iget-object v0, p0, Lcom/google/f/a/j;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 170
    :cond_7
    iget-object v0, p0, Lcom/google/f/a/j;->h:Lcom/google/f/a/a;

    invoke-virtual {v0}, Lcom/google/f/a/a;->hashCode()I

    move-result v0

    goto :goto_7

    .line 172
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/j;->i:Lcom/google/f/a/k;

    invoke-virtual {v1}, Lcom/google/f/a/k;->hashCode()I

    move-result v1

    goto :goto_8
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/j;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/j;->a:Ljava/lang/Double;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/j;->b:Ljava/lang/Double;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/j;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/j;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/j;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/f/a/j;->f:Lcom/google/o/a/a/a;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/o/a/a/a;

    invoke-direct {v0}, Lcom/google/o/a/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/j;->f:Lcom/google/o/a/a/a;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/j;->f:Lcom/google/o/a/a/a;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/j;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/f/a/j;->i:Lcom/google/f/a/k;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/f/a/k;

    invoke-direct {v0}, Lcom/google/f/a/k;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/j;->i:Lcom/google/f/a/k;

    :cond_2
    iget-object v0, p0, Lcom/google/f/a/j;->i:Lcom/google/f/a/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/f/a/j;->h:Lcom/google/f/a/a;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/f/a/a;

    invoke-direct {v0}, Lcom/google/f/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/j;->h:Lcom/google/f/a/a;

    :cond_3
    iget-object v0, p0, Lcom/google/f/a/j;->h:Lcom/google/f/a/a;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x11 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/f/a/j;->a:Ljava/lang/Double;

    if-eqz v0, :cond_0

    .line 182
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/j;->a:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 184
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/j;->b:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 185
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/j;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 187
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/j;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 188
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/f/a/j;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 190
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/j;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 191
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/f/a/j;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 193
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/j;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 194
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/f/a/j;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 196
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/j;->f:Lcom/google/o/a/a/a;

    if-eqz v0, :cond_5

    .line 197
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/f/a/j;->f:Lcom/google/o/a/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 199
    :cond_5
    iget-object v0, p0, Lcom/google/f/a/j;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 200
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/f/a/j;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 202
    :cond_6
    iget-object v0, p0, Lcom/google/f/a/j;->i:Lcom/google/f/a/k;

    if-eqz v0, :cond_7

    .line 203
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/f/a/j;->i:Lcom/google/f/a/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 205
    :cond_7
    iget-object v0, p0, Lcom/google/f/a/j;->h:Lcom/google/f/a/a;

    if-eqz v0, :cond_8

    .line 206
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/f/a/j;->h:Lcom/google/f/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 208
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 209
    return-void
.end method
