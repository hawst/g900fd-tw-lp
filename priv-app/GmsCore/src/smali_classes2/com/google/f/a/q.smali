.class public final Lcom/google/f/a/q;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/f/a/h;

.field public b:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 61
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 62
    iput-object v0, p0, Lcom/google/f/a/q;->a:Lcom/google/f/a/h;

    iput-object v0, p0, Lcom/google/f/a/q;->b:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/f/a/q;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/q;->cachedSize:I

    .line 63
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 126
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 127
    iget-object v1, p0, Lcom/google/f/a/q;->a:Lcom/google/f/a/h;

    if-eqz v1, :cond_0

    .line 128
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/q;->a:Lcom/google/f/a/h;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/q;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 132
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/f/a/q;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 75
    if-ne p1, p0, :cond_1

    .line 76
    const/4 v0, 0x1

    .line 98
    :cond_0
    :goto_0
    return v0

    .line 78
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/q;

    if-eqz v1, :cond_0

    .line 81
    check-cast p1, Lcom/google/f/a/q;

    .line 82
    iget-object v1, p0, Lcom/google/f/a/q;->a:Lcom/google/f/a/h;

    if-nez v1, :cond_4

    .line 83
    iget-object v1, p1, Lcom/google/f/a/q;->a:Lcom/google/f/a/h;

    if-nez v1, :cond_0

    .line 91
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/q;->b:Ljava/lang/Long;

    if-nez v1, :cond_5

    .line 92
    iget-object v1, p1, Lcom/google/f/a/q;->b:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 98
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/f/a/q;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 87
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/q;->a:Lcom/google/f/a/h;

    iget-object v2, p1, Lcom/google/f/a/q;->a:Lcom/google/f/a/h;

    invoke-virtual {v1, v2}, Lcom/google/f/a/h;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 95
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/q;->b:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/f/a/q;->b:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 103
    iget-object v0, p0, Lcom/google/f/a/q;->a:Lcom/google/f/a/h;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 106
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/q;->b:Ljava/lang/Long;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 108
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/q;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 109
    return v0

    .line 103
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/q;->a:Lcom/google/f/a/h;

    invoke-virtual {v0}, Lcom/google/f/a/h;->hashCode()I

    move-result v0

    goto :goto_0

    .line 106
    :cond_1
    iget-object v1, p0, Lcom/google/f/a/q;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 38
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/q;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/f/a/q;->a:Lcom/google/f/a/h;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/h;

    invoke-direct {v0}, Lcom/google/f/a/h;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/q;->a:Lcom/google/f/a/h;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/q;->a:Lcom/google/f/a/h;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/q;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/f/a/q;->a:Lcom/google/f/a/h;

    if-eqz v0, :cond_0

    .line 116
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/q;->a:Lcom/google/f/a/h;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 118
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/q;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 119
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/q;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 121
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 122
    return-void
.end method
