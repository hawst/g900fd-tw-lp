.class public final Lcom/google/f/a/a/i;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:[Lcom/google/f/a/a/j;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Boolean;

.field public e:Lcom/google/f/a/a/q;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 208
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 209
    iput-object v1, p0, Lcom/google/f/a/a/i;->a:Ljava/lang/Boolean;

    invoke-static {}, Lcom/google/f/a/a/j;->a()[Lcom/google/f/a/a/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    iput-object v1, p0, Lcom/google/f/a/a/i;->c:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/f/a/a/i;->d:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/f/a/a/i;->e:Lcom/google/f/a/a/q;

    iput-object v1, p0, Lcom/google/f/a/a/i;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/a/i;->cachedSize:I

    .line 210
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 314
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 315
    iget-object v1, p0, Lcom/google/f/a/a/i;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 316
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/a/i;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 319
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 320
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 321
    iget-object v2, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    aget-object v2, v2, v0

    .line 322
    if-eqz v2, :cond_1

    .line 323
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 320
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 328
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/i;->c:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 329
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/f/a/a/i;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 332
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/i;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_5

    .line 333
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/f/a/a/i;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 336
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/a/i;->e:Lcom/google/f/a/a/q;

    if-eqz v1, :cond_6

    .line 337
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/f/a/a/i;->e:Lcom/google/f/a/a/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 340
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 225
    if-ne p1, p0, :cond_1

    .line 226
    const/4 v0, 0x1

    .line 266
    :cond_0
    :goto_0
    return v0

    .line 228
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/a/i;

    if-eqz v1, :cond_0

    .line 231
    check-cast p1, Lcom/google/f/a/a/i;

    .line 232
    iget-object v1, p0, Lcom/google/f/a/a/i;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_6

    .line 233
    iget-object v1, p1, Lcom/google/f/a/a/i;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 239
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    iget-object v2, p1, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 243
    iget-object v1, p0, Lcom/google/f/a/a/i;->c:Ljava/lang/Long;

    if-nez v1, :cond_7

    .line 244
    iget-object v1, p1, Lcom/google/f/a/a/i;->c:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 250
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/i;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_8

    .line 251
    iget-object v1, p1, Lcom/google/f/a/a/i;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 257
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/i;->e:Lcom/google/f/a/a/q;

    if-nez v1, :cond_9

    .line 258
    iget-object v1, p1, Lcom/google/f/a/a/i;->e:Lcom/google/f/a/a/q;

    if-nez v1, :cond_0

    .line 266
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/f/a/a/i;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 236
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/a/i;->a:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/a/i;->a:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 247
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/a/i;->c:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/f/a/a/i;->c:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 254
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/a/i;->d:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/a/i;->d:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 262
    :cond_9
    iget-object v1, p0, Lcom/google/f/a/a/i;->e:Lcom/google/f/a/a/q;

    iget-object v2, p1, Lcom/google/f/a/a/i;->e:Lcom/google/f/a/a/q;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 271
    iget-object v0, p0, Lcom/google/f/a/a/i;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 274
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 276
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/i;->c:Ljava/lang/Long;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 278
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/i;->d:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 280
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/a/i;->e:Lcom/google/f/a/a/q;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 282
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/a/i;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 283
    return v0

    .line 271
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/i;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0

    .line 276
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/i;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_1

    .line 278
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/i;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_2

    .line 280
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/i;->e:Lcom/google/f/a/a/q;

    invoke-virtual {v1}, Lcom/google/f/a/a/q;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/a/i;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/i;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/f/a/a/j;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/f/a/a/j;

    invoke-direct {v3}, Lcom/google/f/a/a/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/f/a/a/j;

    invoke-direct {v3}, Lcom/google/f/a/a/j;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/i;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/i;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/f/a/a/i;->e:Lcom/google/f/a/a/q;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/f/a/a/q;

    invoke-direct {v0}, Lcom/google/f/a/a/q;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/i;->e:Lcom/google/f/a/a/q;

    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/i;->e:Lcom/google/f/a/a/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 289
    iget-object v0, p0, Lcom/google/f/a/a/i;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 290
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/a/i;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 292
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 293
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 294
    iget-object v1, p0, Lcom/google/f/a/a/i;->b:[Lcom/google/f/a/a/j;

    aget-object v1, v1, v0

    .line 295
    if-eqz v1, :cond_1

    .line 296
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 293
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 300
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/i;->c:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 301
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/f/a/a/i;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 303
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/i;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 304
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/f/a/a/i;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 306
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/i;->e:Lcom/google/f/a/a/q;

    if-eqz v0, :cond_5

    .line 307
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/f/a/a/i;->e:Lcom/google/f/a/a/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 309
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 310
    return-void
.end method
