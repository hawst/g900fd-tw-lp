.class public final Lcom/google/f/a/n;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/f/a/i;

.field public b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 422
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 423
    iput-object v0, p0, Lcom/google/f/a/n;->a:Lcom/google/f/a/i;

    iput-object v0, p0, Lcom/google/f/a/n;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/f/a/n;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/n;->cachedSize:I

    .line 424
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 485
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 486
    iget-object v1, p0, Lcom/google/f/a/n;->a:Lcom/google/f/a/i;

    if-eqz v1, :cond_0

    .line 487
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/n;->a:Lcom/google/f/a/i;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 490
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/n;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 491
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/f/a/n;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 494
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 436
    if-ne p1, p0, :cond_1

    .line 437
    const/4 v0, 0x1

    .line 458
    :cond_0
    :goto_0
    return v0

    .line 439
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/n;

    if-eqz v1, :cond_0

    .line 442
    check-cast p1, Lcom/google/f/a/n;

    .line 443
    iget-object v1, p0, Lcom/google/f/a/n;->a:Lcom/google/f/a/i;

    if-nez v1, :cond_4

    .line 444
    iget-object v1, p1, Lcom/google/f/a/n;->a:Lcom/google/f/a/i;

    if-nez v1, :cond_0

    .line 452
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/n;->b:Ljava/lang/Integer;

    if-nez v1, :cond_5

    .line 453
    iget-object v1, p1, Lcom/google/f/a/n;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 458
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/f/a/n;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 448
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/n;->a:Lcom/google/f/a/i;

    iget-object v2, p1, Lcom/google/f/a/n;->a:Lcom/google/f/a/i;

    invoke-virtual {v1, v2}, Lcom/google/f/a/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 456
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/n;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/n;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 463
    iget-object v0, p0, Lcom/google/f/a/n;->a:Lcom/google/f/a/i;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 466
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/n;->b:Ljava/lang/Integer;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 467
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/n;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 468
    return v0

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/n;->a:Lcom/google/f/a/i;

    invoke-virtual {v0}, Lcom/google/f/a/i;->hashCode()I

    move-result v0

    goto :goto_0

    .line 466
    :cond_1
    iget-object v1, p0, Lcom/google/f/a/n;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 399
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/n;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/f/a/n;->a:Lcom/google/f/a/i;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/i;

    invoke-direct {v0}, Lcom/google/f/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/n;->a:Lcom/google/f/a/i;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/n;->a:Lcom/google/f/a/i;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/n;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 474
    iget-object v0, p0, Lcom/google/f/a/n;->a:Lcom/google/f/a/i;

    if-eqz v0, :cond_0

    .line 475
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/n;->a:Lcom/google/f/a/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 477
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/n;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 478
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/n;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 480
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 481
    return-void
.end method
