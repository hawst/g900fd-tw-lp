.class public final Lcom/google/f/a/a/m;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/f/a/a/t;

.field public b:[Lcom/google/f/a/a/u;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Boolean;

.field public h:Ljava/lang/Boolean;

.field public i:Ljava/lang/Boolean;

.field public j:[Lcom/google/f/a/t;

.field public k:Lcom/google/f/a/a/p;

.field public l:Ljava/lang/Integer;

.field public m:Ljava/lang/String;

.field public n:Lcom/google/f/a/a/a;

.field public o:Lcom/google/f/a/a/o;

.field public p:Lcom/google/f/a/a/g;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 71
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 72
    iput-object v1, p0, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    invoke-static {}, Lcom/google/f/a/a/u;->a()[Lcom/google/f/a/a/u;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    iput-object v1, p0, Lcom/google/f/a/a/m;->c:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/f/a/a/m;->d:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/f/a/a/m;->e:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/f/a/a/m;->f:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/f/a/a/m;->g:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/f/a/a/m;->h:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/f/a/a/m;->i:Ljava/lang/Boolean;

    invoke-static {}, Lcom/google/f/a/t;->a()[Lcom/google/f/a/t;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    iput-object v1, p0, Lcom/google/f/a/a/m;->k:Lcom/google/f/a/a/p;

    iput-object v1, p0, Lcom/google/f/a/a/m;->l:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/f/a/a/m;->m:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    iput-object v1, p0, Lcom/google/f/a/a/m;->o:Lcom/google/f/a/a/o;

    iput-object v1, p0, Lcom/google/f/a/a/m;->p:Lcom/google/f/a/a/g;

    iput-object v1, p0, Lcom/google/f/a/a/m;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/a/m;->cachedSize:I

    .line 73
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 330
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 331
    iget-object v2, p0, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    if-eqz v2, :cond_0

    .line 332
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 335
    :cond_0
    iget-object v2, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 336
    :goto_0
    iget-object v3, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 337
    iget-object v3, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    aget-object v3, v3, v0

    .line 338
    if-eqz v3, :cond_1

    .line 339
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 336
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 344
    :cond_3
    iget-object v2, p0, Lcom/google/f/a/a/m;->c:Ljava/lang/Long;

    if-eqz v2, :cond_4

    .line 345
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/f/a/a/m;->c:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 348
    :cond_4
    iget-object v2, p0, Lcom/google/f/a/a/m;->d:Ljava/lang/Long;

    if-eqz v2, :cond_5

    .line 349
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/f/a/a/m;->d:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x8

    add-int/2addr v0, v2

    .line 352
    :cond_5
    iget-object v2, p0, Lcom/google/f/a/a/m;->g:Ljava/lang/Boolean;

    if-eqz v2, :cond_6

    .line 353
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/f/a/a/m;->g:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 356
    :cond_6
    iget-object v2, p0, Lcom/google/f/a/a/m;->l:Ljava/lang/Integer;

    if-eqz v2, :cond_7

    .line 357
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/f/a/a/m;->l:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 360
    :cond_7
    iget-object v2, p0, Lcom/google/f/a/a/m;->m:Ljava/lang/String;

    if-eqz v2, :cond_8

    .line 361
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/f/a/a/m;->m:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 364
    :cond_8
    iget-object v2, p0, Lcom/google/f/a/a/m;->h:Ljava/lang/Boolean;

    if-eqz v2, :cond_9

    .line 365
    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/f/a/a/m;->h:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 368
    :cond_9
    iget-object v2, p0, Lcom/google/f/a/a/m;->i:Ljava/lang/Boolean;

    if-eqz v2, :cond_a

    .line 369
    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/f/a/a/m;->i:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 372
    :cond_a
    iget-object v2, p0, Lcom/google/f/a/a/m;->o:Lcom/google/f/a/a/o;

    if-eqz v2, :cond_b

    .line 373
    const/16 v2, 0xb

    iget-object v3, p0, Lcom/google/f/a/a/m;->o:Lcom/google/f/a/a/o;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 376
    :cond_b
    iget-object v2, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    array-length v2, v2

    if-lez v2, :cond_d

    .line 377
    :goto_1
    iget-object v2, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    array-length v2, v2

    if-ge v1, v2, :cond_d

    .line 378
    iget-object v2, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    aget-object v2, v2, v1

    .line 379
    if-eqz v2, :cond_c

    .line 380
    const/16 v3, 0xc

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 377
    :cond_c
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 385
    :cond_d
    iget-object v1, p0, Lcom/google/f/a/a/m;->k:Lcom/google/f/a/a/p;

    if-eqz v1, :cond_e

    .line 386
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/f/a/a/m;->k:Lcom/google/f/a/a/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 389
    :cond_e
    iget-object v1, p0, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    if-eqz v1, :cond_f

    .line 390
    const/16 v1, 0xe

    iget-object v2, p0, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 393
    :cond_f
    iget-object v1, p0, Lcom/google/f/a/a/m;->e:Ljava/lang/Long;

    if-eqz v1, :cond_10

    .line 394
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/f/a/a/m;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 397
    :cond_10
    iget-object v1, p0, Lcom/google/f/a/a/m;->f:Ljava/lang/Long;

    if-eqz v1, :cond_11

    .line 398
    const/16 v1, 0x10

    iget-object v2, p0, Lcom/google/f/a/a/m;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 401
    :cond_11
    iget-object v1, p0, Lcom/google/f/a/a/m;->p:Lcom/google/f/a/a/g;

    if-eqz v1, :cond_12

    .line 402
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/f/a/a/m;->p:Lcom/google/f/a/a/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 405
    :cond_12
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 99
    if-ne p1, p0, :cond_1

    .line 100
    const/4 v0, 0x1

    .line 222
    :cond_0
    :goto_0
    return v0

    .line 102
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/a/m;

    if-eqz v1, :cond_0

    .line 105
    check-cast p1, Lcom/google/f/a/a/m;

    .line 106
    iget-object v1, p0, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    if-nez v1, :cond_10

    .line 107
    iget-object v1, p1, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    if-nez v1, :cond_0

    .line 115
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    iget-object v2, p1, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    iget-object v1, p0, Lcom/google/f/a/a/m;->c:Ljava/lang/Long;

    if-nez v1, :cond_11

    .line 120
    iget-object v1, p1, Lcom/google/f/a/a/m;->c:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 126
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/m;->d:Ljava/lang/Long;

    if-nez v1, :cond_12

    .line 127
    iget-object v1, p1, Lcom/google/f/a/a/m;->d:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 133
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/m;->e:Ljava/lang/Long;

    if-nez v1, :cond_13

    .line 134
    iget-object v1, p1, Lcom/google/f/a/a/m;->e:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 140
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/a/m;->f:Ljava/lang/Long;

    if-nez v1, :cond_14

    .line 141
    iget-object v1, p1, Lcom/google/f/a/a/m;->f:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 147
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/a/m;->g:Ljava/lang/Boolean;

    if-nez v1, :cond_15

    .line 148
    iget-object v1, p1, Lcom/google/f/a/a/m;->g:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 154
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/a/m;->h:Ljava/lang/Boolean;

    if-nez v1, :cond_16

    .line 155
    iget-object v1, p1, Lcom/google/f/a/a/m;->h:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 161
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/a/m;->i:Ljava/lang/Boolean;

    if-nez v1, :cond_17

    .line 162
    iget-object v1, p1, Lcom/google/f/a/a/m;->i:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 168
    :cond_9
    iget-object v1, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    iget-object v2, p1, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 172
    iget-object v1, p0, Lcom/google/f/a/a/m;->k:Lcom/google/f/a/a/p;

    if-nez v1, :cond_18

    .line 173
    iget-object v1, p1, Lcom/google/f/a/a/m;->k:Lcom/google/f/a/a/p;

    if-nez v1, :cond_0

    .line 181
    :cond_a
    iget-object v1, p0, Lcom/google/f/a/a/m;->l:Ljava/lang/Integer;

    if-nez v1, :cond_19

    .line 182
    iget-object v1, p1, Lcom/google/f/a/a/m;->l:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 188
    :cond_b
    iget-object v1, p0, Lcom/google/f/a/a/m;->m:Ljava/lang/String;

    if-nez v1, :cond_1a

    .line 189
    iget-object v1, p1, Lcom/google/f/a/a/m;->m:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 195
    :cond_c
    iget-object v1, p0, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    if-nez v1, :cond_1b

    .line 196
    iget-object v1, p1, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    if-nez v1, :cond_0

    .line 204
    :cond_d
    iget-object v1, p0, Lcom/google/f/a/a/m;->o:Lcom/google/f/a/a/o;

    if-nez v1, :cond_1c

    .line 205
    iget-object v1, p1, Lcom/google/f/a/a/m;->o:Lcom/google/f/a/a/o;

    if-nez v1, :cond_0

    .line 213
    :cond_e
    iget-object v1, p0, Lcom/google/f/a/a/m;->p:Lcom/google/f/a/a/g;

    if-nez v1, :cond_1d

    .line 214
    iget-object v1, p1, Lcom/google/f/a/a/m;->p:Lcom/google/f/a/a/g;

    if-nez v1, :cond_0

    .line 222
    :cond_f
    invoke-virtual {p0, p1}, Lcom/google/f/a/a/m;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto/16 :goto_0

    .line 111
    :cond_10
    iget-object v1, p0, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    iget-object v2, p1, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto/16 :goto_0

    .line 123
    :cond_11
    iget-object v1, p0, Lcom/google/f/a/a/m;->c:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/f/a/a/m;->c:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 130
    :cond_12
    iget-object v1, p0, Lcom/google/f/a/a/m;->d:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/f/a/a/m;->d:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 137
    :cond_13
    iget-object v1, p0, Lcom/google/f/a/a/m;->e:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/f/a/a/m;->e:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 144
    :cond_14
    iget-object v1, p0, Lcom/google/f/a/a/m;->f:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/f/a/a/m;->f:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 151
    :cond_15
    iget-object v1, p0, Lcom/google/f/a/a/m;->g:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/a/m;->g:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 158
    :cond_16
    iget-object v1, p0, Lcom/google/f/a/a/m;->h:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/a/m;->h:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 165
    :cond_17
    iget-object v1, p0, Lcom/google/f/a/a/m;->i:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/a/m;->i:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 177
    :cond_18
    iget-object v1, p0, Lcom/google/f/a/a/m;->k:Lcom/google/f/a/a/p;

    iget-object v2, p1, Lcom/google/f/a/a/m;->k:Lcom/google/f/a/a/p;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/p;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 185
    :cond_19
    iget-object v1, p0, Lcom/google/f/a/a/m;->l:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/a/m;->l:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    .line 192
    :cond_1a
    iget-object v1, p0, Lcom/google/f/a/a/m;->m:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/f/a/a/m;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    goto/16 :goto_0

    .line 200
    :cond_1b
    iget-object v1, p0, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    iget-object v2, p1, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    goto/16 :goto_0

    .line 209
    :cond_1c
    iget-object v1, p0, Lcom/google/f/a/a/m;->o:Lcom/google/f/a/a/o;

    iget-object v2, p1, Lcom/google/f/a/a/m;->o:Lcom/google/f/a/a/o;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/o;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    goto/16 :goto_0

    .line 218
    :cond_1d
    iget-object v1, p0, Lcom/google/f/a/a/m;->p:Lcom/google/f/a/a/g;

    iget-object v2, p1, Lcom/google/f/a/a/m;->p:Lcom/google/f/a/a/g;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/g;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_f

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 227
    iget-object v0, p0, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 230
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 232
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/m;->c:Ljava/lang/Long;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 234
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/m;->d:Ljava/lang/Long;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 236
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/m;->e:Ljava/lang/Long;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 238
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/m;->f:Ljava/lang/Long;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 240
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/m;->g:Ljava/lang/Boolean;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 242
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/m;->h:Ljava/lang/Boolean;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 244
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/m;->i:Ljava/lang/Boolean;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 246
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 248
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/m;->k:Lcom/google/f/a/a/p;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 250
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/m;->l:Ljava/lang/Integer;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 252
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/m;->m:Ljava/lang/String;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 254
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    if-nez v0, :cond_b

    move v0, v1

    :goto_b
    add-int/2addr v0, v2

    .line 256
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/m;->o:Lcom/google/f/a/a/o;

    if-nez v0, :cond_c

    move v0, v1

    :goto_c
    add-int/2addr v0, v2

    .line 258
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/a/m;->p:Lcom/google/f/a/a/g;

    if-nez v2, :cond_d

    :goto_d
    add-int/2addr v0, v1

    .line 260
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/a/m;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    return v0

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    invoke-virtual {v0}, Lcom/google/f/a/a/t;->hashCode()I

    move-result v0

    goto/16 :goto_0

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/m;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_1

    .line 234
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/m;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_2

    .line 236
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/m;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_3

    .line 238
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/m;->f:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_4

    .line 240
    :cond_5
    iget-object v0, p0, Lcom/google/f/a/a/m;->g:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_5

    .line 242
    :cond_6
    iget-object v0, p0, Lcom/google/f/a/a/m;->h:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_6

    .line 244
    :cond_7
    iget-object v0, p0, Lcom/google/f/a/a/m;->i:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_7

    .line 248
    :cond_8
    iget-object v0, p0, Lcom/google/f/a/a/m;->k:Lcom/google/f/a/a/p;

    invoke-virtual {v0}, Lcom/google/f/a/a/p;->hashCode()I

    move-result v0

    goto :goto_8

    .line 250
    :cond_9
    iget-object v0, p0, Lcom/google/f/a/a/m;->l:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_9

    .line 252
    :cond_a
    iget-object v0, p0, Lcom/google/f/a/a/m;->m:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_a

    .line 254
    :cond_b
    iget-object v0, p0, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    invoke-virtual {v0}, Lcom/google/f/a/a/a;->hashCode()I

    move-result v0

    goto :goto_b

    .line 256
    :cond_c
    iget-object v0, p0, Lcom/google/f/a/a/m;->o:Lcom/google/f/a/a/o;

    invoke-virtual {v0}, Lcom/google/f/a/a/o;->hashCode()I

    move-result v0

    goto :goto_c

    .line 258
    :cond_d
    iget-object v1, p0, Lcom/google/f/a/a/m;->p:Lcom/google/f/a/a/g;

    invoke-virtual {v1}, Lcom/google/f/a/a/g;->hashCode()I

    move-result v1

    goto :goto_d
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/a/m;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/a/t;

    invoke-direct {v0}, Lcom/google/f/a/a/t;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/f/a/a/u;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/f/a/a/u;

    invoke-direct {v3}, Lcom/google/f/a/a/u;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/f/a/a/u;

    invoke-direct {v3}, Lcom/google/f/a/a/u;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/m;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/m;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/m;->g:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/m;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/m;->m:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/m;->h:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/m;->i:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/f/a/a/m;->o:Lcom/google/f/a/a/o;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/f/a/a/o;

    invoke-direct {v0}, Lcom/google/f/a/a/o;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/m;->o:Lcom/google/f/a/a/o;

    :cond_5
    iget-object v0, p0, Lcom/google/f/a/a/m;->o:Lcom/google/f/a/a/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x62

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/f/a/t;

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Lcom/google/f/a/t;

    invoke-direct {v3}, Lcom/google/f/a/t;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Lcom/google/f/a/t;

    invoke-direct {v3}, Lcom/google/f/a/t;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    goto/16 :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/google/f/a/a/m;->k:Lcom/google/f/a/a/p;

    if-nez v0, :cond_9

    new-instance v0, Lcom/google/f/a/a/p;

    invoke-direct {v0}, Lcom/google/f/a/a/p;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/m;->k:Lcom/google/f/a/a/p;

    :cond_9
    iget-object v0, p0, Lcom/google/f/a/a/m;->k:Lcom/google/f/a/a/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_d
    iget-object v0, p0, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/f/a/a/a;

    invoke-direct {v0}, Lcom/google/f/a/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    :cond_a
    iget-object v0, p0, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/m;->e:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/m;->f:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_10
    iget-object v0, p0, Lcom/google/f/a/a/m;->p:Lcom/google/f/a/a/g;

    if-nez v0, :cond_b

    new-instance v0, Lcom/google/f/a/a/g;

    invoke-direct {v0}, Lcom/google/f/a/a/g;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/m;->p:Lcom/google/f/a/a/g;

    :cond_b
    iget-object v0, p0, Lcom/google/f/a/a/m;->p:Lcom/google/f/a/a/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x19 -> :sswitch_3
        0x21 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
        0x48 -> :sswitch_8
        0x50 -> :sswitch_9
        0x5a -> :sswitch_a
        0x62 -> :sswitch_b
        0x6a -> :sswitch_c
        0x72 -> :sswitch_d
        0x79 -> :sswitch_e
        0x81 -> :sswitch_f
        0x8a -> :sswitch_10
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 267
    iget-object v0, p0, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    if-eqz v0, :cond_0

    .line 268
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/f/a/a/m;->a:Lcom/google/f/a/a/t;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 271
    :goto_0
    iget-object v2, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 272
    iget-object v2, p0, Lcom/google/f/a/a/m;->b:[Lcom/google/f/a/a/u;

    aget-object v2, v2, v0

    .line 273
    if-eqz v2, :cond_1

    .line 274
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 271
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 278
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/m;->c:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 279
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/f/a/a/m;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 281
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/m;->d:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 282
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/f/a/a/m;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 284
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/m;->g:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 285
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/f/a/a/m;->g:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 287
    :cond_5
    iget-object v0, p0, Lcom/google/f/a/a/m;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 288
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/f/a/a/m;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 290
    :cond_6
    iget-object v0, p0, Lcom/google/f/a/a/m;->m:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 291
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/f/a/a/m;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 293
    :cond_7
    iget-object v0, p0, Lcom/google/f/a/a/m;->h:Ljava/lang/Boolean;

    if-eqz v0, :cond_8

    .line 294
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/f/a/a/m;->h:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 296
    :cond_8
    iget-object v0, p0, Lcom/google/f/a/a/m;->i:Ljava/lang/Boolean;

    if-eqz v0, :cond_9

    .line 297
    const/16 v0, 0xa

    iget-object v2, p0, Lcom/google/f/a/a/m;->i:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 299
    :cond_9
    iget-object v0, p0, Lcom/google/f/a/a/m;->o:Lcom/google/f/a/a/o;

    if-eqz v0, :cond_a

    .line 300
    const/16 v0, 0xb

    iget-object v2, p0, Lcom/google/f/a/a/m;->o:Lcom/google/f/a/a/o;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 302
    :cond_a
    iget-object v0, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    array-length v0, v0

    if-lez v0, :cond_c

    .line 303
    :goto_1
    iget-object v0, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    array-length v0, v0

    if-ge v1, v0, :cond_c

    .line 304
    iget-object v0, p0, Lcom/google/f/a/a/m;->j:[Lcom/google/f/a/t;

    aget-object v0, v0, v1

    .line 305
    if-eqz v0, :cond_b

    .line 306
    const/16 v2, 0xc

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 303
    :cond_b
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 310
    :cond_c
    iget-object v0, p0, Lcom/google/f/a/a/m;->k:Lcom/google/f/a/a/p;

    if-eqz v0, :cond_d

    .line 311
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/f/a/a/m;->k:Lcom/google/f/a/a/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 313
    :cond_d
    iget-object v0, p0, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    if-eqz v0, :cond_e

    .line 314
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/f/a/a/m;->n:Lcom/google/f/a/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 316
    :cond_e
    iget-object v0, p0, Lcom/google/f/a/a/m;->e:Ljava/lang/Long;

    if-eqz v0, :cond_f

    .line 317
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/f/a/a/m;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 319
    :cond_f
    iget-object v0, p0, Lcom/google/f/a/a/m;->f:Ljava/lang/Long;

    if-eqz v0, :cond_10

    .line 320
    const/16 v0, 0x10

    iget-object v1, p0, Lcom/google/f/a/a/m;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->c(IJ)V

    .line 322
    :cond_10
    iget-object v0, p0, Lcom/google/f/a/a/m;->p:Lcom/google/f/a/a/g;

    if-eqz v0, :cond_11

    .line 323
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/f/a/a/m;->p:Lcom/google/f/a/a/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 325
    :cond_11
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 326
    return-void
.end method
