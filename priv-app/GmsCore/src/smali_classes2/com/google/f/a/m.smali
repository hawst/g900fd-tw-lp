.class public final Lcom/google/f/a/m;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Lcom/google/f/a/q;

.field public d:Lcom/google/f/a/p;

.field public e:Lcom/google/f/a/n;

.field public f:Lcom/google/f/a/r;

.field public g:Lcom/google/f/a/o;

.field public h:Lcom/google/f/a/s;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1280
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1281
    iput-object v0, p0, Lcom/google/f/a/m;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/f/a/m;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/f/a/m;->c:Lcom/google/f/a/q;

    iput-object v0, p0, Lcom/google/f/a/m;->d:Lcom/google/f/a/p;

    iput-object v0, p0, Lcom/google/f/a/m;->e:Lcom/google/f/a/n;

    iput-object v0, p0, Lcom/google/f/a/m;->f:Lcom/google/f/a/r;

    iput-object v0, p0, Lcom/google/f/a/m;->g:Lcom/google/f/a/o;

    iput-object v0, p0, Lcom/google/f/a/m;->h:Lcom/google/f/a/s;

    iput-object v0, p0, Lcom/google/f/a/m;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/m;->cachedSize:I

    .line 1282
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1431
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1432
    iget-object v1, p0, Lcom/google/f/a/m;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1433
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/m;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1436
    :cond_0
    iget-object v1, p0, Lcom/google/f/a/m;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 1437
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/f/a/m;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1440
    :cond_1
    iget-object v1, p0, Lcom/google/f/a/m;->c:Lcom/google/f/a/q;

    if-eqz v1, :cond_2

    .line 1441
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/f/a/m;->c:Lcom/google/f/a/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1444
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/m;->d:Lcom/google/f/a/p;

    if-eqz v1, :cond_3

    .line 1445
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/f/a/m;->d:Lcom/google/f/a/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1448
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/m;->e:Lcom/google/f/a/n;

    if-eqz v1, :cond_4

    .line 1449
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/f/a/m;->e:Lcom/google/f/a/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1452
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/m;->f:Lcom/google/f/a/r;

    if-eqz v1, :cond_5

    .line 1453
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/f/a/m;->f:Lcom/google/f/a/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1456
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/m;->g:Lcom/google/f/a/o;

    if-eqz v1, :cond_6

    .line 1457
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/f/a/m;->g:Lcom/google/f/a/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1460
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/m;->h:Lcom/google/f/a/s;

    if-eqz v1, :cond_7

    .line 1461
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/f/a/m;->h:Lcom/google/f/a/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1464
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1300
    if-ne p1, p0, :cond_1

    .line 1301
    const/4 v0, 0x1

    .line 1374
    :cond_0
    :goto_0
    return v0

    .line 1303
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/m;

    if-eqz v1, :cond_0

    .line 1306
    check-cast p1, Lcom/google/f/a/m;

    .line 1307
    iget-object v1, p0, Lcom/google/f/a/m;->a:Ljava/lang/Integer;

    if-nez v1, :cond_a

    .line 1308
    iget-object v1, p1, Lcom/google/f/a/m;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 1313
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/m;->b:Ljava/lang/Integer;

    if-nez v1, :cond_b

    .line 1314
    iget-object v1, p1, Lcom/google/f/a/m;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 1320
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/m;->c:Lcom/google/f/a/q;

    if-nez v1, :cond_c

    .line 1321
    iget-object v1, p1, Lcom/google/f/a/m;->c:Lcom/google/f/a/q;

    if-nez v1, :cond_0

    .line 1329
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/m;->d:Lcom/google/f/a/p;

    if-nez v1, :cond_d

    .line 1330
    iget-object v1, p1, Lcom/google/f/a/m;->d:Lcom/google/f/a/p;

    if-nez v1, :cond_0

    .line 1338
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/m;->e:Lcom/google/f/a/n;

    if-nez v1, :cond_e

    .line 1339
    iget-object v1, p1, Lcom/google/f/a/m;->e:Lcom/google/f/a/n;

    if-nez v1, :cond_0

    .line 1347
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/m;->f:Lcom/google/f/a/r;

    if-nez v1, :cond_f

    .line 1348
    iget-object v1, p1, Lcom/google/f/a/m;->f:Lcom/google/f/a/r;

    if-nez v1, :cond_0

    .line 1356
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/m;->g:Lcom/google/f/a/o;

    if-nez v1, :cond_10

    .line 1357
    iget-object v1, p1, Lcom/google/f/a/m;->g:Lcom/google/f/a/o;

    if-nez v1, :cond_0

    .line 1365
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/m;->h:Lcom/google/f/a/s;

    if-nez v1, :cond_11

    .line 1366
    iget-object v1, p1, Lcom/google/f/a/m;->h:Lcom/google/f/a/s;

    if-nez v1, :cond_0

    .line 1374
    :cond_9
    invoke-virtual {p0, p1}, Lcom/google/f/a/m;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1311
    :cond_a
    iget-object v1, p0, Lcom/google/f/a/m;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/m;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1317
    :cond_b
    iget-object v1, p0, Lcom/google/f/a/m;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/m;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 1325
    :cond_c
    iget-object v1, p0, Lcom/google/f/a/m;->c:Lcom/google/f/a/q;

    iget-object v2, p1, Lcom/google/f/a/m;->c:Lcom/google/f/a/q;

    invoke-virtual {v1, v2}, Lcom/google/f/a/q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 1334
    :cond_d
    iget-object v1, p0, Lcom/google/f/a/m;->d:Lcom/google/f/a/p;

    iget-object v2, p1, Lcom/google/f/a/m;->d:Lcom/google/f/a/p;

    invoke-virtual {v1, v2}, Lcom/google/f/a/p;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 1343
    :cond_e
    iget-object v1, p0, Lcom/google/f/a/m;->e:Lcom/google/f/a/n;

    iget-object v2, p1, Lcom/google/f/a/m;->e:Lcom/google/f/a/n;

    invoke-virtual {v1, v2}, Lcom/google/f/a/n;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 1352
    :cond_f
    iget-object v1, p0, Lcom/google/f/a/m;->f:Lcom/google/f/a/r;

    iget-object v2, p1, Lcom/google/f/a/m;->f:Lcom/google/f/a/r;

    invoke-virtual {v1, v2}, Lcom/google/f/a/r;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 1361
    :cond_10
    iget-object v1, p0, Lcom/google/f/a/m;->g:Lcom/google/f/a/o;

    iget-object v2, p1, Lcom/google/f/a/m;->g:Lcom/google/f/a/o;

    invoke-virtual {v1, v2}, Lcom/google/f/a/o;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 1370
    :cond_11
    iget-object v1, p0, Lcom/google/f/a/m;->h:Lcom/google/f/a/s;

    iget-object v2, p1, Lcom/google/f/a/m;->h:Lcom/google/f/a/s;

    invoke-virtual {v1, v2}, Lcom/google/f/a/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1379
    iget-object v0, p0, Lcom/google/f/a/m;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1381
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/m;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1383
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/m;->c:Lcom/google/f/a/q;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1385
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/m;->d:Lcom/google/f/a/p;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 1387
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/m;->e:Lcom/google/f/a/n;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 1389
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/m;->f:Lcom/google/f/a/r;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 1391
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/m;->g:Lcom/google/f/a/o;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 1393
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/m;->h:Lcom/google/f/a/s;

    if-nez v2, :cond_7

    :goto_7
    add-int/2addr v0, v1

    .line 1395
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/m;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1396
    return v0

    .line 1379
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/m;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 1381
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/m;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1383
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/m;->c:Lcom/google/f/a/q;

    invoke-virtual {v0}, Lcom/google/f/a/q;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1385
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/m;->d:Lcom/google/f/a/p;

    invoke-virtual {v0}, Lcom/google/f/a/p;->hashCode()I

    move-result v0

    goto :goto_3

    .line 1387
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/m;->e:Lcom/google/f/a/n;

    invoke-virtual {v0}, Lcom/google/f/a/n;->hashCode()I

    move-result v0

    goto :goto_4

    .line 1389
    :cond_5
    iget-object v0, p0, Lcom/google/f/a/m;->f:Lcom/google/f/a/r;

    invoke-virtual {v0}, Lcom/google/f/a/r;->hashCode()I

    move-result v0

    goto :goto_5

    .line 1391
    :cond_6
    iget-object v0, p0, Lcom/google/f/a/m;->g:Lcom/google/f/a/o;

    invoke-virtual {v0}, Lcom/google/f/a/o;->hashCode()I

    move-result v0

    goto :goto_6

    .line 1393
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/m;->h:Lcom/google/f/a/s;

    invoke-virtual {v1}, Lcom/google/f/a/s;->hashCode()I

    move-result v1

    goto :goto_7
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/m;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/m;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/m;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/f/a/m;->c:Lcom/google/f/a/q;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/f/a/q;

    invoke-direct {v0}, Lcom/google/f/a/q;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/m;->c:Lcom/google/f/a/q;

    :cond_1
    iget-object v0, p0, Lcom/google/f/a/m;->c:Lcom/google/f/a/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/f/a/m;->d:Lcom/google/f/a/p;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/f/a/p;

    invoke-direct {v0}, Lcom/google/f/a/p;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/m;->d:Lcom/google/f/a/p;

    :cond_2
    iget-object v0, p0, Lcom/google/f/a/m;->d:Lcom/google/f/a/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/f/a/m;->e:Lcom/google/f/a/n;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/f/a/n;

    invoke-direct {v0}, Lcom/google/f/a/n;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/m;->e:Lcom/google/f/a/n;

    :cond_3
    iget-object v0, p0, Lcom/google/f/a/m;->e:Lcom/google/f/a/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/f/a/m;->f:Lcom/google/f/a/r;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/f/a/r;

    invoke-direct {v0}, Lcom/google/f/a/r;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/m;->f:Lcom/google/f/a/r;

    :cond_4
    iget-object v0, p0, Lcom/google/f/a/m;->f:Lcom/google/f/a/r;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/f/a/m;->g:Lcom/google/f/a/o;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/f/a/o;

    invoke-direct {v0}, Lcom/google/f/a/o;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/m;->g:Lcom/google/f/a/o;

    :cond_5
    iget-object v0, p0, Lcom/google/f/a/m;->g:Lcom/google/f/a/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/f/a/m;->h:Lcom/google/f/a/s;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/f/a/s;

    invoke-direct {v0}, Lcom/google/f/a/s;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/m;->h:Lcom/google/f/a/s;

    :cond_6
    iget-object v0, p0, Lcom/google/f/a/m;->h:Lcom/google/f/a/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1402
    iget-object v0, p0, Lcom/google/f/a/m;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1403
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/m;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1405
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/m;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1406
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/m;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1408
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/m;->c:Lcom/google/f/a/q;

    if-eqz v0, :cond_2

    .line 1409
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/f/a/m;->c:Lcom/google/f/a/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1411
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/m;->d:Lcom/google/f/a/p;

    if-eqz v0, :cond_3

    .line 1412
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/f/a/m;->d:Lcom/google/f/a/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1414
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/m;->e:Lcom/google/f/a/n;

    if-eqz v0, :cond_4

    .line 1415
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/f/a/m;->e:Lcom/google/f/a/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1417
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/m;->f:Lcom/google/f/a/r;

    if-eqz v0, :cond_5

    .line 1418
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/f/a/m;->f:Lcom/google/f/a/r;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1420
    :cond_5
    iget-object v0, p0, Lcom/google/f/a/m;->g:Lcom/google/f/a/o;

    if-eqz v0, :cond_6

    .line 1421
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/f/a/m;->g:Lcom/google/f/a/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1423
    :cond_6
    iget-object v0, p0, Lcom/google/f/a/m;->h:Lcom/google/f/a/s;

    if-eqz v0, :cond_7

    .line 1424
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/f/a/m;->h:Lcom/google/f/a/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1426
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1427
    return-void
.end method
