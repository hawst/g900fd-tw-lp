.class public final Lcom/google/f/a/a/n;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/f/a/a/r;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/f/a/a/a;

.field public d:Ljava/lang/Long;

.field public e:Lcom/google/f/a/a/q;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 38
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 39
    invoke-static {}, Lcom/google/f/a/a/r;->a()[Lcom/google/f/a/a/r;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    iput-object v1, p0, Lcom/google/f/a/a/n;->b:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    iput-object v1, p0, Lcom/google/f/a/a/n;->d:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/f/a/a/n;->e:Lcom/google/f/a/a/q;

    iput-object v1, p0, Lcom/google/f/a/a/n;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/a/n;->cachedSize:I

    .line 40
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 146
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 147
    iget-object v0, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 148
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 149
    iget-object v2, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    aget-object v2, v2, v0

    .line 150
    if-eqz v2, :cond_0

    .line 151
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 148
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 156
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/n;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 157
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/f/a/a/n;->b:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 160
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/n;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 161
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/f/a/a/n;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v0

    add-int/2addr v1, v0

    .line 164
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    if-eqz v0, :cond_4

    .line 165
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 168
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/n;->e:Lcom/google/f/a/a/q;

    if-eqz v0, :cond_5

    .line 169
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/f/a/a/n;->e:Lcom/google/f/a/a/q;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 172
    :cond_5
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 55
    if-ne p1, p0, :cond_1

    .line 56
    const/4 v0, 0x1

    .line 98
    :cond_0
    :goto_0
    return v0

    .line 58
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/a/n;

    if-eqz v1, :cond_0

    .line 61
    check-cast p1, Lcom/google/f/a/a/n;

    .line 62
    iget-object v1, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    iget-object v2, p1, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 66
    iget-object v1, p0, Lcom/google/f/a/a/n;->b:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 67
    iget-object v1, p1, Lcom/google/f/a/a/n;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 73
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    if-nez v1, :cond_7

    .line 74
    iget-object v1, p1, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    if-nez v1, :cond_0

    .line 82
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/n;->d:Ljava/lang/Long;

    if-nez v1, :cond_8

    .line 83
    iget-object v1, p1, Lcom/google/f/a/a/n;->d:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 89
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/a/n;->e:Lcom/google/f/a/a/q;

    if-nez v1, :cond_9

    .line 90
    iget-object v1, p1, Lcom/google/f/a/a/n;->e:Lcom/google/f/a/a/q;

    if-nez v1, :cond_0

    .line 98
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/f/a/a/n;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 70
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/a/n;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/f/a/a/n;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 78
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    iget-object v2, p1, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 86
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/a/n;->d:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/f/a/a/n;->d:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 94
    :cond_9
    iget-object v1, p0, Lcom/google/f/a/a/n;->e:Lcom/google/f/a/a/q;

    iget-object v2, p1, Lcom/google/f/a/a/n;->e:Lcom/google/f/a/a/q;

    invoke-virtual {v1, v2}, Lcom/google/f/a/a/q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 103
    iget-object v0, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 106
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/n;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 108
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 110
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/a/n;->d:Ljava/lang/Long;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 112
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/a/n;->e:Lcom/google/f/a/a/q;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 114
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/a/n;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 115
    return v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/a/n;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    invoke-virtual {v0}, Lcom/google/f/a/a/a;->hashCode()I

    move-result v0

    goto :goto_1

    .line 110
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/n;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_2

    .line 112
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/a/n;->e:Lcom/google/f/a/a/q;

    invoke-virtual {v1}, Lcom/google/f/a/a/q;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/a/n;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/f/a/a/r;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/f/a/a/r;

    invoke-direct {v3}, Lcom/google/f/a/a/r;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/f/a/a/r;

    invoke-direct {v3}, Lcom/google/f/a/a/r;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/n;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/a/n;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/f/a/a/a;

    invoke-direct {v0}, Lcom/google/f/a/a/a;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/f/a/a/n;->e:Lcom/google/f/a/a/q;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/f/a/a/q;

    invoke-direct {v0}, Lcom/google/f/a/a/q;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/a/n;->e:Lcom/google/f/a/a/q;

    :cond_5
    iget-object v0, p0, Lcom/google/f/a/a/n;->e:Lcom/google/f/a/a/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 122
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 123
    iget-object v1, p0, Lcom/google/f/a/a/n;->a:[Lcom/google/f/a/a/r;

    aget-object v1, v1, v0

    .line 124
    if-eqz v1, :cond_0

    .line 125
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 122
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/a/n;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 130
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/a/n;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 132
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/a/n;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 133
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/f/a/a/n;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 135
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    if-eqz v0, :cond_4

    .line 136
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/f/a/a/n;->c:Lcom/google/f/a/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 138
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/a/n;->e:Lcom/google/f/a/a/q;

    if-eqz v0, :cond_5

    .line 139
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/f/a/a/n;->e:Lcom/google/f/a/a/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 141
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 142
    return-void
.end method
