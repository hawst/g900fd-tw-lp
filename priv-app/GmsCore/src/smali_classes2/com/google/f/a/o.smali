.class public final Lcom/google/f/a/o;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[I

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 775
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 776
    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/f/a/o;->a:[I

    iput-object v1, p0, Lcom/google/f/a/o;->b:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/f/a/o;->c:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/f/a/o;->d:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/f/a/o;->e:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/f/a/o;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/o;->cachedSize:I

    .line 777
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 874
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v2

    .line 875
    iget-object v1, p0, Lcom/google/f/a/o;->a:[I

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/f/a/o;->a:[I

    array-length v1, v1

    if-lez v1, :cond_5

    move v1, v0

    .line 877
    :goto_0
    iget-object v3, p0, Lcom/google/f/a/o;->a:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 878
    iget-object v3, p0, Lcom/google/f/a/o;->a:[I

    aget v3, v3, v0

    .line 879
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 877
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 882
    :cond_0
    add-int v0, v2, v1

    .line 883
    iget-object v1, p0, Lcom/google/f/a/o;->a:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 885
    :goto_1
    iget-object v1, p0, Lcom/google/f/a/o;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 886
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/f/a/o;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 889
    :cond_1
    iget-object v1, p0, Lcom/google/f/a/o;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 890
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/f/a/o;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 893
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/o;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 894
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/f/a/o;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 897
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/o;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 898
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/f/a/o;->e:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 901
    :cond_4
    return v0

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 792
    if-ne p1, p0, :cond_1

    .line 793
    const/4 v0, 0x1

    .line 830
    :cond_0
    :goto_0
    return v0

    .line 795
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/o;

    if-eqz v1, :cond_0

    .line 798
    check-cast p1, Lcom/google/f/a/o;

    .line 799
    iget-object v1, p0, Lcom/google/f/a/o;->a:[I

    iget-object v2, p1, Lcom/google/f/a/o;->a:[I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 803
    iget-object v1, p0, Lcom/google/f/a/o;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_6

    .line 804
    iget-object v1, p1, Lcom/google/f/a/o;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 810
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/o;->c:Ljava/lang/Integer;

    if-nez v1, :cond_7

    .line 811
    iget-object v1, p1, Lcom/google/f/a/o;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 816
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/o;->d:Ljava/lang/Integer;

    if-nez v1, :cond_8

    .line 817
    iget-object v1, p1, Lcom/google/f/a/o;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 823
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/o;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_9

    .line 824
    iget-object v1, p1, Lcom/google/f/a/o;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 830
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/f/a/o;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 807
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/o;->b:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/o;->b:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 814
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/o;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/o;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 820
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/o;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/o;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 827
    :cond_9
    iget-object v1, p0, Lcom/google/f/a/o;->e:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/f/a/o;->e:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 835
    iget-object v0, p0, Lcom/google/f/a/o;->a:[I

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 838
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/o;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 840
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/o;->c:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 841
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/o;->d:Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 843
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/o;->e:Ljava/lang/Boolean;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 845
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/o;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 846
    return v0

    .line 838
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/o;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0

    .line 840
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/o;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    .line 841
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/o;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_2

    .line 843
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/o;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 743
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/o;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/f/a/o;->a:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/f/a/o;->a:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/f/a/o;->a:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/f/a/o;->a:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/f/a/o;->a:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/f/a/o;->a:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/google/f/a/o;->a:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lcom/google/f/a/o;->a:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/o;->b:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/o;->c:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/o;->d:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/o;->e:Ljava/lang/Boolean;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
        0x18 -> :sswitch_4
        0x20 -> :sswitch_5
        0x28 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 852
    iget-object v0, p0, Lcom/google/f/a/o;->a:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/f/a/o;->a:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 853
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/f/a/o;->a:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 854
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/f/a/o;->a:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 853
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 857
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/o;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 858
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/o;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 860
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/o;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 861
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/f/a/o;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 863
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/o;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 864
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/f/a/o;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 866
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/o;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 867
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/f/a/o;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 869
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 870
    return-void
.end method
