.class public final Lcom/google/f/a/l;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:[Ljava/lang/String;

.field public d:Lcom/google/f/a/e;

.field public e:Lcom/google/f/a/c;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 42
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 43
    iput-object v1, p0, Lcom/google/f/a/l;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/f/a/l;->b:Ljava/lang/Integer;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/f/a/l;->d:Lcom/google/f/a/e;

    iput-object v1, p0, Lcom/google/f/a/l;->e:Lcom/google/f/a/c;

    iput-object v1, p0, Lcom/google/f/a/l;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/f/a/l;->cachedSize:I

    .line 44
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 148
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 149
    iget-object v2, p0, Lcom/google/f/a/l;->a:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 150
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/f/a/l;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 153
    :cond_0
    iget-object v2, p0, Lcom/google/f/a/l;->b:Ljava/lang/Integer;

    if-eqz v2, :cond_1

    .line 154
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/f/a/l;->b:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 157
    :cond_1
    iget-object v2, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v1

    move v3, v1

    .line 160
    :goto_0
    iget-object v4, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_3

    .line 161
    iget-object v4, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 162
    if-eqz v4, :cond_2

    .line 163
    add-int/lit8 v3, v3, 0x1

    .line 164
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 160
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 168
    :cond_3
    add-int/2addr v0, v2

    .line 169
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 171
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/l;->d:Lcom/google/f/a/e;

    if-eqz v1, :cond_5

    .line 172
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/f/a/l;->d:Lcom/google/f/a/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    :cond_5
    iget-object v1, p0, Lcom/google/f/a/l;->e:Lcom/google/f/a/c;

    if-eqz v1, :cond_6

    .line 176
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/f/a/l;->e:Lcom/google/f/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 59
    if-ne p1, p0, :cond_1

    .line 60
    const/4 v0, 0x1

    .line 101
    :cond_0
    :goto_0
    return v0

    .line 62
    :cond_1
    instance-of v1, p1, Lcom/google/f/a/l;

    if-eqz v1, :cond_0

    .line 65
    check-cast p1, Lcom/google/f/a/l;

    .line 66
    iget-object v1, p0, Lcom/google/f/a/l;->a:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 67
    iget-object v1, p1, Lcom/google/f/a/l;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 73
    :cond_2
    iget-object v1, p0, Lcom/google/f/a/l;->b:Ljava/lang/Integer;

    if-nez v1, :cond_7

    .line 74
    iget-object v1, p1, Lcom/google/f/a/l;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 79
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, p0, Lcom/google/f/a/l;->d:Lcom/google/f/a/e;

    if-nez v1, :cond_8

    .line 84
    iget-object v1, p1, Lcom/google/f/a/l;->d:Lcom/google/f/a/e;

    if-nez v1, :cond_0

    .line 92
    :cond_4
    iget-object v1, p0, Lcom/google/f/a/l;->e:Lcom/google/f/a/c;

    if-nez v1, :cond_9

    .line 93
    iget-object v1, p1, Lcom/google/f/a/l;->e:Lcom/google/f/a/c;

    if-nez v1, :cond_0

    .line 101
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/f/a/l;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 70
    :cond_6
    iget-object v1, p0, Lcom/google/f/a/l;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/f/a/l;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 77
    :cond_7
    iget-object v1, p0, Lcom/google/f/a/l;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/f/a/l;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 88
    :cond_8
    iget-object v1, p0, Lcom/google/f/a/l;->d:Lcom/google/f/a/e;

    iget-object v2, p1, Lcom/google/f/a/l;->d:Lcom/google/f/a/e;

    invoke-virtual {v1, v2}, Lcom/google/f/a/e;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 97
    :cond_9
    iget-object v1, p0, Lcom/google/f/a/l;->e:Lcom/google/f/a/c;

    iget-object v2, p1, Lcom/google/f/a/l;->e:Lcom/google/f/a/c;

    invoke-virtual {v1, v2}, Lcom/google/f/a/c;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 106
    iget-object v0, p0, Lcom/google/f/a/l;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 109
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/l;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 110
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 112
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/f/a/l;->d:Lcom/google/f/a/e;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 114
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/f/a/l;->e:Lcom/google/f/a/c;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 116
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/f/a/l;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 117
    return v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/l;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 109
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/l;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    .line 112
    :cond_2
    iget-object v0, p0, Lcom/google/f/a/l;->d:Lcom/google/f/a/e;

    invoke-virtual {v0}, Lcom/google/f/a/e;->hashCode()I

    move-result v0

    goto :goto_2

    .line 114
    :cond_3
    iget-object v1, p0, Lcom/google/f/a/l;->e:Lcom/google/f/a/c;

    invoke-virtual {v1}, Lcom/google/f/a/c;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/f/a/l;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/l;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/f/a/l;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/f/a/l;->d:Lcom/google/f/a/e;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/f/a/e;

    invoke-direct {v0}, Lcom/google/f/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/l;->d:Lcom/google/f/a/e;

    :cond_4
    iget-object v0, p0, Lcom/google/f/a/l;->d:Lcom/google/f/a/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/f/a/l;->e:Lcom/google/f/a/c;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/f/a/c;

    invoke-direct {v0}, Lcom/google/f/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/f/a/l;->e:Lcom/google/f/a/c;

    :cond_5
    iget-object v0, p0, Lcom/google/f/a/l;->e:Lcom/google/f/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/f/a/l;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 124
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/f/a/l;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 126
    :cond_0
    iget-object v0, p0, Lcom/google/f/a/l;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 127
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/f/a/l;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 129
    :cond_1
    iget-object v0, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 130
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 131
    iget-object v1, p0, Lcom/google/f/a/l;->c:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 132
    if-eqz v1, :cond_2

    .line 133
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 130
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 137
    :cond_3
    iget-object v0, p0, Lcom/google/f/a/l;->d:Lcom/google/f/a/e;

    if-eqz v0, :cond_4

    .line 138
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/f/a/l;->d:Lcom/google/f/a/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 140
    :cond_4
    iget-object v0, p0, Lcom/google/f/a/l;->e:Lcom/google/f/a/c;

    if-eqz v0, :cond_5

    .line 141
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/f/a/l;->e:Lcom/google/f/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 143
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 144
    return-void
.end method
