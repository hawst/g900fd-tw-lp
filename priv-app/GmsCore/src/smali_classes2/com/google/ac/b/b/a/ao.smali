.class public final Lcom/google/ac/b/b/a/ao;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:[Lcom/google/ac/b/b/a/ai;

.field public c:[Lcom/google/ac/b/b/a/af;

.field public d:[Lcom/google/ac/b/b/a/l;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4861
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 4862
    iput-object v1, p0, Lcom/google/ac/b/b/a/ao;->a:Ljava/lang/Integer;

    invoke-static {}, Lcom/google/ac/b/b/a/ai;->a()[Lcom/google/ac/b/b/a/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    invoke-static {}, Lcom/google/ac/b/b/a/af;->a()[Lcom/google/ac/b/b/a/af;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    invoke-static {}, Lcom/google/ac/b/b/a/l;->a()[Lcom/google/ac/b/b/a/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    iput-object v1, p0, Lcom/google/ac/b/b/a/ao;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/ao;->cachedSize:I

    .line 4863
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 4956
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 4957
    iget-object v2, p0, Lcom/google/ac/b/b/a/ao;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 4958
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/ac/b/b/a/ao;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4961
    :cond_0
    iget-object v2, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 4962
    :goto_0
    iget-object v3, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 4963
    iget-object v3, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    aget-object v3, v3, v0

    .line 4964
    if-eqz v3, :cond_1

    .line 4965
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4962
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 4970
    :cond_3
    iget-object v2, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    .line 4971
    :goto_1
    iget-object v3, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 4972
    iget-object v3, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    aget-object v3, v3, v0

    .line 4973
    if-eqz v3, :cond_4

    .line 4974
    const/4 v4, 0x3

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4971
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v0, v2

    .line 4979
    :cond_6
    iget-object v2, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 4980
    :goto_2
    iget-object v2, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 4981
    iget-object v2, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    aget-object v2, v2, v1

    .line 4982
    if-eqz v2, :cond_7

    .line 4983
    const/4 v3, 0x4

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4980
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4988
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4877
    if-ne p1, p0, :cond_1

    .line 4878
    const/4 v0, 0x1

    .line 4903
    :cond_0
    :goto_0
    return v0

    .line 4880
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/ao;

    if-eqz v1, :cond_0

    .line 4883
    check-cast p1, Lcom/google/ac/b/b/a/ao;

    .line 4884
    iget-object v1, p0, Lcom/google/ac/b/b/a/ao;->a:Ljava/lang/Integer;

    if-nez v1, :cond_3

    .line 4885
    iget-object v1, p1, Lcom/google/ac/b/b/a/ao;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 4891
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4895
    iget-object v1, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4899
    iget-object v1, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4903
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/ao;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 4888
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/ao;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ao;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 4908
    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4911
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4913
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4915
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4917
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/ao;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4918
    return v0

    .line 4908
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4832
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/ao;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/ao;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/b/a/ai;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/b/a/ai;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/ai;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/b/a/ai;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/ai;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/b/a/af;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/ac/b/b/a/af;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/af;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/ac/b/b/a/af;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/af;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/b/a/l;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/ac/b/b/a/l;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/l;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lcom/google/ac/b/b/a/l;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/l;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4924
    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 4925
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/ao;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4927
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 4928
    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 4929
    iget-object v2, p0, Lcom/google/ac/b/b/a/ao;->b:[Lcom/google/ac/b/b/a/ai;

    aget-object v2, v2, v0

    .line 4930
    if-eqz v2, :cond_1

    .line 4931
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4928
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4935
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 4936
    :goto_1
    iget-object v2, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 4937
    iget-object v2, p0, Lcom/google/ac/b/b/a/ao;->c:[Lcom/google/ac/b/b/a/af;

    aget-object v2, v2, v0

    .line 4938
    if-eqz v2, :cond_3

    .line 4939
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4936
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 4943
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 4944
    :goto_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 4945
    iget-object v0, p0, Lcom/google/ac/b/b/a/ao;->d:[Lcom/google/ac/b/b/a/l;

    aget-object v0, v0, v1

    .line 4946
    if-eqz v0, :cond_5

    .line 4947
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4944
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 4951
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4952
    return-void
.end method
