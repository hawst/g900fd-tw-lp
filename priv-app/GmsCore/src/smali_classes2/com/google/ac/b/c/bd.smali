.class public final Lcom/google/ac/b/c/bd;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5625
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 5626
    iput-object v0, p0, Lcom/google/ac/b/c/bd;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/bd;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/c/bd;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/bd;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bd;->cachedSize:I

    .line 5627
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5697
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 5698
    iget-object v1, p0, Lcom/google/ac/b/c/bd;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 5699
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/bd;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5702
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/bd;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 5703
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/bd;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5706
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/bd;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 5707
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/bd;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5710
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 5640
    if-ne p1, p0, :cond_1

    .line 5641
    const/4 v0, 0x1

    .line 5666
    :cond_0
    :goto_0
    return v0

    .line 5643
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bd;

    if-eqz v1, :cond_0

    .line 5646
    check-cast p1, Lcom/google/ac/b/c/bd;

    .line 5647
    iget-object v1, p0, Lcom/google/ac/b/c/bd;->a:Ljava/lang/Integer;

    if-nez v1, :cond_5

    .line 5648
    iget-object v1, p1, Lcom/google/ac/b/c/bd;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 5653
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bd;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_6

    .line 5654
    iget-object v1, p1, Lcom/google/ac/b/c/bd;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 5660
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bd;->c:Ljava/lang/Integer;

    if-nez v1, :cond_7

    .line 5661
    iget-object v1, p1, Lcom/google/ac/b/c/bd;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 5666
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bd;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 5651
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/bd;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/bd;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 5657
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/bd;->b:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/bd;->b:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 5664
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/bd;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/bd;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5671
    iget-object v0, p0, Lcom/google/ac/b/c/bd;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 5673
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bd;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 5675
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/bd;->c:Ljava/lang/Integer;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 5676
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bd;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 5677
    return v0

    .line 5671
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bd;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 5673
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bd;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_1

    .line 5675
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bd;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5599
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bd;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bd;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bd;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bd;->c:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5683
    iget-object v0, p0, Lcom/google/ac/b/c/bd;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 5684
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/bd;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5686
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bd;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 5687
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/bd;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5689
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bd;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 5690
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/bd;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5692
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5693
    return-void
.end method
