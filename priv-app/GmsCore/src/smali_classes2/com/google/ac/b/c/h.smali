.class public final Lcom/google/ac/b/c/h;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 916
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 917
    iput-object v0, p0, Lcom/google/ac/b/c/h;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/c/h;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/c/h;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/h;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/h;->cachedSize:I

    .line 918
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 992
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 993
    iget-object v1, p0, Lcom/google/ac/b/c/h;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 994
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/h;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 997
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/h;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 998
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/h;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1001
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/h;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1002
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/h;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1005
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 931
    if-ne p1, p0, :cond_1

    .line 932
    const/4 v0, 0x1

    .line 959
    :cond_0
    :goto_0
    return v0

    .line 934
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/h;

    if-eqz v1, :cond_0

    .line 937
    check-cast p1, Lcom/google/ac/b/c/h;

    .line 938
    iget-object v1, p0, Lcom/google/ac/b/c/h;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_5

    .line 939
    iget-object v1, p1, Lcom/google/ac/b/c/h;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 945
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/h;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_6

    .line 946
    iget-object v1, p1, Lcom/google/ac/b/c/h;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 952
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/h;->c:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 953
    iget-object v1, p1, Lcom/google/ac/b/c/h;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 959
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/h;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 942
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/h;->a:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/h;->a:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 949
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/h;->b:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/h;->b:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 956
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/h;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/h;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 964
    iget-object v0, p0, Lcom/google/ac/b/c/h;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 967
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/h;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 969
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/h;->c:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 971
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/h;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 972
    return v0

    .line 964
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/h;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0

    .line 967
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/h;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_1

    .line 969
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/h;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 890
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/h;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/h;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/h;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/h;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 978
    iget-object v0, p0, Lcom/google/ac/b/c/h;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 979
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/h;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 981
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/h;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 982
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/h;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 984
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/h;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 985
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/h;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 987
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 988
    return-void
.end method
