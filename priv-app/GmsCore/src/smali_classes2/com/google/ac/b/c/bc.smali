.class public final Lcom/google/ac/b/c/bc;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/ac/b/c/cl;

.field public b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9598
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 9599
    invoke-static {}, Lcom/google/ac/b/c/cl;->a()[Lcom/google/ac/b/c/cl;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    iput-object v1, p0, Lcom/google/ac/b/c/bc;->b:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/ac/b/c/bc;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bc;->cachedSize:I

    .line 9600
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 9661
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 9662
    iget-object v0, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 9663
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 9664
    iget-object v2, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    aget-object v2, v2, v0

    .line 9665
    if-eqz v2, :cond_0

    .line 9666
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 9663
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9671
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bc;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 9672
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/bc;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 9675
    :cond_2
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 9612
    if-ne p1, p0, :cond_1

    .line 9613
    const/4 v0, 0x1

    .line 9629
    :cond_0
    :goto_0
    return v0

    .line 9615
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bc;

    if-eqz v1, :cond_0

    .line 9618
    check-cast p1, Lcom/google/ac/b/c/bc;

    .line 9619
    iget-object v1, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    iget-object v2, p1, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9623
    iget-object v1, p0, Lcom/google/ac/b/c/bc;->b:Ljava/lang/Integer;

    if-nez v1, :cond_3

    .line 9624
    iget-object v1, p1, Lcom/google/ac/b/c/bc;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 9629
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bc;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 9627
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bc;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/bc;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 9634
    iget-object v0, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 9637
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bc;->b:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 9638
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bc;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 9639
    return v0

    .line 9637
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bc;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9575
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bc;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/cl;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/c/cl;

    invoke-direct {v3}, Lcom/google/ac/b/c/cl;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/c/cl;

    invoke-direct {v3}, Lcom/google/ac/b/c/cl;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bc;->b:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 9645
    iget-object v0, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 9646
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 9647
    iget-object v1, p0, Lcom/google/ac/b/c/bc;->a:[Lcom/google/ac/b/c/cl;

    aget-object v1, v1, v0

    .line 9648
    if-eqz v1, :cond_0

    .line 9649
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9646
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9653
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bc;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 9654
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/bc;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 9656
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 9657
    return-void
.end method
