.class public final Lcom/google/ac/b/c/db;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/c/y;

.field public b:Lcom/google/ac/b/c/y;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/String;

.field public e:[Ljava/lang/String;

.field public f:Lcom/google/ac/b/c/ad;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 47
    iput-object v1, p0, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    iput-object v1, p0, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    iput-object v1, p0, Lcom/google/ac/b/c/db;->c:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/ac/b/c/db;->d:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    iput-object v1, p0, Lcom/google/ac/b/c/db;->g:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/ac/b/c/db;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/db;->cachedSize:I

    .line 48
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 182
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 183
    iget-object v2, p0, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    if-eqz v2, :cond_0

    .line 184
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 187
    :cond_0
    iget-object v2, p0, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    if-eqz v2, :cond_1

    .line 188
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 191
    :cond_1
    iget-object v2, p0, Lcom/google/ac/b/c/db;->c:Ljava/lang/Long;

    if-eqz v2, :cond_2

    .line 192
    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/ac/b/c/db;->c:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 195
    :cond_2
    iget-object v2, p0, Lcom/google/ac/b/c/db;->d:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 196
    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/ac/b/c/db;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 199
    :cond_3
    iget-object v2, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v1

    move v3, v1

    .line 202
    :goto_0
    iget-object v4, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_5

    .line 203
    iget-object v4, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 204
    if-eqz v4, :cond_4

    .line 205
    add-int/lit8 v3, v3, 0x1

    .line 206
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 202
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 210
    :cond_5
    add-int/2addr v0, v2

    .line 211
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 213
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    if-eqz v1, :cond_7

    .line 214
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 217
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/db;->g:Ljava/lang/String;

    if-eqz v1, :cond_8

    .line 218
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/ac/b/c/db;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 221
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 65
    if-ne p1, p0, :cond_1

    .line 66
    const/4 v0, 0x1

    .line 124
    :cond_0
    :goto_0
    return v0

    .line 68
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/db;

    if-eqz v1, :cond_0

    .line 71
    check-cast p1, Lcom/google/ac/b/c/db;

    .line 72
    iget-object v1, p0, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    if-nez v1, :cond_8

    .line 73
    iget-object v1, p1, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    if-nez v1, :cond_0

    .line 81
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    if-nez v1, :cond_9

    .line 82
    iget-object v1, p1, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    if-nez v1, :cond_0

    .line 90
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/db;->c:Ljava/lang/Long;

    if-nez v1, :cond_a

    .line 91
    iget-object v1, p1, Lcom/google/ac/b/c/db;->c:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 97
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/db;->d:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 98
    iget-object v1, p1, Lcom/google/ac/b/c/db;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 104
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    if-nez v1, :cond_c

    .line 109
    iget-object v1, p1, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    if-nez v1, :cond_0

    .line 117
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/db;->g:Ljava/lang/String;

    if-nez v1, :cond_d

    .line 118
    iget-object v1, p1, Lcom/google/ac/b/c/db;->g:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 124
    :cond_7
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/db;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 77
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    iget-object v2, p1, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/y;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 86
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    iget-object v2, p1, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/y;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 94
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/db;->c:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/db;->c:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 101
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/db;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/db;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 113
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    iget-object v2, p1, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/ad;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 121
    :cond_d
    iget-object v1, p0, Lcom/google/ac/b/c/db;->g:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/db;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 129
    iget-object v0, p0, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 132
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 134
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/db;->c:Ljava/lang/Long;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 136
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/db;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 138
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 140
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 142
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/db;->g:Ljava/lang/String;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 144
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/db;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 145
    return v0

    .line 129
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    invoke-virtual {v0}, Lcom/google/ac/b/c/y;->hashCode()I

    move-result v0

    goto :goto_0

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    invoke-virtual {v0}, Lcom/google/ac/b/c/y;->hashCode()I

    move-result v0

    goto :goto_1

    .line 134
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/db;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_2

    .line 136
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/db;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 140
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    invoke-virtual {v0}, Lcom/google/ac/b/c/ad;->hashCode()I

    move-result v0

    goto :goto_4

    .line 142
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/db;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/db;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/y;

    invoke-direct {v0}, Lcom/google/ac/b/c/y;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/y;

    invoke-direct {v0}, Lcom/google/ac/b/c/y;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/db;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/db;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x42

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/ac/b/c/ad;

    invoke-direct {v0}, Lcom/google/ac/b/c/ad;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/db;->g:Ljava/lang/String;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x22 -> :sswitch_1
        0x2a -> :sswitch_2
        0x30 -> :sswitch_3
        0x3a -> :sswitch_4
        0x42 -> :sswitch_5
        0x52 -> :sswitch_6
        0x5a -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    if-eqz v0, :cond_0

    .line 152
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/db;->a:Lcom/google/ac/b/c/y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 154
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    if-eqz v0, :cond_1

    .line 155
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/db;->b:Lcom/google/ac/b/c/y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/db;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 158
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/db;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 160
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/db;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 161
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/b/c/db;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 163
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 164
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 165
    iget-object v1, p0, Lcom/google/ac/b/c/db;->e:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 166
    if-eqz v1, :cond_4

    .line 167
    const/16 v2, 0x8

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 164
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 171
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    if-eqz v0, :cond_6

    .line 172
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/ac/b/c/db;->f:Lcom/google/ac/b/c/ad;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 174
    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/db;->g:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 175
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/ac/b/c/db;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 177
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 178
    return-void
.end method
