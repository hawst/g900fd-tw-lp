.class public final Lcom/google/ac/b/b/a/r;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/ac/b/b/a/r;


# instance fields
.field public a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4466
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 4467
    iput-object v0, p0, Lcom/google/ac/b/b/a/r;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/b/a/r;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/r;->cachedSize:I

    .line 4468
    return-void
.end method

.method public static a()[Lcom/google/ac/b/b/a/r;
    .locals 2

    .prologue
    .line 4452
    sget-object v0, Lcom/google/ac/b/b/a/r;->b:[Lcom/google/ac/b/b/a/r;

    if-nez v0, :cond_1

    .line 4453
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 4455
    :try_start_0
    sget-object v0, Lcom/google/ac/b/b/a/r;->b:[Lcom/google/ac/b/b/a/r;

    if-nez v0, :cond_0

    .line 4456
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/b/a/r;

    sput-object v0, Lcom/google/ac/b/b/a/r;->b:[Lcom/google/ac/b/b/a/r;

    .line 4458
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4460
    :cond_1
    sget-object v0, Lcom/google/ac/b/b/a/r;->b:[Lcom/google/ac/b/b/a/r;

    return-object v0

    .line 4458
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 4516
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 4517
    iget-object v1, p0, Lcom/google/ac/b/b/a/r;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4518
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/r;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4521
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4479
    if-ne p1, p0, :cond_1

    .line 4480
    const/4 v0, 0x1

    .line 4493
    :cond_0
    :goto_0
    return v0

    .line 4482
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/r;

    if-eqz v1, :cond_0

    .line 4485
    check-cast p1, Lcom/google/ac/b/b/a/r;

    .line 4486
    iget-object v1, p0, Lcom/google/ac/b/b/a/r;->a:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 4487
    iget-object v1, p1, Lcom/google/ac/b/b/a/r;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4493
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/r;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 4490
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/r;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/b/a/r;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 4498
    iget-object v0, p0, Lcom/google/ac/b/b/a/r;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4501
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/r;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4502
    return v0

    .line 4498
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/r;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4446
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/r;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/r;->a:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4508
    iget-object v0, p0, Lcom/google/ac/b/b/a/r;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 4509
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/r;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4511
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4512
    return-void
.end method
