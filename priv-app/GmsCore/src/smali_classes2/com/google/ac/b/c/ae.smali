.class public final Lcom/google/ac/b/c/ae;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/ac/b/c/cj;

.field public c:Lcom/google/ac/b/c/cl;

.field public d:Lcom/google/ac/b/c/x;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1111
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1112
    iput-object v0, p0, Lcom/google/ac/b/c/ae;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/ae;->b:Lcom/google/ac/b/c/cj;

    iput-object v0, p0, Lcom/google/ac/b/c/ae;->c:Lcom/google/ac/b/c/cl;

    iput-object v0, p0, Lcom/google/ac/b/c/ae;->d:Lcom/google/ac/b/c/x;

    iput-object v0, p0, Lcom/google/ac/b/c/ae;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/ae;->cachedSize:I

    .line 1113
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1206
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1207
    iget-object v1, p0, Lcom/google/ac/b/c/ae;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 1208
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/ae;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1211
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/ae;->b:Lcom/google/ac/b/c/cj;

    if-eqz v1, :cond_1

    .line 1212
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/ae;->b:Lcom/google/ac/b/c/cj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1215
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/ae;->c:Lcom/google/ac/b/c/cl;

    if-eqz v1, :cond_2

    .line 1216
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/ae;->c:Lcom/google/ac/b/c/cl;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1219
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ae;->d:Lcom/google/ac/b/c/x;

    if-eqz v1, :cond_3

    .line 1220
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/ae;->d:Lcom/google/ac/b/c/x;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1223
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1127
    if-ne p1, p0, :cond_1

    .line 1128
    const/4 v0, 0x1

    .line 1168
    :cond_0
    :goto_0
    return v0

    .line 1130
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/ae;

    if-eqz v1, :cond_0

    .line 1133
    check-cast p1, Lcom/google/ac/b/c/ae;

    .line 1134
    iget-object v1, p0, Lcom/google/ac/b/c/ae;->a:Ljava/lang/Integer;

    if-nez v1, :cond_6

    .line 1135
    iget-object v1, p1, Lcom/google/ac/b/c/ae;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 1141
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ae;->b:Lcom/google/ac/b/c/cj;

    if-nez v1, :cond_7

    .line 1142
    iget-object v1, p1, Lcom/google/ac/b/c/ae;->b:Lcom/google/ac/b/c/cj;

    if-nez v1, :cond_0

    .line 1150
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ae;->c:Lcom/google/ac/b/c/cl;

    if-nez v1, :cond_8

    .line 1151
    iget-object v1, p1, Lcom/google/ac/b/c/ae;->c:Lcom/google/ac/b/c/cl;

    if-nez v1, :cond_0

    .line 1159
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/ae;->d:Lcom/google/ac/b/c/x;

    if-nez v1, :cond_9

    .line 1160
    iget-object v1, p1, Lcom/google/ac/b/c/ae;->d:Lcom/google/ac/b/c/x;

    if-nez v1, :cond_0

    .line 1168
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/ae;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1138
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/ae;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/ae;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1146
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/ae;->b:Lcom/google/ac/b/c/cj;

    iget-object v2, p1, Lcom/google/ac/b/c/ae;->b:Lcom/google/ac/b/c/cj;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/cj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 1155
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/ae;->c:Lcom/google/ac/b/c/cl;

    iget-object v2, p1, Lcom/google/ac/b/c/ae;->c:Lcom/google/ac/b/c/cl;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/cl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 1164
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/ae;->d:Lcom/google/ac/b/c/x;

    iget-object v2, p1, Lcom/google/ac/b/c/ae;->d:Lcom/google/ac/b/c/x;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/x;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1173
    iget-object v0, p0, Lcom/google/ac/b/c/ae;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1176
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ae;->b:Lcom/google/ac/b/c/cj;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1178
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ae;->c:Lcom/google/ac/b/c/cl;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1180
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/ae;->d:Lcom/google/ac/b/c/x;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 1182
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/ae;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1183
    return v0

    .line 1173
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ae;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1176
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ae;->b:Lcom/google/ac/b/c/cj;

    invoke-virtual {v0}, Lcom/google/ac/b/c/cj;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1178
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ae;->c:Lcom/google/ac/b/c/cl;

    invoke-virtual {v0}, Lcom/google/ac/b/c/cl;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1180
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ae;->d:Lcom/google/ac/b/c/x;

    invoke-virtual {v1}, Lcom/google/ac/b/c/x;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1082
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/ae;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ae;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/ae;->b:Lcom/google/ac/b/c/cj;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/cj;

    invoke-direct {v0}, Lcom/google/ac/b/c/cj;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/ae;->b:Lcom/google/ac/b/c/cj;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ae;->b:Lcom/google/ac/b/c/cj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/ae;->c:Lcom/google/ac/b/c/cl;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/cl;

    invoke-direct {v0}, Lcom/google/ac/b/c/cl;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/ae;->c:Lcom/google/ac/b/c/cl;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ae;->c:Lcom/google/ac/b/c/cl;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/ae;->d:Lcom/google/ac/b/c/x;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/b/c/x;

    invoke-direct {v0}, Lcom/google/ac/b/c/x;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/ae;->d:Lcom/google/ac/b/c/x;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/ae;->d:Lcom/google/ac/b/c/x;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1189
    iget-object v0, p0, Lcom/google/ac/b/c/ae;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 1190
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/ae;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1192
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ae;->b:Lcom/google/ac/b/c/cj;

    if-eqz v0, :cond_1

    .line 1193
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/ae;->b:Lcom/google/ac/b/c/cj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1195
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ae;->c:Lcom/google/ac/b/c/cl;

    if-eqz v0, :cond_2

    .line 1196
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/ae;->c:Lcom/google/ac/b/c/cl;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1198
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ae;->d:Lcom/google/ac/b/c/x;

    if-eqz v0, :cond_3

    .line 1199
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/ae;->d:Lcom/google/ac/b/c/x;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1201
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1202
    return-void
.end method
