.class public final Lcom/google/ac/b/c/be;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/c/bd;

.field public b:[Ljava/lang/String;

.field public c:Ljava/lang/Boolean;

.field public d:Lcom/google/ac/b/c/af;

.field public e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5803
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 5804
    iput-object v1, p0, Lcom/google/ac/b/c/be;->a:Lcom/google/ac/b/c/bd;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/ac/b/c/be;->c:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/ac/b/c/be;->d:Lcom/google/ac/b/c/af;

    iput-object v1, p0, Lcom/google/ac/b/c/be;->e:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/ac/b/c/be;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/be;->cachedSize:I

    .line 5805
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 5911
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 5912
    iget-object v2, p0, Lcom/google/ac/b/c/be;->a:Lcom/google/ac/b/c/bd;

    if-eqz v2, :cond_0

    .line 5913
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/ac/b/c/be;->a:Lcom/google/ac/b/c/bd;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5916
    :cond_0
    iget-object v2, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    move v3, v1

    .line 5919
    :goto_0
    iget-object v4, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 5920
    iget-object v4, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 5921
    if-eqz v4, :cond_1

    .line 5922
    add-int/lit8 v3, v3, 0x1

    .line 5923
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 5919
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 5927
    :cond_2
    add-int/2addr v0, v2

    .line 5928
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 5930
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/be;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 5931
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/be;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5934
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/be;->d:Lcom/google/ac/b/c/af;

    if-eqz v1, :cond_5

    .line 5935
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/be;->d:Lcom/google/ac/b/c/af;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5938
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/be;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_6

    .line 5939
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/be;->e:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 5942
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 5820
    if-ne p1, p0, :cond_1

    .line 5821
    const/4 v0, 0x1

    .line 5863
    :cond_0
    :goto_0
    return v0

    .line 5823
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/be;

    if-eqz v1, :cond_0

    .line 5826
    check-cast p1, Lcom/google/ac/b/c/be;

    .line 5827
    iget-object v1, p0, Lcom/google/ac/b/c/be;->a:Lcom/google/ac/b/c/bd;

    if-nez v1, :cond_6

    .line 5828
    iget-object v1, p1, Lcom/google/ac/b/c/be;->a:Lcom/google/ac/b/c/bd;

    if-nez v1, :cond_0

    .line 5836
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5840
    iget-object v1, p0, Lcom/google/ac/b/c/be;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_7

    .line 5841
    iget-object v1, p1, Lcom/google/ac/b/c/be;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 5847
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/be;->d:Lcom/google/ac/b/c/af;

    if-nez v1, :cond_8

    .line 5848
    iget-object v1, p1, Lcom/google/ac/b/c/be;->d:Lcom/google/ac/b/c/af;

    if-nez v1, :cond_0

    .line 5856
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/be;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_9

    .line 5857
    iget-object v1, p1, Lcom/google/ac/b/c/be;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 5863
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/be;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 5832
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/be;->a:Lcom/google/ac/b/c/bd;

    iget-object v2, p1, Lcom/google/ac/b/c/be;->a:Lcom/google/ac/b/c/bd;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bd;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 5844
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/be;->c:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/be;->c:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 5852
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/be;->d:Lcom/google/ac/b/c/af;

    iget-object v2, p1, Lcom/google/ac/b/c/be;->d:Lcom/google/ac/b/c/af;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/af;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 5860
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/be;->e:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/be;->e:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5868
    iget-object v0, p0, Lcom/google/ac/b/c/be;->a:Lcom/google/ac/b/c/bd;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 5871
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 5873
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/be;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 5875
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/be;->d:Lcom/google/ac/b/c/af;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 5877
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/be;->e:Ljava/lang/Boolean;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 5879
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/be;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 5880
    return v0

    .line 5868
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/be;->a:Lcom/google/ac/b/c/bd;

    invoke-virtual {v0}, Lcom/google/ac/b/c/bd;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5873
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/be;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_1

    .line 5875
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/be;->d:Lcom/google/ac/b/c/af;

    invoke-virtual {v0}, Lcom/google/ac/b/c/af;->hashCode()I

    move-result v0

    goto :goto_2

    .line 5877
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/be;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5771
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/be;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/be;->a:Lcom/google/ac/b/c/bd;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/bd;

    invoke-direct {v0}, Lcom/google/ac/b/c/bd;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/be;->a:Lcom/google/ac/b/c/bd;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/be;->a:Lcom/google/ac/b/c/bd;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/be;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/be;->d:Lcom/google/ac/b/c/af;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/ac/b/c/af;

    invoke-direct {v0}, Lcom/google/ac/b/c/af;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/be;->d:Lcom/google/ac/b/c/af;

    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/be;->d:Lcom/google/ac/b/c/af;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/be;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 5886
    iget-object v0, p0, Lcom/google/ac/b/c/be;->a:Lcom/google/ac/b/c/bd;

    if-eqz v0, :cond_0

    .line 5887
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/be;->a:Lcom/google/ac/b/c/bd;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5889
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 5890
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 5891
    iget-object v1, p0, Lcom/google/ac/b/c/be;->b:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 5892
    if-eqz v1, :cond_1

    .line 5893
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5890
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5897
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/be;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 5898
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/be;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5900
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/be;->d:Lcom/google/ac/b/c/af;

    if-eqz v0, :cond_4

    .line 5901
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/be;->d:Lcom/google/ac/b/c/af;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5903
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/be;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_5

    .line 5904
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/be;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 5906
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5907
    return-void
.end method
