.class public final Lcom/google/ac/b/c/v;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[B

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 973
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 974
    iput-object v0, p0, Lcom/google/ac/b/c/v;->a:[B

    iput-object v0, p0, Lcom/google/ac/b/c/v;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/v;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/v;->cachedSize:I

    .line 975
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1031
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1032
    iget-object v1, p0, Lcom/google/ac/b/c/v;->a:[B

    if-eqz v1, :cond_0

    .line 1033
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/v;->a:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 1036
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/v;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1037
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/v;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1040
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 987
    if-ne p1, p0, :cond_1

    .line 988
    const/4 v0, 0x1

    .line 1004
    :cond_0
    :goto_0
    return v0

    .line 990
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/v;

    if-eqz v1, :cond_0

    .line 993
    check-cast p1, Lcom/google/ac/b/c/v;

    .line 994
    iget-object v1, p0, Lcom/google/ac/b/c/v;->a:[B

    iget-object v2, p1, Lcom/google/ac/b/c/v;->a:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 997
    iget-object v1, p0, Lcom/google/ac/b/c/v;->b:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 998
    iget-object v1, p1, Lcom/google/ac/b/c/v;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1004
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/v;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1001
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/v;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/v;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1009
    iget-object v0, p0, Lcom/google/ac/b/c/v;->a:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1011
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/v;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 1013
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/v;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1014
    return v0

    .line 1011
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/v;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 950
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/v;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/v;->a:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/v;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1020
    iget-object v0, p0, Lcom/google/ac/b/c/v;->a:[B

    if-eqz v0, :cond_0

    .line 1021
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/v;->a:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 1023
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/v;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1024
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/v;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1026
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1027
    return-void
.end method
