.class public final Lcom/google/ac/b/c/bq;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/ac/b/c/bq;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;

.field public d:Lcom/google/ac/b/c/br;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3418
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3419
    iput-object v0, p0, Lcom/google/ac/b/c/bq;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/bq;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/bq;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/bq;->d:Lcom/google/ac/b/c/br;

    iput-object v0, p0, Lcom/google/ac/b/c/bq;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bq;->cachedSize:I

    .line 3420
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/bq;
    .locals 2

    .prologue
    .line 3395
    sget-object v0, Lcom/google/ac/b/c/bq;->e:[Lcom/google/ac/b/c/bq;

    if-nez v0, :cond_1

    .line 3396
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 3398
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/bq;->e:[Lcom/google/ac/b/c/bq;

    if-nez v0, :cond_0

    .line 3399
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/bq;

    sput-object v0, Lcom/google/ac/b/c/bq;->e:[Lcom/google/ac/b/c/bq;

    .line 3401
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3403
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/bq;->e:[Lcom/google/ac/b/c/bq;

    return-object v0

    .line 3401
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3507
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3508
    iget-object v1, p0, Lcom/google/ac/b/c/bq;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3509
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/bq;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3512
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/bq;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 3513
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/bq;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3516
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/bq;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 3517
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/bq;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3520
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bq;->d:Lcom/google/ac/b/c/br;

    if-eqz v1, :cond_3

    .line 3521
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/bq;->d:Lcom/google/ac/b/c/br;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3524
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3434
    if-ne p1, p0, :cond_1

    .line 3435
    const/4 v0, 0x1

    .line 3470
    :cond_0
    :goto_0
    return v0

    .line 3437
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bq;

    if-eqz v1, :cond_0

    .line 3440
    check-cast p1, Lcom/google/ac/b/c/bq;

    .line 3441
    iget-object v1, p0, Lcom/google/ac/b/c/bq;->a:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 3442
    iget-object v1, p1, Lcom/google/ac/b/c/bq;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3448
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bq;->b:Ljava/lang/Integer;

    if-nez v1, :cond_7

    .line 3449
    iget-object v1, p1, Lcom/google/ac/b/c/bq;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3455
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bq;->c:Ljava/lang/Integer;

    if-nez v1, :cond_8

    .line 3456
    iget-object v1, p1, Lcom/google/ac/b/c/bq;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3461
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/bq;->d:Lcom/google/ac/b/c/br;

    if-nez v1, :cond_9

    .line 3462
    iget-object v1, p1, Lcom/google/ac/b/c/bq;->d:Lcom/google/ac/b/c/br;

    if-nez v1, :cond_0

    .line 3470
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bq;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 3445
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/bq;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/bq;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 3452
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/bq;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/bq;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 3459
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/bq;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/bq;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 3466
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/bq;->d:Lcom/google/ac/b/c/br;

    iget-object v2, p1, Lcom/google/ac/b/c/bq;->d:Lcom/google/ac/b/c/br;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/br;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3475
    iget-object v0, p0, Lcom/google/ac/b/c/bq;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3478
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bq;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3480
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bq;->c:Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3481
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/bq;->d:Lcom/google/ac/b/c/br;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 3483
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bq;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3484
    return v0

    .line 3475
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bq;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3478
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bq;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3480
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bq;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_2

    .line 3481
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bq;->d:Lcom/google/ac/b/c/br;

    invoke-virtual {v1}, Lcom/google/ac/b/c/br;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3251
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bq;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bq;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bq;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bq;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/bq;->d:Lcom/google/ac/b/c/br;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/br;

    invoke-direct {v0}, Lcom/google/ac/b/c/br;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bq;->d:Lcom/google/ac/b/c/br;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bq;->d:Lcom/google/ac/b/c/br;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3490
    iget-object v0, p0, Lcom/google/ac/b/c/bq;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3491
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/bq;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3493
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bq;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3494
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/bq;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 3496
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bq;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 3497
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/bq;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3499
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bq;->d:Lcom/google/ac/b/c/br;

    if-eqz v0, :cond_3

    .line 3500
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/bq;->d:Lcom/google/ac/b/c/br;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3502
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3503
    return-void
.end method
