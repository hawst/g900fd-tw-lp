.class public final Lcom/google/ac/b/c/c;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Double;

.field public d:Ljava/lang/Integer;

.field public e:Ljava/lang/Integer;

.field public f:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3694
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3695
    iput-object v0, p0, Lcom/google/ac/b/c/c;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/c/c;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/c/c;->c:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/ac/b/c/c;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/c;->e:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/c;->f:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/c;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/c;->cachedSize:I

    .line 3696
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3809
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3810
    iget-object v1, p0, Lcom/google/ac/b/c/c;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 3811
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3814
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/c;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 3815
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/c;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3818
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/c;->c:Ljava/lang/Double;

    if-eqz v1, :cond_2

    .line 3819
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/c;->c:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 3822
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/c;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 3823
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/c;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3826
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/c;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 3827
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/c;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3830
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/c;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 3831
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/c;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3834
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3712
    if-ne p1, p0, :cond_1

    .line 3713
    const/4 v0, 0x1

    .line 3761
    :cond_0
    :goto_0
    return v0

    .line 3715
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/c;

    if-eqz v1, :cond_0

    .line 3718
    check-cast p1, Lcom/google/ac/b/c/c;

    .line 3719
    iget-object v1, p0, Lcom/google/ac/b/c/c;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_8

    .line 3720
    iget-object v1, p1, Lcom/google/ac/b/c/c;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 3726
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/c;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_9

    .line 3727
    iget-object v1, p1, Lcom/google/ac/b/c/c;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 3733
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/c;->c:Ljava/lang/Double;

    if-nez v1, :cond_a

    .line 3734
    iget-object v1, p1, Lcom/google/ac/b/c/c;->c:Ljava/lang/Double;

    if-nez v1, :cond_0

    .line 3740
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/c;->d:Ljava/lang/Integer;

    if-nez v1, :cond_b

    .line 3741
    iget-object v1, p1, Lcom/google/ac/b/c/c;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3747
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/c;->e:Ljava/lang/Integer;

    if-nez v1, :cond_c

    .line 3748
    iget-object v1, p1, Lcom/google/ac/b/c/c;->e:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3754
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/c;->f:Ljava/lang/Integer;

    if-nez v1, :cond_d

    .line 3755
    iget-object v1, p1, Lcom/google/ac/b/c/c;->f:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3761
    :cond_7
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/c;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 3723
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/c;->a:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 3730
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/c;->b:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/c;->b:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 3737
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/c;->c:Ljava/lang/Double;

    iget-object v2, p1, Lcom/google/ac/b/c/c;->c:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 3744
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/c;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/c;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 3751
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/c/c;->e:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/c;->e:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 3758
    :cond_d
    iget-object v1, p0, Lcom/google/ac/b/c/c;->f:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/c;->f:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3766
    iget-object v0, p0, Lcom/google/ac/b/c/c;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3769
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/c;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3771
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/c;->c:Ljava/lang/Double;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3773
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/c;->d:Ljava/lang/Integer;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 3775
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/c;->e:Ljava/lang/Integer;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 3777
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/c;->f:Ljava/lang/Integer;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 3779
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/c;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3780
    return v0

    .line 3766
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3769
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/c;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3771
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/c;->c:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto :goto_2

    .line 3773
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/c;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_3

    .line 3775
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/c;->e:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_4

    .line 3777
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/c;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 3659
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/c;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/c;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/c;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/c;->c:Ljava/lang/Double;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/c;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/c;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/c;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x19 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 3786
    iget-object v0, p0, Lcom/google/ac/b/c/c;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 3787
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/c;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3789
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/c;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 3790
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/c;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3792
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/c;->c:Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 3793
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/c;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 3795
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/c;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 3796
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/c;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3798
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/c;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 3799
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/c;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3801
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/c;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 3802
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/c;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3804
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3805
    return-void
.end method
