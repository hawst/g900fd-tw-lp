.class public final Lcom/google/ac/b/c/ch;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 873
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 874
    iput-object v0, p0, Lcom/google/ac/b/c/ch;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ch;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ch;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ch;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/ch;->cachedSize:I

    .line 875
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 949
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 950
    iget-object v1, p0, Lcom/google/ac/b/c/ch;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 951
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/ch;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 954
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/ch;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 955
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/ch;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 958
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/ch;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 959
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/ch;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 962
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 888
    if-ne p1, p0, :cond_1

    .line 889
    const/4 v0, 0x1

    .line 916
    :cond_0
    :goto_0
    return v0

    .line 891
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/ch;

    if-eqz v1, :cond_0

    .line 894
    check-cast p1, Lcom/google/ac/b/c/ch;

    .line 895
    iget-object v1, p0, Lcom/google/ac/b/c/ch;->a:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 896
    iget-object v1, p1, Lcom/google/ac/b/c/ch;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 902
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ch;->b:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 903
    iget-object v1, p1, Lcom/google/ac/b/c/ch;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 909
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ch;->c:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 910
    iget-object v1, p1, Lcom/google/ac/b/c/ch;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 916
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/ch;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 899
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/ch;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ch;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 906
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/ch;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ch;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 913
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/ch;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ch;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 921
    iget-object v0, p0, Lcom/google/ac/b/c/ch;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 924
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ch;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 926
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/ch;->c:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 928
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/ch;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 929
    return v0

    .line 921
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ch;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 924
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ch;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 926
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ch;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 847
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/ch;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ch;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ch;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ch;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 935
    iget-object v0, p0, Lcom/google/ac/b/c/ch;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 936
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/ch;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 938
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ch;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 939
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/ch;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 941
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ch;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 942
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/ch;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 944
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 945
    return-void
.end method
