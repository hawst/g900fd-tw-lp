.class public final Lcom/google/ac/b/b/a/l;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/ac/b/b/a/l;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/ac/b/b/a/aj;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5425
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 5426
    iput-object v0, p0, Lcom/google/ac/b/b/a/l;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    iput-object v0, p0, Lcom/google/ac/b/b/a/l;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/b/a/l;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/b/a/l;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/l;->cachedSize:I

    .line 5427
    return-void
.end method

.method public static a()[Lcom/google/ac/b/b/a/l;
    .locals 2

    .prologue
    .line 5402
    sget-object v0, Lcom/google/ac/b/b/a/l;->e:[Lcom/google/ac/b/b/a/l;

    if-nez v0, :cond_1

    .line 5403
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 5405
    :try_start_0
    sget-object v0, Lcom/google/ac/b/b/a/l;->e:[Lcom/google/ac/b/b/a/l;

    if-nez v0, :cond_0

    .line 5406
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/b/a/l;

    sput-object v0, Lcom/google/ac/b/b/a/l;->e:[Lcom/google/ac/b/b/a/l;

    .line 5408
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5410
    :cond_1
    sget-object v0, Lcom/google/ac/b/b/a/l;->e:[Lcom/google/ac/b/b/a/l;

    return-object v0

    .line 5408
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5514
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 5515
    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 5516
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/l;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5519
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    if-eqz v1, :cond_1

    .line 5520
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5523
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 5524
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/b/a/l;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5527
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 5528
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/b/a/l;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5531
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 5441
    if-ne p1, p0, :cond_1

    .line 5442
    const/4 v0, 0x1

    .line 5477
    :cond_0
    :goto_0
    return v0

    .line 5444
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/l;

    if-eqz v1, :cond_0

    .line 5447
    check-cast p1, Lcom/google/ac/b/b/a/l;

    .line 5448
    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->a:Ljava/lang/Integer;

    if-nez v1, :cond_6

    .line 5449
    iget-object v1, p1, Lcom/google/ac/b/b/a/l;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 5454
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    if-nez v1, :cond_7

    .line 5455
    iget-object v1, p1, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    if-nez v1, :cond_0

    .line 5463
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->c:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 5464
    iget-object v1, p1, Lcom/google/ac/b/b/a/l;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5470
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->d:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 5471
    iget-object v1, p1, Lcom/google/ac/b/b/a/l;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5477
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/l;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 5452
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/b/a/l;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 5459
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    iget-object v2, p1, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/aj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 5467
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/b/a/l;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 5474
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/b/a/l;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5482
    iget-object v0, p0, Lcom/google/ac/b/b/a/l;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 5484
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 5486
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/l;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 5488
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/b/a/l;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 5490
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/l;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 5491
    return v0

    .line 5482
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/l;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 5484
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/aj;->hashCode()I

    move-result v0

    goto :goto_1

    .line 5486
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/l;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 5488
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5396
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/l;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/l;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/b/a/aj;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/aj;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/l;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/l;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x2a -> :sswitch_3
        0x32 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5497
    iget-object v0, p0, Lcom/google/ac/b/b/a/l;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 5498
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5500
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    if-eqz v0, :cond_1

    .line 5501
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->b:Lcom/google/ac/b/b/a/aj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5503
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/l;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 5504
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5506
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/l;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 5507
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/b/a/l;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5509
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5510
    return-void
.end method
