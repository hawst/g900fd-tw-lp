.class public final Lcom/google/ac/b/c/ac;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/c/ad;

.field public b:[Lcom/google/ac/b/c/bw;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4396
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 4397
    iput-object v1, p0, Lcom/google/ac/b/c/ac;->a:Lcom/google/ac/b/c/ad;

    invoke-static {}, Lcom/google/ac/b/c/bw;->a()[Lcom/google/ac/b/c/bw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    iput-object v1, p0, Lcom/google/ac/b/c/ac;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/ac;->cachedSize:I

    .line 4398
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 4463
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 4464
    iget-object v1, p0, Lcom/google/ac/b/c/ac;->a:Lcom/google/ac/b/c/ad;

    if-eqz v1, :cond_0

    .line 4465
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/ac;->a:Lcom/google/ac/b/c/ad;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4468
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 4469
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 4470
    iget-object v2, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    aget-object v2, v2, v0

    .line 4471
    if-eqz v2, :cond_1

    .line 4472
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 4469
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 4477
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4410
    if-ne p1, p0, :cond_1

    .line 4411
    const/4 v0, 0x1

    .line 4430
    :cond_0
    :goto_0
    return v0

    .line 4413
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/ac;

    if-eqz v1, :cond_0

    .line 4416
    check-cast p1, Lcom/google/ac/b/c/ac;

    .line 4417
    iget-object v1, p0, Lcom/google/ac/b/c/ac;->a:Lcom/google/ac/b/c/ad;

    if-nez v1, :cond_3

    .line 4418
    iget-object v1, p1, Lcom/google/ac/b/c/ac;->a:Lcom/google/ac/b/c/ad;

    if-nez v1, :cond_0

    .line 4426
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    iget-object v2, p1, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4430
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/ac;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 4422
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ac;->a:Lcom/google/ac/b/c/ad;

    iget-object v2, p1, Lcom/google/ac/b/c/ac;->a:Lcom/google/ac/b/c/ad;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/ad;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 4435
    iget-object v0, p0, Lcom/google/ac/b/c/ac;->a:Lcom/google/ac/b/c/ad;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4438
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4440
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/ac;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4441
    return v0

    .line 4435
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ac;->a:Lcom/google/ac/b/c/ad;

    invoke-virtual {v0}, Lcom/google/ac/b/c/ad;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4373
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/ac;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/ac;->a:Lcom/google/ac/b/c/ad;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/ad;

    invoke-direct {v0}, Lcom/google/ac/b/c/ad;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/ac;->a:Lcom/google/ac/b/c/ad;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ac;->a:Lcom/google/ac/b/c/ad;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/bw;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/ac/b/c/bw;

    invoke-direct {v3}, Lcom/google/ac/b/c/bw;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/ac/b/c/bw;

    invoke-direct {v3}, Lcom/google/ac/b/c/bw;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 4447
    iget-object v0, p0, Lcom/google/ac/b/c/ac;->a:Lcom/google/ac/b/c/ad;

    if-eqz v0, :cond_0

    .line 4448
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/ac;->a:Lcom/google/ac/b/c/ad;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4450
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 4451
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 4452
    iget-object v1, p0, Lcom/google/ac/b/c/ac;->b:[Lcom/google/ac/b/c/bw;

    aget-object v1, v1, v0

    .line 4453
    if-eqz v1, :cond_1

    .line 4454
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4451
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4458
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4459
    return-void
.end method
