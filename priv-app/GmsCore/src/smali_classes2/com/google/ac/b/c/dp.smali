.class public final Lcom/google/ac/b/c/dp;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/ac/b/c/dp;


# instance fields
.field public a:[Lcom/google/ac/b/c/do;

.field public b:Lcom/google/ac/b/c/dr;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1775
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1776
    invoke-static {}, Lcom/google/ac/b/c/do;->a()[Lcom/google/ac/b/c/do;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    iput-object v1, p0, Lcom/google/ac/b/c/dp;->b:Lcom/google/ac/b/c/dr;

    iput-object v1, p0, Lcom/google/ac/b/c/dp;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/dp;->cachedSize:I

    .line 1777
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/dp;
    .locals 2

    .prologue
    .line 1758
    sget-object v0, Lcom/google/ac/b/c/dp;->c:[Lcom/google/ac/b/c/dp;

    if-nez v0, :cond_1

    .line 1759
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1761
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/dp;->c:[Lcom/google/ac/b/c/dp;

    if-nez v0, :cond_0

    .line 1762
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/dp;

    sput-object v0, Lcom/google/ac/b/c/dp;->c:[Lcom/google/ac/b/c/dp;

    .line 1764
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1766
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/dp;->c:[Lcom/google/ac/b/c/dp;

    return-object v0

    .line 1764
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 1842
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 1843
    iget-object v0, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1844
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1845
    iget-object v2, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    aget-object v2, v2, v0

    .line 1846
    if-eqz v2, :cond_0

    .line 1847
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1844
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1852
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dp;->b:Lcom/google/ac/b/c/dr;

    if-eqz v0, :cond_2

    .line 1853
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/dp;->b:Lcom/google/ac/b/c/dr;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1856
    :cond_2
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1789
    if-ne p1, p0, :cond_1

    .line 1790
    const/4 v0, 0x1

    .line 1809
    :cond_0
    :goto_0
    return v0

    .line 1792
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/dp;

    if-eqz v1, :cond_0

    .line 1795
    check-cast p1, Lcom/google/ac/b/c/dp;

    .line 1796
    iget-object v1, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    iget-object v2, p1, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1800
    iget-object v1, p0, Lcom/google/ac/b/c/dp;->b:Lcom/google/ac/b/c/dr;

    if-nez v1, :cond_3

    .line 1801
    iget-object v1, p1, Lcom/google/ac/b/c/dp;->b:Lcom/google/ac/b/c/dr;

    if-nez v1, :cond_0

    .line 1809
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/dp;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1805
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/dp;->b:Lcom/google/ac/b/c/dr;

    iget-object v2, p1, Lcom/google/ac/b/c/dp;->b:Lcom/google/ac/b/c/dr;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/dr;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1814
    iget-object v0, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1817
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/dp;->b:Lcom/google/ac/b/c/dr;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 1819
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/dp;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1820
    return v0

    .line 1817
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/dp;->b:Lcom/google/ac/b/c/dr;

    invoke-virtual {v0}, Lcom/google/ac/b/c/dr;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1752
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/dp;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/do;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/c/do;

    invoke-direct {v3}, Lcom/google/ac/b/c/do;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/c/do;

    invoke-direct {v3}, Lcom/google/ac/b/c/do;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/dp;->b:Lcom/google/ac/b/c/dr;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/c/dr;

    invoke-direct {v0}, Lcom/google/ac/b/c/dr;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/dp;->b:Lcom/google/ac/b/c/dr;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/dp;->b:Lcom/google/ac/b/c/dr;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1826
    iget-object v0, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1827
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1828
    iget-object v1, p0, Lcom/google/ac/b/c/dp;->a:[Lcom/google/ac/b/c/do;

    aget-object v1, v1, v0

    .line 1829
    if-eqz v1, :cond_0

    .line 1830
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1827
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1834
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dp;->b:Lcom/google/ac/b/c/dr;

    if-eqz v0, :cond_2

    .line 1835
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/dp;->b:Lcom/google/ac/b/c/dr;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1837
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1838
    return-void
.end method
