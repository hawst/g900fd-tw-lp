.class public final Lcom/google/ac/b/c/cd;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[B

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 556
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 557
    iput-object v0, p0, Lcom/google/ac/b/c/cd;->a:[B

    iput-object v0, p0, Lcom/google/ac/b/c/cd;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/cd;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/cd;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/cd;->cachedSize:I

    .line 558
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 627
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 628
    iget-object v1, p0, Lcom/google/ac/b/c/cd;->a:[B

    if-eqz v1, :cond_0

    .line 629
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/cd;->a:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 632
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/cd;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 633
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/cd;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 636
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/cd;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 637
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/cd;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 640
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 571
    if-ne p1, p0, :cond_1

    .line 572
    const/4 v0, 0x1

    .line 595
    :cond_0
    :goto_0
    return v0

    .line 574
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/cd;

    if-eqz v1, :cond_0

    .line 577
    check-cast p1, Lcom/google/ac/b/c/cd;

    .line 578
    iget-object v1, p0, Lcom/google/ac/b/c/cd;->a:[B

    iget-object v2, p1, Lcom/google/ac/b/c/cd;->a:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 581
    iget-object v1, p0, Lcom/google/ac/b/c/cd;->b:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 582
    iget-object v1, p1, Lcom/google/ac/b/c/cd;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 588
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/cd;->c:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 589
    iget-object v1, p1, Lcom/google/ac/b/c/cd;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 595
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/cd;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 585
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/cd;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/cd;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 592
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/cd;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/cd;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 600
    iget-object v0, p0, Lcom/google/ac/b/c/cd;->a:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 602
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/cd;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 604
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/cd;->c:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 606
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/cd;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 607
    return v0

    .line 602
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cd;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 604
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/cd;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 530
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/cd;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cd;->a:[B

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cd;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cd;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 613
    iget-object v0, p0, Lcom/google/ac/b/c/cd;->a:[B

    if-eqz v0, :cond_0

    .line 614
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/cd;->a:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 616
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cd;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 617
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/cd;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 619
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cd;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 620
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/cd;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 622
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 623
    return-void
.end method
