.class public final Lcom/google/ac/b/c/cx;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/c/db;

.field public b:Lcom/google/ac/b/c/af;

.field public c:Lcom/google/ac/b/c/af;

.field public d:Lcom/google/ac/b/c/bg;

.field public e:Lcom/google/ac/b/c/ae;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 543
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 544
    iput-object v0, p0, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    iput-object v0, p0, Lcom/google/ac/b/c/cx;->b:Lcom/google/ac/b/c/af;

    iput-object v0, p0, Lcom/google/ac/b/c/cx;->c:Lcom/google/ac/b/c/af;

    iput-object v0, p0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    iput-object v0, p0, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    iput-object v0, p0, Lcom/google/ac/b/c/cx;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/cx;->cachedSize:I

    .line 545
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 655
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 656
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    if-eqz v1, :cond_0

    .line 657
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 660
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->b:Lcom/google/ac/b/c/af;

    if-eqz v1, :cond_1

    .line 661
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/cx;->b:Lcom/google/ac/b/c/af;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 664
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->c:Lcom/google/ac/b/c/af;

    if-eqz v1, :cond_2

    .line 665
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/cx;->c:Lcom/google/ac/b/c/af;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 668
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    if-eqz v1, :cond_3

    .line 669
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 672
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    if-eqz v1, :cond_4

    .line 673
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 676
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 560
    if-ne p1, p0, :cond_1

    .line 561
    const/4 v0, 0x1

    .line 612
    :cond_0
    :goto_0
    return v0

    .line 563
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/cx;

    if-eqz v1, :cond_0

    .line 566
    check-cast p1, Lcom/google/ac/b/c/cx;

    .line 567
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    if-nez v1, :cond_7

    .line 568
    iget-object v1, p1, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    if-nez v1, :cond_0

    .line 576
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->b:Lcom/google/ac/b/c/af;

    if-nez v1, :cond_8

    .line 577
    iget-object v1, p1, Lcom/google/ac/b/c/cx;->b:Lcom/google/ac/b/c/af;

    if-nez v1, :cond_0

    .line 585
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->c:Lcom/google/ac/b/c/af;

    if-nez v1, :cond_9

    .line 586
    iget-object v1, p1, Lcom/google/ac/b/c/cx;->c:Lcom/google/ac/b/c/af;

    if-nez v1, :cond_0

    .line 594
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    if-nez v1, :cond_a

    .line 595
    iget-object v1, p1, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    if-nez v1, :cond_0

    .line 603
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    if-nez v1, :cond_b

    .line 604
    iget-object v1, p1, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    if-nez v1, :cond_0

    .line 612
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/cx;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 572
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    iget-object v2, p1, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/db;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 581
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->b:Lcom/google/ac/b/c/af;

    iget-object v2, p1, Lcom/google/ac/b/c/cx;->b:Lcom/google/ac/b/c/af;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/af;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 590
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->c:Lcom/google/ac/b/c/af;

    iget-object v2, p1, Lcom/google/ac/b/c/cx;->c:Lcom/google/ac/b/c/af;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/af;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 599
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    iget-object v2, p1, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bg;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 608
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    iget-object v2, p1, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/ae;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 617
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 620
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/cx;->b:Lcom/google/ac/b/c/af;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 622
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/cx;->c:Lcom/google/ac/b/c/af;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 624
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 626
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 628
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/cx;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 629
    return v0

    .line 617
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {v0}, Lcom/google/ac/b/c/db;->hashCode()I

    move-result v0

    goto :goto_0

    .line 620
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->b:Lcom/google/ac/b/c/af;

    invoke-virtual {v0}, Lcom/google/ac/b/c/af;->hashCode()I

    move-result v0

    goto :goto_1

    .line 622
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->c:Lcom/google/ac/b/c/af;

    invoke-virtual {v0}, Lcom/google/ac/b/c/af;->hashCode()I

    move-result v0

    goto :goto_2

    .line 624
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    invoke-virtual {v0}, Lcom/google/ac/b/c/bg;->hashCode()I

    move-result v0

    goto :goto_3

    .line 626
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    invoke-virtual {v1}, Lcom/google/ac/b/c/ae;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 511
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/cx;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/db;

    invoke-direct {v0}, Lcom/google/ac/b/c/db;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->b:Lcom/google/ac/b/c/af;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/af;

    invoke-direct {v0}, Lcom/google/ac/b/c/af;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cx;->b:Lcom/google/ac/b/c/af;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->b:Lcom/google/ac/b/c/af;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->c:Lcom/google/ac/b/c/af;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/b/c/af;

    invoke-direct {v0}, Lcom/google/ac/b/c/af;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cx;->c:Lcom/google/ac/b/c/af;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->c:Lcom/google/ac/b/c/af;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/c/bg;

    invoke-direct {v0}, Lcom/google/ac/b/c/bg;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/ac/b/c/ae;

    invoke-direct {v0}, Lcom/google/ac/b/c/ae;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 635
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    if-eqz v0, :cond_0

    .line 636
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/cx;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 638
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->b:Lcom/google/ac/b/c/af;

    if-eqz v0, :cond_1

    .line 639
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/cx;->b:Lcom/google/ac/b/c/af;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 641
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->c:Lcom/google/ac/b/c/af;

    if-eqz v0, :cond_2

    .line 642
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/cx;->c:Lcom/google/ac/b/c/af;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 644
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    if-eqz v0, :cond_3

    .line 645
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/cx;->d:Lcom/google/ac/b/c/bg;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 647
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    if-eqz v0, :cond_4

    .line 648
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/cx;->e:Lcom/google/ac/b/c/ae;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 650
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 651
    return-void
.end method
