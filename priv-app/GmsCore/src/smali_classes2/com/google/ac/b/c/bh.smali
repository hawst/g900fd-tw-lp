.class public final Lcom/google/ac/b/c/bh;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7382
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 7383
    iput-object v0, p0, Lcom/google/ac/b/c/bh;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/bh;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/bh;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/bh;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bh;->cachedSize:I

    .line 7384
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 7458
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 7459
    iget-object v1, p0, Lcom/google/ac/b/c/bh;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 7460
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/bh;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7463
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/bh;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 7464
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/bh;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7467
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/bh;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 7468
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/bh;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7471
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 7397
    if-ne p1, p0, :cond_1

    .line 7398
    const/4 v0, 0x1

    .line 7425
    :cond_0
    :goto_0
    return v0

    .line 7400
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bh;

    if-eqz v1, :cond_0

    .line 7403
    check-cast p1, Lcom/google/ac/b/c/bh;

    .line 7404
    iget-object v1, p0, Lcom/google/ac/b/c/bh;->a:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 7405
    iget-object v1, p1, Lcom/google/ac/b/c/bh;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 7411
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bh;->b:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 7412
    iget-object v1, p1, Lcom/google/ac/b/c/bh;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 7418
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bh;->c:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 7419
    iget-object v1, p1, Lcom/google/ac/b/c/bh;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 7425
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bh;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 7408
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/bh;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/bh;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 7415
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/bh;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/bh;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 7422
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/bh;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/bh;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 7430
    iget-object v0, p0, Lcom/google/ac/b/c/bh;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 7433
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bh;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 7435
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/bh;->c:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 7437
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bh;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 7438
    return v0

    .line 7430
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bh;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 7433
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bh;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 7435
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bh;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 7356
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bh;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bh;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bh;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bh;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 7444
    iget-object v0, p0, Lcom/google/ac/b/c/bh;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 7445
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/bh;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 7447
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bh;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 7448
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/bh;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 7450
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bh;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 7451
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/bh;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 7453
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7454
    return-void
.end method
