.class public final Lcom/google/ac/b/c/cs;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/c/dc;

.field public b:Lcom/google/ac/b/c/bi;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1057
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1058
    iput-object v0, p0, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    iput-object v0, p0, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    iput-object v0, p0, Lcom/google/ac/b/c/cs;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/cs;->cachedSize:I

    .line 1059
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1124
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1125
    iget-object v1, p0, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    if-eqz v1, :cond_0

    .line 1126
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1129
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    if-eqz v1, :cond_1

    .line 1130
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1133
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1071
    if-ne p1, p0, :cond_1

    .line 1072
    const/4 v0, 0x1

    .line 1096
    :cond_0
    :goto_0
    return v0

    .line 1074
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/cs;

    if-eqz v1, :cond_0

    .line 1077
    check-cast p1, Lcom/google/ac/b/c/cs;

    .line 1078
    iget-object v1, p0, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    if-nez v1, :cond_4

    .line 1079
    iget-object v1, p1, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    if-nez v1, :cond_0

    .line 1087
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    if-nez v1, :cond_5

    .line 1088
    iget-object v1, p1, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    if-nez v1, :cond_0

    .line 1096
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/cs;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1083
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    iget-object v2, p1, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/dc;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1092
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    iget-object v2, p1, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bi;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1101
    iget-object v0, p0, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1104
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1106
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/cs;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1107
    return v0

    .line 1101
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    invoke-virtual {v0}, Lcom/google/ac/b/c/dc;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1104
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    invoke-virtual {v1}, Lcom/google/ac/b/c/bi;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1034
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/cs;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/dc;

    invoke-direct {v0}, Lcom/google/ac/b/c/dc;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/bi;

    invoke-direct {v0}, Lcom/google/ac/b/c/bi;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1113
    iget-object v0, p0, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    if-eqz v0, :cond_0

    .line 1114
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/cs;->a:Lcom/google/ac/b/c/dc;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1116
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    if-eqz v0, :cond_1

    .line 1117
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/cs;->b:Lcom/google/ac/b/c/bi;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1119
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1120
    return-void
.end method
