.class public final Lcom/google/ac/b/c/ay;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/ac/b/c/ax;

.field public b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2434
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2435
    invoke-static {}, Lcom/google/ac/b/c/ax;->a()[Lcom/google/ac/b/c/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    iput-object v1, p0, Lcom/google/ac/b/c/ay;->b:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/ac/b/c/ay;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/ay;->cachedSize:I

    .line 2436
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 2499
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 2500
    iget-object v0, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 2501
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2502
    iget-object v2, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    aget-object v2, v2, v0

    .line 2503
    if-eqz v2, :cond_0

    .line 2504
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2501
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2509
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ay;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 2510
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/ay;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 2513
    :cond_2
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2448
    if-ne p1, p0, :cond_1

    .line 2449
    const/4 v0, 0x1

    .line 2466
    :cond_0
    :goto_0
    return v0

    .line 2451
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/ay;

    if-eqz v1, :cond_0

    .line 2454
    check-cast p1, Lcom/google/ac/b/c/ay;

    .line 2455
    iget-object v1, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    iget-object v2, p1, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2459
    iget-object v1, p0, Lcom/google/ac/b/c/ay;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_3

    .line 2460
    iget-object v1, p1, Lcom/google/ac/b/c/ay;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 2466
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/ay;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2463
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ay;->b:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/ay;->b:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2471
    iget-object v0, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 2474
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ay;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 2476
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/ay;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2477
    return v0

    .line 2474
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ay;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2411
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/ay;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/ax;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/c/ax;

    invoke-direct {v3}, Lcom/google/ac/b/c/ax;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/c/ax;

    invoke-direct {v3}, Lcom/google/ac/b/c/ax;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ay;->b:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 2483
    iget-object v0, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 2484
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 2485
    iget-object v1, p0, Lcom/google/ac/b/c/ay;->a:[Lcom/google/ac/b/c/ax;

    aget-object v1, v1, v0

    .line 2486
    if-eqz v1, :cond_0

    .line 2487
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2484
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2491
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ay;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 2492
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/ay;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2494
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2495
    return-void
.end method
