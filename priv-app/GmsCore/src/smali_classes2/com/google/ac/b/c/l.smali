.class public final Lcom/google/ac/b/c/l;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Double;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 521
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 522
    iput-object v0, p0, Lcom/google/ac/b/c/l;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/c/l;->b:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/ac/b/c/l;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/l;->cachedSize:I

    .line 523
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 584
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 585
    iget-object v1, p0, Lcom/google/ac/b/c/l;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 586
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/l;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 589
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/l;->b:Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 590
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/l;->b:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 593
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 535
    if-ne p1, p0, :cond_1

    .line 536
    const/4 v0, 0x1

    .line 556
    :cond_0
    :goto_0
    return v0

    .line 538
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/l;

    if-eqz v1, :cond_0

    .line 541
    check-cast p1, Lcom/google/ac/b/c/l;

    .line 542
    iget-object v1, p0, Lcom/google/ac/b/c/l;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_4

    .line 543
    iget-object v1, p1, Lcom/google/ac/b/c/l;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 549
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/l;->b:Ljava/lang/Double;

    if-nez v1, :cond_5

    .line 550
    iget-object v1, p1, Lcom/google/ac/b/c/l;->b:Ljava/lang/Double;

    if-nez v1, :cond_0

    .line 556
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/l;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 546
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/l;->a:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/l;->a:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 553
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/l;->b:Ljava/lang/Double;

    iget-object v2, p1, Lcom/google/ac/b/c/l;->b:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 561
    iget-object v0, p0, Lcom/google/ac/b/c/l;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 564
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/l;->b:Ljava/lang/Double;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 566
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/l;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    return v0

    .line 561
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/l;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0

    .line 564
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/l;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 498
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/l;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/l;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/l;->b:Ljava/lang/Double;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 573
    iget-object v0, p0, Lcom/google/ac/b/c/l;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 574
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/l;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 576
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/l;->b:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 577
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/l;->b:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 579
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 580
    return-void
.end method
