.class public final Lcom/google/ac/b/c/bi;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:[Lcom/google/ac/b/c/aw;

.field public d:[Lcom/google/ac/b/c/am;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1311
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1312
    iput-object v1, p0, Lcom/google/ac/b/c/bi;->a:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/ac/b/c/bi;->b:Ljava/lang/Boolean;

    invoke-static {}, Lcom/google/ac/b/c/aw;->a()[Lcom/google/ac/b/c/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    invoke-static {}, Lcom/google/ac/b/c/am;->a()[Lcom/google/ac/b/c/am;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    iput-object v1, p0, Lcom/google/ac/b/c/bi;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bi;->cachedSize:I

    .line 1313
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1404
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1405
    iget-object v2, p0, Lcom/google/ac/b/c/bi;->a:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 1406
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/ac/b/c/bi;->a:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1409
    :cond_0
    iget-object v2, p0, Lcom/google/ac/b/c/bi;->b:Ljava/lang/Boolean;

    if-eqz v2, :cond_1

    .line 1410
    const/4 v2, 0x5

    iget-object v3, p0, Lcom/google/ac/b/c/bi;->b:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v2}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    .line 1413
    :cond_1
    iget-object v2, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    .line 1414
    :goto_0
    iget-object v3, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 1415
    iget-object v3, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    aget-object v3, v3, v0

    .line 1416
    if-eqz v3, :cond_2

    .line 1417
    const/4 v4, 0x6

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1414
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 1422
    :cond_4
    iget-object v2, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 1423
    :goto_1
    iget-object v2, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 1424
    iget-object v2, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    aget-object v2, v2, v1

    .line 1425
    if-eqz v2, :cond_5

    .line 1426
    const/4 v3, 0x7

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1423
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1431
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1327
    if-ne p1, p0, :cond_1

    .line 1328
    const/4 v0, 0x1

    .line 1356
    :cond_0
    :goto_0
    return v0

    .line 1330
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bi;

    if-eqz v1, :cond_0

    .line 1333
    check-cast p1, Lcom/google/ac/b/c/bi;

    .line 1334
    iget-object v1, p0, Lcom/google/ac/b/c/bi;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_4

    .line 1335
    iget-object v1, p1, Lcom/google/ac/b/c/bi;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1341
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bi;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_5

    .line 1342
    iget-object v1, p1, Lcom/google/ac/b/c/bi;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1348
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    iget-object v2, p1, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1352
    iget-object v1, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    iget-object v2, p1, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1356
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bi;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1338
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/bi;->a:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/bi;->a:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1345
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/bi;->b:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/bi;->b:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1361
    iget-object v0, p0, Lcom/google/ac/b/c/bi;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1364
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/bi;->b:Ljava/lang/Boolean;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1366
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1368
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1370
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bi;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1371
    return v0

    .line 1361
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bi;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1364
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/bi;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1282
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bi;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bi;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bi;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x32

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/aw;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/c/aw;

    invoke-direct {v3}, Lcom/google/ac/b/c/aw;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/c/aw;

    invoke-direct {v3}, Lcom/google/ac/b/c/aw;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/am;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/ac/b/c/am;

    invoke-direct {v3}, Lcom/google/ac/b/c/am;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/ac/b/c/am;

    invoke-direct {v3}, Lcom/google/ac/b/c/am;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x28 -> :sswitch_2
        0x32 -> :sswitch_3
        0x3a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1377
    iget-object v0, p0, Lcom/google/ac/b/c/bi;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1378
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/bi;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1380
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bi;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1381
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/bi;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1383
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 1384
    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 1385
    iget-object v2, p0, Lcom/google/ac/b/c/bi;->c:[Lcom/google/ac/b/c/aw;

    aget-object v2, v2, v0

    .line 1386
    if-eqz v2, :cond_2

    .line 1387
    const/4 v3, 0x6

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1384
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1391
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 1392
    :goto_1
    iget-object v0, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 1393
    iget-object v0, p0, Lcom/google/ac/b/c/bi;->d:[Lcom/google/ac/b/c/am;

    aget-object v0, v0, v1

    .line 1394
    if-eqz v0, :cond_4

    .line 1395
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1392
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1399
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1400
    return-void
.end method
