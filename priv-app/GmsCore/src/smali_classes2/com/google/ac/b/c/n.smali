.class public final Lcom/google/ac/b/c/n;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2633
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2634
    iput-object v0, p0, Lcom/google/ac/b/c/n;->a:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/n;->b:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/n;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/n;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/n;->cachedSize:I

    .line 2635
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 2709
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2710
    iget-object v1, p0, Lcom/google/ac/b/c/n;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 2711
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/n;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2714
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/n;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 2715
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/n;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2718
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/n;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 2719
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/n;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2722
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2648
    if-ne p1, p0, :cond_1

    .line 2649
    const/4 v0, 0x1

    .line 2676
    :cond_0
    :goto_0
    return v0

    .line 2651
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/n;

    if-eqz v1, :cond_0

    .line 2654
    check-cast p1, Lcom/google/ac/b/c/n;

    .line 2655
    iget-object v1, p0, Lcom/google/ac/b/c/n;->a:Ljava/lang/Long;

    if-nez v1, :cond_5

    .line 2656
    iget-object v1, p1, Lcom/google/ac/b/c/n;->a:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 2662
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/n;->b:Ljava/lang/Long;

    if-nez v1, :cond_6

    .line 2663
    iget-object v1, p1, Lcom/google/ac/b/c/n;->b:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 2669
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/n;->c:Ljava/lang/Long;

    if-nez v1, :cond_7

    .line 2670
    iget-object v1, p1, Lcom/google/ac/b/c/n;->c:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 2676
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/n;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2659
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/n;->a:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/n;->a:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2666
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/n;->b:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/n;->b:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 2673
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/n;->c:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/n;->c:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2681
    iget-object v0, p0, Lcom/google/ac/b/c/n;->a:Ljava/lang/Long;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2684
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/n;->b:Ljava/lang/Long;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2686
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/n;->c:Ljava/lang/Long;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 2688
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/n;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2689
    return v0

    .line 2681
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/n;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2684
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/n;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2686
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/n;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 2607
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/n;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/n;->a:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/n;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/n;->c:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 2695
    iget-object v0, p0, Lcom/google/ac/b/c/n;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2696
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/n;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2698
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/n;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2699
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/n;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2701
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/n;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2702
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/n;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2704
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2705
    return-void
.end method
