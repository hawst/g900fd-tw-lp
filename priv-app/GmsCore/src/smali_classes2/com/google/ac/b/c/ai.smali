.class public final Lcom/google/ac/b/c/ai;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7827
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 7828
    iput-object v0, p0, Lcom/google/ac/b/c/ai;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/ai;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/ai;->cachedSize:I

    .line 7829
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 7875
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 7876
    iget-object v1, p0, Lcom/google/ac/b/c/ai;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 7877
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/ai;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7880
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 7840
    if-ne p1, p0, :cond_1

    .line 7841
    const/4 v0, 0x1

    .line 7853
    :cond_0
    :goto_0
    return v0

    .line 7843
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/ai;

    if-eqz v1, :cond_0

    .line 7846
    check-cast p1, Lcom/google/ac/b/c/ai;

    .line 7847
    iget-object v1, p0, Lcom/google/ac/b/c/ai;->a:Ljava/lang/Integer;

    if-nez v1, :cond_3

    .line 7848
    iget-object v1, p1, Lcom/google/ac/b/c/ai;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 7853
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/ai;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 7851
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ai;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/ai;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 7858
    iget-object v0, p0, Lcom/google/ac/b/c/ai;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 7860
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/ai;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 7861
    return v0

    .line 7858
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ai;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 7807
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/ai;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ai;->a:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 7867
    iget-object v0, p0, Lcom/google/ac/b/c/ai;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 7868
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/ai;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7870
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7871
    return-void
.end method
