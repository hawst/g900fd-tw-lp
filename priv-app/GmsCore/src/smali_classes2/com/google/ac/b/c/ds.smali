.class public final Lcom/google/ac/b/c/ds;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/ac/b/c/ds;


# instance fields
.field public a:Lcom/google/ac/b/c/dt;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Lcom/google/ac/b/c/dj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1189
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1190
    iput-object v0, p0, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    iput-object v0, p0, Lcom/google/ac/b/c/ds;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ds;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ds;->d:Lcom/google/ac/b/c/dj;

    iput-object v0, p0, Lcom/google/ac/b/c/ds;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/ds;->cachedSize:I

    .line 1191
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/ds;
    .locals 2

    .prologue
    .line 1166
    sget-object v0, Lcom/google/ac/b/c/ds;->e:[Lcom/google/ac/b/c/ds;

    if-nez v0, :cond_1

    .line 1167
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1169
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/ds;->e:[Lcom/google/ac/b/c/ds;

    if-nez v0, :cond_0

    .line 1170
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/ds;

    sput-object v0, Lcom/google/ac/b/c/ds;->e:[Lcom/google/ac/b/c/ds;

    .line 1172
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1174
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/ds;->e:[Lcom/google/ac/b/c/ds;

    return-object v0

    .line 1172
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1282
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1283
    iget-object v1, p0, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    if-eqz v1, :cond_0

    .line 1284
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1287
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/ds;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1288
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/ds;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1291
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/ds;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 1292
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/ds;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1295
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ds;->d:Lcom/google/ac/b/c/dj;

    if-eqz v1, :cond_3

    .line 1296
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/ds;->d:Lcom/google/ac/b/c/dj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1299
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1205
    if-ne p1, p0, :cond_1

    .line 1206
    const/4 v0, 0x1

    .line 1244
    :cond_0
    :goto_0
    return v0

    .line 1208
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/ds;

    if-eqz v1, :cond_0

    .line 1211
    check-cast p1, Lcom/google/ac/b/c/ds;

    .line 1212
    iget-object v1, p0, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    if-nez v1, :cond_6

    .line 1213
    iget-object v1, p1, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    if-nez v1, :cond_0

    .line 1221
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ds;->b:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 1222
    iget-object v1, p1, Lcom/google/ac/b/c/ds;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1228
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ds;->c:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 1229
    iget-object v1, p1, Lcom/google/ac/b/c/ds;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1235
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/ds;->d:Lcom/google/ac/b/c/dj;

    if-nez v1, :cond_9

    .line 1236
    iget-object v1, p1, Lcom/google/ac/b/c/ds;->d:Lcom/google/ac/b/c/dj;

    if-nez v1, :cond_0

    .line 1244
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/ds;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1217
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    iget-object v2, p1, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/dt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1225
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/ds;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ds;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 1232
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/ds;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ds;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 1240
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/ds;->d:Lcom/google/ac/b/c/dj;

    iget-object v2, p1, Lcom/google/ac/b/c/ds;->d:Lcom/google/ac/b/c/dj;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/dj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1249
    iget-object v0, p0, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1252
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ds;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1254
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ds;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1256
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/ds;->d:Lcom/google/ac/b/c/dj;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 1258
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/ds;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1259
    return v0

    .line 1249
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    invoke-virtual {v0}, Lcom/google/ac/b/c/dt;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1252
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ds;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1254
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ds;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1256
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ds;->d:Lcom/google/ac/b/c/dj;

    invoke-virtual {v1}, Lcom/google/ac/b/c/dj;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1160
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/ds;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/dt;

    invoke-direct {v0}, Lcom/google/ac/b/c/dt;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ds;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ds;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/ds;->d:Lcom/google/ac/b/c/dj;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/dj;

    invoke-direct {v0}, Lcom/google/ac/b/c/dj;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/ds;->d:Lcom/google/ac/b/c/dj;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ds;->d:Lcom/google/ac/b/c/dj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1265
    iget-object v0, p0, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    if-eqz v0, :cond_0

    .line 1266
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/ds;->a:Lcom/google/ac/b/c/dt;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1268
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ds;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 1269
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/ds;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1271
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ds;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 1272
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/ds;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1274
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ds;->d:Lcom/google/ac/b/c/dj;

    if-eqz v0, :cond_3

    .line 1275
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/ds;->d:Lcom/google/ac/b/c/dj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1277
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1278
    return-void
.end method
