.class public final Lcom/google/ac/b/c/dm;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/ac/b/c/dn;

.field public b:[Lcom/google/ac/b/c/di;

.field public c:[Lcom/google/ac/b/c/ds;

.field public d:Lcom/google/ac/b/c/dk;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2396
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2397
    invoke-static {}, Lcom/google/ac/b/c/dn;->a()[Lcom/google/ac/b/c/dn;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    invoke-static {}, Lcom/google/ac/b/c/di;->a()[Lcom/google/ac/b/c/di;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    invoke-static {}, Lcom/google/ac/b/c/ds;->a()[Lcom/google/ac/b/c/ds;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    iput-object v1, p0, Lcom/google/ac/b/c/dm;->d:Lcom/google/ac/b/c/dk;

    iput-object v1, p0, Lcom/google/ac/b/c/dm;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/dm;->cachedSize:I

    .line 2398
    return-void
.end method

.method public static a([B)Lcom/google/ac/b/c/dm;
    .locals 1

    .prologue
    .line 2616
    new-instance v0, Lcom/google/ac/b/c/dm;

    invoke-direct {v0}, Lcom/google/ac/b/c/dm;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/ac/b/c/dm;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2493
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2494
    iget-object v2, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 2495
    :goto_0
    iget-object v3, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 2496
    iget-object v3, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    aget-object v3, v3, v0

    .line 2497
    if-eqz v3, :cond_0

    .line 2498
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2495
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2503
    :cond_2
    iget-object v2, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 2504
    :goto_1
    iget-object v3, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 2505
    iget-object v3, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    aget-object v3, v3, v0

    .line 2506
    if-eqz v3, :cond_3

    .line 2507
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2504
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    move v0, v2

    .line 2512
    :cond_5
    iget-object v2, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 2513
    :goto_2
    iget-object v2, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 2514
    iget-object v2, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    aget-object v2, v2, v1

    .line 2515
    if-eqz v2, :cond_6

    .line 2516
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2513
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2521
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/dm;->d:Lcom/google/ac/b/c/dk;

    if-eqz v1, :cond_8

    .line 2522
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/dm;->d:Lcom/google/ac/b/c/dk;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2525
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2412
    if-ne p1, p0, :cond_1

    .line 2413
    const/4 v0, 0x1

    .line 2440
    :cond_0
    :goto_0
    return v0

    .line 2415
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/dm;

    if-eqz v1, :cond_0

    .line 2418
    check-cast p1, Lcom/google/ac/b/c/dm;

    .line 2419
    iget-object v1, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    iget-object v2, p1, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2423
    iget-object v1, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    iget-object v2, p1, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2427
    iget-object v1, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    iget-object v2, p1, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2431
    iget-object v1, p0, Lcom/google/ac/b/c/dm;->d:Lcom/google/ac/b/c/dk;

    if-nez v1, :cond_3

    .line 2432
    iget-object v1, p1, Lcom/google/ac/b/c/dm;->d:Lcom/google/ac/b/c/dk;

    if-nez v1, :cond_0

    .line 2440
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/dm;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2436
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/dm;->d:Lcom/google/ac/b/c/dk;

    iget-object v2, p1, Lcom/google/ac/b/c/dm;->d:Lcom/google/ac/b/c/dk;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/dk;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2445
    iget-object v0, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 2448
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2450
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2452
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/dm;->d:Lcom/google/ac/b/c/dk;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 2454
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/dm;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2455
    return v0

    .line 2452
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/dm;->d:Lcom/google/ac/b/c/dk;

    invoke-virtual {v0}, Lcom/google/ac/b/c/dk;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2367
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/dm;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/dn;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/c/dn;

    invoke-direct {v3}, Lcom/google/ac/b/c/dn;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/c/dn;

    invoke-direct {v3}, Lcom/google/ac/b/c/dn;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/di;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/ac/b/c/di;

    invoke-direct {v3}, Lcom/google/ac/b/c/di;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/ac/b/c/di;

    invoke-direct {v3}, Lcom/google/ac/b/c/di;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    goto/16 :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/ds;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/ac/b/c/ds;

    invoke-direct {v3}, Lcom/google/ac/b/c/ds;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lcom/google/ac/b/c/ds;

    invoke-direct {v3}, Lcom/google/ac/b/c/ds;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/dm;->d:Lcom/google/ac/b/c/dk;

    if-nez v0, :cond_a

    new-instance v0, Lcom/google/ac/b/c/dk;

    invoke-direct {v0}, Lcom/google/ac/b/c/dk;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/dm;->d:Lcom/google/ac/b/c/dk;

    :cond_a
    iget-object v0, p0, Lcom/google/ac/b/c/dm;->d:Lcom/google/ac/b/c/dk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2461
    iget-object v0, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 2462
    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2463
    iget-object v2, p0, Lcom/google/ac/b/c/dm;->a:[Lcom/google/ac/b/c/dn;

    aget-object v2, v2, v0

    .line 2464
    if-eqz v2, :cond_0

    .line 2465
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2462
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2469
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 2470
    :goto_1
    iget-object v2, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 2471
    iget-object v2, p0, Lcom/google/ac/b/c/dm;->b:[Lcom/google/ac/b/c/di;

    aget-object v2, v2, v0

    .line 2472
    if-eqz v2, :cond_2

    .line 2473
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2470
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2477
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 2478
    :goto_2
    iget-object v0, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 2479
    iget-object v0, p0, Lcom/google/ac/b/c/dm;->c:[Lcom/google/ac/b/c/ds;

    aget-object v0, v0, v1

    .line 2480
    if-eqz v0, :cond_4

    .line 2481
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2478
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 2485
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/dm;->d:Lcom/google/ac/b/c/dk;

    if-eqz v0, :cond_6

    .line 2486
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/dm;->d:Lcom/google/ac/b/c/dk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2488
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2489
    return-void
.end method
