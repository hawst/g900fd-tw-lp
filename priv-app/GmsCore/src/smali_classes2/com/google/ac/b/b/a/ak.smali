.class public final Lcom/google/ac/b/b/a/ak;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/ac/b/b/a/ak;


# instance fields
.field public a:[Lcom/google/ac/b/b/a/al;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 3458
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3459
    invoke-static {}, Lcom/google/ac/b/b/a/al;->a()[Lcom/google/ac/b/b/a/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ac/b/b/a/ak;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/ak;->cachedSize:I

    .line 3460
    return-void
.end method

.method public static a()[Lcom/google/ac/b/b/a/ak;
    .locals 2

    .prologue
    .line 3444
    sget-object v0, Lcom/google/ac/b/b/a/ak;->b:[Lcom/google/ac/b/b/a/ak;

    if-nez v0, :cond_1

    .line 3445
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 3447
    :try_start_0
    sget-object v0, Lcom/google/ac/b/b/a/ak;->b:[Lcom/google/ac/b/b/a/ak;

    if-nez v0, :cond_0

    .line 3448
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/b/a/ak;

    sput-object v0, Lcom/google/ac/b/b/a/ak;->b:[Lcom/google/ac/b/b/a/ak;

    .line 3450
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3452
    :cond_1
    sget-object v0, Lcom/google/ac/b/b/a/ak;->b:[Lcom/google/ac/b/b/a/ak;

    return-object v0

    .line 3450
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 3510
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 3511
    iget-object v0, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 3512
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 3513
    iget-object v2, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    aget-object v2, v2, v0

    .line 3514
    if-eqz v2, :cond_0

    .line 3515
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3512
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3520
    :cond_1
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3471
    if-ne p1, p0, :cond_1

    .line 3472
    const/4 v0, 0x1

    .line 3482
    :cond_0
    :goto_0
    return v0

    .line 3474
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/ak;

    if-eqz v1, :cond_0

    .line 3477
    check-cast p1, Lcom/google/ac/b/b/a/ak;

    .line 3478
    iget-object v1, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3482
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/ak;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 3487
    iget-object v0, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 3490
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/ak;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3491
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3438
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/ak;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/b/a/al;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/b/a/al;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/al;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/b/a/al;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/al;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 3497
    iget-object v0, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 3498
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 3499
    iget-object v1, p0, Lcom/google/ac/b/b/a/ak;->a:[Lcom/google/ac/b/b/a/al;

    aget-object v1, v1, v0

    .line 3500
    if-eqz v1, :cond_0

    .line 3501
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3498
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3505
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3506
    return-void
.end method
