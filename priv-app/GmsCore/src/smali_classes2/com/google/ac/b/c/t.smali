.class public final Lcom/google/ac/b/c/t;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Lcom/google/ac/b/c/u;

.field public c:Lcom/google/ac/b/c/r;

.field public d:Lcom/google/ac/b/c/ai;

.field public e:Lcom/google/ac/b/c/s;

.field public f:Lcom/google/ac/b/c/bx;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 6793
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 6794
    iput-object v0, p0, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    iput-object v0, p0, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    iput-object v0, p0, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    iput-object v0, p0, Lcom/google/ac/b/c/t;->e:Lcom/google/ac/b/c/s;

    iput-object v0, p0, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    iput-object v0, p0, Lcom/google/ac/b/c/t;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/t;->cachedSize:I

    .line 6795
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 6918
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 6919
    iget-object v1, p0, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 6920
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 6923
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    if-eqz v1, :cond_1

    .line 6924
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6927
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    if-eqz v1, :cond_2

    .line 6928
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6931
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    if-eqz v1, :cond_3

    .line 6932
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6935
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/t;->e:Lcom/google/ac/b/c/s;

    if-eqz v1, :cond_4

    .line 6936
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/t;->e:Lcom/google/ac/b/c/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6939
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    if-eqz v1, :cond_5

    .line 6940
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6943
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 6811
    if-ne p1, p0, :cond_1

    .line 6812
    const/4 v0, 0x1

    .line 6870
    :cond_0
    :goto_0
    return v0

    .line 6814
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/t;

    if-eqz v1, :cond_0

    .line 6817
    check-cast p1, Lcom/google/ac/b/c/t;

    .line 6818
    iget-object v1, p0, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    if-nez v1, :cond_8

    .line 6819
    iget-object v1, p1, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 6825
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    if-nez v1, :cond_9

    .line 6826
    iget-object v1, p1, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    if-nez v1, :cond_0

    .line 6834
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    if-nez v1, :cond_a

    .line 6835
    iget-object v1, p1, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    if-nez v1, :cond_0

    .line 6843
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    if-nez v1, :cond_b

    .line 6844
    iget-object v1, p1, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    if-nez v1, :cond_0

    .line 6852
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/t;->e:Lcom/google/ac/b/c/s;

    if-nez v1, :cond_c

    .line 6853
    iget-object v1, p1, Lcom/google/ac/b/c/t;->e:Lcom/google/ac/b/c/s;

    if-nez v1, :cond_0

    .line 6861
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    if-nez v1, :cond_d

    .line 6862
    iget-object v1, p1, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    if-nez v1, :cond_0

    .line 6870
    :cond_7
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/t;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 6822
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 6830
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    iget-object v2, p1, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/u;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 6839
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    iget-object v2, p1, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/r;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 6848
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    iget-object v2, p1, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/ai;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 6857
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/c/t;->e:Lcom/google/ac/b/c/s;

    iget-object v2, p1, Lcom/google/ac/b/c/t;->e:Lcom/google/ac/b/c/s;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 6866
    :cond_d
    iget-object v1, p0, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    iget-object v2, p1, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bx;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 6875
    iget-object v0, p0, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 6878
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 6880
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 6882
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 6884
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/t;->e:Lcom/google/ac/b/c/s;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 6886
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 6888
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/t;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 6889
    return v0

    .line 6875
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_0

    .line 6878
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    invoke-virtual {v0}, Lcom/google/ac/b/c/u;->hashCode()I

    move-result v0

    goto :goto_1

    .line 6880
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    invoke-virtual {v0}, Lcom/google/ac/b/c/r;->hashCode()I

    move-result v0

    goto :goto_2

    .line 6882
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    invoke-virtual {v0}, Lcom/google/ac/b/c/ai;->hashCode()I

    move-result v0

    goto :goto_3

    .line 6884
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/t;->e:Lcom/google/ac/b/c/s;

    invoke-virtual {v0}, Lcom/google/ac/b/c/s;->hashCode()I

    move-result v0

    goto :goto_4

    .line 6886
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    invoke-virtual {v1}, Lcom/google/ac/b/c/bx;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 6758
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/t;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/u;

    invoke-direct {v0}, Lcom/google/ac/b/c/u;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/r;

    invoke-direct {v0}, Lcom/google/ac/b/c/r;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/b/c/ai;

    invoke-direct {v0}, Lcom/google/ac/b/c/ai;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/ac/b/c/t;->e:Lcom/google/ac/b/c/s;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/c/s;

    invoke-direct {v0}, Lcom/google/ac/b/c/s;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/t;->e:Lcom/google/ac/b/c/s;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/t;->e:Lcom/google/ac/b/c/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/ac/b/c/bx;

    invoke-direct {v0}, Lcom/google/ac/b/c/bx;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 6895
    iget-object v0, p0, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 6896
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/t;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 6898
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    if-eqz v0, :cond_1

    .line 6899
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/t;->b:Lcom/google/ac/b/c/u;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 6901
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    if-eqz v0, :cond_2

    .line 6902
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/t;->c:Lcom/google/ac/b/c/r;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 6904
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    if-eqz v0, :cond_3

    .line 6905
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/t;->d:Lcom/google/ac/b/c/ai;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 6907
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/t;->e:Lcom/google/ac/b/c/s;

    if-eqz v0, :cond_4

    .line 6908
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/t;->e:Lcom/google/ac/b/c/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 6910
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    if-eqz v0, :cond_5

    .line 6911
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/b/c/t;->f:Lcom/google/ac/b/c/bx;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 6913
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 6914
    return-void
.end method
