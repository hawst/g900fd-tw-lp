.class public final Lcom/google/ac/b/c/i;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/c/o;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 655
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 656
    iput-object v0, p0, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    iput-object v0, p0, Lcom/google/ac/b/c/i;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/i;->cachedSize:I

    .line 657
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 707
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 708
    iget-object v1, p0, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    if-eqz v1, :cond_0

    .line 709
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 712
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 668
    if-ne p1, p0, :cond_1

    .line 669
    const/4 v0, 0x1

    .line 684
    :cond_0
    :goto_0
    return v0

    .line 671
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/i;

    if-eqz v1, :cond_0

    .line 674
    check-cast p1, Lcom/google/ac/b/c/i;

    .line 675
    iget-object v1, p0, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    if-nez v1, :cond_3

    .line 676
    iget-object v1, p1, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    if-nez v1, :cond_0

    .line 684
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/i;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 680
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    iget-object v2, p1, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/o;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 689
    iget-object v0, p0, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 692
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/i;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 693
    return v0

    .line 689
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    invoke-virtual {v0}, Lcom/google/ac/b/c/o;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 635
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/i;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/o;

    invoke-direct {v0}, Lcom/google/ac/b/c/o;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 699
    iget-object v0, p0, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    if-eqz v0, :cond_0

    .line 700
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/i;->a:Lcom/google/ac/b/c/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 702
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 703
    return-void
.end method
