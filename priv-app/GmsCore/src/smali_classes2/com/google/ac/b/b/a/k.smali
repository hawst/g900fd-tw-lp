.class public final Lcom/google/ac/b/b/a/k;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 827
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 828
    iput-object v0, p0, Lcom/google/ac/b/b/a/k;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/b/a/k;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/b/a/k;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/b/a/k;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/b/a/k;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/k;->cachedSize:I

    .line 829
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 914
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 915
    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 916
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/k;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 919
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 920
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/b/a/k;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 923
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 924
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/b/a/k;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 927
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 928
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/b/a/k;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 931
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 843
    if-ne p1, p0, :cond_1

    .line 844
    const/4 v0, 0x1

    .line 877
    :cond_0
    :goto_0
    return v0

    .line 846
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/k;

    if-eqz v1, :cond_0

    .line 849
    check-cast p1, Lcom/google/ac/b/b/a/k;

    .line 850
    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->a:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 851
    iget-object v1, p1, Lcom/google/ac/b/b/a/k;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 857
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->b:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 858
    iget-object v1, p1, Lcom/google/ac/b/b/a/k;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 864
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->c:Ljava/lang/Integer;

    if-nez v1, :cond_8

    .line 865
    iget-object v1, p1, Lcom/google/ac/b/b/a/k;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 870
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->d:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 871
    iget-object v1, p1, Lcom/google/ac/b/b/a/k;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 877
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/k;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 854
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/b/a/k;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 861
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/b/a/k;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 868
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/b/a/k;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 874
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/b/a/k;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 882
    iget-object v0, p0, Lcom/google/ac/b/b/a/k;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 885
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/k;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 887
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/k;->c:Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 888
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/b/a/k;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 890
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/k;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 891
    return v0

    .line 882
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/k;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 885
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/k;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 887
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/k;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_2

    .line 888
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 798
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/k;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/k;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/k;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/k;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/k;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 897
    iget-object v0, p0, Lcom/google/ac/b/b/a/k;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 898
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 900
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/k;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 901
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 903
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/k;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 904
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 906
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/k;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 907
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/b/a/k;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 909
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 910
    return-void
.end method
