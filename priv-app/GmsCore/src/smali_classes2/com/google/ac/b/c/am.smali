.class public final Lcom/google/ac/b/c/am;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/ac/b/c/am;


# instance fields
.field public a:Lcom/google/ac/b/c/al;

.field public b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2967
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2968
    iput-object v0, p0, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    iput-object v0, p0, Lcom/google/ac/b/c/am;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/am;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/am;->cachedSize:I

    .line 2969
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/am;
    .locals 2

    .prologue
    .line 2950
    sget-object v0, Lcom/google/ac/b/c/am;->c:[Lcom/google/ac/b/c/am;

    if-nez v0, :cond_1

    .line 2951
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2953
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/am;->c:[Lcom/google/ac/b/c/am;

    if-nez v0, :cond_0

    .line 2954
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/am;

    sput-object v0, Lcom/google/ac/b/c/am;->c:[Lcom/google/ac/b/c/am;

    .line 2956
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2958
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/am;->c:[Lcom/google/ac/b/c/am;

    return-object v0

    .line 2956
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3030
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3031
    iget-object v1, p0, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    if-eqz v1, :cond_0

    .line 3032
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3035
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/am;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 3036
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/am;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3039
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2981
    if-ne p1, p0, :cond_1

    .line 2982
    const/4 v0, 0x1

    .line 3003
    :cond_0
    :goto_0
    return v0

    .line 2984
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/am;

    if-eqz v1, :cond_0

    .line 2987
    check-cast p1, Lcom/google/ac/b/c/am;

    .line 2988
    iget-object v1, p0, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    if-nez v1, :cond_4

    .line 2989
    iget-object v1, p1, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    if-nez v1, :cond_0

    .line 2997
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/am;->b:Ljava/lang/Integer;

    if-nez v1, :cond_5

    .line 2998
    iget-object v1, p1, Lcom/google/ac/b/c/am;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3003
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/am;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2993
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    iget-object v2, p1, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/al;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 3001
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/am;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/am;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3008
    iget-object v0, p0, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3011
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/am;->b:Ljava/lang/Integer;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 3012
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/am;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3013
    return v0

    .line 3008
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    invoke-virtual {v0}, Lcom/google/ac/b/c/al;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3011
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/am;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2944
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/am;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/al;

    invoke-direct {v0}, Lcom/google/ac/b/c/al;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/am;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3019
    iget-object v0, p0, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    if-eqz v0, :cond_0

    .line 3020
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/am;->a:Lcom/google/ac/b/c/al;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3022
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/am;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3023
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/am;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3025
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3026
    return-void
.end method
