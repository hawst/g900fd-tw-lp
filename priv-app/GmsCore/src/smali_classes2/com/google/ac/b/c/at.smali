.class public final Lcom/google/ac/b/c/at;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/c/av;

.field public b:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8875
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 8876
    iput-object v0, p0, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    iput-object v0, p0, Lcom/google/ac/b/c/at;->b:[B

    iput-object v0, p0, Lcom/google/ac/b/c/at;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/at;->cachedSize:I

    .line 8877
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 8935
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 8936
    iget-object v1, p0, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    if-eqz v1, :cond_0

    .line 8937
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8940
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/at;->b:[B

    if-eqz v1, :cond_1

    .line 8941
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/at;->b:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 8944
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 8889
    if-ne p1, p0, :cond_1

    .line 8890
    const/4 v0, 0x1

    .line 8908
    :cond_0
    :goto_0
    return v0

    .line 8892
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/at;

    if-eqz v1, :cond_0

    .line 8895
    check-cast p1, Lcom/google/ac/b/c/at;

    .line 8896
    iget-object v1, p0, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    if-nez v1, :cond_3

    .line 8897
    iget-object v1, p1, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    if-nez v1, :cond_0

    .line 8905
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/at;->b:[B

    iget-object v2, p1, Lcom/google/ac/b/c/at;->b:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8908
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/at;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 8901
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    iget-object v2, p1, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/av;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 8913
    iget-object v0, p0, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 8916
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/at;->b:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    .line 8917
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/at;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 8918
    return v0

    .line 8913
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    invoke-virtual {v0}, Lcom/google/ac/b/c/av;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8852
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/at;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/av;

    invoke-direct {v0}, Lcom/google/ac/b/c/av;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/at;->b:[B

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 8924
    iget-object v0, p0, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    if-eqz v0, :cond_0

    .line 8925
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/at;->a:Lcom/google/ac/b/c/av;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 8927
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/at;->b:[B

    if-eqz v0, :cond_1

    .line 8928
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/at;->b:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 8930
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8931
    return-void
.end method
