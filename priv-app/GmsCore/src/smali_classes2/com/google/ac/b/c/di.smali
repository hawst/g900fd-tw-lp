.class public final Lcom/google/ac/b/c/di;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/ac/b/c/di;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:[I

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 887
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 888
    iput-object v1, p0, Lcom/google/ac/b/c/di;->a:Ljava/lang/Integer;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/ac/b/c/di;->b:[I

    iput-object v1, p0, Lcom/google/ac/b/c/di;->c:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/ac/b/c/di;->d:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/ac/b/c/di;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/di;->cachedSize:I

    .line 889
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/di;
    .locals 2

    .prologue
    .line 864
    sget-object v0, Lcom/google/ac/b/c/di;->e:[Lcom/google/ac/b/c/di;

    if-nez v0, :cond_1

    .line 865
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 867
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/di;->e:[Lcom/google/ac/b/c/di;

    if-nez v0, :cond_0

    .line 868
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/di;

    sput-object v0, Lcom/google/ac/b/c/di;->e:[Lcom/google/ac/b/c/di;

    .line 870
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 872
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/di;->e:[Lcom/google/ac/b/c/di;

    return-object v0

    .line 870
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 973
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v2

    .line 974
    iget-object v1, p0, Lcom/google/ac/b/c/di;->b:[I

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/ac/b/c/di;->b:[I

    array-length v1, v1

    if-lez v1, :cond_4

    move v1, v0

    .line 976
    :goto_0
    iget-object v3, p0, Lcom/google/ac/b/c/di;->b:[I

    array-length v3, v3

    if-ge v0, v3, :cond_0

    .line 977
    iget-object v3, p0, Lcom/google/ac/b/c/di;->b:[I

    aget v3, v3, v0

    .line 978
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 976
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 981
    :cond_0
    add-int v0, v2, v1

    .line 982
    iget-object v1, p0, Lcom/google/ac/b/c/di;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 984
    :goto_1
    iget-object v1, p0, Lcom/google/ac/b/c/di;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 985
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/di;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 988
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/di;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 989
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/di;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 992
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/di;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 993
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/di;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 996
    :cond_3
    return v0

    :cond_4
    move v0, v2

    goto :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 903
    if-ne p1, p0, :cond_1

    .line 904
    const/4 v0, 0x1

    .line 934
    :cond_0
    :goto_0
    return v0

    .line 906
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/di;

    if-eqz v1, :cond_0

    .line 909
    check-cast p1, Lcom/google/ac/b/c/di;

    .line 910
    iget-object v1, p0, Lcom/google/ac/b/c/di;->a:Ljava/lang/Integer;

    if-nez v1, :cond_5

    .line 911
    iget-object v1, p1, Lcom/google/ac/b/c/di;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 916
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/di;->b:[I

    iget-object v2, p1, Lcom/google/ac/b/c/di;->b:[I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 920
    iget-object v1, p0, Lcom/google/ac/b/c/di;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_6

    .line 921
    iget-object v1, p1, Lcom/google/ac/b/c/di;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 927
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/di;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_7

    .line 928
    iget-object v1, p1, Lcom/google/ac/b/c/di;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 934
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/di;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 914
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/di;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/di;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 924
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/di;->c:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/di;->c:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 931
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/di;->d:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/di;->d:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 939
    iget-object v0, p0, Lcom/google/ac/b/c/di;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 941
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/di;->b:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v2

    add-int/2addr v0, v2

    .line 943
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/di;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 945
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/di;->d:Ljava/lang/Boolean;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 947
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/di;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 948
    return v0

    .line 939
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/di;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 943
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/di;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_1

    .line 945
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/di;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 858
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/di;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_0
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/ac/b/c/di;->b:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lcom/google/ac/b/c/di;->b:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/di;->b:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/google/ac/b/c/di;->b:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/ac/b/c/di;->b:[I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_1

    goto :goto_4

    :pswitch_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/ac/b/c/di;->b:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/ac/b/c/di;->b:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_2

    goto :goto_6

    :pswitch_2
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/di;->b:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lcom/google/ac/b/c/di;->b:[I

    :cond_a
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/di;->c:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/di;->d:Ljava/lang/Boolean;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_3

    goto/16 :goto_0

    :pswitch_3
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/di;->a:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0xa -> :sswitch_2
        0x10 -> :sswitch_3
        0x18 -> :sswitch_4
        0x20 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 954
    iget-object v0, p0, Lcom/google/ac/b/c/di;->b:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/ac/b/c/di;->b:[I

    array-length v0, v0

    if-lez v0, :cond_0

    .line 955
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/c/di;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 956
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/di;->b:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 955
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 959
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/di;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 960
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/di;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 962
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/di;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 963
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/di;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 965
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/di;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 966
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/di;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 968
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 969
    return-void
.end method
