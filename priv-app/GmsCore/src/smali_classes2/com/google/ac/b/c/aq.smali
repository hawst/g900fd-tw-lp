.class public final Lcom/google/ac/b/c/aq;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11751
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 11752
    iput-object v0, p0, Lcom/google/ac/b/c/aq;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/aq;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/aq;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/aq;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/aq;->cachedSize:I

    .line 11753
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 11827
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 11828
    iget-object v1, p0, Lcom/google/ac/b/c/aq;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 11829
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/aq;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11832
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/aq;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 11833
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/aq;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11836
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/aq;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 11837
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/aq;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11840
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 11766
    if-ne p1, p0, :cond_1

    .line 11767
    const/4 v0, 0x1

    .line 11794
    :cond_0
    :goto_0
    return v0

    .line 11769
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/aq;

    if-eqz v1, :cond_0

    .line 11772
    check-cast p1, Lcom/google/ac/b/c/aq;

    .line 11773
    iget-object v1, p0, Lcom/google/ac/b/c/aq;->a:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 11774
    iget-object v1, p1, Lcom/google/ac/b/c/aq;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 11780
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/aq;->b:Ljava/lang/Integer;

    if-nez v1, :cond_6

    .line 11781
    iget-object v1, p1, Lcom/google/ac/b/c/aq;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 11787
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/aq;->c:Ljava/lang/Integer;

    if-nez v1, :cond_7

    .line 11788
    iget-object v1, p1, Lcom/google/ac/b/c/aq;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 11794
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/aq;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 11777
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/aq;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/aq;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 11784
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/aq;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/aq;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 11791
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/aq;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/aq;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 11799
    iget-object v0, p0, Lcom/google/ac/b/c/aq;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 11802
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/aq;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 11804
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/aq;->c:Ljava/lang/Integer;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 11806
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/aq;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 11807
    return v0

    .line 11799
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/aq;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 11802
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/aq;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_1

    .line 11804
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/aq;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 11725
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/aq;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/aq;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/aq;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/aq;->c:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 11813
    iget-object v0, p0, Lcom/google/ac/b/c/aq;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 11814
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/aq;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 11816
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/aq;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 11817
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/aq;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 11819
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/aq;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 11820
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/aq;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 11822
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 11823
    return-void
.end method
