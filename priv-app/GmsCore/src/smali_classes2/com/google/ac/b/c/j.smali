.class public final Lcom/google/ac/b/c/j;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Double;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Double;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2800
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2801
    iput-object v0, p0, Lcom/google/ac/b/c/j;->a:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/ac/b/c/j;->b:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/j;->c:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/ac/b/c/j;->d:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/j;->e:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/j;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/j;->cachedSize:I

    .line 2802
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 2902
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2903
    iget-object v1, p0, Lcom/google/ac/b/c/j;->a:Ljava/lang/Double;

    if-eqz v1, :cond_0

    .line 2904
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/j;->a:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 2907
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/j;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 2908
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/j;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2911
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/j;->c:Ljava/lang/Double;

    if-eqz v1, :cond_2

    .line 2912
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/j;->c:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 2915
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/j;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 2916
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/j;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2919
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/j;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 2920
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/j;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2923
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2817
    if-ne p1, p0, :cond_1

    .line 2818
    const/4 v0, 0x1

    .line 2859
    :cond_0
    :goto_0
    return v0

    .line 2820
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/j;

    if-eqz v1, :cond_0

    .line 2823
    check-cast p1, Lcom/google/ac/b/c/j;

    .line 2824
    iget-object v1, p0, Lcom/google/ac/b/c/j;->a:Ljava/lang/Double;

    if-nez v1, :cond_7

    .line 2825
    iget-object v1, p1, Lcom/google/ac/b/c/j;->a:Ljava/lang/Double;

    if-nez v1, :cond_0

    .line 2831
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/j;->b:Ljava/lang/Long;

    if-nez v1, :cond_8

    .line 2832
    iget-object v1, p1, Lcom/google/ac/b/c/j;->b:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 2838
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/j;->c:Ljava/lang/Double;

    if-nez v1, :cond_9

    .line 2839
    iget-object v1, p1, Lcom/google/ac/b/c/j;->c:Ljava/lang/Double;

    if-nez v1, :cond_0

    .line 2845
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/j;->d:Ljava/lang/Long;

    if-nez v1, :cond_a

    .line 2846
    iget-object v1, p1, Lcom/google/ac/b/c/j;->d:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 2852
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/j;->e:Ljava/lang/Long;

    if-nez v1, :cond_b

    .line 2853
    iget-object v1, p1, Lcom/google/ac/b/c/j;->e:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 2859
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/j;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2828
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/j;->a:Ljava/lang/Double;

    iget-object v2, p1, Lcom/google/ac/b/c/j;->a:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2835
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/j;->b:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/j;->b:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 2842
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/j;->c:Ljava/lang/Double;

    iget-object v2, p1, Lcom/google/ac/b/c/j;->c:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 2849
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/j;->d:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/j;->d:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 2856
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/j;->e:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/j;->e:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2864
    iget-object v0, p0, Lcom/google/ac/b/c/j;->a:Ljava/lang/Double;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2867
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/j;->b:Ljava/lang/Long;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2869
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/j;->c:Ljava/lang/Double;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2871
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/j;->d:Ljava/lang/Long;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 2873
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/j;->e:Ljava/lang/Long;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 2875
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/j;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2876
    return v0

    .line 2864
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/j;->a:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2867
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/j;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2869
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/j;->c:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2871
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/j;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_3

    .line 2873
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/j;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 2768
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/j;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/j;->a:Ljava/lang/Double;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/j;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/j;->c:Ljava/lang/Double;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/j;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/j;->e:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x9 -> :sswitch_1
        0x10 -> :sswitch_2
        0x19 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 2882
    iget-object v0, p0, Lcom/google/ac/b/c/j;->a:Ljava/lang/Double;

    if-eqz v0, :cond_0

    .line 2883
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/j;->a:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 2885
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/j;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2886
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/j;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2888
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/j;->c:Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 2889
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/j;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 2891
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/j;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2892
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/j;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2894
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/j;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 2895
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/j;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2897
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2898
    return-void
.end method
