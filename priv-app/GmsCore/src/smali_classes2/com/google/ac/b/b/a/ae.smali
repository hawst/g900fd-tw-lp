.class public final Lcom/google/ac/b/b/a/ae;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4008
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 4009
    iput-object v0, p0, Lcom/google/ac/b/b/a/ae;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/b/a/ae;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/ae;->cachedSize:I

    .line 4010
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 4056
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 4057
    iget-object v1, p0, Lcom/google/ac/b/b/a/ae;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 4058
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/ae;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4061
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4021
    if-ne p1, p0, :cond_1

    .line 4022
    const/4 v0, 0x1

    .line 4034
    :cond_0
    :goto_0
    return v0

    .line 4024
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/ae;

    if-eqz v1, :cond_0

    .line 4027
    check-cast p1, Lcom/google/ac/b/b/a/ae;

    .line 4028
    iget-object v1, p0, Lcom/google/ac/b/b/a/ae;->a:Ljava/lang/Integer;

    if-nez v1, :cond_3

    .line 4029
    iget-object v1, p1, Lcom/google/ac/b/b/a/ae;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 4034
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/ae;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 4032
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/ae;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ae;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 4039
    iget-object v0, p0, Lcom/google/ac/b/b/a/ae;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4041
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/ae;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4042
    return v0

    .line 4039
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/ae;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3988
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/ae;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/ae;->a:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x1 -> :sswitch_2
        0x0 -> :sswitch_2
        0xc9 -> :sswitch_2
        0xca -> :sswitch_2
        0xcb -> :sswitch_2
        0xcc -> :sswitch_2
        0x12d -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4048
    iget-object v0, p0, Lcom/google/ac/b/b/a/ae;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 4049
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/ae;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4051
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4052
    return-void
.end method
