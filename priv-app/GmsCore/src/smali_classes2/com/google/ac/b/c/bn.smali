.class public final Lcom/google/ac/b/c/bn;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Integer;

.field public g:Lcom/google/ac/b/c/bs;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8400
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 8401
    iput-object v0, p0, Lcom/google/ac/b/c/bn;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/bn;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/bn;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/bn;->d:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/bn;->e:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/bn;->f:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/bn;->g:Lcom/google/ac/b/c/bs;

    iput-object v0, p0, Lcom/google/ac/b/c/bn;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bn;->cachedSize:I

    .line 8402
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 8528
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 8529
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 8530
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/bn;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8533
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 8534
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/bn;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8537
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 8538
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/bn;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8541
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 8542
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/bn;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8545
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 8546
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/bn;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8549
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 8550
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/bn;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8553
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->g:Lcom/google/ac/b/c/bs;

    if-eqz v1, :cond_6

    .line 8554
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/b/c/bn;->g:Lcom/google/ac/b/c/bs;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8557
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 8419
    if-ne p1, p0, :cond_1

    .line 8420
    const/4 v0, 0x1

    .line 8476
    :cond_0
    :goto_0
    return v0

    .line 8422
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bn;

    if-eqz v1, :cond_0

    .line 8425
    check-cast p1, Lcom/google/ac/b/c/bn;

    .line 8426
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->a:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 8427
    iget-object v1, p1, Lcom/google/ac/b/c/bn;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 8433
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->b:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 8434
    iget-object v1, p1, Lcom/google/ac/b/c/bn;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 8440
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->c:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 8441
    iget-object v1, p1, Lcom/google/ac/b/c/bn;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 8447
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->d:Ljava/lang/Long;

    if-nez v1, :cond_c

    .line 8448
    iget-object v1, p1, Lcom/google/ac/b/c/bn;->d:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 8454
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->e:Ljava/lang/Long;

    if-nez v1, :cond_d

    .line 8455
    iget-object v1, p1, Lcom/google/ac/b/c/bn;->e:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 8461
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->f:Ljava/lang/Integer;

    if-nez v1, :cond_e

    .line 8462
    iget-object v1, p1, Lcom/google/ac/b/c/bn;->f:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 8467
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->g:Lcom/google/ac/b/c/bs;

    if-nez v1, :cond_f

    .line 8468
    iget-object v1, p1, Lcom/google/ac/b/c/bn;->g:Lcom/google/ac/b/c/bs;

    if-nez v1, :cond_0

    .line 8476
    :cond_8
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bn;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 8430
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/bn;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 8437
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/bn;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 8444
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/bn;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 8451
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->d:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/bn;->d:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 8458
    :cond_d
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->e:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/bn;->e:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 8465
    :cond_e
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->f:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/bn;->f:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 8472
    :cond_f
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->g:Lcom/google/ac/b/c/bs;

    iget-object v2, p1, Lcom/google/ac/b/c/bn;->g:Lcom/google/ac/b/c/bs;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bs;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 8481
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 8484
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bn;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 8486
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bn;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 8488
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bn;->d:Ljava/lang/Long;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 8490
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bn;->e:Ljava/lang/Long;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 8492
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bn;->f:Ljava/lang/Integer;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 8493
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/bn;->g:Lcom/google/ac/b/c/bs;

    if-nez v2, :cond_6

    :goto_6
    add-int/2addr v0, v1

    .line 8495
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bn;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 8496
    return v0

    .line 8481
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 8484
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 8486
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 8488
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_3

    .line 8490
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_4

    .line 8492
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_5

    .line 8493
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/bn;->g:Lcom/google/ac/b/c/bs;

    invoke-virtual {v1}, Lcom/google/ac/b/c/bs;->hashCode()I

    move-result v1

    goto :goto_6
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 8362
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bn;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bn;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bn;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bn;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bn;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bn;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bn;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->g:Lcom/google/ac/b/c/bs;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/bs;

    invoke-direct {v0}, Lcom/google/ac/b/c/bs;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bn;->g:Lcom/google/ac/b/c/bs;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->g:Lcom/google/ac/b/c/bs;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 8502
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 8503
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/bn;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8505
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 8506
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/bn;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8508
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 8509
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/bn;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8511
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 8512
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/bn;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 8514
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 8515
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/bn;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 8517
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 8518
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/bn;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8520
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/bn;->g:Lcom/google/ac/b/c/bs;

    if-eqz v0, :cond_6

    .line 8521
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/b/c/bn;->g:Lcom/google/ac/b/c/bs;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 8523
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8524
    return-void
.end method
