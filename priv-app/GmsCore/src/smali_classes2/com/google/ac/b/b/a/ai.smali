.class public final Lcom/google/ac/b/b/a/ai;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/ac/b/b/a/ai;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5109
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 5110
    iput-object v0, p0, Lcom/google/ac/b/b/a/ai;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/b/a/ai;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/b/a/ai;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/ai;->cachedSize:I

    .line 5111
    return-void
.end method

.method public static a()[Lcom/google/ac/b/b/a/ai;
    .locals 2

    .prologue
    .line 5092
    sget-object v0, Lcom/google/ac/b/b/a/ai;->c:[Lcom/google/ac/b/b/a/ai;

    if-nez v0, :cond_1

    .line 5093
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 5095
    :try_start_0
    sget-object v0, Lcom/google/ac/b/b/a/ai;->c:[Lcom/google/ac/b/b/a/ai;

    if-nez v0, :cond_0

    .line 5096
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/b/a/ai;

    sput-object v0, Lcom/google/ac/b/b/a/ai;->c:[Lcom/google/ac/b/b/a/ai;

    .line 5098
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5100
    :cond_1
    sget-object v0, Lcom/google/ac/b/b/a/ai;->c:[Lcom/google/ac/b/b/a/ai;

    return-object v0

    .line 5098
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5170
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 5171
    iget-object v1, p0, Lcom/google/ac/b/b/a/ai;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5172
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/ai;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5175
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/ai;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 5176
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/b/a/ai;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5179
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 5123
    if-ne p1, p0, :cond_1

    .line 5124
    const/4 v0, 0x1

    .line 5143
    :cond_0
    :goto_0
    return v0

    .line 5126
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/ai;

    if-eqz v1, :cond_0

    .line 5129
    check-cast p1, Lcom/google/ac/b/b/a/ai;

    .line 5130
    iget-object v1, p0, Lcom/google/ac/b/b/a/ai;->a:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 5131
    iget-object v1, p1, Lcom/google/ac/b/b/a/ai;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5137
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/ai;->b:Ljava/lang/Integer;

    if-nez v1, :cond_5

    .line 5138
    iget-object v1, p1, Lcom/google/ac/b/b/a/ai;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 5143
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/ai;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 5134
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/b/a/ai;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ai;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 5141
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/b/a/ai;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ai;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5148
    iget-object v0, p0, Lcom/google/ac/b/b/a/ai;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 5151
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/b/a/ai;->b:Ljava/lang/Integer;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 5152
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/ai;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 5153
    return v0

    .line 5148
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/ai;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5151
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/b/a/ai;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5086
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/ai;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/ai;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/ai;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5159
    iget-object v0, p0, Lcom/google/ac/b/b/a/ai;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 5160
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/ai;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5162
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/ai;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 5163
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/b/a/ai;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5165
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5166
    return-void
.end method
