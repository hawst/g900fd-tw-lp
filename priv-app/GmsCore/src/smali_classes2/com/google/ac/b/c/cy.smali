.class public final Lcom/google/ac/b/c/cy;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/c/dc;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 768
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 769
    iput-object v0, p0, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    iput-object v0, p0, Lcom/google/ac/b/c/cy;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/cy;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/cy;->cachedSize:I

    .line 770
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 833
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 834
    iget-object v1, p0, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    if-eqz v1, :cond_0

    .line 835
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 838
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/cy;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 839
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/cy;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 842
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 782
    if-ne p1, p0, :cond_1

    .line 783
    const/4 v0, 0x1

    .line 805
    :cond_0
    :goto_0
    return v0

    .line 785
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/cy;

    if-eqz v1, :cond_0

    .line 788
    check-cast p1, Lcom/google/ac/b/c/cy;

    .line 789
    iget-object v1, p0, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    if-nez v1, :cond_4

    .line 790
    iget-object v1, p1, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    if-nez v1, :cond_0

    .line 798
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/cy;->b:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 799
    iget-object v1, p1, Lcom/google/ac/b/c/cy;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 805
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/cy;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 794
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    iget-object v2, p1, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/dc;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 802
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/cy;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/cy;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 810
    iget-object v0, p0, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 813
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/cy;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 815
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/cy;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 816
    return v0

    .line 810
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    invoke-virtual {v0}, Lcom/google/ac/b/c/dc;->hashCode()I

    move-result v0

    goto :goto_0

    .line 813
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/cy;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 745
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/cy;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/dc;

    invoke-direct {v0}, Lcom/google/ac/b/c/dc;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cy;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 822
    iget-object v0, p0, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    if-eqz v0, :cond_0

    .line 823
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/cy;->a:Lcom/google/ac/b/c/dc;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 825
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cy;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 826
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/cy;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 828
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 829
    return-void
.end method
