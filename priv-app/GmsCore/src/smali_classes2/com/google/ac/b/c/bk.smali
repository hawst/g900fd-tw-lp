.class public final Lcom/google/ac/b/c/bk;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/c/z;

.field public b:[Lcom/google/ac/b/c/az;

.field public c:[Lcom/google/ac/b/c/ao;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1734
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1735
    iput-object v1, p0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    invoke-static {}, Lcom/google/ac/b/c/az;->a()[Lcom/google/ac/b/c/az;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    invoke-static {}, Lcom/google/ac/b/c/ao;->a()[Lcom/google/ac/b/c/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    iput-object v1, p0, Lcom/google/ac/b/c/bk;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bk;->cachedSize:I

    .line 1736
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1816
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1817
    iget-object v2, p0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    if-eqz v2, :cond_0

    .line 1818
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1821
    :cond_0
    iget-object v2, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 1822
    :goto_0
    iget-object v3, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 1823
    iget-object v3, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    aget-object v3, v3, v0

    .line 1824
    if-eqz v3, :cond_1

    .line 1825
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1822
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1830
    :cond_3
    iget-object v2, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 1831
    :goto_1
    iget-object v2, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 1832
    iget-object v2, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    aget-object v2, v2, v1

    .line 1833
    if-eqz v2, :cond_4

    .line 1834
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1831
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1839
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1749
    if-ne p1, p0, :cond_1

    .line 1750
    const/4 v0, 0x1

    .line 1773
    :cond_0
    :goto_0
    return v0

    .line 1752
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bk;

    if-eqz v1, :cond_0

    .line 1755
    check-cast p1, Lcom/google/ac/b/c/bk;

    .line 1756
    iget-object v1, p0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    if-nez v1, :cond_3

    .line 1757
    iget-object v1, p1, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    if-nez v1, :cond_0

    .line 1765
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    iget-object v2, p1, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1769
    iget-object v1, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    iget-object v2, p1, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1773
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bk;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1761
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    iget-object v2, p1, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/z;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1778
    iget-object v0, p0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1781
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1783
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1785
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bk;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1786
    return v0

    .line 1778
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    invoke-virtual {v0}, Lcom/google/ac/b/c/z;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1708
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bk;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/z;

    invoke-direct {v0}, Lcom/google/ac/b/c/z;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/az;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/ac/b/c/az;

    invoke-direct {v3}, Lcom/google/ac/b/c/az;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/ac/b/c/az;

    invoke-direct {v3}, Lcom/google/ac/b/c/az;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/ao;

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Lcom/google/ac/b/c/ao;

    invoke-direct {v3}, Lcom/google/ac/b/c/ao;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    array-length v0, v0

    goto :goto_3

    :cond_7
    new-instance v3, Lcom/google/ac/b/c/ao;

    invoke-direct {v3}, Lcom/google/ac/b/c/ao;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1792
    iget-object v0, p0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    if-eqz v0, :cond_0

    .line 1793
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/bk;->a:Lcom/google/ac/b/c/z;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1795
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 1796
    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1797
    iget-object v2, p0, Lcom/google/ac/b/c/bk;->b:[Lcom/google/ac/b/c/az;

    aget-object v2, v2, v0

    .line 1798
    if-eqz v2, :cond_1

    .line 1799
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1796
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1803
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 1804
    :goto_1
    iget-object v0, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 1805
    iget-object v0, p0, Lcom/google/ac/b/c/bk;->c:[Lcom/google/ac/b/c/ao;

    aget-object v0, v0, v1

    .line 1806
    if-eqz v0, :cond_3

    .line 1807
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1804
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1811
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1812
    return-void
.end method
