.class public final Lcom/google/ac/b/c/ab;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 9763
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 9764
    iput-object v1, p0, Lcom/google/ac/b/c/ab;->a:Ljava/lang/Integer;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/ac/b/c/ab;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/ab;->cachedSize:I

    .line 9765
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 9826
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 9827
    iget-object v2, p0, Lcom/google/ac/b/c/ab;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 9828
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/ac/b/c/ab;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 9831
    :cond_0
    iget-object v2, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    move v3, v1

    .line 9834
    :goto_0
    iget-object v4, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 9835
    iget-object v4, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 9836
    if-eqz v4, :cond_1

    .line 9837
    add-int/lit8 v3, v3, 0x1

    .line 9838
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 9834
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 9842
    :cond_2
    add-int/2addr v0, v2

    .line 9843
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 9845
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 9777
    if-ne p1, p0, :cond_1

    .line 9778
    const/4 v0, 0x1

    .line 9794
    :cond_0
    :goto_0
    return v0

    .line 9780
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/ab;

    if-eqz v1, :cond_0

    .line 9783
    check-cast p1, Lcom/google/ac/b/c/ab;

    .line 9784
    iget-object v1, p0, Lcom/google/ac/b/c/ab;->a:Ljava/lang/Integer;

    if-nez v1, :cond_3

    .line 9785
    iget-object v1, p1, Lcom/google/ac/b/c/ab;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 9790
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 9794
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/ab;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 9788
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ab;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/ab;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 9799
    iget-object v0, p0, Lcom/google/ac/b/c/ab;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 9801
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9803
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/ab;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 9804
    return v0

    .line 9799
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ab;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9740
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/ab;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ab;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 9810
    iget-object v0, p0, Lcom/google/ac/b/c/ab;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 9811
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/ab;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 9813
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 9814
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 9815
    iget-object v1, p0, Lcom/google/ac/b/c/ab;->b:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 9816
    if-eqz v1, :cond_1

    .line 9817
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 9814
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9821
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 9822
    return-void
.end method
