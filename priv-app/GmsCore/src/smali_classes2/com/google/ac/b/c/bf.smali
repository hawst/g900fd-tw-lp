.class public final Lcom/google/ac/b/c/bf;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile g:[Lcom/google/ac/b/c/bf;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/ac/b/c/t;

.field public c:Lcom/google/ac/b/c/at;

.field public d:Ljava/lang/Integer;

.field public e:Lcom/google/ac/b/c/bs;

.field public f:Lcom/google/ac/b/c/bb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7959
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 7960
    iput-object v0, p0, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    iput-object v0, p0, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    iput-object v0, p0, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    iput-object v0, p0, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    iput-object v0, p0, Lcom/google/ac/b/c/bf;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bf;->cachedSize:I

    .line 7961
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/bf;
    .locals 2

    .prologue
    .line 7930
    sget-object v0, Lcom/google/ac/b/c/bf;->g:[Lcom/google/ac/b/c/bf;

    if-nez v0, :cond_1

    .line 7931
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 7933
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/bf;->g:[Lcom/google/ac/b/c/bf;

    if-nez v0, :cond_0

    .line 7934
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/bf;

    sput-object v0, Lcom/google/ac/b/c/bf;->g:[Lcom/google/ac/b/c/bf;

    .line 7936
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7938
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/bf;->g:[Lcom/google/ac/b/c/bf;

    return-object v0

    .line 7936
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 8080
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 8081
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 8082
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8085
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    if-eqz v1, :cond_1

    .line 8086
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8089
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    if-eqz v1, :cond_2

    .line 8090
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8093
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 8094
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8097
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    if-eqz v1, :cond_4

    .line 8098
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8101
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    if-eqz v1, :cond_5

    .line 8102
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8105
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 7977
    if-ne p1, p0, :cond_1

    .line 7978
    const/4 v0, 0x1

    .line 8033
    :cond_0
    :goto_0
    return v0

    .line 7980
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bf;

    if-eqz v1, :cond_0

    .line 7983
    check-cast p1, Lcom/google/ac/b/c/bf;

    .line 7984
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 7985
    iget-object v1, p1, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 7991
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    if-nez v1, :cond_9

    .line 7992
    iget-object v1, p1, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    if-nez v1, :cond_0

    .line 8000
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    if-nez v1, :cond_a

    .line 8001
    iget-object v1, p1, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    if-nez v1, :cond_0

    .line 8009
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    if-nez v1, :cond_b

    .line 8010
    iget-object v1, p1, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 8015
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    if-nez v1, :cond_c

    .line 8016
    iget-object v1, p1, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    if-nez v1, :cond_0

    .line 8024
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    if-nez v1, :cond_d

    .line 8025
    iget-object v1, p1, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    if-nez v1, :cond_0

    .line 8033
    :cond_7
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bf;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 7988
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 7996
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    iget-object v2, p1, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 8005
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    iget-object v2, p1, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/at;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 8013
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 8020
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    iget-object v2, p1, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bs;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 8029
    :cond_d
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    iget-object v2, p1, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 8038
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 8041
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 8043
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 8045
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 8046
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 8048
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 8050
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bf;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 8051
    return v0

    .line 8038
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 8041
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    invoke-virtual {v0}, Lcom/google/ac/b/c/t;->hashCode()I

    move-result v0

    goto :goto_1

    .line 8043
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    invoke-virtual {v0}, Lcom/google/ac/b/c/at;->hashCode()I

    move-result v0

    goto :goto_2

    .line 8045
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_3

    .line 8046
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    invoke-virtual {v0}, Lcom/google/ac/b/c/bs;->hashCode()I

    move-result v0

    goto :goto_4

    .line 8048
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    invoke-virtual {v1}, Lcom/google/ac/b/c/bb;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 7924
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bf;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/t;

    invoke-direct {v0}, Lcom/google/ac/b/c/t;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/at;

    invoke-direct {v0}, Lcom/google/ac/b/c/at;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/b/c/bs;

    invoke-direct {v0}, Lcom/google/ac/b/c/bs;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/c/bb;

    invoke-direct {v0}, Lcom/google/ac/b/c/bb;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 8057
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 8058
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/bf;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8060
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    if-eqz v0, :cond_1

    .line 8061
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/bf;->b:Lcom/google/ac/b/c/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 8063
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    if-eqz v0, :cond_2

    .line 8064
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/bf;->c:Lcom/google/ac/b/c/at;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 8066
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 8067
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/bf;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8069
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    if-eqz v0, :cond_4

    .line 8070
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/bf;->e:Lcom/google/ac/b/c/bs;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 8072
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    if-eqz v0, :cond_5

    .line 8073
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/bf;->f:Lcom/google/ac/b/c/bb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 8075
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8076
    return-void
.end method
