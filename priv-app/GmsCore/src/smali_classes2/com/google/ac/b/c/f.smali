.class public final Lcom/google/ac/b/c/f;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Long;

.field public h:Ljava/lang/Long;

.field public i:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3021
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3022
    iput-object v0, p0, Lcom/google/ac/b/c/f;->a:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/f;->b:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/f;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/f;->d:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/f;->e:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/f;->f:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/f;->g:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/f;->h:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/f;->i:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/f;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/f;->cachedSize:I

    .line 3023
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 3175
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3176
    iget-object v1, p0, Lcom/google/ac/b/c/f;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 3177
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/f;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3180
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/f;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 3181
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/f;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3184
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/f;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 3185
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/f;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3188
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/f;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 3189
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/f;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3192
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/f;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 3193
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/f;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3196
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/f;->f:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 3197
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/f;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3200
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/f;->g:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 3201
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/b/c/f;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3204
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/f;->h:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 3205
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/ac/b/c/f;->h:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3208
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/f;->i:Ljava/lang/Long;

    if-eqz v1, :cond_8

    .line 3209
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/ac/b/c/f;->i:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3212
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3042
    if-ne p1, p0, :cond_1

    .line 3043
    const/4 v0, 0x1

    .line 3112
    :cond_0
    :goto_0
    return v0

    .line 3045
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/f;

    if-eqz v1, :cond_0

    .line 3048
    check-cast p1, Lcom/google/ac/b/c/f;

    .line 3049
    iget-object v1, p0, Lcom/google/ac/b/c/f;->a:Ljava/lang/Long;

    if-nez v1, :cond_b

    .line 3050
    iget-object v1, p1, Lcom/google/ac/b/c/f;->a:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 3056
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/f;->b:Ljava/lang/Long;

    if-nez v1, :cond_c

    .line 3057
    iget-object v1, p1, Lcom/google/ac/b/c/f;->b:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 3063
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/f;->c:Ljava/lang/Long;

    if-nez v1, :cond_d

    .line 3064
    iget-object v1, p1, Lcom/google/ac/b/c/f;->c:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 3070
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/f;->d:Ljava/lang/Long;

    if-nez v1, :cond_e

    .line 3071
    iget-object v1, p1, Lcom/google/ac/b/c/f;->d:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 3077
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/f;->e:Ljava/lang/Long;

    if-nez v1, :cond_f

    .line 3078
    iget-object v1, p1, Lcom/google/ac/b/c/f;->e:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 3084
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/f;->f:Ljava/lang/Long;

    if-nez v1, :cond_10

    .line 3085
    iget-object v1, p1, Lcom/google/ac/b/c/f;->f:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 3091
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/f;->g:Ljava/lang/Long;

    if-nez v1, :cond_11

    .line 3092
    iget-object v1, p1, Lcom/google/ac/b/c/f;->g:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 3098
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/f;->h:Ljava/lang/Long;

    if-nez v1, :cond_12

    .line 3099
    iget-object v1, p1, Lcom/google/ac/b/c/f;->h:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 3105
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/f;->i:Ljava/lang/Long;

    if-nez v1, :cond_13

    .line 3106
    iget-object v1, p1, Lcom/google/ac/b/c/f;->i:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 3112
    :cond_a
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/f;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 3053
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/f;->a:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/f;->a:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 3060
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/c/f;->b:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/f;->b:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 3067
    :cond_d
    iget-object v1, p0, Lcom/google/ac/b/c/f;->c:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/f;->c:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 3074
    :cond_e
    iget-object v1, p0, Lcom/google/ac/b/c/f;->d:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/f;->d:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 3081
    :cond_f
    iget-object v1, p0, Lcom/google/ac/b/c/f;->e:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/f;->e:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 3088
    :cond_10
    iget-object v1, p0, Lcom/google/ac/b/c/f;->f:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/f;->f:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 3095
    :cond_11
    iget-object v1, p0, Lcom/google/ac/b/c/f;->g:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/f;->g:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 3102
    :cond_12
    iget-object v1, p0, Lcom/google/ac/b/c/f;->h:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/f;->h:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 3109
    :cond_13
    iget-object v1, p0, Lcom/google/ac/b/c/f;->i:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/f;->i:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3117
    iget-object v0, p0, Lcom/google/ac/b/c/f;->a:Ljava/lang/Long;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3120
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/f;->b:Ljava/lang/Long;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3122
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/f;->c:Ljava/lang/Long;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3124
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/f;->d:Ljava/lang/Long;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 3126
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/f;->e:Ljava/lang/Long;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 3128
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/f;->f:Ljava/lang/Long;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 3130
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/f;->g:Ljava/lang/Long;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 3132
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/f;->h:Ljava/lang/Long;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 3134
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/f;->i:Ljava/lang/Long;

    if-nez v2, :cond_8

    :goto_8
    add-int/2addr v0, v1

    .line 3136
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/f;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3137
    return v0

    .line 3117
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/f;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3120
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/f;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3122
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/f;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_2

    .line 3124
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/f;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_3

    .line 3126
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/f;->e:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_4

    .line 3128
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/f;->f:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_5

    .line 3130
    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/f;->g:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_6

    .line 3132
    :cond_7
    iget-object v0, p0, Lcom/google/ac/b/c/f;->h:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_7

    .line 3134
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/f;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_8
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 2977
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/f;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/f;->a:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/f;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/f;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/f;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/f;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/f;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/f;->g:Ljava/lang/Long;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/f;->h:Ljava/lang/Long;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/f;->i:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 3143
    iget-object v0, p0, Lcom/google/ac/b/c/f;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 3144
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/f;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3146
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/f;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 3147
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/f;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3149
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/f;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 3150
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/f;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3152
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/f;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 3153
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/f;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3155
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/f;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 3156
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/f;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3158
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/f;->f:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 3159
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/f;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3161
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/f;->g:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 3162
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/b/c/f;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3164
    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/f;->h:Ljava/lang/Long;

    if-eqz v0, :cond_7

    .line 3165
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/ac/b/c/f;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3167
    :cond_7
    iget-object v0, p0, Lcom/google/ac/b/c/f;->i:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 3168
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/ac/b/c/f;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3170
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3171
    return-void
.end method
