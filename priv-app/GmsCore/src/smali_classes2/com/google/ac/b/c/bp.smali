.class public final Lcom/google/ac/b/c/bp;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/ac/b/c/bp;


# instance fields
.field public a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 10071
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 10072
    iput-object v0, p0, Lcom/google/ac/b/c/bp;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/bp;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bp;->cachedSize:I

    .line 10073
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/bp;
    .locals 2

    .prologue
    .line 10057
    sget-object v0, Lcom/google/ac/b/c/bp;->b:[Lcom/google/ac/b/c/bp;

    if-nez v0, :cond_1

    .line 10058
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 10060
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/bp;->b:[Lcom/google/ac/b/c/bp;

    if-nez v0, :cond_0

    .line 10061
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/bp;

    sput-object v0, Lcom/google/ac/b/c/bp;->b:[Lcom/google/ac/b/c/bp;

    .line 10063
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 10065
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/bp;->b:[Lcom/google/ac/b/c/bp;

    return-object v0

    .line 10063
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 10121
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 10122
    iget-object v1, p0, Lcom/google/ac/b/c/bp;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 10123
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/bp;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10126
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 10084
    if-ne p1, p0, :cond_1

    .line 10085
    const/4 v0, 0x1

    .line 10098
    :cond_0
    :goto_0
    return v0

    .line 10087
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bp;

    if-eqz v1, :cond_0

    .line 10090
    check-cast p1, Lcom/google/ac/b/c/bp;

    .line 10091
    iget-object v1, p0, Lcom/google/ac/b/c/bp;->a:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 10092
    iget-object v1, p1, Lcom/google/ac/b/c/bp;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 10098
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bp;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 10095
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bp;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/bp;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 10103
    iget-object v0, p0, Lcom/google/ac/b/c/bp;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 10106
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bp;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 10107
    return v0

    .line 10103
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bp;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 10051
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bp;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bp;->a:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 10113
    iget-object v0, p0, Lcom/google/ac/b/c/bp;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 10114
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/bp;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 10116
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 10117
    return-void
.end method
