.class public final Lcom/google/ac/b/c/bz;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:[Lcom/google/ac/b/c/ca;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 10645
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 10646
    iput-object v1, p0, Lcom/google/ac/b/c/bz;->a:Ljava/lang/Long;

    invoke-static {}, Lcom/google/ac/b/c/ca;->a()[Lcom/google/ac/b/c/ca;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    iput-object v1, p0, Lcom/google/ac/b/c/bz;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bz;->cachedSize:I

    .line 10647
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 10710
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 10711
    iget-object v1, p0, Lcom/google/ac/b/c/bz;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 10712
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/bz;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 10715
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 10716
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 10717
    iget-object v2, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    aget-object v2, v2, v0

    .line 10718
    if-eqz v2, :cond_1

    .line 10719
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 10716
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 10724
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 10659
    if-ne p1, p0, :cond_1

    .line 10660
    const/4 v0, 0x1

    .line 10677
    :cond_0
    :goto_0
    return v0

    .line 10662
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bz;

    if-eqz v1, :cond_0

    .line 10665
    check-cast p1, Lcom/google/ac/b/c/bz;

    .line 10666
    iget-object v1, p0, Lcom/google/ac/b/c/bz;->a:Ljava/lang/Long;

    if-nez v1, :cond_3

    .line 10667
    iget-object v1, p1, Lcom/google/ac/b/c/bz;->a:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 10673
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    iget-object v2, p1, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 10677
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bz;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 10670
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bz;->a:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/bz;->a:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 10682
    iget-object v0, p0, Lcom/google/ac/b/c/bz;->a:Ljava/lang/Long;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 10685
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10687
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bz;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 10688
    return v0

    .line 10682
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bz;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 10622
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bz;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bz;->a:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/ca;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/c/ca;

    invoke-direct {v3}, Lcom/google/ac/b/c/ca;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/c/ca;

    invoke-direct {v3}, Lcom/google/ac/b/c/ca;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 10694
    iget-object v0, p0, Lcom/google/ac/b/c/bz;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 10695
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/bz;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 10697
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 10698
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 10699
    iget-object v1, p0, Lcom/google/ac/b/c/bz;->b:[Lcom/google/ac/b/c/ca;

    aget-object v1, v1, v0

    .line 10700
    if-eqz v1, :cond_1

    .line 10701
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 10698
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 10705
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 10706
    return-void
.end method
