.class public final Lcom/google/ac/b/b/a/ad;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Lcom/google/ac/b/b/a/t;

.field public c:Lcom/google/ac/b/b/a/j;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1137
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1138
    iput-object v0, p0, Lcom/google/ac/b/b/a/ad;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/b/a/ad;->b:Lcom/google/ac/b/b/a/t;

    iput-object v0, p0, Lcom/google/ac/b/b/a/ad;->c:Lcom/google/ac/b/b/a/j;

    iput-object v0, p0, Lcom/google/ac/b/b/a/ad;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/ad;->cachedSize:I

    .line 1139
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1217
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1218
    iget-object v1, p0, Lcom/google/ac/b/b/a/ad;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 1219
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/ad;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1222
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/ad;->b:Lcom/google/ac/b/b/a/t;

    if-eqz v1, :cond_1

    .line 1223
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/b/a/ad;->b:Lcom/google/ac/b/b/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1226
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/b/a/ad;->c:Lcom/google/ac/b/b/a/j;

    if-eqz v1, :cond_2

    .line 1227
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/b/a/ad;->c:Lcom/google/ac/b/b/a/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1230
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1152
    if-ne p1, p0, :cond_1

    .line 1153
    const/4 v0, 0x1

    .line 1184
    :cond_0
    :goto_0
    return v0

    .line 1155
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/ad;

    if-eqz v1, :cond_0

    .line 1158
    check-cast p1, Lcom/google/ac/b/b/a/ad;

    .line 1159
    iget-object v1, p0, Lcom/google/ac/b/b/a/ad;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_5

    .line 1160
    iget-object v1, p1, Lcom/google/ac/b/b/a/ad;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1166
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/ad;->b:Lcom/google/ac/b/b/a/t;

    if-nez v1, :cond_6

    .line 1167
    iget-object v1, p1, Lcom/google/ac/b/b/a/ad;->b:Lcom/google/ac/b/b/a/t;

    if-nez v1, :cond_0

    .line 1175
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/ad;->c:Lcom/google/ac/b/b/a/j;

    if-nez v1, :cond_7

    .line 1176
    iget-object v1, p1, Lcom/google/ac/b/b/a/ad;->c:Lcom/google/ac/b/b/a/j;

    if-nez v1, :cond_0

    .line 1184
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/ad;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1163
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/b/a/ad;->a:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ad;->a:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1171
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/b/a/ad;->b:Lcom/google/ac/b/b/a/t;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ad;->b:Lcom/google/ac/b/b/a/t;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/t;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 1180
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/b/a/ad;->c:Lcom/google/ac/b/b/a/j;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ad;->c:Lcom/google/ac/b/b/a/j;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/j;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1189
    iget-object v0, p0, Lcom/google/ac/b/b/a/ad;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1192
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/ad;->b:Lcom/google/ac/b/b/a/t;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1194
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/b/a/ad;->c:Lcom/google/ac/b/b/a/j;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 1196
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/ad;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1197
    return v0

    .line 1189
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/ad;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1192
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/ad;->b:Lcom/google/ac/b/b/a/t;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/t;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1194
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/ad;->c:Lcom/google/ac/b/b/a/j;

    invoke-virtual {v1}, Lcom/google/ac/b/b/a/j;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1111
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/ad;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/ad;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/ad;->b:Lcom/google/ac/b/b/a/t;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/b/a/t;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/t;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/ad;->b:Lcom/google/ac/b/b/a/t;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/ad;->b:Lcom/google/ac/b/b/a/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/ad;->c:Lcom/google/ac/b/b/a/j;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/b/a/j;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/j;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/ad;->c:Lcom/google/ac/b/b/a/j;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/ad;->c:Lcom/google/ac/b/b/a/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1203
    iget-object v0, p0, Lcom/google/ac/b/b/a/ad;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1204
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/ad;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1206
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/ad;->b:Lcom/google/ac/b/b/a/t;

    if-eqz v0, :cond_1

    .line 1207
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/b/a/ad;->b:Lcom/google/ac/b/b/a/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1209
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/ad;->c:Lcom/google/ac/b/b/a/j;

    if-eqz v0, :cond_2

    .line 1210
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/b/a/ad;->c:Lcom/google/ac/b/b/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1212
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1213
    return-void
.end method
