.class public final Lcom/google/ac/b/b/a/aj;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5618
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 5619
    iput-object v0, p0, Lcom/google/ac/b/b/a/aj;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/b/a/aj;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/b/a/aj;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/b/a/aj;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/aj;->cachedSize:I

    .line 5620
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5690
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 5691
    iget-object v1, p0, Lcom/google/ac/b/b/a/aj;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 5692
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/aj;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5695
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/aj;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 5696
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/b/a/aj;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5699
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/b/a/aj;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 5700
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/b/a/aj;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5703
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 5633
    if-ne p1, p0, :cond_1

    .line 5634
    const/4 v0, 0x1

    .line 5659
    :cond_0
    :goto_0
    return v0

    .line 5636
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/aj;

    if-eqz v1, :cond_0

    .line 5639
    check-cast p1, Lcom/google/ac/b/b/a/aj;

    .line 5640
    iget-object v1, p0, Lcom/google/ac/b/b/a/aj;->a:Ljava/lang/Integer;

    if-nez v1, :cond_5

    .line 5641
    iget-object v1, p1, Lcom/google/ac/b/b/a/aj;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 5646
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/aj;->b:Ljava/lang/Integer;

    if-nez v1, :cond_6

    .line 5647
    iget-object v1, p1, Lcom/google/ac/b/b/a/aj;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 5652
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/aj;->c:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 5653
    iget-object v1, p1, Lcom/google/ac/b/b/a/aj;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5659
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/aj;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 5644
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/b/a/aj;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/b/a/aj;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 5650
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/b/a/aj;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/b/a/aj;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 5656
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/b/a/aj;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/b/a/aj;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5664
    iget-object v0, p0, Lcom/google/ac/b/b/a/aj;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 5666
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/aj;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 5667
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/b/a/aj;->c:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 5669
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/aj;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 5670
    return v0

    .line 5664
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/aj;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 5666
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/aj;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    .line 5667
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/aj;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5592
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/aj;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/aj;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/aj;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/aj;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5676
    iget-object v0, p0, Lcom/google/ac/b/b/a/aj;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 5677
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/aj;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5679
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/aj;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 5680
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/b/a/aj;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5682
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/aj;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 5683
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/b/a/aj;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5685
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5686
    return-void
.end method
