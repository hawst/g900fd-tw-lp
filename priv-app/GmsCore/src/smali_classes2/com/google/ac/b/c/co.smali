.class public final Lcom/google/ac/b/c/co;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Lcom/google/ac/b/c/aa;

.field public d:[B

.field public e:Lcom/google/ac/b/c/ba;

.field public f:Lcom/google/ac/b/c/cc;

.field public g:Lcom/google/ac/b/c/ce;

.field public h:Lcom/google/ac/b/c/cd;

.field public i:Lcom/google/ac/b/c/cp;

.field public j:Lcom/google/ac/b/c/cn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 68
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 69
    iput-object v0, p0, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/co;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    iput-object v0, p0, Lcom/google/ac/b/c/co;->d:[B

    iput-object v0, p0, Lcom/google/ac/b/c/co;->e:Lcom/google/ac/b/c/ba;

    iput-object v0, p0, Lcom/google/ac/b/c/co;->f:Lcom/google/ac/b/c/cc;

    iput-object v0, p0, Lcom/google/ac/b/c/co;->g:Lcom/google/ac/b/c/ce;

    iput-object v0, p0, Lcom/google/ac/b/c/co;->h:Lcom/google/ac/b/c/cd;

    iput-object v0, p0, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    iput-object v0, p0, Lcom/google/ac/b/c/co;->j:Lcom/google/ac/b/c/cn;

    iput-object v0, p0, Lcom/google/ac/b/c/co;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/co;->cachedSize:I

    .line 70
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 240
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 241
    iget-object v1, p0, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 242
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 245
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/co;->d:[B

    if-eqz v1, :cond_1

    .line 246
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/co;->d:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 249
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/co;->e:Lcom/google/ac/b/c/ba;

    if-eqz v1, :cond_2

    .line 250
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/co;->e:Lcom/google/ac/b/c/ba;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 253
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    if-eqz v1, :cond_3

    .line 254
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 257
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/co;->f:Lcom/google/ac/b/c/cc;

    if-eqz v1, :cond_4

    .line 258
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/co;->f:Lcom/google/ac/b/c/cc;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/co;->g:Lcom/google/ac/b/c/ce;

    if-eqz v1, :cond_5

    .line 262
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/co;->g:Lcom/google/ac/b/c/ce;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 265
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/co;->h:Lcom/google/ac/b/c/cd;

    if-eqz v1, :cond_6

    .line 266
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/b/c/co;->h:Lcom/google/ac/b/c/cd;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 269
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/co;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_7

    .line 270
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/ac/b/c/co;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 273
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    if-eqz v1, :cond_8

    .line 274
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 277
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/co;->j:Lcom/google/ac/b/c/cn;

    if-eqz v1, :cond_9

    .line 278
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/ac/b/c/co;->j:Lcom/google/ac/b/c/cn;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 281
    :cond_9
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 90
    if-ne p1, p0, :cond_1

    .line 91
    const/4 v0, 0x1

    .line 175
    :cond_0
    :goto_0
    return v0

    .line 93
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/co;

    if-eqz v1, :cond_0

    .line 96
    check-cast p1, Lcom/google/ac/b/c/co;

    .line 97
    iget-object v1, p0, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    if-nez v1, :cond_b

    .line 98
    iget-object v1, p1, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 103
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/co;->b:Ljava/lang/Integer;

    if-nez v1, :cond_c

    .line 104
    iget-object v1, p1, Lcom/google/ac/b/c/co;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 109
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    if-nez v1, :cond_d

    .line 110
    iget-object v1, p1, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    if-nez v1, :cond_0

    .line 118
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/co;->d:[B

    iget-object v2, p1, Lcom/google/ac/b/c/co;->d:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 121
    iget-object v1, p0, Lcom/google/ac/b/c/co;->e:Lcom/google/ac/b/c/ba;

    if-nez v1, :cond_e

    .line 122
    iget-object v1, p1, Lcom/google/ac/b/c/co;->e:Lcom/google/ac/b/c/ba;

    if-nez v1, :cond_0

    .line 130
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/co;->f:Lcom/google/ac/b/c/cc;

    if-nez v1, :cond_f

    .line 131
    iget-object v1, p1, Lcom/google/ac/b/c/co;->f:Lcom/google/ac/b/c/cc;

    if-nez v1, :cond_0

    .line 139
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/co;->g:Lcom/google/ac/b/c/ce;

    if-nez v1, :cond_10

    .line 140
    iget-object v1, p1, Lcom/google/ac/b/c/co;->g:Lcom/google/ac/b/c/ce;

    if-nez v1, :cond_0

    .line 148
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/co;->h:Lcom/google/ac/b/c/cd;

    if-nez v1, :cond_11

    .line 149
    iget-object v1, p1, Lcom/google/ac/b/c/co;->h:Lcom/google/ac/b/c/cd;

    if-nez v1, :cond_0

    .line 157
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    if-nez v1, :cond_12

    .line 158
    iget-object v1, p1, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    if-nez v1, :cond_0

    .line 166
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/co;->j:Lcom/google/ac/b/c/cn;

    if-nez v1, :cond_13

    .line 167
    iget-object v1, p1, Lcom/google/ac/b/c/co;->j:Lcom/google/ac/b/c/cn;

    if-nez v1, :cond_0

    .line 175
    :cond_a
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/co;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 101
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 107
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/c/co;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/co;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 114
    :cond_d
    iget-object v1, p0, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    iget-object v2, p1, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/aa;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 126
    :cond_e
    iget-object v1, p0, Lcom/google/ac/b/c/co;->e:Lcom/google/ac/b/c/ba;

    iget-object v2, p1, Lcom/google/ac/b/c/co;->e:Lcom/google/ac/b/c/ba;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/ba;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 135
    :cond_f
    iget-object v1, p0, Lcom/google/ac/b/c/co;->f:Lcom/google/ac/b/c/cc;

    iget-object v2, p1, Lcom/google/ac/b/c/co;->f:Lcom/google/ac/b/c/cc;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/cc;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 144
    :cond_10
    iget-object v1, p0, Lcom/google/ac/b/c/co;->g:Lcom/google/ac/b/c/ce;

    iget-object v2, p1, Lcom/google/ac/b/c/co;->g:Lcom/google/ac/b/c/ce;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/ce;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 153
    :cond_11
    iget-object v1, p0, Lcom/google/ac/b/c/co;->h:Lcom/google/ac/b/c/cd;

    iget-object v2, p1, Lcom/google/ac/b/c/co;->h:Lcom/google/ac/b/c/cd;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/cd;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 162
    :cond_12
    iget-object v1, p0, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    iget-object v2, p1, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/cp;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 171
    :cond_13
    iget-object v1, p0, Lcom/google/ac/b/c/co;->j:Lcom/google/ac/b/c/cn;

    iget-object v2, p1, Lcom/google/ac/b/c/co;->j:Lcom/google/ac/b/c/cn;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/cn;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 180
    iget-object v0, p0, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 182
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/co;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 183
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 185
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/co;->d:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 186
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/co;->e:Lcom/google/ac/b/c/ba;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 188
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/co;->f:Lcom/google/ac/b/c/cc;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 190
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/co;->g:Lcom/google/ac/b/c/ce;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 192
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/co;->h:Lcom/google/ac/b/c/cd;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 194
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 196
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/co;->j:Lcom/google/ac/b/c/cn;

    if-nez v2, :cond_8

    :goto_8
    add-int/2addr v0, v1

    .line 198
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/co;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 199
    return v0

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 182
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/co;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    .line 183
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    invoke-virtual {v0}, Lcom/google/ac/b/c/aa;->hashCode()I

    move-result v0

    goto :goto_2

    .line 186
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/co;->e:Lcom/google/ac/b/c/ba;

    invoke-virtual {v0}, Lcom/google/ac/b/c/ba;->hashCode()I

    move-result v0

    goto :goto_3

    .line 188
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/co;->f:Lcom/google/ac/b/c/cc;

    invoke-virtual {v0}, Lcom/google/ac/b/c/cc;->hashCode()I

    move-result v0

    goto :goto_4

    .line 190
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/co;->g:Lcom/google/ac/b/c/ce;

    invoke-virtual {v0}, Lcom/google/ac/b/c/ce;->hashCode()I

    move-result v0

    goto :goto_5

    .line 192
    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/co;->h:Lcom/google/ac/b/c/cd;

    invoke-virtual {v0}, Lcom/google/ac/b/c/cd;->hashCode()I

    move-result v0

    goto :goto_6

    .line 194
    :cond_7
    iget-object v0, p0, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    invoke-virtual {v0}, Lcom/google/ac/b/c/cp;->hashCode()I

    move-result v0

    goto :goto_7

    .line 196
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/co;->j:Lcom/google/ac/b/c/cn;

    invoke-virtual {v1}, Lcom/google/ac/b/c/cn;->hashCode()I

    move-result v1

    goto :goto_8
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/co;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/co;->d:[B

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/co;->e:Lcom/google/ac/b/c/ba;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/ba;

    invoke-direct {v0}, Lcom/google/ac/b/c/ba;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/co;->e:Lcom/google/ac/b/c/ba;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/co;->e:Lcom/google/ac/b/c/ba;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/aa;

    invoke-direct {v0}, Lcom/google/ac/b/c/aa;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/ac/b/c/co;->f:Lcom/google/ac/b/c/cc;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/b/c/cc;

    invoke-direct {v0}, Lcom/google/ac/b/c/cc;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/co;->f:Lcom/google/ac/b/c/cc;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/co;->f:Lcom/google/ac/b/c/cc;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/ac/b/c/co;->g:Lcom/google/ac/b/c/ce;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/c/ce;

    invoke-direct {v0}, Lcom/google/ac/b/c/ce;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/co;->g:Lcom/google/ac/b/c/ce;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/co;->g:Lcom/google/ac/b/c/ce;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/ac/b/c/co;->h:Lcom/google/ac/b/c/cd;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/ac/b/c/cd;

    invoke-direct {v0}, Lcom/google/ac/b/c/cd;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/co;->h:Lcom/google/ac/b/c/cd;

    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/co;->h:Lcom/google/ac/b/c/cd;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/co;->b:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/ac/b/c/cp;

    invoke-direct {v0}, Lcom/google/ac/b/c/cp;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/ac/b/c/co;->j:Lcom/google/ac/b/c/cn;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/ac/b/c/cn;

    invoke-direct {v0}, Lcom/google/ac/b/c/cn;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/co;->j:Lcom/google/ac/b/c/cn;

    :cond_7
    iget-object v0, p0, Lcom/google/ac/b/c/co;->j:Lcom/google/ac/b/c/cn;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x40 -> :sswitch_8
        0x4a -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 206
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/co;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/co;->d:[B

    if-eqz v0, :cond_1

    .line 209
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/co;->d:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 211
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/co;->e:Lcom/google/ac/b/c/ba;

    if-eqz v0, :cond_2

    .line 212
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/co;->e:Lcom/google/ac/b/c/ba;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 214
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    if-eqz v0, :cond_3

    .line 215
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/co;->c:Lcom/google/ac/b/c/aa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 217
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/co;->f:Lcom/google/ac/b/c/cc;

    if-eqz v0, :cond_4

    .line 218
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/co;->f:Lcom/google/ac/b/c/cc;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 220
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/co;->g:Lcom/google/ac/b/c/ce;

    if-eqz v0, :cond_5

    .line 221
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/co;->g:Lcom/google/ac/b/c/ce;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 223
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/co;->h:Lcom/google/ac/b/c/cd;

    if-eqz v0, :cond_6

    .line 224
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/b/c/co;->h:Lcom/google/ac/b/c/cd;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 226
    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/co;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_7

    .line 227
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/ac/b/c/co;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 229
    :cond_7
    iget-object v0, p0, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    if-eqz v0, :cond_8

    .line 230
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/ac/b/c/co;->i:Lcom/google/ac/b/c/cp;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 232
    :cond_8
    iget-object v0, p0, Lcom/google/ac/b/c/co;->j:Lcom/google/ac/b/c/cn;

    if-eqz v0, :cond_9

    .line 233
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/ac/b/c/co;->j:Lcom/google/ac/b/c/cn;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 235
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 236
    return-void
.end method
