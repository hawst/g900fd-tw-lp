.class public final Lcom/google/ac/b/c/z;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1943
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1944
    iput-object v0, p0, Lcom/google/ac/b/c/z;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/c/z;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/z;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/z;->cachedSize:I

    .line 1945
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2004
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2005
    iget-object v1, p0, Lcom/google/ac/b/c/z;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 2006
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/z;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2009
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/z;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 2010
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/z;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2013
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1957
    if-ne p1, p0, :cond_1

    .line 1958
    const/4 v0, 0x1

    .line 1977
    :cond_0
    :goto_0
    return v0

    .line 1960
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/z;

    if-eqz v1, :cond_0

    .line 1963
    check-cast p1, Lcom/google/ac/b/c/z;

    .line 1964
    iget-object v1, p0, Lcom/google/ac/b/c/z;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_4

    .line 1965
    iget-object v1, p1, Lcom/google/ac/b/c/z;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1971
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/z;->b:Ljava/lang/Integer;

    if-nez v1, :cond_5

    .line 1972
    iget-object v1, p1, Lcom/google/ac/b/c/z;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 1977
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/z;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1968
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/z;->a:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/z;->a:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1975
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/z;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/z;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1982
    iget-object v0, p0, Lcom/google/ac/b/c/z;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1985
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/z;->b:Ljava/lang/Integer;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 1986
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/z;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1987
    return v0

    .line 1982
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/z;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1985
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/z;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1920
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/z;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/z;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/z;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1993
    iget-object v0, p0, Lcom/google/ac/b/c/z;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1994
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/z;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1996
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/z;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 1997
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/z;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1999
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2000
    return-void
.end method
