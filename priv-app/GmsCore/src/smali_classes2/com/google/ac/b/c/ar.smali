.class public final Lcom/google/ac/b/c/ar;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 11453
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 11454
    iput-object v0, p0, Lcom/google/ac/b/c/ar;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/ar;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/ar;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/ar;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/ar;->cachedSize:I

    .line 11455
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 11529
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 11530
    iget-object v1, p0, Lcom/google/ac/b/c/ar;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 11531
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/ar;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11534
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/ar;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 11535
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/ar;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11538
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/ar;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 11539
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/ar;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11542
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 11468
    if-ne p1, p0, :cond_1

    .line 11469
    const/4 v0, 0x1

    .line 11496
    :cond_0
    :goto_0
    return v0

    .line 11471
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/ar;

    if-eqz v1, :cond_0

    .line 11474
    check-cast p1, Lcom/google/ac/b/c/ar;

    .line 11475
    iget-object v1, p0, Lcom/google/ac/b/c/ar;->a:Ljava/lang/Integer;

    if-nez v1, :cond_5

    .line 11476
    iget-object v1, p1, Lcom/google/ac/b/c/ar;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 11482
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ar;->b:Ljava/lang/Integer;

    if-nez v1, :cond_6

    .line 11483
    iget-object v1, p1, Lcom/google/ac/b/c/ar;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 11489
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ar;->c:Ljava/lang/Integer;

    if-nez v1, :cond_7

    .line 11490
    iget-object v1, p1, Lcom/google/ac/b/c/ar;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 11496
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/ar;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 11479
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/ar;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/ar;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 11486
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/ar;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/ar;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 11493
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/ar;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/ar;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 11501
    iget-object v0, p0, Lcom/google/ac/b/c/ar;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 11504
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ar;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 11506
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/ar;->c:Ljava/lang/Integer;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 11508
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/ar;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 11509
    return v0

    .line 11501
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ar;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_0

    .line 11504
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ar;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_1

    .line 11506
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ar;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 11427
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/ar;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ar;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ar;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ar;->c:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 11515
    iget-object v0, p0, Lcom/google/ac/b/c/ar;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 11516
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/ar;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 11518
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ar;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 11519
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/ar;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 11521
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ar;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 11522
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/ar;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 11524
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 11525
    return-void
.end method
