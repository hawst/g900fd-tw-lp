.class public final Lcom/google/ac/b/c/dj;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1375
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1376
    iput-object v0, p0, Lcom/google/ac/b/c/dj;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/dj;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/dj;->cachedSize:I

    .line 1377
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1425
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1426
    iget-object v1, p0, Lcom/google/ac/b/c/dj;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 1427
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/dj;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1430
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1388
    if-ne p1, p0, :cond_1

    .line 1389
    const/4 v0, 0x1

    .line 1402
    :cond_0
    :goto_0
    return v0

    .line 1391
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/dj;

    if-eqz v1, :cond_0

    .line 1394
    check-cast p1, Lcom/google/ac/b/c/dj;

    .line 1395
    iget-object v1, p0, Lcom/google/ac/b/c/dj;->a:Ljava/lang/String;

    if-nez v1, :cond_3

    .line 1396
    iget-object v1, p1, Lcom/google/ac/b/c/dj;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1402
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/dj;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1399
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/dj;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/dj;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1407
    iget-object v0, p0, Lcom/google/ac/b/c/dj;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1410
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/dj;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1411
    return v0

    .line 1407
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/dj;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1355
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/dj;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dj;->a:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1417
    iget-object v0, p0, Lcom/google/ac/b/c/dj;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 1418
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/dj;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1420
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1421
    return-void
.end method
