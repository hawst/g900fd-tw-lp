.class public final Lcom/google/ac/b/b/a/p;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:[Lcom/google/ac/b/b/a/ah;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4582
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 4583
    iput-object v1, p0, Lcom/google/ac/b/b/a/p;->a:Ljava/lang/Integer;

    invoke-static {}, Lcom/google/ac/b/b/a/ah;->a()[Lcom/google/ac/b/b/a/ah;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    iput-object v1, p0, Lcom/google/ac/b/b/a/p;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/p;->cachedSize:I

    .line 4584
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 4647
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 4648
    iget-object v1, p0, Lcom/google/ac/b/b/a/p;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 4649
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/p;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4652
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 4653
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 4654
    iget-object v2, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    aget-object v2, v2, v0

    .line 4655
    if-eqz v2, :cond_1

    .line 4656
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 4653
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 4661
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4596
    if-ne p1, p0, :cond_1

    .line 4597
    const/4 v0, 0x1

    .line 4614
    :cond_0
    :goto_0
    return v0

    .line 4599
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/p;

    if-eqz v1, :cond_0

    .line 4602
    check-cast p1, Lcom/google/ac/b/b/a/p;

    .line 4603
    iget-object v1, p0, Lcom/google/ac/b/b/a/p;->a:Ljava/lang/Integer;

    if-nez v1, :cond_3

    .line 4604
    iget-object v1, p1, Lcom/google/ac/b/b/a/p;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 4610
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    iget-object v2, p1, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4614
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/p;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 4607
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/p;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/b/a/p;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 4619
    iget-object v0, p0, Lcom/google/ac/b/b/a/p;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4622
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4624
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/p;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4625
    return v0

    .line 4619
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/p;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4559
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/p;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/p;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/b/a/ah;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/b/a/ah;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/ah;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/b/a/ah;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/ah;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x1a -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 4631
    iget-object v0, p0, Lcom/google/ac/b/b/a/p;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 4632
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/p;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4634
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 4635
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 4636
    iget-object v1, p0, Lcom/google/ac/b/b/a/p;->b:[Lcom/google/ac/b/b/a/ah;

    aget-object v1, v1, v0

    .line 4637
    if-eqz v1, :cond_1

    .line 4638
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4635
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4642
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4643
    return-void
.end method
