.class public final Lcom/google/ac/b/c/da;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/c/dc;

.field public b:Lcom/google/ac/b/c/cu;

.field public c:Lcom/google/ac/b/c/cw;

.field public d:Lcom/google/ac/b/c/dg;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3382
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3383
    iput-object v0, p0, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    iput-object v0, p0, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    iput-object v0, p0, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    iput-object v0, p0, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    iput-object v0, p0, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/da;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/da;->cachedSize:I

    .line 3384
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3492
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3493
    iget-object v1, p0, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    if-eqz v1, :cond_0

    .line 3494
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3497
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    if-eqz v1, :cond_1

    .line 3498
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3501
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    if-eqz v1, :cond_2

    .line 3502
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3505
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    if-eqz v1, :cond_3

    .line 3506
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3509
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 3510
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3513
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3399
    if-ne p1, p0, :cond_1

    .line 3400
    const/4 v0, 0x1

    .line 3449
    :cond_0
    :goto_0
    return v0

    .line 3402
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/da;

    if-eqz v1, :cond_0

    .line 3405
    check-cast p1, Lcom/google/ac/b/c/da;

    .line 3406
    iget-object v1, p0, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    if-nez v1, :cond_7

    .line 3407
    iget-object v1, p1, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    if-nez v1, :cond_0

    .line 3415
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    if-nez v1, :cond_8

    .line 3416
    iget-object v1, p1, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    if-nez v1, :cond_0

    .line 3424
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    if-nez v1, :cond_9

    .line 3425
    iget-object v1, p1, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    if-nez v1, :cond_0

    .line 3433
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    if-nez v1, :cond_a

    .line 3434
    iget-object v1, p1, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    if-nez v1, :cond_0

    .line 3442
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 3443
    iget-object v1, p1, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3449
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/da;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 3411
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    iget-object v2, p1, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/dc;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 3420
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    iget-object v2, p1, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/cu;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 3429
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    iget-object v2, p1, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/cw;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 3438
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    iget-object v2, p1, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/dg;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 3446
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3454
    iget-object v0, p0, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3457
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3459
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3461
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 3463
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 3465
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/da;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3466
    return v0

    .line 3454
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    invoke-virtual {v0}, Lcom/google/ac/b/c/dc;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3457
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    invoke-virtual {v0}, Lcom/google/ac/b/c/cu;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3459
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    invoke-virtual {v0}, Lcom/google/ac/b/c/cw;->hashCode()I

    move-result v0

    goto :goto_2

    .line 3461
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    invoke-virtual {v0}, Lcom/google/ac/b/c/dg;->hashCode()I

    move-result v0

    goto :goto_3

    .line 3463
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3350
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/da;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/dc;

    invoke-direct {v0}, Lcom/google/ac/b/c/dc;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/cu;

    invoke-direct {v0}, Lcom/google/ac/b/c/cu;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/b/c/cw;

    invoke-direct {v0}, Lcom/google/ac/b/c/cw;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/c/dg;

    invoke-direct {v0}, Lcom/google/ac/b/c/dg;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3472
    iget-object v0, p0, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    if-eqz v0, :cond_0

    .line 3473
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/da;->a:Lcom/google/ac/b/c/dc;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3475
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    if-eqz v0, :cond_1

    .line 3476
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/da;->b:Lcom/google/ac/b/c/cu;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3478
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    if-eqz v0, :cond_2

    .line 3479
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/da;->c:Lcom/google/ac/b/c/cw;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3481
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    if-eqz v0, :cond_3

    .line 3482
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/da;->d:Lcom/google/ac/b/c/dg;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3484
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 3485
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/da;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3487
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3488
    return-void
.end method
