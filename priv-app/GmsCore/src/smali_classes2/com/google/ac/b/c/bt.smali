.class public final Lcom/google/ac/b/c/bt;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4564
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 4565
    iput-object v0, p0, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/bt;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bt;->cachedSize:I

    .line 4566
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 4636
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 4637
    iget-object v1, p0, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 4638
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4641
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 4642
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4645
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 4646
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4649
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4579
    if-ne p1, p0, :cond_1

    .line 4580
    const/4 v0, 0x1

    .line 4605
    :cond_0
    :goto_0
    return v0

    .line 4582
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bt;

    if-eqz v1, :cond_0

    .line 4585
    check-cast p1, Lcom/google/ac/b/c/bt;

    .line 4586
    iget-object v1, p0, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    if-nez v1, :cond_5

    .line 4587
    iget-object v1, p1, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 4592
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    if-nez v1, :cond_6

    .line 4593
    iget-object v1, p1, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 4598
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 4599
    iget-object v1, p1, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4605
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bt;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 4590
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 4596
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 4602
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4610
    iget-object v0, p0, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4612
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 4613
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 4615
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bt;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4616
    return v0

    .line 4610
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 4612
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    .line 4613
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4538
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bt;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x22 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4622
    iget-object v0, p0, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 4623
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/bt;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4625
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 4626
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/bt;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4628
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 4629
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/bt;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4631
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4632
    return-void
.end method
