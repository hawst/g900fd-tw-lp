.class public final Lcom/google/ac/b/b/a/j;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1415
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1416
    iput-object v0, p0, Lcom/google/ac/b/b/a/j;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/b/a/j;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/j;->cachedSize:I

    .line 1417
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1465
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1466
    iget-object v1, p0, Lcom/google/ac/b/b/a/j;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 1467
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/j;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1470
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1428
    if-ne p1, p0, :cond_1

    .line 1429
    const/4 v0, 0x1

    .line 1442
    :cond_0
    :goto_0
    return v0

    .line 1431
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/j;

    if-eqz v1, :cond_0

    .line 1434
    check-cast p1, Lcom/google/ac/b/b/a/j;

    .line 1435
    iget-object v1, p0, Lcom/google/ac/b/b/a/j;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_3

    .line 1436
    iget-object v1, p1, Lcom/google/ac/b/b/a/j;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1442
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/j;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1439
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/j;->a:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/b/a/j;->a:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1447
    iget-object v0, p0, Lcom/google/ac/b/b/a/j;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1450
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/j;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1451
    return v0

    .line 1447
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/j;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1395
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/j;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/j;->a:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1457
    iget-object v0, p0, Lcom/google/ac/b/b/a/j;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1458
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/j;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1460
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1461
    return-void
.end method
