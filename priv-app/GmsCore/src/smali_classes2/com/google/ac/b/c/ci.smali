.class public final Lcom/google/ac/b/c/ci;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/c/cl;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 280
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 281
    iput-object v0, p0, Lcom/google/ac/b/c/ci;->a:Lcom/google/ac/b/c/cl;

    iput-object v0, p0, Lcom/google/ac/b/c/ci;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ci;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ci;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ci;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ci;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/ci;->cachedSize:I

    .line 282
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 384
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 385
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->a:Lcom/google/ac/b/c/cl;

    if-eqz v1, :cond_0

    .line 386
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/ci;->a:Lcom/google/ac/b/c/cl;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 389
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 390
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/ci;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 393
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 394
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/ci;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 397
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 398
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/ci;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 401
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 402
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/ci;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 405
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 297
    if-ne p1, p0, :cond_1

    .line 298
    const/4 v0, 0x1

    .line 341
    :cond_0
    :goto_0
    return v0

    .line 300
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/ci;

    if-eqz v1, :cond_0

    .line 303
    check-cast p1, Lcom/google/ac/b/c/ci;

    .line 304
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->a:Lcom/google/ac/b/c/cl;

    if-nez v1, :cond_7

    .line 305
    iget-object v1, p1, Lcom/google/ac/b/c/ci;->a:Lcom/google/ac/b/c/cl;

    if-nez v1, :cond_0

    .line 313
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->b:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 314
    iget-object v1, p1, Lcom/google/ac/b/c/ci;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 320
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->c:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 321
    iget-object v1, p1, Lcom/google/ac/b/c/ci;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 327
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->d:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 328
    iget-object v1, p1, Lcom/google/ac/b/c/ci;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 334
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->e:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 335
    iget-object v1, p1, Lcom/google/ac/b/c/ci;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 341
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/ci;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 309
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->a:Lcom/google/ac/b/c/cl;

    iget-object v2, p1, Lcom/google/ac/b/c/ci;->a:Lcom/google/ac/b/c/cl;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/cl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 317
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ci;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 324
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ci;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 331
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ci;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 338
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ci;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 346
    iget-object v0, p0, Lcom/google/ac/b/c/ci;->a:Lcom/google/ac/b/c/cl;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 349
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ci;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 351
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ci;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 353
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ci;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 355
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/ci;->e:Ljava/lang/String;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 357
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/ci;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 358
    return v0

    .line 346
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ci;->a:Lcom/google/ac/b/c/cl;

    invoke-virtual {v0}, Lcom/google/ac/b/c/cl;->hashCode()I

    move-result v0

    goto :goto_0

    .line 349
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ci;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 351
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ci;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 353
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/ci;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 355
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/ci;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 248
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/ci;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/ci;->a:Lcom/google/ac/b/c/cl;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/cl;

    invoke-direct {v0}, Lcom/google/ac/b/c/cl;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/ci;->a:Lcom/google/ac/b/c/cl;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ci;->a:Lcom/google/ac/b/c/cl;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ci;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ci;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ci;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ci;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/ac/b/c/ci;->a:Lcom/google/ac/b/c/cl;

    if-eqz v0, :cond_0

    .line 365
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/ci;->a:Lcom/google/ac/b/c/cl;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 367
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ci;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 368
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/ci;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 370
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ci;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 371
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/ci;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 373
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ci;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 374
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/ci;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 376
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/ci;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 377
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/ci;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 379
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 380
    return-void
.end method
