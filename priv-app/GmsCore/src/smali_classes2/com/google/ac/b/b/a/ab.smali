.class public final Lcom/google/ac/b/b/a/ab;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/b/a/ac;

.field public b:Lcom/google/ac/b/b/a/y;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3746
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3747
    iput-object v0, p0, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    iput-object v0, p0, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    iput-object v0, p0, Lcom/google/ac/b/b/a/ab;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/ab;->cachedSize:I

    .line 3748
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3813
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3814
    iget-object v1, p0, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    if-eqz v1, :cond_0

    .line 3815
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3818
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    if-eqz v1, :cond_1

    .line 3819
    const/16 v1, 0x66

    iget-object v2, p0, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3822
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3760
    if-ne p1, p0, :cond_1

    .line 3761
    const/4 v0, 0x1

    .line 3785
    :cond_0
    :goto_0
    return v0

    .line 3763
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/ab;

    if-eqz v1, :cond_0

    .line 3766
    check-cast p1, Lcom/google/ac/b/b/a/ab;

    .line 3767
    iget-object v1, p0, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    if-nez v1, :cond_4

    .line 3768
    iget-object v1, p1, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    if-nez v1, :cond_0

    .line 3776
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    if-nez v1, :cond_5

    .line 3777
    iget-object v1, p1, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    if-nez v1, :cond_0

    .line 3785
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/ab;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 3772
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/ac;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 3781
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/y;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3790
    iget-object v0, p0, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3793
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 3795
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/ab;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3796
    return v0

    .line 3790
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/ac;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3793
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    invoke-virtual {v1}, Lcom/google/ac/b/b/a/y;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3723
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/ab;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/b/a/ac;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/ac;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/b/a/y;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/y;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x332 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3802
    iget-object v0, p0, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    if-eqz v0, :cond_0

    .line 3803
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/ab;->a:Lcom/google/ac/b/b/a/ac;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3805
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    if-eqz v0, :cond_1

    .line 3806
    const/16 v0, 0x66

    iget-object v1, p0, Lcom/google/ac/b/b/a/ab;->b:Lcom/google/ac/b/b/a/y;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3808
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3809
    return-void
.end method
