.class public final Lcom/google/ac/b/c/bj;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Boolean;

.field public b:Ljava/lang/Boolean;

.field public c:Lcom/google/ac/b/c/ay;

.field public d:Lcom/google/ac/b/c/an;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1542
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1543
    iput-object v0, p0, Lcom/google/ac/b/c/bj;->a:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/c/bj;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    iput-object v0, p0, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    iput-object v0, p0, Lcom/google/ac/b/c/bj;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bj;->cachedSize:I

    .line 1544
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1635
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1636
    iget-object v1, p0, Lcom/google/ac/b/c/bj;->a:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 1637
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/bj;->a:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1640
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/bj;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 1641
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/bj;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1644
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    if-eqz v1, :cond_2

    .line 1645
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1648
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    if-eqz v1, :cond_3

    .line 1649
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1652
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1558
    if-ne p1, p0, :cond_1

    .line 1559
    const/4 v0, 0x1

    .line 1597
    :cond_0
    :goto_0
    return v0

    .line 1561
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bj;

    if-eqz v1, :cond_0

    .line 1564
    check-cast p1, Lcom/google/ac/b/c/bj;

    .line 1565
    iget-object v1, p0, Lcom/google/ac/b/c/bj;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_6

    .line 1566
    iget-object v1, p1, Lcom/google/ac/b/c/bj;->a:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1572
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bj;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_7

    .line 1573
    iget-object v1, p1, Lcom/google/ac/b/c/bj;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 1579
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    if-nez v1, :cond_8

    .line 1580
    iget-object v1, p1, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    if-nez v1, :cond_0

    .line 1588
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    if-nez v1, :cond_9

    .line 1589
    iget-object v1, p1, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    if-nez v1, :cond_0

    .line 1597
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bj;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1569
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/bj;->a:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/bj;->a:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1576
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/bj;->b:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/bj;->b:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 1584
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    iget-object v2, p1, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/ay;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 1593
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    iget-object v2, p1, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/an;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1602
    iget-object v0, p0, Lcom/google/ac/b/c/bj;->a:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1605
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bj;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1607
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1609
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 1611
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bj;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1612
    return v0

    .line 1602
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bj;->a:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1605
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bj;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1607
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    invoke-virtual {v0}, Lcom/google/ac/b/c/ay;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1609
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    invoke-virtual {v1}, Lcom/google/ac/b/c/an;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1513
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bj;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bj;->a:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bj;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/ay;

    invoke-direct {v0}, Lcom/google/ac/b/c/ay;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/an;

    invoke-direct {v0}, Lcom/google/ac/b/c/an;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x28 -> :sswitch_2
        0x32 -> :sswitch_3
        0x3a -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1618
    iget-object v0, p0, Lcom/google/ac/b/c/bj;->a:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 1619
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/bj;->a:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1621
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bj;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 1622
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/bj;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 1624
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    if-eqz v0, :cond_2

    .line 1625
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/bj;->c:Lcom/google/ac/b/c/ay;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1627
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    if-eqz v0, :cond_3

    .line 1628
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/b/c/bj;->d:Lcom/google/ac/b/c/an;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1630
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1631
    return-void
.end method
