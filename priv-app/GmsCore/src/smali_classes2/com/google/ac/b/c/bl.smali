.class public final Lcom/google/ac/b/c/bl;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 216
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 217
    iput-object v0, p0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/bl;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/bl;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bl;->cachedSize:I

    .line 218
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 277
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 278
    iget-object v1, p0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 279
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 282
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/bl;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 283
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/bl;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 286
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 230
    if-ne p1, p0, :cond_1

    .line 231
    const/4 v0, 0x1

    .line 250
    :cond_0
    :goto_0
    return v0

    .line 233
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bl;

    if-eqz v1, :cond_0

    .line 236
    check-cast p1, Lcom/google/ac/b/c/bl;

    .line 237
    iget-object v1, p0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    if-nez v1, :cond_4

    .line 238
    iget-object v1, p1, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 243
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bl;->b:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 244
    iget-object v1, p1, Lcom/google/ac/b/c/bl;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 250
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bl;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 241
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 247
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/bl;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/bl;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 255
    iget-object v0, p0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 257
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/bl;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 259
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bl;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 260
    return v0

    .line 255
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 257
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/bl;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 193
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bl;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bl;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_3
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        -0x1 -> :sswitch_2
        0x0 -> :sswitch_2
        0xc9 -> :sswitch_2
        0xca -> :sswitch_2
        0xcb -> :sswitch_2
        0xcc -> :sswitch_2
        0x12d -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 267
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/bl;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 269
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bl;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 270
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/bl;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 272
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 273
    return-void
.end method
