.class public final Lcom/google/ac/b/c/y;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 38
    iput-object v0, p0, Lcom/google/ac/b/c/y;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/y;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/y;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/y;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/y;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/y;->cachedSize:I

    .line 39
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 126
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 127
    iget-object v1, p0, Lcom/google/ac/b/c/y;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 128
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/y;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 131
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/y;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 132
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/y;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 135
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/y;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 136
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/y;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 139
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/y;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 140
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/y;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 143
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 53
    if-ne p1, p0, :cond_1

    .line 54
    const/4 v0, 0x1

    .line 88
    :cond_0
    :goto_0
    return v0

    .line 56
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/y;

    if-eqz v1, :cond_0

    .line 59
    check-cast p1, Lcom/google/ac/b/c/y;

    .line 60
    iget-object v1, p0, Lcom/google/ac/b/c/y;->a:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 61
    iget-object v1, p1, Lcom/google/ac/b/c/y;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 67
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/y;->b:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 68
    iget-object v1, p1, Lcom/google/ac/b/c/y;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 74
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/y;->c:Ljava/lang/Long;

    if-nez v1, :cond_8

    .line 75
    iget-object v1, p1, Lcom/google/ac/b/c/y;->c:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 81
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/y;->d:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 82
    iget-object v1, p1, Lcom/google/ac/b/c/y;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 88
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/y;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 64
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/y;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/y;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 71
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/y;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/y;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 78
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/y;->c:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/y;->c:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 85
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/y;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/y;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 93
    iget-object v0, p0, Lcom/google/ac/b/c/y;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 96
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/y;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 98
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/y;->c:Ljava/lang/Long;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 100
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/y;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 102
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/y;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 103
    return v0

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/y;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 96
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/y;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 98
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/y;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_2

    .line 100
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/y;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/y;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/y;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/y;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/y;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/y;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/ac/b/c/y;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 110
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/y;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/y;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 113
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/y;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 115
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/y;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 116
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/y;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 118
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/y;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 119
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/y;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 121
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 122
    return-void
.end method
