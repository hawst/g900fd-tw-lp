.class public final Lcom/google/ac/b/c/w;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5009
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 5010
    iput-object v0, p0, Lcom/google/ac/b/c/w;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/w;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/w;->cachedSize:I

    .line 5011
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5057
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 5058
    iget-object v1, p0, Lcom/google/ac/b/c/w;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 5059
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/w;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5062
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 5022
    if-ne p1, p0, :cond_1

    .line 5023
    const/4 v0, 0x1

    .line 5035
    :cond_0
    :goto_0
    return v0

    .line 5025
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/w;

    if-eqz v1, :cond_0

    .line 5028
    check-cast p1, Lcom/google/ac/b/c/w;

    .line 5029
    iget-object v1, p0, Lcom/google/ac/b/c/w;->a:Ljava/lang/Integer;

    if-nez v1, :cond_3

    .line 5030
    iget-object v1, p1, Lcom/google/ac/b/c/w;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 5035
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/w;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 5033
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/w;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/w;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 5040
    iget-object v0, p0, Lcom/google/ac/b/c/w;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 5042
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/w;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 5043
    return v0

    .line 5040
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/w;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4989
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/w;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/w;->a:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5049
    iget-object v0, p0, Lcom/google/ac/b/c/w;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 5050
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/w;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5052
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5053
    return-void
.end method
