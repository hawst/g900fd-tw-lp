.class public final Lcom/google/ac/b/c/ba;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/Integer;

.field public d:Lcom/google/ac/b/c/be;

.field public e:Ljava/lang/String;

.field public f:Lcom/google/ac/b/c/cl;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 5345
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 5346
    iput-object v0, p0, Lcom/google/ac/b/c/ba;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->d:Lcom/google/ac/b/c/be;

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->f:Lcom/google/ac/b/c/cl;

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/ba;->cachedSize:I

    .line 5347
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 5488
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 5489
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 5490
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/ba;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5493
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 5494
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/ba;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5497
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 5498
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/ba;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 5501
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->d:Lcom/google/ac/b/c/be;

    if-eqz v1, :cond_3

    .line 5502
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/ba;->d:Lcom/google/ac/b/c/be;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5505
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 5506
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/ba;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5509
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->f:Lcom/google/ac/b/c/cl;

    if-eqz v1, :cond_5

    .line 5510
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/ba;->f:Lcom/google/ac/b/c/cl;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5513
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 5514
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/b/c/ba;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5517
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 5518
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/ac/b/c/ba;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5521
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 5365
    if-ne p1, p0, :cond_1

    .line 5366
    const/4 v0, 0x1

    .line 5431
    :cond_0
    :goto_0
    return v0

    .line 5368
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/ba;

    if-eqz v1, :cond_0

    .line 5371
    check-cast p1, Lcom/google/ac/b/c/ba;

    .line 5372
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->a:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 5373
    iget-object v1, p1, Lcom/google/ac/b/c/ba;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5379
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->b:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 5380
    iget-object v1, p1, Lcom/google/ac/b/c/ba;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5386
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->c:Ljava/lang/Integer;

    if-nez v1, :cond_c

    .line 5387
    iget-object v1, p1, Lcom/google/ac/b/c/ba;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 5392
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->d:Lcom/google/ac/b/c/be;

    if-nez v1, :cond_d

    .line 5393
    iget-object v1, p1, Lcom/google/ac/b/c/ba;->d:Lcom/google/ac/b/c/be;

    if-nez v1, :cond_0

    .line 5401
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->e:Ljava/lang/String;

    if-nez v1, :cond_e

    .line 5402
    iget-object v1, p1, Lcom/google/ac/b/c/ba;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5408
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->f:Lcom/google/ac/b/c/cl;

    if-nez v1, :cond_f

    .line 5409
    iget-object v1, p1, Lcom/google/ac/b/c/ba;->f:Lcom/google/ac/b/c/cl;

    if-nez v1, :cond_0

    .line 5417
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->g:Ljava/lang/String;

    if-nez v1, :cond_10

    .line 5418
    iget-object v1, p1, Lcom/google/ac/b/c/ba;->g:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5424
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->h:Ljava/lang/String;

    if-nez v1, :cond_11

    .line 5425
    iget-object v1, p1, Lcom/google/ac/b/c/ba;->h:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 5431
    :cond_9
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/ba;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 5376
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ba;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 5383
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ba;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 5390
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/ba;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 5397
    :cond_d
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->d:Lcom/google/ac/b/c/be;

    iget-object v2, p1, Lcom/google/ac/b/c/ba;->d:Lcom/google/ac/b/c/be;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/be;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 5405
    :cond_e
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ba;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 5413
    :cond_f
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->f:Lcom/google/ac/b/c/cl;

    iget-object v2, p1, Lcom/google/ac/b/c/ba;->f:Lcom/google/ac/b/c/cl;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/cl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 5421
    :cond_10
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->g:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ba;->g:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 5428
    :cond_11
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->h:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ba;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 5436
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 5439
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ba;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 5441
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ba;->c:Ljava/lang/Integer;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 5442
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ba;->d:Lcom/google/ac/b/c/be;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 5444
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ba;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 5446
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ba;->f:Lcom/google/ac/b/c/cl;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 5448
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ba;->g:Ljava/lang/String;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 5450
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/ba;->h:Ljava/lang/String;

    if-nez v2, :cond_7

    :goto_7
    add-int/2addr v0, v1

    .line 5452
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/ba;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 5453
    return v0

    .line 5436
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 5439
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 5441
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_2

    .line 5442
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->d:Lcom/google/ac/b/c/be;

    invoke-virtual {v0}, Lcom/google/ac/b/c/be;->hashCode()I

    move-result v0

    goto :goto_3

    .line 5444
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 5446
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->f:Lcom/google/ac/b/c/cl;

    invoke-virtual {v0}, Lcom/google/ac/b/c/cl;->hashCode()I

    move-result v0

    goto :goto_5

    .line 5448
    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->g:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_6

    .line 5450
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/ba;->h:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_7
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5304
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/ba;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->d:Lcom/google/ac/b/c/be;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/be;

    invoke-direct {v0}, Lcom/google/ac/b/c/be;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->d:Lcom/google/ac/b/c/be;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->d:Lcom/google/ac/b/c/be;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->f:Lcom/google/ac/b/c/cl;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/cl;

    invoke-direct {v0}, Lcom/google/ac/b/c/cl;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->f:Lcom/google/ac/b/c/cl;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->f:Lcom/google/ac/b/c/cl;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ba;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5459
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 5460
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/ba;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5462
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 5463
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/ba;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5465
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 5466
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/ba;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 5468
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->d:Lcom/google/ac/b/c/be;

    if-eqz v0, :cond_3

    .line 5469
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/ba;->d:Lcom/google/ac/b/c/be;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5471
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 5472
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/ba;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5474
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->f:Lcom/google/ac/b/c/cl;

    if-eqz v0, :cond_5

    .line 5475
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/ba;->f:Lcom/google/ac/b/c/cl;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5477
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 5478
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/b/c/ba;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5480
    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/ba;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 5481
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/ac/b/c/ba;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5483
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5484
    return-void
.end method
