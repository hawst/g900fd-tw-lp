.class public final Lcom/google/ac/b/c/au;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/ac/b/c/au;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/ac/b/c/r;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9932
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 9933
    iput-object v0, p0, Lcom/google/ac/b/c/au;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/au;->b:Lcom/google/ac/b/c/r;

    iput-object v0, p0, Lcom/google/ac/b/c/au;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/au;->cachedSize:I

    .line 9934
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/au;
    .locals 2

    .prologue
    .line 9915
    sget-object v0, Lcom/google/ac/b/c/au;->c:[Lcom/google/ac/b/c/au;

    if-nez v0, :cond_1

    .line 9916
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 9918
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/au;->c:[Lcom/google/ac/b/c/au;

    if-nez v0, :cond_0

    .line 9919
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/au;

    sput-object v0, Lcom/google/ac/b/c/au;->c:[Lcom/google/ac/b/c/au;

    .line 9921
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9923
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/au;->c:[Lcom/google/ac/b/c/au;

    return-object v0

    .line 9921
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 9997
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 9998
    iget-object v1, p0, Lcom/google/ac/b/c/au;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9999
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/au;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10002
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/au;->b:Lcom/google/ac/b/c/r;

    if-eqz v1, :cond_1

    .line 10003
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/au;->b:Lcom/google/ac/b/c/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10006
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 9946
    if-ne p1, p0, :cond_1

    .line 9947
    const/4 v0, 0x1

    .line 9969
    :cond_0
    :goto_0
    return v0

    .line 9949
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/au;

    if-eqz v1, :cond_0

    .line 9952
    check-cast p1, Lcom/google/ac/b/c/au;

    .line 9953
    iget-object v1, p0, Lcom/google/ac/b/c/au;->a:Ljava/lang/String;

    if-nez v1, :cond_4

    .line 9954
    iget-object v1, p1, Lcom/google/ac/b/c/au;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 9960
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/au;->b:Lcom/google/ac/b/c/r;

    if-nez v1, :cond_5

    .line 9961
    iget-object v1, p1, Lcom/google/ac/b/c/au;->b:Lcom/google/ac/b/c/r;

    if-nez v1, :cond_0

    .line 9969
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/au;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 9957
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/au;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/au;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 9965
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/au;->b:Lcom/google/ac/b/c/r;

    iget-object v2, p1, Lcom/google/ac/b/c/au;->b:Lcom/google/ac/b/c/r;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/r;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 9974
    iget-object v0, p0, Lcom/google/ac/b/c/au;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 9977
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/au;->b:Lcom/google/ac/b/c/r;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 9979
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/au;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 9980
    return v0

    .line 9974
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/au;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 9977
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/au;->b:Lcom/google/ac/b/c/r;

    invoke-virtual {v1}, Lcom/google/ac/b/c/r;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 9909
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/au;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/au;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/au;->b:Lcom/google/ac/b/c/r;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/r;

    invoke-direct {v0}, Lcom/google/ac/b/c/r;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/au;->b:Lcom/google/ac/b/c/r;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/au;->b:Lcom/google/ac/b/c/r;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 9986
    iget-object v0, p0, Lcom/google/ac/b/c/au;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 9987
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/au;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 9989
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/au;->b:Lcom/google/ac/b/c/r;

    if-eqz v0, :cond_1

    .line 9990
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/au;->b:Lcom/google/ac/b/c/r;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9992
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 9993
    return-void
.end method
