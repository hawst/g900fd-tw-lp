.class public final Lcom/google/ac/b/b/a/y;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/b/a/n;

.field public b:Lcom/google/ac/b/b/a/p;

.field public c:Lcom/google/ac/b/b/a/ao;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4136
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 4137
    iput-object v0, p0, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    iput-object v0, p0, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    iput-object v0, p0, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    iput-object v0, p0, Lcom/google/ac/b/b/a/y;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/y;->cachedSize:I

    .line 4138
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 4218
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 4219
    iget-object v1, p0, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    if-eqz v1, :cond_0

    .line 4220
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4223
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    if-eqz v1, :cond_1

    .line 4224
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4227
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    if-eqz v1, :cond_2

    .line 4228
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4231
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4151
    if-ne p1, p0, :cond_1

    .line 4152
    const/4 v0, 0x1

    .line 4185
    :cond_0
    :goto_0
    return v0

    .line 4154
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/y;

    if-eqz v1, :cond_0

    .line 4157
    check-cast p1, Lcom/google/ac/b/b/a/y;

    .line 4158
    iget-object v1, p0, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    if-nez v1, :cond_5

    .line 4159
    iget-object v1, p1, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    if-nez v1, :cond_0

    .line 4167
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    if-nez v1, :cond_6

    .line 4168
    iget-object v1, p1, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    if-nez v1, :cond_0

    .line 4176
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    if-nez v1, :cond_7

    .line 4177
    iget-object v1, p1, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    if-nez v1, :cond_0

    .line 4185
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/y;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 4163
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    iget-object v2, p1, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/n;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 4172
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    iget-object v2, p1, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/p;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 4181
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    iget-object v2, p1, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/ao;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4190
    iget-object v0, p0, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4193
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 4195
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 4197
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/y;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4198
    return v0

    .line 4190
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/n;->hashCode()I

    move-result v0

    goto :goto_0

    .line 4193
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/p;->hashCode()I

    move-result v0

    goto :goto_1

    .line 4195
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    invoke-virtual {v1}, Lcom/google/ac/b/b/a/ao;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4110
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/y;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/b/a/n;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/n;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/b/a/p;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/p;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/b/b/a/ao;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/ao;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4204
    iget-object v0, p0, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    if-eqz v0, :cond_0

    .line 4205
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/y;->a:Lcom/google/ac/b/b/a/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4207
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    if-eqz v0, :cond_1

    .line 4208
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/b/a/y;->b:Lcom/google/ac/b/b/a/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4210
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    if-eqz v0, :cond_2

    .line 4211
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/b/a/y;->c:Lcom/google/ac/b/b/a/ao;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4213
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4214
    return-void
.end method
