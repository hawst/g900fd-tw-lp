.class public final Lcom/google/ac/b/c/bo;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile i:[Lcom/google/ac/b/c/bo;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Long;

.field public d:Lcom/google/ac/b/c/av;

.field public e:Lcom/google/ac/b/c/r;

.field public f:Lcom/google/ac/b/c/bc;

.field public g:Lcom/google/ac/b/c/bs;

.field public h:Lcom/google/ac/b/c/bb;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 9304
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 9305
    iput-object v0, p0, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bo;->cachedSize:I

    .line 9306
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/bo;
    .locals 2

    .prologue
    .line 9269
    sget-object v0, Lcom/google/ac/b/c/bo;->i:[Lcom/google/ac/b/c/bo;

    if-nez v0, :cond_1

    .line 9270
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 9272
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/bo;->i:[Lcom/google/ac/b/c/bo;

    if-nez v0, :cond_0

    .line 9273
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/bo;

    sput-object v0, Lcom/google/ac/b/c/bo;->i:[Lcom/google/ac/b/c/bo;

    .line 9275
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 9277
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/bo;->i:[Lcom/google/ac/b/c/bo;

    return-object v0

    .line 9275
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 9453
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 9454
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 9455
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9458
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 9459
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 9462
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 9463
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9466
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    if-eqz v1, :cond_3

    .line 9467
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9470
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    if-eqz v1, :cond_4

    .line 9471
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9474
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    if-eqz v1, :cond_5

    .line 9475
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9478
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    if-eqz v1, :cond_6

    .line 9479
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9482
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    if-eqz v1, :cond_7

    .line 9483
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 9486
    :cond_7
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 9324
    if-ne p1, p0, :cond_1

    .line 9325
    const/4 v0, 0x1

    .line 9396
    :cond_0
    :goto_0
    return v0

    .line 9327
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bo;

    if-eqz v1, :cond_0

    .line 9330
    check-cast p1, Lcom/google/ac/b/c/bo;

    .line 9331
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 9332
    iget-object v1, p1, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 9338
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    if-nez v1, :cond_b

    .line 9339
    iget-object v1, p1, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 9344
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    if-nez v1, :cond_c

    .line 9345
    iget-object v1, p1, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 9351
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    if-nez v1, :cond_d

    .line 9352
    iget-object v1, p1, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    if-nez v1, :cond_0

    .line 9360
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    if-nez v1, :cond_e

    .line 9361
    iget-object v1, p1, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    if-nez v1, :cond_0

    .line 9369
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    if-nez v1, :cond_f

    .line 9370
    iget-object v1, p1, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    if-nez v1, :cond_0

    .line 9378
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    if-nez v1, :cond_10

    .line 9379
    iget-object v1, p1, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    if-nez v1, :cond_0

    .line 9387
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    if-nez v1, :cond_11

    .line 9388
    iget-object v1, p1, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    if-nez v1, :cond_0

    .line 9396
    :cond_9
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bo;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 9335
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 9342
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 9348
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 9356
    :cond_d
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    iget-object v2, p1, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/av;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 9365
    :cond_e
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    iget-object v2, p1, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/r;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 9374
    :cond_f
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    iget-object v2, p1, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bc;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 9383
    :cond_10
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    iget-object v2, p1, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bs;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 9392
    :cond_11
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    iget-object v2, p1, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bb;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 9401
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 9404
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 9405
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 9407
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 9409
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 9411
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 9413
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 9415
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    if-nez v2, :cond_7

    :goto_7
    add-int/2addr v0, v1

    .line 9417
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bo;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 9418
    return v0

    .line 9401
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 9404
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    .line 9405
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_2

    .line 9407
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    invoke-virtual {v0}, Lcom/google/ac/b/c/av;->hashCode()I

    move-result v0

    goto :goto_3

    .line 9409
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    invoke-virtual {v0}, Lcom/google/ac/b/c/r;->hashCode()I

    move-result v0

    goto :goto_4

    .line 9411
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    invoke-virtual {v0}, Lcom/google/ac/b/c/bc;->hashCode()I

    move-result v0

    goto :goto_5

    .line 9413
    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    invoke-virtual {v0}, Lcom/google/ac/b/c/bs;->hashCode()I

    move-result v0

    goto :goto_6

    .line 9415
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    invoke-virtual {v1}, Lcom/google/ac/b/c/bb;->hashCode()I

    move-result v1

    goto :goto_7
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 9263
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bo;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/av;

    invoke-direct {v0}, Lcom/google/ac/b/c/av;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/r;

    invoke-direct {v0}, Lcom/google/ac/b/c/r;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/b/c/bc;

    invoke-direct {v0}, Lcom/google/ac/b/c/bc;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/c/bs;

    invoke-direct {v0}, Lcom/google/ac/b/c/bs;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/ac/b/c/bb;

    invoke-direct {v0}, Lcom/google/ac/b/c/bb;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 9424
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 9425
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/bo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 9427
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 9428
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/bo;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 9430
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 9431
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/bo;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 9433
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    if-eqz v0, :cond_3

    .line 9434
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/bo;->d:Lcom/google/ac/b/c/av;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9436
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    if-eqz v0, :cond_4

    .line 9437
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/bo;->e:Lcom/google/ac/b/c/r;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9439
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    if-eqz v0, :cond_5

    .line 9440
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/bo;->f:Lcom/google/ac/b/c/bc;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9442
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    if-eqz v0, :cond_6

    .line 9443
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/b/c/bo;->g:Lcom/google/ac/b/c/bs;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9445
    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    if-eqz v0, :cond_7

    .line 9446
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/ac/b/c/bo;->h:Lcom/google/ac/b/c/bb;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9448
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 9449
    return-void
.end method
