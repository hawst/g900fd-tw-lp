.class public final Lcom/google/ac/b/c/ao;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/ac/b/c/ao;


# instance fields
.field public a:Lcom/google/ac/b/c/al;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2262
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2263
    iput-object v0, p0, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    iput-object v0, p0, Lcom/google/ac/b/c/ao;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/ao;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/ao;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/ao;->cachedSize:I

    .line 2264
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/ao;
    .locals 2

    .prologue
    .line 2242
    sget-object v0, Lcom/google/ac/b/c/ao;->d:[Lcom/google/ac/b/c/ao;

    if-nez v0, :cond_1

    .line 2243
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2245
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/ao;->d:[Lcom/google/ac/b/c/ao;

    if-nez v0, :cond_0

    .line 2246
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/ao;

    sput-object v0, Lcom/google/ac/b/c/ao;->d:[Lcom/google/ac/b/c/ao;

    .line 2248
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2250
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/ao;->d:[Lcom/google/ac/b/c/ao;

    return-object v0

    .line 2248
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2336
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2337
    iget-object v1, p0, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    if-eqz v1, :cond_0

    .line 2338
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2341
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/ao;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 2342
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/ao;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2345
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/ao;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 2346
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/ao;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2349
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2277
    if-ne p1, p0, :cond_1

    .line 2278
    const/4 v0, 0x1

    .line 2305
    :cond_0
    :goto_0
    return v0

    .line 2280
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/ao;

    if-eqz v1, :cond_0

    .line 2283
    check-cast p1, Lcom/google/ac/b/c/ao;

    .line 2284
    iget-object v1, p0, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    if-nez v1, :cond_5

    .line 2285
    iget-object v1, p1, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    if-nez v1, :cond_0

    .line 2293
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ao;->b:Ljava/lang/Integer;

    if-nez v1, :cond_6

    .line 2294
    iget-object v1, p1, Lcom/google/ac/b/c/ao;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 2299
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ao;->c:Ljava/lang/Integer;

    if-nez v1, :cond_7

    .line 2300
    iget-object v1, p1, Lcom/google/ac/b/c/ao;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 2305
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/ao;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2289
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    iget-object v2, p1, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/al;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2297
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/ao;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/ao;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 2303
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/ao;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/ao;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2310
    iget-object v0, p0, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2313
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ao;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2314
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/ao;->c:Ljava/lang/Integer;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 2315
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/ao;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2316
    return v0

    .line 2310
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    invoke-virtual {v0}, Lcom/google/ac/b/c/al;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2313
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ao;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    .line 2314
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ao;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2236
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/ao;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/al;

    invoke-direct {v0}, Lcom/google/ac/b/c/al;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ao;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ao;->c:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2322
    iget-object v0, p0, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    if-eqz v0, :cond_0

    .line 2323
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/ao;->a:Lcom/google/ac/b/c/al;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2325
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ao;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 2326
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/ao;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2328
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ao;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2329
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/ao;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2331
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2332
    return-void
.end method
