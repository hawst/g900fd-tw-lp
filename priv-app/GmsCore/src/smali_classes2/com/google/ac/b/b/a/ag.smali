.class public final Lcom/google/ac/b/b/a/ag;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/ac/b/b/a/ag;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Long;

.field public d:Lcom/google/ac/b/b/a/s;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3135
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3136
    iput-object v0, p0, Lcom/google/ac/b/b/a/ag;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/b/a/ag;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/b/a/ag;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    iput-object v0, p0, Lcom/google/ac/b/b/a/ag;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/ag;->cachedSize:I

    .line 3137
    return-void
.end method

.method public static a()[Lcom/google/ac/b/b/a/ag;
    .locals 2

    .prologue
    .line 3112
    sget-object v0, Lcom/google/ac/b/b/a/ag;->e:[Lcom/google/ac/b/b/a/ag;

    if-nez v0, :cond_1

    .line 3113
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 3115
    :try_start_0
    sget-object v0, Lcom/google/ac/b/b/a/ag;->e:[Lcom/google/ac/b/b/a/ag;

    if-nez v0, :cond_0

    .line 3116
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/b/a/ag;

    sput-object v0, Lcom/google/ac/b/b/a/ag;->e:[Lcom/google/ac/b/b/a/ag;

    .line 3118
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3120
    :cond_1
    sget-object v0, Lcom/google/ac/b/b/a/ag;->e:[Lcom/google/ac/b/b/a/ag;

    return-object v0

    .line 3118
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 3224
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3225
    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 3226
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/ag;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3229
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 3230
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/b/a/ag;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3233
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 3234
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/b/a/ag;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3237
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    if-eqz v1, :cond_3

    .line 3238
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3241
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3151
    if-ne p1, p0, :cond_1

    .line 3152
    const/4 v0, 0x1

    .line 3187
    :cond_0
    :goto_0
    return v0

    .line 3154
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/ag;

    if-eqz v1, :cond_0

    .line 3157
    check-cast p1, Lcom/google/ac/b/b/a/ag;

    .line 3158
    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->a:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 3159
    iget-object v1, p1, Lcom/google/ac/b/b/a/ag;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3165
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->b:Ljava/lang/Integer;

    if-nez v1, :cond_7

    .line 3166
    iget-object v1, p1, Lcom/google/ac/b/b/a/ag;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3171
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->c:Ljava/lang/Long;

    if-nez v1, :cond_8

    .line 3172
    iget-object v1, p1, Lcom/google/ac/b/b/a/ag;->c:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 3178
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    if-nez v1, :cond_9

    .line 3179
    iget-object v1, p1, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    if-nez v1, :cond_0

    .line 3187
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/ag;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 3162
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ag;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 3169
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ag;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 3175
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->c:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ag;->c:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 3183
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/s;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3192
    iget-object v0, p0, Lcom/google/ac/b/b/a/ag;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3195
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/ag;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3196
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/ag;->c:Ljava/lang/Long;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3198
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 3200
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/ag;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3201
    return v0

    .line 3192
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/ag;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3195
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/ag;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1

    .line 3196
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/ag;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_2

    .line 3198
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    invoke-virtual {v1}, Lcom/google/ac/b/b/a/s;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 3106
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/ag;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/ag;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/ag;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/ag;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/b/a/s;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/s;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 3207
    iget-object v0, p0, Lcom/google/ac/b/b/a/ag;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 3208
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3210
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/ag;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3211
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3213
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/ag;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 3214
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3216
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    if-eqz v0, :cond_3

    .line 3217
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/b/a/ag;->d:Lcom/google/ac/b/b/a/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3219
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3220
    return-void
.end method
