.class public final Lcom/google/ac/b/c/cp;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/ac/b/c/ah;

.field public b:[Lcom/google/ac/b/c/bm;

.field public c:Lcom/google/ac/b/c/as;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 429
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 430
    invoke-static {}, Lcom/google/ac/b/c/ah;->a()[Lcom/google/ac/b/c/ah;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    invoke-static {}, Lcom/google/ac/b/c/bm;->a()[Lcom/google/ac/b/c/bm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    iput-object v1, p0, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    iput-object v1, p0, Lcom/google/ac/b/c/cp;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/cp;->cachedSize:I

    .line 431
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 511
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 512
    iget-object v2, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 513
    :goto_0
    iget-object v3, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 514
    iget-object v3, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    aget-object v3, v3, v0

    .line 515
    if-eqz v3, :cond_0

    .line 516
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 513
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 521
    :cond_2
    iget-object v2, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 522
    :goto_1
    iget-object v2, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 523
    iget-object v2, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    aget-object v2, v2, v1

    .line 524
    if-eqz v2, :cond_3

    .line 525
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 522
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 530
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    if-eqz v1, :cond_5

    .line 531
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 534
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 444
    if-ne p1, p0, :cond_1

    .line 445
    const/4 v0, 0x1

    .line 468
    :cond_0
    :goto_0
    return v0

    .line 447
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/cp;

    if-eqz v1, :cond_0

    .line 450
    check-cast p1, Lcom/google/ac/b/c/cp;

    .line 451
    iget-object v1, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    iget-object v2, p1, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 455
    iget-object v1, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    iget-object v2, p1, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 459
    iget-object v1, p0, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    if-nez v1, :cond_3

    .line 460
    iget-object v1, p1, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    if-nez v1, :cond_0

    .line 468
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/cp;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 464
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    iget-object v2, p1, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/as;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 473
    iget-object v0, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 476
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 478
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 480
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/cp;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 481
    return v0

    .line 478
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    invoke-virtual {v0}, Lcom/google/ac/b/c/as;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 403
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/cp;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/ah;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/c/ah;

    invoke-direct {v3}, Lcom/google/ac/b/c/ah;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/c/ah;

    invoke-direct {v3}, Lcom/google/ac/b/c/ah;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/bm;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/ac/b/c/bm;

    invoke-direct {v3}, Lcom/google/ac/b/c/bm;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/ac/b/c/bm;

    invoke-direct {v3}, Lcom/google/ac/b/c/bm;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/ac/b/c/as;

    invoke-direct {v0}, Lcom/google/ac/b/c/as;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    :cond_7
    iget-object v0, p0, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 487
    iget-object v0, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 488
    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 489
    iget-object v2, p0, Lcom/google/ac/b/c/cp;->a:[Lcom/google/ac/b/c/ah;

    aget-object v2, v2, v0

    .line 490
    if-eqz v2, :cond_0

    .line 491
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 488
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 495
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 496
    :goto_1
    iget-object v0, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 497
    iget-object v0, p0, Lcom/google/ac/b/c/cp;->b:[Lcom/google/ac/b/c/bm;

    aget-object v0, v0, v1

    .line 498
    if-eqz v0, :cond_2

    .line 499
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 496
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 503
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    if-eqz v0, :cond_4

    .line 504
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/cp;->c:Lcom/google/ac/b/c/as;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 506
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 507
    return-void
.end method
