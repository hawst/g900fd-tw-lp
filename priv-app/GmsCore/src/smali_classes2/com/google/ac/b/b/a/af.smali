.class public final Lcom/google/ac/b/b/a/af;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/ac/b/b/a/af;


# instance fields
.field public a:[Ljava/lang/String;

.field public b:Lcom/google/ac/b/b/a/q;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 5252
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 5253
    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    iput-object v1, p0, Lcom/google/ac/b/b/a/af;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/af;->cachedSize:I

    .line 5254
    return-void
.end method

.method public static a()[Lcom/google/ac/b/b/a/af;
    .locals 2

    .prologue
    .line 5235
    sget-object v0, Lcom/google/ac/b/b/a/af;->c:[Lcom/google/ac/b/b/a/af;

    if-nez v0, :cond_1

    .line 5236
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 5238
    :try_start_0
    sget-object v0, Lcom/google/ac/b/b/a/af;->c:[Lcom/google/ac/b/b/a/af;

    if-nez v0, :cond_0

    .line 5239
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/b/a/af;

    sput-object v0, Lcom/google/ac/b/b/a/af;->c:[Lcom/google/ac/b/b/a/af;

    .line 5241
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5243
    :cond_1
    sget-object v0, Lcom/google/ac/b/b/a/af;->c:[Lcom/google/ac/b/b/a/af;

    return-object v0

    .line 5241
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 5319
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v3

    .line 5320
    iget-object v1, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v0

    move v2, v0

    .line 5323
    :goto_0
    iget-object v4, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 5324
    iget-object v4, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    aget-object v4, v4, v0

    .line 5325
    if-eqz v4, :cond_0

    .line 5326
    add-int/lit8 v2, v2, 0x1

    .line 5327
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 5323
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5331
    :cond_1
    add-int v0, v3, v1

    .line 5332
    mul-int/lit8 v1, v2, 0x1

    add-int/2addr v0, v1

    .line 5334
    :goto_1
    iget-object v1, p0, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    if-eqz v1, :cond_2

    .line 5335
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 5338
    :cond_2
    return v0

    :cond_3
    move v0, v3

    goto :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 5266
    if-ne p1, p0, :cond_1

    .line 5267
    const/4 v0, 0x1

    .line 5286
    :cond_0
    :goto_0
    return v0

    .line 5269
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/af;

    if-eqz v1, :cond_0

    .line 5272
    check-cast p1, Lcom/google/ac/b/b/a/af;

    .line 5273
    iget-object v1, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 5277
    iget-object v1, p0, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    if-nez v1, :cond_3

    .line 5278
    iget-object v1, p1, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    if-nez v1, :cond_0

    .line 5286
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/af;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 5282
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    iget-object v2, p1, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 5291
    iget-object v0, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 5294
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 5296
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/af;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 5297
    return v0

    .line 5294
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/q;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5229
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/af;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/b/a/q;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/q;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 5303
    iget-object v0, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 5304
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 5305
    iget-object v1, p0, Lcom/google/ac/b/b/a/af;->a:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 5306
    if-eqz v1, :cond_0

    .line 5307
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 5304
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 5311
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    if-eqz v0, :cond_2

    .line 5312
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/b/a/af;->b:Lcom/google/ac/b/b/a/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 5314
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5315
    return-void
.end method
