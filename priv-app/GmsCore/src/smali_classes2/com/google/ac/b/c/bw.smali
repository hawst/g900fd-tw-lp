.class public final Lcom/google/ac/b/c/bw;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/ac/b/c/bw;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:[I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4176
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 4177
    iput-object v1, p0, Lcom/google/ac/b/c/bw;->a:Ljava/lang/Integer;

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/ac/b/c/bw;->b:[I

    iput-object v1, p0, Lcom/google/ac/b/c/bw;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bw;->cachedSize:I

    .line 4178
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/bw;
    .locals 2

    .prologue
    .line 4159
    sget-object v0, Lcom/google/ac/b/c/bw;->c:[Lcom/google/ac/b/c/bw;

    if-nez v0, :cond_1

    .line 4160
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 4162
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/bw;->c:[Lcom/google/ac/b/c/bw;

    if-nez v0, :cond_0

    .line 4163
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/bw;

    sput-object v0, Lcom/google/ac/b/c/bw;->c:[Lcom/google/ac/b/c/bw;

    .line 4165
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4167
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/bw;->c:[Lcom/google/ac/b/c/bw;

    return-object v0

    .line 4165
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 4236
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 4237
    iget-object v2, p0, Lcom/google/ac/b/c/bw;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 4238
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/ac/b/c/bw;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4241
    :cond_0
    iget-object v2, p0, Lcom/google/ac/b/c/bw;->b:[I

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/ac/b/c/bw;->b:[I

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v1

    .line 4243
    :goto_0
    iget-object v3, p0, Lcom/google/ac/b/c/bw;->b:[I

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 4244
    iget-object v3, p0, Lcom/google/ac/b/c/bw;->b:[I

    aget v3, v3, v1

    .line 4245
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 4243
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 4248
    :cond_1
    add-int/2addr v0, v2

    .line 4249
    iget-object v1, p0, Lcom/google/ac/b/c/bw;->b:[I

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4251
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4190
    if-ne p1, p0, :cond_1

    .line 4191
    const/4 v0, 0x1

    .line 4207
    :cond_0
    :goto_0
    return v0

    .line 4193
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bw;

    if-eqz v1, :cond_0

    .line 4196
    check-cast p1, Lcom/google/ac/b/c/bw;

    .line 4197
    iget-object v1, p0, Lcom/google/ac/b/c/bw;->a:Ljava/lang/Integer;

    if-nez v1, :cond_3

    .line 4198
    iget-object v1, p1, Lcom/google/ac/b/c/bw;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 4203
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bw;->b:[I

    iget-object v2, p1, Lcom/google/ac/b/c/bw;->b:[I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4207
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bw;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 4201
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bw;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/bw;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 4212
    iget-object v0, p0, Lcom/google/ac/b/c/bw;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4214
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/bw;->b:[I

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v1

    add-int/2addr v0, v1

    .line 4216
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bw;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4217
    return v0

    .line 4212
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bw;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 4153
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bw;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/bw;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x10

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v4

    new-array v5, v4, [I

    move v3, v2

    move v1, v2

    :goto_1
    if-ge v3, v4, :cond_2

    if-eqz v3, :cond_1

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    :cond_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v6

    packed-switch v6, :pswitch_data_1

    move v0, v1

    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    :pswitch_1
    add-int/lit8 v0, v1, 0x1

    aput v6, v5, v1

    goto :goto_2

    :cond_2
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/google/ac/b/c/bw;->b:[I

    if-nez v0, :cond_3

    move v0, v2

    :goto_3
    if-nez v0, :cond_4

    array-length v3, v5

    if-ne v1, v3, :cond_4

    iput-object v5, p0, Lcom/google/ac/b/c/bw;->b:[I

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/bw;->b:[I

    array-length v0, v0

    goto :goto_3

    :cond_4
    add-int v3, v0, v1

    new-array v3, v3, [I

    if-eqz v0, :cond_5

    iget-object v4, p0, Lcom/google/ac/b/c/bw;->b:[I

    invoke-static {v4, v2, v3, v2, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    invoke-static {v5, v2, v3, v0, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iput-object v3, p0, Lcom/google/ac/b/c/bw;->b:[I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v1

    move v0, v2

    :goto_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    packed-switch v4, :pswitch_data_2

    goto :goto_4

    :pswitch_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    if-eqz v0, :cond_a

    invoke-virtual {p1, v1}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v1, p0, Lcom/google/ac/b/c/bw;->b:[I

    if-nez v1, :cond_8

    move v1, v2

    :goto_5
    add-int/2addr v0, v1

    new-array v4, v0, [I

    if-eqz v1, :cond_7

    iget-object v0, p0, Lcom/google/ac/b/c/bw;->b:[I

    invoke-static {v0, v2, v4, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v0

    if-lez v0, :cond_9

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v5

    packed-switch v5, :pswitch_data_3

    goto :goto_6

    :pswitch_3
    add-int/lit8 v0, v1, 0x1

    aput v5, v4, v1

    move v1, v0

    goto :goto_6

    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/bw;->b:[I

    array-length v1, v1

    goto :goto_5

    :cond_9
    iput-object v4, p0, Lcom/google/ac/b/c/bw;->b:[I

    :cond_a
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x12 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_2
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_3
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 4223
    iget-object v0, p0, Lcom/google/ac/b/c/bw;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 4224
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/bw;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4226
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bw;->b:[I

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/bw;->b:[I

    array-length v0, v0

    if-lez v0, :cond_1

    .line 4227
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/c/bw;->b:[I

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 4228
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/bw;->b:[I

    aget v2, v2, v0

    invoke-virtual {p1, v1, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4227
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4231
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4232
    return-void
.end method
