.class public final Lcom/google/ac/b/c/dr;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/ac/b/c/dt;

.field public b:Lcom/google/ac/b/c/dt;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 716
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 717
    invoke-static {}, Lcom/google/ac/b/c/dt;->a()[Lcom/google/ac/b/c/dt;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    iput-object v1, p0, Lcom/google/ac/b/c/dr;->b:Lcom/google/ac/b/c/dt;

    iput-object v1, p0, Lcom/google/ac/b/c/dr;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/dr;->cachedSize:I

    .line 718
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 783
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 784
    iget-object v0, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 785
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 786
    iget-object v2, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    aget-object v2, v2, v0

    .line 787
    if-eqz v2, :cond_0

    .line 788
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 785
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 793
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dr;->b:Lcom/google/ac/b/c/dt;

    if-eqz v0, :cond_2

    .line 794
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/dr;->b:Lcom/google/ac/b/c/dt;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 797
    :cond_2
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 730
    if-ne p1, p0, :cond_1

    .line 731
    const/4 v0, 0x1

    .line 750
    :cond_0
    :goto_0
    return v0

    .line 733
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/dr;

    if-eqz v1, :cond_0

    .line 736
    check-cast p1, Lcom/google/ac/b/c/dr;

    .line 737
    iget-object v1, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    iget-object v2, p1, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 741
    iget-object v1, p0, Lcom/google/ac/b/c/dr;->b:Lcom/google/ac/b/c/dt;

    if-nez v1, :cond_3

    .line 742
    iget-object v1, p1, Lcom/google/ac/b/c/dr;->b:Lcom/google/ac/b/c/dt;

    if-nez v1, :cond_0

    .line 750
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/dr;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 746
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/dr;->b:Lcom/google/ac/b/c/dt;

    iget-object v2, p1, Lcom/google/ac/b/c/dr;->b:Lcom/google/ac/b/c/dt;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/dt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 755
    iget-object v0, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 758
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/dr;->b:Lcom/google/ac/b/c/dt;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 760
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/dr;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 761
    return v0

    .line 758
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/dr;->b:Lcom/google/ac/b/c/dt;

    invoke-virtual {v0}, Lcom/google/ac/b/c/dt;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 693
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/dr;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/dt;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/c/dt;

    invoke-direct {v3}, Lcom/google/ac/b/c/dt;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/c/dt;

    invoke-direct {v3}, Lcom/google/ac/b/c/dt;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/dr;->b:Lcom/google/ac/b/c/dt;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/c/dt;

    invoke-direct {v0}, Lcom/google/ac/b/c/dt;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/dr;->b:Lcom/google/ac/b/c/dt;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/dr;->b:Lcom/google/ac/b/c/dt;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 767
    iget-object v0, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 768
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 769
    iget-object v1, p0, Lcom/google/ac/b/c/dr;->a:[Lcom/google/ac/b/c/dt;

    aget-object v1, v1, v0

    .line 770
    if-eqz v1, :cond_0

    .line 771
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 768
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 775
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dr;->b:Lcom/google/ac/b/c/dt;

    if-eqz v0, :cond_2

    .line 776
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/dr;->b:Lcom/google/ac/b/c/dt;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 778
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 779
    return-void
.end method
