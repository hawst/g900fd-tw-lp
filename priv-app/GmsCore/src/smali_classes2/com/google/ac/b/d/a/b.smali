.class public final Lcom/google/ac/b/d/a/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 346
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 347
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ac/b/d/a/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/d/a/b;->cachedSize:I

    .line 348
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 358
    if-ne p1, p0, :cond_0

    .line 359
    const/4 v0, 0x1

    .line 365
    :goto_0
    return v0

    .line 361
    :cond_0
    instance-of v0, p1, Lcom/google/ac/b/d/a/b;

    if-nez v0, :cond_1

    .line 362
    const/4 v0, 0x0

    goto :goto_0

    .line 364
    :cond_1
    check-cast p1, Lcom/google/ac/b/d/a/b;

    .line 365
    invoke-virtual {p0, p1}, Lcom/google/ac/b/d/a/b;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 370
    invoke-virtual {p0}, Lcom/google/ac/b/d/a/b;->unknownFieldDataHashCode()I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 372
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/d/a/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :pswitch_0
    return-object p0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
