.class public final Lcom/google/ac/b/c/cn;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 641
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 642
    iput-object v0, p0, Lcom/google/ac/b/c/cn;->a:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/cn;->b:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/cn;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/cn;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/cn;->cachedSize:I

    .line 643
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 717
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 718
    iget-object v1, p0, Lcom/google/ac/b/c/cn;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 719
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/cn;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 722
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/cn;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 723
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/cn;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 726
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/cn;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 727
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/b/c/cn;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 730
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 656
    if-ne p1, p0, :cond_1

    .line 657
    const/4 v0, 0x1

    .line 684
    :cond_0
    :goto_0
    return v0

    .line 659
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/cn;

    if-eqz v1, :cond_0

    .line 662
    check-cast p1, Lcom/google/ac/b/c/cn;

    .line 663
    iget-object v1, p0, Lcom/google/ac/b/c/cn;->a:Ljava/lang/Long;

    if-nez v1, :cond_5

    .line 664
    iget-object v1, p1, Lcom/google/ac/b/c/cn;->a:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 670
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/cn;->b:Ljava/lang/Long;

    if-nez v1, :cond_6

    .line 671
    iget-object v1, p1, Lcom/google/ac/b/c/cn;->b:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 677
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/cn;->c:Ljava/lang/Integer;

    if-nez v1, :cond_7

    .line 678
    iget-object v1, p1, Lcom/google/ac/b/c/cn;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 684
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/cn;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 667
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/cn;->a:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/cn;->a:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 674
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/cn;->b:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/cn;->b:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 681
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/cn;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/cn;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 689
    iget-object v0, p0, Lcom/google/ac/b/c/cn;->a:Ljava/lang/Long;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 692
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/cn;->b:Ljava/lang/Long;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 694
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/cn;->c:Ljava/lang/Integer;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 696
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/cn;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 697
    return v0

    .line 689
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cn;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_0

    .line 692
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cn;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_1

    .line 694
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/cn;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 615
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/cn;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cn;->a:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cn;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cn;->c:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x38 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 703
    iget-object v0, p0, Lcom/google/ac/b/c/cn;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 704
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/cn;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 706
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cn;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 707
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/cn;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 709
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cn;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 710
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/b/c/cn;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 712
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 713
    return-void
.end method
