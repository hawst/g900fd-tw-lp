.class public final Lcom/google/ac/b/c/dg;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:[Lcom/google/ac/b/c/bq;

.field public c:[Lcom/google/ac/b/c/bm;

.field public d:[Lcom/google/ac/b/c/ah;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3611
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3612
    iput-object v1, p0, Lcom/google/ac/b/c/dg;->a:Ljava/lang/Integer;

    invoke-static {}, Lcom/google/ac/b/c/bq;->a()[Lcom/google/ac/b/c/bq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    invoke-static {}, Lcom/google/ac/b/c/bm;->a()[Lcom/google/ac/b/c/bm;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    invoke-static {}, Lcom/google/ac/b/c/ah;->a()[Lcom/google/ac/b/c/ah;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    iput-object v1, p0, Lcom/google/ac/b/c/dg;->e:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/ac/b/c/dg;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/dg;->cachedSize:I

    .line 3613
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 3717
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3718
    iget-object v2, p0, Lcom/google/ac/b/c/dg;->a:Ljava/lang/Integer;

    if-eqz v2, :cond_0

    .line 3719
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/ac/b/c/dg;->a:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 3722
    :cond_0
    iget-object v2, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 3723
    :goto_0
    iget-object v3, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 3724
    iget-object v3, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    aget-object v3, v3, v0

    .line 3725
    if-eqz v3, :cond_1

    .line 3726
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3723
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 3731
    :cond_3
    iget-object v2, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    array-length v2, v2

    if-lez v2, :cond_6

    move v2, v0

    move v0, v1

    .line 3732
    :goto_1
    iget-object v3, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 3733
    iget-object v3, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    aget-object v3, v3, v0

    .line 3734
    if-eqz v3, :cond_4

    .line 3735
    const/4 v4, 0x3

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 3732
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v0, v2

    .line 3740
    :cond_6
    iget-object v2, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    array-length v2, v2

    if-lez v2, :cond_8

    .line 3741
    :goto_2
    iget-object v2, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    array-length v2, v2

    if-ge v1, v2, :cond_8

    .line 3742
    iget-object v2, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    aget-object v2, v2, v1

    .line 3743
    if-eqz v2, :cond_7

    .line 3744
    const/4 v3, 0x4

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3741
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3749
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/dg;->e:Ljava/lang/String;

    if-eqz v1, :cond_9

    .line 3750
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/dg;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3753
    :cond_9
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3628
    if-ne p1, p0, :cond_1

    .line 3629
    const/4 v0, 0x1

    .line 3660
    :cond_0
    :goto_0
    return v0

    .line 3631
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/dg;

    if-eqz v1, :cond_0

    .line 3634
    check-cast p1, Lcom/google/ac/b/c/dg;

    .line 3635
    iget-object v1, p0, Lcom/google/ac/b/c/dg;->a:Ljava/lang/Integer;

    if-nez v1, :cond_4

    .line 3636
    iget-object v1, p1, Lcom/google/ac/b/c/dg;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3641
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    iget-object v2, p1, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3645
    iget-object v1, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    iget-object v2, p1, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3649
    iget-object v1, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    iget-object v2, p1, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3653
    iget-object v1, p0, Lcom/google/ac/b/c/dg;->e:Ljava/lang/String;

    if-nez v1, :cond_5

    .line 3654
    iget-object v1, p1, Lcom/google/ac/b/c/dg;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 3660
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/dg;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 3639
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/dg;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/dg;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 3657
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/dg;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/dg;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3665
    iget-object v0, p0, Lcom/google/ac/b/c/dg;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3667
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3669
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3671
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 3673
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/dg;->e:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 3675
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/dg;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3676
    return v0

    .line 3665
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/dg;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 3673
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/dg;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3579
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/dg;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dg;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/bq;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/c/bq;

    invoke-direct {v3}, Lcom/google/ac/b/c/bq;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/c/bq;

    invoke-direct {v3}, Lcom/google/ac/b/c/bq;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/bm;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/ac/b/c/bm;

    invoke-direct {v3}, Lcom/google/ac/b/c/bm;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/ac/b/c/bm;

    invoke-direct {v3}, Lcom/google/ac/b/c/bm;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    goto/16 :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    if-nez v0, :cond_8

    move v0, v1

    :goto_5
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/ah;

    if-eqz v0, :cond_7

    iget-object v3, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_7
    :goto_6
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_9

    new-instance v3, Lcom/google/ac/b/c/ah;

    invoke-direct {v3}, Lcom/google/ac/b/c/ah;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :cond_8
    iget-object v0, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    array-length v0, v0

    goto :goto_5

    :cond_9
    new-instance v3, Lcom/google/ac/b/c/ah;

    invoke-direct {v3}, Lcom/google/ac/b/c/ah;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    goto/16 :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dg;->e:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3682
    iget-object v0, p0, Lcom/google/ac/b/c/dg;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3683
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/dg;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3685
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 3686
    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 3687
    iget-object v2, p0, Lcom/google/ac/b/c/dg;->b:[Lcom/google/ac/b/c/bq;

    aget-object v2, v2, v0

    .line 3688
    if-eqz v2, :cond_1

    .line 3689
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3686
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3693
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 3694
    :goto_1
    iget-object v2, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 3695
    iget-object v2, p0, Lcom/google/ac/b/c/dg;->c:[Lcom/google/ac/b/c/bm;

    aget-object v2, v2, v0

    .line 3696
    if-eqz v2, :cond_3

    .line 3697
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3694
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 3701
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 3702
    :goto_2
    iget-object v0, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 3703
    iget-object v0, p0, Lcom/google/ac/b/c/dg;->d:[Lcom/google/ac/b/c/ah;

    aget-object v0, v0, v1

    .line 3704
    if-eqz v0, :cond_5

    .line 3705
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3702
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 3709
    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/dg;->e:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 3710
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/dg;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3712
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3713
    return-void
.end method
