.class public final Lcom/google/ac/b/b/a/m;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/ac/b/b/a/v;

.field public b:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 2015
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2016
    invoke-static {}, Lcom/google/ac/b/b/a/v;->a()[Lcom/google/ac/b/b/a/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ac/b/b/a/m;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/m;->cachedSize:I

    .line 2017
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2082
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2083
    iget-object v2, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 2084
    :goto_0
    iget-object v3, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 2085
    iget-object v3, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    aget-object v3, v3, v0

    .line 2086
    if-eqz v3, :cond_0

    .line 2087
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2084
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 2092
    :cond_2
    iget-object v2, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v1

    move v3, v1

    .line 2095
    :goto_1
    iget-object v4, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_4

    .line 2096
    iget-object v4, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 2097
    if-eqz v4, :cond_3

    .line 2098
    add-int/lit8 v3, v3, 0x1

    .line 2099
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 2095
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2103
    :cond_4
    add-int/2addr v0, v2

    .line 2104
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 2106
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2029
    if-ne p1, p0, :cond_1

    .line 2030
    const/4 v0, 0x1

    .line 2044
    :cond_0
    :goto_0
    return v0

    .line 2032
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/m;

    if-eqz v1, :cond_0

    .line 2035
    check-cast p1, Lcom/google/ac/b/b/a/m;

    .line 2036
    iget-object v1, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    iget-object v2, p1, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2040
    iget-object v1, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2044
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/m;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 2049
    iget-object v0, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 2052
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2054
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/m;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2055
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1992
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/m;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/b/a/v;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/b/a/v;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/v;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/b/a/v;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/v;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2061
    iget-object v0, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 2062
    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2063
    iget-object v2, p0, Lcom/google/ac/b/b/a/m;->a:[Lcom/google/ac/b/b/a/v;

    aget-object v2, v2, v0

    .line 2064
    if-eqz v2, :cond_0

    .line 2065
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2062
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2069
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 2070
    :goto_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 2071
    iget-object v0, p0, Lcom/google/ac/b/b/a/m;->b:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 2072
    if-eqz v0, :cond_2

    .line 2073
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2070
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2077
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2078
    return-void
.end method
