.class public final Lcom/google/ac/b/c/b;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Boolean;

.field public d:Ljava/lang/Double;

.field public e:Ljava/lang/Boolean;

.field public f:Ljava/lang/Integer;

.field public g:Ljava/lang/Integer;

.field public h:Ljava/lang/Double;

.field public i:Ljava/lang/Integer;

.field public j:Ljava/lang/Integer;

.field public k:Ljava/lang/Integer;

.field public l:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3335
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3336
    iput-object v0, p0, Lcom/google/ac/b/c/b;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/b;->b:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/c/b;->c:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/c/b;->d:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/ac/b/c/b;->e:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/c/b;->f:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/b;->g:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/b;->h:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/ac/b/c/b;->i:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/b;->j:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/b;->k:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/b;->l:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/b;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/b;->cachedSize:I

    .line 3337
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3528
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3529
    iget-object v1, p0, Lcom/google/ac/b/c/b;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 3530
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/b;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3533
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/b;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_1

    .line 3534
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3537
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/b;->c:Ljava/lang/Boolean;

    if-eqz v1, :cond_2

    .line 3538
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/b;->c:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3541
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/b;->d:Ljava/lang/Double;

    if-eqz v1, :cond_3

    .line 3542
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/b;->d:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 3545
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/b;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 3546
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/b;->e:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 3549
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/b;->f:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 3550
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/b;->f:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3553
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/b;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 3554
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/b/c/b;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3557
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/b;->h:Ljava/lang/Double;

    if-eqz v1, :cond_7

    .line 3558
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/ac/b/c/b;->h:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 3561
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/b;->i:Ljava/lang/Integer;

    if-eqz v1, :cond_8

    .line 3562
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/ac/b/c/b;->i:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3565
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/b;->j:Ljava/lang/Integer;

    if-eqz v1, :cond_9

    .line 3566
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/ac/b/c/b;->j:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3569
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/b;->k:Ljava/lang/Integer;

    if-eqz v1, :cond_a

    .line 3570
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/ac/b/c/b;->k:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3573
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/b;->l:Ljava/lang/Integer;

    if-eqz v1, :cond_b

    .line 3574
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/ac/b/c/b;->l:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3577
    :cond_b
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3359
    if-ne p1, p0, :cond_1

    .line 3360
    const/4 v0, 0x1

    .line 3450
    :cond_0
    :goto_0
    return v0

    .line 3362
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/b;

    if-eqz v1, :cond_0

    .line 3365
    check-cast p1, Lcom/google/ac/b/c/b;

    .line 3366
    iget-object v1, p0, Lcom/google/ac/b/c/b;->a:Ljava/lang/Integer;

    if-nez v1, :cond_e

    .line 3367
    iget-object v1, p1, Lcom/google/ac/b/c/b;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3373
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/b;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_f

    .line 3374
    iget-object v1, p1, Lcom/google/ac/b/c/b;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 3380
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/b;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_10

    .line 3381
    iget-object v1, p1, Lcom/google/ac/b/c/b;->c:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 3387
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/b;->d:Ljava/lang/Double;

    if-nez v1, :cond_11

    .line 3388
    iget-object v1, p1, Lcom/google/ac/b/c/b;->d:Ljava/lang/Double;

    if-nez v1, :cond_0

    .line 3394
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/b;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_12

    .line 3395
    iget-object v1, p1, Lcom/google/ac/b/c/b;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 3401
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/b;->f:Ljava/lang/Integer;

    if-nez v1, :cond_13

    .line 3402
    iget-object v1, p1, Lcom/google/ac/b/c/b;->f:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3408
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/b;->g:Ljava/lang/Integer;

    if-nez v1, :cond_14

    .line 3409
    iget-object v1, p1, Lcom/google/ac/b/c/b;->g:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3415
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/b;->h:Ljava/lang/Double;

    if-nez v1, :cond_15

    .line 3416
    iget-object v1, p1, Lcom/google/ac/b/c/b;->h:Ljava/lang/Double;

    if-nez v1, :cond_0

    .line 3422
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/b;->i:Ljava/lang/Integer;

    if-nez v1, :cond_16

    .line 3423
    iget-object v1, p1, Lcom/google/ac/b/c/b;->i:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3429
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/b;->j:Ljava/lang/Integer;

    if-nez v1, :cond_17

    .line 3430
    iget-object v1, p1, Lcom/google/ac/b/c/b;->j:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3436
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/b;->k:Ljava/lang/Integer;

    if-nez v1, :cond_18

    .line 3437
    iget-object v1, p1, Lcom/google/ac/b/c/b;->k:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3443
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/c/b;->l:Ljava/lang/Integer;

    if-nez v1, :cond_19

    .line 3444
    iget-object v1, p1, Lcom/google/ac/b/c/b;->l:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3450
    :cond_d
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/b;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 3370
    :cond_e
    iget-object v1, p0, Lcom/google/ac/b/c/b;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/b;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 3377
    :cond_f
    iget-object v1, p0, Lcom/google/ac/b/c/b;->b:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto/16 :goto_0

    .line 3384
    :cond_10
    iget-object v1, p0, Lcom/google/ac/b/c/b;->c:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/b;->c:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto/16 :goto_0

    .line 3391
    :cond_11
    iget-object v1, p0, Lcom/google/ac/b/c/b;->d:Ljava/lang/Double;

    iget-object v2, p1, Lcom/google/ac/b/c/b;->d:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 3398
    :cond_12
    iget-object v1, p0, Lcom/google/ac/b/c/b;->e:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/b;->e:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 3405
    :cond_13
    iget-object v1, p0, Lcom/google/ac/b/c/b;->f:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/b;->f:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 3412
    :cond_14
    iget-object v1, p0, Lcom/google/ac/b/c/b;->g:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/b;->g:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 3419
    :cond_15
    iget-object v1, p0, Lcom/google/ac/b/c/b;->h:Ljava/lang/Double;

    iget-object v2, p1, Lcom/google/ac/b/c/b;->h:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0

    .line 3426
    :cond_16
    iget-object v1, p0, Lcom/google/ac/b/c/b;->i:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/b;->i:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    goto/16 :goto_0

    .line 3433
    :cond_17
    iget-object v1, p0, Lcom/google/ac/b/c/b;->j:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/b;->j:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    goto/16 :goto_0

    .line 3440
    :cond_18
    iget-object v1, p0, Lcom/google/ac/b/c/b;->k:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/b;->k:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_c

    goto/16 :goto_0

    .line 3447
    :cond_19
    iget-object v1, p0, Lcom/google/ac/b/c/b;->l:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/b;->l:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_d

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3455
    iget-object v0, p0, Lcom/google/ac/b/c/b;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3458
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/b;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3460
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/b;->c:Ljava/lang/Boolean;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3462
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/b;->d:Ljava/lang/Double;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 3464
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/b;->e:Ljava/lang/Boolean;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 3466
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/b;->f:Ljava/lang/Integer;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 3468
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/b;->g:Ljava/lang/Integer;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 3470
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/b;->h:Ljava/lang/Double;

    if-nez v0, :cond_7

    move v0, v1

    :goto_7
    add-int/2addr v0, v2

    .line 3472
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/b;->i:Ljava/lang/Integer;

    if-nez v0, :cond_8

    move v0, v1

    :goto_8
    add-int/2addr v0, v2

    .line 3474
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/b;->j:Ljava/lang/Integer;

    if-nez v0, :cond_9

    move v0, v1

    :goto_9
    add-int/2addr v0, v2

    .line 3476
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/b;->k:Ljava/lang/Integer;

    if-nez v0, :cond_a

    move v0, v1

    :goto_a
    add-int/2addr v0, v2

    .line 3478
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/b;->l:Ljava/lang/Integer;

    if-nez v2, :cond_b

    :goto_b
    add-int/2addr v0, v1

    .line 3480
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/b;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3481
    return v0

    .line 3455
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/b;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3458
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3460
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/b;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_2

    .line 3462
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/b;->d:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto :goto_3

    .line 3464
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/b;->e:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_4

    .line 3466
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/b;->f:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_5

    .line 3468
    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/b;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_6

    .line 3470
    :cond_7
    iget-object v0, p0, Lcom/google/ac/b/c/b;->h:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto :goto_7

    .line 3472
    :cond_8
    iget-object v0, p0, Lcom/google/ac/b/c/b;->i:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_8

    .line 3474
    :cond_9
    iget-object v0, p0, Lcom/google/ac/b/c/b;->j:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_9

    .line 3476
    :cond_a
    iget-object v0, p0, Lcom/google/ac/b/c/b;->k:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_a

    .line 3478
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/b;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_b
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 3282
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/b;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/b;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/b;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/b;->c:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/b;->d:Ljava/lang/Double;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/b;->e:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/b;->f:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/b;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/b;->h:Ljava/lang/Double;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/b;->i:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/b;->j:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/b;->k:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/b;->l:Ljava/lang/Integer;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x21 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x41 -> :sswitch_8
        0x48 -> :sswitch_9
        0x50 -> :sswitch_a
        0x58 -> :sswitch_b
        0x60 -> :sswitch_c
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 3487
    iget-object v0, p0, Lcom/google/ac/b/c/b;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3488
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/b;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3490
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/b;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_1

    .line 3491
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/b;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3493
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/b;->c:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3494
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/b;->c:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3496
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/b;->d:Ljava/lang/Double;

    if-eqz v0, :cond_3

    .line 3497
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/b;->d:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 3499
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/b;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 3500
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/b;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3502
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/b;->f:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 3503
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/b;->f:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3505
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/b;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 3506
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/b/c/b;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3508
    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/b;->h:Ljava/lang/Double;

    if-eqz v0, :cond_7

    .line 3509
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/ac/b/c/b;->h:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 3511
    :cond_7
    iget-object v0, p0, Lcom/google/ac/b/c/b;->i:Ljava/lang/Integer;

    if-eqz v0, :cond_8

    .line 3512
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/ac/b/c/b;->i:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3514
    :cond_8
    iget-object v0, p0, Lcom/google/ac/b/c/b;->j:Ljava/lang/Integer;

    if-eqz v0, :cond_9

    .line 3515
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/ac/b/c/b;->j:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3517
    :cond_9
    iget-object v0, p0, Lcom/google/ac/b/c/b;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_a

    .line 3518
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/ac/b/c/b;->k:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3520
    :cond_a
    iget-object v0, p0, Lcom/google/ac/b/c/b;->l:Ljava/lang/Integer;

    if-eqz v0, :cond_b

    .line 3521
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/ac/b/c/b;->l:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3523
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3524
    return-void
.end method
