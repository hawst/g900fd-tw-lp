.class public final Lcom/google/ac/b/b/a/x;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/ac/b/b/a/ak;

.field public b:Lcom/google/ac/b/b/a/m;

.field public c:Lcom/google/ac/b/b/a/o;

.field public d:Lcom/google/ac/b/b/a/an;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1798
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1799
    invoke-static {}, Lcom/google/ac/b/b/a/ak;->a()[Lcom/google/ac/b/b/a/ak;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    iput-object v1, p0, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    iput-object v1, p0, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    iput-object v1, p0, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    iput-object v1, p0, Lcom/google/ac/b/b/a/x;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/x;->cachedSize:I

    .line 1800
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 1895
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 1896
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1897
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 1898
    iget-object v2, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    aget-object v2, v2, v0

    .line 1899
    if-eqz v2, :cond_0

    .line 1900
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 1897
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1905
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    if-eqz v0, :cond_2

    .line 1906
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1909
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    if-eqz v0, :cond_3

    .line 1910
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1913
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    if-eqz v0, :cond_4

    .line 1914
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1917
    :cond_4
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1814
    if-ne p1, p0, :cond_1

    .line 1815
    const/4 v0, 0x1

    .line 1852
    :cond_0
    :goto_0
    return v0

    .line 1817
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/x;

    if-eqz v1, :cond_0

    .line 1820
    check-cast p1, Lcom/google/ac/b/b/a/x;

    .line 1821
    iget-object v1, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    iget-object v2, p1, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1825
    iget-object v1, p0, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    if-nez v1, :cond_5

    .line 1826
    iget-object v1, p1, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    if-nez v1, :cond_0

    .line 1834
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    if-nez v1, :cond_6

    .line 1835
    iget-object v1, p1, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    if-nez v1, :cond_0

    .line 1843
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    if-nez v1, :cond_7

    .line 1844
    iget-object v1, p1, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    if-nez v1, :cond_0

    .line 1852
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/x;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1830
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    iget-object v2, p1, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/m;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1839
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    iget-object v2, p1, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/o;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 1848
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    iget-object v2, p1, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/an;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1857
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 1860
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 1862
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1864
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 1866
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/x;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1867
    return v0

    .line 1860
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/m;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1862
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/o;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1864
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    invoke-virtual {v1}, Lcom/google/ac/b/b/a/an;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1769
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/x;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/b/a/ak;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/b/a/ak;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/ak;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/b/a/ak;

    invoke-direct {v3}, Lcom/google/ac/b/b/a/ak;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/b/a/m;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/m;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/ac/b/b/a/o;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/o;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/ac/b/b/a/an;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/an;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 1873
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 1874
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 1875
    iget-object v1, p0, Lcom/google/ac/b/b/a/x;->a:[Lcom/google/ac/b/b/a/ak;

    aget-object v1, v1, v0

    .line 1876
    if-eqz v1, :cond_0

    .line 1877
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1874
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1881
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    if-eqz v0, :cond_2

    .line 1882
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/b/a/x;->b:Lcom/google/ac/b/b/a/m;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1884
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    if-eqz v0, :cond_3

    .line 1885
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/b/a/x;->c:Lcom/google/ac/b/b/a/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1887
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    if-eqz v0, :cond_4

    .line 1888
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/b/a/x;->d:Lcom/google/ac/b/b/a/an;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1890
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1891
    return-void
.end method
