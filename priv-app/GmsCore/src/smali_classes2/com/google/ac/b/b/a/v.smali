.class public final Lcom/google/ac/b/b/a/v;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/ac/b/b/a/v;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/ac/b/b/a/g;

.field public c:Lcom/google/ac/b/b/a/q;

.field public d:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2206
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2207
    iput-object v0, p0, Lcom/google/ac/b/b/a/v;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    iput-object v0, p0, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    iput-object v0, p0, Lcom/google/ac/b/b/a/v;->d:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/b/a/v;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/v;->cachedSize:I

    .line 2208
    return-void
.end method

.method public static a()[Lcom/google/ac/b/b/a/v;
    .locals 2

    .prologue
    .line 2183
    sget-object v0, Lcom/google/ac/b/b/a/v;->e:[Lcom/google/ac/b/b/a/v;

    if-nez v0, :cond_1

    .line 2184
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2186
    :try_start_0
    sget-object v0, Lcom/google/ac/b/b/a/v;->e:[Lcom/google/ac/b/b/a/v;

    if-nez v0, :cond_0

    .line 2187
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/b/a/v;

    sput-object v0, Lcom/google/ac/b/b/a/v;->e:[Lcom/google/ac/b/b/a/v;

    .line 2189
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2191
    :cond_1
    sget-object v0, Lcom/google/ac/b/b/a/v;->e:[Lcom/google/ac/b/b/a/v;

    return-object v0

    .line 2189
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2297
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2298
    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2299
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/v;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2302
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    if-eqz v1, :cond_1

    .line 2303
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2306
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    if-eqz v1, :cond_2

    .line 2307
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2310
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_3

    .line 2311
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/b/a/v;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2314
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2222
    if-ne p1, p0, :cond_1

    .line 2223
    const/4 v0, 0x1

    .line 2260
    :cond_0
    :goto_0
    return v0

    .line 2225
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/v;

    if-eqz v1, :cond_0

    .line 2228
    check-cast p1, Lcom/google/ac/b/b/a/v;

    .line 2229
    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->a:Ljava/lang/String;

    if-nez v1, :cond_6

    .line 2230
    iget-object v1, p1, Lcom/google/ac/b/b/a/v;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2236
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    if-nez v1, :cond_7

    .line 2237
    iget-object v1, p1, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    if-nez v1, :cond_0

    .line 2245
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    if-nez v1, :cond_8

    .line 2246
    iget-object v1, p1, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    if-nez v1, :cond_0

    .line 2254
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->d:Ljava/lang/Integer;

    if-nez v1, :cond_9

    .line 2255
    iget-object v1, p1, Lcom/google/ac/b/b/a/v;->d:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 2260
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/v;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2233
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/b/a/v;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2241
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    iget-object v2, p1, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/g;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 2250
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    iget-object v2, p1, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 2258
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->d:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/b/a/v;->d:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2265
    iget-object v0, p0, Lcom/google/ac/b/b/a/v;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2268
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2270
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2272
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/b/a/v;->d:Ljava/lang/Integer;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 2273
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/v;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2274
    return v0

    .line 2265
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/v;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2268
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/g;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2270
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/q;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2272
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2177
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/v;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/v;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/b/a/g;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/g;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/b/a/q;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/q;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/v;->d:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2280
    iget-object v0, p0, Lcom/google/ac/b/b/a/v;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2281
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2283
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    if-eqz v0, :cond_1

    .line 2284
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->b:Lcom/google/ac/b/b/a/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2286
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    if-eqz v0, :cond_2

    .line 2287
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->c:Lcom/google/ac/b/b/a/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2289
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/v;->d:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 2290
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/b/a/v;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2292
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2293
    return-void
.end method
