.class public final Lcom/google/ac/b/c/as;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Double;

.field public d:Ljava/lang/Double;

.field public e:Ljava/lang/Double;

.field public f:Ljava/lang/Double;

.field public g:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3623
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3624
    iput-object v0, p0, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/as;->c:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/ac/b/c/as;->d:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/ac/b/c/as;->e:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/ac/b/c/as;->f:Ljava/lang/Double;

    iput-object v0, p0, Lcom/google/ac/b/c/as;->g:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/as;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/as;->cachedSize:I

    .line 3625
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 3751
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3752
    iget-object v1, p0, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 3753
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3756
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 3757
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3760
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/as;->c:Ljava/lang/Double;

    if-eqz v1, :cond_2

    .line 3761
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/as;->c:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 3764
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/as;->d:Ljava/lang/Double;

    if-eqz v1, :cond_3

    .line 3765
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/as;->d:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 3768
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/as;->e:Ljava/lang/Double;

    if-eqz v1, :cond_4

    .line 3769
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/as;->e:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 3772
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/as;->f:Ljava/lang/Double;

    if-eqz v1, :cond_5

    .line 3773
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/as;->f:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 3776
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/as;->g:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 3777
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/b/c/as;->g:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3780
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3642
    if-ne p1, p0, :cond_1

    .line 3643
    const/4 v0, 0x1

    .line 3698
    :cond_0
    :goto_0
    return v0

    .line 3645
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/as;

    if-eqz v1, :cond_0

    .line 3648
    check-cast p1, Lcom/google/ac/b/c/as;

    .line 3649
    iget-object v1, p0, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    if-nez v1, :cond_9

    .line 3650
    iget-object v1, p1, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3656
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    if-nez v1, :cond_a

    .line 3657
    iget-object v1, p1, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 3663
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/as;->c:Ljava/lang/Double;

    if-nez v1, :cond_b

    .line 3664
    iget-object v1, p1, Lcom/google/ac/b/c/as;->c:Ljava/lang/Double;

    if-nez v1, :cond_0

    .line 3670
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/as;->d:Ljava/lang/Double;

    if-nez v1, :cond_c

    .line 3671
    iget-object v1, p1, Lcom/google/ac/b/c/as;->d:Ljava/lang/Double;

    if-nez v1, :cond_0

    .line 3677
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/as;->e:Ljava/lang/Double;

    if-nez v1, :cond_d

    .line 3678
    iget-object v1, p1, Lcom/google/ac/b/c/as;->e:Ljava/lang/Double;

    if-nez v1, :cond_0

    .line 3684
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/as;->f:Ljava/lang/Double;

    if-nez v1, :cond_e

    .line 3685
    iget-object v1, p1, Lcom/google/ac/b/c/as;->f:Ljava/lang/Double;

    if-nez v1, :cond_0

    .line 3691
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/as;->g:Ljava/lang/Long;

    if-nez v1, :cond_f

    .line 3692
    iget-object v1, p1, Lcom/google/ac/b/c/as;->g:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 3698
    :cond_8
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/as;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 3653
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 3660
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 3667
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/as;->c:Ljava/lang/Double;

    iget-object v2, p1, Lcom/google/ac/b/c/as;->c:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 3674
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/c/as;->d:Ljava/lang/Double;

    iget-object v2, p1, Lcom/google/ac/b/c/as;->d:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 3681
    :cond_d
    iget-object v1, p0, Lcom/google/ac/b/c/as;->e:Ljava/lang/Double;

    iget-object v2, p1, Lcom/google/ac/b/c/as;->e:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 3688
    :cond_e
    iget-object v1, p0, Lcom/google/ac/b/c/as;->f:Ljava/lang/Double;

    iget-object v2, p1, Lcom/google/ac/b/c/as;->f:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 3695
    :cond_f
    iget-object v1, p0, Lcom/google/ac/b/c/as;->g:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/as;->g:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3703
    iget-object v0, p0, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3706
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 3708
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/as;->c:Ljava/lang/Double;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 3710
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/as;->d:Ljava/lang/Double;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 3712
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/as;->e:Ljava/lang/Double;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 3714
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/as;->f:Ljava/lang/Double;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 3716
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/as;->g:Ljava/lang/Long;

    if-nez v2, :cond_6

    :goto_6
    add-int/2addr v0, v1

    .line 3718
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/as;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3719
    return v0

    .line 3703
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3706
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_1

    .line 3708
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/as;->c:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto :goto_2

    .line 3710
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/as;->d:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto :goto_3

    .line 3712
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/as;->e:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto :goto_4

    .line 3714
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/as;->f:Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->hashCode()I

    move-result v0

    goto :goto_5

    .line 3716
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/as;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_6
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 3585
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/as;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/as;->c:Ljava/lang/Double;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/as;->d:Ljava/lang/Double;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/as;->e:Ljava/lang/Double;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/as;->f:Ljava/lang/Double;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/as;->g:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x19 -> :sswitch_3
        0x21 -> :sswitch_4
        0x29 -> :sswitch_5
        0x31 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 3725
    iget-object v0, p0, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 3726
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/as;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3728
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 3729
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/as;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3731
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/as;->c:Ljava/lang/Double;

    if-eqz v0, :cond_2

    .line 3732
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/as;->c:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 3734
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/as;->d:Ljava/lang/Double;

    if-eqz v0, :cond_3

    .line 3735
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/as;->d:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 3737
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/as;->e:Ljava/lang/Double;

    if-eqz v0, :cond_4

    .line 3738
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/as;->e:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 3740
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/as;->f:Ljava/lang/Double;

    if-eqz v0, :cond_5

    .line 3741
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/as;->f:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 3743
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/as;->g:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 3744
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/b/c/as;->g:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3746
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3747
    return-void
.end method
