.class public final Lcom/google/ac/b/c/cr;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/c/db;

.field public b:Lcom/google/ac/b/c/bj;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 910
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 911
    iput-object v0, p0, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    iput-object v0, p0, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    iput-object v0, p0, Lcom/google/ac/b/c/cr;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/cr;->cachedSize:I

    .line 912
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 977
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 978
    iget-object v1, p0, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    if-eqz v1, :cond_0

    .line 979
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 982
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    if-eqz v1, :cond_1

    .line 983
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 986
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 924
    if-ne p1, p0, :cond_1

    .line 925
    const/4 v0, 0x1

    .line 949
    :cond_0
    :goto_0
    return v0

    .line 927
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/cr;

    if-eqz v1, :cond_0

    .line 930
    check-cast p1, Lcom/google/ac/b/c/cr;

    .line 931
    iget-object v1, p0, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    if-nez v1, :cond_4

    .line 932
    iget-object v1, p1, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    if-nez v1, :cond_0

    .line 940
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    if-nez v1, :cond_5

    .line 941
    iget-object v1, p1, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    if-nez v1, :cond_0

    .line 949
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/cr;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 936
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    iget-object v2, p1, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/db;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 945
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    iget-object v2, p1, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 954
    iget-object v0, p0, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 957
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 959
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/cr;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 960
    return v0

    .line 954
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {v0}, Lcom/google/ac/b/c/db;->hashCode()I

    move-result v0

    goto :goto_0

    .line 957
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    invoke-virtual {v1}, Lcom/google/ac/b/c/bj;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 887
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/cr;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/db;

    invoke-direct {v0}, Lcom/google/ac/b/c/db;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/bj;

    invoke-direct {v0}, Lcom/google/ac/b/c/bj;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 966
    iget-object v0, p0, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    if-eqz v0, :cond_0

    .line 967
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/cr;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 969
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    if-eqz v0, :cond_1

    .line 970
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/cr;->b:Lcom/google/ac/b/c/bj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 972
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 973
    return-void
.end method
