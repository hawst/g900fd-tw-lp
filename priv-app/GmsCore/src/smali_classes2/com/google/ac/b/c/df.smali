.class public final Lcom/google/ac/b/c/df;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/ac/b/c/bu;

.field public b:Lcom/google/ac/b/c/as;

.field public c:Lcom/google/ac/b/c/ag;

.field public d:Lcom/google/ac/b/c/ak;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 2707
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2708
    invoke-static {}, Lcom/google/ac/b/c/bu;->a()[Lcom/google/ac/b/c/bu;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    iput-object v1, p0, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    iput-object v1, p0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    iput-object v1, p0, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    iput-object v1, p0, Lcom/google/ac/b/c/df;->e:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/ac/b/c/df;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/df;->cachedSize:I

    .line 2709
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 2817
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 2818
    iget-object v0, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 2819
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 2820
    iget-object v2, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    aget-object v2, v2, v0

    .line 2821
    if-eqz v2, :cond_0

    .line 2822
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 2819
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2827
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    if-eqz v0, :cond_2

    .line 2828
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 2831
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    if-eqz v0, :cond_3

    .line 2832
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 2835
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    if-eqz v0, :cond_4

    .line 2836
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v0

    add-int/2addr v1, v0

    .line 2839
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/df;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2840
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/df;->e:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 2843
    :cond_5
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2724
    if-ne p1, p0, :cond_1

    .line 2725
    const/4 v0, 0x1

    .line 2769
    :cond_0
    :goto_0
    return v0

    .line 2727
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/df;

    if-eqz v1, :cond_0

    .line 2730
    check-cast p1, Lcom/google/ac/b/c/df;

    .line 2731
    iget-object v1, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    iget-object v2, p1, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2735
    iget-object v1, p0, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    if-nez v1, :cond_6

    .line 2736
    iget-object v1, p1, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    if-nez v1, :cond_0

    .line 2744
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    if-nez v1, :cond_7

    .line 2745
    iget-object v1, p1, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    if-nez v1, :cond_0

    .line 2753
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    if-nez v1, :cond_8

    .line 2754
    iget-object v1, p1, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    if-nez v1, :cond_0

    .line 2762
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/df;->e:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 2763
    iget-object v1, p1, Lcom/google/ac/b/c/df;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2769
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/df;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2740
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    iget-object v2, p1, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/as;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2749
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    iget-object v2, p1, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/ag;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 2758
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    iget-object v2, p1, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/ak;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 2766
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/df;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/df;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2774
    iget-object v0, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 2777
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 2779
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2781
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2783
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/df;->e:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 2785
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/df;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2786
    return v0

    .line 2777
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    invoke-virtual {v0}, Lcom/google/ac/b/c/as;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2779
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    invoke-virtual {v0}, Lcom/google/ac/b/c/ag;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2781
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    invoke-virtual {v0}, Lcom/google/ac/b/c/ak;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2783
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/df;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2675
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/df;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/bu;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/c/bu;

    invoke-direct {v3}, Lcom/google/ac/b/c/bu;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/c/bu;

    invoke-direct {v3}, Lcom/google/ac/b/c/bu;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/c/as;

    invoke-direct {v0}, Lcom/google/ac/b/c/as;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/ac/b/c/ag;

    invoke-direct {v0}, Lcom/google/ac/b/c/ag;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/ac/b/c/ak;

    invoke-direct {v0}, Lcom/google/ac/b/c/ak;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/df;->e:Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 2792
    iget-object v0, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 2793
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 2794
    iget-object v1, p0, Lcom/google/ac/b/c/df;->a:[Lcom/google/ac/b/c/bu;

    aget-object v1, v1, v0

    .line 2795
    if-eqz v1, :cond_0

    .line 2796
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2793
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2800
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    if-eqz v0, :cond_2

    .line 2801
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/df;->b:Lcom/google/ac/b/c/as;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2803
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    if-eqz v0, :cond_3

    .line 2804
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/df;->c:Lcom/google/ac/b/c/ag;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2806
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    if-eqz v0, :cond_4

    .line 2807
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/df;->d:Lcom/google/ac/b/c/ak;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2809
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/df;->e:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 2810
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/df;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2812
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2813
    return-void
.end method
