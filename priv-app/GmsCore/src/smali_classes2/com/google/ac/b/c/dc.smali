.class public final Lcom/google/ac/b/c/dc;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/ac/b/c/aa;

.field public c:Lcom/google/ac/b/c/bl;

.field public d:Lcom/google/ac/b/c/g;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 334
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 335
    iput-object v0, p0, Lcom/google/ac/b/c/dc;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    iput-object v0, p0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    iput-object v0, p0, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    iput-object v0, p0, Lcom/google/ac/b/c/dc;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/dc;->cachedSize:I

    .line 336
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 427
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 428
    iget-object v1, p0, Lcom/google/ac/b/c/dc;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 429
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/dc;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 432
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    if-eqz v1, :cond_1

    .line 433
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 436
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    if-eqz v1, :cond_2

    .line 437
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 440
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    if-eqz v1, :cond_3

    .line 441
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 444
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 350
    if-ne p1, p0, :cond_1

    .line 351
    const/4 v0, 0x1

    .line 390
    :cond_0
    :goto_0
    return v0

    .line 353
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/dc;

    if-eqz v1, :cond_0

    .line 356
    check-cast p1, Lcom/google/ac/b/c/dc;

    .line 357
    iget-object v1, p0, Lcom/google/ac/b/c/dc;->a:Ljava/lang/Integer;

    if-nez v1, :cond_6

    .line 358
    iget-object v1, p1, Lcom/google/ac/b/c/dc;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 363
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    if-nez v1, :cond_7

    .line 364
    iget-object v1, p1, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    if-nez v1, :cond_0

    .line 372
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    if-nez v1, :cond_8

    .line 373
    iget-object v1, p1, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    if-nez v1, :cond_0

    .line 381
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    if-nez v1, :cond_9

    .line 382
    iget-object v1, p1, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    if-nez v1, :cond_0

    .line 390
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/dc;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 361
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/dc;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/dc;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 368
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    iget-object v2, p1, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/aa;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 377
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    iget-object v2, p1, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 386
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    iget-object v2, p1, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/g;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 395
    iget-object v0, p0, Lcom/google/ac/b/c/dc;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 397
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 399
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 401
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 403
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/dc;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 404
    return v0

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/dc;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 397
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    invoke-virtual {v0}, Lcom/google/ac/b/c/aa;->hashCode()I

    move-result v0

    goto :goto_1

    .line 399
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    invoke-virtual {v0}, Lcom/google/ac/b/c/bl;->hashCode()I

    move-result v0

    goto :goto_2

    .line 401
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    invoke-virtual {v1}, Lcom/google/ac/b/c/g;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 305
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/dc;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dc;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/aa;

    invoke-direct {v0}, Lcom/google/ac/b/c/aa;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/bl;

    invoke-direct {v0}, Lcom/google/ac/b/c/bl;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/b/c/g;

    invoke-direct {v0}, Lcom/google/ac/b/c/g;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 410
    iget-object v0, p0, Lcom/google/ac/b/c/dc;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 411
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/dc;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 413
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    if-eqz v0, :cond_1

    .line 414
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/dc;->b:Lcom/google/ac/b/c/aa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 416
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    if-eqz v0, :cond_2

    .line 417
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/dc;->c:Lcom/google/ac/b/c/bl;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 419
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    if-eqz v0, :cond_3

    .line 420
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/dc;->d:Lcom/google/ac/b/c/g;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 422
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 423
    return-void
.end method
