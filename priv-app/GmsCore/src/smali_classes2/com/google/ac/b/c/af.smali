.class public final Lcom/google/ac/b/c/af;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/ac/b/c/cj;

.field public d:Ljava/lang/String;

.field public e:[B

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/Integer;

.field public h:Lcom/google/ac/b/c/cl;

.field public i:Lcom/google/ac/b/c/x;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 383
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 384
    iput-object v0, p0, Lcom/google/ac/b/c/af;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/af;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/af;->c:Lcom/google/ac/b/c/cj;

    iput-object v0, p0, Lcom/google/ac/b/c/af;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/af;->e:[B

    iput-object v0, p0, Lcom/google/ac/b/c/af;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/af;->g:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/af;->h:Lcom/google/ac/b/c/cl;

    iput-object v0, p0, Lcom/google/ac/b/c/af;->i:Lcom/google/ac/b/c/x;

    iput-object v0, p0, Lcom/google/ac/b/c/af;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/af;->cachedSize:I

    .line 385
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 536
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 537
    iget-object v1, p0, Lcom/google/ac/b/c/af;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 538
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/af;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 541
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/af;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 542
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/af;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 545
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/af;->c:Lcom/google/ac/b/c/cj;

    if-eqz v1, :cond_2

    .line 546
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/af;->c:Lcom/google/ac/b/c/cj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 549
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/af;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 550
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/af;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 553
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/af;->e:[B

    if-eqz v1, :cond_4

    .line 554
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/af;->e:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 557
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/af;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 558
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/af;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 561
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/af;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 562
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/b/c/af;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 565
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/af;->h:Lcom/google/ac/b/c/cl;

    if-eqz v1, :cond_7

    .line 566
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/ac/b/c/af;->h:Lcom/google/ac/b/c/cl;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 569
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/af;->i:Lcom/google/ac/b/c/x;

    if-eqz v1, :cond_8

    .line 570
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/ac/b/c/af;->i:Lcom/google/ac/b/c/x;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 573
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 404
    if-ne p1, p0, :cond_1

    .line 405
    const/4 v0, 0x1

    .line 475
    :cond_0
    :goto_0
    return v0

    .line 407
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/af;

    if-eqz v1, :cond_0

    .line 410
    check-cast p1, Lcom/google/ac/b/c/af;

    .line 411
    iget-object v1, p0, Lcom/google/ac/b/c/af;->a:Ljava/lang/Integer;

    if-nez v1, :cond_a

    .line 412
    iget-object v1, p1, Lcom/google/ac/b/c/af;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 417
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/af;->b:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 418
    iget-object v1, p1, Lcom/google/ac/b/c/af;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 424
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/af;->c:Lcom/google/ac/b/c/cj;

    if-nez v1, :cond_c

    .line 425
    iget-object v1, p1, Lcom/google/ac/b/c/af;->c:Lcom/google/ac/b/c/cj;

    if-nez v1, :cond_0

    .line 433
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/af;->d:Ljava/lang/String;

    if-nez v1, :cond_d

    .line 434
    iget-object v1, p1, Lcom/google/ac/b/c/af;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 440
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/af;->e:[B

    iget-object v2, p1, Lcom/google/ac/b/c/af;->e:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 443
    iget-object v1, p0, Lcom/google/ac/b/c/af;->f:Ljava/lang/String;

    if-nez v1, :cond_e

    .line 444
    iget-object v1, p1, Lcom/google/ac/b/c/af;->f:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 450
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/af;->g:Ljava/lang/Integer;

    if-nez v1, :cond_f

    .line 451
    iget-object v1, p1, Lcom/google/ac/b/c/af;->g:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 457
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/af;->h:Lcom/google/ac/b/c/cl;

    if-nez v1, :cond_10

    .line 458
    iget-object v1, p1, Lcom/google/ac/b/c/af;->h:Lcom/google/ac/b/c/cl;

    if-nez v1, :cond_0

    .line 466
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/af;->i:Lcom/google/ac/b/c/x;

    if-nez v1, :cond_11

    .line 467
    iget-object v1, p1, Lcom/google/ac/b/c/af;->i:Lcom/google/ac/b/c/x;

    if-nez v1, :cond_0

    .line 475
    :cond_9
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/af;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 415
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/af;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/af;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 421
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/af;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/af;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 429
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/c/af;->c:Lcom/google/ac/b/c/cj;

    iget-object v2, p1, Lcom/google/ac/b/c/af;->c:Lcom/google/ac/b/c/cj;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/cj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 437
    :cond_d
    iget-object v1, p0, Lcom/google/ac/b/c/af;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/af;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto/16 :goto_0

    .line 447
    :cond_e
    iget-object v1, p0, Lcom/google/ac/b/c/af;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/af;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto/16 :goto_0

    .line 454
    :cond_f
    iget-object v1, p0, Lcom/google/ac/b/c/af;->g:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/af;->g:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 462
    :cond_10
    iget-object v1, p0, Lcom/google/ac/b/c/af;->h:Lcom/google/ac/b/c/cl;

    iget-object v2, p1, Lcom/google/ac/b/c/af;->h:Lcom/google/ac/b/c/cl;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/cl;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0

    .line 471
    :cond_11
    iget-object v1, p0, Lcom/google/ac/b/c/af;->i:Lcom/google/ac/b/c/x;

    iget-object v2, p1, Lcom/google/ac/b/c/af;->i:Lcom/google/ac/b/c/x;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/x;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 480
    iget-object v0, p0, Lcom/google/ac/b/c/af;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 482
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/af;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 484
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/af;->c:Lcom/google/ac/b/c/cj;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 486
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/af;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 488
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/af;->e:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 489
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/af;->f:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 491
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/af;->g:Ljava/lang/Integer;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 493
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/af;->h:Lcom/google/ac/b/c/cl;

    if-nez v0, :cond_6

    move v0, v1

    :goto_6
    add-int/2addr v0, v2

    .line 495
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/af;->i:Lcom/google/ac/b/c/x;

    if-nez v2, :cond_7

    :goto_7
    add-int/2addr v0, v1

    .line 497
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/af;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 498
    return v0

    .line 480
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/af;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 482
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/af;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 484
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/af;->c:Lcom/google/ac/b/c/cj;

    invoke-virtual {v0}, Lcom/google/ac/b/c/cj;->hashCode()I

    move-result v0

    goto :goto_2

    .line 486
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/af;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 489
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/af;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 491
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/af;->g:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_5

    .line 493
    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/af;->h:Lcom/google/ac/b/c/cl;

    invoke-virtual {v0}, Lcom/google/ac/b/c/cl;->hashCode()I

    move-result v0

    goto :goto_6

    .line 495
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/af;->i:Lcom/google/ac/b/c/x;

    invoke-virtual {v1}, Lcom/google/ac/b/c/x;->hashCode()I

    move-result v1

    goto :goto_7
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 339
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/af;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/af;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/af;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/af;->c:Lcom/google/ac/b/c/cj;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/cj;

    invoke-direct {v0}, Lcom/google/ac/b/c/cj;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/af;->c:Lcom/google/ac/b/c/cj;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/af;->c:Lcom/google/ac/b/c/cj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/af;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/af;->e:[B

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/af;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/af;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/ac/b/c/af;->h:Lcom/google/ac/b/c/cl;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/cl;

    invoke-direct {v0}, Lcom/google/ac/b/c/cl;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/af;->h:Lcom/google/ac/b/c/cl;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/af;->h:Lcom/google/ac/b/c/cl;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_9
    iget-object v0, p0, Lcom/google/ac/b/c/af;->i:Lcom/google/ac/b/c/x;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/b/c/x;

    invoke-direct {v0}, Lcom/google/ac/b/c/x;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/af;->i:Lcom/google/ac/b/c/x;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/af;->i:Lcom/google/ac/b/c/x;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x4a -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 504
    iget-object v0, p0, Lcom/google/ac/b/c/af;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 505
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/af;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 507
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/af;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 508
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/af;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 510
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/af;->c:Lcom/google/ac/b/c/cj;

    if-eqz v0, :cond_2

    .line 511
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/af;->c:Lcom/google/ac/b/c/cj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 513
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/af;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 514
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/af;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 516
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/af;->e:[B

    if-eqz v0, :cond_4

    .line 517
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/af;->e:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 519
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/af;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 520
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/af;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 522
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/af;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_6

    .line 523
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/b/c/af;->g:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 525
    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/af;->h:Lcom/google/ac/b/c/cl;

    if-eqz v0, :cond_7

    .line 526
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/ac/b/c/af;->h:Lcom/google/ac/b/c/cl;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 528
    :cond_7
    iget-object v0, p0, Lcom/google/ac/b/c/af;->i:Lcom/google/ac/b/c/x;

    if-eqz v0, :cond_8

    .line 529
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/ac/b/c/af;->i:Lcom/google/ac/b/c/x;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 531
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 532
    return-void
.end method
