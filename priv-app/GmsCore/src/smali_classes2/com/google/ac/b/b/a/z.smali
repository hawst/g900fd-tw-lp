.class public final Lcom/google/ac/b/b/a/z;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/b/a/aa;

.field public b:Lcom/google/ac/b/b/a/am;

.field public c:Lcom/google/ac/b/b/a/w;

.field public d:Lcom/google/ac/b/b/a/x;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 184
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 185
    iput-object v0, p0, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    iput-object v0, p0, Lcom/google/ac/b/b/a/z;->b:Lcom/google/ac/b/b/a/am;

    iput-object v0, p0, Lcom/google/ac/b/b/a/z;->c:Lcom/google/ac/b/b/a/w;

    iput-object v0, p0, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    iput-object v0, p0, Lcom/google/ac/b/b/a/z;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/z;->cachedSize:I

    .line 186
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 281
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 282
    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    if-eqz v1, :cond_0

    .line 283
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 286
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->b:Lcom/google/ac/b/b/a/am;

    if-eqz v1, :cond_1

    .line 287
    const/16 v1, 0x64

    iget-object v2, p0, Lcom/google/ac/b/b/a/z;->b:Lcom/google/ac/b/b/a/am;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 290
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->c:Lcom/google/ac/b/b/a/w;

    if-eqz v1, :cond_2

    .line 291
    const/16 v1, 0x65

    iget-object v2, p0, Lcom/google/ac/b/b/a/z;->c:Lcom/google/ac/b/b/a/w;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 294
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    if-eqz v1, :cond_3

    .line 295
    const/16 v1, 0x66

    iget-object v2, p0, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 298
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 200
    if-ne p1, p0, :cond_1

    .line 201
    const/4 v0, 0x1

    .line 243
    :cond_0
    :goto_0
    return v0

    .line 203
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/z;

    if-eqz v1, :cond_0

    .line 206
    check-cast p1, Lcom/google/ac/b/b/a/z;

    .line 207
    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    if-nez v1, :cond_6

    .line 208
    iget-object v1, p1, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    if-nez v1, :cond_0

    .line 216
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->b:Lcom/google/ac/b/b/a/am;

    if-nez v1, :cond_7

    .line 217
    iget-object v1, p1, Lcom/google/ac/b/b/a/z;->b:Lcom/google/ac/b/b/a/am;

    if-nez v1, :cond_0

    .line 225
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->c:Lcom/google/ac/b/b/a/w;

    if-nez v1, :cond_8

    .line 226
    iget-object v1, p1, Lcom/google/ac/b/b/a/z;->c:Lcom/google/ac/b/b/a/w;

    if-nez v1, :cond_0

    .line 234
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    if-nez v1, :cond_9

    .line 235
    iget-object v1, p1, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    if-nez v1, :cond_0

    .line 243
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/z;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 212
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    iget-object v2, p1, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/aa;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 221
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->b:Lcom/google/ac/b/b/a/am;

    iget-object v2, p1, Lcom/google/ac/b/b/a/z;->b:Lcom/google/ac/b/b/a/am;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/am;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 230
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->c:Lcom/google/ac/b/b/a/w;

    iget-object v2, p1, Lcom/google/ac/b/b/a/z;->c:Lcom/google/ac/b/b/a/w;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/w;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 239
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    iget-object v2, p1, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/x;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 248
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 251
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->b:Lcom/google/ac/b/b/a/am;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 253
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->c:Lcom/google/ac/b/b/a/w;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 255
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 257
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/z;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 258
    return v0

    .line 248
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/aa;->hashCode()I

    move-result v0

    goto :goto_0

    .line 251
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->b:Lcom/google/ac/b/b/a/am;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/am;->hashCode()I

    move-result v0

    goto :goto_1

    .line 253
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->c:Lcom/google/ac/b/b/a/w;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/w;->hashCode()I

    move-result v0

    goto :goto_2

    .line 255
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    invoke-virtual {v1}, Lcom/google/ac/b/b/a/x;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 155
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/z;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/b/a/aa;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/aa;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->b:Lcom/google/ac/b/b/a/am;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/b/a/am;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/am;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/z;->b:Lcom/google/ac/b/b/a/am;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->b:Lcom/google/ac/b/b/a/am;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->c:Lcom/google/ac/b/b/a/w;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/b/b/a/w;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/w;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/z;->c:Lcom/google/ac/b/b/a/w;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->c:Lcom/google/ac/b/b/a/w;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/b/a/x;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/x;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x322 -> :sswitch_2
        0x32a -> :sswitch_3
        0x332 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    if-eqz v0, :cond_0

    .line 265
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->a:Lcom/google/ac/b/b/a/aa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->b:Lcom/google/ac/b/b/a/am;

    if-eqz v0, :cond_1

    .line 268
    const/16 v0, 0x64

    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->b:Lcom/google/ac/b/b/a/am;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 270
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->c:Lcom/google/ac/b/b/a/w;

    if-eqz v0, :cond_2

    .line 271
    const/16 v0, 0x65

    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->c:Lcom/google/ac/b/b/a/w;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 273
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    if-eqz v0, :cond_3

    .line 274
    const/16 v0, 0x66

    iget-object v1, p0, Lcom/google/ac/b/b/a/z;->d:Lcom/google/ac/b/b/a/x;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 276
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 277
    return-void
.end method
