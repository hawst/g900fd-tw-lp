.class public final Lcom/google/ac/b/c/bm;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/ac/b/c/bm;


# instance fields
.field public a:[Ljava/lang/String;

.field public b:Lcom/google/ac/b/c/at;

.field public c:Lcom/google/ac/b/c/bn;

.field public d:Lcom/google/ac/b/c/aj;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 8656
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 8657
    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    iput-object v1, p0, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    iput-object v1, p0, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    iput-object v1, p0, Lcom/google/ac/b/c/bm;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/bm;->cachedSize:I

    .line 8658
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/bm;
    .locals 2

    .prologue
    .line 8633
    sget-object v0, Lcom/google/ac/b/c/bm;->e:[Lcom/google/ac/b/c/bm;

    if-nez v0, :cond_1

    .line 8634
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 8636
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/bm;->e:[Lcom/google/ac/b/c/bm;

    if-nez v0, :cond_0

    .line 8637
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/bm;

    sput-object v0, Lcom/google/ac/b/c/bm;->e:[Lcom/google/ac/b/c/bm;

    .line 8639
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 8641
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/bm;->e:[Lcom/google/ac/b/c/bm;

    return-object v0

    .line 8639
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 8753
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v3

    .line 8754
    iget-object v1, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_5

    move v1, v0

    move v2, v0

    .line 8757
    :goto_0
    iget-object v4, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 8758
    iget-object v4, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    aget-object v4, v4, v0

    .line 8759
    if-eqz v4, :cond_0

    .line 8760
    add-int/lit8 v2, v2, 0x1

    .line 8761
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 8757
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8765
    :cond_1
    add-int v0, v3, v1

    .line 8766
    mul-int/lit8 v1, v2, 0x1

    add-int/2addr v0, v1

    .line 8768
    :goto_1
    iget-object v1, p0, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    if-eqz v1, :cond_2

    .line 8769
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8772
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    if-eqz v1, :cond_3

    .line 8773
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8776
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    if-eqz v1, :cond_4

    .line 8777
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8780
    :cond_4
    return v0

    :cond_5
    move v0, v3

    goto :goto_1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 8672
    if-ne p1, p0, :cond_1

    .line 8673
    const/4 v0, 0x1

    .line 8710
    :cond_0
    :goto_0
    return v0

    .line 8675
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/bm;

    if-eqz v1, :cond_0

    .line 8678
    check-cast p1, Lcom/google/ac/b/c/bm;

    .line 8679
    iget-object v1, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 8683
    iget-object v1, p0, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    if-nez v1, :cond_5

    .line 8684
    iget-object v1, p1, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    if-nez v1, :cond_0

    .line 8692
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    if-nez v1, :cond_6

    .line 8693
    iget-object v1, p1, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    if-nez v1, :cond_0

    .line 8701
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    if-nez v1, :cond_7

    .line 8702
    iget-object v1, p1, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    if-nez v1, :cond_0

    .line 8710
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/bm;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 8688
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    iget-object v2, p1, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/at;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 8697
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    iget-object v2, p1, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bn;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 8706
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    iget-object v2, p1, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/aj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 8715
    iget-object v0, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 8718
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 8720
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 8722
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 8724
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/bm;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 8725
    return v0

    .line 8718
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    invoke-virtual {v0}, Lcom/google/ac/b/c/at;->hashCode()I

    move-result v0

    goto :goto_0

    .line 8720
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    invoke-virtual {v0}, Lcom/google/ac/b/c/bn;->hashCode()I

    move-result v0

    goto :goto_1

    .line 8722
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    invoke-virtual {v1}, Lcom/google/ac/b/c/aj;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8359
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/bm;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/c/at;

    invoke-direct {v0}, Lcom/google/ac/b/c/at;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/ac/b/c/bn;

    invoke-direct {v0}, Lcom/google/ac/b/c/bn;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/ac/b/c/aj;

    invoke-direct {v0}, Lcom/google/ac/b/c/aj;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    :cond_6
    iget-object v0, p0, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 8731
    iget-object v0, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 8732
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 8733
    iget-object v1, p0, Lcom/google/ac/b/c/bm;->a:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 8734
    if-eqz v1, :cond_0

    .line 8735
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8732
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8739
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    if-eqz v0, :cond_2

    .line 8740
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/bm;->b:Lcom/google/ac/b/c/at;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 8742
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    if-eqz v0, :cond_3

    .line 8743
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/bm;->c:Lcom/google/ac/b/c/bn;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 8745
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    if-eqz v0, :cond_4

    .line 8746
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/bm;->d:Lcom/google/ac/b/c/aj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 8748
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8749
    return-void
.end method
