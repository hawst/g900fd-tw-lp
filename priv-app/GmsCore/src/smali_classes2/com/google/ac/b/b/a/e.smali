.class public final Lcom/google/ac/b/b/a/e;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;

.field public c:Ljava/lang/Long;

.field public d:Lcom/google/ac/b/b/a/z;

.field public e:Lcom/google/ac/b/b/a/ab;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 360
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 361
    iput-object v0, p0, Lcom/google/ac/b/b/a/e;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/b/a/e;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/b/a/e;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    iput-object v0, p0, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    iput-object v0, p0, Lcom/google/ac/b/b/a/e;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/e;->cachedSize:I

    .line 362
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 464
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 465
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 466
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/e;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 469
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 470
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/b/a/e;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 473
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 474
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/b/a/e;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 477
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    if-eqz v1, :cond_3

    .line 478
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 481
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    if-eqz v1, :cond_4

    .line 482
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 485
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 377
    if-ne p1, p0, :cond_1

    .line 378
    const/4 v0, 0x1

    .line 422
    :cond_0
    :goto_0
    return v0

    .line 380
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/e;

    if-eqz v1, :cond_0

    .line 383
    check-cast p1, Lcom/google/ac/b/b/a/e;

    .line 384
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->a:Ljava/lang/Integer;

    if-nez v1, :cond_7

    .line 385
    iget-object v1, p1, Lcom/google/ac/b/b/a/e;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 390
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->b:Ljava/lang/Integer;

    if-nez v1, :cond_8

    .line 391
    iget-object v1, p1, Lcom/google/ac/b/b/a/e;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 397
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->c:Ljava/lang/Long;

    if-nez v1, :cond_9

    .line 398
    iget-object v1, p1, Lcom/google/ac/b/b/a/e;->c:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 404
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    if-nez v1, :cond_a

    .line 405
    iget-object v1, p1, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    if-nez v1, :cond_0

    .line 413
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    if-nez v1, :cond_b

    .line 414
    iget-object v1, p1, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    if-nez v1, :cond_0

    .line 422
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/e;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 388
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/b/a/e;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 394
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/b/a/e;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 401
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->c:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/b/a/e;->c:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 409
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    iget-object v2, p1, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/z;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 418
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    iget-object v2, p1, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/ab;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 427
    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 429
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->b:Ljava/lang/Integer;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 431
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->c:Ljava/lang/Long;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 433
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 435
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 437
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/e;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 438
    return v0

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 429
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->b:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    goto :goto_1

    .line 431
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_2

    .line 433
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/z;->hashCode()I

    move-result v0

    goto :goto_3

    .line 435
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    invoke-virtual {v1}, Lcom/google/ac/b/b/a/ab;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 319
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/e;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/e;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/e;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/e;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/b/a/z;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/z;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/b/a/ab;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/ab;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 445
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 447
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 448
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 450
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 451
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 453
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    if-eqz v0, :cond_3

    .line 454
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->d:Lcom/google/ac/b/b/a/z;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 456
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    if-eqz v0, :cond_4

    .line 457
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/b/a/e;->e:Lcom/google/ac/b/b/a/ab;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 459
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 460
    return-void
.end method
