.class public final Lcom/google/ac/b/c/s;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7689
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 7690
    iput-object v0, p0, Lcom/google/ac/b/c/s;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/s;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/s;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/s;->cachedSize:I

    .line 7691
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 7750
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 7751
    iget-object v1, p0, Lcom/google/ac/b/c/s;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 7752
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/s;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7755
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/s;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 7756
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/s;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7759
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 7703
    if-ne p1, p0, :cond_1

    .line 7704
    const/4 v0, 0x1

    .line 7723
    :cond_0
    :goto_0
    return v0

    .line 7706
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/s;

    if-eqz v1, :cond_0

    .line 7709
    check-cast p1, Lcom/google/ac/b/c/s;

    .line 7710
    iget-object v1, p0, Lcom/google/ac/b/c/s;->a:Ljava/lang/Integer;

    if-nez v1, :cond_4

    .line 7711
    iget-object v1, p1, Lcom/google/ac/b/c/s;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 7716
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/s;->b:Ljava/lang/Integer;

    if-nez v1, :cond_5

    .line 7717
    iget-object v1, p1, Lcom/google/ac/b/c/s;->b:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 7723
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/s;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 7714
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/s;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/s;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 7720
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/s;->b:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/s;->b:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 7728
    iget-object v0, p0, Lcom/google/ac/b/c/s;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 7730
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/s;->b:Ljava/lang/Integer;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 7732
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/s;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 7733
    return v0

    .line 7728
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/s;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 7730
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/s;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 7666
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/s;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/s;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/s;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 7739
    iget-object v0, p0, Lcom/google/ac/b/c/s;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 7740
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/s;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7742
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/s;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 7743
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/s;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7745
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7746
    return-void
.end method
