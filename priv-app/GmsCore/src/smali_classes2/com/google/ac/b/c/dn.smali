.class public final Lcom/google/ac/b/c/dn;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/ac/b/c/dn;


# instance fields
.field public a:Lcom/google/ac/b/c/dq;

.field public b:Lcom/google/ac/b/c/dr;

.field public c:[Lcom/google/ac/b/c/do;

.field public d:[Lcom/google/ac/b/c/dp;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1946
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1947
    iput-object v1, p0, Lcom/google/ac/b/c/dn;->a:Lcom/google/ac/b/c/dq;

    iput-object v1, p0, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    invoke-static {}, Lcom/google/ac/b/c/do;->a()[Lcom/google/ac/b/c/do;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    invoke-static {}, Lcom/google/ac/b/c/dp;->a()[Lcom/google/ac/b/c/dp;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    iput-object v1, p0, Lcom/google/ac/b/c/dn;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/dn;->cachedSize:I

    .line 1948
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/dn;
    .locals 2

    .prologue
    .line 1923
    sget-object v0, Lcom/google/ac/b/c/dn;->e:[Lcom/google/ac/b/c/dn;

    if-nez v0, :cond_1

    .line 1924
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1926
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/dn;->e:[Lcom/google/ac/b/c/dn;

    if-nez v0, :cond_0

    .line 1927
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/dn;

    sput-object v0, Lcom/google/ac/b/c/dn;->e:[Lcom/google/ac/b/c/dn;

    .line 1929
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1931
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/dn;->e:[Lcom/google/ac/b/c/dn;

    return-object v0

    .line 1929
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2043
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2044
    iget-object v2, p0, Lcom/google/ac/b/c/dn;->a:Lcom/google/ac/b/c/dq;

    if-eqz v2, :cond_0

    .line 2045
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/ac/b/c/dn;->a:Lcom/google/ac/b/c/dq;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2048
    :cond_0
    iget-object v2, p0, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    if-eqz v2, :cond_1

    .line 2049
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2052
    :cond_1
    iget-object v2, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    array-length v2, v2

    if-lez v2, :cond_4

    move v2, v0

    move v0, v1

    .line 2053
    :goto_0
    iget-object v3, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    array-length v3, v3

    if-ge v0, v3, :cond_3

    .line 2054
    iget-object v3, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    aget-object v3, v3, v0

    .line 2055
    if-eqz v3, :cond_2

    .line 2056
    const/4 v4, 0x3

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 2053
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v2

    .line 2061
    :cond_4
    iget-object v2, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    array-length v2, v2

    if-lez v2, :cond_6

    .line 2062
    :goto_1
    iget-object v2, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 2063
    iget-object v2, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    aget-object v2, v2, v1

    .line 2064
    if-eqz v2, :cond_5

    .line 2065
    const/4 v3, 0x4

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 2062
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2070
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1962
    if-ne p1, p0, :cond_1

    .line 1963
    const/4 v0, 0x1

    .line 1995
    :cond_0
    :goto_0
    return v0

    .line 1965
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/dn;

    if-eqz v1, :cond_0

    .line 1968
    check-cast p1, Lcom/google/ac/b/c/dn;

    .line 1969
    iget-object v1, p0, Lcom/google/ac/b/c/dn;->a:Lcom/google/ac/b/c/dq;

    if-nez v1, :cond_4

    .line 1970
    iget-object v1, p1, Lcom/google/ac/b/c/dn;->a:Lcom/google/ac/b/c/dq;

    if-nez v1, :cond_0

    .line 1978
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    if-nez v1, :cond_5

    .line 1979
    iget-object v1, p1, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    if-nez v1, :cond_0

    .line 1987
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    iget-object v2, p1, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1991
    iget-object v1, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    iget-object v2, p1, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1995
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/dn;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1974
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/dn;->a:Lcom/google/ac/b/c/dq;

    iget-object v2, p1, Lcom/google/ac/b/c/dn;->a:Lcom/google/ac/b/c/dq;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/dq;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1983
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    iget-object v2, p1, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/dr;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2000
    iget-object v0, p0, Lcom/google/ac/b/c/dn;->a:Lcom/google/ac/b/c/dq;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2003
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 2005
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2007
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2009
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/dn;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2010
    return v0

    .line 2000
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/dn;->a:Lcom/google/ac/b/c/dq;

    invoke-virtual {v0}, Lcom/google/ac/b/c/dq;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2003
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    invoke-virtual {v1}, Lcom/google/ac/b/c/dr;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1917
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/dn;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/dn;->a:Lcom/google/ac/b/c/dq;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/dq;

    invoke-direct {v0}, Lcom/google/ac/b/c/dq;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/dn;->a:Lcom/google/ac/b/c/dq;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dn;->a:Lcom/google/ac/b/c/dq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/dr;

    invoke-direct {v0}, Lcom/google/ac/b/c/dr;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/do;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Lcom/google/ac/b/c/do;

    invoke-direct {v3}, Lcom/google/ac/b/c/do;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Lcom/google/ac/b/c/do;

    invoke-direct {v3}, Lcom/google/ac/b/c/do;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    if-nez v0, :cond_7

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/dp;

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_6
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_8

    new-instance v3, Lcom/google/ac/b/c/dp;

    invoke-direct {v3}, Lcom/google/ac/b/c/dp;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_7
    iget-object v0, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    array-length v0, v0

    goto :goto_3

    :cond_8
    new-instance v3, Lcom/google/ac/b/c/dp;

    invoke-direct {v3}, Lcom/google/ac/b/c/dp;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 2016
    iget-object v0, p0, Lcom/google/ac/b/c/dn;->a:Lcom/google/ac/b/c/dq;

    if-eqz v0, :cond_0

    .line 2017
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/dn;->a:Lcom/google/ac/b/c/dq;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2019
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    if-eqz v0, :cond_1

    .line 2020
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/dn;->b:Lcom/google/ac/b/c/dr;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2022
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 2023
    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 2024
    iget-object v2, p0, Lcom/google/ac/b/c/dn;->c:[Lcom/google/ac/b/c/do;

    aget-object v2, v2, v0

    .line 2025
    if-eqz v2, :cond_2

    .line 2026
    const/4 v3, 0x3

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2023
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2030
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 2031
    :goto_1
    iget-object v0, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    array-length v0, v0

    if-ge v1, v0, :cond_5

    .line 2032
    iget-object v0, p0, Lcom/google/ac/b/c/dn;->d:[Lcom/google/ac/b/c/dp;

    aget-object v0, v0, v1

    .line 2033
    if-eqz v0, :cond_4

    .line 2034
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2031
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2038
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2039
    return-void
.end method
