.class public final Lcom/google/ac/b/c/az;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/ac/b/c/az;


# instance fields
.field public a:Lcom/google/ac/b/c/ax;

.field public b:Lcom/google/ac/b/c/dm;

.field public c:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2087
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2088
    iput-object v0, p0, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    iput-object v0, p0, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    iput-object v0, p0, Lcom/google/ac/b/c/az;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/az;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/az;->cachedSize:I

    .line 2089
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/az;
    .locals 2

    .prologue
    .line 2067
    sget-object v0, Lcom/google/ac/b/c/az;->d:[Lcom/google/ac/b/c/az;

    if-nez v0, :cond_1

    .line 2068
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2070
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/az;->d:[Lcom/google/ac/b/c/az;

    if-nez v0, :cond_0

    .line 2071
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/az;

    sput-object v0, Lcom/google/ac/b/c/az;->d:[Lcom/google/ac/b/c/az;

    .line 2073
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2075
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/az;->d:[Lcom/google/ac/b/c/az;

    return-object v0

    .line 2073
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2165
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2166
    iget-object v1, p0, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    if-eqz v1, :cond_0

    .line 2167
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2170
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    if-eqz v1, :cond_1

    .line 2171
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2174
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/az;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 2175
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/az;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 2178
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2102
    if-ne p1, p0, :cond_1

    .line 2103
    const/4 v0, 0x1

    .line 2133
    :cond_0
    :goto_0
    return v0

    .line 2105
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/az;

    if-eqz v1, :cond_0

    .line 2108
    check-cast p1, Lcom/google/ac/b/c/az;

    .line 2109
    iget-object v1, p0, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    if-nez v1, :cond_5

    .line 2110
    iget-object v1, p1, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    if-nez v1, :cond_0

    .line 2118
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    if-nez v1, :cond_6

    .line 2119
    iget-object v1, p1, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    if-nez v1, :cond_0

    .line 2127
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/az;->c:Ljava/lang/Integer;

    if-nez v1, :cond_7

    .line 2128
    iget-object v1, p1, Lcom/google/ac/b/c/az;->c:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 2133
    :cond_4
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/az;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2114
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    iget-object v2, p1, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/ax;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2123
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    iget-object v2, p1, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/dm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 2131
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/az;->c:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/az;->c:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2138
    iget-object v0, p0, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2141
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2143
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/az;->c:Ljava/lang/Integer;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 2144
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/az;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2145
    return v0

    .line 2138
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    invoke-virtual {v0}, Lcom/google/ac/b/c/ax;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2141
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    invoke-virtual {v0}, Lcom/google/ac/b/c/dm;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2143
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/az;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_2
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2061
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/az;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/ax;

    invoke-direct {v0}, Lcom/google/ac/b/c/ax;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/dm;

    invoke-direct {v0}, Lcom/google/ac/b/c/dm;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/az;->c:Ljava/lang/Integer;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2151
    iget-object v0, p0, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    if-eqz v0, :cond_0

    .line 2152
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/az;->a:Lcom/google/ac/b/c/ax;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2154
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    if-eqz v0, :cond_1

    .line 2155
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/az;->b:Lcom/google/ac/b/c/dm;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2157
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/az;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 2158
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/az;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 2160
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2161
    return-void
.end method
