.class public final Lcom/google/ac/b/c/dk;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2190
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2191
    iput-object v0, p0, Lcom/google/ac/b/c/dk;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/dk;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/dk;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/dk;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/c/dk;->e:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/b/c/dk;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/dk;->cachedSize:I

    .line 2192
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2292
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2293
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 2294
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/dk;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2297
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 2298
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/dk;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2301
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 2302
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/dk;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2305
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 2306
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/dk;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2309
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->e:Ljava/lang/Boolean;

    if-eqz v1, :cond_4

    .line 2310
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/dk;->e:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 2313
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2207
    if-ne p1, p0, :cond_1

    .line 2208
    const/4 v0, 0x1

    .line 2249
    :cond_0
    :goto_0
    return v0

    .line 2210
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/dk;

    if-eqz v1, :cond_0

    .line 2213
    check-cast p1, Lcom/google/ac/b/c/dk;

    .line 2214
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->a:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 2215
    iget-object v1, p1, Lcom/google/ac/b/c/dk;->a:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2221
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->b:Ljava/lang/String;

    if-nez v1, :cond_8

    .line 2222
    iget-object v1, p1, Lcom/google/ac/b/c/dk;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2228
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->c:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 2229
    iget-object v1, p1, Lcom/google/ac/b/c/dk;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 2235
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_a

    .line 2236
    iget-object v1, p1, Lcom/google/ac/b/c/dk;->d:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 2242
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_b

    .line 2243
    iget-object v1, p1, Lcom/google/ac/b/c/dk;->e:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 2249
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/dk;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2218
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->a:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/dk;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2225
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/dk;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 2232
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/dk;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 2239
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->d:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/dk;->d:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 2246
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->e:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/dk;->e:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2254
    iget-object v0, p0, Lcom/google/ac/b/c/dk;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2257
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/dk;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2259
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/dk;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2261
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/dk;->d:Ljava/lang/Boolean;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 2263
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/dk;->e:Ljava/lang/Boolean;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 2265
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/dk;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2266
    return v0

    .line 2254
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/dk;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2257
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dk;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2259
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/dk;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2261
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/dk;->d:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_3

    .line 2263
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/dk;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2158
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/dk;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dk;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dk;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dk;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dk;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/dk;->e:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2272
    iget-object v0, p0, Lcom/google/ac/b/c/dk;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 2273
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/dk;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2275
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/dk;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 2276
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/dk;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2278
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dk;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 2279
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/dk;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 2281
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/dk;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 2282
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/dk;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2284
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/dk;->e:Ljava/lang/Boolean;

    if-eqz v0, :cond_4

    .line 2285
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/dk;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 2287
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2288
    return-void
.end method
