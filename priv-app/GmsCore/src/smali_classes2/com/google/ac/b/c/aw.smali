.class public final Lcom/google/ac/b/c/aw;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/ac/b/c/aw;


# instance fields
.field public a:Lcom/google/ac/b/c/ax;

.field public b:Lcom/google/ac/b/c/dm;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2707
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2708
    iput-object v0, p0, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    iput-object v0, p0, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    iput-object v0, p0, Lcom/google/ac/b/c/aw;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/aw;->cachedSize:I

    .line 2709
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/aw;
    .locals 2

    .prologue
    .line 2690
    sget-object v0, Lcom/google/ac/b/c/aw;->c:[Lcom/google/ac/b/c/aw;

    if-nez v0, :cond_1

    .line 2691
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 2693
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/aw;->c:[Lcom/google/ac/b/c/aw;

    if-nez v0, :cond_0

    .line 2694
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/aw;

    sput-object v0, Lcom/google/ac/b/c/aw;->c:[Lcom/google/ac/b/c/aw;

    .line 2696
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2698
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/aw;->c:[Lcom/google/ac/b/c/aw;

    return-object v0

    .line 2696
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2774
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2775
    iget-object v1, p0, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    if-eqz v1, :cond_0

    .line 2776
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2779
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    if-eqz v1, :cond_1

    .line 2780
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2783
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2721
    if-ne p1, p0, :cond_1

    .line 2722
    const/4 v0, 0x1

    .line 2746
    :cond_0
    :goto_0
    return v0

    .line 2724
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/aw;

    if-eqz v1, :cond_0

    .line 2727
    check-cast p1, Lcom/google/ac/b/c/aw;

    .line 2728
    iget-object v1, p0, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    if-nez v1, :cond_4

    .line 2729
    iget-object v1, p1, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    if-nez v1, :cond_0

    .line 2737
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    if-nez v1, :cond_5

    .line 2738
    iget-object v1, p1, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    if-nez v1, :cond_0

    .line 2746
    :cond_3
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/aw;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2733
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    iget-object v2, p1, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/ax;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2742
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    iget-object v2, p1, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/dm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2751
    iget-object v0, p0, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2754
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 2756
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/aw;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2757
    return v0

    .line 2751
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    invoke-virtual {v0}, Lcom/google/ac/b/c/ax;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2754
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    invoke-virtual {v1}, Lcom/google/ac/b/c/dm;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2684
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/aw;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/ax;

    invoke-direct {v0}, Lcom/google/ac/b/c/ax;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/dm;

    invoke-direct {v0}, Lcom/google/ac/b/c/dm;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2763
    iget-object v0, p0, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    if-eqz v0, :cond_0

    .line 2764
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/aw;->a:Lcom/google/ac/b/c/ax;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2766
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    if-eqz v0, :cond_1

    .line 2767
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/aw;->b:Lcom/google/ac/b/c/dm;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2769
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2770
    return-void
.end method
