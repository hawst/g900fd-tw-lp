.class public final Lcom/google/ac/b/b/a/ac;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/b/a/ae;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3890
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3891
    iput-object v0, p0, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    iput-object v0, p0, Lcom/google/ac/b/b/a/ac;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/ac;->cachedSize:I

    .line 3892
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3942
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 3943
    iget-object v1, p0, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    if-eqz v1, :cond_0

    .line 3944
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3947
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3903
    if-ne p1, p0, :cond_1

    .line 3904
    const/4 v0, 0x1

    .line 3919
    :cond_0
    :goto_0
    return v0

    .line 3906
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/ac;

    if-eqz v1, :cond_0

    .line 3909
    check-cast p1, Lcom/google/ac/b/b/a/ac;

    .line 3910
    iget-object v1, p0, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    if-nez v1, :cond_3

    .line 3911
    iget-object v1, p1, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    if-nez v1, :cond_0

    .line 3919
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/ac;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 3915
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    iget-object v2, p1, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/ae;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 3924
    iget-object v0, p0, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 3927
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/ac;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3928
    return v0

    .line 3924
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/ae;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3870
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/ac;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/b/a/ae;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/ae;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3934
    iget-object v0, p0, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    if-eqz v0, :cond_0

    .line 3935
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/ac;->a:Lcom/google/ac/b/b/a/ae;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3937
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3938
    return-void
.end method
