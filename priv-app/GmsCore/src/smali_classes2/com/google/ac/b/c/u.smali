.class public final Lcom/google/ac/b/c/u;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/ac/b/c/dm;

.field public d:Lcom/google/ac/b/c/bh;

.field public e:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 7165
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 7166
    iput-object v0, p0, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;

    iput-object v0, p0, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    iput-object v0, p0, Lcom/google/ac/b/c/u;->e:[B

    iput-object v0, p0, Lcom/google/ac/b/c/u;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/u;->cachedSize:I

    .line 7167
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 7264
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 7265
    iget-object v1, p0, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 7266
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 7269
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 7270
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7273
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;

    if-eqz v1, :cond_2

    .line 7274
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7277
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    if-eqz v1, :cond_3

    .line 7278
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7281
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/u;->e:[B

    if-eqz v1, :cond_4

    .line 7282
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/u;->e:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 7285
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 7182
    if-ne p1, p0, :cond_1

    .line 7183
    const/4 v0, 0x1

    .line 7223
    :cond_0
    :goto_0
    return v0

    .line 7185
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/u;

    if-eqz v1, :cond_0

    .line 7188
    check-cast p1, Lcom/google/ac/b/c/u;

    .line 7189
    iget-object v1, p0, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    if-nez v1, :cond_6

    .line 7190
    iget-object v1, p1, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 7195
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    if-nez v1, :cond_7

    .line 7196
    iget-object v1, p1, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 7202
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;

    if-nez v1, :cond_8

    .line 7203
    iget-object v1, p1, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;

    if-nez v1, :cond_0

    .line 7211
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    if-nez v1, :cond_9

    .line 7212
    iget-object v1, p1, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    if-nez v1, :cond_0

    .line 7220
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/u;->e:[B

    iget-object v2, p1, Lcom/google/ac/b/c/u;->e:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 7223
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/u;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 7193
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 7199
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 7207
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;

    iget-object v2, p1, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/dm;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 7216
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    iget-object v2, p1, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bh;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 7228
    iget-object v0, p0, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 7230
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 7232
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 7234
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 7236
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/ac/b/c/u;->e:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    .line 7237
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/u;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 7238
    return v0

    .line 7228
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 7230
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 7232
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;

    invoke-virtual {v0}, Lcom/google/ac/b/c/dm;->hashCode()I

    move-result v0

    goto :goto_2

    .line 7234
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    invoke-virtual {v1}, Lcom/google/ac/b/c/bh;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 7133
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/u;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/dm;

    invoke-direct {v0}, Lcom/google/ac/b/c/dm;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/bh;

    invoke-direct {v0}, Lcom/google/ac/b/c/bh;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/u;->e:[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 7244
    iget-object v0, p0, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 7245
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/u;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 7247
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 7248
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/u;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 7250
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;

    if-eqz v0, :cond_2

    .line 7251
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/u;->c:Lcom/google/ac/b/c/dm;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 7253
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    if-eqz v0, :cond_3

    .line 7254
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/u;->d:Lcom/google/ac/b/c/bh;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 7256
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/u;->e:[B

    if-eqz v0, :cond_4

    .line 7257
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/u;->e:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 7259
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7260
    return-void
.end method
