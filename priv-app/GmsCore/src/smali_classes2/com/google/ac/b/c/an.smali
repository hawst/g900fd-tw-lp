.class public final Lcom/google/ac/b/c/an;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/ac/b/c/al;

.field public b:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 3114
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 3115
    invoke-static {}, Lcom/google/ac/b/c/al;->a()[Lcom/google/ac/b/c/al;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    iput-object v1, p0, Lcom/google/ac/b/c/an;->b:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/ac/b/c/an;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/an;->cachedSize:I

    .line 3116
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 3179
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v1

    .line 3180
    iget-object v0, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 3181
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 3182
    iget-object v2, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    aget-object v2, v2, v0

    .line 3183
    if-eqz v2, :cond_0

    .line 3184
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 3181
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3189
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/an;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3190
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/an;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v0}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 3193
    :cond_2
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 3128
    if-ne p1, p0, :cond_1

    .line 3129
    const/4 v0, 0x1

    .line 3146
    :cond_0
    :goto_0
    return v0

    .line 3131
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/an;

    if-eqz v1, :cond_0

    .line 3134
    check-cast p1, Lcom/google/ac/b/c/an;

    .line 3135
    iget-object v1, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    iget-object v2, p1, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 3139
    iget-object v1, p0, Lcom/google/ac/b/c/an;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_3

    .line 3140
    iget-object v1, p1, Lcom/google/ac/b/c/an;->b:Ljava/lang/Boolean;

    if-nez v1, :cond_0

    .line 3146
    :cond_2
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/an;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 3143
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/an;->b:Ljava/lang/Boolean;

    iget-object v2, p1, Lcom/google/ac/b/c/an;->b:Ljava/lang/Boolean;

    invoke-virtual {v1, v2}, Ljava/lang/Boolean;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 3151
    iget-object v0, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 3154
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/an;->b:Ljava/lang/Boolean;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 3156
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/an;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 3157
    return v0

    .line 3154
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/an;->b:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3091
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/an;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/b/c/al;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/ac/b/c/al;

    invoke-direct {v3}, Lcom/google/ac/b/c/al;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/ac/b/c/al;

    invoke-direct {v3}, Lcom/google/ac/b/c/al;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/an;->b:Ljava/lang/Boolean;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 3163
    iget-object v0, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 3164
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 3165
    iget-object v1, p0, Lcom/google/ac/b/c/an;->a:[Lcom/google/ac/b/c/al;

    aget-object v1, v1, v0

    .line 3166
    if-eqz v1, :cond_0

    .line 3167
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3164
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3171
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/an;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 3172
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/an;->b:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 3174
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3175
    return-void
.end method
