.class public final Lcom/google/ac/b/c/cz;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/c/db;

.field public b:Lcom/google/ac/b/c/ct;

.field public c:Lcom/google/ac/b/c/cv;

.field public d:Lcom/google/ac/b/c/df;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2499
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2500
    iput-object v0, p0, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    iput-object v0, p0, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    iput-object v0, p0, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    iput-object v0, p0, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    iput-object v0, p0, Lcom/google/ac/b/c/cz;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/cz;->cachedSize:I

    .line 2501
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 2596
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2597
    iget-object v1, p0, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    if-eqz v1, :cond_0

    .line 2598
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2601
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    if-eqz v1, :cond_1

    .line 2602
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2605
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    if-eqz v1, :cond_2

    .line 2606
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2609
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    if-eqz v1, :cond_3

    .line 2610
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 2613
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2515
    if-ne p1, p0, :cond_1

    .line 2516
    const/4 v0, 0x1

    .line 2558
    :cond_0
    :goto_0
    return v0

    .line 2518
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/cz;

    if-eqz v1, :cond_0

    .line 2521
    check-cast p1, Lcom/google/ac/b/c/cz;

    .line 2522
    iget-object v1, p0, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    if-nez v1, :cond_6

    .line 2523
    iget-object v1, p1, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    if-nez v1, :cond_0

    .line 2531
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    if-nez v1, :cond_7

    .line 2532
    iget-object v1, p1, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    if-nez v1, :cond_0

    .line 2540
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    if-nez v1, :cond_8

    .line 2541
    iget-object v1, p1, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    if-nez v1, :cond_0

    .line 2549
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    if-nez v1, :cond_9

    .line 2550
    iget-object v1, p1, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    if-nez v1, :cond_0

    .line 2558
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/cz;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2527
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    iget-object v2, p1, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/db;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2536
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    iget-object v2, p1, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/ct;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 2545
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    iget-object v2, p1, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/cv;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 2554
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    iget-object v2, p1, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/df;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2563
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2566
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2568
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2570
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 2572
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/cz;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2573
    return v0

    .line 2563
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {v0}, Lcom/google/ac/b/c/db;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2566
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    invoke-virtual {v0}, Lcom/google/ac/b/c/ct;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2568
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    invoke-virtual {v0}, Lcom/google/ac/b/c/cv;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2570
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    invoke-virtual {v1}, Lcom/google/ac/b/c/df;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 2470
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/cz;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/db;

    invoke-direct {v0}, Lcom/google/ac/b/c/db;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/ct;

    invoke-direct {v0}, Lcom/google/ac/b/c/ct;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/b/c/cv;

    invoke-direct {v0}, Lcom/google/ac/b/c/cv;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/c/df;

    invoke-direct {v0}, Lcom/google/ac/b/c/df;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 2579
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    if-eqz v0, :cond_0

    .line 2580
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/cz;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2582
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    if-eqz v0, :cond_1

    .line 2583
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/cz;->b:Lcom/google/ac/b/c/ct;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2585
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    if-eqz v0, :cond_2

    .line 2586
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/cz;->c:Lcom/google/ac/b/c/cv;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2588
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    if-eqz v0, :cond_3

    .line 2589
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/cz;->d:Lcom/google/ac/b/c/df;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 2591
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2592
    return-void
.end method
