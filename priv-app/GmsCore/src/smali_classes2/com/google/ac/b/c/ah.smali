.class public final Lcom/google/ac/b/c/ah;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile h:[Lcom/google/ac/b/c/ah;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/ac/b/c/bt;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lcom/google/ac/b/c/i;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4754
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 4755
    iput-object v0, p0, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iput-object v0, p0, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    iput-object v0, p0, Lcom/google/ac/b/c/ah;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/ah;->cachedSize:I

    .line 4756
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/ah;
    .locals 2

    .prologue
    .line 4722
    sget-object v0, Lcom/google/ac/b/c/ah;->h:[Lcom/google/ac/b/c/ah;

    if-nez v0, :cond_1

    .line 4723
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 4725
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/ah;->h:[Lcom/google/ac/b/c/ah;

    if-nez v0, :cond_0

    .line 4726
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/ah;

    sput-object v0, Lcom/google/ac/b/c/ah;->h:[Lcom/google/ac/b/c/ah;

    .line 4728
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4730
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/ah;->h:[Lcom/google/ac/b/c/ah;

    return-object v0

    .line 4728
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 4884
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 4885
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 4886
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4889
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    if-eqz v1, :cond_1

    .line 4890
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4893
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 4894
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4897
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 4898
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4901
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 4902
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4905
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 4906
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4909
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    if-eqz v1, :cond_6

    .line 4910
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4913
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 4773
    if-ne p1, p0, :cond_1

    .line 4774
    const/4 v0, 0x1

    .line 4832
    :cond_0
    :goto_0
    return v0

    .line 4776
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/ah;

    if-eqz v1, :cond_0

    .line 4779
    check-cast p1, Lcom/google/ac/b/c/ah;

    .line 4780
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    if-nez v1, :cond_9

    .line 4781
    iget-object v1, p1, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 4786
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    if-nez v1, :cond_a

    .line 4787
    iget-object v1, p1, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    if-nez v1, :cond_0

    .line 4795
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    if-nez v1, :cond_b

    .line 4796
    iget-object v1, p1, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 4802
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    if-nez v1, :cond_c

    .line 4803
    iget-object v1, p1, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 4809
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    if-nez v1, :cond_d

    .line 4810
    iget-object v1, p1, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4816
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    if-nez v1, :cond_e

    .line 4817
    iget-object v1, p1, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 4823
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    if-nez v1, :cond_f

    .line 4824
    iget-object v1, p1, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    if-nez v1, :cond_0

    .line 4832
    :cond_8
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/ah;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 4784
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 4791
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    iget-object v2, p1, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bt;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 4799
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 4806
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 4813
    :cond_d
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 4820
    :cond_e
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 4828
    :cond_f
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    iget-object v2, p1, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 4837
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4839
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 4841
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 4843
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 4845
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 4847
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 4849
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    if-nez v2, :cond_6

    :goto_6
    add-int/2addr v0, v1

    .line 4851
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/ah;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 4852
    return v0

    .line 4837
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 4839
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    invoke-virtual {v0}, Lcom/google/ac/b/c/bt;->hashCode()I

    move-result v0

    goto :goto_1

    .line 4841
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_2

    .line 4843
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_3

    .line 4845
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 4847
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 4849
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    invoke-virtual {v1}, Lcom/google/ac/b/c/i;->hashCode()I

    move-result v1

    goto :goto_6
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 4716
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/ah;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/bt;

    invoke-direct {v0}, Lcom/google/ac/b/c/bt;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/i;

    invoke-direct {v0}, Lcom/google/ac/b/c/i;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 4858
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 4859
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/ah;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4861
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    if-eqz v0, :cond_1

    .line 4862
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/ah;->b:Lcom/google/ac/b/c/bt;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4864
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 4865
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/ah;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 4867
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 4868
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/ah;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 4870
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 4871
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/ah;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4873
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 4874
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/ah;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4876
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    if-eqz v0, :cond_6

    .line 4877
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/b/c/ah;->g:Lcom/google/ac/b/c/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4879
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4880
    return-void
.end method
