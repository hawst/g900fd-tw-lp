.class public final Lcom/google/ac/b/c/cl;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile g:[Lcom/google/ac/b/c/cl;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/google/ac/b/c/cl;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/b/c/cl;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/cl;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/cl;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/cl;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/c/cl;->f:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/cl;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/cl;->cachedSize:I

    .line 45
    return-void
.end method

.method public static a()[Lcom/google/ac/b/c/cl;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/ac/b/c/cl;->g:[Lcom/google/ac/b/c/cl;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/ac/b/c/cl;->g:[Lcom/google/ac/b/c/cl;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/b/c/cl;

    sput-object v0, Lcom/google/ac/b/c/cl;->g:[Lcom/google/ac/b/c/cl;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/ac/b/c/cl;->g:[Lcom/google/ac/b/c/cl;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 156
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 157
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 158
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/cl;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 161
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 162
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/cl;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 165
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 166
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/cl;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 169
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 170
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/cl;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 173
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 174
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/cl;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 177
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->f:Ljava/lang/Long;

    if-eqz v1, :cond_5

    .line 178
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/cl;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 181
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 61
    if-ne p1, p0, :cond_1

    .line 62
    const/4 v0, 0x1

    .line 109
    :cond_0
    :goto_0
    return v0

    .line 64
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/cl;

    if-eqz v1, :cond_0

    .line 67
    check-cast p1, Lcom/google/ac/b/c/cl;

    .line 68
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->a:Ljava/lang/Integer;

    if-nez v1, :cond_8

    .line 69
    iget-object v1, p1, Lcom/google/ac/b/c/cl;->a:Ljava/lang/Integer;

    if-nez v1, :cond_0

    .line 74
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->b:Ljava/lang/String;

    if-nez v1, :cond_9

    .line 75
    iget-object v1, p1, Lcom/google/ac/b/c/cl;->b:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 81
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->c:Ljava/lang/String;

    if-nez v1, :cond_a

    .line 82
    iget-object v1, p1, Lcom/google/ac/b/c/cl;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 88
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->d:Ljava/lang/String;

    if-nez v1, :cond_b

    .line 89
    iget-object v1, p1, Lcom/google/ac/b/c/cl;->d:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 95
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->e:Ljava/lang/String;

    if-nez v1, :cond_c

    .line 96
    iget-object v1, p1, Lcom/google/ac/b/c/cl;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 102
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->f:Ljava/lang/Long;

    if-nez v1, :cond_d

    .line 103
    iget-object v1, p1, Lcom/google/ac/b/c/cl;->f:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 109
    :cond_7
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/cl;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 72
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->a:Ljava/lang/Integer;

    iget-object v2, p1, Lcom/google/ac/b/c/cl;->a:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 78
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->b:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/cl;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 85
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->c:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/cl;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 92
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->d:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/cl;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 99
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/c/cl;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 106
    :cond_d
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->f:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/cl;->f:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 114
    iget-object v0, p0, Lcom/google/ac/b/c/cl;->a:Ljava/lang/Integer;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 116
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/cl;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 118
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/cl;->c:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 120
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/cl;->d:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 122
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/cl;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 124
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/cl;->f:Ljava/lang/Long;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 126
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/cl;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 127
    return v0

    .line 114
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cl;->a:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0

    .line 116
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cl;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 118
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/cl;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 120
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/cl;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 122
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/cl;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 124
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/cl;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/cl;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cl;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cl;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cl;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cl;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cl;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/cl;->f:Ljava/lang/Long;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/ac/b/c/cl;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 134
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/cl;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 136
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/cl;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 137
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/cl;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 139
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/cl;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 140
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/cl;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 142
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/cl;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 143
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/cl;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 145
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/cl;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 146
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/cl;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 148
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/cl;->f:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 149
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/cl;->f:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 151
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 152
    return-void
.end method
