.class public final Lcom/google/ac/b/b/a/aa;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/a/a/b;

.field public b:Lcom/google/ac/b/b/a/i;

.field public c:Lcom/google/ac/b/b/a/i;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Lcom/google/ac/b/b/a/k;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 398
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 399
    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->a:Lcom/google/ac/b/a/a/b;

    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->d:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/b/a/aa;->cachedSize:I

    .line 400
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 534
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 535
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->a:Lcom/google/ac/b/a/a/b;

    if-eqz v1, :cond_0

    .line 536
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/b/a/aa;->a:Lcom/google/ac/b/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 539
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    if-eqz v1, :cond_1

    .line 540
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 543
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    if-eqz v1, :cond_2

    .line 544
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 547
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 548
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/b/a/aa;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 551
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 552
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/b/b/a/aa;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 555
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 556
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/ac/b/b/a/aa;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 559
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    if-eqz v1, :cond_6

    .line 560
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 563
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 417
    if-ne p1, p0, :cond_1

    .line 418
    const/4 v0, 0x1

    .line 481
    :cond_0
    :goto_0
    return v0

    .line 420
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/b/a/aa;

    if-eqz v1, :cond_0

    .line 423
    check-cast p1, Lcom/google/ac/b/b/a/aa;

    .line 424
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->a:Lcom/google/ac/b/a/a/b;

    if-nez v1, :cond_9

    .line 425
    iget-object v1, p1, Lcom/google/ac/b/b/a/aa;->a:Lcom/google/ac/b/a/a/b;

    if-nez v1, :cond_0

    .line 433
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    if-nez v1, :cond_a

    .line 434
    iget-object v1, p1, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    if-nez v1, :cond_0

    .line 442
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    if-nez v1, :cond_b

    .line 443
    iget-object v1, p1, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    if-nez v1, :cond_0

    .line 451
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->d:Ljava/lang/Long;

    if-nez v1, :cond_c

    .line 452
    iget-object v1, p1, Lcom/google/ac/b/b/a/aa;->d:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 458
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->e:Ljava/lang/String;

    if-nez v1, :cond_d

    .line 459
    iget-object v1, p1, Lcom/google/ac/b/b/a/aa;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 465
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->f:Ljava/lang/String;

    if-nez v1, :cond_e

    .line 466
    iget-object v1, p1, Lcom/google/ac/b/b/a/aa;->f:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 472
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    if-nez v1, :cond_f

    .line 473
    iget-object v1, p1, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    if-nez v1, :cond_0

    .line 481
    :cond_8
    invoke-virtual {p0, p1}, Lcom/google/ac/b/b/a/aa;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 429
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->a:Lcom/google/ac/b/a/a/b;

    iget-object v2, p1, Lcom/google/ac/b/b/a/aa;->a:Lcom/google/ac/b/a/a/b;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/a/a/b;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 438
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    iget-object v2, p1, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 447
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    iget-object v2, p1, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/i;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 455
    :cond_c
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->d:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/b/a/aa;->d:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 462
    :cond_d
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->e:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/b/a/aa;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0

    .line 469
    :cond_e
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->f:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/ac/b/b/a/aa;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    goto/16 :goto_0

    .line 477
    :cond_f
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    iget-object v2, p1, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/b/a/k;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 486
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->a:Lcom/google/ac/b/a/a/b;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 489
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 491
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 493
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->d:Ljava/lang/Long;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 495
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->e:Ljava/lang/String;

    if-nez v0, :cond_4

    move v0, v1

    :goto_4
    add-int/2addr v0, v2

    .line 497
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->f:Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_5
    add-int/2addr v0, v2

    .line 499
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    if-nez v2, :cond_6

    :goto_6
    add-int/2addr v0, v1

    .line 501
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/b/a/aa;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 502
    return v0

    .line 486
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->a:Lcom/google/ac/b/a/a/b;

    invoke-virtual {v0}, Lcom/google/ac/b/a/a/b;->hashCode()I

    move-result v0

    goto :goto_0

    .line 489
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/i;->hashCode()I

    move-result v0

    goto :goto_1

    .line 491
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    invoke-virtual {v0}, Lcom/google/ac/b/b/a/i;->hashCode()I

    move-result v0

    goto :goto_2

    .line 493
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_3

    .line 495
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_4

    .line 497
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_5

    .line 499
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    invoke-virtual {v1}, Lcom/google/ac/b/b/a/k;->hashCode()I

    move-result v1

    goto :goto_6
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 360
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/b/a/aa;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->a:Lcom/google/ac/b/a/a/b;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/a/a/b;

    invoke-direct {v0}, Lcom/google/ac/b/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->a:Lcom/google/ac/b/a/a/b;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->a:Lcom/google/ac/b/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/b/a/i;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/b/b/a/i;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/i;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/b/a/k;

    invoke-direct {v0}, Lcom/google/ac/b/b/a/k;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x22 -> :sswitch_2
        0x2a -> :sswitch_3
        0x30 -> :sswitch_4
        0x3a -> :sswitch_5
        0x4a -> :sswitch_6
        0x52 -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->a:Lcom/google/ac/b/a/a/b;

    if-eqz v0, :cond_0

    .line 509
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->a:Lcom/google/ac/b/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 511
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    if-eqz v0, :cond_1

    .line 512
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->b:Lcom/google/ac/b/b/a/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 514
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    if-eqz v0, :cond_2

    .line 515
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->c:Lcom/google/ac/b/b/a/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 517
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 518
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 520
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 521
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 523
    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 524
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 526
    :cond_5
    iget-object v0, p0, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    if-eqz v0, :cond_6

    .line 527
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/ac/b/b/a/aa;->g:Lcom/google/ac/b/b/a/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 529
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 530
    return-void
.end method
