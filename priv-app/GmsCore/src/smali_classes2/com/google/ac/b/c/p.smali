.class public final Lcom/google/ac/b/c/p;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Long;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/Long;

.field public d:Ljava/lang/Long;

.field public e:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 2430
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 2431
    iput-object v0, p0, Lcom/google/ac/b/c/p;->a:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/p;->b:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/p;->c:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/p;->d:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/p;->e:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/b/c/p;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/p;->cachedSize:I

    .line 2432
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 2532
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 2533
    iget-object v1, p0, Lcom/google/ac/b/c/p;->a:Ljava/lang/Long;

    if-eqz v1, :cond_0

    .line 2534
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/p;->a:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2537
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/p;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 2538
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/p;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2541
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/p;->c:Ljava/lang/Long;

    if-eqz v1, :cond_2

    .line 2542
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/p;->c:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2545
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/p;->d:Ljava/lang/Long;

    if-eqz v1, :cond_3

    .line 2546
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/b/c/p;->d:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2549
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/p;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 2550
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/b/c/p;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 2553
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 2447
    if-ne p1, p0, :cond_1

    .line 2448
    const/4 v0, 0x1

    .line 2489
    :cond_0
    :goto_0
    return v0

    .line 2450
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/p;

    if-eqz v1, :cond_0

    .line 2453
    check-cast p1, Lcom/google/ac/b/c/p;

    .line 2454
    iget-object v1, p0, Lcom/google/ac/b/c/p;->a:Ljava/lang/Long;

    if-nez v1, :cond_7

    .line 2455
    iget-object v1, p1, Lcom/google/ac/b/c/p;->a:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 2461
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/p;->b:Ljava/lang/Long;

    if-nez v1, :cond_8

    .line 2462
    iget-object v1, p1, Lcom/google/ac/b/c/p;->b:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 2468
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/p;->c:Ljava/lang/Long;

    if-nez v1, :cond_9

    .line 2469
    iget-object v1, p1, Lcom/google/ac/b/c/p;->c:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 2475
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/p;->d:Ljava/lang/Long;

    if-nez v1, :cond_a

    .line 2476
    iget-object v1, p1, Lcom/google/ac/b/c/p;->d:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 2482
    :cond_5
    iget-object v1, p0, Lcom/google/ac/b/c/p;->e:Ljava/lang/Long;

    if-nez v1, :cond_b

    .line 2483
    iget-object v1, p1, Lcom/google/ac/b/c/p;->e:Ljava/lang/Long;

    if-nez v1, :cond_0

    .line 2489
    :cond_6
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/p;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 2458
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/p;->a:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/p;->a:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 2465
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/p;->b:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/p;->b:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 2472
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/p;->c:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/p;->c:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 2479
    :cond_a
    iget-object v1, p0, Lcom/google/ac/b/c/p;->d:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/p;->d:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0

    .line 2486
    :cond_b
    iget-object v1, p0, Lcom/google/ac/b/c/p;->e:Ljava/lang/Long;

    iget-object v2, p1, Lcom/google/ac/b/c/p;->e:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 2494
    iget-object v0, p0, Lcom/google/ac/b/c/p;->a:Ljava/lang/Long;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 2497
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/p;->b:Ljava/lang/Long;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 2499
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/p;->c:Ljava/lang/Long;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 2501
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/p;->d:Ljava/lang/Long;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 2503
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/p;->e:Ljava/lang/Long;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 2505
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/p;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 2506
    return v0

    .line 2494
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/p;->a:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_0

    .line 2497
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/p;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_1

    .line 2499
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/p;->c:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_2

    .line 2501
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/p;->d:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    goto :goto_3

    .line 2503
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/p;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 2398
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/p;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/p;->a:Ljava/lang/Long;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/p;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/p;->c:Ljava/lang/Long;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/p;->d:Ljava/lang/Long;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/b/c/p;->e:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x20 -> :sswitch_3
        0x28 -> :sswitch_4
        0x30 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 2512
    iget-object v0, p0, Lcom/google/ac/b/c/p;->a:Ljava/lang/Long;

    if-eqz v0, :cond_0

    .line 2513
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/p;->a:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2515
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/p;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 2516
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/p;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2518
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/p;->c:Ljava/lang/Long;

    if-eqz v0, :cond_2

    .line 2519
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/p;->c:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2521
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/p;->d:Ljava/lang/Long;

    if-eqz v0, :cond_3

    .line 2522
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/b/c/p;->d:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2524
    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/p;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 2525
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/b/c/p;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 2527
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 2528
    return-void
.end method
