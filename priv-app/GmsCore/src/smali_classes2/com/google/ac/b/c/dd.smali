.class public final Lcom/google/ac/b/c/dd;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/b/c/db;

.field public b:Lcom/google/ac/b/c/bi;

.field public c:Lcom/google/ac/b/c/bj;

.field public d:Lcom/google/ac/b/c/bk;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1210
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 1211
    iput-object v0, p0, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    iput-object v0, p0, Lcom/google/ac/b/c/dd;->b:Lcom/google/ac/b/c/bi;

    iput-object v0, p0, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    iput-object v0, p0, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    iput-object v0, p0, Lcom/google/ac/b/c/dd;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/b/c/dd;->cachedSize:I

    .line 1212
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1307
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 1308
    iget-object v1, p0, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    if-eqz v1, :cond_0

    .line 1309
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1312
    :cond_0
    iget-object v1, p0, Lcom/google/ac/b/c/dd;->b:Lcom/google/ac/b/c/bi;

    if-eqz v1, :cond_1

    .line 1313
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/b/c/dd;->b:Lcom/google/ac/b/c/bi;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1316
    :cond_1
    iget-object v1, p0, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    if-eqz v1, :cond_2

    .line 1317
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1320
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    if-eqz v1, :cond_3

    .line 1321
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1324
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 1226
    if-ne p1, p0, :cond_1

    .line 1227
    const/4 v0, 0x1

    .line 1269
    :cond_0
    :goto_0
    return v0

    .line 1229
    :cond_1
    instance-of v1, p1, Lcom/google/ac/b/c/dd;

    if-eqz v1, :cond_0

    .line 1232
    check-cast p1, Lcom/google/ac/b/c/dd;

    .line 1233
    iget-object v1, p0, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    if-nez v1, :cond_6

    .line 1234
    iget-object v1, p1, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    if-nez v1, :cond_0

    .line 1242
    :cond_2
    iget-object v1, p0, Lcom/google/ac/b/c/dd;->b:Lcom/google/ac/b/c/bi;

    if-nez v1, :cond_7

    .line 1243
    iget-object v1, p1, Lcom/google/ac/b/c/dd;->b:Lcom/google/ac/b/c/bi;

    if-nez v1, :cond_0

    .line 1251
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    if-nez v1, :cond_8

    .line 1252
    iget-object v1, p1, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    if-nez v1, :cond_0

    .line 1260
    :cond_4
    iget-object v1, p0, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    if-nez v1, :cond_9

    .line 1261
    iget-object v1, p1, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    if-nez v1, :cond_0

    .line 1269
    :cond_5
    invoke-virtual {p0, p1}, Lcom/google/ac/b/c/dd;->unknownFieldDataEquals(Lcom/google/protobuf/nano/d;)Z

    move-result v0

    goto :goto_0

    .line 1238
    :cond_6
    iget-object v1, p0, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    iget-object v2, p1, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/db;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    .line 1247
    :cond_7
    iget-object v1, p0, Lcom/google/ac/b/c/dd;->b:Lcom/google/ac/b/c/bi;

    iget-object v2, p1, Lcom/google/ac/b/c/dd;->b:Lcom/google/ac/b/c/bi;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bi;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0

    .line 1256
    :cond_8
    iget-object v1, p0, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    iget-object v2, p1, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bj;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    goto :goto_0

    .line 1265
    :cond_9
    iget-object v1, p0, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    iget-object v2, p1, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    invoke-virtual {v1, v2}, Lcom/google/ac/b/c/bk;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1274
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 1277
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/dd;->b:Lcom/google/ac/b/c/bi;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1279
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1281
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 1283
    mul-int/lit8 v0, v0, 0x1f

    invoke-virtual {p0}, Lcom/google/ac/b/c/dd;->unknownFieldDataHashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 1284
    return v0

    .line 1274
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {v0}, Lcom/google/ac/b/c/db;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1277
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->b:Lcom/google/ac/b/c/bi;

    invoke-virtual {v0}, Lcom/google/ac/b/c/bi;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1279
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    invoke-virtual {v0}, Lcom/google/ac/b/c/bj;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1281
    :cond_3
    iget-object v1, p0, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    invoke-virtual {v1}, Lcom/google/ac/b/c/bk;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 1181
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/b/c/dd;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/b/c/db;

    invoke-direct {v0}, Lcom/google/ac/b/c/db;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->b:Lcom/google/ac/b/c/bi;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/b/c/bi;

    invoke-direct {v0}, Lcom/google/ac/b/c/bi;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/dd;->b:Lcom/google/ac/b/c/bi;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->b:Lcom/google/ac/b/c/bi;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/b/c/bj;

    invoke-direct {v0}, Lcom/google/ac/b/c/bj;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/b/c/bk;

    invoke-direct {v0}, Lcom/google/ac/b/c/bk;-><init>()V

    iput-object v0, p0, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1290
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    if-eqz v0, :cond_0

    .line 1291
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/b/c/dd;->a:Lcom/google/ac/b/c/db;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1293
    :cond_0
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->b:Lcom/google/ac/b/c/bi;

    if-eqz v0, :cond_1

    .line 1294
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/b/c/dd;->b:Lcom/google/ac/b/c/bi;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1296
    :cond_1
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    if-eqz v0, :cond_2

    .line 1297
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/b/c/dd;->c:Lcom/google/ac/b/c/bj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1299
    :cond_2
    iget-object v0, p0, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    if-eqz v0, :cond_3

    .line 1300
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/b/c/dd;->d:Lcom/google/ac/b/c/bk;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1302
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1303
    return-void
.end method
