.class public final Lcom/google/ac/d/a/d;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile i:[Lcom/google/ac/d/a/d;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Boolean;

.field public c:Ljava/lang/Double;

.field public d:[Ljava/lang/String;

.field public e:Ljava/lang/Long;

.field public f:Ljava/lang/Long;

.field public g:Ljava/lang/Integer;

.field public h:[J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 141
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 142
    iput-object v1, p0, Lcom/google/ac/d/a/d;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/ac/d/a/d;->b:Ljava/lang/Boolean;

    iput-object v1, p0, Lcom/google/ac/d/a/d;->c:Ljava/lang/Double;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/d/a/d;->d:[Ljava/lang/String;

    iput-object v1, p0, Lcom/google/ac/d/a/d;->e:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/ac/d/a/d;->f:Ljava/lang/Long;

    iput-object v1, p0, Lcom/google/ac/d/a/d;->g:Ljava/lang/Integer;

    sget-object v0, Lcom/google/protobuf/nano/m;->b:[J

    iput-object v0, p0, Lcom/google/ac/d/a/d;->h:[J

    iput-object v1, p0, Lcom/google/ac/d/a/d;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/d/a/d;->cachedSize:I

    .line 143
    return-void
.end method

.method public static a()[Lcom/google/ac/d/a/d;
    .locals 2

    .prologue
    .line 106
    sget-object v0, Lcom/google/ac/d/a/d;->i:[Lcom/google/ac/d/a/d;

    if-nez v0, :cond_1

    .line 107
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 109
    :try_start_0
    sget-object v0, Lcom/google/ac/d/a/d;->i:[Lcom/google/ac/d/a/d;

    if-nez v0, :cond_0

    .line 110
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/d/a/d;

    sput-object v0, Lcom/google/ac/d/a/d;->i:[Lcom/google/ac/d/a/d;

    .line 112
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 114
    :cond_1
    sget-object v0, Lcom/google/ac/d/a/d;->i:[Lcom/google/ac/d/a/d;

    return-object v0

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 196
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 197
    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/ac/d/a/d;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 199
    iget-object v1, p0, Lcom/google/ac/d/a/d;->b:Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 200
    const/4 v1, 0x2

    iget-object v3, p0, Lcom/google/ac/d/a/d;->b:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 203
    :cond_0
    iget-object v1, p0, Lcom/google/ac/d/a/d;->c:Ljava/lang/Double;

    if-eqz v1, :cond_1

    .line 204
    const/4 v1, 0x3

    iget-object v3, p0, Lcom/google/ac/d/a/d;->c:Ljava/lang/Double;

    invoke-virtual {v3}, Ljava/lang/Double;->doubleValue()D

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 207
    :cond_1
    iget-object v1, p0, Lcom/google/ac/d/a/d;->d:[Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/ac/d/a/d;->d:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_4

    move v1, v2

    move v3, v2

    move v4, v2

    .line 210
    :goto_0
    iget-object v5, p0, Lcom/google/ac/d/a/d;->d:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_3

    .line 211
    iget-object v5, p0, Lcom/google/ac/d/a/d;->d:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 212
    if-eqz v5, :cond_2

    .line 213
    add-int/lit8 v4, v4, 0x1

    .line 214
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 210
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 218
    :cond_3
    add-int/2addr v0, v3

    .line 219
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 221
    :cond_4
    iget-object v1, p0, Lcom/google/ac/d/a/d;->g:Ljava/lang/Integer;

    if-eqz v1, :cond_5

    .line 222
    const/4 v1, 0x5

    iget-object v3, p0, Lcom/google/ac/d/a/d;->g:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 225
    :cond_5
    iget-object v1, p0, Lcom/google/ac/d/a/d;->e:Ljava/lang/Long;

    if-eqz v1, :cond_6

    .line 226
    const/4 v1, 0x6

    iget-object v3, p0, Lcom/google/ac/d/a/d;->e:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 229
    :cond_6
    iget-object v1, p0, Lcom/google/ac/d/a/d;->f:Ljava/lang/Long;

    if-eqz v1, :cond_7

    .line 230
    const/4 v1, 0x7

    iget-object v3, p0, Lcom/google/ac/d/a/d;->f:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 233
    :cond_7
    iget-object v1, p0, Lcom/google/ac/d/a/d;->h:[J

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/google/ac/d/a/d;->h:[J

    array-length v1, v1

    if-lez v1, :cond_9

    move v1, v2

    .line 235
    :goto_1
    iget-object v3, p0, Lcom/google/ac/d/a/d;->h:[J

    array-length v3, v3

    if-ge v2, v3, :cond_8

    .line 236
    iget-object v3, p0, Lcom/google/ac/d/a/d;->h:[J

    aget-wide v4, v3, v2

    .line 237
    invoke-static {v4, v5}, Lcom/google/protobuf/nano/b;->a(J)I

    move-result v3

    add-int/2addr v1, v3

    .line 235
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 240
    :cond_8
    add-int/2addr v0, v1

    .line 241
    iget-object v1, p0, Lcom/google/ac/d/a/d;->h:[J

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 243
    :cond_9
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 100
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/d/a/d;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/d/a/d;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/d/a/d;->b:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/d/a/d;->c:Ljava/lang/Double;

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/d/a/d;->d:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/ac/d/a/d;->d:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/ac/d/a/d;->d:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/ac/d/a/d;->d:[Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/d/a/d;->g:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/d/a/d;->e:Ljava/lang/Long;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/d/a/d;->f:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_8
    const/16 v0, 0x40

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/d/a/d;->h:[J

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [J

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/ac/d/a/d;->h:[J

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/ac/d/a/d;->h:[J

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lcom/google/ac/d/a/d;->h:[J

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :cond_7
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/ac/d/a/d;->h:[J

    if-nez v2, :cond_9

    move v2, v1

    :goto_6
    add-int/2addr v0, v2

    new-array v0, v0, [J

    if-eqz v2, :cond_8

    iget-object v4, p0, Lcom/google/ac/d/a/d;->h:[J

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_7
    array-length v4, v0

    if-ge v2, v4, :cond_a

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v4

    aput-wide v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_7

    :cond_9
    iget-object v2, p0, Lcom/google/ac/d/a/d;->h:[J

    array-length v2, v2

    goto :goto_6

    :cond_a
    iput-object v0, p0, Lcom/google/ac/d/a/d;->h:[J

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x19 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x42 -> :sswitch_9
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 162
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/ac/d/a/d;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 163
    iget-object v0, p0, Lcom/google/ac/d/a/d;->b:Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 164
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/ac/d/a/d;->b:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 166
    :cond_0
    iget-object v0, p0, Lcom/google/ac/d/a/d;->c:Ljava/lang/Double;

    if-eqz v0, :cond_1

    .line 167
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/ac/d/a/d;->c:Ljava/lang/Double;

    invoke-virtual {v2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(ID)V

    .line 169
    :cond_1
    iget-object v0, p0, Lcom/google/ac/d/a/d;->d:[Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ac/d/a/d;->d:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    .line 170
    :goto_0
    iget-object v2, p0, Lcom/google/ac/d/a/d;->d:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 171
    iget-object v2, p0, Lcom/google/ac/d/a/d;->d:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 172
    if-eqz v2, :cond_2

    .line 173
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 170
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 177
    :cond_3
    iget-object v0, p0, Lcom/google/ac/d/a/d;->g:Ljava/lang/Integer;

    if-eqz v0, :cond_4

    .line 178
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/ac/d/a/d;->g:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 180
    :cond_4
    iget-object v0, p0, Lcom/google/ac/d/a/d;->e:Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 181
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/ac/d/a/d;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 183
    :cond_5
    iget-object v0, p0, Lcom/google/ac/d/a/d;->f:Ljava/lang/Long;

    if-eqz v0, :cond_6

    .line 184
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/ac/d/a/d;->f:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 186
    :cond_6
    iget-object v0, p0, Lcom/google/ac/d/a/d;->h:[J

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/ac/d/a/d;->h:[J

    array-length v0, v0

    if-lez v0, :cond_7

    .line 187
    :goto_1
    iget-object v0, p0, Lcom/google/ac/d/a/d;->h:[J

    array-length v0, v0

    if-ge v1, v0, :cond_7

    .line 188
    const/16 v0, 0x8

    iget-object v2, p0, Lcom/google/ac/d/a/d;->h:[J

    aget-wide v2, v2, v1

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 187
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 191
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 192
    return-void
.end method
