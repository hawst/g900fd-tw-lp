.class public final Lcom/google/ac/e/a/a/b;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile f:[Lcom/google/ac/e/a/a/b;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/Long;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Long;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 485
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 486
    iput-object v0, p0, Lcom/google/ac/e/a/a/b;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/e/a/a/b;->b:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/e/a/a/b;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/e/a/a/b;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/e/a/a/b;->e:Ljava/lang/Long;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/e/a/a/b;->cachedSize:I

    .line 487
    return-void
.end method

.method public static a()[Lcom/google/ac/e/a/a/b;
    .locals 2

    .prologue
    .line 459
    sget-object v0, Lcom/google/ac/e/a/a/b;->f:[Lcom/google/ac/e/a/a/b;

    if-nez v0, :cond_1

    .line 460
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 462
    :try_start_0
    sget-object v0, Lcom/google/ac/e/a/a/b;->f:[Lcom/google/ac/e/a/a/b;

    if-nez v0, :cond_0

    .line 463
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/e/a/a/b;

    sput-object v0, Lcom/google/ac/e/a/a/b;->f:[Lcom/google/ac/e/a/a/b;

    .line 465
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 467
    :cond_1
    sget-object v0, Lcom/google/ac/e/a/a/b;->f:[Lcom/google/ac/e/a/a/b;

    return-object v0

    .line 465
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 522
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 523
    iget-object v1, p0, Lcom/google/ac/e/a/a/b;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 524
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/e/a/a/b;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 527
    :cond_0
    iget-object v1, p0, Lcom/google/ac/e/a/a/b;->b:Ljava/lang/Long;

    if-eqz v1, :cond_1

    .line 528
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/e/a/a/b;->b:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 531
    :cond_1
    iget-object v1, p0, Lcom/google/ac/e/a/a/b;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 532
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/e/a/a/b;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 535
    :cond_2
    iget-object v1, p0, Lcom/google/ac/e/a/a/b;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 536
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/e/a/a/b;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 539
    :cond_3
    iget-object v1, p0, Lcom/google/ac/e/a/a/b;->e:Ljava/lang/Long;

    if-eqz v1, :cond_4

    .line 540
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/e/a/a/b;->e:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 543
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 453
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/e/a/a/b;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/e/a/a/b;->b:Ljava/lang/Long;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/e/a/a/b;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/e/a/a/b;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/e/a/a/b;->e:Ljava/lang/Long;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 502
    iget-object v0, p0, Lcom/google/ac/e/a/a/b;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 503
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/e/a/a/b;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 505
    :cond_0
    iget-object v0, p0, Lcom/google/ac/e/a/a/b;->b:Ljava/lang/Long;

    if-eqz v0, :cond_1

    .line 506
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/e/a/a/b;->b:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 508
    :cond_1
    iget-object v0, p0, Lcom/google/ac/e/a/a/b;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 509
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/e/a/a/b;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 511
    :cond_2
    iget-object v0, p0, Lcom/google/ac/e/a/a/b;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 512
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/e/a/a/b;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 514
    :cond_3
    iget-object v0, p0, Lcom/google/ac/e/a/a/b;->e:Ljava/lang/Long;

    if-eqz v0, :cond_4

    .line 515
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/e/a/a/b;->e:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 517
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 518
    return-void
.end method
