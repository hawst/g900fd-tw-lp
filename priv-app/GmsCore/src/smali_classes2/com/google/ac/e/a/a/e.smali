.class public final Lcom/google/ac/e/a/a/e;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/ac/e/a/a/e;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Lcom/google/c/f/b/f;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 95
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 96
    iput-object v0, p0, Lcom/google/ac/e/a/a/e;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/e/a/a/e;->b:Lcom/google/c/f/b/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/e/a/a/e;->cachedSize:I

    .line 97
    return-void
.end method

.method public static a()[Lcom/google/ac/e/a/a/e;
    .locals 2

    .prologue
    .line 78
    sget-object v0, Lcom/google/ac/e/a/a/e;->c:[Lcom/google/ac/e/a/a/e;

    if-nez v0, :cond_1

    .line 79
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 81
    :try_start_0
    sget-object v0, Lcom/google/ac/e/a/a/e;->c:[Lcom/google/ac/e/a/a/e;

    if-nez v0, :cond_0

    .line 82
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/e/a/a/e;

    sput-object v0, Lcom/google/ac/e/a/a/e;->c:[Lcom/google/ac/e/a/a/e;

    .line 84
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    :cond_1
    sget-object v0, Lcom/google/ac/e/a/a/e;->c:[Lcom/google/ac/e/a/a/e;

    return-object v0

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 120
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 121
    iget-object v1, p0, Lcom/google/ac/e/a/a/e;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 122
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/e/a/a/e;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 125
    :cond_0
    iget-object v1, p0, Lcom/google/ac/e/a/a/e;->b:Lcom/google/c/f/b/f;

    if-eqz v1, :cond_1

    .line 126
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/e/a/a/e;->b:Lcom/google/c/f/b/f;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 129
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 72
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/e/a/a/e;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/e/a/a/e;->b:Lcom/google/c/f/b/f;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/c/f/b/f;

    invoke-direct {v0}, Lcom/google/c/f/b/f;-><init>()V

    iput-object v0, p0, Lcom/google/ac/e/a/a/e;->b:Lcom/google/c/f/b/f;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/e/a/a/e;->b:Lcom/google/c/f/b/f;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/ac/e/a/a/e;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 110
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/e/a/a/e;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 112
    :cond_0
    iget-object v0, p0, Lcom/google/ac/e/a/a/e;->b:Lcom/google/c/f/b/f;

    if-eqz v0, :cond_1

    .line 113
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/e/a/a/e;->b:Lcom/google/c/f/b/f;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 115
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 116
    return-void
.end method
