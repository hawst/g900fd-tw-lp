.class public final Lcom/google/ac/e/a/a/f;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/ac/e/a/a/f;


# instance fields
.field public a:Lcom/google/ac/e/a/a/e;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 201
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 202
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ac/e/a/a/f;->a:Lcom/google/ac/e/a/a/e;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/e/a/a/f;->cachedSize:I

    .line 203
    return-void
.end method

.method public static a()[Lcom/google/ac/e/a/a/f;
    .locals 2

    .prologue
    .line 187
    sget-object v0, Lcom/google/ac/e/a/a/f;->b:[Lcom/google/ac/e/a/a/f;

    if-nez v0, :cond_1

    .line 188
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 190
    :try_start_0
    sget-object v0, Lcom/google/ac/e/a/a/f;->b:[Lcom/google/ac/e/a/a/f;

    if-nez v0, :cond_0

    .line 191
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/e/a/a/f;

    sput-object v0, Lcom/google/ac/e/a/a/f;->b:[Lcom/google/ac/e/a/a/f;

    .line 193
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 195
    :cond_1
    sget-object v0, Lcom/google/ac/e/a/a/f;->b:[Lcom/google/ac/e/a/a/f;

    return-object v0

    .line 193
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 222
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 223
    iget-object v1, p0, Lcom/google/ac/e/a/a/f;->a:Lcom/google/ac/e/a/a/e;

    if-eqz v1, :cond_0

    .line 224
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/e/a/a/f;->a:Lcom/google/ac/e/a/a/e;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 227
    :cond_0
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 181
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/e/a/a/f;->a:Lcom/google/ac/e/a/a/e;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/e/a/a/e;

    invoke-direct {v0}, Lcom/google/ac/e/a/a/e;-><init>()V

    iput-object v0, p0, Lcom/google/ac/e/a/a/f;->a:Lcom/google/ac/e/a/a/e;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/e/a/a/f;->a:Lcom/google/ac/e/a/a/e;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/ac/e/a/a/f;->a:Lcom/google/ac/e/a/a/e;

    if-eqz v0, :cond_0

    .line 215
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/e/a/a/f;->a:Lcom/google/ac/e/a/a/e;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 217
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 218
    return-void
.end method
