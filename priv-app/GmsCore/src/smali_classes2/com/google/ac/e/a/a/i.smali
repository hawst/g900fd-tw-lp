.class public final Lcom/google/ac/e/a/a/i;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/e/a/a/c;

.field public b:[Lcom/google/ac/e/a/a/e;

.field public c:[Lcom/google/ac/e/a/a/b;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 307
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 308
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/ac/e/a/a/i;->a:Lcom/google/ac/e/a/a/c;

    invoke-static {}, Lcom/google/ac/e/a/a/e;->a()[Lcom/google/ac/e/a/a/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    invoke-static {}, Lcom/google/ac/e/a/a/b;->a()[Lcom/google/ac/e/a/a/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/e/a/a/i;->cachedSize:I

    .line 309
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 346
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 347
    iget-object v2, p0, Lcom/google/ac/e/a/a/i;->a:Lcom/google/ac/e/a/a/c;

    if-eqz v2, :cond_0

    .line 348
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/ac/e/a/a/i;->a:Lcom/google/ac/e/a/a/c;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 351
    :cond_0
    iget-object v2, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v0

    move v0, v1

    .line 352
    :goto_0
    iget-object v3, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 353
    iget-object v3, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    aget-object v3, v3, v0

    .line 354
    if-eqz v3, :cond_1

    .line 355
    const/4 v4, 0x2

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 352
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 360
    :cond_3
    iget-object v2, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 361
    :goto_1
    iget-object v2, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 362
    iget-object v2, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    aget-object v2, v2, v1

    .line 363
    if-eqz v2, :cond_4

    .line 364
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 361
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 369
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 281
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->a:Lcom/google/ac/e/a/a/c;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/e/a/a/c;

    invoke-direct {v0}, Lcom/google/ac/e/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/ac/e/a/a/i;->a:Lcom/google/ac/e/a/a/c;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->a:Lcom/google/ac/e/a/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/e/a/a/e;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/ac/e/a/a/e;

    invoke-direct {v3}, Lcom/google/ac/e/a/a/e;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/ac/e/a/a/e;

    invoke-direct {v3}, Lcom/google/ac/e/a/a/e;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    if-nez v0, :cond_6

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/e/a/a/b;

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_7

    new-instance v3, Lcom/google/ac/e/a/a/b;

    invoke-direct {v3}, Lcom/google/ac/e/a/a/b;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    array-length v0, v0

    goto :goto_3

    :cond_7
    new-instance v3, Lcom/google/ac/e/a/a/b;

    invoke-direct {v3}, Lcom/google/ac/e/a/a/b;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 322
    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->a:Lcom/google/ac/e/a/a/c;

    if-eqz v0, :cond_0

    .line 323
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/ac/e/a/a/i;->a:Lcom/google/ac/e/a/a/c;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 325
    :cond_0
    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 326
    :goto_0
    iget-object v2, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 327
    iget-object v2, p0, Lcom/google/ac/e/a/a/i;->b:[Lcom/google/ac/e/a/a/e;

    aget-object v2, v2, v0

    .line 328
    if-eqz v2, :cond_1

    .line 329
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 326
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 333
    :cond_2
    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    array-length v0, v0

    if-lez v0, :cond_4

    .line 334
    :goto_1
    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 335
    iget-object v0, p0, Lcom/google/ac/e/a/a/i;->c:[Lcom/google/ac/e/a/a/b;

    aget-object v0, v0, v1

    .line 336
    if-eqz v0, :cond_3

    .line 337
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 334
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 341
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 342
    return-void
.end method
