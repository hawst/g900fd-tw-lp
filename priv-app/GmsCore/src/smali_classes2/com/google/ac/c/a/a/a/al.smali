.class public final Lcom/google/ac/c/a/a/a/al;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/c/a/a/a/z;

.field public b:[Lcom/google/ac/c/a/a/a/an;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/Integer;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 150
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 151
    iput-object v1, p0, Lcom/google/ac/c/a/a/a/al;->a:Lcom/google/ac/c/a/a/a/z;

    invoke-static {}, Lcom/google/ac/c/a/a/a/an;->a()[Lcom/google/ac/c/a/a/a/an;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    iput-object v1, p0, Lcom/google/ac/c/a/a/a/al;->c:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/ac/c/a/a/a/al;->d:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/ac/c/a/a/a/al;->e:Ljava/lang/Integer;

    iput-object v1, p0, Lcom/google/ac/c/a/a/a/al;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/c/a/a/a/al;->cachedSize:I

    .line 152
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 193
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 194
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/al;->a:Lcom/google/ac/c/a/a/a/z;

    if-eqz v1, :cond_0

    .line 195
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/al;->a:Lcom/google/ac/c/a/a/a/z;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 198
    :cond_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 199
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 200
    iget-object v2, p0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    aget-object v2, v2, v0

    .line 201
    if-eqz v2, :cond_1

    .line 202
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 199
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 207
    :cond_3
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/al;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_4

    .line 208
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/al;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 211
    :cond_4
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/al;->d:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 212
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/al;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 215
    :cond_5
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/al;->e:Ljava/lang/Integer;

    if-eqz v1, :cond_6

    .line 216
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/al;->e:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_6
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 111
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/c/a/a/a/al;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/al;->a:Lcom/google/ac/c/a/a/a/z;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/c/a/a/a/z;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/z;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/al;->a:Lcom/google/ac/c/a/a/a/z;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/al;->a:Lcom/google/ac/c/a/a/a/z;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/c/a/a/a/an;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/ac/c/a/a/a/an;

    invoke-direct {v3}, Lcom/google/ac/c/a/a/a/an;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/ac/c/a/a/a/an;

    invoke-direct {v3}, Lcom/google/ac/c/a/a/a/an;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/al;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/al;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/al;->e:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x28 -> :sswitch_5
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/al;->a:Lcom/google/ac/c/a/a/a/z;

    if-eqz v0, :cond_0

    .line 169
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/al;->a:Lcom/google/ac/c/a/a/a/z;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 171
    :cond_0
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 172
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 173
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/al;->b:[Lcom/google/ac/c/a/a/a/an;

    aget-object v1, v1, v0

    .line 174
    if-eqz v1, :cond_1

    .line 175
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 172
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 179
    :cond_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/al;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_3

    .line 180
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/al;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 182
    :cond_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/al;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 183
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/al;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 185
    :cond_4
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/al;->e:Ljava/lang/Integer;

    if-eqz v0, :cond_5

    .line 186
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/al;->e:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 188
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 189
    return-void
.end method
