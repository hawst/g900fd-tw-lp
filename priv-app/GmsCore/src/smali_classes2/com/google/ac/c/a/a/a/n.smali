.class public final Lcom/google/ac/c/a/a/a/n;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/c/a/a/a/z;

.field public b:Ljava/lang/String;

.field public c:[Lcom/google/ac/c/a/a/a/o;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 179
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 180
    iput-object v1, p0, Lcom/google/ac/c/a/a/a/n;->a:Lcom/google/ac/c/a/a/a/z;

    iput-object v1, p0, Lcom/google/ac/c/a/a/a/n;->b:Ljava/lang/String;

    invoke-static {}, Lcom/google/ac/c/a/a/a/o;->a()[Lcom/google/ac/c/a/a/a/o;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    iput-object v1, p0, Lcom/google/ac/c/a/a/a/n;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/c/a/a/a/n;->cachedSize:I

    .line 181
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 214
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 215
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/n;->a:Lcom/google/ac/c/a/a/a/z;

    if-eqz v1, :cond_0

    .line 216
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/n;->a:Lcom/google/ac/c/a/a/a/z;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 219
    :cond_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/n;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 220
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/n;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 223
    :cond_1
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 224
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 225
    iget-object v2, p0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    aget-object v2, v2, v0

    .line 226
    if-eqz v2, :cond_2

    .line 227
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 224
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 232
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 153
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/c/a/a/a/n;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/n;->a:Lcom/google/ac/c/a/a/a/z;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/c/a/a/a/z;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/z;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/n;->a:Lcom/google/ac/c/a/a/a/z;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/n;->a:Lcom/google/ac/c/a/a/a/z;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/n;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/c/a/a/a/o;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/ac/c/a/a/a/o;

    invoke-direct {v3}, Lcom/google/ac/c/a/a/a/o;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/ac/c/a/a/a/o;

    invoke-direct {v3}, Lcom/google/ac/c/a/a/a/o;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/n;->a:Lcom/google/ac/c/a/a/a/z;

    if-eqz v0, :cond_0

    .line 196
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/n;->a:Lcom/google/ac/c/a/a/a/z;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 198
    :cond_0
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/n;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 199
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/n;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 201
    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 202
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 203
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/n;->c:[Lcom/google/ac/c/a/a/a/o;

    aget-object v1, v1, v0

    .line 204
    if-eqz v1, :cond_2

    .line 205
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 202
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 209
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 210
    return-void
.end method
