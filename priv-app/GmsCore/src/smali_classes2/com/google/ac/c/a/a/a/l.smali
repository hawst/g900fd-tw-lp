.class public final Lcom/google/ac/c/a/a/a/l;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile h:[Lcom/google/ac/c/a/a/a/l;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 145
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 146
    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/c/a/a/a/l;->cachedSize:I

    .line 147
    return-void
.end method

.method public static a()[Lcom/google/ac/c/a/a/a/l;
    .locals 2

    .prologue
    .line 113
    sget-object v0, Lcom/google/ac/c/a/a/a/l;->h:[Lcom/google/ac/c/a/a/a/l;

    if-nez v0, :cond_1

    .line 114
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 116
    :try_start_0
    sget-object v0, Lcom/google/ac/c/a/a/a/l;->h:[Lcom/google/ac/c/a/a/a/l;

    if-nez v0, :cond_0

    .line 117
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/c/a/a/a/l;

    sput-object v0, Lcom/google/ac/c/a/a/a/l;->h:[Lcom/google/ac/c/a/a/a/l;

    .line 119
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    :cond_1
    sget-object v0, Lcom/google/ac/c/a/a/a/l;->h:[Lcom/google/ac/c/a/a/a/l;

    return-object v0

    .line 119
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 191
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 192
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/l;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 193
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/l;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/l;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 197
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/l;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 200
    :cond_1
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/l;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 201
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/l;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 204
    :cond_2
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/l;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 205
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/l;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 208
    :cond_3
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/l;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 209
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/l;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 212
    :cond_4
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/l;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 213
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/l;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 216
    :cond_5
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/l;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 217
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/l;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 220
    :cond_6
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 96
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/c/a/a/a/l;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/l;->g:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/l;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 166
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/l;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 168
    :cond_0
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/l;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 169
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/l;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 171
    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/l;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 172
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/l;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 174
    :cond_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/l;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 175
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/l;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 177
    :cond_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/l;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 178
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/l;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 180
    :cond_4
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/l;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 181
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/l;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 183
    :cond_5
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/l;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 184
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/l;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 186
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 187
    return-void
.end method
