.class public final Lcom/google/ac/c/a/a/a/q;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/protobuf/nano/e;

.field private static final e:[Lcom/google/ac/c/a/a/a/q;


# instance fields
.field public b:Lcom/google/ac/c/a/a/a/x;

.field public c:Lcom/google/ac/c/a/a/a/k;

.field public d:[Lcom/google/ac/c/a/a/a/g;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 15
    const-class v0, Lcom/google/ac/c/a/a/a/q;

    const v1, 0x1eb53ee2

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/e;->a(Ljava/lang/Class;I)Lcom/google/protobuf/nano/e;

    move-result-object v0

    sput-object v0, Lcom/google/ac/c/a/a/a/q;->a:Lcom/google/protobuf/nano/e;

    .line 21
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/c/a/a/a/q;

    sput-object v0, Lcom/google/ac/c/a/a/a/q;->e:[Lcom/google/ac/c/a/a/a/q;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 35
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 36
    iput-object v1, p0, Lcom/google/ac/c/a/a/a/q;->b:Lcom/google/ac/c/a/a/a/x;

    iput-object v1, p0, Lcom/google/ac/c/a/a/a/q;->c:Lcom/google/ac/c/a/a/a/k;

    invoke-static {}, Lcom/google/ac/c/a/a/a/g;->a()[Lcom/google/ac/c/a/a/a/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/q;->d:[Lcom/google/ac/c/a/a/a/g;

    iput-object v1, p0, Lcom/google/ac/c/a/a/a/q;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/c/a/a/a/q;->cachedSize:I

    .line 37
    return-void
.end method

.method public static a([B)Lcom/google/ac/c/a/a/a/q;
    .locals 1

    .prologue
    .line 146
    new-instance v0, Lcom/google/ac/c/a/a/a/q;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/q;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/ac/c/a/a/a/q;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 70
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 71
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/q;->b:Lcom/google/ac/c/a/a/a/x;

    if-eqz v1, :cond_0

    .line 72
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/q;->b:Lcom/google/ac/c/a/a/a/x;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 75
    :cond_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/q;->c:Lcom/google/ac/c/a/a/a/k;

    if-eqz v1, :cond_1

    .line 76
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/q;->c:Lcom/google/ac/c/a/a/a/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 79
    :cond_1
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/q;->d:[Lcom/google/ac/c/a/a/a/g;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/q;->d:[Lcom/google/ac/c/a/a/a/g;

    array-length v1, v1

    if-lez v1, :cond_4

    .line 80
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/ac/c/a/a/a/q;->d:[Lcom/google/ac/c/a/a/a/g;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 81
    iget-object v2, p0, Lcom/google/ac/c/a/a/a/q;->d:[Lcom/google/ac/c/a/a/a/g;

    aget-object v2, v2, v0

    .line 82
    if-eqz v2, :cond_2

    .line 83
    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 80
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    .line 88
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/c/a/a/a/q;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/q;->b:Lcom/google/ac/c/a/a/a/x;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/c/a/a/a/x;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/x;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/q;->b:Lcom/google/ac/c/a/a/a/x;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/q;->b:Lcom/google/ac/c/a/a/a/x;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/q;->c:Lcom/google/ac/c/a/a/a/k;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/c/a/a/a/k;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/k;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/q;->c:Lcom/google/ac/c/a/a/a/k;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/q;->c:Lcom/google/ac/c/a/a/a/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x1a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/c/a/a/a/q;->d:[Lcom/google/ac/c/a/a/a/g;

    if-nez v0, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/c/a/a/a/g;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/ac/c/a/a/a/q;->d:[Lcom/google/ac/c/a/a/a/g;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_3
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_5

    new-instance v3, Lcom/google/ac/c/a/a/a/g;

    invoke-direct {v3}, Lcom/google/ac/c/a/a/a/g;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/q;->d:[Lcom/google/ac/c/a/a/a/g;

    array-length v0, v0

    goto :goto_1

    :cond_5
    new-instance v3, Lcom/google/ac/c/a/a/a/g;

    invoke-direct {v3}, Lcom/google/ac/c/a/a/a/g;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/c/a/a/a/q;->d:[Lcom/google/ac/c/a/a/a/g;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/q;->b:Lcom/google/ac/c/a/a/a/x;

    if-eqz v0, :cond_0

    .line 52
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/q;->b:Lcom/google/ac/c/a/a/a/x;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 54
    :cond_0
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/q;->c:Lcom/google/ac/c/a/a/a/k;

    if-eqz v0, :cond_1

    .line 55
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/q;->c:Lcom/google/ac/c/a/a/a/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 57
    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/q;->d:[Lcom/google/ac/c/a/a/a/g;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/ac/c/a/a/a/q;->d:[Lcom/google/ac/c/a/a/a/g;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 58
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/q;->d:[Lcom/google/ac/c/a/a/a/g;

    array-length v1, v1

    if-ge v0, v1, :cond_3

    .line 59
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/q;->d:[Lcom/google/ac/c/a/a/a/g;

    aget-object v1, v1, v0

    .line 60
    if-eqz v1, :cond_2

    .line 61
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 58
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 65
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 66
    return-void
.end method
