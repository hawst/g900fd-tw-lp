.class public final Lcom/google/ac/c/a/a/a/af;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 912
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 913
    iput-object v0, p0, Lcom/google/ac/c/a/a/a/af;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/af;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/af;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/c/a/a/a/af;->cachedSize:I

    .line 914
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 938
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 939
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/af;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 940
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/af;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 943
    :cond_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/af;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 944
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/af;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 947
    :cond_1
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 882
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/c/a/a/a/af;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/af;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/af;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 927
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/af;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 928
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/af;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 930
    :cond_0
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/af;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 931
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/af;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 933
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 934
    return-void
.end method
