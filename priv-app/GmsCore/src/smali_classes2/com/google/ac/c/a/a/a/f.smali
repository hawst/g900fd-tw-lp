.class public final Lcom/google/ac/c/a/a/a/f;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile h:[Lcom/google/ac/c/a/a/a/f;


# instance fields
.field public a:Lcom/google/ac/c/a/a/a/aq;

.field public b:Lcom/google/ac/c/a/a/a/ai;

.field public c:Lcom/google/ac/c/a/a/a/al;

.field public d:Lcom/google/ac/c/a/a/a/n;

.field public e:Lcom/google/ac/c/a/a/a/b;

.field public f:Lcom/google/ac/c/a/a/a/j;

.field public g:Lcom/google/ac/c/a/a/a/t;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 224
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 225
    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->a:Lcom/google/ac/c/a/a/a/aq;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->c:Lcom/google/ac/c/a/a/a/al;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->e:Lcom/google/ac/c/a/a/a/b;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->f:Lcom/google/ac/c/a/a/a/j;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->g:Lcom/google/ac/c/a/a/a/t;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/c/a/a/a/f;->cachedSize:I

    .line 226
    return-void
.end method

.method public static a()[Lcom/google/ac/c/a/a/a/f;
    .locals 2

    .prologue
    .line 192
    sget-object v0, Lcom/google/ac/c/a/a/a/f;->h:[Lcom/google/ac/c/a/a/a/f;

    if-nez v0, :cond_1

    .line 193
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 195
    :try_start_0
    sget-object v0, Lcom/google/ac/c/a/a/a/f;->h:[Lcom/google/ac/c/a/a/a/f;

    if-nez v0, :cond_0

    .line 196
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/c/a/a/a/f;

    sput-object v0, Lcom/google/ac/c/a/a/a/f;->h:[Lcom/google/ac/c/a/a/a/f;

    .line 198
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 200
    :cond_1
    sget-object v0, Lcom/google/ac/c/a/a/a/f;->h:[Lcom/google/ac/c/a/a/a/f;

    return-object v0

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 270
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 271
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/f;->a:Lcom/google/ac/c/a/a/a/aq;

    if-eqz v1, :cond_0

    .line 272
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/f;->a:Lcom/google/ac/c/a/a/a/aq;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 275
    :cond_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    if-eqz v1, :cond_1

    .line 276
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 279
    :cond_1
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/f;->c:Lcom/google/ac/c/a/a/a/al;

    if-eqz v1, :cond_2

    .line 280
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/f;->c:Lcom/google/ac/c/a/a/a/al;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 283
    :cond_2
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    if-eqz v1, :cond_3

    .line 284
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 287
    :cond_3
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/f;->e:Lcom/google/ac/c/a/a/a/b;

    if-eqz v1, :cond_4

    .line 288
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/f;->e:Lcom/google/ac/c/a/a/a/b;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 291
    :cond_4
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/f;->f:Lcom/google/ac/c/a/a/a/j;

    if-eqz v1, :cond_5

    .line 292
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/f;->f:Lcom/google/ac/c/a/a/a/j;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 295
    :cond_5
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/f;->g:Lcom/google/ac/c/a/a/a/t;

    if-eqz v1, :cond_6

    .line 296
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/f;->g:Lcom/google/ac/c/a/a/a/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 299
    :cond_6
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 186
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/c/a/a/a/f;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->a:Lcom/google/ac/c/a/a/a/aq;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/c/a/a/a/aq;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/aq;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->a:Lcom/google/ac/c/a/a/a/aq;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->a:Lcom/google/ac/c/a/a/a/aq;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/c/a/a/a/ai;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/ai;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->c:Lcom/google/ac/c/a/a/a/al;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/c/a/a/a/al;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/al;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->c:Lcom/google/ac/c/a/a/a/al;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->c:Lcom/google/ac/c/a/a/a/al;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/c/a/a/a/n;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/n;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->e:Lcom/google/ac/c/a/a/a/b;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/ac/c/a/a/a/b;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->e:Lcom/google/ac/c/a/a/a/b;

    :cond_5
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->e:Lcom/google/ac/c/a/a/a/b;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->f:Lcom/google/ac/c/a/a/a/j;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/ac/c/a/a/a/j;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/j;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->f:Lcom/google/ac/c/a/a/a/j;

    :cond_6
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->f:Lcom/google/ac/c/a/a/a/j;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_7
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->g:Lcom/google/ac/c/a/a/a/t;

    if-nez v0, :cond_7

    new-instance v0, Lcom/google/ac/c/a/a/a/t;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/t;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/f;->g:Lcom/google/ac/c/a/a/a/t;

    :cond_7
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->g:Lcom/google/ac/c/a/a/a/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
        0x42 -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->a:Lcom/google/ac/c/a/a/a/aq;

    if-eqz v0, :cond_0

    .line 245
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/f;->a:Lcom/google/ac/c/a/a/a/aq;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 247
    :cond_0
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    if-eqz v0, :cond_1

    .line 248
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/f;->b:Lcom/google/ac/c/a/a/a/ai;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 250
    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->c:Lcom/google/ac/c/a/a/a/al;

    if-eqz v0, :cond_2

    .line 251
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/f;->c:Lcom/google/ac/c/a/a/a/al;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 253
    :cond_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    if-eqz v0, :cond_3

    .line 254
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/f;->d:Lcom/google/ac/c/a/a/a/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 256
    :cond_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->e:Lcom/google/ac/c/a/a/a/b;

    if-eqz v0, :cond_4

    .line 257
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/f;->e:Lcom/google/ac/c/a/a/a/b;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 259
    :cond_4
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->f:Lcom/google/ac/c/a/a/a/j;

    if-eqz v0, :cond_5

    .line 260
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/f;->f:Lcom/google/ac/c/a/a/a/j;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 262
    :cond_5
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/f;->g:Lcom/google/ac/c/a/a/a/t;

    if-eqz v0, :cond_6

    .line 263
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/f;->g:Lcom/google/ac/c/a/a/a/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 265
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 266
    return-void
.end method
