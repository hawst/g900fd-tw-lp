.class public final Lcom/google/ac/c/a/a/a/aj;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/c/a/a/a/aa;

.field public b:Ljava/lang/Integer;

.field public c:Lcom/google/ac/c/a/a/a/ah;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 35
    iput-object v0, p0, Lcom/google/ac/c/a/a/a/aj;->a:Lcom/google/ac/c/a/a/a/aa;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/aj;->b:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/aj;->c:Lcom/google/ac/c/a/a/a/ah;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/aj;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/c/a/a/a/aj;->cachedSize:I

    .line 36
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 64
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 65
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/aj;->a:Lcom/google/ac/c/a/a/a/aa;

    if-eqz v1, :cond_0

    .line 66
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/aj;->a:Lcom/google/ac/c/a/a/a/aa;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 69
    :cond_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/aj;->b:Ljava/lang/Integer;

    if-eqz v1, :cond_1

    .line 70
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/aj;->b:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 73
    :cond_1
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/aj;->c:Lcom/google/ac/c/a/a/a/ah;

    if-eqz v1, :cond_2

    .line 74
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/aj;->c:Lcom/google/ac/c/a/a/a/ah;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 77
    :cond_2
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/c/a/a/a/aj;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/aj;->a:Lcom/google/ac/c/a/a/a/aa;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/c/a/a/a/aa;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/aa;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/aj;->a:Lcom/google/ac/c/a/a/a/aa;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/aj;->a:Lcom/google/ac/c/a/a/a/aa;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/aj;->b:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/aj;->c:Lcom/google/ac/c/a/a/a/ah;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/c/a/a/a/ah;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/ah;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/aj;->c:Lcom/google/ac/c/a/a/a/ah;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/aj;->c:Lcom/google/ac/c/a/a/a/ah;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/aj;->a:Lcom/google/ac/c/a/a/a/aa;

    if-eqz v0, :cond_0

    .line 51
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/aj;->a:Lcom/google/ac/c/a/a/a/aa;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/aj;->b:Ljava/lang/Integer;

    if-eqz v0, :cond_1

    .line 54
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/aj;->b:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 56
    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/aj;->c:Lcom/google/ac/c/a/a/a/ah;

    if-eqz v0, :cond_2

    .line 57
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/aj;->c:Lcom/google/ac/c/a/a/a/ah;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 59
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 60
    return-void
.end method
