.class public final Lcom/google/ac/c/a/a/a/j;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/c/a/a/a/z;

.field public b:[Lcom/google/ac/c/a/a/a/l;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 316
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 317
    iput-object v1, p0, Lcom/google/ac/c/a/a/a/j;->a:Lcom/google/ac/c/a/a/a/z;

    invoke-static {}, Lcom/google/ac/c/a/a/a/l;->a()[Lcom/google/ac/c/a/a/a/l;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/j;->b:[Lcom/google/ac/c/a/a/a/l;

    iput-object v1, p0, Lcom/google/ac/c/a/a/a/j;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/c/a/a/a/j;->cachedSize:I

    .line 318
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 347
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 348
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/j;->a:Lcom/google/ac/c/a/a/a/z;

    if-eqz v1, :cond_0

    .line 349
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/j;->a:Lcom/google/ac/c/a/a/a/z;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 352
    :cond_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/j;->b:[Lcom/google/ac/c/a/a/a/l;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/j;->b:[Lcom/google/ac/c/a/a/a/l;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 353
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/ac/c/a/a/a/j;->b:[Lcom/google/ac/c/a/a/a/l;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 354
    iget-object v2, p0, Lcom/google/ac/c/a/a/a/j;->b:[Lcom/google/ac/c/a/a/a/l;

    aget-object v2, v2, v0

    .line 355
    if-eqz v2, :cond_1

    .line 356
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 353
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 361
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 293
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/c/a/a/a/j;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/j;->a:Lcom/google/ac/c/a/a/a/z;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/c/a/a/a/z;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/z;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/j;->a:Lcom/google/ac/c/a/a/a/z;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/j;->a:Lcom/google/ac/c/a/a/a/z;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/ac/c/a/a/a/j;->b:[Lcom/google/ac/c/a/a/a/l;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/ac/c/a/a/a/l;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/ac/c/a/a/a/j;->b:[Lcom/google/ac/c/a/a/a/l;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/ac/c/a/a/a/l;

    invoke-direct {v3}, Lcom/google/ac/c/a/a/a/l;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/j;->b:[Lcom/google/ac/c/a/a/a/l;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/ac/c/a/a/a/l;

    invoke-direct {v3}, Lcom/google/ac/c/a/a/a/l;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/ac/c/a/a/a/j;->b:[Lcom/google/ac/c/a/a/a/l;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 331
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/j;->a:Lcom/google/ac/c/a/a/a/z;

    if-eqz v0, :cond_0

    .line 332
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/j;->a:Lcom/google/ac/c/a/a/a/z;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 334
    :cond_0
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/j;->b:[Lcom/google/ac/c/a/a/a/l;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/ac/c/a/a/a/j;->b:[Lcom/google/ac/c/a/a/a/l;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 335
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/j;->b:[Lcom/google/ac/c/a/a/a/l;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 336
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/j;->b:[Lcom/google/ac/c/a/a/a/l;

    aget-object v1, v1, v0

    .line 337
    if-eqz v1, :cond_1

    .line 338
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 335
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 342
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 343
    return-void
.end method
