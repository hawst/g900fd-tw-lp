.class public final Lcom/google/ac/c/a/a/a/g;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile g:[Lcom/google/ac/c/a/a/a/g;


# instance fields
.field public a:Lcom/google/ac/c/a/a/a/ar;

.field public b:Lcom/google/ac/c/a/a/a/aj;

.field public c:Lcom/google/ac/c/a/a/a/am;

.field public d:Lcom/google/ac/c/a/a/a/c;

.field public e:Lcom/google/ac/c/a/a/a/k;

.field public f:Lcom/google/ac/c/a/a/a/u;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 43
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 44
    iput-object v0, p0, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/g;->b:Lcom/google/ac/c/a/a/a/aj;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/g;->d:Lcom/google/ac/c/a/a/a/c;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/g;->e:Lcom/google/ac/c/a/a/a/k;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/g;->f:Lcom/google/ac/c/a/a/a/u;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/g;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/c/a/a/a/g;->cachedSize:I

    .line 45
    return-void
.end method

.method public static a()[Lcom/google/ac/c/a/a/a/g;
    .locals 2

    .prologue
    .line 14
    sget-object v0, Lcom/google/ac/c/a/a/a/g;->g:[Lcom/google/ac/c/a/a/a/g;

    if-nez v0, :cond_1

    .line 15
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 17
    :try_start_0
    sget-object v0, Lcom/google/ac/c/a/a/a/g;->g:[Lcom/google/ac/c/a/a/a/g;

    if-nez v0, :cond_0

    .line 18
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/c/a/a/a/g;

    sput-object v0, Lcom/google/ac/c/a/a/a/g;->g:[Lcom/google/ac/c/a/a/a/g;

    .line 20
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 22
    :cond_1
    sget-object v0, Lcom/google/ac/c/a/a/a/g;->g:[Lcom/google/ac/c/a/a/a/g;

    return-object v0

    .line 20
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 85
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 86
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    if-eqz v1, :cond_0

    .line 87
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 90
    :cond_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/g;->b:Lcom/google/ac/c/a/a/a/aj;

    if-eqz v1, :cond_1

    .line 91
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/g;->b:Lcom/google/ac/c/a/a/a/aj;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 94
    :cond_1
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    if-eqz v1, :cond_2

    .line 95
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 98
    :cond_2
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/g;->d:Lcom/google/ac/c/a/a/a/c;

    if-eqz v1, :cond_3

    .line 99
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/g;->d:Lcom/google/ac/c/a/a/a/c;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 102
    :cond_3
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/g;->e:Lcom/google/ac/c/a/a/a/k;

    if-eqz v1, :cond_4

    .line 103
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/g;->e:Lcom/google/ac/c/a/a/a/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 106
    :cond_4
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/g;->f:Lcom/google/ac/c/a/a/a/u;

    if-eqz v1, :cond_5

    .line 107
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/g;->f:Lcom/google/ac/c/a/a/a/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 110
    :cond_5
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 8
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/c/a/a/a/g;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/c/a/a/a/ar;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/ar;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->b:Lcom/google/ac/c/a/a/a/aj;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/c/a/a/a/aj;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/aj;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/g;->b:Lcom/google/ac/c/a/a/a/aj;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->b:Lcom/google/ac/c/a/a/a/aj;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/c/a/a/a/am;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/am;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->d:Lcom/google/ac/c/a/a/a/c;

    if-nez v0, :cond_4

    new-instance v0, Lcom/google/ac/c/a/a/a/c;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/c;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/g;->d:Lcom/google/ac/c/a/a/a/c;

    :cond_4
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->d:Lcom/google/ac/c/a/a/a/c;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->e:Lcom/google/ac/c/a/a/a/k;

    if-nez v0, :cond_5

    new-instance v0, Lcom/google/ac/c/a/a/a/k;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/k;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/g;->e:Lcom/google/ac/c/a/a/a/k;

    :cond_5
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->e:Lcom/google/ac/c/a/a/a/k;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->f:Lcom/google/ac/c/a/a/a/u;

    if-nez v0, :cond_6

    new-instance v0, Lcom/google/ac/c/a/a/a/u;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/u;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/g;->f:Lcom/google/ac/c/a/a/a/u;

    :cond_6
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->f:Lcom/google/ac/c/a/a/a/u;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x12 -> :sswitch_1
        0x1a -> :sswitch_2
        0x22 -> :sswitch_3
        0x2a -> :sswitch_4
        0x32 -> :sswitch_5
        0x3a -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    if-eqz v0, :cond_0

    .line 63
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/g;->a:Lcom/google/ac/c/a/a/a/ar;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->b:Lcom/google/ac/c/a/a/a/aj;

    if-eqz v0, :cond_1

    .line 66
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/g;->b:Lcom/google/ac/c/a/a/a/aj;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    if-eqz v0, :cond_2

    .line 69
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/g;->c:Lcom/google/ac/c/a/a/a/am;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 71
    :cond_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->d:Lcom/google/ac/c/a/a/a/c;

    if-eqz v0, :cond_3

    .line 72
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/g;->d:Lcom/google/ac/c/a/a/a/c;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 74
    :cond_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->e:Lcom/google/ac/c/a/a/a/k;

    if-eqz v0, :cond_4

    .line 75
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/g;->e:Lcom/google/ac/c/a/a/a/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 77
    :cond_4
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/g;->f:Lcom/google/ac/c/a/a/a/u;

    if-eqz v0, :cond_5

    .line 78
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/g;->f:Lcom/google/ac/c/a/a/a/u;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 80
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 81
    return-void
.end method
