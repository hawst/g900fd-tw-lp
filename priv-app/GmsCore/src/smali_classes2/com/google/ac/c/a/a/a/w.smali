.class public final Lcom/google/ac/c/a/a/a/w;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/ac/c/a/a/a/z;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 128
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 129
    iput-object v0, p0, Lcom/google/ac/c/a/a/a/w;->a:Lcom/google/ac/c/a/a/a/z;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/w;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/w;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/w;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/w;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/w;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/c/a/a/a/w;->cachedSize:I

    .line 130
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 166
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 167
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/w;->a:Lcom/google/ac/c/a/a/a/z;

    if-eqz v1, :cond_0

    .line 168
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/w;->a:Lcom/google/ac/c/a/a/a/z;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 171
    :cond_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/w;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 172
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/w;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 175
    :cond_1
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/w;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 176
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/w;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 179
    :cond_2
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/w;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 180
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/w;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 183
    :cond_3
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/w;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 184
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/w;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 187
    :cond_4
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 96
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/c/a/a/a/w;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/w;->a:Lcom/google/ac/c/a/a/a/z;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/c/a/a/a/z;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/z;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/w;->a:Lcom/google/ac/c/a/a/a/z;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/w;->a:Lcom/google/ac/c/a/a/a/z;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/w;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/w;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/w;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/w;->e:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/w;->a:Lcom/google/ac/c/a/a/a/z;

    if-eqz v0, :cond_0

    .line 147
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/w;->a:Lcom/google/ac/c/a/a/a/z;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 149
    :cond_0
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/w;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 150
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/w;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 152
    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/w;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 153
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/w;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 155
    :cond_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/w;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 156
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/w;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 158
    :cond_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/w;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 159
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/w;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 161
    :cond_4
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 162
    return-void
.end method
