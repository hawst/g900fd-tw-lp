.class public final Lcom/google/ac/c/a/a/a/ag;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile m:[Lcom/google/ac/c/a/a/a/ag;


# instance fields
.field public a:Ljava/lang/Integer;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/Boolean;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/Long;

.field public j:Lcom/google/ac/c/a/a/a/ad;

.field public k:Lcom/google/ac/c/a/a/a/ae;

.field public l:Lcom/google/ac/c/a/a/a/af;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 435
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 436
    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->a:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->d:Ljava/lang/Boolean;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->g:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->h:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->i:Ljava/lang/Long;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->j:Lcom/google/ac/c/a/a/a/ad;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->k:Lcom/google/ac/c/a/a/a/ae;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->l:Lcom/google/ac/c/a/a/a/af;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/c/a/a/a/ag;->cachedSize:I

    .line 437
    return-void
.end method

.method public static a()[Lcom/google/ac/c/a/a/a/ag;
    .locals 2

    .prologue
    .line 388
    sget-object v0, Lcom/google/ac/c/a/a/a/ag;->m:[Lcom/google/ac/c/a/a/a/ag;

    if-nez v0, :cond_1

    .line 389
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 391
    :try_start_0
    sget-object v0, Lcom/google/ac/c/a/a/a/ag;->m:[Lcom/google/ac/c/a/a/a/ag;

    if-nez v0, :cond_0

    .line 392
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/c/a/a/a/ag;

    sput-object v0, Lcom/google/ac/c/a/a/a/ag;->m:[Lcom/google/ac/c/a/a/a/ag;

    .line 394
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 396
    :cond_1
    sget-object v0, Lcom/google/ac/c/a/a/a/ag;->m:[Lcom/google/ac/c/a/a/a/ag;

    return-object v0

    .line 394
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    .line 501
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 502
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_0

    .line 503
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ag;->a:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 506
    :cond_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 507
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ag;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 510
    :cond_1
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 511
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ag;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 514
    :cond_2
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->d:Ljava/lang/Boolean;

    if-eqz v1, :cond_3

    .line 515
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ag;->d:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 518
    :cond_3
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    if-eqz v1, :cond_4

    .line 519
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 522
    :cond_4
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->f:Ljava/lang/String;

    if-eqz v1, :cond_5

    .line 523
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ag;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 526
    :cond_5
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->g:Ljava/lang/String;

    if-eqz v1, :cond_6

    .line 527
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ag;->g:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 530
    :cond_6
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->h:Ljava/lang/String;

    if-eqz v1, :cond_7

    .line 531
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ag;->h:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 534
    :cond_7
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->i:Ljava/lang/Long;

    if-eqz v1, :cond_8

    .line 535
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ag;->i:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 538
    :cond_8
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->j:Lcom/google/ac/c/a/a/a/ad;

    if-eqz v1, :cond_9

    .line 539
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ag;->j:Lcom/google/ac/c/a/a/a/ad;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 542
    :cond_9
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->k:Lcom/google/ac/c/a/a/a/ae;

    if-eqz v1, :cond_a

    .line 543
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ag;->k:Lcom/google/ac/c/a/a/a/ae;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 546
    :cond_a
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->l:Lcom/google/ac/c/a/a/a/af;

    if-eqz v1, :cond_b

    .line 547
    const/16 v1, 0xc

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ag;->l:Lcom/google/ac/c/a/a/a/af;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 550
    :cond_b
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 373
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/c/a/a/a/ag;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->a:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->d:Ljava/lang/Boolean;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->f:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->h:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->i:Ljava/lang/Long;

    goto :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->j:Lcom/google/ac/c/a/a/a/ad;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/ac/c/a/a/a/ad;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/ad;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->j:Lcom/google/ac/c/a/a/a/ad;

    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->j:Lcom/google/ac/c/a/a/a/ad;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_b
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->k:Lcom/google/ac/c/a/a/a/ae;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/ac/c/a/a/a/ae;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/ae;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->k:Lcom/google/ac/c/a/a/a/ae;

    :cond_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->k:Lcom/google/ac/c/a/a/a/ae;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_c
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->l:Lcom/google/ac/c/a/a/a/af;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/ac/c/a/a/a/af;

    invoke-direct {v0}, Lcom/google/ac/c/a/a/a/af;-><init>()V

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->l:Lcom/google/ac/c/a/a/a/af;

    :cond_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->l:Lcom/google/ac/c/a/a/a/af;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
        0x5a -> :sswitch_b
        0x62 -> :sswitch_c
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->a:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    .line 461
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 463
    :cond_0
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 464
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 466
    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 467
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 469
    :cond_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->d:Ljava/lang/Boolean;

    if-eqz v0, :cond_3

    .line 470
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->d:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 472
    :cond_3
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    .line 473
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 475
    :cond_4
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->f:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 476
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 478
    :cond_5
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 479
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 481
    :cond_6
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->h:Ljava/lang/String;

    if-eqz v0, :cond_7

    .line 482
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->h:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 484
    :cond_7
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->i:Ljava/lang/Long;

    if-eqz v0, :cond_8

    .line 485
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 487
    :cond_8
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->j:Lcom/google/ac/c/a/a/a/ad;

    if-eqz v0, :cond_9

    .line 488
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->j:Lcom/google/ac/c/a/a/a/ad;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 490
    :cond_9
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->k:Lcom/google/ac/c/a/a/a/ae;

    if-eqz v0, :cond_a

    .line 491
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->k:Lcom/google/ac/c/a/a/a/ae;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 493
    :cond_a
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ag;->l:Lcom/google/ac/c/a/a/a/af;

    if-eqz v0, :cond_b

    .line 494
    const/16 v0, 0xc

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ag;->l:Lcom/google/ac/c/a/a/a/af;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 496
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 497
    return-void
.end method
