.class public final Lcom/google/ac/c/a/a/a/d;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/ac/c/a/a/a/d;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 125
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 126
    iput-object v0, p0, Lcom/google/ac/c/a/a/a/d;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/d;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/d;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/d;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/d;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/c/a/a/a/d;->cachedSize:I

    .line 127
    return-void
.end method

.method public static a()[Lcom/google/ac/c/a/a/a/d;
    .locals 2

    .prologue
    .line 102
    sget-object v0, Lcom/google/ac/c/a/a/a/d;->e:[Lcom/google/ac/c/a/a/a/d;

    if-nez v0, :cond_1

    .line 103
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 105
    :try_start_0
    sget-object v0, Lcom/google/ac/c/a/a/a/d;->e:[Lcom/google/ac/c/a/a/a/d;

    if-nez v0, :cond_0

    .line 106
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/c/a/a/a/d;

    sput-object v0, Lcom/google/ac/c/a/a/a/d;->e:[Lcom/google/ac/c/a/a/a/d;

    .line 108
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 110
    :cond_1
    sget-object v0, Lcom/google/ac/c/a/a/a/d;->e:[Lcom/google/ac/c/a/a/a/d;

    return-object v0

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 159
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 160
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/d;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 161
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/d;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 164
    :cond_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/d;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 165
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/d;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 168
    :cond_1
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/d;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 169
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/d;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 172
    :cond_2
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/d;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 173
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/d;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 176
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 96
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/c/a/a/a/d;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/d;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/d;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/d;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/d;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/d;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 143
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/d;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/d;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 146
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/d;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 148
    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/d;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 149
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/d;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 151
    :cond_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/d;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 152
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/d;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 154
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 155
    return-void
.end method
