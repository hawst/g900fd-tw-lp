.class public final Lcom/google/ac/c/a/a/a/ap;
.super Lcom/google/protobuf/nano/d;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/ac/c/a/a/a/ap;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 284
    invoke-direct {p0}, Lcom/google/protobuf/nano/d;-><init>()V

    .line 285
    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ap;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ap;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ap;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ap;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ap;->unknownFieldData:Lcom/google/protobuf/nano/f;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/ac/c/a/a/a/ap;->cachedSize:I

    .line 286
    return-void
.end method

.method public static a()[Lcom/google/ac/c/a/a/a/ap;
    .locals 2

    .prologue
    .line 261
    sget-object v0, Lcom/google/ac/c/a/a/a/ap;->e:[Lcom/google/ac/c/a/a/a/ap;

    if-nez v0, :cond_1

    .line 262
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 264
    :try_start_0
    sget-object v0, Lcom/google/ac/c/a/a/a/ap;->e:[Lcom/google/ac/c/a/a/a/ap;

    if-nez v0, :cond_0

    .line 265
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/ac/c/a/a/a/ap;

    sput-object v0, Lcom/google/ac/c/a/a/a/ap;->e:[Lcom/google/ac/c/a/a/a/ap;

    .line 267
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    :cond_1
    sget-object v0, Lcom/google/ac/c/a/a/a/ap;->e:[Lcom/google/ac/c/a/a/a/ap;

    return-object v0

    .line 267
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 318
    invoke-super {p0}, Lcom/google/protobuf/nano/d;->computeSerializedSize()I

    move-result v0

    .line 319
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ap;->a:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 320
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ap;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 323
    :cond_0
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ap;->b:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 324
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ap;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 327
    :cond_1
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ap;->c:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 328
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ap;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 331
    :cond_2
    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ap;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 332
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/ac/c/a/a/a/ap;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 335
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 255
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p0, p1, v0}, Lcom/google/ac/c/a/a/a/ap;->storeUnknownField(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ap;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ap;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ap;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/ac/c/a/a/a/ap;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 301
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ap;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 302
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ap;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 304
    :cond_0
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ap;->b:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 305
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ap;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 307
    :cond_1
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ap;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 308
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ap;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 310
    :cond_2
    iget-object v0, p0, Lcom/google/ac/c/a/a/a/ap;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 311
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/ac/c/a/a/a/ap;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 313
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/d;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 314
    return-void
.end method
