.class public final Lcom/google/c/a/a/a/c/a/d/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:I

.field public final b:Ljava/lang/String;

.field private final c:Ljava/util/Map;


# direct methods
.method public constructor <init>(ILjava/util/Map;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 108
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 109
    iput p1, p0, Lcom/google/c/a/a/a/c/a/d/o;->a:I

    .line 110
    iput-object p2, p0, Lcom/google/c/a/a/a/c/a/d/o;->c:Ljava/util/Map;

    .line 111
    iput-object p3, p0, Lcom/google/c/a/a/a/c/a/d/o;->b:Ljava/lang/String;

    .line 112
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 148
    if-ne p0, p1, :cond_1

    .line 175
    :cond_0
    :goto_0
    return v0

    .line 151
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 152
    goto :goto_0

    .line 154
    :cond_2
    instance-of v2, p1, Lcom/google/c/a/a/a/c/a/d/o;

    if-nez v2, :cond_3

    move v0, v1

    .line 155
    goto :goto_0

    .line 157
    :cond_3
    check-cast p1, Lcom/google/c/a/a/a/c/a/d/o;

    .line 158
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/o;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 159
    iget-object v2, p1, Lcom/google/c/a/a/a/c/a/d/o;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 160
    goto :goto_0

    .line 162
    :cond_4
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/o;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/c/a/a/a/c/a/d/o;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 163
    goto :goto_0

    .line 165
    :cond_5
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/o;->c:Ljava/util/Map;

    if-nez v2, :cond_6

    .line 166
    iget-object v2, p1, Lcom/google/c/a/a/a/c/a/d/o;->c:Ljava/util/Map;

    if-eqz v2, :cond_7

    move v0, v1

    .line 167
    goto :goto_0

    .line 169
    :cond_6
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/o;->c:Ljava/util/Map;

    iget-object v3, p1, Lcom/google/c/a/a/a/c/a/d/o;->c:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 170
    goto :goto_0

    .line 172
    :cond_7
    iget v2, p0, Lcom/google/c/a/a/a/c/a/d/o;->a:I

    iget v3, p1, Lcom/google/c/a/a/a/c/a/d/o;->a:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 173
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 138
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/o;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 141
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/o;->c:Ljava/util/Map;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 142
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/c/a/a/a/c/a/d/o;->a:I

    add-int/2addr v0, v1

    .line 143
    return v0

    .line 138
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/o;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 141
    :cond_1
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/d/o;->c:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    goto :goto_1
.end method
