.class public final Lcom/google/c/a/a/b/b/a/b/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/Iterable;

.field public b:Z

.field public c:Ljava/util/List;

.field public d:Ljava/util/List;

.field public e:Z

.field public f:Ljava/util/Set;

.field public g:I

.field public h:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-boolean v1, p0, Lcom/google/c/a/a/b/b/a/b/u;->e:Z

    .line 37
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/u;->f:Ljava/util/Set;

    .line 38
    const/high16 v0, 0x3200000

    iput v0, p0, Lcom/google/c/a/a/b/b/a/b/u;->g:I

    .line 39
    iput-boolean v1, p0, Lcom/google/c/a/a/b/b/a/b/u;->h:Z

    .line 42
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/u;->a:Ljava/lang/Iterable;

    .line 43
    iput-boolean v1, p0, Lcom/google/c/a/a/b/b/a/b/u;->b:Z

    .line 44
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/c/a/a/b/b/a/b/t;
    .locals 8

    .prologue
    .line 117
    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/u;->a:Ljava/lang/Iterable;

    .line 118
    iget-boolean v3, p0, Lcom/google/c/a/a/b/b/a/b/u;->b:Z

    .line 119
    iget-object v4, p0, Lcom/google/c/a/a/b/b/a/b/u;->f:Ljava/util/Set;

    .line 120
    iget-boolean v5, p0, Lcom/google/c/a/a/b/b/a/b/u;->e:Z

    .line 121
    iget v6, p0, Lcom/google/c/a/a/b/b/a/b/u;->g:I

    .line 122
    iget-boolean v7, p0, Lcom/google/c/a/a/b/b/a/b/u;->h:Z

    .line 124
    new-instance v0, Lcom/google/c/a/a/b/b/a/b/v;

    move-object v1, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/c/a/a/b/b/a/b/v;-><init>(Lcom/google/c/a/a/b/b/a/b/u;Ljava/lang/Iterable;ZLjava/util/Set;ZIZ)V

    return-object v0
.end method
