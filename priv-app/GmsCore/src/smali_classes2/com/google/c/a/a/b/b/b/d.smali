.class final Lcom/google/c/a/a/b/b/b/d;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/b/s;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/c/a/a/b/b/b/y;Lcom/google/c/a/a/b/b/b/y;)V
    .locals 13

    .prologue
    const/4 v3, 0x0

    const/4 v12, 0x0

    .line 41
    invoke-interface {p1}, Lcom/google/c/a/a/b/b/b/y;->c()Lcom/google/c/b/a/c;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/c/a;

    .line 42
    invoke-interface {p2}, Lcom/google/c/a/a/b/b/b/y;->c()Lcom/google/c/b/a/c;

    move-result-object v1

    check-cast v1, Lcom/google/c/a/a/b/b/a/c/s;

    .line 45
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/a;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 104
    :goto_0
    return-void

    .line 52
    :cond_0
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->c()I

    move-result v4

    .line 53
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->d()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v5

    .line 54
    add-int v2, v4, v5

    add-int/lit8 v6, v2, -0x1

    .line 55
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/a;->c()I

    move-result v2

    .line 56
    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/a;->a()I

    move-result v7

    .line 57
    add-int v8, v2, v7

    add-int/lit8 v8, v8, -0x1

    .line 59
    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->d()Ljava/util/List;

    move-result-object v9

    .line 61
    if-le v2, v4, :cond_5

    .line 65
    sub-int v2, v6, v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v12, v2}, Ljava/lang/Math;->max(II)I

    move-result v10

    .line 67
    new-instance v2, Lcom/google/c/a/a/b/b/a/c/s;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v11

    sub-int v10, v5, v10

    invoke-interface {v9, v12, v10}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v10

    invoke-direct {v2, v11, v4, v10}, Lcom/google/c/a/a/b/b/a/c/s;-><init>(Ljava/lang/String;ILjava/util/List;)V

    .line 73
    :goto_1
    if-ge v8, v6, :cond_4

    .line 76
    sub-int v3, v8, v4

    add-int/lit8 v3, v3, 0x1

    invoke-static {v12, v3}, Ljava/lang/Math;->max(II)I

    move-result v6

    .line 77
    sub-int/2addr v7, v6

    .line 79
    new-instance v3, Lcom/google/c/a/a/b/b/a/c/s;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/s;->e()Ljava/lang/String;

    move-result-object v1

    sub-int/2addr v4, v7

    invoke-interface {v9, v6, v5}, Ljava/util/List;->subList(II)Ljava/util/List;

    move-result-object v5

    invoke-direct {v3, v1, v4, v5}, Lcom/google/c/a/a/b/b/a/c/s;-><init>(Ljava/lang/String;ILjava/util/List;)V

    move-object v1, v3

    .line 86
    :goto_2
    if-eqz v2, :cond_2

    if-eqz v1, :cond_2

    .line 87
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 88
    invoke-interface {v3, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 89
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 90
    new-instance v1, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-direct {v1}, Lcom/google/c/a/a/b/b/a/c/o;-><init>()V

    invoke-virtual {v1, v3}, Lcom/google/c/a/a/b/b/a/c/o;->a(Ljava/util/Collection;)Lcom/google/c/a/a/b/b/a/c/o;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/c/o;->a()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v1

    .line 99
    :cond_1
    :goto_3
    const-string v2, "Transformed set mutation cannot be null"

    invoke-static {v1, v2}, Lcom/google/c/a/a/b/h/c;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 102
    invoke-interface {p1, v0}, Lcom/google/c/a/a/b/b/b/y;->a(Lcom/google/c/b/a/c;)V

    .line 103
    invoke-interface {p2, v1}, Lcom/google/c/a/a/b/b/b/y;->a(Lcom/google/c/b/a/c;)V

    goto :goto_0

    .line 91
    :cond_2
    if-eqz v2, :cond_3

    move-object v1, v2

    .line 92
    goto :goto_3

    .line 93
    :cond_3
    if-nez v1, :cond_1

    .line 96
    invoke-static {}, Lcom/google/c/b/a/d;->a()Lcom/google/c/b/a/d;

    move-result-object v1

    goto :goto_3

    :cond_4
    move-object v1, v3

    goto :goto_2

    :cond_5
    move-object v2, v3

    goto :goto_1
.end method
