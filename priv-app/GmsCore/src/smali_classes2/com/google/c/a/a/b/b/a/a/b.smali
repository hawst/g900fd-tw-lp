.class public final Lcom/google/c/a/a/b/b/a/a/b;
.super Lcom/google/c/a/a/b/b/a/a/a;
.source "SourceFile"


# instance fields
.field public final g:Ljava/util/List;

.field public final h:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/b/a/g;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct/range {p0 .. p6}, Lcom/google/c/a/a/b/b/a/a/a;-><init>(Lcom/google/c/a/a/b/b/a/g;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;)V

    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/a/b;->g:Ljava/util/List;

    .line 26
    iput-object p7, p0, Lcom/google/c/a/a/b/b/a/a/b;->h:Ljava/util/List;

    .line 27
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 55
    instance-of v1, p1, Lcom/google/c/a/a/b/b/a/a/b;

    if-nez v1, :cond_0

    .line 59
    :goto_0
    return v0

    .line 58
    :cond_0
    check-cast p1, Lcom/google/c/a/a/b/b/a/a/b;

    .line 59
    const/16 v1, 0xc

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->a:Lcom/google/c/a/a/b/b/a/g;

    invoke-interface {v2}, Lcom/google/c/a/a/b/b/a/g;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/a/a;->a:Lcom/google/c/a/a/b/b/a/g;

    invoke-interface {v2}, Lcom/google/c/a/a/b/b/a/g;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/b;->g:Ljava/util/List;

    aput-object v2, v1, v0

    const/4 v0, 0x3

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/a/b;->g:Ljava/util/List;

    aput-object v2, v1, v0

    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->c:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x5

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/a/a;->c:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->d:Ljava/lang/String;

    aput-object v2, v1, v0

    const/4 v0, 0x7

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/a/a;->d:Ljava/lang/String;

    aput-object v2, v1, v0

    const/16 v0, 0x8

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->f:Ljava/util/List;

    aput-object v2, v1, v0

    const/16 v0, 0x9

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/a/a;->f:Ljava/util/List;

    aput-object v2, v1, v0

    const/16 v0, 0xa

    iget-boolean v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    const/16 v0, 0xb

    iget-boolean v2, p1, Lcom/google/c/a/a/b/b/a/a/a;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/google/c/a/a/b/h/b;->a([Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    .line 50
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->a:Lcom/google/c/a/a/b/b/a/g;

    invoke-interface {v2}, Lcom/google/c/a/a/b/b/a/g;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/b;->g:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->f:Ljava/util/List;

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-boolean v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
