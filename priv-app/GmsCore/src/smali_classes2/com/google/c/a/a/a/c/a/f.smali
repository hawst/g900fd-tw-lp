.class final Lcom/google/c/a/a/a/c/a/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/a/c/a/a/h;
.implements Lcom/google/c/a/a/a/c/a/a/i;
.implements Lcom/google/c/a/a/a/c/a/a/j;
.implements Lcom/google/c/a/a/a/c/a/a/k;
.implements Lcom/google/c/a/a/a/c/a/c/c;


# instance fields
.field final synthetic a:Lcom/google/c/a/a/a/c/a/a;


# direct methods
.method private constructor <init>(Lcom/google/c/a/a/a/c/a/a;)V
    .locals 0

    .prologue
    .line 274
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/c/a/a/a/c/a/a;B)V
    .locals 0

    .prologue
    .line 274
    invoke-direct {p0, p1}, Lcom/google/c/a/a/a/c/a/f;-><init>(Lcom/google/c/a/a/a/c/a/a;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;

    move-result-object v1

    monitor-enter v1

    .line 299
    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;

    move-result-object v0

    sget-object v2, Lcom/google/c/a/a/a/c/a/d;->c:Lcom/google/c/a/a/a/c/a/d;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;

    move-result-object v0

    sget-object v2, Lcom/google/c/a/a/a/c/a/d;->d:Lcom/google/c/a/a/a/c/a/d;

    if-ne v0, v2, :cond_1

    .line 300
    :cond_0
    invoke-static {}, Lcom/google/c/a/a/a/c/a/a;->c()Ljava/util/logging/Logger;

    move-result-object v0

    const-string v2, "Bind request is now connected."

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 301
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    sget-object v2, Lcom/google/c/a/a/a/c/a/d;->d:Lcom/google/c/a/a/a/c/a/d;

    invoke-static {v0, v2}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;Lcom/google/c/a/a/a/c/a/d;)V

    .line 303
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/a/f;)V
    .locals 7

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;

    move-result-object v1

    monitor-enter v1

    .line 309
    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;

    move-result-object v0

    sget-object v2, Lcom/google/c/a/a/a/c/a/d;->d:Lcom/google/c/a/a/a/c/a/d;

    if-ne v0, v2, :cond_0

    .line 310
    invoke-static {}, Lcom/google/c/a/a/a/c/a/a;->c()Ljava/util/logging/Logger;

    move-result-object v0

    const-string v2, "Bind request completed successfully."

    invoke-virtual {v0, v2}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 311
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->g(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/e/a;

    move-result-object v0

    sget-object v2, Lcom/google/c/a/a/a/c/a/e/d;->a:Lcom/google/c/a/a/a/c/a/e/d;

    new-instance v3, Lcom/google/c/a/a/a/c/a/b;

    iget-object v4, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    iget-object v5, p1, Lcom/google/c/a/a/a/c/a/a/f;->a:Lcom/google/c/a/a/a/c/a/a/n;

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/google/c/a/a/a/c/a/b;-><init>(Lcom/google/c/a/a/a/c/a/a;Lcom/google/c/a/a/a/c/a/a/n;B)V

    invoke-interface {v0, v2, v3}, Lcom/google/c/a/a/a/c/a/e/a;->a(Lcom/google/c/a/a/a/c/a/e/d;Ljava/lang/Runnable;)V

    .line 314
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/a/g;)V
    .locals 7

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;

    move-result-object v1

    monitor-enter v1

    .line 320
    :try_start_0
    invoke-static {}, Lcom/google/c/a/a/a/c/a/a;->c()Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Error in bind request."

    iget-object v4, p1, Lcom/google/c/a/a/a/c/a/a/g;->b:Ljava/lang/Exception;

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 321
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/a/g;->a:Lcom/google/c/a/a/a/c/a/e/e;

    sget-object v2, Lcom/google/c/a/a/a/c/a/e/e;->a:Lcom/google/c/a/a/a/c/a/e/e;

    if-eq v0, v2, :cond_0

    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/a/g;->a:Lcom/google/c/a/a/a/c/a/e/e;

    sget-object v2, Lcom/google/c/a/a/a/c/a/e/e;->c:Lcom/google/c/a/a/a/c/a/e/e;

    if-ne v0, v2, :cond_3

    .line 323
    :cond_0
    iget-object v0, p1, Lcom/google/c/a/a/a/c/a/a/g;->a:Lcom/google/c/a/a/a/c/a/e/e;

    sget-object v2, Lcom/google/c/a/a/a/c/a/e/e;->c:Lcom/google/c/a/a/a/c/a/e/e;

    if-ne v0, v2, :cond_2

    sget-object v0, Lcom/google/c/a/a/a/c/a/e/d;->c:Lcom/google/c/a/a/a/c/a/e/d;

    .line 326
    :goto_0
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v2}, Lcom/google/c/a/a/a/c/a/a;->g(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/e/a;

    move-result-object v2

    new-instance v3, Lcom/google/c/a/a/a/c/a/b;

    iget-object v4, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    iget-object v5, p1, Lcom/google/c/a/a/a/c/a/a/g;->d:Lcom/google/c/a/a/a/c/a/a/n;

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/google/c/a/a/a/c/a/b;-><init>(Lcom/google/c/a/a/a/c/a/a;Lcom/google/c/a/a/a/c/a/a/n;B)V

    invoke-interface {v2, v0, v3}, Lcom/google/c/a/a/a/c/a/e/a;->a(Lcom/google/c/a/a/a/c/a/e/d;Ljava/lang/Runnable;)V

    .line 330
    :goto_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 331
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->i(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 332
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->i(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/c;

    move-result-object v0

    iget-object v1, p1, Lcom/google/c/a/a/a/c/a/a/g;->a:Lcom/google/c/a/a/a/c/a/e/e;

    iget v2, p1, Lcom/google/c/a/a/a/c/a/a/g;->c:I

    invoke-interface {v0, v1, v2}, Lcom/google/c/a/a/a/c/a/c;->a(Lcom/google/c/a/a/a/c/a/e/e;I)V

    .line 334
    :cond_1
    return-void

    .line 323
    :cond_2
    :try_start_1
    sget-object v0, Lcom/google/c/a/a/a/c/a/e/d;->b:Lcom/google/c/a/a/a/c/a/e/d;

    goto :goto_0

    .line 328
    :cond_3
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-virtual {v0}, Lcom/google/c/a/a/a/c/a/a;->b()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 330
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/a/l;)V
    .locals 2

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;

    move-result-object v0

    sget-object v1, Lcom/google/c/a/a/a/c/a/d;->e:Lcom/google/c/a/a/a/c/a/d;

    if-eq v0, v1, :cond_0

    .line 339
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->j(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/a/k;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 340
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->j(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/a/k;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/c/a/a/a/c/a/a/k;->a(Lcom/google/c/a/a/a/c/a/a/l;)V

    .line 343
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/c/d;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 278
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v1}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;

    move-result-object v1

    monitor-enter v1

    .line 279
    :try_start_0
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v2}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/d;

    move-result-object v2

    sget-object v3, Lcom/google/c/a/a/a/c/a/d;->b:Lcom/google/c/a/a/a/c/a/d;

    if-ne v2, v3, :cond_1

    .line 280
    iget-object v2, p1, Lcom/google/c/a/a/a/c/a/c/d;->a:Lcom/google/c/a/a/a/c/a/c/e;

    invoke-virtual {v2}, Lcom/google/c/a/a/a/c/a/c/e;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 281
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    sget-object v3, Lcom/google/c/a/a/a/c/a/d;->c:Lcom/google/c/a/a/a/c/a/d;

    invoke-static {v2, v3}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;Lcom/google/c/a/a/a/c/a/d;)V

    .line 282
    invoke-static {}, Lcom/google/c/a/a/a/c/a/a;->c()Ljava/util/logging/Logger;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Connection test successful. Result: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p1, Lcom/google/c/a/a/a/c/a/c/d;->a:Lcom/google/c/a/a/a/c/a/c/e;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 283
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    iget-object v3, p1, Lcom/google/c/a/a/a/c/a/c/d;->a:Lcom/google/c/a/a/a/c/a/c/e;

    sget-object v4, Lcom/google/c/a/a/a/c/a/c/e;->a:Lcom/google/c/a/a/a/c/a/c/e;

    if-ne v3, v4, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-static {v2, v0}, Lcom/google/c/a/a/a/c/a/a;->a(Lcom/google/c/a/a/a/c/a/a;Z)Z

    .line 284
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->g(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/e/a;

    move-result-object v0

    sget-object v2, Lcom/google/c/a/a/a/c/a/e/d;->a:Lcom/google/c/a/a/a/c/a/e/d;

    new-instance v3, Lcom/google/c/a/a/a/c/a/b;

    iget-object v4, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-direct {v3, v4, v5, v6}, Lcom/google/c/a/a/a/c/a/b;-><init>(Lcom/google/c/a/a/a/c/a/a;Lcom/google/c/a/a/a/c/a/a/n;B)V

    invoke-interface {v0, v2, v3}, Lcom/google/c/a/a/a/c/a/e/a;->a(Lcom/google/c/a/a/a/c/a/e/d;Ljava/lang/Runnable;)V

    .line 293
    :cond_1
    :goto_0
    monitor-exit v1

    return-void

    .line 286
    :cond_2
    invoke-static {}, Lcom/google/c/a/a/a/c/a/a;->c()Ljava/util/logging/Logger;

    move-result-object v0

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Connection test failed."

    iget-object v4, p1, Lcom/google/c/a/a/a/c/a/c/d;->b:Ljava/lang/Exception;

    invoke-virtual {v0, v2, v3, v4}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 287
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->g(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/e/a;

    move-result-object v0

    sget-object v2, Lcom/google/c/a/a/a/c/a/e/d;->b:Lcom/google/c/a/a/a/c/a/e/d;

    iget-object v3, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v3}, Lcom/google/c/a/a/a/c/a/a;->h(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/h;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Lcom/google/c/a/a/a/c/a/e/a;->a(Lcom/google/c/a/a/a/c/a/e/d;Ljava/lang/Runnable;)V

    .line 288
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->i(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/c;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 289
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/f;->a:Lcom/google/c/a/a/a/c/a/a;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/a;->i(Lcom/google/c/a/a/a/c/a/a;)Lcom/google/c/a/a/a/c/a/c;

    move-result-object v0

    iget-object v2, p1, Lcom/google/c/a/a/a/c/a/c/d;->c:Lcom/google/c/a/a/a/c/a/e/e;

    iget v3, p1, Lcom/google/c/a/a/a/c/a/c/d;->d:I

    invoke-interface {v0, v2, v3}, Lcom/google/c/a/a/a/c/a/c;->a(Lcom/google/c/a/a/a/c/a/e/e;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
