.class final Lcom/google/c/a/a/b/b/a/b/j;
.super Lcom/google/c/a/a/b/b/a/b/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/b/a/h;


# instance fields
.field private final b:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/b/a/b/w;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Lcom/google/c/a/a/b/b/a/b/a;-><init>(Lcom/google/c/a/a/b/b/a/b/w;Ljava/lang/String;)V

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    .line 49
    return-void
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 206
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 207
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/r;

    .line 208
    instance-of v3, v0, Lcom/google/c/a/a/b/b/a/c;

    if-eqz v3, :cond_0

    .line 209
    check-cast v0, Lcom/google/c/a/a/b/b/a/c;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c;->a()C

    move-result v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 211
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "values must only contain CharValue instances"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 213
    :cond_1
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/CharSequence;)Ljava/util/List;
    .locals 3

    .prologue
    .line 218
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 219
    const/4 v0, 0x0

    :goto_0
    invoke-interface {p0}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 220
    invoke-interface {p0, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v2

    invoke-static {v2}, Lcom/google/c/a/a/b/b/a/c;->a(C)Lcom/google/c/a/a/b/b/a/c;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 219
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 222
    :cond_0
    return-object v1
.end method

.method private a(Lcom/google/c/a/a/b/b/a/c/a;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V
    .locals 10

    .prologue
    .line 132
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/a;->c()I

    move-result v7

    .line 133
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/a;->a()I

    move-result v0

    add-int v9, v7, v0

    .line 135
    if-eqz p3, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7, v9}, Ljava/lang/StringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    .line 137
    new-instance v0, Lcom/google/c/a/a/b/b/a/a/e;

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->d()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->a()Z

    move-result v5

    move-object v1, p0

    move-object v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/c/a/a/b/b/a/a/e;-><init>(Lcom/google/c/a/a/b/b/a/g;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;ILjava/lang/String;)V

    .line 140
    invoke-virtual {p4, v0}, Lcom/google/c/a/a/b/b/a/b/y;->a(Lcom/google/c/a/a/b/b/a/a/a;)V

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0, v7, v9}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    .line 143
    return-void
.end method

.method private a(Lcom/google/c/a/a/b/b/a/c/b;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V
    .locals 9

    .prologue
    .line 147
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/b;->c()I

    move-result v0

    .line 148
    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/b;->d()Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/google/c/a/a/b/b/a/b/j;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v8

    .line 149
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v1, v0, v8}, Ljava/lang/StringBuilder;->insert(ILjava/lang/String;)Ljava/lang/StringBuilder;

    .line 151
    if-eqz p3, :cond_0

    .line 152
    new-instance v0, Lcom/google/c/a/a/b/b/a/a/f;

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->d()Ljava/util/List;

    move-result-object v4

    invoke-virtual {p2}, Lcom/google/c/a/a/b/b/a/o;->a()Z

    move-result v5

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/b;->c()I

    move-result v7

    move-object v1, p0

    move-object v6, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/c/a/a/b/b/a/a/f;-><init>(Lcom/google/c/a/a/b/b/a/g;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;ILjava/lang/String;)V

    .line 155
    invoke-virtual {p4, v0}, Lcom/google/c/a/a/b/b/a/b/y;->a(Lcom/google/c/a/a/b/b/a/a/a;)V

    .line 157
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/i;)I
    .locals 1

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final a(II)Lcom/google/c/a/a/b/b/a/a;
    .locals 3

    .prologue
    .line 71
    new-instance v0, Lcom/google/c/a/a/b/b/a/c/f;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/j;->d()Ljava/lang/String;

    move-result-object v1

    sub-int v2, p2, p1

    invoke-direct {v0, v1, p1, v2}, Lcom/google/c/a/a/b/b/a/c/f;-><init>(Ljava/lang/String;II)V

    .line 72
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/j;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v1

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/j;->d_:Lcom/google/c/a/a/b/b/a/b/w;

    iget-object v2, v2, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-static {v2, v0}, Lcom/google/c/a/a/b/b/a/b/s;->a(Lcom/google/c/a/a/b/b/a/b/ab;Lcom/google/c/b/a/c;)Lcom/google/c/b/a/c;

    move-result-object v0

    sget-object v2, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-interface {v1, v0, v2}, Lcom/google/c/a/a/b/b/a/b;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(ILjava/lang/String;)Lcom/google/c/a/a/b/b/a/a;
    .locals 4

    .prologue
    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 64
    new-instance v1, Lcom/google/c/a/a/b/b/a/c/i;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/j;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {p2}, Lcom/google/c/a/a/b/b/a/b/j;->a(Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3}, Lcom/google/c/a/a/b/b/a/c/i;-><init>(Ljava/lang/String;ILjava/util/List;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/j;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v1

    new-instance v2, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-direct {v2}, Lcom/google/c/a/a/b/b/a/c/o;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/c/a/a/b/b/a/c/o;->a(Ljava/util/Collection;)Lcom/google/c/a/a/b/b/a/c/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/o;->a()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v0

    sget-object v2, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-interface {v1, v0, v2}, Lcom/google/c/a/a/b/b/a/b;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/c/a/a/b/b/a/a;
    .locals 4

    .prologue
    .line 79
    new-instance v0, Lcom/google/c/a/a/b/h/a/f;

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Lcom/google/c/a/a/b/h/a/f;-><init>(Ljava/lang/StringBuilder;)V

    .line 80
    new-instance v1, Lcom/google/c/a/a/b/h/a/g;

    invoke-direct {v1, p1}, Lcom/google/c/a/a/b/h/a/g;-><init>(Ljava/lang/String;)V

    .line 81
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/j;->d()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/c/a/a/b/h/a/c;->a(Lcom/google/c/a/a/b/h/a/h;Lcom/google/c/a/a/b/h/a/h;Ljava/lang/String;)Lcom/google/c/a/a/b/h/a/d;

    move-result-object v0

    iget-object v0, v0, Lcom/google/c/a/a/b/h/a/d;->a:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    .line 83
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 84
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/j;->d_:Lcom/google/c/a/a/b/b/a/b/w;

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/j;->d_:Lcom/google/c/a/a/b/b/a/b/w;

    iget-object v2, v2, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    const/4 v3, 0x0

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    invoke-static {v2, v0}, Lcom/google/c/a/a/b/b/a/b/s;->a(Lcom/google/c/a/a/b/b/a/b/ab;Lcom/google/c/b/a/c;)Lcom/google/c/b/a/c;

    move-result-object v0

    sget-object v2, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-virtual {v1, v0, v2}, Lcom/google/c/a/a/b/b/a/b/w;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    .line 88
    :cond_0
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/j;->d_:Lcom/google/c/a/a/b/b/a/b/w;

    const-string v2, ""

    sget-object v3, Lcom/google/c/a/a/b/b/a/b/z;->b:Lcom/google/c/a/a/b/b/a/b/z;

    invoke-virtual {v1, v2, v3}, Lcom/google/c/a/a/b/b/a/b/w;->a(Ljava/lang/String;Lcom/google/c/a/a/b/b/a/b/z;)V

    .line 89
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    .line 90
    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/b/j;->d_:Lcom/google/c/a/a/b/b/a/b/w;

    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/b/j;->d_:Lcom/google/c/a/a/b/b/a/b/w;

    iget-object v3, v3, Lcom/google/c/a/a/b/b/a/b/w;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    invoke-static {v3, v0}, Lcom/google/c/a/a/b/b/a/b/s;->a(Lcom/google/c/a/a/b/b/a/b/ab;Lcom/google/c/b/a/c;)Lcom/google/c/b/a/c;

    move-result-object v0

    sget-object v3, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-virtual {v2, v0, v3}, Lcom/google/c/a/a/b/b/a/b/w;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    goto :goto_1

    .line 94
    :cond_1
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/j;->j()Lcom/google/c/a/a/b/b/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/c/a/a/b/b/a/b;->c()Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/c/r;)Lcom/google/c/a/a/b/b/a/c/r;
    .locals 6

    .prologue
    .line 161
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/f;

    if-eqz v0, :cond_0

    .line 162
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/f;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/f;->c()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/f;->c()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/f;->a()I

    move-result v2

    add-int/2addr v2, v0

    new-instance v0, Lcom/google/c/a/a/b/b/a/c/j;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/f;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/f;->c()I

    move-result v4

    iget-object v5, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Lcom/google/c/a/a/b/b/a/b/j;->a(Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v3, v4, v1}, Lcom/google/c/a/a/b/b/a/c/j;-><init>(Ljava/lang/String;ILjava/util/List;)V

    .line 168
    :goto_0
    return-object v0

    .line 163
    :cond_0
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/i;

    if-eqz v0, :cond_1

    .line 164
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/i;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/i;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/c/a/a/b/b/a/b/j;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lcom/google/c/a/a/b/b/a/c/k;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/i;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/i;->c()I

    move-result v3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v2, v3, v1}, Lcom/google/c/a/a/b/b/a/c/k;-><init>(Ljava/lang/String;II)V

    goto :goto_0

    .line 165
    :cond_1
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/j;

    if-eqz v0, :cond_2

    .line 166
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/j;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/j;->d()Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Lcom/google/c/a/a/b/b/a/b/j;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lcom/google/c/a/a/b/b/a/c/f;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/j;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/j;->c()I

    move-result v3

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    invoke-direct {v0, v2, v3, v1}, Lcom/google/c/a/a/b/b/a/c/f;-><init>(Ljava/lang/String;II)V

    goto :goto_0

    .line 167
    :cond_2
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/k;

    if-eqz v0, :cond_3

    .line 168
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/k;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/k;->c()I

    move-result v1

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/k;->c()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/k;->a()I

    move-result v2

    add-int/2addr v2, v0

    new-instance v0, Lcom/google/c/a/a/b/b/a/c/i;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/k;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/k;->c()I

    move-result v4

    iget-object v5, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v5, v1, v2}, Ljava/lang/StringBuilder;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Lcom/google/c/a/a/b/b/a/b/j;->a(Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v3, v4, v1}, Lcom/google/c/a/a/b/b/a/c/i;-><init>(Ljava/lang/String;ILjava/util/List;)V

    goto :goto_0

    .line 170
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only insert and delete mutations can be applied to strings."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/c/p;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V
    .locals 6

    .prologue
    .line 114
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/f;

    if-eqz v0, :cond_0

    move-object v1, p1

    .line 115
    check-cast v1, Lcom/google/c/a/a/b/b/a/c/f;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/b/j;->a(Lcom/google/c/a/a/b/b/a/c/a;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    .line 122
    :goto_0
    return-void

    .line 116
    :cond_0
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/j;

    if-eqz v0, :cond_1

    move-object v1, p1

    .line 117
    check-cast v1, Lcom/google/c/a/a/b/b/a/c/j;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/b/j;->a(Lcom/google/c/a/a/b/b/a/c/b;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    goto :goto_0

    .line 119
    :cond_1
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/i;

    if-eqz v0, :cond_2

    move-object v1, p1

    .line 120
    check-cast v1, Lcom/google/c/a/a/b/b/a/c/i;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/b/j;->a(Lcom/google/c/a/a/b/b/a/c/b;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    goto :goto_0

    .line 121
    :cond_2
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/k;

    if-eqz v0, :cond_3

    move-object v1, p1

    .line 122
    check-cast v1, Lcom/google/c/a/a/b/b/a/c/k;

    move-object v0, p0

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/c/a/a/b/b/a/b/j;->a(Lcom/google/c/a/a/b/b/a/c/a;Lcom/google/c/a/a/b/b/a/o;ZLcom/google/c/a/a/b/b/a/b/y;Lcom/google/c/a/a/b/b/a/k;)V

    goto :goto_0

    .line 125
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Only insert and delete mutations can be applied to strings."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final a(Lcom/google/c/a/a/b/b/a/g;)Z
    .locals 2

    .prologue
    .line 234
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/h;

    if-nez v0, :cond_0

    .line 235
    const/4 v0, 0x0

    .line 238
    :goto_0
    return v0

    .line 237
    :cond_0
    check-cast p1, Lcom/google/c/a/a/b/b/a/h;

    .line 238
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final a(Lcom/google/c/a/a/b/b/a/j;Lcom/google/c/a/a/b/b/a/g;)Z
    .locals 1

    .prologue
    .line 242
    invoke-virtual {p0, p2}, Lcom/google/c/a/a/b/b/a/b/j;->a(Lcom/google/c/a/a/b/b/a/g;)Z

    move-result v0

    return v0
.end method

.method protected final aA_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final az_()I
    .locals 1

    .prologue
    .line 246
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/c/a/a/b/b/a/i;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 271
    sget-object v0, Lcom/google/c/a/a/b/b/a/b/c;->c:Lcom/google/c/a/a/b/b/a/b/c;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/c;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final i()Lcom/google/c/a/a/b/b/a/c/n;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 100
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 101
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 102
    new-instance v1, Lcom/google/c/a/a/b/b/a/c/f;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/j;->d()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    invoke-direct {v1, v2, v4, v3}, Lcom/google/c/a/a/b/b/a/c/f;-><init>(Ljava/lang/String;II)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 105
    :cond_0
    new-instance v1, Lcom/google/c/a/a/b/b/a/c/o;

    invoke-direct {v1}, Lcom/google/c/a/a/b/b/a/c/o;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/c/a/a/b/b/a/c/o;->a(Ljava/util/Collection;)Lcom/google/c/a/a/b/b/a/c/o;

    move-result-object v0

    iput-boolean v4, v0, Lcom/google/c/a/a/b/b/a/c/o;->d:Z

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/c/o;->a()Lcom/google/c/a/a/b/b/a/c/n;

    move-result-object v0

    return-object v0
.end method

.method public final k()Ljava/util/List;
    .locals 5

    .prologue
    .line 226
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 227
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-lez v1, :cond_0

    .line 228
    new-instance v1, Lcom/google/c/a/a/b/b/a/c/i;

    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/a/b/j;->d()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-static {v4}, Lcom/google/c/a/a/b/b/a/b/j;->a(Ljava/lang/CharSequence;)Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/c/a/a/b/b/a/c/i;-><init>(Ljava/lang/String;ILjava/util/List;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 230
    :cond_0
    return-object v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 266
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/j;->b:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x10

    return v0
.end method

.method public final n()Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 276
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
