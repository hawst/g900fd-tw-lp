.class final Lcom/google/c/a/a/b/b/b/x;
.super Lcom/google/c/a/a/b/b/b/v;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/List;

.field private b:Ljava/util/ListIterator;

.field private c:Lcom/google/c/b/a/c;


# direct methods
.method public constructor <init>(Ljava/util/List;)V
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/c/a/a/b/b/b/v;-><init>(B)V

    .line 246
    iput-object p1, p0, Lcom/google/c/a/a/b/b/b/x;->a:Ljava/util/List;

    .line 247
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/b/x;->b()V

    .line 248
    return-void
.end method


# virtual methods
.method final a()V
    .locals 1

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/c/a/a/b/b/b/x;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 265
    iget-object v0, p0, Lcom/google/c/a/a/b/b/b/x;->b:Ljava/util/ListIterator;

    invoke-interface {v0}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    iput-object v0, p0, Lcom/google/c/a/a/b/b/b/x;->c:Lcom/google/c/b/a/c;

    .line 269
    :goto_0
    return-void

    .line 267
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/b/x;->c:Lcom/google/c/b/a/c;

    goto :goto_0
.end method

.method public final a(Lcom/google/c/b/a/c;)V
    .locals 2

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/c/a/a/b/b/b/x;->c:Lcom/google/c/b/a/c;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "No current command."

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    .line 258
    iput-object p1, p0, Lcom/google/c/a/a/b/b/b/x;->c:Lcom/google/c/b/a/c;

    .line 259
    iget-object v0, p0, Lcom/google/c/a/a/b/b/b/x;->b:Ljava/util/ListIterator;

    invoke-interface {v0, p1}, Ljava/util/ListIterator;->set(Ljava/lang/Object;)V

    .line 260
    return-void

    .line 257
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b()V
    .locals 1

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/c/a/a/b/b/b/x;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->listIterator()Ljava/util/ListIterator;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/b/x;->b:Ljava/util/ListIterator;

    .line 274
    invoke-virtual {p0}, Lcom/google/c/a/a/b/b/b/x;->a()V

    .line 275
    return-void
.end method

.method public final c()Lcom/google/c/b/a/c;
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/google/c/a/a/b/b/b/x;->c:Lcom/google/c/b/a/c;

    return-object v0
.end method
