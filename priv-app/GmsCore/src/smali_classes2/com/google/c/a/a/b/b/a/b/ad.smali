.class public final Lcom/google/c/a/a/b/b/a/b/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/c/a/a/b/b/a/b;

.field final b:Ljava/util/List;

.field final c:Ljava/util/List;

.field d:Z

.field e:Z

.field f:Z

.field g:Lcom/google/c/b/a/c;

.field h:Lcom/google/c/b/a/c;

.field i:I

.field j:I

.field k:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/b/a/b;Ljava/util/List;Ljava/util/List;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ad;->b:Ljava/util/List;

    .line 35
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ad;->c:Ljava/util/List;

    .line 36
    iput-boolean v1, p0, Lcom/google/c/a/a/b/b/a/b/ad;->d:Z

    .line 37
    iput-boolean v1, p0, Lcom/google/c/a/a/b/b/a/b/ad;->e:Z

    .line 38
    iput-boolean v1, p0, Lcom/google/c/a/a/b/b/a/b/ad;->f:Z

    .line 41
    iput v1, p0, Lcom/google/c/a/a/b/b/a/b/ad;->i:I

    .line 42
    iput v1, p0, Lcom/google/c/a/a/b/b/a/b/ad;->j:I

    .line 43
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ad;->k:Ljava/util/List;

    .line 47
    iput-object p1, p0, Lcom/google/c/a/a/b/b/a/b/ad;->a:Lcom/google/c/a/a/b/b/a/b;

    .line 48
    if-eqz p2, :cond_0

    .line 49
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ad;->b:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 51
    :cond_0
    if-eqz p3, :cond_1

    .line 52
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ad;->c:Ljava/util/List;

    invoke-interface {v0, p3}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 54
    :cond_1
    return-void
.end method

.method private static a(Ljava/util/List;)Z
    .locals 2

    .prologue
    .line 238
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    .line 239
    instance-of v0, v0, Lcom/google/c/a/a/b/b/a/b/k;

    if-nez v0, :cond_0

    .line 240
    const/4 v0, 0x1

    .line 243
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method final a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ad;->a:Lcom/google/c/a/a/b/b/a/b;

    invoke-interface {v0, p1, p2}, Lcom/google/c/a/a/b/b/a/b;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v0

    return-object v0
.end method

.method final a(Ljava/util/List;I)Lcom/google/c/b/a/c;
    .locals 9

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 192
    .line 195
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v2, v3

    move-object v1, v4

    .line 196
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 197
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    .line 198
    invoke-interface {v6}, Ljava/util/Iterator;->remove()V

    .line 199
    add-int/lit8 v2, v2, 0x1

    .line 200
    instance-of v7, v0, Lcom/google/c/a/a/b/b/a/b/k;

    if-eqz v7, :cond_1

    .line 201
    if-nez v1, :cond_0

    .line 202
    new-instance v1, Ljava/util/LinkedList;

    invoke-direct {v1}, Ljava/util/LinkedList;-><init>()V

    .line 206
    :cond_0
    check-cast v0, Lcom/google/c/a/a/b/b/a/b/k;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/b/a/b/k;->a()Lcom/google/c/a/a/b/b/a/c/p;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    goto :goto_0

    :cond_1
    move v8, v2

    move-object v2, v0

    move v0, v8

    .line 210
    :goto_1
    if-nez p2, :cond_5

    .line 215
    iput v0, p0, Lcom/google/c/a/a/b/b/a/b/ad;->i:I

    .line 219
    :cond_2
    :goto_2
    invoke-static {p1}, Lcom/google/c/a/a/b/b/a/b/ad;->a(Ljava/util/List;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 221
    invoke-interface {p1}, Ljava/util/List;->clear()V

    .line 223
    :cond_3
    if-eqz v2, :cond_6

    move v0, v5

    :goto_3
    const-string v4, "Null undo mutation."

    invoke-static {v0, v4}, Lcom/google/c/a/a/b/h/c;->b(ZLjava/lang/String;)V

    .line 224
    if-eqz v1, :cond_4

    .line 225
    invoke-static {v2, v1}, Lcom/google/c/a/a/b/b/b/u;->a(Lcom/google/c/b/a/c;Ljava/util/List;)Lcom/google/c/b/a/c;

    move-result-object v2

    .line 226
    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 227
    const-string v0, ""

    invoke-static {v1, v0}, Lcom/google/c/a/a/b/b/a/c/q;->a(Ljava/util/List;Ljava/lang/String;)Lcom/google/c/b/a/c;

    move-result-object v0

    .line 228
    instance-of v1, v0, Lcom/google/c/a/a/b/b/a/c/p;

    if-eqz v1, :cond_4

    .line 229
    new-instance v1, Lcom/google/c/a/a/b/b/a/b/k;

    check-cast v0, Lcom/google/c/a/a/b/b/a/c/p;

    invoke-direct {v1, v0}, Lcom/google/c/a/a/b/b/a/b/k;-><init>(Lcom/google/c/a/a/b/b/a/c/p;)V

    .line 230
    invoke-interface {p1, v3, v1}, Ljava/util/List;->add(ILjava/lang/Object;)V

    :cond_4
    move-object v0, v2

    .line 234
    return-object v0

    .line 216
    :cond_5
    if-ne p2, v5, :cond_2

    .line 217
    iput v0, p0, Lcom/google/c/a/a/b/b/a/b/ad;->j:I

    goto :goto_2

    :cond_6
    move v0, v3

    .line 223
    goto :goto_3

    :cond_7
    move v0, v2

    move-object v2, v4

    goto :goto_1
.end method

.method public final a()Z
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ad;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/ad;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
