.class final Lcom/google/c/a/a/a/c/a/d/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Ljava/nio/CharBuffer;

.field b:Ljava/io/BufferedReader;

.field final synthetic c:Lcom/google/c/a/a/a/c/a/d/s;


# direct methods
.method private constructor <init>(Lcom/google/c/a/a/a/c/a/d/s;)V
    .locals 1

    .prologue
    .line 208
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/d/v;->c:Lcom/google/c/a/a/a/c/a/d/s;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 209
    const/16 v0, 0x7d0

    invoke-static {v0}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->a:Ljava/nio/CharBuffer;

    .line 210
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->b:Ljava/io/BufferedReader;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/c/a/a/a/c/a/d/s;B)V
    .locals 0

    .prologue
    .line 208
    invoke-direct {p0, p1}, Lcom/google/c/a/a/a/c/a/d/v;-><init>(Lcom/google/c/a/a/a/c/a/d/s;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 215
    :try_start_0
    new-instance v0, Ljava/io/BufferedReader;

    new-instance v1, Ljava/io/InputStreamReader;

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/v;->c:Lcom/google/c/a/a/a/c/a/d/s;

    invoke-static {v2}, Lcom/google/c/a/a/a/c/a/d/s;->c(Lcom/google/c/a/a/a/c/a/d/s;)Ljava/net/HttpURLConnection;

    move-result-object v2

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {}, Lcom/google/c/a/a/a/c/a/d/s;->f()Ljava/nio/charset/Charset;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v0, v1}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->b:Ljava/io/BufferedReader;

    .line 216
    :cond_0
    :goto_0
    invoke-static {}, Ljava/lang/Thread;->interrupted()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->c:Lcom/google/c/a/a/a/c/a/d/s;

    iget-object v0, v0, Lcom/google/c/a/a/a/c/a/d/s;->a:Lcom/google/c/a/a/a/c/a/d/c;

    sget-object v1, Lcom/google/c/a/a/a/c/a/d/c;->d:Lcom/google/c/a/a/a/c/a/d/c;

    if-ne v0, v1, :cond_6

    .line 217
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->b:Ljava/io/BufferedReader;

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/d/v;->a:Ljava/nio/CharBuffer;

    invoke-virtual {v0, v1}, Ljava/io/BufferedReader;->read(Ljava/nio/CharBuffer;)I

    move-result v0

    .line 218
    if-lez v0, :cond_4

    .line 219
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->a:Ljava/nio/CharBuffer;

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->flip()Ljava/nio/Buffer;

    .line 220
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->c:Lcom/google/c/a/a/a/c/a/d/s;

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/d/v;->a:Ljava/nio/CharBuffer;

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/v;->a:Ljava/nio/CharBuffer;

    invoke-virtual {v2}, Ljava/nio/CharBuffer;->position()I

    move-result v2

    iget-object v3, p0, Lcom/google/c/a/a/a/c/a/d/v;->a:Ljava/nio/CharBuffer;

    invoke-virtual {v3}, Ljava/nio/CharBuffer;->limit()I

    move-result v3

    iget-object v4, v0, Lcom/google/c/a/a/a/c/a/d/a;->b:Lcom/google/c/a/a/a/c/a/d/g;

    if-eqz v4, :cond_1

    iget-object v0, v0, Lcom/google/c/a/a/a/c/a/d/a;->b:Lcom/google/c/a/a/a/c/a/d/g;

    const/4 v4, 0x0

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/c/a/a/a/c/a/d/g;->a(Ljava/lang/CharSequence;IIZ)V

    .line 221
    :cond_1
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->a:Ljava/nio/CharBuffer;

    invoke-virtual {v0}, Ljava/nio/CharBuffer;->clear()Ljava/nio/Buffer;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 227
    :catch_0
    move-exception v0

    .line 228
    :try_start_1
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/d/v;->c:Lcom/google/c/a/a/a/c/a/d/s;

    invoke-static {v1, v0}, Lcom/google/c/a/a/a/c/a/d/s;->a(Lcom/google/c/a/a/a/c/a/d/s;Ljava/io/IOException;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 231
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->b:Ljava/io/BufferedReader;

    if-eqz v0, :cond_2

    .line 233
    :try_start_2
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->b:Ljava/io/BufferedReader;

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 238
    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->c:Lcom/google/c/a/a/a/c/a/d/s;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/d/s;->c(Lcom/google/c/a/a/a/c/a/d/s;)Ljava/net/HttpURLConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 239
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->c:Lcom/google/c/a/a/a/c/a/d/s;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/d/s;->c(Lcom/google/c/a/a/a/c/a/d/s;)Ljava/net/HttpURLConnection;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    .line 242
    :cond_3
    :goto_2
    return-void

    .line 222
    :cond_4
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 223
    :try_start_3
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->c:Lcom/google/c/a/a/a/c/a/d/s;

    invoke-virtual {v0}, Lcom/google/c/a/a/a/c/a/d/s;->b()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 231
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->b:Ljava/io/BufferedReader;

    if-eqz v0, :cond_5

    .line 233
    :try_start_4
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->b:Ljava/io/BufferedReader;

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4

    .line 238
    :cond_5
    :goto_3
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->c:Lcom/google/c/a/a/a/c/a/d/s;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/d/s;->c(Lcom/google/c/a/a/a/c/a/d/s;)Ljava/net/HttpURLConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 239
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->c:Lcom/google/c/a/a/a/c/a/d/s;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/d/s;->c(Lcom/google/c/a/a/a/c/a/d/s;)Ljava/net/HttpURLConnection;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_2

    .line 231
    :cond_6
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->b:Ljava/io/BufferedReader;

    if-eqz v0, :cond_7

    .line 233
    :try_start_5
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->b:Ljava/io/BufferedReader;

    invoke-virtual {v0}, Ljava/io/BufferedReader;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    .line 238
    :cond_7
    :goto_4
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->c:Lcom/google/c/a/a/a/c/a/d/s;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/d/s;->c(Lcom/google/c/a/a/a/c/a/d/s;)Ljava/net/HttpURLConnection;

    move-result-object v0

    if-eqz v0, :cond_3

    .line 239
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/v;->c:Lcom/google/c/a/a/a/c/a/d/s;

    invoke-static {v0}, Lcom/google/c/a/a/a/c/a/d/s;->c(Lcom/google/c/a/a/a/c/a/d/s;)Ljava/net/HttpURLConnection;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    goto :goto_2

    .line 231
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/d/v;->b:Ljava/io/BufferedReader;

    if-eqz v1, :cond_8

    .line 233
    :try_start_6
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/d/v;->b:Ljava/io/BufferedReader;

    invoke-virtual {v1}, Ljava/io/BufferedReader;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    .line 238
    :cond_8
    :goto_5
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/d/v;->c:Lcom/google/c/a/a/a/c/a/d/s;

    invoke-static {v1}, Lcom/google/c/a/a/a/c/a/d/s;->c(Lcom/google/c/a/a/a/c/a/d/s;)Ljava/net/HttpURLConnection;

    move-result-object v1

    if-eqz v1, :cond_9

    .line 239
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/d/v;->c:Lcom/google/c/a/a/a/c/a/d/s;

    invoke-static {v1}, Lcom/google/c/a/a/a/c/a/d/s;->c(Lcom/google/c/a/a/a/c/a/d/s;)Ljava/net/HttpURLConnection;

    move-result-object v1

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_9
    throw v0

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_4

    :catch_4
    move-exception v0

    goto :goto_3
.end method
