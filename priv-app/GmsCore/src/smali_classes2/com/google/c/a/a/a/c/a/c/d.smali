.class public final Lcom/google/c/a/a/a/c/a/c/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/c/a/a/a/c/a/c/e;

.field public final b:Ljava/lang/Exception;

.field public final c:Lcom/google/c/a/a/a/c/a/e/e;

.field public final d:I


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/a/c/a/c/e;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 65
    const/4 v0, -0x1

    invoke-direct {p0, p1, v1, v1, v0}, Lcom/google/c/a/a/a/c/a/c/d;-><init>(Lcom/google/c/a/a/a/c/a/c/e;Lcom/google/c/a/a/a/c/a/e/e;Ljava/lang/Exception;I)V

    .line 66
    return-void
.end method

.method public constructor <init>(Lcom/google/c/a/a/a/c/a/c/e;Lcom/google/c/a/a/a/c/a/e/e;Ljava/lang/Exception;I)V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/c/d;->a:Lcom/google/c/a/a/a/c/a/c/e;

    .line 71
    iput-object p3, p0, Lcom/google/c/a/a/a/c/a/c/d;->b:Ljava/lang/Exception;

    .line 72
    iput-object p2, p0, Lcom/google/c/a/a/a/c/a/c/d;->c:Lcom/google/c/a/a/a/c/a/e/e;

    .line 73
    iput p4, p0, Lcom/google/c/a/a/a/c/a/c/d;->d:I

    .line 74
    if-nez p1, :cond_0

    .line 75
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 77
    :cond_0
    sget-object v0, Lcom/google/c/a/a/a/c/a/c/e;->c:Lcom/google/c/a/a/a/c/a/c/e;

    if-ne p1, v0, :cond_2

    .line 78
    if-eqz p3, :cond_1

    if-nez p2, :cond_4

    .line 79
    :cond_1
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "failureCause and errorType must be set when result is FAIL"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 83
    :cond_2
    if-nez p3, :cond_3

    if-eqz p2, :cond_4

    .line 84
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "failureCause and errorType must not be set when result is not FAIL"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 88
    :cond_4
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 143
    if-ne p0, p1, :cond_1

    .line 166
    :cond_0
    :goto_0
    return v0

    .line 146
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 147
    goto :goto_0

    .line 149
    :cond_2
    instance-of v2, p1, Lcom/google/c/a/a/a/c/a/c/d;

    if-nez v2, :cond_3

    move v0, v1

    .line 150
    goto :goto_0

    .line 152
    :cond_3
    check-cast p1, Lcom/google/c/a/a/a/c/a/c/d;

    .line 153
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/c/d;->c:Lcom/google/c/a/a/a/c/a/e/e;

    iget-object v3, p1, Lcom/google/c/a/a/a/c/a/c/d;->c:Lcom/google/c/a/a/a/c/a/e/e;

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 154
    goto :goto_0

    .line 156
    :cond_4
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/c/d;->b:Ljava/lang/Exception;

    if-nez v2, :cond_5

    .line 157
    iget-object v2, p1, Lcom/google/c/a/a/a/c/a/c/d;->b:Ljava/lang/Exception;

    if-eqz v2, :cond_6

    move v0, v1

    .line 158
    goto :goto_0

    .line 160
    :cond_5
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/c/d;->b:Ljava/lang/Exception;

    iget-object v3, p1, Lcom/google/c/a/a/a/c/a/c/d;->b:Ljava/lang/Exception;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 161
    goto :goto_0

    .line 163
    :cond_6
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/c/d;->a:Lcom/google/c/a/a/a/c/a/c/e;

    iget-object v3, p1, Lcom/google/c/a/a/a/c/a/c/d;->a:Lcom/google/c/a/a/a/c/a/c/e;

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 164
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 133
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/d;->c:Lcom/google/c/a/a/a/c/a/e/e;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 136
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/d;->b:Ljava/lang/Exception;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 137
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/c/d;->a:Lcom/google/c/a/a/a/c/a/c/e;

    if-nez v2, :cond_2

    :goto_2
    add-int/2addr v0, v1

    .line 138
    return v0

    .line 133
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/d;->c:Lcom/google/c/a/a/a/c/a/e/e;

    invoke-virtual {v0}, Lcom/google/c/a/a/a/c/a/e/e;->hashCode()I

    move-result v0

    goto :goto_0

    .line 136
    :cond_1
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/d;->b:Ljava/lang/Exception;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_1

    .line 137
    :cond_2
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/c/d;->a:Lcom/google/c/a/a/a/c/a/c/e;

    invoke-virtual {v1}, Lcom/google/c/a/a/a/c/a/c/e;->hashCode()I

    move-result v1

    goto :goto_2
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/d;->b:Ljava/lang/Exception;

    if-eqz v0, :cond_0

    .line 125
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TestCompleteEvent [status="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/c/d;->a:Lcom/google/c/a/a/a/c/a/c/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", failureCause="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/c/d;->b:Ljava/lang/Exception;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "TestCompleteEvent [status="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/c/d;->a:Lcom/google/c/a/a/a/c/a/c/e;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
