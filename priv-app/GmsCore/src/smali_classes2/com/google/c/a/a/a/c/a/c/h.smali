.class final Lcom/google/c/a/a/a/c/a/c/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/a/c/a/d/f;
.implements Lcom/google/c/a/a/a/c/a/d/g;
.implements Lcom/google/c/a/a/a/c/a/d/h;
.implements Lcom/google/c/a/a/a/c/a/d/i;


# instance fields
.field final synthetic a:Lcom/google/c/a/a/a/c/a/c/g;


# direct methods
.method private constructor <init>(Lcom/google/c/a/a/a/c/a/c/g;)V
    .locals 0

    .prologue
    .line 151
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/c/a/a/a/c/a/c/g;B)V
    .locals 0

    .prologue
    .line 151
    invoke-direct {p0, p1}, Lcom/google/c/a/a/a/c/a/c/h;-><init>(Lcom/google/c/a/a/a/c/a/c/g;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    sget-object v1, Lcom/google/c/a/a/a/c/a/c/i;->f:Lcom/google/c/a/a/a/c/a/c/i;

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/a/c/a/c/g;->a(Lcom/google/c/a/a/a/c/a/c/i;)V

    .line 156
    return-void
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/d/d;)V
    .locals 4

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    sget-object v1, Lcom/google/c/a/a/a/c/a/e/e;->a:Lcom/google/c/a/a/a/c/a/e/e;

    iget-object v2, p1, Lcom/google/c/a/a/a/c/a/d/d;->a:Lcom/google/c/a/a/a/c/a/d/j;

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/c/a/a/a/c/a/c/g;->a(Lcom/google/c/a/a/a/c/a/e/e;Ljava/lang/Exception;I)V

    .line 171
    return-void
.end method

.method public final a(Lcom/google/c/a/a/a/c/a/d/e;)V
    .locals 4

    .prologue
    .line 160
    iget v0, p1, Lcom/google/c/a/a/a/c/a/d/e;->b:I

    const/16 v1, 0xc8

    if-eq v0, v1, :cond_0

    .line 161
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Server returned non-OK response code: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p1, Lcom/google/c/a/a/a/c/a/d/e;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 162
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    sget-object v2, Lcom/google/c/a/a/a/c/a/e/e;->b:Lcom/google/c/a/a/a/c/a/e/e;

    new-instance v3, Ljava/lang/IllegalStateException;

    invoke-direct {v3, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    iget v0, p1, Lcom/google/c/a/a/a/c/a/d/e;->b:I

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/c/a/a/a/c/a/c/g;->a(Lcom/google/c/a/a/a/c/a/e/e;Ljava/lang/Exception;I)V

    .line 166
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    sget-object v1, Lcom/google/c/a/a/a/c/a/c/i;->c:Lcom/google/c/a/a/a/c/a/c/i;

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/a/c/a/c/g;->a(Lcom/google/c/a/a/a/c/a/c/i;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/CharSequence;IIZ)V
    .locals 8

    .prologue
    const-wide/16 v6, 0x5

    .line 175
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    monitor-enter v1

    .line 176
    :goto_0
    if-ge p2, p3, :cond_5

    .line 177
    :try_start_0
    invoke-interface {p1, p2}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v0

    .line 178
    const/16 v2, 0x31

    if-ne v0, v2, :cond_3

    .line 179
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    iget v2, v0, Lcom/google/c/a/a/a/c/a/c/g;->a:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/c/a/a/a/c/a/c/g;->a:I

    .line 180
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    iget v0, v0, Lcom/google/c/a/a/a/c/a/c/g;->a:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 181
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/c/a/a/a/c/a/c/g;->b:J

    .line 183
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    iget v0, v0, Lcom/google/c/a/a/a/c/a/c/g;->a:I

    int-to-long v2, v0

    cmp-long v0, v2, v6

    if-nez v0, :cond_2

    .line 184
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    sget-object v2, Lcom/google/c/a/a/a/c/a/c/i;->d:Lcom/google/c/a/a/a/c/a/c/i;

    invoke-virtual {v0, v2}, Lcom/google/c/a/a/a/c/a/c/g;->a(Lcom/google/c/a/a/a/c/a/c/i;)V

    .line 176
    :cond_1
    :goto_1
    add-int/lit8 p2, p2, 0x1

    goto :goto_0

    .line 185
    :cond_2
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    iget v0, v0, Lcom/google/c/a/a/a/c/a/c/g;->a:I

    int-to-long v2, v0

    cmp-long v0, v2, v6

    if-lez v0, :cond_1

    .line 186
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    sget-object v2, Lcom/google/c/a/a/a/c/a/e/e;->d:Lcom/google/c/a/a/a/c/a/e/e;

    new-instance v3, Ljava/lang/IllegalStateException;

    const-string v4, "Too many stage one responses received."

    invoke-direct {v3, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const/4 v4, -0x1

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/c/a/a/a/c/a/c/g;->a(Lcom/google/c/a/a/a/c/a/e/e;Ljava/lang/Exception;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 197
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 189
    :cond_3
    const/16 v2, 0x32

    if-ne v0, v2, :cond_4

    .line 190
    :try_start_1
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    sget-object v2, Lcom/google/c/a/a/a/c/a/c/i;->e:Lcom/google/c/a/a/a/c/a/c/i;

    invoke-virtual {v0, v2}, Lcom/google/c/a/a/a/c/a/c/g;->a(Lcom/google/c/a/a/a/c/a/c/i;)V

    .line 191
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/c/a/a/a/c/a/c/g;->c:J

    goto :goto_1

    .line 193
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Unexpected response from server: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 194
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/c/h;->a:Lcom/google/c/a/a/a/c/a/c/g;

    sget-object v3, Lcom/google/c/a/a/a/c/a/e/e;->d:Lcom/google/c/a/a/a/c/a/e/e;

    new-instance v4, Ljava/lang/IllegalStateException;

    invoke-direct {v4, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    const/4 v0, -0x1

    invoke-virtual {v2, v3, v4, v0}, Lcom/google/c/a/a/a/c/a/c/g;->a(Lcom/google/c/a/a/a/c/a/e/e;Ljava/lang/Exception;I)V

    goto :goto_1

    .line 197
    :cond_5
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
