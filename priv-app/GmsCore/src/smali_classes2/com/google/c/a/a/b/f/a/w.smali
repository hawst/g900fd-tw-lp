.class public final Lcom/google/c/a/a/b/f/a/w;
.super Lcom/google/c/a/a/b/f/b;
.source "SourceFile"


# direct methods
.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 33
    const-class v0, Lcom/google/c/a/a/b/b/a/c/h;

    invoke-direct {p0, v0, p1}, Lcom/google/c/a/a/b/f/b;-><init>(Ljava/lang/Class;Z)V

    .line 34
    return-void
.end method


# virtual methods
.method protected final synthetic a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)V
    .locals 2

    .prologue
    .line 10
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/h;

    sget-object v0, Lcom/google/c/a/a/b/f/a/x;->a:Lcom/google/c/a/a/b/f/a/x;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/w;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    sget-object v0, Lcom/google/c/a/a/b/f/a/x;->b:Lcom/google/c/a/a/b/f/a/x;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/w;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    sget-object v0, Lcom/google/c/a/a/b/f/a/x;->c:Lcom/google/c/a/a/b/f/a/x;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->c()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/w;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    sget-object v0, Lcom/google/c/a/a/b/f/a/x;->d:Lcom/google/c/a/a/b/f/a/x;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->d()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/w;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    sget-object v0, Lcom/google/c/a/a/b/f/a/x;->e:Lcom/google/c/a/a/b/f/a/x;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->f()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/w;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    sget-object v0, Lcom/google/c/a/a/b/f/a/x;->f:Lcom/google/c/a/a/b/f/a/x;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/b/a/c/h;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p2, p3, v0, v1}, Lcom/google/c/a/a/b/f/a/w;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V

    return-void
.end method

.method public final a(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 62
    instance-of v0, p1, Lcom/google/c/a/a/b/b/a/c/h;

    return v0
.end method

.method public final synthetic b(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 10
    sget-object v0, Lcom/google/c/a/a/b/f/a/x;->a:Lcom/google/c/a/a/b/f/a/x;

    const-class v1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0, v1}, Lcom/google/c/a/a/b/f/a/w;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    sget-object v0, Lcom/google/c/a/a/b/f/a/x;->b:Lcom/google/c/a/a/b/f/a/x;

    const-class v2, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0, v2}, Lcom/google/c/a/a/b/f/a/w;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    sget-object v0, Lcom/google/c/a/a/b/f/a/x;->c:Lcom/google/c/a/a/b/f/a/x;

    const-class v3, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2, v0, v3}, Lcom/google/c/a/a/b/f/a/w;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget-object v0, Lcom/google/c/a/a/b/f/a/x;->d:Lcom/google/c/a/a/b/f/a/x;

    const-class v4, Ljava/lang/Boolean;

    invoke-virtual {p0, p1, p2, v0, v4}, Lcom/google/c/a/a/b/f/a/w;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    sget-object v0, Lcom/google/c/a/a/b/f/a/x;->e:Lcom/google/c/a/a/b/f/a/x;

    const-class v5, Ljava/lang/Integer;

    invoke-virtual {p0, p1, p2, v0, v5}, Lcom/google/c/a/a/b/f/a/w;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    sget-object v0, Lcom/google/c/a/a/b/f/a/x;->f:Lcom/google/c/a/a/b/f/a/x;

    const-class v5, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, v0, v5}, Lcom/google/c/a/a/b/f/a/w;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    new-instance v0, Lcom/google/c/a/a/b/b/a/c/h;

    invoke-direct/range {v0 .. v6}, Lcom/google/c/a/a/b/b/a/c/h;-><init>(Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;I)V

    return-object v0
.end method
