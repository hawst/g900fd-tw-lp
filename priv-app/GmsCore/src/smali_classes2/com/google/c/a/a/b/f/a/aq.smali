.class public final enum Lcom/google/c/a/a/b/f/a/aq;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/f/a/k;


# static fields
.field public static final enum a:Lcom/google/c/a/a/b/f/a/aq;

.field public static final enum b:Lcom/google/c/a/a/b/f/a/aq;

.field public static final enum c:Lcom/google/c/a/a/b/f/a/aq;

.field private static final synthetic e:[Lcom/google/c/a/a/b/f/a/aq;


# instance fields
.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 16
    new-instance v0, Lcom/google/c/a/a/b/f/a/aq;

    const-string v1, "REVISION"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/c/a/a/b/f/a/aq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/aq;->a:Lcom/google/c/a/a/b/f/a/aq;

    .line 17
    new-instance v0, Lcom/google/c/a/a/b/f/a/aq;

    const-string v1, "REQUEST_NUMBER"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/c/a/a/b/f/a/aq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/aq;->b:Lcom/google/c/a/a/b/f/a/aq;

    .line 18
    new-instance v0, Lcom/google/c/a/a/b/f/a/aq;

    const-string v1, "CHANGES"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/c/a/a/b/f/a/aq;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/aq;->c:Lcom/google/c/a/a/b/f/a/aq;

    .line 15
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/c/a/a/b/f/a/aq;

    sget-object v1, Lcom/google/c/a/a/b/f/a/aq;->a:Lcom/google/c/a/a/b/f/a/aq;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/c/a/a/b/f/a/aq;->b:Lcom/google/c/a/a/b/f/a/aq;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/c/a/a/b/f/a/aq;->c:Lcom/google/c/a/a/b/f/a/aq;

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/c/a/a/b/f/a/aq;->e:[Lcom/google/c/a/a/b/f/a/aq;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 23
    iput p3, p0, Lcom/google/c/a/a/b/f/a/aq;->d:I

    .line 24
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/b/f/a/aq;
    .locals 1

    .prologue
    .line 15
    const-class v0, Lcom/google/c/a/a/b/f/a/aq;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/f/a/aq;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/b/f/a/aq;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/c/a/a/b/f/a/aq;->e:[Lcom/google/c/a/a/b/f/a/aq;

    invoke-virtual {v0}, [Lcom/google/c/a/a/b/f/a/aq;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/b/f/a/aq;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/c/a/a/b/f/a/aq;->d:I

    return v0
.end method
