.class final Lcom/google/c/a/a/a/c/a/a/b;
.super Lcom/google/c/a/a/a/c/a/a/a;
.source "SourceFile"


# instance fields
.field final e:Lcom/google/c/a/a/b/d/b;

.field final f:Lcom/google/c/a/a/a/c/a/a/p;

.field g:Ljava/lang/String;

.field h:J

.field private final i:Lcom/google/c/a/a/a/c/a/d/b;


# direct methods
.method constructor <init>(Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/a/c/a/b/d;Lcom/google/c/a/a/a/c/a/a/n;Lcom/google/c/a/a/a/c/a/d/k;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 45
    invoke-direct {p0}, Lcom/google/c/a/a/a/c/a/a/a;-><init>()V

    .line 41
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/c/a/a/a/c/a/a/b;->h:J

    .line 46
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/a/b;->e:Lcom/google/c/a/a/b/d/b;

    .line 47
    if-nez p1, :cond_0

    .line 48
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "jsonDriver"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 50
    :cond_0
    new-instance v0, Lcom/google/c/a/a/a/c/a/a/p;

    invoke-direct {v0, p1}, Lcom/google/c/a/a/a/c/a/a/p;-><init>(Lcom/google/c/a/a/b/d/b;)V

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/a/b;->f:Lcom/google/c/a/a/a/c/a/a/p;

    .line 51
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 52
    const-string v0, "VER"

    const-string v1, "8"

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    if-eqz p3, :cond_1

    .line 54
    iget-object v0, p3, Lcom/google/c/a/a/a/c/a/a/n;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/a/b;->g:Ljava/lang/String;

    .line 55
    iget-wide v0, p3, Lcom/google/c/a/a/a/c/a/a/n;->b:J

    iput-wide v0, p0, Lcom/google/c/a/a/a/c/a/a/b;->h:J

    .line 58
    const-string v0, "SID"

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/b;->g:Ljava/lang/String;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const-string v0, "AID"

    iget-wide v4, p0, Lcom/google/c/a/a/a/c/a/a/b;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    const-string v0, "RID"

    const-string v1, "rpc"

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 70
    :goto_0
    const-string v1, "CI"

    if-eqz p5, :cond_2

    const-string v0, "0"

    :goto_1
    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    if-eqz p3, :cond_3

    sget-object v0, Lcom/google/c/a/a/a/c/a/d/r;->a:Lcom/google/c/a/a/a/c/a/d/r;

    .line 73
    :goto_2
    sget-object v1, Lcom/google/c/a/a/a/c/a/d/r;->b:Lcom/google/c/a/a/a/c/a/d/r;

    if-ne v0, v1, :cond_4

    const-string v1, ""

    .line 74
    :goto_3
    iget-object v3, p2, Lcom/google/c/a/a/a/c/a/b/d;->d:Lcom/google/c/a/a/a/c/a/b/a;

    sget-object v4, Lcom/google/c/a/a/a/c/a/b/c;->b:Lcom/google/c/a/a/a/c/a/b/c;

    invoke-virtual {v3, v4}, Lcom/google/c/a/a/a/c/a/b/a;->a(Lcom/google/c/a/a/a/c/a/b/c;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p2, v3, v2, v0, v1}, Lcom/google/c/a/a/a/c/a/d/p;->a(Lcom/google/c/a/a/a/c/a/b/d;Ljava/lang/String;Ljava/util/Map;Lcom/google/c/a/a/a/c/a/d/r;Ljava/lang/String;)Lcom/google/c/a/a/a/c/a/d/p;

    move-result-object v0

    .line 81
    invoke-interface {p4, v0, v6}, Lcom/google/c/a/a/a/c/a/d/k;->a(Lcom/google/c/a/a/a/c/a/d/p;Z)Lcom/google/c/a/a/a/c/a/d/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/a/b;->i:Lcom/google/c/a/a/a/c/a/d/b;

    .line 82
    new-instance v0, Lcom/google/c/a/a/a/c/a/a/c;

    invoke-direct {v0, p0, v6}, Lcom/google/c/a/a/a/c/a/a/c;-><init>(Lcom/google/c/a/a/a/c/a/a/b;B)V

    .line 83
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/b;->i:Lcom/google/c/a/a/a/c/a/d/b;

    invoke-interface {v1, v0}, Lcom/google/c/a/a/a/c/a/d/b;->a(Lcom/google/c/a/a/a/c/a/d/i;)V

    .line 84
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/b;->i:Lcom/google/c/a/a/a/c/a/d/b;

    invoke-interface {v1, v0}, Lcom/google/c/a/a/a/c/a/d/b;->a(Lcom/google/c/a/a/a/c/a/d/g;)V

    .line 85
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/b;->i:Lcom/google/c/a/a/a/c/a/d/b;

    invoke-interface {v1, v0}, Lcom/google/c/a/a/a/c/a/d/b;->a(Lcom/google/c/a/a/a/c/a/d/h;)V

    .line 86
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/b;->i:Lcom/google/c/a/a/a/c/a/d/b;

    invoke-interface {v1, v0}, Lcom/google/c/a/a/a/c/a/d/b;->a(Lcom/google/c/a/a/a/c/a/d/f;)V

    .line 87
    return-void

    .line 66
    :cond_1
    const-string v0, "RID"

    const-string v1, "0"

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 70
    :cond_2
    const-string v0, "1"

    goto :goto_1

    .line 72
    :cond_3
    sget-object v0, Lcom/google/c/a/a/a/c/a/d/r;->b:Lcom/google/c/a/a/a/c/a/d/r;

    goto :goto_2

    .line 73
    :cond_4
    const/4 v1, 0x0

    goto :goto_3
.end method

.method static synthetic a(Lcom/google/c/a/a/a/c/a/a/b;)Lcom/google/c/a/a/a/c/a/a/n;
    .locals 4

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a/b;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/c/a/a/a/c/a/a/n;

    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/b;->g:Ljava/lang/String;

    iget-wide v2, p0, Lcom/google/c/a/a/a/c/a/a/b;->h:J

    invoke-direct {v0, v1, v2, v3}, Lcom/google/c/a/a/a/c/a/a/n;-><init>(Ljava/lang/String;J)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final b()V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a/b;->i:Lcom/google/c/a/a/a/c/a/d/b;

    invoke-interface {v0}, Lcom/google/c/a/a/a/c/a/d/b;->b()V

    .line 97
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a/b;->i:Lcom/google/c/a/a/a/c/a/d/b;

    invoke-interface {v0}, Lcom/google/c/a/a/a/c/a/d/b;->a()V

    .line 92
    return-void
.end method
