.class public abstract Lcom/google/c/a/a/b/f/b;
.super Lcom/google/c/a/a/b/f/g;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/lang/Class;Z)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Lcom/google/c/a/a/b/f/g;-><init>(Ljava/lang/Class;Z)V

    .line 26
    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 105
    const-class v0, Ljava/lang/Object;

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/c/a/a/b/f/b;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 85
    const-string v0, "Field cannot be null."

    invoke-static {p3, v0}, Lcom/google/c/a/a/b/h/c;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 86
    invoke-interface {p3}, Lcom/google/c/a/a/b/f/a/k;->a()I

    move-result v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/c/a/a/b/f/b;->a(Ljava/lang/Object;ILcom/google/c/a/a/b/d/b;)Ljava/lang/Object;

    move-result-object v0

    .line 87
    if-nez v0, :cond_0

    .line 89
    const/4 v0, 0x0

    .line 91
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0, p2, p4}, Lcom/google/c/a/a/b/f/f;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Ljava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 67
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/c/a/a/b/f/b;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;Z)V

    .line 68
    return-void
.end method

.method protected final a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Lcom/google/c/a/a/b/f/a/k;Ljava/lang/Object;Z)V
    .locals 2

    .prologue
    .line 46
    const-string v0, "Field cannot be null."

    invoke-static {p3, v0}, Lcom/google/c/a/a/b/h/c;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 47
    if-nez p4, :cond_0

    .line 52
    :goto_0
    return-void

    .line 50
    :cond_0
    invoke-static {p4, p2, p5}, Lcom/google/c/a/a/b/f/f;->a(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;Z)Ljava/lang/Object;

    move-result-object v0

    .line 51
    invoke-interface {p3}, Lcom/google/c/a/a/b/f/a/k;->a()I

    move-result v1

    invoke-virtual {p0, p1, v1, v0, p2}, Lcom/google/c/a/a/b/f/b;->a(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/c/a/a/b/d/b;)V

    goto :goto_0
.end method
