.class final Lcom/google/c/a/a/b/b/a/b/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/List;

.field final synthetic b:Lcom/google/c/a/a/b/b/a/b/x;


# direct methods
.method private constructor <init>(Lcom/google/c/a/a/b/b/a/b/x;)V
    .locals 1

    .prologue
    .line 302
    iput-object p1, p0, Lcom/google/c/a/a/b/b/a/b/y;->b:Lcom/google/c/a/a/b/b/a/b/x;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 300
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/b/y;->a:Ljava/util/List;

    .line 304
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/c/a/a/b/b/a/b/x;B)V
    .locals 0

    .prologue
    .line 299
    invoke-direct {p0, p1}, Lcom/google/c/a/a/b/b/a/b/y;-><init>(Lcom/google/c/a/a/b/b/a/b/x;)V

    return-void
.end method


# virtual methods
.method final a()V
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/y;->b:Lcom/google/c/a/a/b/b/a/b/x;

    iget-object v0, v0, Lcom/google/c/a/a/b/b/a/b/x;->a:Lcom/google/c/a/a/b/b/a/b/ab;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/c/a/a/b/b/a/b/ab;->f:Z

    .line 312
    return-void
.end method

.method final a(Lcom/google/c/a/a/b/b/a/a/a;)V
    .locals 1

    .prologue
    .line 318
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/y;->a:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 319
    return-void
.end method

.method final b()Ljava/util/Collection;
    .locals 11

    .prologue
    .line 333
    new-instance v9, Ljava/util/IdentityHashMap;

    invoke-direct {v9}, Ljava/util/IdentityHashMap;-><init>()V

    .line 335
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/y;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v8, v0

    check-cast v8, Lcom/google/c/a/a/b/b/a/a/a;

    .line 336
    iget-object v0, v8, Lcom/google/c/a/a/b/b/a/a/a;->a:Lcom/google/c/a/a/b/b/a/g;

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/b/a/a/b;

    .line 337
    if-nez v0, :cond_0

    .line 339
    new-instance v0, Lcom/google/c/a/a/b/b/a/a/b;

    iget-object v1, v8, Lcom/google/c/a/a/b/b/a/a/a;->a:Lcom/google/c/a/a/b/b/a/g;

    iget-object v2, v8, Lcom/google/c/a/a/b/b/a/a/a;->c:Ljava/lang/String;

    iget-object v3, v8, Lcom/google/c/a/a/b/b/a/a/a;->d:Ljava/lang/String;

    iget-object v4, v8, Lcom/google/c/a/a/b/b/a/a/a;->f:Ljava/util/List;

    iget-boolean v5, v8, Lcom/google/c/a/a/b/b/a/a/a;->b:Z

    iget-object v6, v8, Lcom/google/c/a/a/b/b/a/a/a;->e:Lcom/google/c/a/a/b/b/a/k;

    iget-object v7, v8, Lcom/google/c/a/a/b/b/a/a/a;->a:Lcom/google/c/a/a/b/b/a/g;

    invoke-interface {v7}, Lcom/google/c/a/a/b/b/a/g;->g()Ljava/util/List;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/google/c/a/a/b/b/a/a/b;-><init>(Lcom/google/c/a/a/b/b/a/g;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;Ljava/util/List;)V

    .line 342
    iget-object v1, v8, Lcom/google/c/a/a/b/b/a/a/a;->a:Lcom/google/c/a/a/b/b/a/g;

    invoke-interface {v9, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 344
    :cond_0
    iget-object v0, v0, Lcom/google/c/a/a/b/b/a/a/b;->g:Ljava/util/List;

    invoke-interface {v0, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 346
    :cond_1
    invoke-interface {v9}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableCollection(Ljava/util/Collection;)Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method
