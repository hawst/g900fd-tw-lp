.class public final Lcom/google/c/a/a/b/c/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:J

.field private final b:Lcom/google/c/b/a/c;


# direct methods
.method public constructor <init>(JLcom/google/c/b/a/c;)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    iput-wide p1, p0, Lcom/google/c/a/a/b/c/e;->a:J

    .line 16
    const-string v0, "command"

    invoke-static {p3, v0}, Lcom/google/c/a/a/b/h/c;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    iput-object v0, p0, Lcom/google/c/a/a/b/c/e;->b:Lcom/google/c/b/a/c;

    .line 17
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 20
    iget-wide v0, p0, Lcom/google/c/a/a/b/c/e;->a:J

    return-wide v0
.end method

.method public final b()Lcom/google/c/b/a/c;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/c/a/a/b/c/e;->b:Lcom/google/c/b/a/c;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 29
    if-ne p0, p1, :cond_1

    .line 36
    :cond_0
    :goto_0
    return v0

    .line 32
    :cond_1
    instance-of v2, p1, Lcom/google/c/a/a/b/c/e;

    if-nez v2, :cond_2

    move v0, v1

    .line 33
    goto :goto_0

    .line 35
    :cond_2
    check-cast p1, Lcom/google/c/a/a/b/c/e;

    .line 36
    iget-wide v2, p0, Lcom/google/c/a/a/b/c/e;->a:J

    iget-wide v4, p1, Lcom/google/c/a/a/b/c/e;->a:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/c/a/a/b/c/e;->b:Lcom/google/c/b/a/c;

    iget-object v3, p1, Lcom/google/c/a/a/b/c/e;->b:Lcom/google/c/b/a/c;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/c/a/a/b/c/e;->b:Lcom/google/c/b/a/c;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 44
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/c/a/a/b/c/e;->a:J

    iget-wide v4, p0, Lcom/google/c/a/a/b/c/e;->a:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 45
    return v0

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/b/c/e;->b:Lcom/google/c/b/a/c;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    goto :goto_0
.end method
