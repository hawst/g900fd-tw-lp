.class public abstract Lcom/google/c/a/a/b/f/c;
.super Lcom/google/c/a/a/b/f/g;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/lang/Class;Z)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1, p2}, Lcom/google/c/a/a/b/f/g;-><init>(Ljava/lang/Class;Z)V

    .line 19
    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/Object;Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)V
    .locals 2

    .prologue
    .line 23
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p3}, Lcom/google/c/a/a/b/f/c;->c(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {p0, p2, v0, v1, p3}, Lcom/google/c/a/a/b/f/c;->a(Ljava/lang/Object;ILjava/lang/Object;Lcom/google/c/a/a/b/d/b;)V

    .line 24
    return-void
.end method

.method public a(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 34
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/b/f/c;->b:Ljava/lang/Class;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-interface {p2, p1}, Lcom/google/c/a/a/b/d/b;->g(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 28
    const/4 v0, 0x0

    .line 30
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/c/a/a/b/f/c;->a(Ljava/lang/Object;ILcom/google/c/a/a/b/d/b;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/google/c/a/a/b/f/c;->d(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method protected abstract c(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
.end method

.method protected abstract d(Ljava/lang/Object;Lcom/google/c/a/a/b/d/b;)Ljava/lang/Object;
.end method
