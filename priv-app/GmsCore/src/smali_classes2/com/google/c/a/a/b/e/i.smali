.class public Lcom/google/c/a/a/b/e/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/logging/Logger;

.field private final b:Lcom/google/c/a/a/b/e/e;

.field private final c:Lcom/google/c/a/a/b/e/p;

.field private final d:Lcom/google/c/a/a/b/e/n;

.field private final e:Lcom/google/c/a/a/b/b/a/b;

.field private final f:Lcom/google/c/a/a/b/e/c;

.field private g:Lcom/google/c/a/a/b/a/e;

.field private final h:Ljava/lang/String;

.field private i:Z

.field private j:J

.field private k:Z

.field private l:Z

.field private final m:Ljava/util/PriorityQueue;

.field private final n:Ljava/util/Set;

.field private o:Lcom/google/c/a/a/b/c/d;

.field private p:I

.field private q:Z

.field private final r:Ljava/lang/Runnable;

.field private s:Lcom/google/c/a/a/b/e/d;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/e/e;Lcom/google/c/a/a/b/e/n;Lcom/google/c/a/a/b/b/a/b;Lcom/google/c/a/a/b/e/c;Ljava/lang/String;Lcom/google/c/a/a/b/a/e;)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    const-class v0, Lcom/google/c/a/a/b/e/i;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/e/i;->a:Ljava/util/logging/Logger;

    .line 46
    new-instance v0, Lcom/google/c/a/a/b/e/p;

    invoke-direct {v0}, Lcom/google/c/a/a/b/e/p;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/e/i;->c:Lcom/google/c/a/a/b/e/p;

    .line 62
    new-instance v0, Ljava/util/PriorityQueue;

    invoke-direct {v0}, Ljava/util/PriorityQueue;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/b/e/i;->m:Ljava/util/PriorityQueue;

    .line 63
    new-instance v0, Ljava/util/IdentityHashMap;

    invoke-direct {v0}, Ljava/util/IdentityHashMap;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/e/i;->n:Ljava/util/Set;

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/c/a/a/b/e/i;->p:I

    .line 70
    new-instance v0, Lcom/google/c/a/a/b/e/j;

    invoke-direct {v0, p0}, Lcom/google/c/a/a/b/e/j;-><init>(Lcom/google/c/a/a/b/e/i;)V

    iput-object v0, p0, Lcom/google/c/a/a/b/e/i;->r:Ljava/lang/Runnable;

    .line 81
    iput-object p1, p0, Lcom/google/c/a/a/b/e/i;->b:Lcom/google/c/a/a/b/e/e;

    .line 82
    new-instance v0, Lcom/google/c/a/a/b/e/k;

    invoke-direct {v0, p0}, Lcom/google/c/a/a/b/e/k;-><init>(Lcom/google/c/a/a/b/e/i;)V

    invoke-interface {p1, v0}, Lcom/google/c/a/a/b/e/e;->a(Lcom/google/c/a/a/b/e/h;)V

    .line 93
    iput-object p2, p0, Lcom/google/c/a/a/b/e/i;->d:Lcom/google/c/a/a/b/e/n;

    .line 94
    iput-object p3, p0, Lcom/google/c/a/a/b/e/i;->e:Lcom/google/c/a/a/b/b/a/b;

    .line 95
    iput-object p4, p0, Lcom/google/c/a/a/b/e/i;->f:Lcom/google/c/a/a/b/e/c;

    .line 96
    iput-object p5, p0, Lcom/google/c/a/a/b/e/i;->h:Ljava/lang/String;

    .line 97
    iput-object p6, p0, Lcom/google/c/a/a/b/e/i;->g:Lcom/google/c/a/a/b/a/e;

    .line 98
    return-void
.end method

.method private declared-synchronized a(ILcom/google/c/a/a/b/e/a;)V
    .locals 3

    .prologue
    .line 259
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/c/a/a/b/e/i;->p:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 260
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Initial revision has not yet been set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 262
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/c/a/a/b/e/i;->q:Z

    .line 264
    new-instance v0, Lcom/google/c/a/a/b/e/m;

    invoke-direct {v0, p0, p2}, Lcom/google/c/a/a/b/e/m;-><init>(Lcom/google/c/a/a/b/e/i;Lcom/google/c/a/a/b/e/a;)V

    .line 286
    iget-object v1, p0, Lcom/google/c/a/a/b/e/i;->b:Lcom/google/c/a/a/b/e/e;

    iget v2, p0, Lcom/google/c/a/a/b/e/i;->p:I

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v1, v2, p1, v0}, Lcom/google/c/a/a/b/e/e;->a(IILcom/google/c/a/a/b/e/a;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 287
    monitor-exit p0

    return-void
.end method

.method static synthetic a(Lcom/google/c/a/a/b/e/i;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/c/a/a/b/e/i;->f()V

    return-void
.end method

.method static synthetic a(Lcom/google/c/a/a/b/e/i;Ljava/util/Collection;Z)V
    .locals 3

    .prologue
    .line 37
    monitor-enter p0

    if-eqz p2, :cond_1

    :try_start_0
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/c/d;

    invoke-direct {p0, v0}, Lcom/google/c/a/a/b/e/i;->a(Lcom/google/c/a/a/b/c/d;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/c/a/a/b/e/i;->n:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->m:Ljava/util/PriorityQueue;

    invoke-virtual {v0, p1}, Ljava/util/PriorityQueue;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->o:Lcom/google/c/a/a/b/c/d;

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/google/c/a/a/b/e/i;->d()Lcom/google/c/a/a/b/c/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/e/i;->o:Lcom/google/c/a/a/b/c/d;

    :cond_2
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {p0}, Lcom/google/c/a/a/b/e/i;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->f:Lcom/google/c/a/a/b/e/c;

    invoke-interface {v0}, Lcom/google/c/a/a/b/e/c;->a()V

    :cond_3
    invoke-direct {p0}, Lcom/google/c/a/a/b/e/i;->e()V

    return-void
.end method

.method private a(Lcom/google/c/a/a/b/c/d;)Z
    .locals 2

    .prologue
    .line 247
    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->f()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/d;->f()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    .line 249
    :goto_0
    iget-object v1, p0, Lcom/google/c/a/a/b/e/i;->h:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0

    .line 247
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/c/a/a/b/e/i;)Z
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/c/a/a/b/e/i;->q:Z

    return v0
.end method

.method private declared-synchronized d()Lcom/google/c/a/a/b/c/d;
    .locals 9

    .prologue
    const/4 v4, -0x1

    const/4 v1, 0x0

    .line 201
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->m:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    .line 243
    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    move v2, v4

    .line 205
    :cond_1
    :goto_1
    :try_start_1
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->m:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    .line 206
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->m:Ljava/util/PriorityQueue;

    invoke-virtual {v0}, Ljava/util/PriorityQueue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/c/d;

    .line 207
    iget-object v3, p0, Lcom/google/c/a/a/b/e/i;->n:Ljava/util/Set;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v6

    .line 208
    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/d;->d()I

    move-result v5

    .line 209
    iget v3, p0, Lcom/google/c/a/a/b/e/i;->p:I

    add-int/lit8 v3, v3, 0x1

    .line 210
    if-ne v5, v3, :cond_7

    .line 212
    invoke-direct {p0, v0}, Lcom/google/c/a/a/b/e/i;->a(Lcom/google/c/a/a/b/c/d;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 214
    iget-object v3, p0, Lcom/google/c/a/a/b/e/i;->c:Lcom/google/c/a/a/b/e/p;

    invoke-virtual {v3, v0}, Lcom/google/c/a/a/b/e/p;->a(Lcom/google/c/a/a/b/c/d;)Z

    move-result v3

    if-nez v3, :cond_2

    const/4 v3, 0x0

    :goto_2
    if-eqz v3, :cond_4

    .line 215
    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/d;->e()I

    move-result v0

    iput v0, p0, Lcom/google/c/a/a/b/e/i;->p:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 201
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 214
    :cond_2
    :try_start_2
    iget-object v3, p0, Lcom/google/c/a/a/b/e/i;->g:Lcom/google/c/a/a/b/a/e;

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/c/a/a/b/e/i;->g:Lcom/google/c/a/a/b/a/e;

    new-instance v7, Lcom/google/c/a/a/b/a/c;

    iget-object v8, p0, Lcom/google/c/a/a/b/e/i;->c:Lcom/google/c/a/a/b/e/p;

    invoke-virtual {v8}, Lcom/google/c/a/a/b/e/p;->a()I

    move-result v8

    invoke-direct {v7, v8}, Lcom/google/c/a/a/b/a/c;-><init>(I)V

    invoke-interface {v3, v7}, Lcom/google/c/a/a/b/a/e;->a(Lcom/google/c/a/a/b/a/c;)V

    :cond_3
    invoke-direct {p0}, Lcom/google/c/a/a/b/e/i;->e()V

    invoke-direct {p0}, Lcom/google/c/a/a/b/e/i;->g()V

    const/4 v3, 0x1

    goto :goto_2

    .line 216
    :cond_4
    if-eqz v6, :cond_5

    .line 219
    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/d;->e()I

    move-result v1

    iput v1, p0, Lcom/google/c/a/a/b/e/i;->p:I

    goto :goto_0

    :cond_5
    move v2, v5

    .line 225
    goto :goto_1

    .line 229
    :cond_6
    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/d;->e()I

    move-result v1

    iput v1, p0, Lcom/google/c/a/a/b/e/i;->p:I

    goto :goto_0

    .line 232
    :cond_7
    if-le v5, v3, :cond_1

    .line 235
    iget-object v2, p0, Lcom/google/c/a/a/b/e/i;->m:Ljava/util/PriorityQueue;

    invoke-virtual {v2, v0}, Ljava/util/PriorityQueue;->add(Ljava/lang/Object;)Z

    .line 236
    add-int/lit8 v0, v5, -0x1

    .line 240
    :goto_3
    iget-boolean v2, p0, Lcom/google/c/a/a/b/e/i;->q:Z

    if-nez v2, :cond_8

    if-eq v0, v4, :cond_8

    iget v2, p0, Lcom/google/c/a/a/b/e/i;->p:I

    if-le v0, v2, :cond_8

    .line 241
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/google/c/a/a/b/e/i;->a(ILcom/google/c/a/a/b/e/a;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :cond_8
    move-object v0, v1

    .line 243
    goto/16 :goto_0

    :cond_9
    move v0, v2

    goto :goto_3
.end method

.method private declared-synchronized e()V
    .locals 6

    .prologue
    const-wide/16 v0, 0x0

    .line 379
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/c/a/a/b/e/i;->i:Z

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/c/a/a/b/e/i;->c:Lcom/google/c/a/a/b/e/p;

    invoke-virtual {v2}, Lcom/google/c/a/a/b/e/p;->e()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_1

    .line 392
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 383
    :cond_1
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/c/a/a/b/e/i;->j:J

    sub-long/2addr v2, v4

    .line 384
    cmp-long v4, v2, v0

    if-gez v4, :cond_2

    .line 386
    iget-object v2, p0, Lcom/google/c/a/a/b/e/i;->a:Ljava/util/logging/Logger;

    const-string v3, "Detected system clock skew."

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    .line 389
    :goto_1
    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xc8

    sub-long v0, v4, v0

    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 390
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/c/a/a/b/e/i;->i:Z

    .line 391
    iget-object v2, p0, Lcom/google/c/a/a/b/e/i;->d:Lcom/google/c/a/a/b/e/n;

    iget-object v3, p0, Lcom/google/c/a/a/b/e/i;->r:Ljava/lang/Runnable;

    long-to-int v0, v0

    invoke-interface {v2, v3, v0}, Lcom/google/c/a/a/b/e/n;->a(Ljava/lang/Runnable;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 379
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move-wide v0, v2

    goto :goto_1
.end method

.method private declared-synchronized f()V
    .locals 2

    .prologue
    .line 398
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->a:Ljava/util/logging/Logger;

    const-string v1, "Sending mutations"

    invoke-virtual {v0, v1}, Ljava/util/logging/Logger;->info(Ljava/lang/String;)V

    .line 399
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/c/a/a/b/e/i;->j:J

    .line 400
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->c:Lcom/google/c/a/a/b/e/p;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/e/p;->f()Lcom/google/c/a/a/b/c/g;

    move-result-object v0

    .line 401
    if-eqz v0, :cond_0

    .line 402
    iget-object v1, p0, Lcom/google/c/a/a/b/e/i;->b:Lcom/google/c/a/a/b/e/e;

    invoke-interface {v1, v0}, Lcom/google/c/a/a/b/e/e;->a(Lcom/google/c/a/a/b/c/g;)V

    .line 403
    invoke-direct {p0}, Lcom/google/c/a/a/b/e/i;->g()V

    .line 405
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/c/a/a/b/e/i;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 406
    monitor-exit p0

    return-void

    .line 398
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private g()V
    .locals 3

    .prologue
    .line 409
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->c:Lcom/google/c/a/a/b/e/p;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/e/p;->c()Z

    move-result v0

    .line 410
    iget-object v1, p0, Lcom/google/c/a/a/b/e/i;->c:Lcom/google/c/a/a/b/e/p;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/e/p;->b()Z

    move-result v1

    .line 411
    iget-object v2, p0, Lcom/google/c/a/a/b/e/i;->s:Lcom/google/c/a/a/b/e/d;

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/c/a/a/b/e/i;->k:Z

    if-ne v2, v0, :cond_0

    iget-boolean v2, p0, Lcom/google/c/a/a/b/e/i;->l:Z

    if-eq v2, v1, :cond_1

    .line 413
    :cond_0
    iget-object v2, p0, Lcom/google/c/a/a/b/e/i;->s:Lcom/google/c/a/a/b/e/d;

    invoke-interface {v2, v0, v1}, Lcom/google/c/a/a/b/e/d;->a(ZZ)V

    .line 415
    :cond_1
    iput-boolean v0, p0, Lcom/google/c/a/a/b/e/i;->k:Z

    .line 416
    iput-boolean v1, p0, Lcom/google/c/a/a/b/e/i;->l:Z

    .line 417
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/c/a/a/b/b/a/a;
    .locals 7

    .prologue
    .line 301
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/c/a/a/b/e/i;->b()Z

    move-result v0

    if-nez v0, :cond_0

    .line 302
    invoke-static {}, Lcom/google/c/a/a/b/b/a/a;->f()Lcom/google/c/a/a/b/b/a/a;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 337
    :goto_0
    monitor-exit p0

    return-object v0

    .line 304
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->o:Lcom/google/c/a/a/b/c/d;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/d;->f()Ljava/lang/Long;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->o:Lcom/google/c/a/a/b/c/d;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/d;->f()Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 307
    :goto_1
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->o:Lcom/google/c/a/a/b/c/d;

    invoke-direct {p0, v0}, Lcom/google/c/a/a/b/e/i;->a(Lcom/google/c/a/a/b/c/d;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 311
    sget-object v0, Lcom/google/c/a/a/b/b/a/o;->a:Lcom/google/c/a/a/b/b/a/o;

    .line 312
    iget-object v1, p0, Lcom/google/c/a/a/b/e/i;->a:Ljava/util/logging/Logger;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received change from server for current session that has not beenapplied; treating as remote: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/c/a/a/b/e/i;->o:Lcom/google/c/a/a/b/c/d;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    move-object v1, v0

    .line 321
    :goto_2
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 322
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->o:Lcom/google/c/a/a/b/c/d;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/c/d;->a()Lcom/google/c/b/a/c;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 323
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->c:Lcom/google/c/a/a/b/e/p;

    iget-object v3, p0, Lcom/google/c/a/a/b/e/i;->o:Lcom/google/c/a/a/b/c/d;

    invoke-virtual {v3}, Lcom/google/c/a/a/b/c/d;->e()I

    move-result v3

    invoke-virtual {v0, v2, v3}, Lcom/google/c/a/a/b/e/p;->a(Ljava/util/List;I)V

    .line 326
    iget-object v3, p0, Lcom/google/c/a/a/b/e/i;->e:Lcom/google/c/a/a/b/b/a/b;

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    sget-object v4, Lcom/google/c/a/a/b/b/a/k;->a:Lcom/google/c/a/a/b/b/a/k;

    invoke-interface {v3, v0, v1, v4}, Lcom/google/c/a/a/b/b/a/b;->a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/o;Lcom/google/c/a/a/b/b/a/k;)Lcom/google/c/a/a/b/b/a/a;

    move-result-object v1

    .line 328
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->g:Lcom/google/c/a/a/b/a/e;

    if-eqz v0, :cond_1

    .line 329
    iget-object v3, p0, Lcom/google/c/a/a/b/e/i;->g:Lcom/google/c/a/a/b/a/e;

    new-instance v4, Lcom/google/c/a/a/b/a/g;

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/b/a/c;

    iget-object v2, p0, Lcom/google/c/a/a/b/e/i;->c:Lcom/google/c/a/a/b/e/p;

    invoke-virtual {v2}, Lcom/google/c/a/a/b/e/p;->d()Ljava/util/List;

    move-result-object v2

    iget-object v5, p0, Lcom/google/c/a/a/b/e/i;->o:Lcom/google/c/a/a/b/c/d;

    invoke-virtual {v5}, Lcom/google/c/a/a/b/c/d;->e()I

    move-result v5

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/a;->c()Lcom/google/c/a/a/b/b/a/q;

    move-result-object v6

    invoke-direct {v4, v0, v2, v5, v6}, Lcom/google/c/a/a/b/a/g;-><init>(Lcom/google/c/b/a/c;Ljava/util/List;ILcom/google/c/a/a/b/b/a/q;)V

    invoke-interface {v3, v4}, Lcom/google/c/a/a/b/a/e;->a(Lcom/google/c/a/a/b/a/g;)V

    .line 336
    :cond_1
    invoke-direct {p0}, Lcom/google/c/a/a/b/e/i;->d()Lcom/google/c/a/a/b/c/d;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/e/i;->o:Lcom/google/c/a/a/b/c/d;

    move-object v0, v1

    .line 337
    goto/16 :goto_0

    .line 304
    :cond_2
    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_1

    .line 316
    :cond_3
    new-instance v0, Lcom/google/c/a/a/b/b/a/o;

    iget-object v2, p0, Lcom/google/c/a/a/b/e/i;->o:Lcom/google/c/a/a/b/c/d;

    invoke-virtual {v2}, Lcom/google/c/a/a/b/c/d;->c()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/c/a/a/b/b/a/o;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v1, v0

    goto :goto_2

    .line 301
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(I)V
    .locals 2

    .prologue
    .line 104
    iget v0, p0, Lcom/google/c/a/a/b/e/i;->p:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 105
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Initial revision has already been set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 107
    :cond_0
    iput p1, p0, Lcom/google/c/a/a/b/e/i;->p:I

    .line 108
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->c:Lcom/google/c/a/a/b/e/p;

    invoke-virtual {v0, p1}, Lcom/google/c/a/a/b/e/p;->a(I)V

    .line 109
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/a/f;Lcom/google/c/a/a/b/a/e;Z)V
    .locals 2

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->c:Lcom/google/c/a/a/b/e/p;

    invoke-virtual {v0, p1}, Lcom/google/c/a/a/b/e/p;->a(Lcom/google/c/a/a/b/a/f;)V

    .line 119
    iput-object p2, p0, Lcom/google/c/a/a/b/e/i;->g:Lcom/google/c/a/a/b/a/e;

    .line 120
    invoke-direct {p0}, Lcom/google/c/a/a/b/e/i;->e()V

    .line 121
    const/4 v0, 0x0

    .line 125
    if-nez p3, :cond_0

    .line 126
    new-instance v0, Lcom/google/c/a/a/b/e/l;

    invoke-direct {v0, p0}, Lcom/google/c/a/a/b/e/l;-><init>(Lcom/google/c/a/a/b/e/i;)V

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/google/c/a/a/b/e/i;->b:Lcom/google/c/a/a/b/e/e;

    invoke-interface {v1, p1, p3, v0}, Lcom/google/c/a/a/b/e/e;->a(Lcom/google/c/a/a/b/a/f;ZLcom/google/c/a/a/b/e/f;)V

    .line 138
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/e/a;)V
    .locals 1

    .prologue
    .line 294
    const/4 v0, -0x1

    invoke-direct {p0, v0, p1}, Lcom/google/c/a/a/b/e/i;->a(ILcom/google/c/a/a/b/e/a;)V

    .line 295
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/e/d;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/google/c/a/a/b/e/i;->s:Lcom/google/c/a/a/b/e/d;

    .line 170
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/e/g;)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->b:Lcom/google/c/a/a/b/e/e;

    invoke-interface {v0, p1}, Lcom/google/c/a/a/b/e/e;->a(Lcom/google/c/a/a/b/e/g;)V

    .line 145
    monitor-enter p0

    .line 146
    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->g:Lcom/google/c/a/a/b/a/e;

    if-eqz v0, :cond_0

    .line 147
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->g:Lcom/google/c/a/a/b/a/e;

    invoke-interface {v0}, Lcom/google/c/a/a/b/a/e;->a()V

    .line 149
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/c/a/a/b/e/i;->g:Lcom/google/c/a/a/b/a/e;

    .line 151
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/b/a/q;)V
    .locals 3

    .prologue
    .line 159
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->c:Lcom/google/c/a/a/b/e/p;

    invoke-virtual {v0, p1}, Lcom/google/c/a/a/b/e/p;->a(Lcom/google/c/b/a/c;)Lcom/google/c/a/a/b/c/e;

    move-result-object v0

    .line 160
    invoke-direct {p0}, Lcom/google/c/a/a/b/e/i;->g()V

    .line 161
    iget-object v1, p0, Lcom/google/c/a/a/b/e/i;->g:Lcom/google/c/a/a/b/a/e;

    if-eqz v1, :cond_0

    .line 162
    iget-object v1, p0, Lcom/google/c/a/a/b/e/i;->g:Lcom/google/c/a/a/b/a/e;

    new-instance v2, Lcom/google/c/a/a/b/a/b;

    invoke-direct {v2, p1, v0, p2}, Lcom/google/c/a/a/b/a/b;-><init>(Lcom/google/c/b/a/c;Lcom/google/c/a/a/b/c/e;Lcom/google/c/a/a/b/b/a/q;)V

    invoke-interface {v1, v2}, Lcom/google/c/a/a/b/a/e;->a(Lcom/google/c/a/a/b/a/b;)V

    .line 165
    :cond_0
    invoke-direct {p0}, Lcom/google/c/a/a/b/e/i;->e()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    monitor-exit p0

    return-void

    .line 159
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b()Z
    .locals 1

    .prologue
    .line 345
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->o:Lcom/google/c/a/a/b/c/d;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->c:Lcom/google/c/a/a/b/e/p;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/e/p;->c()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/c/a/a/b/e/i;->c:Lcom/google/c/a/a/b/e/p;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/e/p;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
