.class final enum Lcom/google/c/a/a/a/c/a/c/i;
.super Ljava/lang/Enum;
.source "SourceFile"


# static fields
.field public static final enum a:Lcom/google/c/a/a/a/c/a/c/i;

.field public static final enum b:Lcom/google/c/a/a/a/c/a/c/i;

.field public static final enum c:Lcom/google/c/a/a/a/c/a/c/i;

.field public static final enum d:Lcom/google/c/a/a/a/c/a/c/i;

.field public static final enum e:Lcom/google/c/a/a/a/c/a/c/i;

.field public static final enum f:Lcom/google/c/a/a/a/c/a/c/i;

.field private static final synthetic h:[Lcom/google/c/a/a/a/c/a/c/i;


# instance fields
.field private final g:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 110
    new-instance v0, Lcom/google/c/a/a/a/c/a/c/i;

    const-string v1, "NEW"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/c/a/a/a/c/a/c/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/c/i;->a:Lcom/google/c/a/a/a/c/a/c/i;

    .line 115
    new-instance v0, Lcom/google/c/a/a/a/c/a/c/i;

    const-string v1, "WAIT_FOR_HEADERS"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/c/a/a/a/c/a/c/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/c/i;->b:Lcom/google/c/a/a/a/c/a/c/i;

    .line 120
    new-instance v0, Lcom/google/c/a/a/a/c/a/c/i;

    const-string v1, "WAIT_FOR_STAGE_1"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/c/a/a/a/c/a/c/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/c/i;->c:Lcom/google/c/a/a/a/c/a/c/i;

    .line 125
    new-instance v0, Lcom/google/c/a/a/a/c/a/c/i;

    const-string v1, "WAIT_FOR_STAGE_2"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/c/a/a/a/c/a/c/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/c/i;->d:Lcom/google/c/a/a/a/c/a/c/i;

    .line 130
    new-instance v0, Lcom/google/c/a/a/a/c/a/c/i;

    const-string v1, "WAIT_FOR_CLOSE"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/c/a/a/a/c/a/c/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/c/i;->e:Lcom/google/c/a/a/a/c/a/c/i;

    .line 135
    new-instance v0, Lcom/google/c/a/a/a/c/a/c/i;

    const-string v1, "COMPLETED"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/c/a/a/a/c/a/c/i;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/a/c/a/c/i;->f:Lcom/google/c/a/a/a/c/a/c/i;

    .line 106
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/c/a/a/a/c/a/c/i;

    sget-object v1, Lcom/google/c/a/a/a/c/a/c/i;->a:Lcom/google/c/a/a/a/c/a/c/i;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/c/a/a/a/c/a/c/i;->b:Lcom/google/c/a/a/a/c/a/c/i;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/c/a/a/a/c/a/c/i;->c:Lcom/google/c/a/a/a/c/a/c/i;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/c/a/a/a/c/a/c/i;->d:Lcom/google/c/a/a/a/c/a/c/i;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/c/a/a/a/c/a/c/i;->e:Lcom/google/c/a/a/a/c/a/c/i;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/c/a/a/a/c/a/c/i;->f:Lcom/google/c/a/a/a/c/a/c/i;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/c/a/a/a/c/a/c/i;->h:[Lcom/google/c/a/a/a/c/a/c/i;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 140
    iput p3, p0, Lcom/google/c/a/a/a/c/a/c/i;->g:I

    .line 141
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/a/c/a/c/i;
    .locals 1

    .prologue
    .line 106
    const-class v0, Lcom/google/c/a/a/a/c/a/c/i;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/a/c/a/c/i;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/a/c/a/c/i;
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/google/c/a/a/a/c/a/c/i;->h:[Lcom/google/c/a/a/a/c/a/c/i;

    invoke-virtual {v0}, [Lcom/google/c/a/a/a/c/a/c/i;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/a/c/a/c/i;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 147
    iget v0, p0, Lcom/google/c/a/a/a/c/a/c/i;->g:I

    return v0
.end method
