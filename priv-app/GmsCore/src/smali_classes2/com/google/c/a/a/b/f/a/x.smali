.class public final enum Lcom/google/c/a/a/b/f/a/x;
.super Ljava/lang/Enum;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/f/a/k;


# static fields
.field public static final enum a:Lcom/google/c/a/a/b/f/a/x;

.field public static final enum b:Lcom/google/c/a/a/b/f/a/x;

.field public static final enum c:Lcom/google/c/a/a/b/f/a/x;

.field public static final enum d:Lcom/google/c/a/a/b/f/a/x;

.field public static final enum e:Lcom/google/c/a/a/b/f/a/x;

.field public static final enum f:Lcom/google/c/a/a/b/f/a/x;

.field private static final synthetic h:[Lcom/google/c/a/a/b/f/a/x;


# instance fields
.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 13
    new-instance v0, Lcom/google/c/a/a/b/f/a/x;

    const-string v1, "ID"

    invoke-direct {v0, v1, v4, v4}, Lcom/google/c/a/a/b/f/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/x;->a:Lcom/google/c/a/a/b/f/a/x;

    .line 14
    new-instance v0, Lcom/google/c/a/a/b/f/a/x;

    const-string v1, "REFERENCE_OBJECT_ID"

    invoke-direct {v0, v1, v5, v5}, Lcom/google/c/a/a/b/f/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/x;->b:Lcom/google/c/a/a/b/f/a/x;

    .line 15
    new-instance v0, Lcom/google/c/a/a/b/f/a/x;

    const-string v1, "INDEX"

    invoke-direct {v0, v1, v6, v6}, Lcom/google/c/a/a/b/f/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/x;->c:Lcom/google/c/a/a/b/f/a/x;

    .line 16
    new-instance v0, Lcom/google/c/a/a/b/f/a/x;

    const-string v1, "CAN_BE_DELETED"

    invoke-direct {v0, v1, v7, v7}, Lcom/google/c/a/a/b/f/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/x;->d:Lcom/google/c/a/a/b/f/a/x;

    .line 17
    new-instance v0, Lcom/google/c/a/a/b/f/a/x;

    const-string v1, "PREVIOUS_INDEX"

    invoke-direct {v0, v1, v8, v8}, Lcom/google/c/a/a/b/f/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/x;->e:Lcom/google/c/a/a/b/f/a/x;

    .line 18
    new-instance v0, Lcom/google/c/a/a/b/f/a/x;

    const-string v1, "PREVIOUS_OBJECT_ID"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/google/c/a/a/b/f/a/x;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/google/c/a/a/b/f/a/x;->f:Lcom/google/c/a/a/b/f/a/x;

    .line 12
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/google/c/a/a/b/f/a/x;

    sget-object v1, Lcom/google/c/a/a/b/f/a/x;->a:Lcom/google/c/a/a/b/f/a/x;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/c/a/a/b/f/a/x;->b:Lcom/google/c/a/a/b/f/a/x;

    aput-object v1, v0, v5

    sget-object v1, Lcom/google/c/a/a/b/f/a/x;->c:Lcom/google/c/a/a/b/f/a/x;

    aput-object v1, v0, v6

    sget-object v1, Lcom/google/c/a/a/b/f/a/x;->d:Lcom/google/c/a/a/b/f/a/x;

    aput-object v1, v0, v7

    sget-object v1, Lcom/google/c/a/a/b/f/a/x;->e:Lcom/google/c/a/a/b/f/a/x;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/google/c/a/a/b/f/a/x;->f:Lcom/google/c/a/a/b/f/a/x;

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/c/a/a/b/f/a/x;->h:[Lcom/google/c/a/a/b/f/a/x;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 23
    iput p3, p0, Lcom/google/c/a/a/b/f/a/x;->g:I

    .line 24
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/c/a/a/b/f/a/x;
    .locals 1

    .prologue
    .line 12
    const-class v0, Lcom/google/c/a/a/b/f/a/x;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/b/f/a/x;

    return-object v0
.end method

.method public static values()[Lcom/google/c/a/a/b/f/a/x;
    .locals 1

    .prologue
    .line 12
    sget-object v0, Lcom/google/c/a/a/b/f/a/x;->h:[Lcom/google/c/a/a/b/f/a/x;

    invoke-virtual {v0}, [Lcom/google/c/a/a/b/f/a/x;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/c/a/a/b/f/a/x;

    return-object v0
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/c/a/a/b/f/a/x;->g:I

    return v0
.end method
