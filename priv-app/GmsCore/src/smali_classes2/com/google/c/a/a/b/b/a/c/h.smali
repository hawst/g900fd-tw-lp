.class public Lcom/google/c/a/a/b/b/a/c/h;
.super Lcom/google/c/a/a/b/b/a/c/d;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:I

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:Z

.field private final g:Ljava/util/Set;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IZLjava/lang/String;I)V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/c/a/a/b/b/a/c/d;-><init>(Ljava/lang/String;)V

    .line 20
    iput-object p2, p0, Lcom/google/c/a/a/b/b/a/c/h;->a:Ljava/lang/String;

    .line 21
    iput p3, p0, Lcom/google/c/a/a/b/b/a/c/h;->b:I

    .line 22
    iput-boolean p4, p0, Lcom/google/c/a/a/b/b/a/c/h;->f:Z

    .line 23
    iput p6, p0, Lcom/google/c/a/a/b/b/a/c/h;->e:I

    .line 24
    if-nez p5, :cond_0

    const/4 v0, -0x1

    if-eq p6, v0, :cond_0

    .line 27
    iput-object p2, p0, Lcom/google/c/a/a/b/b/a/c/h;->d:Ljava/lang/String;

    .line 31
    :goto_0
    if-nez p2, :cond_1

    .line 32
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/c/h;->g:Ljava/util/Set;

    .line 36
    :goto_1
    return-void

    .line 29
    :cond_0
    iput-object p5, p0, Lcom/google/c/a/a/b/b/a/c/h;->d:Ljava/lang/String;

    goto :goto_0

    .line 34
    :cond_1
    invoke-static {p2}, Ljava/util/Collections;->singleton(Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/b/a/c/h;->g:Ljava/util/Set;

    goto :goto_1
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/h;->a:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/util/Set;
    .locals 1

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/h;->g:Ljava/util/Set;

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 50
    iget v0, p0, Lcom/google/c/a/a/b/b/a/c/h;->b:I

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 57
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/c/h;->f:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 90
    if-ne p0, p1, :cond_1

    .line 130
    :cond_0
    :goto_0
    return v0

    .line 93
    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    .line 94
    goto :goto_0

    .line 96
    :cond_2
    instance-of v2, p1, Lcom/google/c/a/a/b/b/a/c/h;

    if-nez v2, :cond_3

    move v0, v1

    .line 97
    goto :goto_0

    .line 99
    :cond_3
    check-cast p1, Lcom/google/c/a/a/b/b/a/c/h;

    .line 100
    iget-boolean v2, p0, Lcom/google/c/a/a/b/b/a/c/h;->f:Z

    iget-boolean v3, p1, Lcom/google/c/a/a/b/b/a/c/h;->f:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 101
    goto :goto_0

    .line 103
    :cond_4
    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/h;->g:Ljava/util/Set;

    if-nez v2, :cond_5

    .line 104
    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/c/h;->g:Ljava/util/Set;

    if-eqz v2, :cond_6

    move v0, v1

    .line 105
    goto :goto_0

    .line 107
    :cond_5
    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/h;->g:Ljava/util/Set;

    iget-object v3, p1, Lcom/google/c/a/a/b/b/a/c/h;->g:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 108
    goto :goto_0

    .line 110
    :cond_6
    iget v2, p0, Lcom/google/c/a/a/b/b/a/c/h;->b:I

    iget v3, p1, Lcom/google/c/a/a/b/b/a/c/h;->b:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 111
    goto :goto_0

    .line 113
    :cond_7
    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/h;->a:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 114
    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/c/h;->a:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 115
    goto :goto_0

    .line 117
    :cond_8
    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/h;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/c/a/a/b/b/a/c/h;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 118
    goto :goto_0

    .line 120
    :cond_9
    iget v2, p0, Lcom/google/c/a/a/b/b/a/c/h;->e:I

    iget v3, p1, Lcom/google/c/a/a/b/b/a/c/h;->e:I

    if-eq v2, v3, :cond_a

    move v0, v1

    .line 121
    goto :goto_0

    .line 123
    :cond_a
    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/h;->d:Ljava/lang/String;

    if-nez v2, :cond_b

    .line 124
    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/c/h;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 125
    goto :goto_0

    .line 127
    :cond_b
    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/h;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/c/a/a/b/b/a/c/h;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 128
    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/google/c/a/a/b/b/a/c/h;->e:I

    return v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/h;->d:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 77
    iget-boolean v0, p0, Lcom/google/c/a/a/b/b/a/c/h;->f:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/lit8 v0, v0, 0x1f

    .line 80
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/h;->g:Ljava/util/Set;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 81
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/c/a/a/b/b/a/c/h;->b:I

    add-int/2addr v0, v2

    .line 82
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/h;->a:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 83
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/c/a/a/b/b/a/c/h;->e:I

    add-int/2addr v0, v2

    .line 84
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/c/h;->d:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 85
    return v0

    .line 77
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 80
    :cond_1
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/h;->g:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->hashCode()I

    move-result v0

    goto :goto_1

    .line 82
    :cond_2
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/c/h;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 84
    :cond_3
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/c/h;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "IndexMutation [objectId="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/c/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", index="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/c/a/a/b/b/a/c/h;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", previousObjectId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/c/h;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", previousIndex="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/c/a/a/b/b/a/c/h;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", canBeDeleted="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/c/a/a/b/b/a/c/h;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", childReferences="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/c/h;->g:Ljava/util/Set;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
