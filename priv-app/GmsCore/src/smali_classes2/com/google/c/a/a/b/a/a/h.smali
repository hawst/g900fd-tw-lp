.class final Lcom/google/c/a/a/b/a/a/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/e/c;


# instance fields
.field a:Lcom/google/c/a/a/b/e/i;

.field final synthetic b:Lcom/google/c/a/a/b/a/a/a;

.field private final c:Lcom/google/c/a/a/b/a/a/g;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/a/a/a;Lcom/google/c/a/a/b/a/a/g;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/c/a/a/b/a/a/h;->b:Lcom/google/c/a/a/b/a/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 198
    iput-object p2, p0, Lcom/google/c/a/a/b/a/a/h;->c:Lcom/google/c/a/a/b/a/a/g;

    .line 199
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 209
    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/b/a/a/h;->a:Lcom/google/c/a/a/b/e/i;

    if-eqz v0, :cond_1

    .line 210
    :goto_0
    iget-object v0, p0, Lcom/google/c/a/a/b/a/a/h;->a:Lcom/google/c/a/a/b/e/i;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/e/i;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/google/c/a/a/b/a/a/h;->a:Lcom/google/c/a/a/b/e/i;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/e/i;->a()Lcom/google/c/a/a/b/b/a/a;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 218
    :catch_0
    move-exception v0

    .line 219
    iget-object v1, p0, Lcom/google/c/a/a/b/a/a/h;->b:Lcom/google/c/a/a/b/a/a/a;

    invoke-static {v1}, Lcom/google/c/a/a/b/a/a/a;->a(Lcom/google/c/a/a/b/a/a/a;)Ljava/util/logging/Logger;

    move-result-object v1

    sget-object v2, Ljava/util/logging/Level;->WARNING:Ljava/util/logging/Level;

    const-string v3, "Failed to apply mutation"

    invoke-virtual {v1, v2, v3, v0}, Ljava/util/logging/Logger;->log(Ljava/util/logging/Level;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 220
    iget-object v0, p0, Lcom/google/c/a/a/b/a/a/h;->a:Lcom/google/c/a/a/b/e/i;

    sget-object v1, Lcom/google/c/a/a/b/e/g;->b:Lcom/google/c/a/a/b/e/g;

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/e/i;->a(Lcom/google/c/a/a/b/e/g;)V

    .line 221
    iget-object v0, p0, Lcom/google/c/a/a/b/a/a/h;->c:Lcom/google/c/a/a/b/a/a/g;

    new-instance v1, Lcom/google/c/a/a/b/e/o;

    const/16 v2, 0x1f4

    invoke-direct {v1, v2}, Lcom/google/c/a/a/b/e/o;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/c/a/a/b/a/a/g;->a(Lcom/google/c/a/a/b/e/o;)V

    .line 223
    :cond_0
    :goto_1
    return-void

    .line 214
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/c/a/a/b/a/a/h;->a:Lcom/google/c/a/a/b/e/i;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/e/i;->b()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/c/a/a/b/a/a/h;->a:Lcom/google/c/a/a/b/e/i;

    invoke-virtual {v0}, Lcom/google/c/a/a/b/e/i;->c()Z

    move-result v0

    if-nez v0, :cond_0

    .line 215
    iget-object v0, p0, Lcom/google/c/a/a/b/a/a/h;->a:Lcom/google/c/a/a/b/e/i;

    sget-object v1, Lcom/google/c/a/a/b/e/g;->b:Lcom/google/c/a/a/b/e/g;

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/e/i;->a(Lcom/google/c/a/a/b/e/g;)V

    .line 216
    iget-object v0, p0, Lcom/google/c/a/a/b/a/a/h;->c:Lcom/google/c/a/a/b/a/a/g;

    invoke-interface {v0}, Lcom/google/c/a/a/b/a/a/g;->a()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method
