.class public final Lcom/google/c/a/a/a/c/a/d/s;
.super Lcom/google/c/a/a/a/c/a/d/a;
.source "SourceFile"


# static fields
.field private static final d:Ljava/nio/charset/Charset;

.field private static final e:Lcom/google/c/a/a/a/c/a/d/w;


# instance fields
.field private f:Ljava/net/HttpURLConnection;

.field private final g:Lcom/google/c/a/a/a/c/a/d/p;

.field private final h:Ljava/util/concurrent/ExecutorService;

.field private final i:Lcom/google/c/a/a/a/c/a/d/w;

.field private final j:Z

.field private final k:Ljava/util/concurrent/CountDownLatch;

.field private l:Ljava/util/concurrent/Future;

.field private m:Ljava/util/concurrent/Future;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const-string v0, "utf-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/google/c/a/a/a/c/a/d/s;->d:Ljava/nio/charset/Charset;

    .line 35
    new-instance v0, Lcom/google/c/a/a/a/c/a/d/t;

    invoke-direct {v0}, Lcom/google/c/a/a/a/c/a/d/t;-><init>()V

    sput-object v0, Lcom/google/c/a/a/a/c/a/d/s;->e:Lcom/google/c/a/a/a/c/a/d/w;

    return-void
.end method

.method private constructor <init>(Lcom/google/c/a/a/a/c/a/d/p;Ljava/util/concurrent/ExecutorService;Lcom/google/c/a/a/a/c/a/d/w;Z)V
    .locals 2

    .prologue
    .line 81
    invoke-direct {p0}, Lcom/google/c/a/a/a/c/a/d/a;-><init>()V

    .line 47
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/d/s;->k:Ljava/util/concurrent/CountDownLatch;

    .line 82
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/d/s;->g:Lcom/google/c/a/a/a/c/a/d/p;

    .line 83
    iput-object p2, p0, Lcom/google/c/a/a/a/c/a/d/s;->h:Ljava/util/concurrent/ExecutorService;

    .line 84
    iput-object p3, p0, Lcom/google/c/a/a/a/c/a/d/s;->i:Lcom/google/c/a/a/a/c/a/d/w;

    .line 85
    iput-boolean p4, p0, Lcom/google/c/a/a/a/c/a/d/s;->j:Z

    .line 86
    return-void
.end method

.method public constructor <init>(Lcom/google/c/a/a/a/c/a/d/p;Ljava/util/concurrent/ExecutorService;Z)V
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/google/c/a/a/a/c/a/d/s;->e:Lcom/google/c/a/a/a/c/a/d/w;

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/c/a/a/a/c/a/d/s;-><init>(Lcom/google/c/a/a/a/c/a/d/p;Ljava/util/concurrent/ExecutorService;Lcom/google/c/a/a/a/c/a/d/w;Z)V

    .line 69
    return-void
.end method

.method static synthetic a(Lcom/google/c/a/a/a/c/a/d/s;)Lcom/google/c/a/a/a/c/a/d/p;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/s;->g:Lcom/google/c/a/a/a/c/a/d/p;

    return-object v0
.end method

.method private static a(Ljava/net/HttpURLConnection;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 129
    invoke-virtual {p0}, Ljava/net/HttpURLConnection;->getErrorStream()Ljava/io/InputStream;

    move-result-object v0

    .line 130
    if-nez v0, :cond_0

    .line 131
    const-string v0, ""

    .line 152
    :goto_0
    return-object v0

    .line 133
    :cond_0
    const/16 v1, 0x7d0

    invoke-static {v1}, Ljava/nio/CharBuffer;->allocate(I)Ljava/nio/CharBuffer;

    move-result-object v1

    .line 134
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    .line 135
    new-instance v3, Ljava/io/BufferedReader;

    new-instance v4, Ljava/io/InputStreamReader;

    sget-object v5, Lcom/google/c/a/a/a/c/a/d/s;->d:Ljava/nio/charset/Charset;

    invoke-direct {v4, v0, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;Ljava/nio/charset/Charset;)V

    invoke-direct {v3, v4}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    .line 137
    :try_start_0
    invoke-virtual {v3, v1}, Ljava/io/BufferedReader;->read(Ljava/nio/CharBuffer;)I

    move-result v0

    .line 138
    :goto_1
    if-lez v0, :cond_1

    .line 139
    invoke-virtual {v1}, Ljava/nio/CharBuffer;->flip()Ljava/nio/Buffer;

    .line 140
    invoke-virtual {v1}, Ljava/nio/CharBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    invoke-virtual {v1}, Ljava/nio/CharBuffer;->clear()Ljava/nio/Buffer;

    .line 142
    invoke-virtual {v3, v1}, Ljava/io/BufferedReader;->read(Ljava/nio/CharBuffer;)I

    move-result v0

    goto :goto_1

    .line 144
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 149
    :try_start_1
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    .line 146
    :catch_1
    move-exception v0

    const-string v0, ""

    .line 149
    :try_start_2
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v1

    goto :goto_0

    .line 148
    :catchall_0
    move-exception v0

    .line 149
    :try_start_3
    invoke-virtual {v3}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 152
    :goto_2
    throw v0

    :catch_3
    move-exception v1

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/c/a/a/a/c/a/d/s;Ljava/net/HttpURLConnection;)Ljava/net/HttpURLConnection;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/d/s;->f:Ljava/net/HttpURLConnection;

    return-object p1
.end method

.method static synthetic a(Lcom/google/c/a/a/a/c/a/d/s;Ljava/util/concurrent/Future;)Ljava/util/concurrent/Future;
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/d/s;->m:Ljava/util/concurrent/Future;

    return-object p1
.end method

.method static synthetic a(Lcom/google/c/a/a/a/c/a/d/s;Ljava/io/IOException;)V
    .locals 4

    .prologue
    .line 21
    const/4 v1, 0x0

    const-string v0, ""

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/s;->f:Ljava/net/HttpURLConnection;

    if-eqz v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/s;->f:Ljava/net/HttpURLConnection;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/d/s;->f:Ljava/net/HttpURLConnection;

    invoke-static {v2}, Lcom/google/c/a/a/a/c/a/d/s;->a(Ljava/net/HttpURLConnection;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/c/a/a/a/c/a/d/e;

    iget-object v3, p0, Lcom/google/c/a/a/a/c/a/d/s;->f:Ljava/net/HttpURLConnection;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Lcom/google/c/a/a/a/c/a/d/e;-><init>(ILjava/util/Map;)V

    invoke-virtual {p0, v2}, Lcom/google/c/a/a/a/c/a/d/s;->a(Lcom/google/c/a/a/a/c/a/d/e;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    new-instance v2, Lcom/google/c/a/a/a/c/a/d/d;

    new-instance v3, Lcom/google/c/a/a/a/c/a/d/j;

    invoke-direct {v3, p1, v1, v0}, Lcom/google/c/a/a/a/c/a/d/j;-><init>(Ljava/io/IOException;ILjava/lang/String;)V

    invoke-direct {v2, v3}, Lcom/google/c/a/a/a/c/a/d/d;-><init>(Lcom/google/c/a/a/a/c/a/d/j;)V

    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/a;->c:Lcom/google/c/a/a/a/c/a/d/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/a;->c:Lcom/google/c/a/a/a/c/a/d/h;

    invoke-interface {v0, v2}, Lcom/google/c/a/a/a/c/a/d/h;->a(Lcom/google/c/a/a/a/c/a/d/d;)V

    :cond_1
    invoke-virtual {p0}, Lcom/google/c/a/a/a/c/a/d/a;->b()V

    return-void

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/c/a/a/a/c/a/d/s;)Lcom/google/c/a/a/a/c/a/d/w;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/s;->i:Lcom/google/c/a/a/a/c/a/d/w;

    return-object v0
.end method

.method static synthetic c(Lcom/google/c/a/a/a/c/a/d/s;)Ljava/net/HttpURLConnection;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/s;->f:Ljava/net/HttpURLConnection;

    return-object v0
.end method

.method static synthetic d(Lcom/google/c/a/a/a/c/a/d/s;)Ljava/util/concurrent/ExecutorService;
    .locals 1

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/s;->h:Ljava/util/concurrent/ExecutorService;

    return-object v0
.end method

.method static synthetic f()Ljava/nio/charset/Charset;
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/google/c/a/a/a/c/a/d/s;->d:Ljava/nio/charset/Charset;

    return-object v0
.end method


# virtual methods
.method public final d()V
    .locals 3

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/s;->h:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/google/c/a/a/a/c/a/d/u;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/c/a/a/a/c/a/d/u;-><init>(Lcom/google/c/a/a/a/c/a/d/s;B)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/d/s;->l:Ljava/util/concurrent/Future;

    .line 91
    iget-boolean v0, p0, Lcom/google/c/a/a/a/c/a/d/s;->j:Z

    if-eqz v0, :cond_0

    .line 93
    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/s;->k:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected final e()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 102
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/s;->m:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/s;->m:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 105
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/s;->l:Ljava/util/concurrent/Future;

    if-eqz v0, :cond_1

    .line 106
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/s;->l:Ljava/util/concurrent/Future;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Future;->cancel(Z)Z

    .line 108
    :cond_1
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/d/s;->k:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 109
    return-void
.end method
