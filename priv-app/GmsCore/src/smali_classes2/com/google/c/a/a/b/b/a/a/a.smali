.class public abstract Lcom/google/c/a/a/b/b/a/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/c/a/a/b/b/a/g;

.field public final b:Z

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Lcom/google/c/a/a/b/b/a/k;

.field public final f:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/c/a/a/b/b/a/g;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;ZLcom/google/c/a/a/b/b/a/k;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    iput-object p1, p0, Lcom/google/c/a/a/b/b/a/a/a;->a:Lcom/google/c/a/a/b/b/a/g;

    .line 46
    iput-object p2, p0, Lcom/google/c/a/a/b/b/a/a/a;->c:Ljava/lang/String;

    .line 47
    iput-boolean p5, p0, Lcom/google/c/a/a/b/b/a/a/a;->b:Z

    .line 48
    iput-object p3, p0, Lcom/google/c/a/a/b/b/a/a/a;->d:Ljava/lang/String;

    .line 49
    iput-object p4, p0, Lcom/google/c/a/a/b/b/a/a/a;->f:Ljava/util/List;

    .line 50
    iput-object p6, p0, Lcom/google/c/a/a/b/b/a/a/a;->e:Lcom/google/c/a/a/b/b/a/k;

    .line 51
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 112
    if-ne p0, p1, :cond_0

    .line 122
    :goto_0
    return v0

    .line 115
    :cond_0
    if-nez p1, :cond_1

    move v0, v1

    .line 116
    goto :goto_0

    .line 118
    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_2

    move v0, v1

    .line 119
    goto :goto_0

    .line 121
    :cond_2
    check-cast p1, Lcom/google/c/a/a/b/b/a/a/a;

    .line 122
    const/16 v2, 0xc

    new-array v2, v2, [Ljava/lang/Object;

    iget-boolean v3, p0, Lcom/google/c/a/a/b/b/a/a/a;->b:Z

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    aput-object v3, v2, v1

    iget-boolean v1, p1, Lcom/google/c/a/a/b/b/a/a/a;->b:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v2, v0

    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/a/a;->c:Ljava/lang/String;

    aput-object v1, v2, v0

    const/4 v0, 0x3

    iget-object v1, p1, Lcom/google/c/a/a/b/b/a/a/a;->c:Ljava/lang/String;

    aput-object v1, v2, v0

    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/a/a;->d:Ljava/lang/String;

    aput-object v1, v2, v0

    const/4 v0, 0x5

    iget-object v1, p1, Lcom/google/c/a/a/b/b/a/a/a;->d:Ljava/lang/String;

    aput-object v1, v2, v0

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/a/a;->f:Ljava/util/List;

    aput-object v1, v2, v0

    const/4 v0, 0x7

    iget-object v1, p1, Lcom/google/c/a/a/b/b/a/a/a;->f:Ljava/util/List;

    aput-object v1, v2, v0

    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/a/a;->e:Lcom/google/c/a/a/b/b/a/k;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/k;->b()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v2, v0

    const/16 v0, 0x9

    iget-object v1, p1, Lcom/google/c/a/a/b/b/a/a/a;->e:Lcom/google/c/a/a/b/b/a/k;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/k;->b()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v2, v0

    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/a/a;->e:Lcom/google/c/a/a/b/b/a/k;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/k;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v2, v0

    const/16 v0, 0xb

    iget-object v1, p1, Lcom/google/c/a/a/b/b/a/a/a;->e:Lcom/google/c/a/a/b/b/a/k;

    invoke-virtual {v1}, Lcom/google/c/a/a/b/b/a/k;->a()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v2, v0

    invoke-static {v2}, Lcom/google/c/a/a/b/h/b;->a([Ljava/lang/Object;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 106
    const/4 v0, 0x5

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-boolean v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->e:Lcom/google/c/a/a/b/b/a/k;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/c/a/a/b/b/a/a/a;->f:Ljava/util/List;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method
