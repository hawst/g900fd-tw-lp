.class final Lcom/google/c/a/a/a/c/a/a/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/logging/Logger;


# instance fields
.field private final b:Lcom/google/c/a/a/b/d/b;

.field private c:Lcom/google/c/a/a/a/c/a/a/q;

.field private d:Ljava/lang/StringBuilder;

.field private e:I

.field private final f:Ljava/util/Queue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-string v0, "BrowserChannel"

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    sput-object v0, Lcom/google/c/a/a/a/c/a/a/p;->a:Ljava/util/logging/Logger;

    return-void
.end method

.method public constructor <init>(Lcom/google/c/a/a/b/d/b;)V
    .locals 2

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    sget-object v0, Lcom/google/c/a/a/a/c/a/a/q;->a:Lcom/google/c/a/a/a/c/a/a/q;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/a/p;->c:Lcom/google/c/a/a/a/c/a/a/q;

    .line 29
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/a/p;->d:Ljava/lang/StringBuilder;

    .line 31
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/a/c/a/a/p;->f:Ljava/util/Queue;

    .line 34
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/a/p;->b:Lcom/google/c/a/a/b/d/b;

    .line 35
    if-nez p1, :cond_0

    .line 36
    new-instance v0, Ljava/lang/NullPointerException;

    const-string v1, "jsonDriver"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 38
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()Lcom/google/c/a/a/a/c/a/a/o;
    .locals 1

    .prologue
    .line 99
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/a/p;->f:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/c/a/a/a/c/a/a/o;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/CharSequence;II)V
    .locals 6

    .prologue
    .line 51
    monitor-enter p0

    move v0, p2

    :goto_0
    if-ge v0, p3, :cond_5

    .line 52
    :try_start_0
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/p;->c:Lcom/google/c/a/a/a/c/a/a/q;

    sget-object v2, Lcom/google/c/a/a/a/c/a/a/q;->a:Lcom/google/c/a/a/a/c/a/a/q;

    if-ne v1, v2, :cond_3

    .line 53
    invoke-interface {p1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    .line 54
    const/16 v2, 0xa

    if-ne v1, v2, :cond_2

    .line 55
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/p;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 57
    :try_start_1
    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    iput v2, p0, Lcom/google/c/a/a/a/c/a/a/p;->e:I

    .line 58
    iget v2, p0, Lcom/google/c/a/a/a/c/a/a/p;->e:I

    if-lez v2, :cond_1

    .line 59
    sget-object v2, Lcom/google/c/a/a/a/c/a/a/q;->b:Lcom/google/c/a/a/a/c/a/a/q;

    iput-object v2, p0, Lcom/google/c/a/a/a/c/a/a/p;->c:Lcom/google/c/a/a/a/c/a/a/q;
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 66
    :goto_1
    :try_start_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/google/c/a/a/a/c/a/a/p;->d:Ljava/lang/StringBuilder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 51
    :cond_0
    :goto_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 61
    :cond_1
    :try_start_3
    sget-object v2, Lcom/google/c/a/a/a/c/a/a/p;->a:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignored invalid length: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/c/a/a/a/c/a/a/p;->e:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V
    :try_end_3
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 64
    :catch_0
    move-exception v2

    :try_start_4
    sget-object v2, Lcom/google/c/a/a/a/c/a/a/p;->a:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignored non-numeric length value: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1

    .line 51
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 68
    :cond_2
    :try_start_5
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/a/p;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 70
    :cond_3
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/p;->c:Lcom/google/c/a/a/a/c/a/a/q;

    sget-object v2, Lcom/google/c/a/a/a/c/a/a/q;->b:Lcom/google/c/a/a/a/c/a/a/q;

    if-ne v1, v2, :cond_0

    .line 71
    iget v1, p0, Lcom/google/c/a/a/a/c/a/a/p;->e:I

    add-int/2addr v1, v0

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/a/p;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 72
    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/a/p;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1, v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/CharSequence;II)Ljava/lang/StringBuilder;

    .line 73
    add-int/lit8 v0, v1, -0x1

    .line 74
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/p;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    iget v2, p0, Lcom/google/c/a/a/a/c/a/a/p;->e:I
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-ne v1, v2, :cond_0

    .line 76
    :try_start_6
    iget-object v1, p0, Lcom/google/c/a/a/a/c/a/a/p;->b:Lcom/google/c/a/a/b/d/b;

    iget-object v2, p0, Lcom/google/c/a/a/a/c/a/a/p;->d:Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/c/a/a/b/d/b;->b(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    .line 77
    const/4 v1, 0x0

    :goto_3
    iget-object v3, p0, Lcom/google/c/a/a/a/c/a/a/p;->b:Lcom/google/c/a/a/b/d/b;

    invoke-interface {v3, v2}, Lcom/google/c/a/a/b/d/b;->e(Ljava/lang/Object;)I

    move-result v3

    if-ge v1, v3, :cond_4

    .line 78
    iget-object v3, p0, Lcom/google/c/a/a/a/c/a/a/p;->f:Ljava/util/Queue;

    iget-object v4, p0, Lcom/google/c/a/a/a/c/a/a/p;->b:Lcom/google/c/a/a/b/d/b;

    iget-object v5, p0, Lcom/google/c/a/a/a/c/a/a/p;->b:Lcom/google/c/a/a/b/d/b;

    invoke-interface {v5, v2, v1}, Lcom/google/c/a/a/b/d/b;->a(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/c/a/a/a/c/a/a/o;->a(Lcom/google/c/a/a/b/d/b;Ljava/lang/Object;)Lcom/google/c/a/a/a/c/a/a/o;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_6
    .catch Ljava/lang/IllegalArgumentException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Ljava/lang/IllegalStateException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lcom/google/c/a/a/b/d/a; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 77
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 80
    :catch_1
    move-exception v1

    .line 81
    :try_start_7
    sget-object v2, Lcom/google/c/a/a/a/c/a/a/p;->a:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignored invalid server message: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    .line 87
    :cond_4
    :goto_4
    sget-object v1, Lcom/google/c/a/a/a/c/a/a/q;->a:Lcom/google/c/a/a/a/c/a/a/q;

    iput-object v1, p0, Lcom/google/c/a/a/a/c/a/a/p;->c:Lcom/google/c/a/a/a/c/a/a/q;

    .line 88
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v1, p0, Lcom/google/c/a/a/a/c/a/a/p;->d:Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 82
    :catch_2
    move-exception v1

    .line 83
    sget-object v2, Lcom/google/c/a/a/a/c/a/a/p;->a:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignored invalid server message: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V

    goto :goto_4

    .line 84
    :catch_3
    move-exception v1

    .line 85
    sget-object v2, Lcom/google/c/a/a/a/c/a/a/p;->a:Ljava/util/logging/Logger;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Ignored invalid server message: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/logging/Logger;->warning(Ljava/lang/String;)V
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_4

    .line 92
    :cond_5
    monitor-exit p0

    return-void
.end method
