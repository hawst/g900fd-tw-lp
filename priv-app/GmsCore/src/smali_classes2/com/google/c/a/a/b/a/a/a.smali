.class public Lcom/google/c/a/a/b/a/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/logging/Logger;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const-class v0, Lcom/google/c/a/a/b/a/a/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/logging/Logger;->getLogger(Ljava/lang/String;)Ljava/util/logging/Logger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/b/a/a/a;->a:Ljava/util/logging/Logger;

    .line 230
    return-void
.end method

.method static synthetic a(Lcom/google/c/a/a/b/a/a/a;)Ljava/util/logging/Logger;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/c/a/a/b/a/a/a;->a:Ljava/util/logging/Logger;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/c/a/a/b/a/f;Lcom/google/c/a/a/b/a/e;Lcom/google/c/a/a/b/a/a/f;Lcom/google/c/a/a/b/a/a/g;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 93
    iget-object v0, p1, Lcom/google/c/a/a/b/a/f;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    move v7, v0

    .line 95
    :goto_0
    new-instance v4, Lcom/google/c/a/a/b/a/a/h;

    invoke-direct {v4, p0, p4}, Lcom/google/c/a/a/b/a/a/h;-><init>(Lcom/google/c/a/a/b/a/a/a;Lcom/google/c/a/a/b/a/a/g;)V

    new-instance v1, Lcom/google/c/a/a/a/c/j;

    iget-object v0, p3, Lcom/google/c/a/a/b/a/a/f;->a:Lcom/google/c/a/a/a/c/i;

    iget-object v2, p1, Lcom/google/c/a/a/b/a/f;->a:Ljava/lang/String;

    iget-object v3, p3, Lcom/google/c/a/a/b/a/a/f;->c:Lcom/google/c/a/a/a/c/a/e/b;

    invoke-direct {v1, v0, v2, v3}, Lcom/google/c/a/a/a/c/j;-><init>(Lcom/google/c/a/a/a/c/i;Ljava/lang/String;Lcom/google/c/a/a/a/c/a/e/b;)V

    new-instance v0, Lcom/google/c/a/a/b/e/i;

    iget-object v2, p3, Lcom/google/c/a/a/b/a/a/f;->b:Lcom/google/c/a/a/b/e/n;

    new-instance v3, Lcom/google/c/a/a/b/a/a/e;

    invoke-direct {v3, v8}, Lcom/google/c/a/a/b/a/a/e;-><init>(B)V

    iget-object v5, p1, Lcom/google/c/a/a/b/a/f;->c:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/google/c/a/a/b/e/i;-><init>(Lcom/google/c/a/a/b/e/e;Lcom/google/c/a/a/b/e/n;Lcom/google/c/a/a/b/b/a/b;Lcom/google/c/a/a/b/e/c;Ljava/lang/String;Lcom/google/c/a/a/b/a/e;)V

    iput-object v0, v4, Lcom/google/c/a/a/b/a/a/h;->a:Lcom/google/c/a/a/b/e/i;

    .line 98
    iget v1, p1, Lcom/google/c/a/a/b/a/f;->b:I

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/e/i;->a(I)V

    .line 101
    if-eqz v7, :cond_0

    .line 102
    new-instance v1, Lcom/google/c/a/a/b/a/a/b;

    invoke-direct {v1, p0, v0, p4}, Lcom/google/c/a/a/b/a/a/b;-><init>(Lcom/google/c/a/a/b/a/a/a;Lcom/google/c/a/a/b/e/i;Lcom/google/c/a/a/b/a/a/g;)V

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/e/i;->a(Lcom/google/c/a/a/b/e/d;)V

    .line 112
    :cond_0
    invoke-virtual {v0, p1, p2, v8}, Lcom/google/c/a/a/b/e/i;->a(Lcom/google/c/a/a/b/a/f;Lcom/google/c/a/a/b/a/e;Z)V

    .line 116
    if-nez v7, :cond_1

    .line 117
    new-instance v1, Lcom/google/c/a/a/b/a/a/c;

    invoke-direct {v1, p0, v0, p4}, Lcom/google/c/a/a/b/a/a/c;-><init>(Lcom/google/c/a/a/b/a/a/a;Lcom/google/c/a/a/b/e/i;Lcom/google/c/a/a/b/a/a/g;)V

    invoke-virtual {v0, v1}, Lcom/google/c/a/a/b/e/i;->a(Lcom/google/c/a/a/b/e/a;)V

    .line 136
    :cond_1
    return-void

    :cond_2
    move v7, v8

    .line 93
    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/c/a/a/b/a/e;Lcom/google/c/a/a/b/a/a/f;Lcom/google/c/a/a/b/a/a/g;)V
    .locals 3

    .prologue
    .line 150
    new-instance v0, Lcom/google/c/a/a/a/c/j;

    iget-object v1, p3, Lcom/google/c/a/a/b/a/a/f;->a:Lcom/google/c/a/a/a/c/i;

    iget-object v2, p3, Lcom/google/c/a/a/b/a/a/f;->c:Lcom/google/c/a/a/a/c/a/e/b;

    invoke-direct {v0, v1, p1, v2}, Lcom/google/c/a/a/a/c/j;-><init>(Lcom/google/c/a/a/a/c/i;Ljava/lang/String;Lcom/google/c/a/a/a/c/a/e/b;)V

    .line 154
    new-instance v1, Lcom/google/c/a/a/b/a/a/d;

    invoke-direct {v1, p0, p2, p4}, Lcom/google/c/a/a/b/a/a/d;-><init>(Lcom/google/c/a/a/b/a/a/a;Lcom/google/c/a/a/b/a/e;Lcom/google/c/a/a/b/a/a/g;)V

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/c/a/a/b/e/e;->a(Lcom/google/c/a/a/b/e/f;Z)V

    .line 173
    return-void
.end method
