.class public abstract Lcom/google/c/a/a/a/c/a/c/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/a/c/a/c/b;


# instance fields
.field private a:Lcom/google/c/a/a/a/c/a/c/c;

.field private b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/c/a/a/a/c/a/c/a;->b:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/c/a/a/a/c/a/c/c;)V
    .locals 0

    .prologue
    .line 12
    iput-object p1, p0, Lcom/google/c/a/a/a/c/a/c/a;->a:Lcom/google/c/a/a/a/c/a/c/c;

    .line 13
    return-void
.end method

.method protected final declared-synchronized a(Lcom/google/c/a/a/a/c/a/c/d;)V
    .locals 1

    .prologue
    .line 21
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/c/a/a/a/c/a/c/a;->b:Z

    if-nez v0, :cond_0

    .line 22
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/c/a/a/a/c/a/c/a;->b:Z

    .line 23
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/a;->a:Lcom/google/c/a/a/a/c/a/c/c;

    if-eqz v0, :cond_0

    .line 24
    iget-object v0, p0, Lcom/google/c/a/a/a/c/a/c/a;->a:Lcom/google/c/a/a/a/c/a/c/c;

    invoke-interface {v0, p1}, Lcom/google/c/a/a/a/c/a/c/c;->a(Lcom/google/c/a/a/a/c/a/c/d;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 27
    :cond_0
    monitor-exit p0

    return-void

    .line 21
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
