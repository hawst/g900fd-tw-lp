.class public final Lcom/google/c/a/a/a/c/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/c/a/a/b/e/e;


# static fields
.field private static final a:Lcom/google/c/a/a/b/e/f;


# instance fields
.field private final b:Lcom/google/c/a/a/a/c/a/e/c;

.field private final c:Lcom/google/c/a/a/a/c/a/e/a;

.field private final d:Lcom/google/c/a/a/a/c/i;

.field private final e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private volatile h:Z

.field private volatile i:Z

.field private j:Lcom/google/c/a/a/b/e/h;

.field private final k:Ljava/util/List;

.field private l:Lcom/google/c/a/a/b/e/f;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Lcom/google/c/a/a/a/c/k;

    invoke-direct {v0}, Lcom/google/c/a/a/a/c/k;-><init>()V

    sput-object v0, Lcom/google/c/a/a/a/c/j;->a:Lcom/google/c/a/a/b/e/f;

    return-void
.end method

.method public constructor <init>(Lcom/google/c/a/a/a/c/i;Ljava/lang/String;Lcom/google/c/a/a/a/c/a/e/b;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Lcom/google/c/a/a/a/c/a/e/j;

    invoke-direct {v0}, Lcom/google/c/a/a/a/c/a/e/j;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/a/c/j;->b:Lcom/google/c/a/a/a/c/a/e/c;

    .line 50
    iput-boolean v1, p0, Lcom/google/c/a/a/a/c/j;->h:Z

    .line 51
    iput-boolean v1, p0, Lcom/google/c/a/a/a/c/j;->i:Z

    .line 53
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/c/a/a/a/c/j;->k:Ljava/util/List;

    .line 58
    iput-object p1, p0, Lcom/google/c/a/a/a/c/j;->d:Lcom/google/c/a/a/a/c/i;

    .line 59
    iput-object p2, p0, Lcom/google/c/a/a/a/c/j;->e:Ljava/lang/String;

    .line 60
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->b:Lcom/google/c/a/a/a/c/a/e/c;

    invoke-interface {p3, v0}, Lcom/google/c/a/a/a/c/a/e/b;->a(Lcom/google/c/a/a/a/c/a/e/c;)Lcom/google/c/a/a/a/c/a/e/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/c/a/a/a/c/j;->c:Lcom/google/c/a/a/a/c/a/e/a;

    .line 62
    const-class v0, Lcom/google/c/a/a/b/c/c;

    new-instance v1, Lcom/google/c/a/a/a/c/l;

    invoke-direct {v1, p0}, Lcom/google/c/a/a/a/c/l;-><init>(Lcom/google/c/a/a/a/c/j;)V

    invoke-interface {p1, v0, v1}, Lcom/google/c/a/a/a/c/i;->a(Ljava/lang/Class;Lcom/google/c/a/a/a/c/r;)V

    .line 69
    new-instance v0, Lcom/google/c/a/a/a/c/m;

    invoke-direct {v0, p0}, Lcom/google/c/a/a/a/c/m;-><init>(Lcom/google/c/a/a/a/c/j;)V

    invoke-interface {p1, v0}, Lcom/google/c/a/a/a/c/i;->a(Lcom/google/c/a/a/a/c/a/g;)V

    .line 79
    return-void
.end method

.method static synthetic a(Lcom/google/c/a/a/a/c/j;)Lcom/google/c/a/a/b/e/h;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->j:Lcom/google/c/a/a/b/e/h;

    return-object v0
.end method

.method static synthetic a(Lcom/google/c/a/a/a/c/j;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/c/a/a/a/c/j;->f:Ljava/lang/String;

    return-object p1
.end method

.method private a(Z)Ljava/util/Map;
    .locals 3

    .prologue
    .line 184
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 185
    const-string v1, "id"

    iget-object v2, p0, Lcom/google/c/a/a/a/c/j;->f:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    if-eqz p1, :cond_0

    .line 187
    const-string v1, "sid"

    iget-object v2, p0, Lcom/google/c/a/a/a/c/j;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    :cond_0
    return-object v0
.end method

.method static synthetic a(Lcom/google/c/a/a/a/c/j;Lcom/google/c/a/a/b/c/c;Z)V
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->j:Lcom/google/c/a/a/b/e/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->j:Lcom/google/c/a/a/b/e/h;

    invoke-virtual {p1}, Lcom/google/c/a/a/b/c/c;->a()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1, p2}, Lcom/google/c/a/a/b/e/h;->a(Ljava/util/Collection;Z)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/c/a/a/a/c/j;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/c/a/a/a/c/j;->g:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/google/c/a/a/a/c/j;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/c/a/a/a/c/j;->a(Z)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 247
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 248
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    .line 250
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 251
    return-void
.end method

.method static synthetic c(Lcom/google/c/a/a/a/c/j;)Z
    .locals 1

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/google/c/a/a/a/c/j;->h:Z

    return v0
.end method

.method static synthetic d(Lcom/google/c/a/a/a/c/j;)Lcom/google/c/a/a/a/c/a/e/a;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->c:Lcom/google/c/a/a/a/c/a/e/a;

    return-object v0
.end method

.method static synthetic e(Lcom/google/c/a/a/a/c/j;)Lcom/google/c/a/a/b/e/f;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->l:Lcom/google/c/a/a/b/e/f;

    return-object v0
.end method

.method static synthetic f(Lcom/google/c/a/a/a/c/j;)Lcom/google/c/a/a/a/c/a/e/c;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->b:Lcom/google/c/a/a/a/c/a/e/c;

    return-object v0
.end method

.method static synthetic g(Lcom/google/c/a/a/a/c/j;)Lcom/google/c/a/a/a/c/i;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->d:Lcom/google/c/a/a/a/c/i;

    return-object v0
.end method

.method static synthetic h(Lcom/google/c/a/a/a/c/j;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/google/c/a/a/a/c/j;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->g:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 7

    .prologue
    .line 227
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/c/a/a/a/c/j;->i:Z

    if-nez v0, :cond_0

    .line 228
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/c/a/a/a/c/j;->i:Z

    .line 229
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->d:Lcom/google/c/a/a/a/c/i;

    invoke-interface {v0}, Lcom/google/c/a/a/a/c/i;->b()V

    .line 230
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->d:Lcom/google/c/a/a/a/c/i;

    const-string v1, "leave"

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/c/a/a/a/c/j;->a(Z)Ljava/util/Map;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-object v5, Lcom/google/c/a/a/a/c/j;->a:Lcom/google/c/a/a/b/e/f;

    const-class v6, Ljava/lang/Void;

    invoke-interface/range {v0 .. v6}, Lcom/google/c/a/a/a/c/i;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Object;ZLcom/google/c/a/a/b/e/f;Ljava/lang/Class;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 235
    :cond_0
    monitor-exit p0

    return-void

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(IILcom/google/c/a/a/b/e/a;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 138
    invoke-direct {p0, v4}, Lcom/google/c/a/a/a/c/j;->a(Z)Ljava/util/Map;

    move-result-object v2

    .line 139
    const-string v0, "startRev"

    invoke-static {p1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 140
    const-string v0, "includeType"

    const-string v1, "false"

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    if-ltz p2, :cond_0

    .line 142
    const-string v0, "endRev"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->d:Lcom/google/c/a/a/a/c/i;

    const-string v1, "catchup"

    const/4 v3, 0x0

    new-instance v5, Lcom/google/c/a/a/a/c/p;

    invoke-direct {v5, p0, p3}, Lcom/google/c/a/a/a/c/p;-><init>(Lcom/google/c/a/a/a/c/j;Lcom/google/c/a/a/b/e/a;)V

    const-class v6, Lcom/google/c/a/a/b/c/c;

    invoke-interface/range {v0 .. v6}, Lcom/google/c/a/a/a/c/i;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Object;ZLcom/google/c/a/a/b/e/f;Ljava/lang/Class;)V

    .line 162
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/a/f;ZLcom/google/c/a/a/b/e/f;)V
    .locals 3

    .prologue
    .line 256
    iput-object p3, p0, Lcom/google/c/a/a/a/c/j;->l:Lcom/google/c/a/a/b/e/f;

    .line 257
    iget-object v0, p1, Lcom/google/c/a/a/b/a/f;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/j;->f:Ljava/lang/String;

    .line 258
    iget-object v0, p1, Lcom/google/c/a/a/b/a/f;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/c/a/a/a/c/j;->g:Ljava/lang/String;

    .line 259
    if-eqz p2, :cond_0

    .line 260
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->d:Lcom/google/c/a/a/a/c/i;

    iget-object v1, p0, Lcom/google/c/a/a/a/c/j;->f:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/c/a/a/a/c/j;->g:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/c/a/a/a/c/i;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :cond_0
    invoke-direct {p0}, Lcom/google/c/a/a/a/c/j;->c()V

    .line 263
    return-void
.end method

.method public final a(Lcom/google/c/a/a/b/c/g;)V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->g:Ljava/lang/String;

    const-string v1, "Initial load must be completed before sending mutations."

    invoke-static {v0, v1}, Lcom/google/c/a/a/b/h/c;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    .line 90
    new-instance v0, Lcom/google/c/a/a/a/c/n;

    invoke-direct {v0, p0, p1}, Lcom/google/c/a/a/a/c/n;-><init>(Lcom/google/c/a/a/a/c/j;Lcom/google/c/a/a/b/c/g;)V

    .line 91
    iget-boolean v1, p0, Lcom/google/c/a/a/a/c/j;->i:Z

    if-eqz v1, :cond_0

    .line 92
    iget-object v1, p0, Lcom/google/c/a/a/a/c/j;->k:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    :goto_0
    return-void

    .line 94
    :cond_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public final a(Lcom/google/c/a/a/b/e/f;Z)V
    .locals 7

    .prologue
    .line 130
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 131
    const-string v0, "id"

    iget-object v1, p0, Lcom/google/c/a/a/a/c/j;->e:Ljava/lang/String;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->d:Lcom/google/c/a/a/a/c/i;

    const-string v1, "gs"

    const/4 v3, 0x0

    const/4 v4, 0x0

    new-instance v5, Lcom/google/c/a/a/a/c/q;

    invoke-direct {v5, p0, p1, p2}, Lcom/google/c/a/a/a/c/q;-><init>(Lcom/google/c/a/a/a/c/j;Lcom/google/c/a/a/b/e/f;Z)V

    const-class v6, Lcom/google/c/a/a/b/c/f;

    invoke-interface/range {v0 .. v6}, Lcom/google/c/a/a/a/c/i;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Object;ZLcom/google/c/a/a/b/e/f;Ljava/lang/Class;)V

    .line 134
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/c/a/a/b/e/g;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 166
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/c/a/a/a/c/j;->h:Z

    if-nez v0, :cond_1

    .line 167
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/c/a/a/a/c/j;->h:Z

    .line 168
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->d:Lcom/google/c/a/a/a/c/i;

    invoke-interface {v0}, Lcom/google/c/a/a/a/c/i;->a()V

    .line 169
    iget-boolean v0, p0, Lcom/google/c/a/a/a/c/j;->i:Z

    if-nez v0, :cond_0

    .line 170
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->d:Lcom/google/c/a/a/a/c/i;

    const-string v1, "leave"

    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/c/a/a/a/c/j;->a(Z)Ljava/util/Map;

    move-result-object v2

    const/4 v3, 0x0

    sget-object v6, Lcom/google/c/a/a/b/e/g;->a:Lcom/google/c/a/a/b/e/g;

    if-ne p1, v6, :cond_2

    :goto_0
    sget-object v5, Lcom/google/c/a/a/a/c/j;->a:Lcom/google/c/a/a/b/e/f;

    const-class v6, Ljava/lang/Void;

    invoke-interface/range {v0 .. v6}, Lcom/google/c/a/a/a/c/i;->a(Ljava/lang/String;Ljava/util/Map;Ljava/lang/Object;ZLcom/google/c/a/a/b/e/f;Ljava/lang/Class;)V

    .line 173
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/c/a/a/a/c/j;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 175
    :cond_1
    monitor-exit p0

    return-void

    :cond_2
    move v4, v5

    .line 170
    goto :goto_0

    .line 166
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/c/a/a/b/e/h;)V
    .locals 0

    .prologue
    .line 222
    iput-object p1, p0, Lcom/google/c/a/a/a/c/j;->j:Lcom/google/c/a/a/b/e/h;

    .line 223
    return-void
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 239
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/c/a/a/a/c/j;->i:Z

    if-eqz v0, :cond_0

    .line 240
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/c/a/a/a/c/j;->i:Z

    .line 241
    iget-object v0, p0, Lcom/google/c/a/a/a/c/j;->d:Lcom/google/c/a/a/a/c/i;

    invoke-interface {v0}, Lcom/google/c/a/a/a/c/i;->c()V

    .line 242
    invoke-direct {p0}, Lcom/google/c/a/a/a/c/j;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 244
    :cond_0
    monitor-exit p0

    return-void

    .line 239
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
