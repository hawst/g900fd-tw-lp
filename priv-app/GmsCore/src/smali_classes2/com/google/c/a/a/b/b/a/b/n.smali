.class final Lcom/google/c/a/a/b/b/a/b/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/c/a/a/b/b/a/g;

.field private final b:Lcom/google/c/a/a/b/b/a/g;


# direct methods
.method private constructor <init>(Lcom/google/c/a/a/b/b/a/g;Lcom/google/c/a/a/b/b/a/g;)V
    .locals 0

    .prologue
    .line 210
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 211
    iput-object p1, p0, Lcom/google/c/a/a/b/b/a/b/n;->a:Lcom/google/c/a/a/b/b/a/g;

    .line 212
    iput-object p2, p0, Lcom/google/c/a/a/b/b/a/b/n;->b:Lcom/google/c/a/a/b/b/a/g;

    .line 213
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/c/a/a/b/b/a/g;Lcom/google/c/a/a/b/b/a/g;B)V
    .locals 0

    .prologue
    .line 206
    invoke-direct {p0, p1, p2}, Lcom/google/c/a/a/b/b/a/b/n;-><init>(Lcom/google/c/a/a/b/b/a/g;Lcom/google/c/a/a/b/b/a/g;)V

    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 216
    instance-of v1, p1, Lcom/google/c/a/a/b/b/a/b/n;

    if-nez v1, :cond_1

    .line 220
    :cond_0
    :goto_0
    return v0

    .line 219
    :cond_1
    check-cast p1, Lcom/google/c/a/a/b/b/a/b/n;

    .line 220
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/n;->a:Lcom/google/c/a/a/b/b/a/g;

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/b/n;->a:Lcom/google/c/a/a/b/b/a/g;

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/n;->b:Lcom/google/c/a/a/b/b/a/g;

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/b/n;->b:Lcom/google/c/a/a/b/b/a/g;

    if-eq v1, v2, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/n;->a:Lcom/google/c/a/a/b/b/a/g;

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/b/n;->b:Lcom/google/c/a/a/b/b/a/g;

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/n;->b:Lcom/google/c/a/a/b/b/a/g;

    iget-object v2, p1, Lcom/google/c/a/a/b/b/a/b/n;->a:Lcom/google/c/a/a/b/b/a/g;

    if-ne v1, v2, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 224
    iget-object v0, p0, Lcom/google/c/a/a/b/b/a/b/n;->a:Lcom/google/c/a/a/b/b/a/g;

    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    .line 225
    iget-object v1, p0, Lcom/google/c/a/a/b/b/a/b/n;->b:Lcom/google/c/a/a/b/b/a/g;

    invoke-static {v1}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v1

    .line 226
    if-ge v0, v1, :cond_0

    .line 227
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    .line 229
    :goto_0
    return v0

    :cond_0
    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v2, v3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    goto :goto_0
.end method
